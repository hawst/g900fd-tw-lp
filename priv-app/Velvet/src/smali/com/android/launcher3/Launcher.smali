.class public Lcom/android/launcher3/Launcher;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Laaf;
.implements Labi;
.implements Lacm;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final JJ:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static JK:I

.field private static JL:I

.field private static JM:I

.field private static final Jn:Ljava/lang/Object;

.field private static KC:[Landroid/graphics/drawable/Drawable$ConstantState;

.field private static KD:[Landroid/graphics/drawable/Drawable$ConstantState;

.field private static KG:Ljava/util/ArrayList;

.field private static KI:Ljava/util/ArrayList;

.field private static KP:Lyo;

.field private static KQ:Z

.field private static Kw:Lyn;

.field private static Kx:Ljava/util/HashMap;


# instance fields
.field private GS:Landroid/view/LayoutInflater;

.field private JF:Lyq;

.field private JG:Landroid/animation/AnimatorSet;

.field private JH:Z

.field private JI:Ljava/util/HashMap;

.field private final JN:Landroid/content/BroadcastReceiver;

.field private final JO:Landroid/database/ContentObserver;

.field public JP:Lcom/android/launcher3/Workspace;

.field private JQ:Landroid/view/View;

.field private JR:Landroid/view/View;

.field private JS:Landroid/view/View;

.field private JT:Lahh;

.field private JU:Lyw;

.field private JV:Lwq;

.field private JW:Landroid/appwidget/AppWidgetProviderInfo;

.field private JX:I

.field private JY:[I

.field private JZ:Lvy;

.field private KA:J

.field private KB:Ljava/util/HashMap;

.field private KE:Landroid/graphics/drawable/Drawable;

.field private final KF:Ljava/util/ArrayList;

.field public KH:Landroid/content/SharedPreferences;

.field private KJ:Landroid/widget/ImageView;

.field private KK:Landroid/graphics/Bitmap;

.field private KL:Landroid/graphics/Canvas;

.field private KM:Landroid/graphics/Rect;

.field private KN:Lcom/android/launcher3/BubbleTextView;

.field private KO:Ljava/lang/Runnable;

.field private KR:Ladl;

.field KS:Lcom/android/launcher3/FocusIndicatorView;

.field private KT:Lyp;

.field private final KU:Landroid/content/BroadcastReceiver;

.field private KV:Ljava/util/ArrayList;

.field private KW:Ljava/lang/Runnable;

.field private Ka:Lcom/android/launcher3/Hotseat;

.field private Kb:Landroid/view/ViewGroup;

.field public Kc:Landroid/view/View;

.field private Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

.field private Ke:Lcom/android/launcher3/AppsCustomizePagedView;

.field private Kf:Z

.field private Kg:Landroid/view/View;

.field private Kh:Landroid/os/Bundle;

.field private Ki:Lyq;

.field private Kj:Landroid/text/SpannableStringBuilder;

.field private Kk:Z

.field private Kl:Z

.field private Km:Z

.field private Kn:Z

.field private Ko:Z

.field private Kp:Ljava/util/ArrayList;

.field private Kq:Ljava/util/ArrayList;

.field private Kr:Lzi;

.field private Ks:Z

.field private Kt:Z

.field private Ku:Z

.field private Kv:Z

.field private Ky:Landroid/view/View$OnTouchListener;

.field private Kz:J

.field private final mHandler:Landroid/os/Handler;

.field private xn:Lwi;

.field public xu:Lcom/android/launcher3/DragLayer;

.field private yg:Lty;

.field private zD:Lcom/android/launcher3/SearchDropTargetBar;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x1f4

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 239
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/launcher3/Launcher;->Jn:Ljava/lang/Object;

    .line 240
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/android/launcher3/Launcher;->JJ:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 246
    sput v4, Lcom/android/launcher3/Launcher;->JK:I

    .line 247
    const/4 v0, 0x5

    sput v0, Lcom/android/launcher3/Launcher;->JL:I

    .line 248
    sput v4, Lcom/android/launcher3/Launcher;->JM:I

    .line 313
    sput-object v5, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    .line 315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    .line 333
    new-array v0, v2, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 334
    new-array v0, v2, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher3/Launcher;->KG:Ljava/util/ArrayList;

    .line 342
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 343
    invoke-static {v3, v3}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    .line 345
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 352
    sput-object v5, Lcom/android/launcher3/Launcher;->KI:Ljava/util/ArrayList;

    .line 373
    const-string v0, "launcher_force_rotate"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher3/Launcher;->KQ:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 229
    sget-object v0, Lyq;->LV:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    .line 242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JI:Ljava/util/HashMap;

    .line 251
    new-instance v0, Lyl;

    invoke-direct {v0, p0, v1}, Lyl;-><init>(Lcom/android/launcher3/Launcher;B)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JN:Landroid/content/BroadcastReceiver;

    .line 253
    new-instance v0, Lyk;

    invoke-direct {v0, p0}, Lyk;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JO:Landroid/database/ContentObserver;

    .line 267
    new-instance v0, Lwq;

    invoke-direct {v0}, Lwq;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    .line 269
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/Launcher;->JX:I

    .line 271
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JY:[I

    .line 283
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Kf:Z

    .line 290
    sget-object v0, Lyq;->LU:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    .line 294
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Kk:Z

    .line 296
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Kl:Z

    .line 301
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    .line 302
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kq:Ljava/util/ArrayList;

    .line 308
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Ks:Z

    .line 309
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Kt:Z

    .line 310
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Ku:Z

    .line 311
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Kv:Z

    .line 320
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/launcher3/Launcher;->KA:J

    .line 325
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    .line 330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KF:Ljava/util/ArrayList;

    .line 359
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KM:Landroid/graphics/Rect;

    .line 363
    new-instance v0, Lwr;

    invoke-direct {v0, p0}, Lwr;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KO:Ljava/lang/Runnable;

    .line 1082
    new-instance v0, Lyi;

    invoke-direct {v0, p0}, Lyi;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KT:Lyp;

    .line 1603
    new-instance v0, Lwv;

    invoke-direct {v0, p0}, Lwv;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KU:Landroid/content/BroadcastReceiver;

    .line 1778
    new-instance v0, Lwy;

    invoke-direct {v0, p0}, Lwy;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    .line 4805
    new-instance v0, Lyc;

    invoke-direct {v0, p0}, Lyc;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KW:Ljava/lang/Runnable;

    return-void
.end method

.method private L(II)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 921
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v2, v1, Lwq;->Bd:J

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v2

    .line 924
    const/4 v5, 0x0

    .line 927
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 928
    const/4 v5, 0x3

    .line 929
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v0, p0, p2, v1}, Lyw;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v6

    .line 932
    new-instance v4, Lyh;

    invoke-direct {v4, p0, p2, v6, p1}, Lyh;-><init>(Lcom/android/launcher3/Launcher;ILandroid/appwidget/AppWidgetHostView;I)V

    .line 945
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fN()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 946
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v3}, Lcom/android/launcher3/DragLayer;->fN()Landroid/view/View;

    move-result-object v3

    check-cast v3, Luj;

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher3/Workspace;->a(Lwq;Lcom/android/launcher3/CellLayout;Luj;Ljava/lang/Runnable;ILandroid/view/View;Z)V

    .line 953
    :cond_0
    :goto_1
    return-void

    .line 941
    :cond_1
    if-nez p1, :cond_3

    .line 942
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v0, p2}, Lyw;->deleteAppWidgetId(I)V

    .line 943
    const/4 v5, 0x4

    move-object v4, v6

    goto :goto_0

    .line 949
    :cond_2
    if-eqz v4, :cond_0

    .line 951
    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    :cond_3
    move-object v4, v6

    goto :goto_0
.end method

.method private S(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2630
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 2633
    instance-of v0, v1, Ladh;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 2634
    check-cast v0, Ladh;

    .line 2635
    iget-object v2, v0, Ladh;->intent:Landroid/content/Intent;

    .line 2636
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 2637
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2638
    new-instance v5, Landroid/graphics/Rect;

    aget v6, v4, v10

    aget v7, v4, v11

    aget v8, v4, v10

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    aget v4, v4, v11

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v4, v9

    invoke-direct {v5, v6, v7, v8, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    move-object v12, v2

    move-object v2, v0

    move-object v0, v12

    .line 2648
    :goto_0
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    move-result v1

    .line 2649
    iget-object v4, p0, Lcom/android/launcher3/Launcher;->KR:Ladl;

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v5, v3}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v5, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.android.launcher3.action.LAUNCH"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "intent"

    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v2, :cond_0

    const-string v5, "container"

    iget-wide v6, v2, Ladh;->JA:J

    invoke-virtual {v0, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "screen"

    iget-wide v8, v2, Ladh;->Bd:J

    invoke-virtual {v5, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "cellX"

    iget v7, v2, Ladh;->Bb:I

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "cellY"

    iget v7, v2, Ladh;->Bc:I

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    iget-object v5, v4, Ladl;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v6, v4, Ladl;->Tg:Ljava/lang/String;

    invoke-virtual {v5, v0, v6}, Lcom/android/launcher3/Launcher;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    iget-object v0, v4, Ladl;->Ti:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_5

    iget-object v0, v4, Ladl;->Ti:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Ladl;->Tj:Ljava/util/ArrayList;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v4}, Ladl;->kj()V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    const/16 v5, 0x1000

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/io/DataOutputStream;->writeLong(J)V

    if-nez v2, :cond_6

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    :goto_2
    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2651
    :cond_1
    :goto_3
    if-eqz v1, :cond_2

    instance-of v0, p1, Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_2

    .line 2652
    check-cast p1, Lcom/android/launcher3/BubbleTextView;

    iput-object p1, p0, Lcom/android/launcher3/Launcher;->KN:Lcom/android/launcher3/BubbleTextView;

    .line 2653
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KN:Lcom/android/launcher3/BubbleTextView;

    invoke-virtual {v0, v11}, Lcom/android/launcher3/BubbleTextView;->B(Z)V

    .line 2655
    :cond_2
    return-void

    .line 2641
    :cond_3
    instance-of v0, v1, Lrr;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2643
    check-cast v0, Lrr;

    iget-object v0, v0, Lrr;->intent:Landroid/content/Intent;

    move-object v2, v3

    goto/16 :goto_0

    .line 2645
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must be a Shortcut or AppInfo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2649
    :cond_5
    iget-object v6, v4, Ladl;->Tj:Ljava/util/ArrayList;

    iget-object v0, v4, Ladl;->Tj:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v5, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_6
    :try_start_1
    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    iget-wide v6, v2, Ladh;->JA:J

    long-to-int v5, v6

    int-to-short v5, v5

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    iget-wide v6, v2, Ladh;->Bd:J

    long-to-int v5, v6

    int-to-short v5, v5

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    iget v5, v2, Ladh;->Bb:I

    int-to-short v5, v5

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, v4, Ladl;->Th:Ljava/io/DataOutputStream;

    iget v2, v2, Ladh;->Bc:I

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method private T(Z)V
    .locals 2

    .prologue
    .line 2175
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v0

    .line 2176
    iput-boolean p1, p0, Lcom/android/launcher3/Launcher;->Kk:Z

    .line 2177
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2178
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hA()V

    .line 2180
    :cond_0
    return-void
.end method

.method private U(Z)V
    .locals 2

    .prologue
    .line 2183
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v0

    .line 2184
    iput-boolean p1, p0, Lcom/android/launcher3/Launcher;->Kn:Z

    .line 2185
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2186
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hA()V

    .line 2188
    :cond_0
    return-void
.end method

.method private V(Z)V
    .locals 2

    .prologue
    .line 3114
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JQ:Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KE:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3116
    return-void

    .line 3114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Y(Z)V
    .locals 3

    .prologue
    .line 3751
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->setVisibility(I)V

    .line 3752
    sget-object v0, Lagv;->Xy:Lagv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Lagv;ZLjava/lang/Runnable;)V

    .line 3753
    sget-object v0, Lyq;->LV:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    .line 3754
    return-void
.end method

.method private a(Lyo;)J
    .locals 22

    .prologue
    .line 726
    move-object/from16 v0, p1

    iget-wide v4, v0, Lyo;->Bd:J

    .line 727
    move-object/from16 v0, p1

    iget-wide v6, v0, Lyo;->JA:J

    const-wide/16 v8, -0x64

    cmp-long v6, v6, v8

    if-nez v6, :cond_7

    .line 730
    move-object/from16 v0, p1

    iget-wide v4, v0, Lyo;->Bd:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/launcher3/Launcher;->f(J)J

    move-result-wide v4

    move-wide/from16 v20, v4

    .line 733
    :goto_0
    move-object/from16 v0, p1

    iget v4, v0, Lyo;->LS:I

    sparse-switch v4, :sswitch_data_0

    .line 748
    :cond_0
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher3/Launcher;->hB()V

    .line 749
    return-wide v20

    .line 735
    :sswitch_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lyo;->intent:Landroid/content/Intent;

    move-object/from16 v0, p1

    iget-wide v6, v0, Lyo;->JA:J

    move-object/from16 v0, p1

    iget v10, v0, Lyo;->Bb:I

    move-object/from16 v0, p1

    iget v11, v0, Lyo;->Bc:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Launcher;->JY:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v12, v5, Lwq;->JE:[I

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v6, v7, v1, v2}, Lcom/android/launcher3/Launcher;->a(JJ)Lcom/android/launcher3/CellLayout;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v4, v13}, Lzi;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Ladh;

    move-result-object v19

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/launcher3/Launcher;->l(Ladh;)Landroid/view/View;

    move-result-object v5

    if-ltz v10, :cond_1

    if-ltz v11, :cond_1

    const/4 v4, 0x0

    aput v10, v9, v4

    const/4 v4, 0x1

    aput v11, v9, v4

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v4 .. v13}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JLcom/android/launcher3/CellLayout;[IFZLuj;Ljava/lang/Runnable;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v14, Luq;

    invoke-direct {v14}, Luq;-><init>()V

    move-object/from16 v0, v19

    iput-object v0, v14, Luq;->Ge:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v13, 0x0

    const/4 v15, 0x1

    move-object v11, v8

    move-object v12, v9

    invoke-virtual/range {v10 .. v15}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;[IFLuq;Z)Z

    move-result v4

    if-nez v4, :cond_0

    move/from16 v4, v16

    :goto_2
    if-nez v4, :cond_4

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Launcher;->S(Z)V

    goto :goto_1

    :cond_1
    if-eqz v12, :cond_3

    const/4 v4, 0x0

    aget v11, v12, v4

    const/4 v4, 0x1

    aget v12, v12, v4

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object v10, v8

    move-object v15, v9

    invoke-virtual/range {v10 .. v15}, Lcom/android/launcher3/CellLayout;->b(IIII[I)[I

    move-result-object v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    :cond_3
    const/4 v4, 0x1

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v4, v10}, Lcom/android/launcher3/CellLayout;->b([III)Z

    move-result v4

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    aget v16, v9, v4

    const/4 v4, 0x1

    aget v17, v9, v4

    const/16 v18, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, v19

    move-wide v12, v6

    move-wide/from16 v14, v20

    invoke-static/range {v10 .. v18}, Lzi;->a(Landroid/content/Context;Lwq;JJIIZ)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher3/Launcher;->Km:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v8, 0x0

    aget v10, v9, v8

    const/4 v8, 0x1

    aget v11, v9, v8

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v14

    move-wide/from16 v8, v20

    invoke-virtual/range {v4 .. v14}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZ)V

    goto/16 :goto_1

    .line 739
    :sswitch_1
    move-object/from16 v0, p1

    iget v5, v0, Lyo;->LT:I

    move-object/from16 v0, p1

    iget-wide v6, v0, Lyo;->JA:J

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-wide/from16 v8, v20

    invoke-direct/range {v4 .. v11}, Lcom/android/launcher3/Launcher;->a(IJJLandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    goto/16 :goto_1

    .line 742
    :sswitch_2
    move-object/from16 v0, p1

    iget v4, v0, Lyo;->LT:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v5, v4}, Lcom/android/launcher3/Workspace;->bR(I)Lyx;

    move-result-object v4

    if-eqz v4, :cond_5

    instance-of v5, v4, Lacz;

    if-nez v5, :cond_6

    :cond_5
    const-string v4, "Launcher"

    const-string v5, "Widget update called, when the widget no longer exists."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v4}, Lyx;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lyy;

    const/4 v5, 0x0

    iput v5, v4, Lyy;->Mq:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher3/Workspace;->kX()V

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lzi;->a(Landroid/content/Context;Lwq;)V

    goto/16 :goto_1

    :cond_7
    move-wide/from16 v20, v4

    goto/16 :goto_0

    .line 733
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch
.end method

.method private a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 2

    .prologue
    .line 3930
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3931
    invoke-direct {p0, p2, p4}, Lcom/android/launcher3/Launcher;->a(Landroid/content/ComponentName;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3933
    if-eqz v0, :cond_0

    .line 3936
    if-nez v1, :cond_1

    .line 3937
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3943
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    :goto_1
    return-object v0

    .line 3939
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 3943
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/content/ComponentName;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 3876
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 3878
    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 3880
    if-eqz v1, :cond_0

    .line 3881
    invoke-virtual {v1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 3882
    if-eqz v1, :cond_0

    .line 3883
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v0

    .line 3884
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 3896
    :goto_0
    return-object v0

    .line 3887
    :catch_0
    move-exception v0

    .line 3889
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load toolbar icon; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3896
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 3891
    :catch_1
    move-exception v0

    .line 3893
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load toolbar icon from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KV:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic a(Lyn;)Lyn;
    .locals 0

    .prologue
    .line 137
    sput-object p0, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    return-object p0
.end method

.method private static a(ILandroid/content/Intent;ILwq;)Lyo;
    .locals 4

    .prologue
    .line 890
    new-instance v0, Lyo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lyo;-><init>(B)V

    .line 891
    iput p0, v0, Lyo;->LS:I

    .line 892
    iput-object p1, v0, Lyo;->intent:Landroid/content/Intent;

    .line 893
    iget-wide v2, p3, Lwq;->JA:J

    iput-wide v2, v0, Lyo;->JA:J

    .line 894
    iget-wide v2, p3, Lwq;->Bd:J

    iput-wide v2, v0, Lyo;->Bd:J

    .line 895
    iget v1, p3, Lwq;->Bb:I

    iput v1, v0, Lyo;->Bb:I

    .line 896
    iget v1, p3, Lwq;->Bc:I

    iput v1, v0, Lyo;->Bc:I

    .line 897
    iput p2, v0, Lyo;->LT:I

    .line 898
    return-object v0
.end method

.method private a(IJJLandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 28

    .prologue
    .line 1519
    if-nez p7, :cond_0

    .line 1520
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    move/from16 v0, p1

    invoke-virtual {v6, v0}, Lahh;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object p7

    .line 1524
    :cond_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move-wide/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher3/Launcher;->a(JJ)Lcom/android/launcher3/CellLayout;

    move-result-object v6

    .line 1526
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v1}, Lcom/android/launcher3/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v10

    .line 1527
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v1}, Lcom/android/launcher3/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v16

    .line 1532
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/launcher3/Launcher;->JY:[I

    .line 1533
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v8, v7, Lwq;->JE:[I

    .line 1534
    const/4 v7, 0x2

    new-array v14, v7, [I

    .line 1535
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v7, v7, Lwq;->Bb:I

    if-ltz v7, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v7, v7, Lwq;->Bc:I

    if-ltz v7, :cond_2

    .line 1537
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v8, v8, Lwq;->Bb:I

    aput v8, v13, v7

    .line 1538
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v8, v8, Lwq;->Bc:I

    aput v8, v13, v7

    .line 1539
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v8, v8, Lwq;->AY:I

    aput v8, v16, v7

    .line 1540
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v8, v8, Lwq;->AZ:I

    aput v8, v16, v7

    .line 1541
    const/4 v7, 0x1

    .line 1554
    :goto_0
    if-nez v7, :cond_5

    .line 1555
    const/4 v7, -0x1

    move/from16 v0, p1

    if-eq v0, v7, :cond_1

    .line 1558
    new-instance v7, Lwu;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v7, v0, v1}, Lwu;-><init>(Lcom/android/launcher3/Launcher;I)V

    sget-object v8, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Void;

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Lwu;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1565
    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher3/Launcher;->S(Z)V

    .line 1601
    :goto_1
    return-void

    .line 1542
    :cond_2
    if-eqz v8, :cond_4

    .line 1544
    const/4 v7, 0x0

    aget v7, v8, v7

    const/4 v9, 0x1

    aget v8, v8, v9

    const/4 v9, 0x0

    aget v9, v10, v9

    const/4 v11, 0x1

    aget v10, v10, v11

    const/4 v11, 0x0

    aget v11, v16, v11

    const/4 v12, 0x1

    aget v12, v16, v12

    invoke-virtual/range {v6 .. v14}, Lcom/android/launcher3/CellLayout;->a(IIIIII[I[I)[I

    move-result-object v7

    .line 1547
    const/4 v8, 0x0

    const/4 v9, 0x0

    aget v9, v14, v9

    aput v9, v16, v8

    .line 1548
    const/4 v8, 0x1

    const/4 v9, 0x1

    aget v9, v14, v9

    aput v9, v16, v8

    .line 1549
    if-eqz v7, :cond_3

    const/4 v7, 0x1

    goto :goto_0

    :cond_3
    const/4 v7, 0x0

    goto :goto_0

    .line 1551
    :cond_4
    const/4 v7, 0x0

    aget v7, v10, v7

    const/4 v8, 0x1

    aget v8, v10, v8

    invoke-virtual {v6, v13, v7, v8}, Lcom/android/launcher3/CellLayout;->b([III)Z

    move-result v7

    goto :goto_0

    .line 1570
    :cond_5
    new-instance v15, Lyy;

    move-object/from16 v0, p7

    iget-object v6, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    move/from16 v0, p1

    invoke-direct {v15, v0, v6}, Lyy;-><init>(ILandroid/content/ComponentName;)V

    .line 1572
    const/4 v6, 0x0

    aget v6, v16, v6

    iput v6, v15, Lyy;->AY:I

    .line 1573
    const/4 v6, 0x1

    aget v6, v16, v6

    iput v6, v15, Lyy;->AZ:I

    .line 1574
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v6, v6, Lwq;->JB:I

    iput v6, v15, Lyy;->JB:I

    .line 1575
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v6, v6, Lwq;->JC:I

    iput v6, v15, Lyy;->JC:I

    .line 1576
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Lahh;->c(Landroid/appwidget/AppWidgetProviderInfo;)Lahz;

    move-result-object v6

    iput-object v6, v15, Lyy;->Jl:Lahz;

    .line 1578
    const/4 v6, 0x0

    aget v20, v13, v6

    const/4 v6, 0x1

    aget v21, v13, v6

    const/16 v22, 0x0

    move-object/from16 v14, p0

    move-wide/from16 v16, p2

    move-wide/from16 v18, p4

    invoke-static/range {v14 .. v22}, Lzi;->a(Landroid/content/Context;Lwq;JJIIZ)V

    .line 1581
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/launcher3/Launcher;->Km:Z

    if-nez v6, :cond_6

    .line 1582
    if-nez p6, :cond_7

    .line 1584
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p7

    invoke-virtual {v6, v0, v1, v2}, Lyw;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v6

    iput-object v6, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    .line 1585
    iget-object v6, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    move/from16 v0, p1

    move-object/from16 v1, p7

    invoke-virtual {v6, v0, v1}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    .line 1591
    :goto_2
    iget-object v6, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v6, v15}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 1592
    iget-object v6, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/appwidget/AppWidgetHostView;->setVisibility(I)V

    .line 1593
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lyy;->t(Lcom/android/launcher3/Launcher;)V

    .line 1595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    move-object/from16 v16, v0

    iget-object v0, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v17, v0

    const/4 v6, 0x0

    aget v22, v13, v6

    const/4 v6, 0x1

    aget v23, v13, v6

    iget v0, v15, Lyy;->AY:I

    move/from16 v24, v0

    iget v0, v15, Lyy;->AZ:I

    move/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v26

    move-wide/from16 v18, p2

    move-wide/from16 v20, p4

    invoke-virtual/range {v16 .. v26}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZ)V

    .line 1598
    iget-object v6, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v6, v1}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 1600
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher3/Launcher;->hB()V

    goto/16 :goto_1

    .line 1588
    :cond_7
    move-object/from16 v0, p6

    iput-object v0, v15, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    goto :goto_2
.end method

.method private a(ILandroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 2

    .prologue
    .line 3953
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3954
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3955
    return-void
.end method

.method private a(ILwq;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 6

    .prologue
    .line 2203
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/Launcher;->a(ILwq;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;I)V

    .line 2204
    return-void
.end method

.method private a(ILwq;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;I)V
    .locals 9

    .prologue
    .line 2209
    iget-object v0, p4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    .line 2210
    iput-object p4, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    .line 2211
    iput p1, p0, Lcom/android/launcher3/Launcher;->JX:I

    .line 2214
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    const/4 v5, 0x5

    move-object v1, p4

    move v2, p1

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lahh;->a(Landroid/appwidget/AppWidgetProviderInfo;ILandroid/app/Activity;Landroid/appwidget/AppWidgetHost;I)V

    .line 2231
    :goto_0
    return-void

    .line 2219
    :cond_0
    new-instance v8, Lxa;

    invoke-direct {v8, p0}, Lxa;-><init>(Lcom/android/launcher3/Launcher;)V

    .line 2227
    iget-wide v2, p2, Lwq;->JA:J

    iget-wide v4, p2, Lwq;->Bd:J

    move-object v0, p0

    move v1, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher3/Launcher;->a(IJJLandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 2229
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v8, p5, v2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    goto :goto_0
.end method

.method private a(Lagv;ZLjava/lang/Runnable;)V
    .locals 25

    .prologue
    .line 3447
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    if-eqz v4, :cond_0

    .line 3448
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 3449
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->cancel()V

    .line 3450
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    .line 3453
    :cond_0
    invoke-static {}, Ladp;->km()Z

    move-result v13

    .line 3454
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 3456
    const v4, 0x7f0c0012

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getInteger(I)I

    .line 3457
    const v4, 0x7f0c0015

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getInteger(I)I

    .line 3458
    const v4, 0x7f0c0017

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    .line 3459
    const v4, 0x7f0c0018

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v16

    .line 3462
    const v4, 0x7f0c0013

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getInteger(I)I

    .line 3464
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    .line 3465
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    .line 3466
    const/4 v4, 0x0

    .line 3467
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 3469
    sget-object v5, Lagv;->Xv:Lagv;

    move-object/from16 v0, p1

    if-eq v0, v5, :cond_1

    .line 3470
    sget-object v5, Lagv;->Xx:Lagv;

    move-object/from16 v0, p1

    if-eq v0, v5, :cond_1

    sget-object v5, Lagv;->Xy:Lagv;

    move-object/from16 v0, p1

    if-ne v0, v5, :cond_2

    .line 3474
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v4, v0, v1, v15}, Lcom/android/launcher3/Workspace;->a(Lagv;ZLjava/util/ArrayList;)Landroid/animation/Animator;

    move-result-object v4

    .line 3479
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    .line 3481
    :goto_0
    if-eqz p2, :cond_16

    if-eqz v5, :cond_16

    .line 3482
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    .line 3483
    if-eqz v4, :cond_3

    .line 3484
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3487
    :cond_3
    const v4, 0x7f1100a1

    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/android/launcher3/AppsCustomizePagedView;

    .line 3490
    invoke-virtual {v12}, Lcom/android/launcher3/AppsCustomizePagedView;->jg()I

    move-result v4

    invoke-virtual {v12, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v11

    .line 3493
    invoke-virtual {v12}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v5

    .line 3494
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_6

    .line 3495
    invoke-virtual {v12, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 3496
    if-eq v9, v11, :cond_4

    .line 3497
    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3494
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 3479
    :cond_5
    const/4 v5, 0x0

    goto :goto_0

    .line 3500
    :cond_6
    const v4, 0x7f1100a0

    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 3505
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_c

    .line 3506
    invoke-virtual {v12}, Lcom/android/launcher3/AppsCustomizePagedView;->er()Lse;

    move-result-object v4

    .line 3507
    sget-object v5, Lse;->yS:Lse;

    if-ne v4, v5, :cond_d

    const/4 v4, 0x1

    move v9, v4

    .line 3510
    :goto_2
    if-eqz v9, :cond_e

    .line 3511
    const v4, 0x7f02029d

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3516
    :goto_3
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 3517
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    .line 3518
    mul-int/2addr v4, v4

    div-int/lit8 v4, v4, 0x4

    mul-int v5, v17, v17

    div-int/lit8 v5, v5, 0x4

    add-int/2addr v4, v5

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v18, v0

    .line 3521
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3522
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->z(Z)V

    .line 3524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    move-object/from16 v19, v0

    .line 3525
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 3526
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-static {v10, v0, v4}, Ladp;->b(Landroid/view/View;Landroid/view/View;[I)[I

    move-result-object v4

    .line 3529
    if-eqz v13, :cond_11

    .line 3532
    if-eqz v9, :cond_f

    div-int/lit8 v5, v17, 0x2

    int-to-float v5, v5

    .line 3533
    :goto_4
    if-eqz v9, :cond_10

    const/4 v4, 0x0

    :goto_5
    move/from16 v24, v5

    move v5, v4

    move/from16 v4, v24

    .line 3539
    :goto_6
    const/4 v7, 0x2

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v10, v7, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3540
    if-eqz v13, :cond_12

    new-instance v7, Labx;

    const/16 v20, 0x64

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v7, v0, v1}, Labx;-><init>(II)V

    .line 3547
    :goto_7
    const-string v20, "translationY"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    const/16 v22, 0x1

    aput v4, v21, v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v10, v0, v1}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    .line 3549
    add-int/lit8 v21, v14, -0x10

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3550
    add-int/lit8 v21, v16, 0x10

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 3551
    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3554
    const-string v20, "translationX"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    const/16 v22, 0x1

    aput v5, v21, v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v10, v0, v1}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 3556
    add-int/lit8 v20, v14, -0x10

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3557
    add-int/lit8 v20, v16, 0x10

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 3558
    invoke-virtual {v5, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3561
    if-nez v9, :cond_7

    if-nez v13, :cond_8

    .line 3562
    :cond_7
    if-eqz v13, :cond_13

    const v5, 0x3ecccccd    # 0.4f

    .line 3563
    :goto_8
    const/high16 v20, 0x3f800000    # 1.0f

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/View;->setAlpha(F)V

    .line 3564
    const-string v20, "alpha"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/high16 v23, 0x3f800000    # 1.0f

    aput v23, v21, v22

    const/16 v22, 0x1

    aput v5, v21, v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v10, v0, v1}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    .line 3566
    int-to-long v0, v14

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3567
    if-eqz v13, :cond_14

    move-object v5, v7

    :goto_9
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3569
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3572
    :cond_8
    if-eqz v11, :cond_9

    .line 3573
    const/4 v5, 0x2

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v11, v5, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3575
    const-string v5, "translationY"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    aput v22, v20, v21

    const/16 v21, 0x1

    aput v4, v20, v21

    move-object/from16 v0, v20

    invoke-static {v11, v5, v0}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 3577
    const/4 v5, 0x0

    invoke-virtual {v11, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 3578
    add-int/lit8 v5, v14, -0x10

    int-to-long v0, v5

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3579
    invoke-virtual {v4, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3580
    add-int/lit8 v5, v16, 0x10

    int-to-long v0, v5

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 3581
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3583
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v11, v4}, Landroid/view/View;->setAlpha(F)V

    .line 3584
    const-string v4, "alpha"

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v11, v4, v5}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 3585
    const-wide/16 v20, 0x64

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3586
    invoke-virtual {v4, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3587
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3590
    :cond_9
    const v4, 0x7f1100a2

    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 3591
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    .line 3592
    const-string v5, "alpha"

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v7, v20

    invoke-static {v4, v5, v7}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 3594
    int-to-long v0, v14

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3595
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x3fc00000    # 1.5f

    invoke-direct {v5, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3596
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3598
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 3600
    if-eqz v13, :cond_b

    .line 3601
    if-nez v9, :cond_a

    .line 3602
    const/4 v4, 0x4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3604
    :cond_a
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v4

    iget-object v4, v4, Lyu;->Mk:Lur;

    invoke-virtual {v4}, Lur;->gl()Ltu;

    move-result-object v4

    iget v4, v4, Ltu;->Eg:I

    .line 3606
    if-eqz v9, :cond_15

    const/4 v4, 0x0

    .line 3607
    :goto_a
    div-int/lit8 v5, v5, 0x2

    div-int/lit8 v7, v17, 0x2

    move/from16 v0, v18

    invoke-static {v10, v5, v7, v0, v4}, Lyr;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v4

    .line 3610
    new-instance v5, Labx;

    const/16 v7, 0x64

    const/4 v13, 0x0

    invoke-direct {v5, v7, v13}, Labx;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3611
    int-to-long v0, v14

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 3612
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 3614
    new-instance v5, Lxk;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v5, v0, v10, v9, v1}, Lxk;-><init>(Lcom/android/launcher3/Launcher;Landroid/view/View;ZLandroid/view/View;)V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3623
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3626
    :cond_b
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v6, v1, v4}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3627
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v8, v1, v4}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3628
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v4}, Lcom/android/launcher3/AppsCustomizePagedView;->ji()V

    .line 3631
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    new-instance v4, Lxl;

    move-object/from16 v5, p0

    move/from16 v7, p2

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v12}, Lxl;-><init>(Lcom/android/launcher3/Launcher;Landroid/view/View;ZLandroid/view/View;Ljava/lang/Runnable;Landroid/view/View;Landroid/view/View;Lcom/android/launcher3/AppsCustomizePagedView;)V

    invoke-virtual {v13, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3665
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    .line 3666
    new-instance v9, Lxm;

    move-object/from16 v10, p0

    move-object v12, v6

    move/from16 v13, p2

    move-object v14, v8

    invoke-direct/range {v9 .. v15}, Lxm;-><init>(Lcom/android/launcher3/Launcher;Landroid/animation/AnimatorSet;Landroid/view/View;ZLandroid/view/View;Ljava/util/ArrayList;)V

    .line 3690
    invoke-virtual {v6, v9}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 3700
    :goto_b
    return-void

    .line 3507
    :cond_d
    const/4 v4, 0x0

    move v9, v4

    goto/16 :goto_2

    .line 3513
    :cond_e
    const v4, 0x7f02029c

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 3532
    :cond_f
    const/4 v5, 0x1

    aget v5, v4, v5

    int-to-float v5, v5

    goto/16 :goto_4

    .line 3533
    :cond_10
    const/4 v7, 0x0

    aget v4, v4, v7

    int-to-float v4, v4

    goto/16 :goto_5

    .line 3535
    :cond_11
    mul-int/lit8 v4, v17, 0x5

    div-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    .line 3536
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 3540
    :cond_12
    new-instance v7, Labx;

    const/16 v20, 0x1e

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v7, v0, v1}, Labx;-><init>(II)V

    goto/16 :goto_7

    .line 3562
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_8

    .line 3567
    :cond_14
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v21, 0x3fc00000    # 1.5f

    move/from16 v0, v21

    invoke-direct {v5, v0}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    goto/16 :goto_9

    .line 3606
    :cond_15
    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    goto/16 :goto_a

    .line 3692
    :cond_16
    const/16 v4, 0x8

    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3693
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v6, v1, v4}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3694
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v6, v1, v4}, Lcom/android/launcher3/Launcher;->c(Landroid/view/View;ZZ)V

    .line 3695
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v6, v1, v4}, Lcom/android/launcher3/Launcher;->d(Landroid/view/View;ZZ)V

    .line 3696
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v8, v1, v4}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3697
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v8, v1, v4}, Lcom/android/launcher3/Launcher;->c(Landroid/view/View;ZZ)V

    .line 3698
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v8, v1, v4}, Lcom/android/launcher3/Launcher;->d(Landroid/view/View;ZZ)V

    goto :goto_b

    .line 3584
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static synthetic a(Landroid/content/Context;Lyn;)V
    .locals 4

    .prologue
    .line 137
    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/DataInputStream;

    const-string v2, "launcher.preferences"

    invoke-virtual {p0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lyn;->LR:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iput v1, p1, Lyn;->mcc:I

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iput v1, p1, Lyn;->mnc:I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_1
    :goto_4
    throw v0

    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method private a(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 2

    .prologue
    .line 1801
    if-eqz p2, :cond_0

    iget v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1808
    :cond_0
    :goto_0
    return-void

    .line 1802
    :cond_1
    iget v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1803
    instance-of v1, v0, Landroid/widget/Advanceable;

    if-eqz v1, :cond_0

    .line 1804
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1805
    check-cast v0, Landroid/widget/Advanceable;

    invoke-interface {v0}, Landroid/widget/Advanceable;->fyiWillBeAdvancedByHostKThx()V

    .line 1806
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 3958
    instance-of v0, p0, Lcom/android/launcher3/HolographicLinearLayout;

    if-eqz v0, :cond_1

    .line 3959
    check-cast p0, Lcom/android/launcher3/HolographicLinearLayout;

    .line 3960
    invoke-virtual {p0}, Lcom/android/launcher3/HolographicLinearLayout;->gV()V

    .line 3965
    :cond_0
    :goto_0
    return-void

    .line 3961
    :cond_1
    instance-of v0, p1, Lcom/android/launcher3/HolographicImageView;

    if-eqz v0, :cond_0

    .line 3962
    check-cast p1, Lcom/android/launcher3/HolographicImageView;

    .line 3963
    invoke-virtual {p1}, Lcom/android/launcher3/HolographicImageView;->gV()V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;II)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Lcom/android/launcher3/Launcher;->L(II)V

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;IJJLandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 8

    .prologue
    .line 137
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher3/Launcher;->a(IJJLandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;J)V
    .locals 2

    .prologue
    .line 137
    const-wide/16 v0, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->g(J)V

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->S(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher3/Launcher;->d(Landroid/view/View;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    .prologue
    .line 2567
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x1030128

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00e8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00e6

    new-instance v2, Lxc;

    invoke-direct {v2, p0, p1}, Lxc;-><init>(Lcom/android/launcher3/Launcher;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 2579
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 0

    .prologue
    .line 5156
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 5152
    return-void
.end method

.method private a(ZLse;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3762
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v1, Lyq;->LV:Lyq;

    if-eq v0, v1, :cond_0

    .line 3787
    :goto_0
    return-void

    .line 3764
    :cond_0
    if-eqz p3, :cond_1

    .line 3765
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->reset()V

    .line 3767
    :cond_1
    invoke-direct {p0, p1, v2, p2}, Lcom/android/launcher3/Launcher;->a(ZZLse;)V

    .line 3768
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    new-instance v1, Lxn;

    invoke-direct {v1, p0}, Lxn;-><init>(Lcom/android/launcher3/Launcher;)V

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizeTabHost;->post(Ljava/lang/Runnable;)Z

    .line 3777
    sget-object v0, Lyq;->LW:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    .line 3780
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Ks:Z

    .line 3781
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    .line 3782
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 3785
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method private a(ZZLse;)V
    .locals 25

    .prologue
    .line 3210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    if-eqz v2, :cond_0

    .line 3211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 3212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    .line 3213
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    .line 3216
    :cond_0
    invoke-static {}, Ladp;->km()Z

    move-result v10

    .line 3218
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 3220
    const v2, 0x7f0c0011

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    .line 3221
    const v2, 0x7f0c0014

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    .line 3222
    const v2, 0x7f0c0010

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    .line 3223
    const v2, 0x7f0c0018

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    .line 3226
    const v2, 0x7f0c0013

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    .line 3227
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    .line 3228
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    .line 3230
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 3232
    sget-object v2, Lse;->yS:Lse;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_6

    sget-object v2, Lagv;->Xz:Lagv;

    .line 3234
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v5, v2, v0, v1}, Lcom/android/launcher3/Workspace;->a(Lagv;ZLjava/util/ArrayList;)Landroid/animation/Animator;

    move-result-object v13

    .line 3236
    invoke-static {}, Lyu;->im()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lse;->yS:Lse;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_2

    .line 3239
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->b(Lse;)V

    .line 3243
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    .line 3245
    :goto_1
    if-eqz p1, :cond_f

    if-eqz v2, :cond_f

    .line 3246
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    .line 3247
    const v2, 0x7f1100a1

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/android/launcher3/AppsCustomizePagedView;

    .line 3250
    invoke-virtual {v9}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v2

    invoke-virtual {v9, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v8

    .line 3251
    const v2, 0x7f1100a0

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 3253
    sget-object v2, Lse;->yS:Lse;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_8

    const/4 v2, 0x1

    move v5, v2

    .line 3256
    :goto_2
    if-eqz v5, :cond_9

    .line 3257
    const v2, 0x7f02029d

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3263
    :goto_3
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->z(Z)V

    .line 3264
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3266
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/view/View;->setAlpha(F)V

    .line 3268
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    .line 3269
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    .line 3270
    mul-int v2, v14, v14

    div-int/lit8 v2, v2, 0x4

    mul-int v3, v17, v17

    div-int/lit8 v3, v3, 0x4

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    move/from16 v18, v0

    .line 3272
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 3273
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 3276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    const/4 v3, 0x0

    invoke-static {v7, v2, v3}, Ladp;->b(Landroid/view/View;Landroid/view/View;[I)[I

    move-result-object v2

    .line 3279
    const/4 v15, 0x0

    .line 3280
    if-eqz v10, :cond_d

    .line 3283
    if-eqz v5, :cond_a

    const v15, 0x3e99999a    # 0.3f

    .line 3284
    :goto_4
    if-eqz v5, :cond_b

    div-int/lit8 v3, v17, 0x2

    int-to-float v3, v3

    .line 3285
    :goto_5
    if-eqz v5, :cond_c

    const/4 v2, 0x0

    :goto_6
    move/from16 v24, v3

    move v3, v2

    move/from16 v2, v24

    .line 3292
    :goto_7
    const/16 v19, 0x2

    const/16 v20, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3293
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3294
    const-string v19, "alpha"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput v15, v20, v21

    const/16 v21, 0x1

    const/high16 v22, 0x3f800000    # 1.0f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v19

    .line 3295
    const-string v20, "translationY"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput v2, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x0

    aput v23, v21, v22

    invoke-static/range {v20 .. v21}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v20

    .line 3297
    const-string v21, "translationX"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v3, v22, v23

    const/4 v3, 0x1

    const/16 v23, 0x0

    aput v23, v22, v3

    invoke-static/range {v21 .. v22}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 3300
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v19, v21, v22

    const/16 v19, 0x1

    aput-object v20, v21, v19

    const/16 v19, 0x2

    aput-object v3, v21, v19

    move-object/from16 v0, v21

    invoke-static {v7, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 3303
    int-to-long v0, v11

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3304
    new-instance v19, Labx;

    const/16 v20, 0x64

    const/16 v21, 0x0

    invoke-direct/range {v19 .. v21}, Labx;-><init>(II)V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3308
    if-eqz v8, :cond_3

    .line 3309
    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3310
    const/4 v3, 0x2

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v8, v3, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3311
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3313
    const-string v3, "translationY"

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput v2, v19, v20

    const/16 v20, 0x1

    const/16 v21, 0x0

    aput v21, v19, v20

    move-object/from16 v0, v19

    invoke-static {v8, v3, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 3314
    invoke-virtual {v8, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 3315
    int-to-long v0, v11

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3316
    new-instance v2, Labx;

    const/16 v19, 0x64

    const/16 v20, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v2, v0, v1}, Labx;-><init>(II)V

    invoke-virtual {v3, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3317
    int-to-long v0, v12

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 3318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3320
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/view/View;->setAlpha(F)V

    .line 3321
    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v8, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 3322
    int-to-long v0, v11

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3323
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v19, 0x3fc00000    # 1.5f

    move/from16 v0, v19

    invoke-direct {v3, v0}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3324
    int-to-long v0, v12

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 3325
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3328
    :cond_3
    const v2, 0x7f1100a2

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 3329
    const v3, 0x3c23d70a    # 0.01f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 3330
    const-string v3, "alpha"

    const/4 v12, 0x1

    new-array v12, v12, [F

    const/16 v19, 0x0

    const/high16 v20, 0x3f800000    # 1.0f

    aput v20, v12, v19

    invoke-static {v2, v3, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 3332
    int-to-long v0, v11

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 3333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3335
    if-eqz v10, :cond_4

    .line 3336
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    .line 3337
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v2

    iget-object v2, v2, Lyu;->Mk:Lur;

    invoke-virtual {v2}, Lur;->gl()Ltu;

    move-result-object v2

    iget v2, v2, Ltu;->Eg:I

    .line 3339
    if-eqz v5, :cond_e

    const/4 v2, 0x0

    .line 3340
    :goto_8
    div-int/lit8 v10, v14, 0x2

    div-int/lit8 v12, v17, 0x2

    move/from16 v0, v18

    invoke-static {v7, v10, v12, v2, v0}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v2

    .line 3342
    int-to-long v10, v11

    invoke-virtual {v2, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 3343
    new-instance v10, Labx;

    const/16 v11, 0x64

    const/4 v12, 0x0

    invoke-direct {v10, v11, v12}, Labx;-><init>(II)V

    invoke-virtual {v2, v10}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3345
    new-instance v10, Lxh;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v5, v3}, Lxh;-><init>(Lcom/android/launcher3/Launcher;ZLandroid/view/View;)V

    invoke-virtual {v2, v10}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3357
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3360
    :cond_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    new-instance v2, Lxi;

    move-object/from16 v3, p0

    move/from16 v5, p1

    invoke-direct/range {v2 .. v9}, Lxi;-><init>(Lcom/android/launcher3/Launcher;Landroid/view/View;ZLcom/android/launcher3/AppsCustomizeTabHost;Landroid/view/View;Landroid/view/View;Lcom/android/launcher3/AppsCustomizePagedView;)V

    invoke-virtual {v10, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3381
    if-eqz v13, :cond_5

    .line 3382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v13}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3385
    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3386
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v6, v1, v2}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3387
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    .line 3388
    new-instance v8, Lxj;

    move-object/from16 v9, p0

    move-object v11, v4

    move/from16 v12, p1

    move-object v13, v6

    move-object v14, v7

    invoke-direct/range {v8 .. v16}, Lxj;-><init>(Lcom/android/launcher3/Launcher;Landroid/animation/AnimatorSet;Landroid/view/View;ZLcom/android/launcher3/AppsCustomizeTabHost;Landroid/view/View;FLjava/util/ArrayList;)V

    .line 3413
    invoke-virtual {v6}, Lcom/android/launcher3/AppsCustomizeTabHost;->bringToFront()V

    .line 3414
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3415
    invoke-virtual {v6, v8}, Lcom/android/launcher3/AppsCustomizeTabHost;->post(Ljava/lang/Runnable;)Z

    .line 3437
    :goto_9
    return-void

    .line 3232
    :cond_6
    sget-object v2, Lagv;->Xw:Lagv;

    goto/16 :goto_0

    .line 3243
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 3253
    :cond_8
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_2

    .line 3259
    :cond_9
    const v2, 0x7f02029c

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 3283
    :cond_a
    const/high16 v15, 0x3f800000    # 1.0f

    goto/16 :goto_4

    .line 3284
    :cond_b
    const/4 v3, 0x1

    aget v3, v2, v3

    int-to-float v3, v3

    goto/16 :goto_5

    .line 3285
    :cond_c
    const/16 v19, 0x0

    aget v2, v2, v19

    int-to-float v2, v2

    goto/16 :goto_6

    .line 3287
    :cond_d
    mul-int/lit8 v2, v17, 0x2

    div-int/lit8 v2, v2, 0x3

    int-to-float v2, v2

    .line 3288
    const/4 v3, 0x0

    goto/16 :goto_7

    .line 3339
    :cond_e
    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    goto/16 :goto_8

    .line 3417
    :cond_f
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->setTranslationX(F)V

    .line 3418
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->setTranslationY(F)V

    .line 3419
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->setScaleX(F)V

    .line 3420
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->setScaleY(F)V

    .line 3421
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3422
    invoke-virtual {v6}, Lcom/android/launcher3/AppsCustomizeTabHost;->bringToFront()V

    .line 3424
    if-nez p2, :cond_10

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v2

    iget-boolean v2, v2, Lyu;->Md:Z

    if-nez v2, :cond_10

    .line 3426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v2, :cond_10

    .line 3427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/launcher3/SearchDropTargetBar;->ao(Z)V

    .line 3430
    :cond_10
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3431
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/android/launcher3/Launcher;->c(Landroid/view/View;ZZ)V

    .line 3432
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/android/launcher3/Launcher;->d(Landroid/view/View;ZZ)V

    .line 3433
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v6, v1, v2}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3434
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v6, v1, v2}, Lcom/android/launcher3/Launcher;->c(Landroid/view/View;ZZ)V

    .line 3435
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v6, v1, v2}, Lcom/android/launcher3/Launcher;->d(Landroid/view/View;ZZ)V

    goto/16 :goto_9

    .line 3321
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2814
    const/high16 v3, 0x10000000

    invoke-virtual {p2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2818
    if-eqz p1, :cond_2

    :try_start_0
    const-string v3, "com.android.launcher3.intent.extra.shortcut.INGORE_LAUNCH_ANIMATION"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    move v4, v1

    .line 2820
    :goto_0
    invoke-static {p0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v5

    .line 2821
    invoke-static {p0}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v3

    .line 2824
    const-string v6, "profile"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2825
    const-string v6, "profile"

    const-wide/16 v8, -0x1

    invoke-virtual {p2, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 2826
    invoke-virtual {v3, v6, v7}, Laia;->m(J)Lahz;

    move-result-object v3

    .line 2830
    :goto_1
    if-eqz v4, :cond_0

    .line 2831
    invoke-static {}, Ladp;->km()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f05000a

    const v4, 0x7f050007

    invoke-static {p0, v2, v4}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v2

    .line 2834
    :goto_2
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 2837
    :cond_0
    if-eqz v3, :cond_1

    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v4

    invoke-virtual {v3, v4}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2839
    :cond_1
    invoke-virtual {p0, p2, v2}, Lcom/android/launcher3/Launcher;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    :goto_3
    move v0, v1

    .line 2853
    :goto_4
    return v0

    :cond_2
    move v4, v0

    .line 2818
    goto :goto_0

    .line 2831
    :cond_3
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {p1, v2, v4, v6, v7}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    move-result-object v2

    goto :goto_2

    .line 2842
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getSourceBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v4, v3, v6, v2}, Lahn;->a(Landroid/content/ComponentName;Lahz;Landroid/graphics/Rect;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 2846
    :catch_0
    move-exception v1

    .line 2847
    const v2, 0x7f0a0085

    invoke-static {p0, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2848
    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Launcher does not have the permission to launch "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Make sure to create a MAIN intent-filter for the corresponding activity or use the exported attribute for this activity. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "tag="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :cond_5
    move-object v3, v2

    goto :goto_1
.end method

.method public static synthetic a(Lcom/android/launcher3/Launcher;Z)Z
    .locals 0

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/android/launcher3/Launcher;->Ks:Z

    return p1
.end method

.method public static a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I
    .locals 3

    .prologue
    .line 1495
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-static {p0, v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Landroid/content/Context;Landroid/content/ComponentName;II)[I

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/ComponentName;II)[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1486
    invoke-static {p0, p1, v3}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1489
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    .line 1490
    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    .line 1491
    invoke-static {v1, v0, v3}, Lcom/android/launcher3/CellLayout;->c(II[I)[I

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Landroid/content/Context;Lyn;)V
    .locals 5

    .prologue
    .line 137
    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    const-string v2, "launcher.preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p1, Lyn;->LR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget v1, p1, Lyn;->mcc:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v1, p1, Lyn;->mnc:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v1

    :goto_2
    :try_start_4
    const-string v1, "launcher.preferences"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_0

    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_1
    :goto_4
    throw v0

    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method private b(Landroid/view/View;ZZ)V
    .locals 1

    .prologue
    .line 3129
    instance-of v0, p1, Labp;

    if-eqz v0, :cond_0

    .line 3130
    check-cast p1, Labp;

    invoke-interface {p1, p0, p2, p3}, Labp;->a(Lcom/android/launcher3/Launcher;ZZ)V

    .line 3132
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/Launcher;->c(Landroid/view/View;ZZ)V

    return-void
.end method

.method public static b(Lvy;)V
    .locals 4

    .prologue
    .line 2348
    sget-object v0, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    iget-wide v2, p0, Lvy;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2349
    return-void
.end method

.method private b(Ljava/lang/Runnable;Z)Z
    .locals 2

    .prologue
    .line 4141
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kl:Z

    if-eqz v0, :cond_2

    .line 4142
    const-string v0, "Launcher"

    const-string v1, "Deferring update until onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4143
    if-eqz p2, :cond_1

    .line 4144
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4147
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4148
    const/4 v0, 0x1

    .line 4150
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I
    .locals 3

    .prologue
    .line 1499
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    invoke-static {p0, v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Landroid/content/Context;Landroid/content/ComponentName;II)[I

    move-result-object v0

    return-object v0
.end method

.method static bk(I)V
    .locals 1

    .prologue
    .line 687
    sget-object v0, Lcom/android/launcher3/Launcher;->Jn:Ljava/lang/Object;

    monitor-enter v0

    .line 688
    monitor-exit v0

    return-void
.end method

.method private static bl(I)Lyq;
    .locals 4

    .prologue
    .line 1202
    sget-object v1, Lyq;->LV:Lyq;

    .line 1203
    invoke-static {}, Lyq;->values()[Lyq;

    move-result-object v2

    .line 1204
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 1205
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lyq;->ordinal()I

    move-result v3

    if-ne v3, p0, :cond_0

    .line 1206
    aget-object v0, v2, v0

    .line 1210
    :goto_1
    return-object v0

    .line 1204
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static synthetic c(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/Workspace;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    return-object v0
.end method

.method private c(Landroid/view/View;ZZ)V
    .locals 1

    .prologue
    .line 3135
    instance-of v0, p1, Labp;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 3136
    check-cast v0, Labp;

    invoke-interface {v0, p0, p2, p3}, Labp;->b(Lcom/android/launcher3/Launcher;ZZ)V

    .line 3140
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/Launcher;->e(Landroid/view/View;F)V

    .line 3141
    return-void
.end method

.method private d(Landroid/view/View;ZZ)V
    .locals 1

    .prologue
    .line 3150
    instance-of v0, p1, Labp;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 3151
    check-cast v0, Labp;

    invoke-interface {v0, p0, p2, p3}, Labp;->c(Lcom/android/launcher3/Launcher;ZZ)V

    .line 3155
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/Launcher;->e(Landroid/view/View;F)V

    .line 3156
    return-void
.end method

.method public static synthetic d(Lcom/android/launcher3/Launcher;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hg()V

    return-void
.end method

.method public static synthetic e(Lcom/android/launcher3/Launcher;)Lwq;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    return-object v0
.end method

.method private e(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 3144
    instance-of v0, p1, Labp;

    if-eqz v0, :cond_0

    .line 3145
    check-cast p1, Labp;

    invoke-interface {p1, p0, p2}, Labp;->a(Lcom/android/launcher3/Launcher;F)V

    .line 3147
    :cond_0
    return-void
.end method

.method private f(J)J
    .locals 1

    .prologue
    .line 908
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 910
    if-nez v0, :cond_0

    .line 913
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kQ()Z

    .line 914
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kS()J

    move-result-wide p1

    .line 916
    :cond_0
    return-wide p1
.end method

.method public static synthetic f(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/SearchDropTargetBar;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    return-object v0
.end method

.method public static synthetic g(Lcom/android/launcher3/Launcher;)Lyw;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    return-object v0
.end method

.method private g(J)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1754
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1755
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1756
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1757
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher3/Launcher;->Kz:J

    .line 1758
    return-void
.end method

.method public static synthetic h(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/DragLayer;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    return-object v0
.end method

.method private h(Lcom/android/launcher3/Folder;)V
    .locals 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3019
    invoke-virtual {p1}, Lcom/android/launcher3/Folder;->gt()Lvy;

    move-result-object v0

    iput-boolean v7, v0, Lvy;->Iz:Z

    .line 3021
    invoke-virtual {p1}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3022
    if-eqz v0, :cond_0

    .line 3023
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v1, p1, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->af(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/FolderIcon;

    .line 3024
    if-eqz v0, :cond_0

    const-string v1, "alpha"

    new-array v2, v8, [F

    aput v5, v2, v7

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const-string v1, "scaleX"

    new-array v3, v8, [F

    aput v5, v3, v7

    invoke-static {v1, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const-string v1, "scaleY"

    new-array v4, v8, [F

    aput v5, v4, v7

    invoke-static {v1, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    iget-object v5, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v6, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->h(Lcom/android/launcher3/FolderIcon;)V

    iget-object v5, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    const/4 v6, 0x3

    new-array v6, v6, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v6, v7

    aput-object v3, v6, v8

    const/4 v2, 0x2

    aput-object v4, v6, v2

    invoke-static {v5, v6}, Lyr;->a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0022

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v3, Lxg;

    invoke-direct {v3, p0, v1, v0}, Lxg;-><init>(Lcom/android/launcher3/Launcher;Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/FolderIcon;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 3026
    :cond_0
    invoke-virtual {p1}, Lcom/android/launcher3/Folder;->gu()V

    .line 3030
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/android/launcher3/DragLayer;->sendAccessibilityEvent(I)V

    .line 3031
    return-void
.end method

.method private h(Lcom/android/launcher3/FolderIcon;)V
    .locals 5

    .prologue
    .line 2876
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->getMeasuredWidth()I

    move-result v1

    .line 2877
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->getMeasuredHeight()I

    move-result v2

    .line 2880
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2881
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    .line 2883
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KK:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KK:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KK:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 2885
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KK:Landroid/graphics/Bitmap;

    .line 2886
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->KK:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KL:Landroid/graphics/Canvas;

    .line 2890
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    if-eqz v0, :cond_6

    .line 2891
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    .line 2898
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->KM:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, v4}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result v3

    .line 2899
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    .line 2900
    iget-object v4, p0, Lcom/android/launcher3/Launcher;->KM:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iput v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    .line 2901
    iget-object v4, p0, Lcom/android/launcher3/Launcher;->KM:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iput v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    .line 2902
    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    .line 2903
    int-to-float v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    .line 2905
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KL:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2906
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KL:Landroid/graphics/Canvas;

    invoke-virtual {p1, v1}, Lcom/android/launcher3/FolderIcon;->draw(Landroid/graphics/Canvas;)V

    .line 2907
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->KK:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2908
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2909
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher3/Folder;->gB()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 2910
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher3/Folder;->gC()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 2914
    :cond_3
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/android/launcher3/DragLayer;->indexOfChild(Landroid/view/View;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    .line 2915
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    .line 2917
    :cond_4
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/android/launcher3/DragLayer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2918
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2919
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->bringToFront()V

    .line 2921
    :cond_5
    return-void

    .line 2893
    :cond_6
    new-instance v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v2}, Lcom/android/launcher3/DragLayer$LayoutParams;-><init>(II)V

    goto/16 :goto_0
.end method

.method private hB()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 2193
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide v4, v0, Lwq;->JA:J

    .line 2194
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide v4, v0, Lwq;->Bd:J

    .line 2195
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput v2, v1, Lwq;->Bc:I

    iput v2, v0, Lwq;->Bb:I

    .line 2196
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput v2, v1, Lwq;->AZ:I

    iput v2, v0, Lwq;->AY:I

    .line 2197
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput v2, v1, Lwq;->JC:I

    iput v2, v0, Lwq;->JB:I

    .line 2198
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const/4 v1, 0x0

    iput-object v1, v0, Lwq;->JE:[I

    .line 2199
    return-void
.end method

.method private hL()I
    .locals 1

    .prologue
    .line 3866
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 3870
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 3868
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 3866
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private hS()Z
    .locals 2

    .prologue
    .line 4856
    sget-boolean v0, Lcom/android/launcher3/Launcher;->KQ:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 4858
    :goto_0
    return v0

    .line 4856
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hY()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4962
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    const-string v3, "launcher.first_run_activity_displayed"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hW()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4964
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hX()Landroid/content/Intent;

    move-result-object v2

    .line 4965
    if-eqz v2, :cond_1

    .line 4966
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 4967
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "launcher.first_run_activity_displayed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4971
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 4962
    goto :goto_0

    :cond_1
    move v0, v1

    .line 4971
    goto :goto_1
.end method

.method private hf()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 544
    .line 547
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hL()I

    move-result v3

    .line 548
    sget-object v1, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    if-nez v1, :cond_4

    .line 549
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hN()Z

    move-result v1

    .line 550
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->Z(Z)Z

    move-result v0

    .line 552
    :goto_0
    sget-object v4, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v4, v4, v3

    if-eqz v4, :cond_1

    .line 553
    sget-object v1, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->a(Landroid/graphics/drawable/Drawable$ConstantState;)V

    move v1, v2

    .line 556
    :cond_1
    sget-object v4, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v4, v4, v3

    if-eqz v4, :cond_3

    .line 557
    sget-object v0, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->b(Landroid/graphics/drawable/Drawable$ConstantState;)V

    .line 560
    :goto_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v0, :cond_2

    .line 561
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/SearchDropTargetBar;->c(ZZ)V

    .line 563
    :cond_2
    return-void

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private hg()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 566
    sget-object v0, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    if-nez v0, :cond_1

    .line 567
    new-instance v0, Lxe;

    invoke-direct {v0, p0}, Lxe;-><init>(Lcom/android/launcher3/Launcher;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lxe;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 584
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 586
    sget-object v3, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    iget-object v3, v3, Lyn;->LR:Ljava/lang/String;

    .line 587
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 589
    sget-object v5, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    iget v5, v5, Lyn;->mcc:I

    .line 590
    iget v6, v0, Landroid/content/res/Configuration;->mcc:I

    .line 592
    sget-object v7, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    iget v7, v7, Lyn;->mnc:I

    .line 593
    iget v8, v0, Landroid/content/res/Configuration;->mnc:I

    .line 595
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne v6, v5, :cond_2

    if-eq v8, v7, :cond_3

    :cond_2
    move v0, v2

    .line 597
    :goto_1
    if-eqz v0, :cond_0

    .line 598
    sget-object v0, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    iput-object v4, v0, Lyn;->LR:Ljava/lang/String;

    .line 599
    sget-object v0, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    iput v6, v0, Lyn;->mcc:I

    .line 600
    sget-object v0, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    iput v8, v0, Lyn;->mnc:I

    .line 602
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    iget-object v3, v0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, v0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 604
    sget-object v0, Lcom/android/launcher3/Launcher;->Kw:Lyn;

    .line 605
    new-instance v3, Lxp;

    invoke-direct {v3, p0, v0}, Lxp;-><init>(Lcom/android/launcher3/Launcher;Lyn;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v4, 0x0

    aput-object v4, v2, v1

    invoke-virtual {v3, v0, v2}, Lxp;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 595
    goto :goto_1

    .line 602
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private hk()V
    .locals 2

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 1193
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 1194
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1195
    return-void
.end method

.method private hm()V
    .locals 12

    .prologue
    const-wide/16 v4, 0x4e20

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1761
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kt:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Ks:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1762
    :goto_0
    iget-boolean v3, p0, Lcom/android/launcher3/Launcher;->Kf:Z

    if-eq v0, v3, :cond_0

    .line 1763
    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kf:Z

    .line 1764
    if-eqz v0, :cond_3

    .line 1765
    iget-wide v0, p0, Lcom/android/launcher3/Launcher;->KA:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    move-wide v0, v4

    .line 1766
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->g(J)V

    .line 1776
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 1761
    goto :goto_0

    .line 1765
    :cond_2
    iget-wide v0, p0, Lcom/android/launcher3/Launcher;->KA:J

    goto :goto_1

    .line 1768
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1769
    const-wide/16 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/android/launcher3/Launcher;->Kz:J

    sub-long/2addr v8, v10

    sub-long/2addr v4, v8

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/launcher3/Launcher;->KA:J

    .line 1772
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1773
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_2
.end method

.method public static synthetic i(Lcom/android/launcher3/Launcher;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    return-void
.end method

.method private ic()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5041
    new-instance v1, Lzb;

    invoke-direct {v1, p0}, Lzb;-><init>(Lcom/android/launcher3/Launcher;)V

    .line 5042
    invoke-virtual {v1}, Lzb;->is()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5043
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v0, p0}, Lzi;->u(Lcom/android/launcher3/Launcher;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5044
    iget-object v0, v1, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Workspace;->setAlpha(F)V

    :cond_0
    iget-object v2, v0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Hotseat;->setAlpha(F)V

    :cond_1
    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JR:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/android/launcher3/Launcher;->JR:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    iget-object v2, v0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/SearchDropTargetBar;->ao(Z)V

    :cond_3
    iget-object v0, v1, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    const v2, 0x7f11023b

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, v1, Lzb;->GS:Landroid/view/LayoutInflater;

    const v3, 0x7f0400ce

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f11026f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f110270

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5049
    :cond_4
    :goto_0
    return-void

    .line 5046
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lzb;->af(Z)V

    goto :goto_0
.end method

.method public static synthetic ie()I
    .locals 1

    .prologue
    .line 137
    sget v0, Lcom/android/launcher3/Launcher;->JM:I

    return v0
.end method

.method public static synthetic j(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/AppsCustomizeTabHost;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    return-object v0
.end method

.method private j(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 4240
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "11683562 -   orderedScreenIds: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ", "

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4243
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 4244
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 4245
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher3/Workspace;->h(J)J

    .line 4244
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4247
    :cond_0
    return-void
.end method

.method public static synthetic k(Lcom/android/launcher3/Launcher;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KO:Ljava/lang/Runnable;

    return-object v0
.end method

.method private l(Ladh;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1405
    const v1, 0x7f04001c

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v1, v0, p1}, Lcom/android/launcher3/Launcher;->a(ILandroid/view/ViewGroup;Ladh;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic l(Lcom/android/launcher3/Launcher;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic m(Lcom/android/launcher3/Launcher;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static synthetic n(Lcom/android/launcher3/Launcher;)Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JG:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method public static synthetic o(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/AppsCustomizePagedView;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    return-object v0
.end method

.method public static synthetic p(Lcom/android/launcher3/Launcher;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v0}, Lyw;->startListening()V

    :cond_0
    return-void
.end method

.method public static synthetic q(Lcom/android/launcher3/Launcher;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KV:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic r(Lcom/android/launcher3/Launcher;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->ic()V

    return-void
.end method

.method public static r(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final Q(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1390
    iput-object p1, p0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    .line 1391
    return-void
.end method

.method public R(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2589
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2590
    instance-of v3, v0, Ladh;

    if-nez v3, :cond_0

    .line 2591
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must be a Shortcut"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2595
    :cond_0
    check-cast v0, Ladh;

    .line 2596
    iget-object v3, v0, Ladh;->intent:Landroid/content/Intent;

    .line 2599
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2600
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 2602
    const-class v4, Lcom/android/launcher3/MemoryDumpActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2603
    invoke-static {p0}, Lcom/android/launcher3/MemoryDumpActivity;->startDump(Landroid/content/Context;)V

    .line 2627
    :cond_1
    :goto_0
    return-void

    .line 2605
    :cond_2
    const-class v4, Lcom/android/launcher3/ToggleWeightWatcher;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2606
    const-string v0, "com.android.launcher3.prefs"

    invoke-virtual {p0, v0, v2}, Lcom/android/launcher3/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v0, "debug.show_mem"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "debug.show_mem"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JS:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JS:Landroid/view/View;

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    const/16 v2, 0x8

    goto :goto_2

    .line 2612
    :cond_5
    instance-of v1, p1, Lcom/android/launcher3/BubbleTextView;

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ladh;->kg()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ladh;->bH(I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2615
    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lxd;

    invoke-direct {v1, p0, p1}, Lxd;-><init>(Lcom/android/launcher3/Launcher;Landroid/view/View;)V

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 2626
    :cond_6
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->S(Landroid/view/View;)V

    goto :goto_0
.end method

.method final S(Z)V
    .locals 2

    .prologue
    .line 1823
    if-eqz p1, :cond_0

    const v0, 0x7f0a009a

    .line 1824
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1825
    return-void

    .line 1823
    :cond_0
    const v0, 0x7f0a0099

    goto :goto_0
.end method

.method public T(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2664
    instance-of v0, p1, Lcom/android/launcher3/FolderIcon;

    if-nez v0, :cond_0

    .line 2665
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must be a FolderIcon"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2668
    :cond_0
    check-cast p1, Lcom/android/launcher3/FolderIcon;

    .line 2669
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gM()Lvy;

    move-result-object v0

    .line 2670
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/Workspace;->ae(Ljava/lang/Object;)Lcom/android/launcher3/Folder;

    move-result-object v1

    .line 2674
    iget-boolean v2, v0, Lvy;->Iz:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    .line 2675
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Folder info marked as open, but associated folder is not open. Screen: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lvy;->Bd:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lvy;->Bb:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lvy;->Bc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2677
    const/4 v2, 0x0

    iput-boolean v2, v0, Lvy;->Iz:Z

    .line 2680
    :cond_1
    iget-boolean v0, v0, Lvy;->Iz:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2682
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 2684
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->i(Lcom/android/launcher3/FolderIcon;)V

    .line 2700
    :cond_2
    :goto_0
    return-void

    .line 2688
    :cond_3
    if-eqz v1, :cond_2

    .line 2689
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->ab(Landroid/view/View;)I

    move-result v0

    .line 2691
    invoke-direct {p0, v1}, Lcom/android/launcher3/Launcher;->h(Lcom/android/launcher3/Folder;)V

    .line 2692
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 2694
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 2696
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->i(Lcom/android/launcher3/FolderIcon;)V

    goto :goto_0
.end method

.method public U(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2708
    sget-object v0, Lse;->yS:Lse;

    invoke-direct {p0, v1, v0, v1}, Lcom/android/launcher3/Launcher;->a(ZLse;Z)V

    .line 2709
    return-void
.end method

.method public V(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2717
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2718
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hC()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2719
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2720
    return-void
.end method

.method public W(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2728
    return-void
.end method

.method public W(Z)V
    .locals 3

    .prologue
    const/high16 v1, 0x100000

    .line 3119
    if-eqz p1, :cond_1

    move v0, v1

    .line 3120
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v2, v1

    .line 3122
    if-eq v0, v2, :cond_0

    .line 3123
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setFlags(II)V

    .line 3125
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->V(Z)V

    .line 3126
    return-void

    .line 3119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public X(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2755
    return-void
.end method

.method public final X(Z)V
    .locals 1

    .prologue
    .line 3711
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 3712
    return-void
.end method

.method final Y(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 3090
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected Z(Z)Z
    .locals 9

    .prologue
    const v8, 0x7f020166

    const/16 v6, 0x8

    const v7, 0x7f110243

    const/4 v1, 0x0

    .line 4018
    const v0, 0x7f110384

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 4019
    invoke-virtual {p0, v7}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 4022
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 4024
    invoke-virtual {v0}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v4

    .line 4026
    const/4 v0, 0x0

    .line 4027
    if-eqz v4, :cond_0

    .line 4029
    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4030
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4031
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 4034
    :cond_0
    if-nez v0, :cond_1

    .line 4037
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4038
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 4040
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_4

    .line 4041
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hL()I

    move-result v4

    .line 4042
    sget-object v5, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    const-string v6, "com.android.launcher.toolbar_voice_search_icon"

    invoke-direct {p0, v7, v0, v8, v6}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v6

    aput-object v6, v5, v4

    .line 4045
    sget-object v5, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v5, v5, v4

    if-nez v5, :cond_2

    .line 4046
    sget-object v5, Lcom/android/launcher3/Launcher;->KD:[Landroid/graphics/drawable/Drawable$ConstantState;

    const-string v6, "com.android.launcher.toolbar_icon"

    invoke-direct {p0, v7, v0, v8, v6}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    aput-object v0, v5, v4

    .line 4050
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4051
    :cond_3
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4052
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->aa(Z)V

    .line 4053
    invoke-static {v2, v3}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 4054
    const/4 v0, 0x1

    .line 4059
    :goto_0
    return v0

    .line 4056
    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 4057
    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 4058
    :cond_6
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->aa(Z)V

    move v0, v1

    .line 4059
    goto :goto_0
.end method

.method public final a(ILandroid/view/ViewGroup;Ladh;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->GS:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/BubbleTextView;

    .line 1420
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    const/4 v2, 0x1

    invoke-virtual {v0, p3, v1, v2}, Lcom/android/launcher3/BubbleTextView;->a(Ladh;Lwi;Z)V

    .line 1421
    invoke-virtual {v0, p0}, Lcom/android/launcher3/BubbleTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1422
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KS:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/BubbleTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1423
    return-object v0
.end method

.method public final a(JJ)Lcom/android/launcher3/CellLayout;
    .locals 3

    .prologue
    .line 3098
    const-wide/16 v0, -0x65

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 3099
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v0, :cond_0

    .line 3100
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 3105
    :goto_0
    return-object v0

    .line 3102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3105
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p3, p4}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Lcom/android/launcher3/CellLayout;JJII)Lcom/android/launcher3/FolderIcon;
    .locals 12

    .prologue
    .line 2328
    new-instance v1, Lvy;

    invoke-direct {v1}, Lvy;-><init>()V

    .line 2329
    const v0, 0x7f0a0084

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lvy;->title:Ljava/lang/CharSequence;

    .line 2332
    const/4 v8, 0x0

    move-object v0, p0

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lzi;->a(Landroid/content/Context;Lwq;JJIIZ)V

    .line 2334
    sget-object v0, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    iget-wide v2, v1, Lvy;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2337
    const v0, 0x7f04008d

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    invoke-static {v0, p0, p1, v1}, Lcom/android/launcher3/FolderIcon;->a(ILcom/android/launcher3/Launcher;Landroid/view/ViewGroup;Lvy;)Lcom/android/launcher3/FolderIcon;

    move-result-object v1

    .line 2339
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v10

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v10}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZ)V

    .line 2342
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 2343
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0, v1}, Ladg;->ad(Landroid/view/View;)V

    .line 2344
    return-object v1
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;)Lwq;
    .locals 6

    .prologue
    .line 5080
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v5

    invoke-static {p0}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v0

    invoke-virtual {v0, p2, v5}, Laia;->a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v0, Ladh;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Ladh;-><init>(Landroid/content/Intent;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Lahz;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;Lym;Ljava/lang/String;)Lyp;
    .locals 1

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher3/Workspace;->b(Landroid/view/View;Lym;Ljava/lang/String;)V

    .line 1127
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KT:Lyp;

    return-object v0
.end method

.method public final a(Lacy;JJ[I[I[I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2273
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hB()V

    .line 2274
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide p2, p1, Lacy;->JA:J

    iput-wide p2, v0, Lwq;->JA:J

    .line 2275
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide p4, p1, Lacy;->Bd:J

    iput-wide p4, v0, Lwq;->Bd:J

    .line 2276
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-object v4, v0, Lwq;->JE:[I

    .line 2277
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v1, p1, Lacy;->JB:I

    iput v1, v0, Lwq;->JB:I

    .line 2278
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v1, p1, Lacy;->JC:I

    iput v1, v0, Lwq;->JC:I

    .line 2280
    if-eqz p6, :cond_0

    .line 2281
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    aget v1, p6, v2

    iput v1, v0, Lwq;->Bb:I

    .line 2282
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    aget v1, p6, v3

    iput v1, v0, Lwq;->Bc:I

    .line 2284
    :cond_0
    if-eqz p7, :cond_1

    .line 2285
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    aget v1, p7, v2

    iput v1, v0, Lwq;->AY:I

    .line 2286
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    aget v1, p7, v3

    iput v1, v0, Lwq;->AZ:I

    .line 2289
    :cond_1
    iget-object v0, p1, Lacy;->RR:Landroid/appwidget/AppWidgetHostView;

    .line 2291
    if-eqz v0, :cond_2

    .line 2292
    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHostView;->getAppWidgetId()I

    move-result v1

    .line 2293
    iget-object v2, p1, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {p0, v1, p1, v0, v2}, Lcom/android/launcher3/Launcher;->a(ILwq;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 2316
    :goto_0
    return-void

    .line 2297
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v0}, Lyw;->allocateAppWidgetId()I

    move-result v0

    .line 2298
    iget-object v1, p1, Lacy;->RS:Landroid/os/Bundle;

    .line 2300
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    iget-object v3, p1, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v2, v0, v3, v1}, Lahh;->a(ILandroid/appwidget/AppWidgetProviderInfo;Landroid/os/Bundle;)Z

    move-result v1

    .line 2302
    if-eqz v1, :cond_3

    .line 2303
    iget-object v1, p1, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {p0, v0, p1, v4, v1}, Lcom/android/launcher3/Launcher;->a(ILwq;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    goto :goto_0

    .line 2305
    :cond_3
    iget-object v1, p1, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v1, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    .line 2306
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.appwidget.action.APPWIDGET_BIND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2307
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2308
    const-string v0, "appWidgetProvider"

    iget-object v2, p1, Lacy;->xr:Landroid/content/ComponentName;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2309
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v0, v2}, Lahh;->c(Landroid/appwidget/AppWidgetProviderInfo;)Lahz;

    move-result-object v0

    const-string v2, "appWidgetProviderProfile"

    invoke-virtual {v0, v1, v2}, Lahz;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2313
    const/16 v0, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Landroid/content/ComponentName;JJ[I[I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2248
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hB()V

    .line 2249
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide p2, v0, Lwq;->JA:J

    .line 2250
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide p4, v0, Lwq;->Bd:J

    .line 2251
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const/4 v1, 0x0

    iput-object v1, v0, Lwq;->JE:[I

    .line 2253
    if-eqz p6, :cond_0

    .line 2254
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const/4 v1, 0x0

    aget v1, p6, v1

    iput v1, v0, Lwq;->Bb:I

    .line 2255
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    aget v1, p6, v2

    iput v1, v0, Lwq;->Bc:I

    .line 2258
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2259
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2260
    invoke-static {p0, v0, v2}, Ladp;->a(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 2261
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 3

    .prologue
    const v2, 0x7f110382

    .line 4011
    const v0, 0x7f110383

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 4012
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 4013
    invoke-direct {p0, v2, p1}, Lcom/android/launcher3/Launcher;->a(ILandroid/graphics/drawable/Drawable$ConstantState;)V

    .line 4014
    invoke-static {v1, v0}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 4015
    return-void
.end method

.method public final a(Landroid/view/View;Lwq;Lui;)V
    .locals 1

    .prologue
    .line 5096
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 5097
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Workspace;->af(Landroid/view/View;)V

    .line 5098
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1, p3}, Lcom/android/launcher3/Workspace;->b(Landroid/view/View;Lui;)V

    .line 5099
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 4159
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kq:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4160
    return-void
.end method

.method public final a(Ljava/util/ArrayList;IIZ)V
    .locals 20

    .prologue
    .line 4318
    new-instance v4, Lxr;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v4 .. v9}, Lxr;-><init>(Lcom/android/launcher3/Launcher;Ljava/util/ArrayList;IIZ)V

    .line 4323
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4418
    :goto_0
    return-void

    .line 4328
    :cond_0
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v18

    .line 4329
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 4330
    if-eqz p4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->yg:Lty;

    invoke-virtual {v6}, Lty;->fG()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget v6, Lcom/android/launcher3/Launcher;->JL:I

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    const/4 v4, 0x1

    move v15, v4

    .line 4331
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    .line 4332
    const-wide/16 v16, -0x1

    .line 4333
    :goto_3
    move/from16 v0, p2

    move/from16 v1, p3

    if-ge v0, v1, :cond_5

    .line 4334
    invoke-virtual/range {p1 .. p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v14, v5

    check-cast v14, Lwq;

    .line 4337
    iget-wide v6, v14, Lwq;->JA:J

    const-wide/16 v8, -0x65

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v5, :cond_8

    .line 4339
    :cond_1
    iget v5, v14, Lwq;->Jz:I

    packed-switch v5, :pswitch_data_0

    .line 4385
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Invalid Item Type"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 4330
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    move v15, v4

    goto :goto_2

    :pswitch_0
    move-object v5, v14

    .line 4345
    check-cast v5, Ladh;

    .line 4346
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/launcher3/Launcher;->l(Ladh;)Landroid/view/View;

    move-result-object v5

    .line 4351
    iget-wide v6, v14, Lwq;->JA:J

    const-wide/16 v8, -0x64

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    .line 4352
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-wide v8, v14, Lwq;->Bd:J

    invoke-virtual {v6, v8, v9}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v6

    .line 4353
    if-eqz v6, :cond_4

    iget v7, v14, Lwq;->Bb:I

    iget v8, v14, Lwq;->Bc:I

    invoke-virtual {v6, v7, v8}, Lcom/android/launcher3/CellLayout;->D(II)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 4354
    iget v7, v14, Lwq;->Bb:I

    iget v8, v14, Lwq;->Bc:I

    invoke-virtual {v6, v7, v8}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v6

    .line 4355
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    .line 4356
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Collision while binding workspace item: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". Collides with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 4358
    invoke-static {}, Lyu;->isDogfoodBuild()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 4359
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 4361
    :cond_4
    iget-wide v6, v14, Lwq;->JA:J

    iget-wide v8, v14, Lwq;->Bd:J

    iget v10, v14, Lwq;->Bb:I

    iget v11, v14, Lwq;->Bc:I

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual/range {v4 .. v13}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIII)V

    .line 4368
    if-eqz v15, :cond_8

    .line 4370
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setAlpha(F)V

    .line 4371
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setScaleX(F)V

    .line 4372
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setScaleY(F)V

    .line 4373
    const/4 v6, 0x3

    new-array v6, v6, [Landroid/animation/PropertyValuesHolder;

    const/4 v7, 0x0

    const-string v8, "alpha"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    aput v11, v9, v10

    invoke-static {v8, v9}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "scaleX"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    aput v11, v9, v10

    invoke-static {v8, v9}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "scaleY"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    aput v11, v9, v10

    invoke-static {v8, v9}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lyr;->a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v6, 0x1c2

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    mul-int/lit8 v6, p2, 0x55

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v6, Ladj;

    invoke-direct {v6}, Ladj;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 4374
    iget-wide v6, v14, Lwq;->Bd:J

    .line 4333
    :goto_4
    add-int/lit8 p2, p2, 0x1

    move-wide/from16 v16, v6

    goto/16 :goto_3

    .line 4378
    :pswitch_1
    const v7, 0x7f04008d

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    move-object v6, v14

    check-cast v6, Lvy;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    move-object/from16 v0, p0

    invoke-static {v7, v0, v5, v6}, Lcom/android/launcher3/FolderIcon;->a(ILcom/android/launcher3/Launcher;Landroid/view/ViewGroup;Lvy;)Lcom/android/launcher3/FolderIcon;

    move-result-object v5

    .line 4381
    iget-wide v6, v14, Lwq;->JA:J

    iget-wide v8, v14, Lwq;->Bd:J

    iget v10, v14, Lwq;->Bb:I

    iget v11, v14, Lwq;->Bc:I

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual/range {v4 .. v13}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIII)V

    move-wide/from16 v6, v16

    .line 4383
    goto :goto_4

    .line 4389
    :cond_5
    if-eqz v15, :cond_6

    .line 4391
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-lez v5, :cond_6

    .line 4392
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v6}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v6

    .line 4393
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Lcom/android/launcher3/Workspace;->k(J)I

    move-result v5

    .line 4394
    new-instance v8, Lxs;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v8, v0, v1, v2}, Lxs;-><init>(Lcom/android/launcher3/Launcher;Landroid/animation/AnimatorSet;Ljava/util/Collection;)V

    .line 4400
    cmp-long v6, v16, v6

    if-eqz v6, :cond_7

    .line 4403
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    new-instance v7, Lxt;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v5, v8}, Lxt;-><init>(Lcom/android/launcher3/Launcher;ILjava/lang/Runnable;)V

    sget v5, Lcom/android/launcher3/Launcher;->JK:I

    int-to-long v8, v5

    invoke-virtual {v6, v7, v8, v9}, Lcom/android/launcher3/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4417
    :cond_6
    :goto_5
    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->requestLayout()V

    goto/16 :goto_0

    .line 4413
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    sget v6, Lcom/android/launcher3/Launcher;->JM:I

    int-to-long v6, v6

    invoke-virtual {v5, v8, v6, v7}, Lcom/android/launcher3/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_5

    :cond_8
    move-wide/from16 v6, v16

    goto/16 :goto_4

    .line 4339
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;Lahz;)V
    .locals 2

    .prologue
    .line 4775
    new-instance v0, Lyb;

    invoke-direct {v0, p0, p1, p2, p3}, Lyb;-><init>(Lcom/android/launcher3/Launcher;Ljava/util/ArrayList;Ljava/util/ArrayList;Lahz;)V

    .line 4780
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4799
    :cond_0
    :goto_0
    return-void

    .line 4784
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4785
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1, p3}, Lcom/android/launcher3/Workspace;->a(Ljava/util/ArrayList;Lahz;)V

    .line 4787
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4788
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p2, p3}, Lcom/android/launcher3/Workspace;->b(Ljava/util/ArrayList;Lahz;)V

    .line 4792
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    invoke-virtual {v0, p1, p2}, Lty;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 4795
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_0

    .line 4797
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p2}, Lcom/android/launcher3/AppsCustomizePagedView;->f(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4277
    new-instance v0, Lxq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lxq;-><init>(Lcom/android/launcher3/Launcher;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 4282
    invoke-direct {p0, v0, v6}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4309
    :cond_0
    :goto_0
    return-void

    .line 4287
    :cond_1
    if-eqz p1, :cond_2

    .line 4288
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->j(Ljava/util/ArrayList;)V

    .line 4293
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4294
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, p2, v6, v0, v6}, Lcom/android/launcher3/Launcher;->a(Ljava/util/ArrayList;IIZ)V

    .line 4297
    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 4298
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, p3, v6, v0, v1}, Lcom/android/launcher3/Launcher;->a(Ljava/util/ArrayList;IIZ)V

    .line 4303
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v6, v6}, Lcom/android/launcher3/Workspace;->d(ZZ)V

    .line 4305
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_0

    .line 4307
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p4}, Lcom/android/launcher3/AppsCustomizePagedView;->d(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final a(Lyy;)V
    .locals 2

    .prologue
    .line 1818
    iget-object v0, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    .line 1819
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    .line 1820
    return-void
.end method

.method public final a(ZILjava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 3798
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v1, Lyq;->LX:Lyq;

    if-eq v0, v1, :cond_0

    .line 3814
    :goto_0
    return-void

    .line 3800
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    new-instance v1, Lxo;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lxo;-><init>(Lcom/android/launcher3/Launcher;ZLjava/lang/Runnable;)V

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final a(ZLjava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3719
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v3, Lyq;->LV:Lyq;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lm()Lagv;

    move-result-object v0

    sget-object v3, Lagv;->Xv:Lagv;

    if-eq v0, v3, :cond_3

    .line 3720
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v3, Lyq;->LV:Lyq;

    if-eq v0, v3, :cond_4

    move v0, v1

    .line 3721
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v3, v2}, Lcom/android/launcher3/Workspace;->setVisibility(I)V

    .line 3722
    sget-object v3, Lagv;->Xv:Lagv;

    invoke-direct {p0, v3, p1, p2}, Lcom/android/launcher3/Launcher;->a(Lagv;ZLjava/lang/Runnable;)V

    .line 3726
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v3, :cond_2

    .line 3727
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    move v2, v1

    :cond_1
    invoke-virtual {v3, v2}, Lcom/android/launcher3/SearchDropTargetBar;->an(Z)V

    .line 3731
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 3732
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 3737
    :cond_3
    sget-object v0, Lyq;->LV:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    .line 3740
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Ks:Z

    .line 3741
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    .line 3744
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 3747
    return-void

    :cond_4
    move v0, v2

    .line 3720
    goto :goto_0
.end method

.method final a(Landroid/content/ComponentName;ILahz;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2792
    and-int/lit8 v1, p2, 0x1

    if-nez v1, :cond_0

    .line 2795
    const v1, 0x7f0a00bc

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2809
    :goto_0
    return v0

    .line 2799
    :cond_0
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2800
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 2801
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.DELETE"

    const-string v4, "package"

    invoke-static {v4, v0, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2803
    const/high16 v0, 0x10800000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2805
    if-eqz p3, :cond_1

    .line 2806
    const-string v0, "android.intent.extra.USER"

    invoke-virtual {p3, v2, v0}, Lahz;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2808
    :cond_1
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 2809
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZLandroid/os/Bundle;Landroid/graphics/Rect;)Z
    .locals 5

    .prologue
    .line 2092
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    invoke-virtual {v0}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "Launcher"

    const-string v1, "No global search activity found."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2092
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.search.action.GLOBAL_SEARCH"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    if-nez p3, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :goto_1
    const-string v3, "source"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "source"

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v3, "app_data"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "query"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    if-eqz p2, :cond_3

    const-string v0, "select_query"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v2, p4}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Global search activity not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public aa(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4071
    const v1, 0x7f110242

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 4072
    if-eqz v2, :cond_0

    .line 4073
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->lk()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 4075
    :goto_0
    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 4076
    invoke-virtual {v2}, Landroid/view/View;->bringToFront()V

    .line 4078
    :cond_0
    return-void

    :cond_1
    move v1, v0

    .line 4073
    goto :goto_0

    .line 4075
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final ab(Z)V
    .locals 0

    .prologue
    .line 4085
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->aa(Z)V

    .line 4086
    return-void
.end method

.method public ac(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 4571
    new-instance v0, Lxw;

    invoke-direct {v0, p0, p1}, Lxw;-><init>(Lcom/android/launcher3/Launcher;Z)V

    .line 4576
    invoke-direct {p0, v0, v2}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4614
    :goto_0
    return-void

    .line 4579
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kh:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 4580
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4581
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 4583
    :cond_1
    iput-object v4, p0, Lcom/android/launcher3/Launcher;->Kh:Landroid/os/Bundle;

    .line 4586
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lw()V

    .line 4588
    invoke-direct {p0, v2}, Lcom/android/launcher3/Launcher;->T(Z)V

    .line 4589
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    const-string v1, "launcher.first_load_complete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.launcher3.action.FIRST_LOAD_COMPLETE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/Launcher;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "launcher.first_load_complete"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4593
    :cond_3
    sget-object v0, Lcom/android/launcher3/Launcher;->KP:Lyo;

    if-eqz v0, :cond_4

    .line 4594
    sget-object v0, Lcom/android/launcher3/Launcher;->KP:Lyo;

    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->a(Lyo;)J

    move-result-wide v0

    .line 4599
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    new-instance v3, Lxx;

    invoke-direct {v3, p0, v0, v1}, Lxx;-><init>(Lcom/android/launcher3/Launcher;J)V

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 4605
    sput-object v4, Lcom/android/launcher3/Launcher;->KP:Lyo;

    .line 4608
    :cond_4
    if-eqz p1, :cond_5

    .line 4609
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Workspace;->r(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 4610
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Workspace;->r(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/android/launcher3/Launcher;->KI:Ljava/util/ArrayList;

    .line 4612
    :cond_5
    invoke-static {p0}, Laht;->C(Landroid/content/Context;)Laht;

    move-result-object v0

    invoke-virtual {v0}, Laht;->lL()V

    .line 4613
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v0, p0}, Lzi;->s(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public final ad(Z)V
    .locals 4

    .prologue
    .line 4867
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4868
    if-eqz p1, :cond_1

    .line 4869
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->setRequestedOrientation(I)V

    .line 4878
    :cond_0
    :goto_0
    return-void

    .line 4871
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    new-instance v1, Lyd;

    invoke-direct {v1, p0}, Lyd;-><init>(Lcom/android/launcher3/Launcher;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected b(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 3

    .prologue
    const v2, 0x7f110243

    .line 4064
    const v0, 0x7f110384

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4065
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 4066
    invoke-direct {p0, v2, p1}, Lcom/android/launcher3/Launcher;->a(ILandroid/graphics/drawable/Drawable$ConstantState;)V

    .line 4067
    invoke-static {v0, v1}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 4068
    return-void
.end method

.method public final b(Ljava/util/HashMap;)V
    .locals 2

    .prologue
    .line 4424
    new-instance v0, Lxu;

    invoke-direct {v0, p0, p1}, Lxu;-><init>(Lcom/android/launcher3/Launcher;Ljava/util/HashMap;)V

    .line 4429
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4434
    :goto_0
    return-void

    .line 4432
    :cond_0
    sget-object v0, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4433
    sget-object v0, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final b(Lyy;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 4442
    new-instance v0, Lxv;

    invoke-direct {v0, p0, p1}, Lxv;-><init>(Lcom/android/launcher3/Launcher;Lyy;)V

    .line 4447
    invoke-direct {p0, v0, v10}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4539
    :goto_0
    return-void

    .line 4451
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    .line 4458
    iget v1, p1, Lyy;->Mq:I

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_3

    iget v1, p1, Lyy;->Mq:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 4461
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    iget-object v1, p1, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-static {p0, v1}, Lzi;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    .line 4462
    if-nez v1, :cond_1

    .line 4468
    invoke-static {p0, p1}, Lzi;->b(Landroid/content/Context;Lwq;)V

    goto :goto_0

    .line 4474
    :cond_1
    new-instance v3, Lacy;

    invoke-direct {v3, v1, v2, v2}, Lacy;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 4475
    iget v4, p1, Lyy;->AY:I

    iput v4, v3, Lacy;->AY:I

    .line 4476
    iget v4, p1, Lyy;->AZ:I

    iput v4, v3, Lacy;->AZ:I

    .line 4477
    iget v4, p1, Lyy;->JB:I

    iput v4, v3, Lacy;->JB:I

    .line 4478
    iget v4, p1, Lyy;->JC:I

    iput v4, v3, Lacy;->JC:I

    .line 4479
    invoke-static {p0, v3}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/Launcher;Lacy;)Landroid/os/Bundle;

    move-result-object v3

    .line 4482
    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v4}, Lyw;->allocateAppWidgetId()I

    move-result v4

    .line 4483
    iget-object v5, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    invoke-virtual {v5, v4, v1, v3}, Lahh;->a(ILandroid/appwidget/AppWidgetProviderInfo;Landroid/os/Bundle;)Z

    move-result v3

    .line 4487
    if-nez v3, :cond_2

    .line 4488
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v0, v4}, Lyw;->deleteAppWidgetId(I)V

    .line 4494
    invoke-static {p0, p1}, Lzi;->b(Landroid/content/Context;Lwq;)V

    goto :goto_0

    .line 4498
    :cond_2
    iput v4, p1, Lyy;->LT:I

    .line 4502
    iget-object v1, v1, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-nez v1, :cond_5

    move v1, v10

    :goto_1
    iput v1, p1, Lyy;->Mq:I

    .line 4506
    invoke-static {p0, p1}, Lzi;->a(Landroid/content/Context;Lwq;)V

    .line 4509
    :cond_3
    iget v1, p1, Lyy;->Mq:I

    if-nez v1, :cond_6

    .line 4510
    iget v2, p1, Lyy;->LT:I

    .line 4511
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    invoke-virtual {v1, v2}, Lahh;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    .line 4516
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v3, p0, v2, v1}, Lyw;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v2

    iput-object v2, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    move-object v11, v1

    .line 4526
    :goto_2
    iget-object v1, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v1, p1}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 4527
    iget-boolean v1, p1, Lyy;->Ms:Z

    if-nez v1, :cond_4

    invoke-virtual {p1, p0}, Lyy;->t(Lcom/android/launcher3/Launcher;)V

    .line 4529
    :cond_4
    iget-object v1, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    iget-wide v2, p1, Lyy;->JA:J

    iget-wide v4, p1, Lyy;->Bd:J

    iget v6, p1, Lyy;->Bb:I

    iget v7, p1, Lyy;->Bc:I

    iget v8, p1, Lyy;->AY:I

    iget v9, p1, Lyy;->AZ:I

    invoke-virtual/range {v0 .. v10}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZ)V

    .line 4531
    iget-object v1, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    invoke-direct {p0, v1, v11}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 4533
    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->requestLayout()V

    goto/16 :goto_0

    .line 4502
    :cond_5
    const/4 v1, 0x4

    goto :goto_1

    .line 4519
    :cond_6
    new-instance v1, Lacz;

    invoke-direct {v1, p0, p1}, Lacz;-><init>(Landroid/content/Context;Lyy;)V

    .line 4520
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    invoke-virtual {v1, v3}, Lacz;->a(Lwi;)V

    .line 4521
    iput-object v1, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    .line 4522
    iget-object v1, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetHostView;->updateAppWidget(Landroid/widget/RemoteViews;)V

    .line 4523
    iget-object v1, p1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v1, p0}, Landroid/appwidget/AppWidgetHostView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v11, v2

    goto :goto_2
.end method

.method public final b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2857
    .line 2858
    iget-boolean v1, p0, Lcom/android/launcher3/Launcher;->JH:Z

    if-eqz v1, :cond_0

    invoke-static {p0, p2}, Ladp;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2859
    const v1, 0x7f0a0086

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2868
    :goto_0
    return v0

    .line 2863
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 2864
    :catch_0
    move-exception v1

    .line 2865
    const v2, 0x7f0a0085

    invoke-static {p0, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2866
    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to launch. tag="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final bi(I)Z
    .locals 1

    .prologue
    .line 4629
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v0, :cond_0

    .line 4630
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Hotseat;->bi(I)Z

    move-result v0

    .line 4632
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bm(I)V
    .locals 2

    .prologue
    .line 4562
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KF:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4563
    return-void
.end method

.method final c(Landroid/content/ComponentName;Lahz;)V
    .locals 3

    .prologue
    const v2, 0x7f0a0085

    const/4 v1, 0x0

    .line 2775
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    .line 2777
    :try_start_0
    invoke-static {p0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v0

    .line 2778
    invoke-static {p0}, Laia;->D(Landroid/content/Context;)Laia;

    .line 2779
    invoke-virtual {v0, p1, p2}, Lahn;->d(Landroid/content/ComponentName;Lahz;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2787
    :goto_0
    return-void

    .line 2781
    :catch_0
    move-exception v0

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2782
    const-string v0, "Launcher"

    const-string v1, "Launcher does not have permission to launch settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2784
    :catch_1
    move-exception v0

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2785
    const-string v0, "Launcher"

    const-string v1, "Unable to launch settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final d(Lwq;)I
    .locals 4

    .prologue
    .line 712
    iget-wide v0, p1, Lwq;->id:J

    long-to-int v2, v0

    .line 713
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JI:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JI:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 718
    :goto_0
    return v0

    .line 716
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    .line 717
    :goto_1
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JI:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 716
    :cond_1
    sget-object v0, Lcom/android/launcher3/Launcher;->JJ:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    const v3, 0xffffff

    if-le v0, v3, :cond_2

    const/4 v0, 0x1

    :cond_2
    sget-object v3, Lcom/android/launcher3/Launcher;->JJ:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_1
.end method

.method public final d(Landroid/content/Intent;)Lwq;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5067
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v3

    .line 5068
    invoke-static {p0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v0

    .line 5069
    invoke-virtual {v0, p1, v3}, Lahn;->c(Landroid/content/Intent;Lahz;)Lahk;

    move-result-object v2

    .line 5071
    if-nez v2, :cond_0

    .line 5074
    :goto_0
    return-object v5

    :cond_0
    new-instance v0, Lrr;

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lrr;-><init>(Landroid/content/Context;Lahk;Lahz;Lwi;Ljava/util/HashMap;)V

    move-object v5, v0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2367
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 2368
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2385
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_1
    :pswitch_0
    :sswitch_0
    return v0

    .line 2372
    :sswitch_1
    const-string v1, "launcher_dump_state"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2373
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BEGIN launcher3 dump state for launcher "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mSavedState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->Kh:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mWorkspaceLoading="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/android/launcher3/Launcher;->Kk:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mRestoring="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/android/launcher3/Launcher;->Km:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mWaitingForResult="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/android/launcher3/Launcher;->Kn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mSavedInstanceState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sFolders.size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v1}, Lzi;->ev()V

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->ev()V

    goto :goto_1

    .line 2378
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 2379
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2368
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch

    .line 2379
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4

    .prologue
    .line 4090
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 4091
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    .line 4092
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 4094
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v3, Lyq;->LW:Lyq;

    if-ne v2, v3, :cond_0

    .line 4095
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-virtual {v2}, Lcom/android/launcher3/AppsCustomizeTabHost;->ey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4099
    :goto_0
    return v0

    .line 4097
    :cond_0
    const v2, 0x7f0a00a2

    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 5126
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 5127
    sget-object v2, Lcom/android/launcher3/Launcher;->KG:Ljava/util/ArrayList;

    monitor-enter v2

    .line 5128
    :try_start_0
    const-string v0, " "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 5129
    const-string v0, "Debug logs: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 5130
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/android/launcher3/Launcher;->KG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 5131
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "  "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/android/launcher3/Launcher;->KG:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 5130
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5133
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public hA()V
    .locals 0

    .prologue
    .line 2190
    return-void
.end method

.method protected hC()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 2352
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Labs;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public hD()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x10000000

    .line 2534
    :try_start_0
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 2536
    invoke-virtual {v0}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 2537
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.WEB_SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2538
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2539
    if-eqz v0, :cond_0

    .line 2540
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2542
    :cond_0
    const/4 v0, 0x0

    const-string v2, "onClickVoiceButton"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2548
    :goto_0
    return-void

    .line 2544
    :catch_0
    move-exception v0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2545
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2546
    const-string v1, "onClickVoiceButton"

    invoke-virtual {p0, v4, v0, v1}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final hE()Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ky:Landroid/view/View$OnTouchListener;

    if-nez v0, :cond_0

    .line 2742
    new-instance v0, Lxf;

    invoke-direct {v0, p0}, Lxf;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ky:Landroid/view/View$OnTouchListener;

    .line 2752
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ky:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method protected hF()V
    .locals 0

    .prologue
    .line 2761
    return-void
.end method

.method protected hG()V
    .locals 0

    .prologue
    .line 2772
    return-void
.end method

.method public final hH()V
    .locals 2

    .prologue
    .line 3009
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 3010
    :goto_0
    if-eqz v0, :cond_1

    .line 3011
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gp()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3012
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gq()V

    .line 3014
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->h(Lcom/android/launcher3/Folder;)V

    .line 3016
    :cond_1
    return-void

    .line 3009
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hI()Z
    .locals 2

    .prologue
    .line 3110
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v1, Lyq;->LW:Lyq;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    sget-object v1, Lyq;->LW:Lyq;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hJ()V
    .locals 3

    .prologue
    .line 3790
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3791
    sget-object v0, Lagv;->Xx:Lagv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Lagv;ZLjava/lang/Runnable;)V

    .line 3792
    sget-object v0, Lyq;->LX:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    .line 3794
    :cond_0
    return-void
.end method

.method public final hK()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3817
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v1, Lyq;->LX:Lyq;

    if-ne v0, v1, :cond_0

    .line 3818
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->er()Lse;

    move-result-object v0

    invoke-direct {p0, v2, v2, v0}, Lcom/android/launcher3/Launcher;->a(ZZLse;)V

    .line 3821
    sget-object v0, Lyq;->LW:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    .line 3824
    :cond_0
    return-void
.end method

.method public hM()Landroid/view/View;
    .locals 4

    .prologue
    .line 3968
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kg:Landroid/view/View;

    if-nez v0, :cond_0

    .line 3969
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f04014a

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kg:Landroid/view/View;

    .line 3970
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kg:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/SearchDropTargetBar;->addView(Landroid/view/View;)V

    .line 3972
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kg:Landroid/view/View;

    return-object v0
.end method

.method protected hN()Z
    .locals 9

    .prologue
    const v8, 0x7f020164

    const v7, 0x7f110382

    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 3976
    const v0, 0x7f110383

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3977
    invoke-virtual {p0, v7}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3978
    const v1, 0x7f110384

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 3979
    const v1, 0x7f110243

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 3981
    const-string v1, "search"

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 3983
    invoke-virtual {v1}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v1

    .line 3984
    if-eqz v1, :cond_2

    .line 3985
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hL()I

    move-result v4

    .line 3986
    sget-object v5, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    const-string v6, "com.android.launcher.toolbar_search_icon"

    invoke-direct {p0, v7, v1, v8, v6}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v6

    aput-object v6, v5, v4

    .line 3989
    sget-object v5, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v5, v5, v4

    if-nez v5, :cond_0

    .line 3990
    sget-object v5, Lcom/android/launcher3/Launcher;->KC:[Landroid/graphics/drawable/Drawable$ConstantState;

    const-string v6, "com.android.launcher.toolbar_icon"

    invoke-direct {p0, v7, v1, v8, v6}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    aput-object v1, v5, v4

    .line 3995
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3996
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3997
    invoke-static {v3, v0}, Lcom/android/launcher3/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 3998
    const/4 v0, 0x1

    .line 4006
    :goto_0
    return v0

    .line 4001
    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 4002
    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 4003
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4004
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 4005
    :cond_6
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->aa(Z)V

    move v0, v2

    .line 4006
    goto :goto_0
.end method

.method public final hO()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 4177
    iget-boolean v1, p0, Lcom/android/launcher3/Launcher;->Kl:Z

    if-eqz v1, :cond_0

    .line 4178
    const-string v1, "Launcher"

    const-string v2, "setLoadOnResume"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4179
    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Ko:Z

    .line 4182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hP()I
    .locals 1

    .prologue
    .line 4190
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    .line 4191
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v0

    .line 4193
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final hQ()V
    .locals 1

    .prologue
    .line 4203
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->T(Z)V

    .line 4208
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4211
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->ly()V

    .line 4212
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kM()V

    .line 4214
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4215
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v0, :cond_0

    .line 4216
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gY()V

    .line 4218
    :cond_0
    return-void
.end method

.method public final hR()V
    .locals 3

    .prologue
    .line 4663
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hN()Z

    move-result v0

    .line 4664
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->Z(Z)Z

    move-result v1

    .line 4665
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v2, :cond_0

    .line 4666
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v2, v0, v1}, Lcom/android/launcher3/SearchDropTargetBar;->c(ZZ)V

    .line 4668
    :cond_0
    return-void
.end method

.method public final hT()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 4861
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4862
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v0, v2

    :goto_0
    :pswitch_0
    const/4 v3, 0x4

    new-array v5, v3, [I

    fill-array-data v5, :array_0

    const/4 v3, 0x0

    if-ne v0, v2, :cond_2

    :goto_1
    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v0

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x4

    aget v0, v5, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->setRequestedOrientation(I)V

    .line 4865
    :cond_0
    return-void

    .line 4862
    :pswitch_1
    if-ne v0, v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :array_0
    .array-data 4
        0x1
        0x0
        0x9
        0x8
    .end array-data
.end method

.method public hU()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 4890
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 4892
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 4893
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4894
    const/4 v0, 0x1

    .line 4900
    :cond_0
    :goto_0
    return v0

    .line 4898
    :catch_0
    move-exception v1

    .line 4899
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public hV()Z
    .locals 1

    .prologue
    .line 4909
    const/4 v0, 0x1

    return v0
.end method

.method protected hW()Z
    .locals 1

    .prologue
    .line 4942
    const/4 v0, 0x0

    return v0
.end method

.method protected hX()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 4949
    const/4 v0, 0x0

    return-object v0
.end method

.method protected hZ()Z
    .locals 1

    .prologue
    .line 4985
    const/4 v0, 0x0

    return v0
.end method

.method public hb()V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method protected hc()Z
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x0

    return v0
.end method

.method protected hd()V
    .locals 0

    .prologue
    .line 522
    return-void
.end method

.method protected final he()V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kT()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 534
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hc()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kN()V

    .line 537
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hd()V

    goto :goto_0

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kO()V

    goto :goto_0
.end method

.method public final hh()Z
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v0}, Lzi;->iA()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hi()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1097
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher3/SearchDropTargetBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1098
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hM()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1099
    return-void
.end method

.method protected hj()Z
    .locals 1

    .prologue
    .line 1117
    const/4 v0, 0x0

    return v0
.end method

.method public final hl()Landroid/view/View;
    .locals 1

    .prologue
    .line 1394
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    return-object v0
.end method

.method public final hn()Lcom/android/launcher3/DragLayer;
    .locals 1

    .prologue
    .line 1828
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    return-object v0
.end method

.method public final ho()Lcom/android/launcher3/Workspace;
    .locals 1

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    return-object v0
.end method

.method public final hp()Lcom/android/launcher3/Hotseat;
    .locals 1

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    return-object v0
.end method

.method public final hq()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1840
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kb:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final hr()Lyw;
    .locals 1

    .prologue
    .line 1848
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    return-object v0
.end method

.method public final hs()Lzi;
    .locals 1

    .prologue
    .line 1852
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    return-object v0
.end method

.method public final ht()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 1856
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final hu()V
    .locals 1

    .prologue
    .line 1860
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->closeAllPanels()V

    .line 1863
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->U(Z)V

    .line 1864
    return-void
.end method

.method protected hv()Z
    .locals 1

    .prologue
    .line 1931
    const/4 v0, 0x1

    return v0
.end method

.method public hw()V
    .locals 0

    .prologue
    .line 1939
    return-void
.end method

.method public final hx()Lty;
    .locals 1

    .prologue
    .line 2042
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    return-object v0
.end method

.method public final hy()Z
    .locals 1

    .prologue
    .line 2167
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kk:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kn:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hz()Z
    .locals 1

    .prologue
    .line 2171
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kk:Z

    return v0
.end method

.method public i(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 5103
    return-void
.end method

.method public final i(Lcom/android/launcher3/FolderIcon;)V
    .locals 10

    .prologue
    const/high16 v5, 0x3fc00000    # 1.5f

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2985
    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v2

    .line 2986
    iget-object v0, v2, Lcom/android/launcher3/Folder;->GM:Lvy;

    .line 2988
    iput-boolean v9, v0, Lvy;->Iz:Z

    .line 2992
    invoke-virtual {v2}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2993
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/DragLayer;->addView(Landroid/view/View;)V

    .line 2994
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    invoke-virtual {v0, v2}, Lty;->b(Luo;)V

    .line 2999
    :goto_0
    invoke-virtual {v2}, Lcom/android/launcher3/Folder;->animateOpen()V

    .line 3000
    if-eqz p1, :cond_2

    const-string v0, "alpha"

    new-array v1, v9, [F

    const/4 v3, 0x0

    aput v3, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const-string v0, "scaleX"

    new-array v1, v9, [F

    aput v5, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    const-string v0, "scaleY"

    new-array v1, v9, [F

    aput v5, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    iget-wide v0, v0, Lvy;->JA:J

    const-wide/16 v6, -0x65

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {p1}, Lcom/android/launcher3/FolderIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout$LayoutParams;

    iget v6, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v1, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    invoke-virtual {v0, v6, v1}, Lcom/android/launcher3/CellLayout;->B(II)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->h(Lcom/android/launcher3/FolderIcon;)V

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/android/launcher3/FolderIcon;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KJ:Landroid/widget/ImageView;

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object v3, v1, v8

    aput-object v4, v1, v9

    const/4 v3, 0x2

    aput-object v5, v1, v3

    invoke-static {v0, v1}, Lyr;->a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-static {}, Ladp;->km()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Labx;

    const/16 v3, 0x64

    invoke-direct {v1, v3, v8}, Labx;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c0022

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 3004
    :cond_2
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Folder;->sendAccessibilityEvent(I)V

    .line 3005
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Lcom/android/launcher3/DragLayer;->sendAccessibilityEvent(I)V

    .line 3006
    return-void

    .line 2996
    :cond_3
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Opening folder ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") which already has a parent ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public final i(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 4222
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->j(Ljava/util/ArrayList;)V

    .line 4225
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 4226
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kQ()Z

    .line 4231
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hc()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4232
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kN()V

    .line 4233
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hd()V

    .line 4235
    :cond_1
    return-void
.end method

.method protected ia()Landroid/view/View;
    .locals 1

    .prologue
    .line 4992
    const/4 v0, 0x0

    return-object v0
.end method

.method public final ib()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5013
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "launcher.intro_screen_dismissed"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 5014
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hY()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5017
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    new-instance v1, Lye;

    invoke-direct {v1, p0}, Lye;-><init>(Lcom/android/launcher3/Launcher;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher3/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5028
    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/launcher3/Launcher;->W(Z)V

    .line 5029
    return-void

    .line 5025
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fK()V

    .line 5026
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->ic()V

    goto :goto_0
.end method

.method public final id()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 5052
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->setAlpha(F)V

    .line 5053
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Hotseat;->setAlpha(F)V

    .line 5054
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JR:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JR:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 5055
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/SearchDropTargetBar;->an(Z)V

    .line 5056
    :cond_3
    return-void
.end method

.method public k(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 4676
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4677
    sget-object v0, Lcom/android/launcher3/Launcher;->KI:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 4682
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher3/Launcher;->KI:Ljava/util/ArrayList;

    .line 4684
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_1

    .line 4685
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {p0}, Lzi;->t(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Ljava/util/ArrayList;)V

    .line 4695
    :cond_1
    :goto_0
    return-void

    .line 4689
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_1

    .line 4690
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->b(Ljava/util/ArrayList;)V

    .line 4691
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {p0}, Lzi;->t(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final l(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 4703
    new-instance v0, Lxy;

    invoke-direct {v0, p0, p1}, Lxy;-><init>(Lcom/android/launcher3/Launcher;Ljava/util/ArrayList;)V

    .line 4708
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4720
    :cond_0
    :goto_0
    return-void

    .line 4712
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_2

    .line 4713
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Workspace;->s(Ljava/util/ArrayList;)V

    .line 4716
    :cond_2
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_0

    .line 4718
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->g(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final m(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 4726
    new-instance v0, Lxz;

    invoke-direct {v0, p0, p1}, Lxz;-><init>(Lcom/android/launcher3/Launcher;Ljava/util/ArrayList;)V

    .line 4731
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4738
    :cond_0
    :goto_0
    return-void

    .line 4735
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    .line 4736
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Workspace;->s(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final n(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 4747
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    .line 4748
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Workspace;->n(Ljava/util/ArrayList;)V

    .line 4750
    :cond_0
    return-void
.end method

.method public final o(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 4812
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KW:Ljava/lang/Runnable;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->b(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4813
    iput-object p1, p0, Lcom/android/launcher3/Launcher;->KV:Ljava/util/ArrayList;

    .line 4821
    :cond_0
    :goto_0
    return-void

    .line 4818
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_0

    .line 4819
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const/16 v5, 0x1f4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 756
    invoke-direct {p0, v2}, Lcom/android/launcher3/Launcher;->U(Z)V

    .line 757
    iget v1, p0, Lcom/android/launcher3/Launcher;->JX:I

    .line 758
    iput v4, p0, Lcom/android/launcher3/Launcher;->JX:I

    .line 760
    new-instance v6, Lya;

    invoke-direct {v6, p0, p2}, Lya;-><init>(Lcom/android/launcher3/Launcher;I)V

    .line 768
    const/16 v0, 0xb

    if-ne p1, v0, :cond_3

    .line 769
    if-eqz p3, :cond_1

    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 771
    :goto_0
    if-nez p2, :cond_2

    .line 772
    invoke-direct {p0, v2, v1}, Lcom/android/launcher3/Launcher;->L(II)V

    .line 773
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v3, v6, v5, v2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    .line 886
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v4

    .line 769
    goto :goto_0

    .line 775
    :cond_2
    if-ne p2, v4, :cond_0

    .line 776
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/Launcher;->a(ILwq;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;I)V

    goto :goto_1

    .line 780
    :cond_3
    const/16 v0, 0xa

    if-ne p1, v0, :cond_4

    .line 781
    if-ne p2, v4, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->at(Z)V

    goto :goto_1

    .line 787
    :cond_4
    const/16 v0, 0x9

    if-eq p1, v0, :cond_5

    const/4 v0, 0x5

    if-ne p1, v0, :cond_8

    :cond_5
    move v0, v3

    .line 790
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v7

    .line 792
    if-eqz v0, :cond_e

    .line 794
    if-eqz p3, :cond_9

    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 796
    :goto_3
    if-gez v0, :cond_6

    move v0, v1

    .line 803
    :cond_6
    if-ltz v0, :cond_7

    if-nez p2, :cond_b

    .line 804
    :cond_7
    const-string v1, "Launcher"

    const-string v4, "Error: appWidgetId (EXTRA_APPWIDGET_ID) was not returned from the widget configuration activity."

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    invoke-direct {p0, v2, v0}, Lcom/android/launcher3/Launcher;->L(II)V

    .line 808
    new-instance v0, Lyf;

    invoke-direct {v0, p0}, Lyf;-><init>(Lcom/android/launcher3/Launcher;)V

    .line 814
    if-eqz v7, :cond_a

    .line 817
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/launcher3/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_8
    move v0, v2

    .line 787
    goto :goto_2

    :cond_9
    move v0, v4

    .line 794
    goto :goto_3

    .line 819
    :cond_a
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v3, v0, v5, v2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    goto :goto_1

    .line 823
    :cond_b
    if-nez v7, :cond_d

    .line 824
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v6, v1, Lwq;->JA:J

    const-wide/16 v8, -0x64

    cmp-long v1, v6, v8

    if-nez v1, :cond_c

    .line 827
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v6, v4, Lwq;->Bd:J

    invoke-direct {p0, v6, v7}, Lcom/android/launcher3/Launcher;->f(J)J

    move-result-wide v6

    iput-wide v6, v1, Lwq;->Bd:J

    .line 830
    :cond_c
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v6, v4, Lwq;->Bd:J

    invoke-virtual {v1, v6, v7}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v1

    .line 832
    invoke-virtual {v1, v3}, Lcom/android/launcher3/CellLayout;->G(Z)V

    .line 833
    new-instance v4, Lyg;

    invoke-direct {v4, p0, p2, v0, v1}, Lyg;-><init>(Lcom/android/launcher3/Launcher;IILcom/android/launcher3/CellLayout;)V

    .line 840
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v3, v4, v5, v2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    goto/16 :goto_1

    .line 843
    :cond_d
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    invoke-static {p1, p3, v0, v1}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/Intent;ILwq;)Lyo;

    move-result-object v0

    .line 845
    sput-object v0, Lcom/android/launcher3/Launcher;->KP:Lyo;

    goto/16 :goto_1

    .line 851
    :cond_e
    const/16 v0, 0xc

    if-ne p1, v0, :cond_10

    .line 852
    if-ne p2, v4, :cond_0

    .line 854
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    invoke-static {p1, p3, v1, v0}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/Intent;ILwq;)Lyo;

    move-result-object v0

    .line 856
    if-eqz v7, :cond_f

    .line 857
    sput-object v0, Lcom/android/launcher3/Launcher;->KP:Lyo;

    goto/16 :goto_1

    .line 859
    :cond_f
    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->a(Lyo;)J

    goto/16 :goto_1

    .line 871
    :cond_10
    if-ne p2, v4, :cond_13

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v0, v0, Lwq;->JA:J

    const-wide/16 v8, -0x1

    cmp-long v0, v0, v8

    if-eqz v0, :cond_13

    .line 872
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    invoke-static {p1, p3, v4, v0}, Lcom/android/launcher3/Launcher;->a(ILandroid/content/Intent;ILwq;)Lyo;

    move-result-object v0

    .line 874
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 875
    sput-object v0, Lcom/android/launcher3/Launcher;->KP:Lyo;

    .line 885
    :cond_11
    :goto_4
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fM()V

    goto/16 :goto_1

    .line 877
    :cond_12
    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->a(Lyo;)J

    .line 878
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v3, v6, v5, v2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    goto :goto_4

    .line 881
    :cond_13
    if-nez p2, :cond_11

    .line 882
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v3, v6, v5, v2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    goto :goto_4
.end method

.method public onAttachedToWindow()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1638
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 1641
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1642
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1643
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1645
    const-string v1, "android.intent.action.MANAGED_PROFILE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1646
    const-string v1, "android.intent.action.MANAGED_PROFILE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1651
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1652
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Luv;->O(Landroid/view/View;)V

    .line 1653
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    or-int/lit16 v1, v1, 0x700

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0xc000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    const-class v0, Landroid/view/WindowManager$LayoutParams;

    const-string v1, "FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    const-class v0, Landroid/view/Window;

    const-string v1, "setStatusBarColor"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Landroid/view/Window;

    const-string v2, "setNavigationBarColor"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1654
    :cond_0
    :goto_0
    iput-boolean v6, p0, Lcom/android/launcher3/Launcher;->Kv:Z

    .line 1655
    iput-boolean v6, p0, Lcom/android/launcher3/Launcher;->Kt:Z

    .line 1656
    return-void

    .line 1653
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "Launcher"

    const-string v1, "NoSuchFieldException while setting up transparent bars"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v0, "Launcher"

    const-string v1, "NoSuchMethodException while setting up transparent bars"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "Launcher"

    const-string v1, "IllegalAccessException while setting up transparent bars"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_3
    move-exception v0

    const-string v0, "Launcher"

    const-string v1, "IllegalArgumentException while setting up transparent bars"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_4
    move-exception v0

    const-string v0, "Launcher"

    const-string v1, "InvocationTargetException while setting up transparent bars"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2390
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2391
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->er()Lse;

    move-result-object v0

    sget-object v1, Lse;->yR:Lse;

    if-ne v0, v1, :cond_0

    .line 2393
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2412
    :goto_0
    return-void

    .line 2395
    :cond_0
    invoke-direct {p0, v2}, Lcom/android/launcher3/Launcher;->Y(Z)V

    goto :goto_0

    .line 2397
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2398
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->at(Z)V

    goto :goto_0

    .line 2399
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2400
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 2401
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gp()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2402
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gq()V

    goto :goto_0

    .line 2404
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    goto :goto_0

    .line 2407
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lh()V

    .line 2410
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lb()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 2431
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2468
    :cond_0
    :goto_0
    return-void

    .line 2435
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2439
    instance-of v0, p1, Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_2

    .line 2440
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2441
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->at(Z)V

    goto :goto_0

    .line 2446
    :cond_2
    instance-of v0, p1, Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_3

    .line 2447
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2448
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, p1}, Lcom/android/launcher3/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/Workspace;->l(IZ)V

    .line 2452
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2453
    instance-of v1, v0, Ladh;

    if-eqz v1, :cond_4

    .line 2454
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->R(Landroid/view/View;)V

    goto :goto_0

    .line 2455
    :cond_4
    instance-of v1, v0, Lvy;

    if-eqz v1, :cond_5

    .line 2456
    instance-of v0, p1, Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_0

    .line 2457
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->T(Landroid/view/View;)V

    goto :goto_0

    .line 2459
    :cond_5
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    if-ne p1, v1, :cond_6

    .line 2460
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->onClickAllAppsButton(Landroid/view/View;)V

    goto :goto_0

    .line 2461
    :cond_6
    instance-of v1, v0, Lrr;

    if-eqz v1, :cond_7

    .line 2462
    invoke-direct {p0, p1}, Lcom/android/launcher3/Launcher;->S(Landroid/view/View;)V

    goto :goto_0

    .line 2463
    :cond_7
    instance-of v0, v0, Lyy;

    if-eqz v0, :cond_0

    .line 2464
    instance-of v0, p1, Lacz;

    if-eqz v0, :cond_0

    .line 2465
    check-cast p1, Lacz;

    invoke-virtual {p1}, Lacz;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lyy;

    invoke-virtual {p1}, Lacz;->jW()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, v2, Lyy;->LT:I

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    invoke-virtual {v1, v0}, Lahh;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-object v1, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    invoke-virtual {v3, v2}, Lwq;->c(Lwq;)V

    iput v0, p0, Lcom/android/launcher3/Launcher;->JX:I

    invoke-static {p0}, Lahh;->A(Landroid/content/Context;)Lahh;

    move-result-object v0

    iget v2, v2, Lyy;->LT:I

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    const/16 v5, 0xc

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lahh;->a(Landroid/appwidget/AppWidgetProviderInfo;ILandroid/app/Activity;Landroid/appwidget/AppWidgetHost;I)V

    goto/16 :goto_0

    :cond_8
    iget v0, v2, Lyy;->Mr:I

    if-gez v0, :cond_9

    iget-object v0, v2, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lxb;

    invoke-direct {v1, p0, p1, v0, v2}, Lxb;-><init>(Lcom/android/launcher3/Launcher;Lacz;Ljava/lang/String;Lyy;)V

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, v2, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lzi;->v(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/launcher3/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public onClickAllAppsButton(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2558
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2559
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2563
    :goto_0
    return-void

    .line 2561
    :cond_0
    sget-object v0, Lse;->yR:Lse;

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/android/launcher3/Launcher;->a(ZLse;Z)V

    goto :goto_0
.end method

.method public onClickSearchButton(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2516
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 2518
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->onSearchRequested()Z

    .line 2519
    return-void
.end method

.method public onClickVoiceButton(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2527
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 2529
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hD()V

    .line 2530
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 410
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 412
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lyu;->n(Landroid/content/Context;)V

    .line 413
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v9

    .line 414
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/launcher3/LauncherProvider;->a(Labi;)V

    .line 416
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 417
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 418
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 419
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 420
    invoke-virtual {v3, v0, v1}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 421
    invoke-virtual {v3, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 422
    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    .line 423
    invoke-virtual {v3, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 426
    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget v5, v2, Landroid/graphics/Point;->x:I

    iget v6, v2, Landroid/graphics/Point;->y:I

    iget v7, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v0, v9, Lyu;->Mk:Lur;

    if-nez v0, :cond_0

    new-instance v0, Lur;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lur;-><init>(Landroid/content/Context;Landroid/content/res/Resources;IIIIII)V

    iput-object v0, v9, Lyu;->Mk:Lur;

    iget-object v0, v9, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    iget-object v1, v0, Ltu;->Ek:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v9, v0}, Ltw;->a(Ltu;)V

    :cond_0
    iget-object v0, v9, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object v3, p0

    invoke-virtual/range {v2 .. v8}, Ltu;->a(Landroid/content/Context;Landroid/content/res/Resources;IIII)V

    .line 433
    const-string v0, "com.android.launcher3.prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    .line 435
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->isSafeMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->JH:Z

    .line 436
    invoke-virtual {v9, p0}, Lyu;->s(Lcom/android/launcher3/Launcher;)Lzi;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    .line 437
    iget-object v0, v9, Lyu;->xn:Lwi;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    .line 438
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xn:Lwi;

    invoke-virtual {v0, v2}, Lwi;->b(Ltu;)V

    .line 439
    new-instance v0, Lty;

    invoke-direct {v0, p0}, Lty;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    .line 440
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->GS:Landroid/view/LayoutInflater;

    .line 442
    new-instance v0, Ladl;

    invoke-direct {v0, p0}, Ladl;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KR:Ladl;

    .line 444
    invoke-static {p0}, Lahh;->A(Landroid/content/Context;)Lahh;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JT:Lahh;

    .line 446
    new-instance v0, Lyw;

    const/16 v1, 0x400

    invoke-direct {v0, p0, v1}, Lyw;-><init>(Lcom/android/launcher3/Launcher;I)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    .line 447
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v0}, Lyw;->startListening()V

    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kl:Z

    .line 459
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hg()V

    .line 460
    const v0, 0x7f0400b1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->setContentView(I)V

    .line 462
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    const v0, 0x7f11023b

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JQ:Landroid/view/View;

    const v0, 0x7f11023d

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/FocusIndicatorView;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KS:Lcom/android/launcher3/FocusIndicatorView;

    const v0, 0x7f11023c

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    const v3, 0x7f11023e

    invoke-virtual {v0, v3}, Lcom/android/launcher3/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/Workspace;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/Workspace;->a(Lacm;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    const v3, 0x7f110104

    invoke-virtual {v0, v3}, Lcom/android/launcher3/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JR:Landroid/view/View;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JQ:Landroid/view/View;

    const/16 v3, 0x600

    invoke-virtual {v0, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f02031e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->KE:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0, p0, v1}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/Launcher;Lty;)V

    const v0, 0x7f11023f

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/Hotseat;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/Hotseat;->b(Lcom/android/launcher3/Launcher;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/Hotseat;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    const v0, 0x7f110240

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kb:Landroid/view/ViewGroup;

    const v0, 0x7f1102a0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v0, Lyj;

    invoke-direct {v0, p0}, Lyj;-><init>(Lcom/android/launcher3/Launcher;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hE()Landroid/view/View$OnTouchListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f11029f

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v4, Lws;

    invoke-direct {v4, p0}, Lws;-><init>(Lcom/android/launcher3/Launcher;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hE()Landroid/view/View$OnTouchListener;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f1102a1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hj()Z

    move-result v4

    if-eqz v4, :cond_16

    new-instance v3, Lwt;

    invoke-direct {v3, p0}, Lwt;-><init>(Lcom/android/launcher3/Launcher;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hE()Landroid/view/View$OnTouchListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kb:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/android/launcher3/Workspace;->setHapticFeedbackEnabled(Z)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/Workspace;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->e(Lty;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v0}, Lty;->a(Ltz;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    const v3, 0x7f110241

    invoke-virtual {v0, v3}, Lcom/android/launcher3/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/SearchDropTargetBar;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    const v0, 0x7f110244

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/AppsCustomizeTabHost;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    const v3, 0x7f1100a1

    invoke-virtual {v0, v3}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/AppsCustomizePagedView;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/Launcher;Lty;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v0}, Lty;->a(Luh;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v1, v0}, Lty;->M(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v0}, Lty;->L(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v0}, Lty;->b(Luo;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v0, p0, v1}, Lcom/android/launcher3/SearchDropTargetBar;->a(Lcom/android/launcher3/Launcher;Lty;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Laew;

    invoke-direct {v0, p0}, Laew;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JS:Landroid/view/View;

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JS:Landroid/view/View;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JQ:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JS:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    const/16 v6, 0x50

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const-string v0, "com.android.launcher3.prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "debug.show_mem"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JS:Landroid/view/View;

    if-eqz v0, :cond_17

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 463
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v2}, Ltu;->fC()Z

    move-result v5

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v5, :cond_18

    const/16 v1, 0x33

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v1, v2, Ltu;->Ed:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/4 v1, 0x0

    iget v6, v2, Ltu;->Dv:I

    mul-int/lit8 v6, v6, 0x2

    const/4 v7, 0x0

    iget v8, v2, Ltu;->Dv:I

    mul-int/lit8 v8, v8, 0x2

    invoke-virtual {v3, v1, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    const v1, 0x7f1103ad

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f110242

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    if-nez v5, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const v1, 0x800035

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v1, v2, Ltu;->Dx:I

    iget v3, v2, Ltu;->Eb:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    iget v3, v2, Ltu;->DI:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v1, v2, Ltu;->Ed:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    :cond_4
    const v0, 0x7f11023e

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/PagedView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x11

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-boolean v3, v2, Ltu;->Dp:Z

    if-eqz v3, :cond_19

    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v2, v3}, Ltu;->ba(I)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PagedView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v1, v6, Landroid/graphics/Rect;->left:I

    iget v7, v6, Landroid/graphics/Rect;->top:I

    iget v8, v6, Landroid/graphics/Rect;->right:I

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v7, v8, v9}, Lcom/android/launcher3/PagedView;->setPadding(IIII)V

    if-nez v3, :cond_5

    iget-boolean v1, v2, Ltu;->Dt:Z

    if-nez v1, :cond_6

    :cond_5
    iget-boolean v1, v2, Ltu;->Dr:Z

    if-eqz v1, :cond_1a

    :cond_6
    iget v1, v2, Ltu;->DB:I

    :goto_4
    invoke-virtual {v0, v1}, Lcom/android/launcher3/PagedView;->bw(I)V

    const v0, 0x7f11023f

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v5, :cond_1b

    const v3, 0x800005

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v3, v2, Ltu;->DX:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v3, -0x1

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const v3, 0x7f11020c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v6, 0x0

    iget v7, v2, Ltu;->Dv:I

    mul-int/lit8 v7, v7, 0x2

    const/4 v8, 0x0

    iget v9, v2, Ltu;->Dv:I

    mul-int/lit8 v9, v9, 0x2

    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/view/View;->setPadding(IIII)V

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f110104

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    if-eqz v5, :cond_1d

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    :goto_6
    const v0, 0x7f110244

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/AppsCustomizeTabHost;

    if-eqz v0, :cond_b

    iget v1, v2, Ltu;->Ef:I

    int-to-float v1, v1

    const/high16 v3, 0x3f800000    # 1.0f

    iget v5, v2, Ltu;->DN:I

    int-to-float v5, v5

    sget v6, Lur;->Gm:F

    div-float/2addr v5, v6

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float/2addr v1, v3

    float-to-int v5, v1

    const v1, 0x7f1100a2

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_8
    const v1, 0x7f1100a1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/AppsCustomizePagedView;

    const v3, 0x7f11009f

    invoke-virtual {v0, v3}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    const v6, 0x7f1100a0

    invoke-virtual {v0, v6}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    if-eqz v1, :cond_b

    iget v7, v2, Ltu;->Dz:I

    iget v8, v2, Ltu;->DO:I

    iget v9, v2, Ltu;->Ea:I

    mul-int/2addr v8, v9

    sub-int/2addr v7, v8

    iget v8, v2, Ltu;->Ea:I

    add-int/lit8 v8, v8, 0x1

    mul-int/lit8 v8, v8, 0x2

    div-int/2addr v7, v8

    iget v8, v2, Ltu;->DA:I

    iget v9, v2, Ltu;->DP:I

    iget v10, v2, Ltu;->DZ:I

    mul-int/2addr v9, v10

    sub-int/2addr v8, v9

    iget v9, v2, Ltu;->DZ:I

    add-int/lit8 v9, v9, 0x1

    mul-int/lit8 v9, v9, 0x2

    div-int/2addr v8, v9

    add-int v9, v7, v8

    int-to-float v9, v9

    const/high16 v10, 0x3f400000    # 0.75f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-static {v7, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int v9, v7, v8

    int-to-float v9, v9

    const/high16 v10, 0x3f400000    # 0.75f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget v9, v2, Ltu;->Ea:I

    iget v10, v2, Ltu;->DO:I

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v10

    mul-int/2addr v7, v9

    iget v9, v2, Ltu;->Dz:I

    sub-int v7, v9, v7

    div-int/lit8 v7, v7, 0x2

    iget-boolean v9, v2, Ltu;->Dq:Z

    if-nez v9, :cond_9

    iget-boolean v9, v2, Ltu;->Dp:Z

    if-eqz v9, :cond_a

    :cond_9
    iget v9, v2, Ltu;->DO:I

    div-int/lit8 v9, v9, 0x4

    if-le v7, v9, :cond_a

    iput v7, v6, Landroid/graphics/Rect;->right:I

    iput v7, v6, Landroid/graphics/Rect;->left:I

    :cond_a
    const/4 v7, 0x0

    sub-int v8, v5, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v5}, Lcom/android/launcher3/AppsCustomizePagedView;->aN(I)V

    const v5, 0x7f02029c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0d007b

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v4, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v0

    iput v4, v6, Landroid/graphics/Rect;->left:I

    iget v4, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v4

    iput v0, v6, Landroid/graphics/Rect;->right:I

    iget v0, v6, Landroid/graphics/Rect;->left:I

    iget v4, v6, Landroid/graphics/Rect;->top:I

    iget v5, v6, Landroid/graphics/Rect;->right:I

    iget v7, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v0, v4, v5, v7}, Lcom/android/launcher3/AppsCustomizePagedView;->setPadding(IIII)V

    iget v0, v6, Landroid/graphics/Rect;->left:I

    iget v1, v6, Landroid/graphics/Rect;->top:I

    iget v4, v6, Landroid/graphics/Rect;->right:I

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    :cond_b
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kb:Landroid/view/ViewGroup;

    if-eqz v1, :cond_c

    invoke-virtual {v2}, Ltu;->fB()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x51

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v4, v2, Ltu;->Dz:I

    invoke-static {v1}, Ltu;->b(Landroid/view/ViewGroup;)I

    move-result v5

    iget v6, v2, Ltu;->DE:I

    mul-int/2addr v6, v5

    add-int/lit8 v5, v5, -0x1

    iget v2, v2, Ltu;->DF:I

    mul-int/2addr v2, v5

    add-int/2addr v2, v6

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 465
    :cond_c
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/launcher3/LauncherProvider;->Og:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JO:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 467
    iput-object p1, p0, Lcom/android/launcher3/Launcher;->Kh:Landroid/os/Bundle;

    .line 470
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kh:Landroid/os/Bundle;

    if-eqz v1, :cond_13

    const-string v0, "launcher.state"

    sget-object v2, Lyq;->LV:Lyq;

    invoke-virtual {v2}, Lyq;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->bl(I)Lyq;

    move-result-object v0

    sget-object v2, Lyq;->LW:Lyq;

    if-ne v0, v2, :cond_d

    sget-object v0, Lyq;->LW:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    :cond_d
    const-string v0, "launcher.current_screen"

    const/16 v2, -0x3e9

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/16 v2, -0x3e9

    if-eq v0, v2, :cond_e

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Workspace;->bv(I)V

    :cond_e
    const-string v0, "launcher.add_container"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v0, "launcher.add_screen"

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v0, v2, v6

    if-eqz v0, :cond_f

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-lez v0, :cond_f

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide v2, v0, Lwq;->JA:J

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iput-wide v4, v0, Lwq;->Bd:J

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const-string v2, "launcher.add_cell_x"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lwq;->Bb:I

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const-string v2, "launcher.add_cell_y"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lwq;->Bc:I

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const-string v2, "launcher.add_span_x"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lwq;->AY:I

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    const-string v2, "launcher.add_span_y"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lwq;->AZ:I

    const-string v0, "launcher.add_widget_info"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    const-string v0, "launcher.add_widget_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Launcher;->JX:I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->U(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Km:Z

    :cond_f
    const-string v0, "launcher.rename_folder"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "launcher.rename_folder_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    sget-object v0, Lcom/android/launcher3/Launcher;->Kx:Ljava/util/HashMap;

    invoke-static {p0, v0, v2, v3}, Lzi;->a(Landroid/content/Context;Ljava/util/HashMap;J)Lvy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JZ:Lvy;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Km:Z

    :cond_10
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    if-eqz v0, :cond_12

    const-string v0, "apps_customize_currentTab"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->q(Ljava/lang/String;)Lse;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->b(Lse;)V

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v2}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->bC(I)V

    :cond_11
    const-string v0, "apps_customize_currentIndex"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v2, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->aO(I)V

    :cond_12
    const-string v0, "launcher.view_ids"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->JI:Ljava/util/HashMap;

    .line 476
    :cond_13
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Km:Z

    if-nez v0, :cond_14

    .line 484
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher3/Workspace;->jk()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lzi;->a(ZI)V

    .line 489
    :cond_14
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    .line 490
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 492
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 493
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JN:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 495
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hf()V

    .line 498
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->ad(Z)V

    .line 500
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hZ()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    const-string v1, "launcher.intro_screen_dismissed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_1f

    .line 501
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->ia()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Launcher;->W(Z)V

    if-eqz v0, :cond_15

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/DragLayer;->N(Landroid/view/View;)V

    .line 506
    :cond_15
    :goto_8
    return-void

    .line 462
    :cond_16
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const v4, 0x800035

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_0

    :cond_17
    const/16 v0, 0x8

    goto/16 :goto_1

    .line 463
    :cond_18
    const/16 v1, 0x31

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget v1, v2, Ltu;->Eb:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v1, v2, Ltu;->Ed:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v1, v2, Ltu;->Dv:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v2}, Ltu;->fz()I

    move-result v6

    iget v7, v2, Ltu;->Dv:I

    mul-int/lit8 v7, v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v3, v1, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_2

    :cond_19
    const/4 v3, 0x1

    goto/16 :goto_3

    :cond_1a
    iget v1, v2, Ltu;->DB:I

    invoke-virtual {v2}, Ltu;->fA()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto/16 :goto_4

    :cond_1b
    iget-boolean v3, v2, Ltu;->Dq:Z

    if-eqz v3, :cond_1c

    const/16 v3, 0x50

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/4 v3, -0x1

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v3, v2, Ltu;->DX:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v3, v2, Ltu;->Dv:I

    iget v7, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v7

    const/4 v7, 0x0

    iget v8, v2, Ltu;->Dv:I

    iget v6, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v8

    iget v8, v2, Ltu;->Dv:I

    mul-int/lit8 v8, v8, 0x2

    invoke-virtual {v1, v3, v7, v6, v8}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_5

    :cond_1c
    const/16 v3, 0x50

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/4 v3, -0x1

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v3, v2, Ltu;->DX:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const v3, 0x7f11020c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget v6, v2, Ltu;->Dv:I

    mul-int/lit8 v6, v6, 0x2

    const/4 v7, 0x0

    iget v8, v2, Ltu;->Dv:I

    mul-int/lit8 v8, v8, 0x2

    const/4 v9, 0x0

    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_5

    :cond_1d
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x51

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/4 v3, -0x2

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v3, -0x2

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v3, v2, Ltu;->DX:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_6

    .line 500
    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 503
    :cond_1f
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hY()Z

    .line 504
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->ic()V

    goto/16 :goto_8
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1994
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1997
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1998
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1999
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2002
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 2006
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v1, p0}, Lzi;->b(Laaf;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2007
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v1}, Lzi;->iz()V

    .line 2008
    invoke-virtual {v0, v3}, Lyu;->s(Lcom/android/launcher3/Launcher;)Lzi;

    .line 2012
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    invoke-virtual {v0}, Lyw;->stopListening()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2016
    :goto_0
    iput-object v3, p0, Lcom/android/launcher3/Launcher;->JU:Lyw;

    .line 2018
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KB:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2020
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/method/TextKeyListener;->release()V

    .line 2024
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    if-eqz v0, :cond_1

    .line 2025
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v0}, Lzi;->it()V

    .line 2028
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JO:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2029
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JN:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2031
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fL()V

    .line 2032
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2033
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kM()V

    .line 2034
    iput-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    .line 2035
    iput-object v3, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    .line 2037
    invoke-static {p0}, Laht;->C(Landroid/content/Context;)Laht;

    move-result-object v0

    invoke-virtual {v0}, Laht;->onStop()V

    .line 2038
    invoke-static {}, Lyr;->ih()V

    .line 2039
    return-void

    .line 2013
    :catch_0
    move-exception v0

    .line 2014
    const-string v1, "Launcher"

    const-string v2, "problem while stopping AppWidgetHost during Launcher destruction"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1701
    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    .line 1702
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Kt:Z

    .line 1704
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kv:Z

    if-eqz v0, :cond_0

    .line 1705
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1706
    iput-boolean v1, p0, Lcom/android/launcher3/Launcher;->Kv:Z

    .line 1708
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    .line 1709
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1162
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v0

    .line 1163
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 1164
    if-lez v0, :cond_0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v1

    .line 1165
    :goto_0
    if-nez v2, :cond_2

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    .line 1166
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v0

    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v3, v4, p1, p2}, Landroid/text/method/TextKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1168
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1175
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->onSearchRequested()Z

    move-result v0

    .line 1184
    :goto_2
    return v0

    :cond_0
    move v4, v3

    .line 1164
    goto :goto_0

    :cond_1
    move v0, v3

    .line 1165
    goto :goto_1

    .line 1180
    :cond_2
    const/16 v0, 0x52

    if-ne p1, v0, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1181
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1184
    goto :goto_2
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3034
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hh()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3086
    :cond_0
    :goto_0
    return v2

    .line 3035
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hy()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3036
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v4, Lyq;->LV:Lyq;

    if-ne v1, v4, :cond_0

    .line 3038
    instance-of v1, p1, Lcom/android/launcher3/Workspace;

    if-eqz v1, :cond_2

    .line 3039
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3040
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3041
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher3/Workspace;->performHapticFeedback(II)Z

    move v2, v3

    .line 3043
    goto :goto_0

    .line 3054
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lwq;

    if-eqz v1, :cond_9

    .line 3055
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 3056
    new-instance v1, Lsy;

    invoke-direct {v1, p1, v0}, Lsy;-><init>(Landroid/view/View;Lwq;)V

    .line 3057
    iget-object v0, v1, Lsy;->Ba:Landroid/view/View;

    .line 3058
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hB()V

    .line 3063
    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v5

    .line 3064
    if-nez v5, :cond_3

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->jG()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_3
    move v4, v3

    .line 3065
    :goto_2
    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    invoke-virtual {v4}, Lty;->fE()Z

    move-result v4

    if-nez v4, :cond_4

    .line 3066
    if-nez v0, :cond_7

    .line 3068
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher3/Workspace;->performHapticFeedback(II)Z

    .line 3070
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3071
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Workspace;->ac(Landroid/view/View;)Z

    :cond_4
    :goto_3
    move v2, v3

    .line 3086
    goto :goto_0

    :cond_5
    move v4, v2

    .line 3064
    goto :goto_2

    .line 3073
    :cond_6
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lj()Z

    goto :goto_3

    .line 3076
    :cond_7
    if-eqz v5, :cond_8

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->Ka:Lcom/android/launcher3/Hotseat;

    iget v5, v1, Lsy;->Bb:I

    iget v6, v1, Lsy;->Bc:I

    invoke-virtual {v4, v5, v6}, Lcom/android/launcher3/Hotseat;->K(II)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Launcher;->bi(I)Z

    move-result v4

    if-eqz v4, :cond_8

    move v2, v3

    .line 3080
    :cond_8
    instance-of v0, v0, Lcom/android/launcher3/Folder;

    if-nez v0, :cond_4

    if-nez v2, :cond_4

    .line 3082
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->a(Lsy;)V

    goto :goto_3

    :cond_9
    move-object v1, v0

    goto :goto_1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/high16 v4, 0x400000

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1868
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 1875
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1877
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hu()V

    .line 1879
    iget-boolean v1, p0, Lcom/android/launcher3/Launcher;->Ku:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/2addr v1, v4

    if-eq v1, v4, :cond_1

    move v1, v0

    .line 1883
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-nez v3, :cond_2

    .line 1924
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 1879
    goto :goto_0

    .line 1887
    :cond_2
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v3

    .line 1889
    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->lh()V

    .line 1890
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v5, Lyq;->LV:Lyq;

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->kL()Z

    move-result v4

    if-nez v4, :cond_3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hv()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1892
    iget-object v3, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v3, v0}, Lcom/android/launcher3/Workspace;->av(Z)V

    .line 1895
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 1896
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hK()V

    .line 1900
    if-eqz v1, :cond_6

    .line 1901
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 1906
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v3

    .line 1907
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1908
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1910
    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1914
    :cond_4
    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    if-eqz v0, :cond_5

    .line 1915
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->reset()V

    .line 1918
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hw()V

    goto :goto_1

    .line 1903
    :cond_6
    sget-object v0, Lyq;->LV:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    goto :goto_2
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 1067
    invoke-static {}, Lcom/android/launcher3/InstallShortcutReceiver;->ha()V

    .line 1068
    invoke-static {p0}, Laht;->C(Landroid/content/Context;)Laht;

    move-result-object v0

    invoke-virtual {v0}, Laht;->onPause()V

    .line 1070
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1071
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kl:Z

    .line 1072
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    invoke-virtual {v0}, Lty;->fF()V

    .line 1073
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->yg:Lty;

    invoke-virtual {v0}, Lty;->fH()V

    .line 1077
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kY()Lym;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kY()Lym;

    move-result-object v0

    invoke-interface {v0}, Lym;->if()V

    .line 1080
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2143
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 2144
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->le()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2146
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 2148
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lh()V

    .line 2149
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->li()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2151
    invoke-direct {p0, v1}, Lcom/android/launcher3/Launcher;->Y(Z)V

    .line 2156
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2153
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1943
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1944
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1945
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Workspace;->bQ(I)V

    goto :goto_0

    .line 1947
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 969
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 977
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    sget-object v3, Lyq;->LV:Lyq;

    if-ne v0, v3, :cond_4

    .line 978
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 982
    :cond_0
    :goto_0
    sget-object v0, Lyq;->LU:Lyq;

    iput-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    .line 985
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    sget-object v3, Lyq;->LV:Lyq;

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->V(Z)V

    .line 987
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Kl:Z

    .line 988
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Km:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Ko:Z

    if-eqz v0, :cond_2

    .line 989
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/launcher3/Launcher;->T(Z)V

    .line 990
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    const/16 v3, -0x3e9

    invoke-virtual {v0, v1, v3}, Lzi;->a(ZI)V

    .line 991
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Km:Z

    .line 992
    iput-boolean v2, p0, Lcom/android/launcher3/Launcher;->Ko:Z

    .line 994
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 997
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_3

    .line 1003
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->x(Z)V

    :cond_3
    move v3, v2

    .line 1005
    :goto_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 1006
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1005
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 979
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ki:Lyq;

    sget-object v3, Lyq;->LW:Lyq;

    if-ne v0, v3, :cond_0

    .line 980
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->er()Lse;

    move-result-object v0

    invoke-direct {p0, v2, v0, v2}, Lcom/android/launcher3/Launcher;->a(ZLse;Z)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 985
    goto :goto_1

    .line 1008
    :cond_6
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_7

    .line 1009
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->x(Z)V

    .line 1011
    :cond_7
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1017
    :cond_8
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a

    move v3, v2

    .line 1018
    :goto_3
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    .line 1019
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kq:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1018
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1021
    :cond_9
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1026
    :cond_a
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KN:Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_b

    .line 1028
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->KN:Lcom/android/launcher3/BubbleTextView;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/BubbleTextView;->B(Z)V

    .line 1035
    :cond_b
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kX()V

    .line 1038
    invoke-static {p0}, Lcom/android/launcher3/InstallShortcutReceiver;->l(Landroid/content/Context;)V

    .line 1041
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Launcher;->aa(Z)V

    .line 1045
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hf()V

    .line 1050
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kY()Lym;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1054
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->le()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1055
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kY()Lym;

    move-result-object v0

    invoke-interface {v0, v1}, Lym;->ae(Z)V

    .line 1058
    :cond_c
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->ll()V

    .line 1059
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->onResume()V

    .line 1061
    invoke-static {p0}, Laht;->C(Landroid/content/Context;)Laht;

    move-result-object v0

    invoke-virtual {v0}, Laht;->onResume()V

    .line 1062
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v0, p0}, Lzi;->b(Laaf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1139
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kr:Lzi;

    invoke-virtual {v0}, Lzi;->iz()V

    .line 1141
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v0, :cond_1

    .line 1142
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->ew()V

    .line 1144
    :cond_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1951
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1952
    const-string v0, "launcher.current_screen"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->lv()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1955
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1957
    const-string v0, "launcher.state"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JF:Lyq;

    invoke-virtual {v1}, Lyq;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1960
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 1962
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v0, v0, Lwq;->JA:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v0, v0, Lwq;->Bd:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kn:Z

    if-eqz v0, :cond_1

    .line 1964
    const-string v0, "launcher.add_container"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v2, v1, Lwq;->JA:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1965
    const-string v0, "launcher.add_screen"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget-wide v2, v1, Lwq;->Bd:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1966
    const-string v0, "launcher.add_cell_x"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v1, v1, Lwq;->Bb:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1967
    const-string v0, "launcher.add_cell_y"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v1, v1, Lwq;->Bc:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1968
    const-string v0, "launcher.add_span_x"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v1, v1, Lwq;->AY:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1969
    const-string v0, "launcher.add_span_y"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JV:Lwq;

    iget v1, v1, Lwq;->AZ:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1970
    const-string v0, "launcher.add_widget_info"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JW:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1971
    const-string v0, "launcher.add_widget_id"

    iget v1, p0, Lcom/android/launcher3/Launcher;->JX:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1974
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JZ:Lvy;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kn:Z

    if-eqz v0, :cond_2

    .line 1975
    const-string v0, "launcher.rename_folder"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1976
    const-string v0, "launcher.rename_folder_id"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JZ:Lvy;

    iget-wide v2, v1, Lvy;->id:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1980
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    if-eqz v0, :cond_4

    .line 1981
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->er()Lse;

    move-result-object v0

    .line 1982
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->c(Lse;)Ljava/lang/String;

    move-result-object v0

    .line 1983
    if-eqz v0, :cond_3

    .line 1984
    const-string v1, "apps_customize_currentTab"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1986
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Ke:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->ef()I

    move-result v0

    .line 1987
    const-string v1, "apps_customize_currentIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1989
    :cond_4
    const-string v0, "launcher.view_ids"

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JI:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1990
    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2161
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/android/launcher3/Launcher;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 2163
    return v1
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 963
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 964
    const/4 v0, 0x1

    invoke-static {v0}, Luv;->N(Z)V

    .line 965
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 957
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 958
    const/4 v0, 0x0

    invoke-static {v0}, Luv;->N(Z)V

    .line 959
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2475
    const/4 v0, 0x0

    return v0
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 3704
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 3705
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_0

    .line 3706
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->eA()V

    .line 3708
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 1150
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 1151
    iput-boolean p1, p0, Lcom/android/launcher3/Launcher;->Ku:Z

    .line 1152
    return-void
.end method

.method public final onWindowVisibilityChanged(I)V
    .locals 2

    .prologue
    .line 1712
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kt:Z

    .line 1713
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hm()V

    .line 1717
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kt:Z

    if-eqz v0, :cond_1

    .line 1718
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kd:Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->ez()V

    .line 1719
    iget-boolean v0, p0, Lcom/android/launcher3/Launcher;->Kk:Z

    if-nez v0, :cond_0

    .line 1720
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1724
    new-instance v1, Lww;

    invoke-direct {v1, p0}, Lww;-><init>(Lcom/android/launcher3/Launcher;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnDrawListener(Landroid/view/ViewTreeObserver$OnDrawListener;)V

    .line 1749
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hk()V

    .line 1751
    :cond_1
    return-void

    .line 1712
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4759
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    .line 4760
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/launcher3/Workspace;->h(Ljava/lang/String;Lahz;)V

    .line 4762
    :cond_0
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 2047
    if-ltz p2, :cond_0

    .line 2048
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Launcher;->U(Z)V

    .line 2050
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2051
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 2061
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2063
    if-nez p1, :cond_0

    .line 2065
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->Kj:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2067
    :cond_0
    if-nez p3, :cond_1

    .line 2068
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 2069
    const-string v0, "source"

    const-string v1, "launcher-search"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2071
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2072
    iget-object v1, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    if-eqz v1, :cond_2

    .line 2073
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher3/SearchDropTargetBar;->kc()Landroid/graphics/Rect;

    move-result-object v0

    .line 2076
    :cond_2
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;ZLandroid/os/Bundle;Landroid/graphics/Rect;)Z

    move-result v0

    .line 2078
    if-eqz v0, :cond_3

    .line 2079
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;->hk()V

    .line 2081
    :cond_3
    return-void
.end method
