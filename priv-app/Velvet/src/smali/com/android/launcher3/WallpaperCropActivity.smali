.class public Lcom/android/launcher3/WallpaperCropActivity;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static Tx:Landroid/graphics/Point;


# instance fields
.field public Ty:Lcom/android/launcher3/CropView;

.field public Tz:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 459
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 271
    const/4 v0, 0x0

    invoke-static {v1, v1, v0, p0, p1}, Lcom/android/launcher3/WallpaperCropActivity;->a(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 280
    new-instance v0, Lqp;

    invoke-direct {v0}, Lqp;-><init>()V

    .line 284
    if-eqz p4, :cond_0

    .line 287
    :try_start_0
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 288
    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 289
    :try_start_2
    invoke-virtual {v0, v1}, Lqp;->a(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v2, v3

    .line 295
    :goto_0
    :try_start_3
    sget v3, Lqp;->to:I

    invoke-virtual {v0, v3}, Lqp;->aC(I)Ljava/lang/Integer;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_1

    .line 297
    invoke-virtual {v0}, Ljava/lang/Integer;->shortValue()S

    move-result v0

    invoke-static {v0}, Lqp;->b(S)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result v0

    .line 305
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 306
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 308
    :goto_1
    return v0

    .line 291
    :cond_0
    :try_start_4
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    .line 292
    :try_start_5
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 293
    :try_start_6
    invoke-virtual {v0, v1}, Lqp;->a(Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-object v2, v3

    goto :goto_0

    .line 305
    :cond_1
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 306
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 308
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 299
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 300
    :goto_3
    :try_start_7
    const-string v3, "Launcher3.CropActivity"

    const-string v4, "Getting exif data failed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 305
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 306
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    goto :goto_2

    .line 301
    :catch_1
    move-exception v0

    move-object v3, v2

    .line 303
    :goto_4
    :try_start_8
    const-string v1, "Launcher3.CropActivity"

    const-string v4, "Getting exif data failed"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 305
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 306
    invoke-static {v3}, Lqm;->a(Ljava/io/Closeable;)V

    goto :goto_2

    .line 305
    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_5
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 306
    invoke-static {v3}, Lqm;->a(Ljava/io/Closeable;)V

    throw v0

    .line 305
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_5

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_5

    :catchall_4
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    goto :goto_5

    .line 301
    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catch_4
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catch_5
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    goto :goto_4

    .line 299
    :catch_6
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_3

    :catch_7
    move-exception v0

    move-object v2, v3

    goto :goto_3

    :catch_8
    move-exception v0

    goto :goto_3
.end method

.method public static a(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;
    .locals 5

    .prologue
    .line 236
    sget-object v0, Lcom/android/launcher3/WallpaperCropActivity;->Tx:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 237
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 238
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 239
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 241
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 242
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 244
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 245
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 246
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 247
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v2, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 248
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 254
    :cond_0
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x2d0

    if-lt v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_3

    .line 255
    int-to-float v2, v0

    int-to-float v3, v0

    int-to-float v1, v1

    div-float v1, v3, v1

    const v3, 0x3e9d89d7

    mul-float/2addr v1, v3

    const v3, 0x3f80fc10

    add-float/2addr v1, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move v4, v1

    move v1, v0

    move v0, v4

    .line 261
    :goto_1
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v2, Lcom/android/launcher3/WallpaperCropActivity;->Tx:Landroid/graphics/Point;

    .line 263
    :cond_1
    sget-object v0, Lcom/android/launcher3/WallpaperCropActivity;->Tx:Landroid/graphics/Point;

    return-object v0

    .line 254
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 258
    :cond_3
    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v4, v1

    move v1, v0

    move v0, v4

    .line 259
    goto :goto_1
.end method

.method public static a(IIIIZ)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 865
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 867
    int-to-float v1, p0

    int-to-float v2, p1

    div-float/2addr v1, v2

    int-to-float v2, p2

    int-to-float v3, p3

    div-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 868
    iput v4, v0, Landroid/graphics/RectF;->top:F

    .line 869
    int-to-float v1, p1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 870
    int-to-float v1, p0

    int-to-float v2, p2

    int-to-float v3, p3

    div-float/2addr v2, v3

    int-to-float v3, p1

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 871
    int-to-float v1, p0

    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 872
    if-eqz p4, :cond_0

    .line 873
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 874
    iput v4, v0, Landroid/graphics/RectF;->left:F

    .line 882
    :cond_0
    :goto_0
    return-object v0

    .line 877
    :cond_1
    iput v4, v0, Landroid/graphics/RectF;->left:F

    .line 878
    int-to-float v1, p0

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 879
    int-to-float v1, p1

    int-to-float v2, p3

    int-to-float v3, p2

    div-float/2addr v2, v3

    int-to-float v3, p0

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 880
    int-to-float v1, p1

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Landroid/content/SharedPreferences;Landroid/view/WindowManager;Landroid/app/WallpaperManager;Z)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 842
    invoke-static {p0, p2}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v2

    .line 845
    const-string v0, "wallpaper.width"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 846
    const-string v1, "wallpaper.height"

    invoke-interface {p1, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 848
    if-eq v0, v3, :cond_0

    if-ne v1, v3, :cond_3

    .line 849
    :cond_0
    if-nez p4, :cond_2

    .line 861
    :cond_1
    :goto_0
    return-void

    .line 852
    :cond_2
    iget v0, v2, Landroid/graphics/Point;->x:I

    .line 853
    iget v1, v2, Landroid/graphics/Point;->y:I

    .line 857
    :cond_3
    invoke-virtual {p3}, Landroid/app/WallpaperManager;->getDesiredMinimumWidth()I

    move-result v2

    if-ne v0, v2, :cond_4

    invoke-virtual {p3}, Landroid/app/WallpaperManager;->getDesiredMinimumHeight()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 859
    :cond_4
    invoke-virtual {p3, v0, v1}, Landroid/app/WallpaperManager;->suggestDesiredDimensions(II)V

    goto :goto_0
.end method

.method public static b(Landroid/content/res/Resources;I)I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 275
    invoke-static {v0, p0, p1, v0, v0}, Lcom/android/launcher3/WallpaperCropActivity;->a(Ljava/lang/String;Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public static kn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-class v0, Lcom/android/launcher3/WallpaperCropActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static x(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;
    .locals 1

    .prologue
    .line 886
    const-string v0, "png"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0
.end method

.method public static y(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 890
    if-nez p0, :cond_0

    const-string p0, "jpg"

    .line 893
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 894
    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "png"

    :goto_0
    return-object v0

    :cond_2
    const-string v0, "jpg"

    goto :goto_0
.end method


# virtual methods
.method public final R(II)V
    .locals 5

    .prologue
    .line 822
    const-class v0, Lcom/android/launcher3/WallpaperCropActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 823
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/WallpaperCropActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 824
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 825
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 826
    const-string v2, "wallpaper.width"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 827
    const-string v2, "wallpaper.height"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 832
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 834
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v0, v2, v3, v4}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/res/Resources;Landroid/content/SharedPreferences;Landroid/view/WindowManager;Landroid/app/WallpaperManager;Z)V

    .line 836
    return-void

    .line 829
    :cond_0
    const-string v2, "wallpaper.width"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 830
    const-string v2, "wallpaper.height"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public a(Laif;ZZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    .line 147
    .line 148
    const v0, 0x7f11049c

    invoke-virtual {p0, v0}, Lcom/android/launcher3/WallpaperCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 149
    new-instance v0, Lads;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p0

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lads;-><init>(Lcom/android/launcher3/WallpaperCropActivity;Laif;Landroid/view/View;Landroid/content/Context;ZZLjava/lang/Runnable;)V

    .line 190
    new-instance v1, Ladt;

    invoke-direct {v1, p0, v0, v3}, Ladt;-><init>(Lcom/android/launcher3/WallpaperCropActivity;Landroid/os/AsyncTask;Landroid/view/View;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 198
    return-void
.end method

.method public final a(Landroid/content/res/Resources;IZ)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 334
    invoke-static {p1, p2}, Lcom/android/launcher3/WallpaperCropActivity;->b(Landroid/content/res/Resources;I)I

    move-result v5

    .line 335
    iget-object v0, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v0}, Lcom/android/launcher3/CropView;->fs()Landroid/graphics/Point;

    move-result-object v0

    .line 336
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v1

    .line 338
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v0, v3, v4, v9}, Lcom/android/launcher3/WallpaperCropActivity;->a(IIIIZ)Landroid/graphics/RectF;

    move-result-object v4

    .line 340
    new-instance v10, Ladv;

    invoke-direct {v10, p0, v8}, Ladv;-><init>(Lcom/android/launcher3/WallpaperCropActivity;Z)V

    .line 351
    new-instance v0, Ladx;

    iget v6, v1, Landroid/graphics/Point;->x:I

    iget v7, v1, Landroid/graphics/Point;->y:I

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v10}, Ladx;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 353
    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ladx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 354
    return-void
.end method

.method public final a(Landroid/net/Uri;Lady;Z)V
    .locals 12

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    .line 365
    iget-object v0, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v0}, Lcom/android/launcher3/CropView;->getLayoutDirection()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 367
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 369
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 370
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 371
    iget v1, v2, Landroid/graphics/Point;->x:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    if-ge v1, v2, :cond_2

    const/4 v1, 0x1

    .line 373
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v6

    .line 376
    iget-object v2, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v2}, Lcom/android/launcher3/CropView;->fr()Landroid/graphics/RectF;

    move-result-object v3

    .line 378
    iget-object v2, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v2}, Lcom/android/launcher3/CropView;->fs()Landroid/graphics/Point;

    move-result-object v2

    .line 380
    iget-object v4, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v4}, Lcom/android/launcher3/CropView;->fq()I

    move-result v4

    .line 381
    iget-object v7, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v7}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v8

    div-float/2addr v7, v8

    .line 384
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 385
    int-to-float v9, v4

    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 386
    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    iget v11, v2, Landroid/graphics/Point;->x:I

    int-to-float v11, v11

    aput v11, v9, v10

    const/4 v10, 0x1

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    aput v2, v9, v10

    .line 387
    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 388
    const/4 v2, 0x0

    const/4 v8, 0x0

    aget v8, v9, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    aput v8, v9, v2

    .line 389
    const/4 v2, 0x1

    const/4 v8, 0x1

    aget v8, v9, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    aput v8, v9, v2

    .line 394
    const/4 v2, 0x0

    iget v8, v3, Landroid/graphics/RectF;->left:F

    invoke-static {v2, v8}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v3, Landroid/graphics/RectF;->left:F

    .line 395
    const/4 v2, 0x0

    aget v2, v9, v2

    iget v8, v3, Landroid/graphics/RectF;->right:F

    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, v3, Landroid/graphics/RectF;->right:F

    .line 396
    const/4 v2, 0x0

    iget v8, v3, Landroid/graphics/RectF;->top:F

    invoke-static {v2, v8}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v3, Landroid/graphics/RectF;->top:F

    .line 397
    const/4 v2, 0x1

    aget v2, v9, v2

    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, v3, Landroid/graphics/RectF;->bottom:F

    .line 403
    if-eqz v5, :cond_3

    .line 404
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v8, 0x0

    aget v8, v9, v8

    iget v10, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v8, v10

    iget v10, v3, Landroid/graphics/RectF;->left:F

    invoke-static {v8, v10}, Ljava/lang/Math;->min(FF)F

    move-result v8

    mul-float/2addr v2, v8

    .line 409
    :goto_2
    iget v8, v6, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    div-float/2addr v8, v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v10

    sub-float/2addr v8, v10

    .line 410
    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 412
    if-eqz v5, :cond_5

    .line 413
    iget v0, v3, Landroid/graphics/RectF;->left:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v2, v5

    sub-float/2addr v0, v5

    iput v0, v3, Landroid/graphics/RectF;->left:F

    .line 414
    iget v0, v3, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    add-float/2addr v0, v2

    iput v0, v3, Landroid/graphics/RectF;->right:F

    .line 424
    :goto_3
    if-eqz v1, :cond_7

    .line 425
    iget v0, v3, Landroid/graphics/RectF;->top:F

    iget v1, v6, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    div-float/2addr v1, v7

    add-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/RectF;->bottom:F

    .line 435
    :goto_4
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 436
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 438
    new-instance v9, Ladw;

    const/4 v0, 0x1

    invoke-direct {v9, p0, v5, v6, v0}, Ladw;-><init>(Lcom/android/launcher3/WallpaperCropActivity;IIZ)V

    .line 447
    new-instance v0, Ladx;

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Ladx;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 449
    if-eqz p2, :cond_0

    .line 450
    invoke-virtual {v0, p2}, Ladx;->a(Lady;)V

    .line 452
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ladx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 453
    return-void

    .line 365
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 371
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 406
    :cond_3
    if-eqz v0, :cond_4

    const/4 v2, 0x0

    aget v2, v9, v2

    iget v8, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v8

    goto :goto_2

    :cond_4
    iget v2, v3, Landroid/graphics/RectF;->left:F

    goto :goto_2

    .line 416
    :cond_5
    if-eqz v0, :cond_6

    .line 417
    iget v0, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v2

    iput v0, v3, Landroid/graphics/RectF;->right:F

    goto :goto_3

    .line 419
    :cond_6
    iget v0, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v2

    iput v0, v3, Landroid/graphics/RectF;->left:F

    goto :goto_3

    .line 427
    :cond_7
    iget v0, v6, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    div-float/2addr v0, v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v0, v1

    .line 429
    const/4 v1, 0x1

    aget v1, v9, v1

    iget v2, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v2

    iget v2, v3, Landroid/graphics/RectF;->top:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 432
    iget v1, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v0

    iput v1, v3, Landroid/graphics/RectF;->top:F

    .line 433
    iget v1, v3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/RectF;->bottom:F

    goto :goto_4
.end method

.method public final a(Landroid/net/Uri;Z)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 312
    invoke-static {p0, p1}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    .line 313
    new-instance v0, Ladx;

    move-object v1, p0

    move-object v2, p1

    move v6, v5

    move v8, v5

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Ladx;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 315
    invoke-virtual {v0}, Ladx;->kq()Landroid/graphics/Point;

    move-result-object v1

    .line 316
    new-instance v2, Ladu;

    invoke-direct {v2, p0, v1, v7}, Ladu;-><init>(Lcom/android/launcher3/WallpaperCropActivity;Landroid/graphics/Point;Z)V

    .line 325
    invoke-virtual {v0, v2}, Ladx;->g(Ljava/lang/Runnable;)V

    .line 326
    invoke-virtual {v0, v7}, Ladx;->ap(Z)V

    .line 327
    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ladx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 328
    return-void
.end method

.method protected ed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    const v0, 0x7f0401bf

    invoke-virtual {p0, v0}, Lcom/android/launcher3/WallpaperCropActivity;->setContentView(I)V

    .line 92
    const v0, 0x7f11049b

    invoke-virtual {p0, v0}, Lcom/android/launcher3/WallpaperCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CropView;

    iput-object v0, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    .line 94
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 97
    if-nez v0, :cond_0

    .line 98
    const-string v0, "Launcher3.CropActivity"

    const-string v1, "No URI passed in intent, exiting WallpaperCropActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->finish()V

    .line 134
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 106
    const v2, 0x7f04000c

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 107
    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Ladq;

    invoke-direct {v2, p0, v0}, Ladq;-><init>(Lcom/android/launcher3/WallpaperCropActivity;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    const v1, 0x7f110072

    invoke-virtual {p0, v1}, Lcom/android/launcher3/WallpaperCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/WallpaperCropActivity;->Tz:Landroid/view/View;

    .line 118
    new-instance v1, Laii;

    const/16 v2, 0x400

    invoke-direct {v1, p0, v0, v2}, Laii;-><init>(Landroid/content/Context;Landroid/net/Uri;I)V

    .line 120
    iget-object v0, p0, Lcom/android/launcher3/WallpaperCropActivity;->Tz:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 121
    new-instance v0, Ladr;

    invoke-direct {v0, p0, v1}, Ladr;-><init>(Lcom/android/launcher3/WallpaperCropActivity;Laii;)V

    .line 133
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/android/launcher3/WallpaperCropActivity;->a(Laif;ZZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public iT()Z
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->ed()V

    .line 84
    invoke-virtual {p0}, Lcom/android/launcher3/WallpaperCropActivity;->iT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/WallpaperCropActivity;->setRequestedOrientation(I)V

    .line 87
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v0}, Lcom/android/launcher3/CropView;->destroy()V

    .line 141
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 142
    return-void
.end method
