.class public Lcom/android/launcher3/Hotseat;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private GQ:Lcom/android/launcher3/CellLayout;

.field private Jb:I

.field private Jc:Z

.field private Jd:Z

.field private xZ:Lcom/android/launcher3/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/Hotseat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/Hotseat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 59
    const v1, 0x7f0e0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher3/Hotseat;->Jc:Z

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/launcher3/Hotseat;->Jd:Z

    .line 63
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private gX()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/launcher3/Hotseat;->Jd:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/Hotseat;->Jc:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final K(II)I
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/android/launcher3/Hotseat;->gX()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v0

    sub-int/2addr v0, p2

    add-int/lit8 p1, v0, -0x1

    :cond_0
    return p1
.end method

.method public final b(Lcom/android/launcher3/Launcher;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    .line 67
    return-void
.end method

.method final bg(I)I
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/android/launcher3/Hotseat;->gX()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return p1
.end method

.method final bh(I)I
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/launcher3/Hotseat;->gX()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bi(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-static {}, Lyu;->im()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/android/launcher3/Hotseat;->Jb:I

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final gW()Lcom/android/launcher3/CellLayout;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    return-object v0
.end method

.method final gY()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 142
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->removeAllViewsInLayout()V

    .line 144
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/android/launcher3/Hotseat;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 148
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 149
    const v2, 0x7f04001a

    iget-object v3, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 151
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 153
    invoke-static {v2}, Ladp;->c(Landroid/graphics/drawable/Drawable;)V

    .line 154
    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 156
    const v2, 0x7f0a00a1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 157
    new-instance v0, Lwh;

    invoke-direct {v0}, Lwh;-><init>()V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 158
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hE()Landroid/view/View$OnTouchListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 160
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Launcher;->Q(Landroid/view/View;)V

    .line 161
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v0, v0, Lcom/android/launcher3/Launcher;->KS:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 167
    :cond_0
    iget v0, p0, Lcom/android/launcher3/Hotseat;->Jb:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Hotseat;->bg(I)I

    move-result v0

    .line 168
    iget v2, p0, Lcom/android/launcher3/Hotseat;->Jb:I

    invoke-virtual {p0, v2}, Lcom/android/launcher3/Hotseat;->bh(I)I

    move-result v2

    .line 169
    new-instance v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-direct {v4, v0, v2, v5, v5}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(IIII)V

    .line 170
    iput-boolean v6, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    .line 171
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v2, -0x1

    invoke-virtual {v1}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    .line 173
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 125
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 126
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 127
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v1

    .line 129
    iget v0, v1, Ltu;->DY:I

    iput v0, p0, Lcom/android/launcher3/Hotseat;->Jb:I

    .line 130
    const v0, 0x7f11020c

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Hotseat;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    iput-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    .line 131
    iget-boolean v0, v1, Ltu;->Dp:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v1, Ltu;->Dr:Z

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    iget v1, v1, Ltu;->Di:F

    float-to-int v1, v1

    invoke-virtual {v0, v2, v1}, Lcom/android/launcher3/CellLayout;->A(II)V

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->I(Z)V

    .line 138
    invoke-virtual {p0}, Lcom/android/launcher3/Hotseat;->gY()V

    .line 139
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    iget v1, v1, Ltu;->Di:F

    float-to-int v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/CellLayout;->A(II)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/launcher3/Hotseat;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/CellLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 79
    return-void
.end method
