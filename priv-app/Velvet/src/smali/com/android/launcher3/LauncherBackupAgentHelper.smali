.class public Lcom/android/launcher3/LauncherBackupAgentHelper;
.super Landroid/app/backup/BackupAgentHelper;
.source "PG"


# static fields
.field private static Mu:Landroid/app/backup/BackupManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/backup/BackupAgentHelper;-><init>()V

    return-void
.end method

.method public static o(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/android/launcher3/LauncherBackupAgentHelper;->Mu:Landroid/app/backup/BackupManager;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/launcher3/LauncherBackupAgentHelper;->Mu:Landroid/app/backup/BackupManager;

    .line 51
    :cond_0
    sget-object v0, Lcom/android/launcher3/LauncherBackupAgentHelper;->Mu:Landroid/app/backup/BackupManager;

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->dataChanged()V

    .line 52
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 66
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherBackupAgentHelper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "launcher_restore_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "restore is "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v1, "enabled"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-string v1, "LP"

    new-instance v2, Labb;

    const-string v3, "com.android.launcher3.prefs"

    invoke-direct {v2, p0, v3, v0}, Labb;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher3/LauncherBackupAgentHelper;->addHelper(Ljava/lang/String;Landroid/app/backup/BackupHelper;)V

    .line 74
    const-string v1, "L"

    new-instance v2, Lyz;

    invoke-direct {v2, p0, v0}, Lyz;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher3/LauncherBackupAgentHelper;->addHelper(Ljava/lang/String;Landroid/app/backup/BackupHelper;)V

    .line 76
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :cond_1
    const-string v1, "disabled"

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 59
    const-string v0, "com.android.launcher3.prefs"

    .line 60
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/LauncherBackupAgentHelper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 61
    invoke-super {p0}, Landroid/app/backup/BackupAgentHelper;->onDestroy()V

    .line 62
    return-void
.end method

.method public onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1, p2, p3}, Landroid/app/backup/BackupAgentHelper;->onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V

    .line 84
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherBackupAgentHelper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Labn;->OT:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 87
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 89
    if-nez v1, :cond_0

    .line 90
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/LauncherProvider;->iM()V

    .line 93
    :cond_0
    return-void
.end method
