.class public Lcom/android/launcher3/SearchDropTargetBar;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Ltz;


# static fields
.field private static final SF:Landroid/view/animation/AccelerateInterpolator;


# instance fields
.field private SD:Landroid/animation/ObjectAnimator;

.field private SE:Landroid/animation/ObjectAnimator;

.field private SG:Z

.field private SH:Landroid/view/View;

.field private SI:Landroid/view/View;

.field private SJ:Lss;

.field private SK:Lss;

.field private SL:I

.field private SM:Z

.field private SN:Landroid/graphics/drawable/Drawable;

.field private SO:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/launcher3/SearchDropTargetBar;->SF:Landroid/view/animation/AccelerateInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/SearchDropTargetBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SM:Z

    .line 61
    return-void
.end method

.method private a(Landroid/animation/ObjectAnimator;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/android/launcher3/SearchDropTargetBar;->SF:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 90
    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 91
    new-instance v0, Ladf;

    invoke-direct {v0, p0, p2}, Ladf;-><init>(Lcom/android/launcher3/SearchDropTargetBar;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 97
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/launcher3/Launcher;Lty;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 64
    invoke-virtual {p2, p0}, Lty;->a(Ltz;)V

    .line 65
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SJ:Lss;

    invoke-virtual {p2, v0}, Lty;->a(Ltz;)V

    .line 66
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SK:Lss;

    invoke-virtual {p2, v0}, Lty;->a(Ltz;)V

    .line 67
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SJ:Lss;

    invoke-virtual {p2, v0}, Lty;->b(Luo;)V

    .line 68
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SK:Lss;

    invoke-virtual {p2, v0}, Lty;->b(Luo;)V

    .line 69
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SK:Lss;

    invoke-virtual {p2, v0}, Lty;->d(Luo;)V

    .line 70
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SJ:Lss;

    invoke-virtual {v0, p1}, Lss;->a(Lcom/android/launcher3/Launcher;)V

    .line 71
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SK:Lss;

    invoke-virtual {v0, p1}, Lss;->a(Lcom/android/launcher3/Launcher;)V

    .line 72
    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->hM()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    .line 73
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SO:Z

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/android/launcher3/SearchDropTargetBar;->SL:I

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/SearchDropTargetBar;->a(Landroid/animation/ObjectAnimator;Landroid/view/View;)V

    .line 80
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    goto :goto_0

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public final a(Lui;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 189
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 190
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SD:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 191
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SG:Z

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 193
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 195
    :cond_0
    return-void
.end method

.method public final an(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 142
    :goto_0
    iget-boolean v2, p0, Lcom/android/launcher3/SearchDropTargetBar;->SG:Z

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    .line 155
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 141
    goto :goto_0

    .line 143
    :cond_1
    if-eqz p1, :cond_2

    .line 144
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 145
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 154
    :goto_2
    iput-boolean v1, p0, Lcom/android/launcher3/SearchDropTargetBar;->SG:Z

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 148
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SO:Z

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_2

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_2
.end method

.method public final ao(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 157
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 158
    :goto_0
    iget-boolean v2, p0, Lcom/android/launcher3/SearchDropTargetBar;->SG:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 171
    :goto_1
    return-void

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 164
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SO:Z

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    iget v2, p0, Lcom/android/launcher3/SearchDropTargetBar;->SL:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 170
    :goto_2
    iput-boolean v1, p0, Lcom/android/launcher3/SearchDropTargetBar;->SG:Z

    goto :goto_1

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_2
.end method

.method public final c(ZZ)V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 221
    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SN:Landroid/graphics/drawable/Drawable;

    .line 222
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SN:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    if-eqz p2, :cond_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher3/SearchDropTargetBar;->SN:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final eK()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 203
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SM:Z

    if-nez v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 206
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SD:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 207
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SG:Z

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 209
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SM:Z

    goto :goto_0
.end method

.method public final ka()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 131
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 132
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SD:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 133
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 134
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SE:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 135
    return-void
.end method

.method public final kb()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SM:Z

    .line 199
    return-void
.end method

.method public final kc()Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 231
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 233
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 235
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 236
    aget v2, v1, v3

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 237
    aget v2, v1, v4

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 238
    aget v2, v1, v3

    iget-object v3, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 239
    aget v1, v1, v4

    iget-object v2, p0, Lcom/android/launcher3/SearchDropTargetBar;->SH:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 242
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    .line 101
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 104
    const v0, 0x7f1103ad

    invoke-virtual {p0, v0}, Lcom/android/launcher3/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    .line 105
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    const v1, 0x7f110152

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lss;

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SJ:Lss;

    .line 106
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    const v1, 0x7f110151

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lss;

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SK:Lss;

    .line 108
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SJ:Lss;

    invoke-virtual {v0, p0}, Lss;->a(Lcom/android/launcher3/SearchDropTargetBar;)V

    .line 109
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SK:Lss;

    invoke-virtual {v0, p0}, Lss;->a(Lcom/android/launcher3/SearchDropTargetBar;)V

    .line 111
    invoke-virtual {p0}, Lcom/android/launcher3/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SO:Z

    .line 115
    iget-boolean v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SO:Z

    if-eqz v0, :cond_0

    .line 116
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 117
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 118
    iget v0, v0, Ltu;->Ed:I

    iput v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SL:I

    .line 119
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    iget v1, p0, Lcom/android/launcher3/SearchDropTargetBar;->SL:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 120
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/launcher3/SearchDropTargetBar;->SL:I

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    aput v5, v2, v3

    invoke-static {v0, v1, v2}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SD:Landroid/animation/ObjectAnimator;

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SD:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/SearchDropTargetBar;->a(Landroid/animation/ObjectAnimator;Landroid/view/View;)V

    .line 128
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 125
    iget-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SI:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/SearchDropTargetBar;->SD:Landroid/animation/ObjectAnimator;

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
