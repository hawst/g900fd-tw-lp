.class public Lcom/android/launcher3/DragLayer$LayoutParams;
.super Landroid/widget/FrameLayout$LayoutParams;
.source "PG"


# instance fields
.field public FC:Z

.field public x:I

.field public y:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 444
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    .line 445
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 460
    iget v0, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    return v0
.end method

.method public setHeight(I)V
    .locals 0

    .prologue
    .line 456
    iput p1, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    .line 457
    return-void
.end method

.method public setWidth(I)V
    .locals 0

    .prologue
    .line 448
    iput p1, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    .line 449
    return-void
.end method

.method public setX(I)V
    .locals 0

    .prologue
    .line 464
    iput p1, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    .line 465
    return-void
.end method

.method public setY(I)V
    .locals 0

    .prologue
    .line 472
    iput p1, p0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    .line 473
    return-void
.end method
