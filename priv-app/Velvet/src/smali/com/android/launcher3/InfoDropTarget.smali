.class public Lcom/android/launcher3/InfoDropTarget;
.super Lss;
.source "PG"


# instance fields
.field private CB:Landroid/content/res/ColorStateList;

.field private Jm:Landroid/graphics/drawable/TransitionDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/InfoDropTarget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lss;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lui;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    const/4 v0, 0x1

    .line 108
    invoke-interface {p1}, Lui;->em()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 112
    :goto_0
    iput-boolean v1, p0, Lcom/android/launcher3/InfoDropTarget;->zE:Z

    .line 113
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 114
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->CB:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/InfoDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 115
    invoke-virtual {p0}, Lcom/android/launcher3/InfoDropTarget;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 116
    return-void

    .line 115
    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final a(Luq;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 79
    const/4 v0, 0x0

    .line 80
    iget-object v1, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v1, v1, Lrr;

    if-eqz v1, :cond_1

    .line 81
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Lrr;

    iget-object v0, v0, Lrr;->xr:Landroid/content/ComponentName;

    move-object v1, v0

    .line 88
    :goto_0
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v0, v0, Lwq;

    if-eqz v0, :cond_3

    .line 89
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Lwq;

    iget-object v0, v0, Lwq;->Jl:Lahz;

    .line 94
    :goto_1
    if-eqz v1, :cond_0

    .line 95
    iget-object v2, p0, Lcom/android/launcher3/InfoDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2, v1, v0}, Lcom/android/launcher3/Launcher;->c(Landroid/content/ComponentName;Lahz;)V

    .line 99
    :cond_0
    iput-boolean v3, p1, Luq;->Gh:Z

    .line 100
    return v3

    .line 82
    :cond_1
    iget-object v1, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v1, v1, Ladh;

    if-eqz v1, :cond_2

    .line 83
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Ladh;

    iget-object v0, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 84
    :cond_2
    iget-object v1, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v1, v1, Lacw;

    if-eqz v1, :cond_4

    .line 85
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Lacw;

    iget-object v0, v0, Lacw;->xr:Landroid/content/ComponentName;

    move-object v1, v0

    goto :goto_0

    .line 91
    :cond_3
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final c(Luq;)V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0, p1}, Lss;->c(Luq;)V

    .line 127
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    iget v1, p0, Lcom/android/launcher3/InfoDropTarget;->zB:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 128
    iget v0, p0, Lcom/android/launcher3/InfoDropTarget;->zF:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/InfoDropTarget;->setTextColor(I)V

    .line 129
    return-void
.end method

.method public final e(Luq;)V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1}, Lss;->e(Luq;)V

    .line 134
    iget-boolean v0, p1, Luq;->Gc:Z

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 136
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->CB:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/InfoDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 138
    :cond_0
    return-void
.end method

.method public final eK()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lss;->eK()V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/InfoDropTarget;->zE:Z

    .line 122
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-super {p0}, Lss;->onFinishInflate()V

    .line 48
    invoke-virtual {p0}, Lcom/android/launcher3/InfoDropTarget;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->CB:Landroid/content/res/ColorStateList;

    .line 51
    invoke-virtual {p0}, Lcom/android/launcher3/InfoDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 52
    const v0, 0x7f0b004d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/InfoDropTarget;->zF:I

    .line 53
    invoke-virtual {p0}, Lcom/android/launcher3/InfoDropTarget;->eI()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    .line 55
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    if-nez v0, :cond_0

    .line 57
    const v0, 0x7f020257

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    .line 58
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/android/launcher3/InfoDropTarget;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/android/launcher3/InfoDropTarget;->Jm:Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/InfoDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 67
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 68
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-boolean v0, v0, Lyu;->Md:Z

    if-nez v0, :cond_2

    .line 69
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/launcher3/InfoDropTarget;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :cond_2
    return-void
.end method
