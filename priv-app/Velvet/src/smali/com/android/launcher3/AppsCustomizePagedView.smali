.class public Lcom/android/launcher3/AppsCustomizePagedView;
.super Lacu;
.source "PG"

# interfaces
.implements Labp;
.implements Lact;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lui;


# static fields
.field private static ye:Landroid/graphics/Rect;


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private xZ:Lcom/android/launcher3/Launcher;

.field private xn:Lwi;

.field private yA:Z

.field private yB:Ljava/util/ArrayList;

.field private yC:Ljava/util/ArrayList;

.field private yD:Lafb;

.field private yE:Z

.field private yF:Z

.field private yf:Lse;

.field private yg:Lty;

.field private yh:I

.field private yi:Ljava/util/ArrayList;

.field private yj:Ljava/util/ArrayList;

.field private yk:I

.field private yl:I

.field private ym:I

.field private yn:I

.field private yo:Laco;

.field private yp:I

.field private yq:I

.field public yr:Ljava/util/ArrayList;

.field private ys:Ljava/lang/Runnable;

.field private yt:Ljava/lang/Runnable;

.field public yu:I

.field public yv:I

.field private yw:Lacy;

.field private yx:Z

.field private yy:Z

.field private yz:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/launcher3/AppsCustomizePagedView;->ye:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 215
    invoke-direct {p0, p1, p2}, Lacu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 158
    sget-object v0, Lse;->yR:Lse;

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    .line 167
    iput v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    .line 182
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 188
    iput-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ys:Ljava/lang/Runnable;

    .line 189
    iput-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yt:Ljava/lang/Runnable;

    .line 194
    iput v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    .line 195
    iput v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    .line 196
    iput-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    .line 197
    iput-boolean v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yx:Z

    .line 198
    iput-boolean v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yy:Z

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yB:Ljava/util/ArrayList;

    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yC:Ljava/util/ArrayList;

    .line 216
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 217
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    .line 220
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v0, v0, Lyu;->xn:Lwi;

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xn:Lwi;

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    .line 224
    sget-object v0, Ladb;->Sr:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 225
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    .line 226
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    .line 227
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 228
    new-instance v0, Laco;

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Laco;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yo:Laco;

    .line 232
    iput-boolean v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QL:Z

    .line 235
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getImportantForAccessibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 236
    invoke-virtual {p0, v3}, Lcom/android/launcher3/AppsCustomizePagedView;->setImportantForAccessibility(I)V

    .line 238
    :cond_0
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->QG:Z

    .line 239
    return-void
.end method

.method static a(Lcom/android/launcher3/Launcher;Lacy;)Landroid/os/Bundle;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 486
    .line 487
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 488
    iget v1, p1, Lacy;->AY:I

    iget v2, p1, Lacy;->AZ:I

    sget-object v3, Lcom/android/launcher3/AppsCustomizePagedView;->ye:Landroid/graphics/Rect;

    invoke-static {p0, v1, v2, v3}, Lrs;->a(Lcom/android/launcher3/Launcher;IILandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 489
    iget-object v1, p1, Lacy;->xr:Landroid/content/ComponentName;

    invoke-static {p0, v1, v0}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 492
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 493
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v2, v1

    float-to-int v2, v2

    .line 494
    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v1, v0

    .line 496
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 497
    const-string v3, "appWidgetMinWidth"

    sget-object v4, Lcom/android/launcher3/AppsCustomizePagedView;->ye:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 499
    const-string v3, "appWidgetMinHeight"

    sget-object v4, Lcom/android/launcher3/AppsCustomizePagedView;->ye:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 501
    const-string v3, "appWidgetMaxWidth"

    sget-object v4, Lcom/android/launcher3/AppsCustomizePagedView;->ye:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int v2, v4, v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503
    const-string v2, "appWidgetMaxHeight"

    sget-object v3, Lcom/android/launcher3/AppsCustomizePagedView;->ye:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v3, v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 506
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/android/launcher3/AppsCustomizePagedView;)Lcom/android/launcher3/Launcher;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    return-object v0
.end method

.method private a(Landroid/view/View;ZZ)V
    .locals 2

    .prologue
    .line 727
    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    if-eq p1, v0, :cond_1

    instance-of v0, p1, Lcom/android/launcher3/DeleteDropTarget;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/android/launcher3/Folder;

    if-nez v0, :cond_1

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hK()V

    .line 732
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Launcher;->ad(Z)V

    .line 736
    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/AppsCustomizePagedView;ILjava/util/ArrayList;III)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 144
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    iget v2, v0, Lrv;->yb:I

    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    invoke-virtual {p0, v3}, Lcom/android/launcher3/AppsCustomizePagedView;->aV(I)I

    move-result v3

    if-lt v2, v3, :cond_0

    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    invoke-virtual {p0, v3}, Lcom/android/launcher3/AppsCustomizePagedView;->aW(I)I

    move-result v3

    if-le v2, v3, :cond_1

    :cond_0
    invoke-virtual {v0, v8}, Lrv;->cancel(Z)Z

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->aR(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lrv;->setThreadPriority(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aQ(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0xc8

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v0, Lsg;

    new-instance v5, Lsb;

    invoke-direct {v5, p0, v1}, Lsb;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;I)V

    new-instance v6, Lsc;

    invoke-direct {v6, p0}, Lsc;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;)V

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v7

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v7}, Lsg;-><init>(ILjava/util/ArrayList;IILsf;Lsf;Lafb;)V

    new-instance v1, Lrv;

    sget-object v2, Lsh;->zc:Lsh;

    invoke-direct {v1, p1, v2}, Lrv;-><init>(ILsh;)V

    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aR(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lrv;->setThreadPriority(I)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lsg;

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Lrv;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/AppsCustomizePagedView;Lrv;Lsg;)V
    .locals 6

    .prologue
    .line 144
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lrv;->ea()V

    :cond_0
    iget-object v1, p2, Lsg;->yX:Ljava/util/ArrayList;

    iget-object v2, p2, Lsg;->yY:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lrv;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lrv;->ea()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v4

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lafb;->ac(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/AppsCustomizePagedView;Lsg;Z)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lsg;Z)V

    return-void
.end method

.method private a(Lsg;Z)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1239
    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yA:Z

    if-eqz v0, :cond_0

    .line 1240
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yB:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1268
    :goto_0
    return-void

    .line 1244
    :cond_0
    :try_start_0
    iget v0, p1, Lsg;->yb:I

    .line 1245
    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lacr;

    .line 1247
    iget-object v1, p1, Lsg;->yX:Ljava/util/ArrayList;

    .line 1248
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v4

    .line 1249
    :goto_1
    if-ge v3, v5, :cond_2

    .line 1250
    invoke-virtual {v0, v3}, Lacr;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/PagedViewWidget;

    .line 1251
    if-eqz v1, :cond_1

    .line 1252
    iget-object v2, p1, Lsg;->yY:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1253
    new-instance v6, Lus;

    invoke-direct {v6, v2}, Lus;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v6}, Lcom/android/launcher3/PagedViewWidget;->a(Lus;)V

    .line 1249
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1257
    :cond_2
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->et()V

    .line 1260
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1261
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1262
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    .line 1263
    iget v2, v0, Lrv;->yb:I

    .line 1264
    invoke-direct {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->aR(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lrv;->setThreadPriority(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1267
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v4}, Lsg;->A(Z)V

    throw v0

    :cond_3
    invoke-virtual {p1, v4}, Lsg;->A(Z)V

    goto :goto_0
.end method

.method private aP(I)V
    .locals 15

    .prologue
    .line 949
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->jc()Z

    move-result v8

    .line 950
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QA:I

    mul-int/2addr v0, v1

    .line 951
    mul-int v7, p1, v0

    .line 952
    add-int/2addr v0, v7

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 953
    invoke-virtual/range {p0 .. p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lrw;

    .line 955
    invoke-virtual {v0}, Lrw;->eb()V

    .line 956
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 957
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    move v3, v7

    .line 958
    :goto_0
    if-ge v3, v9, :cond_0

    .line 959
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lrr;

    .line 960
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04001d

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/BubbleTextView;

    .line 962
    invoke-virtual {v1, v6}, Lcom/android/launcher3/BubbleTextView;->b(Lrr;)V

    .line 963
    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, v2}, Lcom/android/launcher3/BubbleTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 964
    invoke-virtual {v1, p0}, Lcom/android/launcher3/BubbleTextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 965
    invoke-virtual {v1, p0}, Lcom/android/launcher3/BubbleTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 966
    invoke-virtual {v1, p0}, Lcom/android/launcher3/BubbleTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 967
    iget-object v2, v0, Lrw;->yd:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v1, v2}, Lcom/android/launcher3/BubbleTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 969
    sub-int v4, v3, v7

    .line 970
    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    rem-int v2, v4, v2

    .line 971
    iget v5, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    div-int v12, v4, v5

    .line 972
    if-eqz v8, :cond_1

    .line 973
    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    sub-int v2, v4, v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    .line 975
    :goto_1
    const/4 v2, -0x1

    new-instance v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    const/4 v13, 0x1

    const/4 v14, 0x1

    invoke-direct {v4, v5, v12, v13, v14}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(IIII)V

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lrw;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    .line 977
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 978
    iget-object v1, v6, Lrr;->xq:Landroid/graphics/Bitmap;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 958
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 981
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->et()V

    .line 982
    return-void

    :cond_1
    move v5, v2

    goto :goto_1
.end method

.method private aQ(I)I
    .locals 4

    .prologue
    .line 989
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    .line 990
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qf:I

    if-ltz v1, :cond_1

    .line 991
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qf:I

    move v1, v0

    .line 997
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 998
    const v0, 0x7fffffff

    .line 999
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1000
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    .line 1001
    iget v0, v0, Lrv;->yb:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    goto :goto_1

    .line 1004
    :cond_0
    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1005
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v1, v0

    return v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private aR(I)I
    .locals 3

    .prologue
    const/16 v1, 0x13

    const/4 v0, 0x1

    .line 1013
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aQ(I)I

    move-result v2

    .line 1014
    if-gtz v2, :cond_0

    .line 1019
    :goto_0
    return v0

    .line 1016
    :cond_0
    if-gt v2, v0, :cond_1

    move v0, v1

    .line 1017
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1019
    goto :goto_0
.end method

.method public static synthetic b(Lcom/android/launcher3/AppsCustomizePagedView;)Laco;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yo:Laco;

    return-object v0
.end method

.method private static b(Landroid/view/ViewGroup;I)V
    .locals 3

    .prologue
    .line 910
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 911
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 912
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 911
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 914
    :cond_0
    return-void
.end method

.method private c(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 1423
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1424
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1425
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrr;

    .line 1426
    iget-object v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-static {}, Lzi;->iB()Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v3, v0, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v3

    .line 1427
    if-gez v3, :cond_0

    .line 1428
    iget-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    add-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    invoke-virtual {v4, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1424
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1431
    :cond_1
    return-void
.end method

.method public static synthetic c(Lcom/android/launcher3/AppsCustomizePagedView;)Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yA:Z

    return v0
.end method

.method public static synthetic d(Lcom/android/launcher3/AppsCustomizePagedView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yC:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic e(Lcom/android/launcher3/AppsCustomizePagedView;)I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    return v0
.end method

.method private e(Ljava/util/ArrayList;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1452
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    .line 1453
    :goto_0
    if-ge v4, v5, :cond_3

    .line 1454
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrr;

    .line 1455
    iget-object v6, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    iget-object v1, v0, Lrr;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    move v2, v3

    :goto_1
    if-ge v2, v8, :cond_2

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrr;

    iget-object v9, v1, Lrr;->Jl:Lahz;

    iget-object v10, v0, Lrr;->Jl:Lahz;

    invoke-virtual {v9, v10}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v1, v1, Lrr;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    .line 1456
    :goto_2
    if-ltz v0, :cond_0

    .line 1457
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1453
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1455
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, -0x1

    goto :goto_2

    .line 1460
    :cond_3
    return-void
.end method

.method private eg()V
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yq:I

    .line 336
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QA:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yp:I

    .line 337
    return-void
.end method

.method private ei()V
    .locals 1

    .prologue
    .line 432
    iget-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yE:Z

    if-eqz v0, :cond_0

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yF:Z

    .line 439
    :goto_0
    return-void

    .line 435
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eg()V

    .line 436
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->je()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->requestLayout()V

    .line 437
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yF:Z

    goto :goto_0

    .line 436
    :cond_1
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eq()V

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->jH()V

    goto :goto_1
.end method

.method private eq()V
    .locals 5

    .prologue
    .line 859
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 860
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 861
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    .line 862
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lrv;->cancel(Z)Z

    .line 863
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 864
    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QJ:Ljava/util/ArrayList;

    iget v3, v0, Lrv;->yb:I

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 867
    iget v0, v0, Lrv;->yb:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    .line 868
    instance-of v2, v0, Lacr;

    if-eqz v2, :cond_0

    .line 869
    check-cast v0, Lacr;

    invoke-virtual {v0}, Lacr;->eb()V

    goto :goto_0

    .line 872
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 873
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 874
    return-void
.end method

.method private et()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v3, 0x0

    .line 1329
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v5

    .line 1331
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QE:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->f([I)V

    .line 1332
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QE:[I

    aget v2, v0, v3

    .line 1333
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QE:[I

    const/4 v1, 0x1

    aget v1, v0, v1

    .line 1334
    const/4 v0, -0x1

    .line 1335
    if-ne v2, v1, :cond_4

    .line 1337
    add-int/lit8 v4, v5, -0x1

    if-ge v1, v4, :cond_3

    .line 1338
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_0
    :goto_0
    move v4, v3

    .line 1348
    :goto_1
    if-ge v4, v5, :cond_5

    .line 1349
    invoke-virtual {p0, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v6

    .line 1350
    if-gt v2, v4, :cond_1

    if-gt v4, v1, :cond_1

    if-eq v4, v0, :cond_2

    invoke-virtual {p0, v6}, Lcom/android/launcher3/AppsCustomizePagedView;->aa(Landroid/view/View;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1352
    :cond_1
    invoke-virtual {v6, v3, v9}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1348
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1340
    :cond_3
    if-lez v2, :cond_0

    .line 1341
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    .line 1342
    goto :goto_0

    .line 1345
    :cond_4
    add-int/lit8 v0, v2, 0x1

    goto :goto_0

    .line 1356
    :cond_5
    :goto_2
    if-ge v3, v5, :cond_8

    .line 1357
    invoke-virtual {p0, v3}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v4

    .line 1359
    if-gt v2, v3, :cond_7

    if-gt v3, v1, :cond_7

    if-eq v3, v0, :cond_6

    invoke-virtual {p0, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->aa(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1361
    :cond_6
    invoke-virtual {v4}, Landroid/view/View;->getLayerType()I

    move-result v6

    if-eq v6, v8, :cond_7

    .line 1362
    invoke-virtual {v4, v8, v9}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1356
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1366
    :cond_8
    return-void
.end method

.method private y(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 571
    if-nez p1, :cond_0

    .line 573
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    .line 574
    iput-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    .line 576
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    if-nez v1, :cond_1

    .line 578
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yt:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 579
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ys:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 599
    :cond_0
    :goto_0
    iput v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    .line 600
    iput v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    .line 601
    iput-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    .line 602
    invoke-static {}, Lcom/android/launcher3/PagedViewWidget;->jP()V

    .line 603
    return-void

    .line 580
    :cond_1
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 582
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    if-eq v0, v3, :cond_2

    .line 583
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hr()Lyw;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    invoke-virtual {v0, v1}, Lyw;->deleteAppWidgetId(I)V

    .line 587
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ys:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 588
    :cond_3
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 590
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    if-eq v1, v3, :cond_4

    .line 591
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hr()Lyw;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    invoke-virtual {v1, v2}, Lyw;->deleteAppWidgetId(I)V

    .line 595
    :cond_4
    iget-object v0, v0, Lacy;->RR:Landroid/appwidget/AppWidgetHostView;

    .line 596
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final F(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 562
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    if-eqz v0, :cond_0

    .line 564
    invoke-direct {p0, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->y(Z)V

    .line 566
    :cond_0
    new-instance v1, Lacy;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacy;

    invoke-direct {v1, v0}, Lacy;-><init>(Lacy;)V

    iput-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    .line 567
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    iget-object v1, v0, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v2, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/Launcher;Lacy;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, v1, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v3, :cond_1

    iput-object v2, v0, Lacy;->RS:Landroid/os/Bundle;

    .line 568
    :goto_0
    return-void

    .line 567
    :cond_1
    iput v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    new-instance v3, Lry;

    invoke-direct {v3, p0, v1, v2}, Lry;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;Landroid/appwidget/AppWidgetProviderInfo;Landroid/os/Bundle;)V

    iput-object v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yt:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yt:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    new-instance v2, Lrz;

    invoke-direct {v2, p0, v1, v0}, Lrz;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;Landroid/appwidget/AppWidgetProviderInfo;Lacy;)V

    iput-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ys:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ys:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected final G(Landroid/view/View;)Z
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3fa00000    # 1.25f

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 695
    invoke-super {p0, p1}, Lacu;->G(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    :goto_0
    return v11

    .line 697
    :cond_0
    instance-of v0, p1, Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_2

    .line 698
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Lui;)V

    .line 707
    :cond_1
    new-instance v0, Lsa;

    invoke-direct {v0, p0}, Lsa;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;)V

    const-wide/16 v2, 0x96

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/launcher3/AppsCustomizePagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v11, v12

    .line 718
    goto :goto_0

    .line 699
    :cond_2
    instance-of v0, p1, Lcom/android/launcher3/PagedViewWidget;

    if-eqz v0, :cond_1

    .line 700
    iput-boolean v12, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yx:Z

    const v0, 0x7f1100a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_3

    iput-boolean v11, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yx:Z

    move v0, v11

    :goto_1
    if-nez v0, :cond_1

    goto :goto_0

    :cond_3
    const/high16 v7, 0x3f800000    # 1.0f

    instance-of v1, v0, Lacy;

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    if-nez v0, :cond_4

    move v0, v11

    goto :goto_1

    :cond_4
    iget-object v10, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yw:Lacy;

    iget v2, v10, Lacw;->AY:I

    iget v3, v10, Lacw;->AZ:I

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v10, v12}, Lcom/android/launcher3/Workspace;->a(IILwq;Z)[I

    move-result-object v1

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lus;

    invoke-virtual {v9}, Lus;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-int v0, v0

    aget v4, v1, v11

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v9}, Lus;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-int v0, v0

    aget v1, v1, v12

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    new-array v7, v12, [I

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v0

    iget-object v1, v10, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual/range {v0 .. v7}, Lafb;->a(Landroid/appwidget/AppWidgetProviderInfo;IIIILandroid/graphics/Bitmap;[I)Landroid/graphics/Bitmap;

    move-result-object v0

    aget v1, v7, v11

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v3

    invoke-virtual {v3, v2}, Lafb;->bL(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v2, v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v7, v2, v3

    invoke-virtual {v9}, Lus;->getIntrinsicWidth()I

    move-result v2

    if-ge v1, v2, :cond_5

    invoke-virtual {v9}, Lus;->getIntrinsicWidth()I

    move-result v2

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6, v1, v11}, Landroid/graphics/Point;-><init>(II)V

    :cond_5
    move-object v4, v10

    move-object v2, v0

    :goto_2
    instance-of v0, v4, Lacy;

    if-eqz v0, :cond_6

    move-object v0, v4

    check-cast v0, Lacy;

    iget v0, v0, Lacy;->previewImage:I

    if-eqz v0, :cond_8

    :cond_6
    move v0, v12

    :goto_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v1, v3, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hT()V

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v1

    invoke-virtual {v1, v4, v9, v0}, Lcom/android/launcher3/Workspace;->a(Lacw;Landroid/graphics/Bitmap;Z)V

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yg:Lty;

    sget v5, Lty;->Es:I

    move-object v1, v8

    move-object v3, p0

    invoke-virtual/range {v0 .. v7}, Lty;->a(Landroid/view/View;Landroid/graphics/Bitmap;Lui;Ljava/lang/Object;ILandroid/graphics/Point;F)V

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    move v0, v12

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lacx;

    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xn:Lwi;

    iget-object v1, v1, Lacx;->RP:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2, v1}, Lwi;->a(Landroid/content/pm/ActivityInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, v2}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput v12, v0, Lacw;->AZ:I

    iput v12, v0, Lacw;->AY:I

    move-object v4, v0

    goto :goto_2

    :cond_8
    move v0, v11

    goto :goto_3
.end method

.method public final a(Landroid/view/View;Luq;ZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 780
    if-eqz p3, :cond_0

    .line 807
    :goto_0
    return-void

    .line 782
    :cond_0
    invoke-direct {p0, p1, v2, p4}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Landroid/view/View;ZZ)V

    .line 786
    if-nez p4, :cond_2

    .line 788
    instance-of v0, p1, Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_4

    .line 789
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hP()I

    move-result v0

    .line 790
    check-cast p1, Lcom/android/launcher3/Workspace;

    .line 791
    invoke-virtual {p1, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 792
    iget-object v1, p2, Luq;->Ge:Ljava/lang/Object;

    check-cast v1, Lwq;

    .line 793
    if-eqz v0, :cond_4

    .line 794
    invoke-static {v1}, Lcom/android/launcher3/CellLayout;->a(Lwq;)V

    .line 795
    const/4 v3, 0x0

    iget v4, v1, Lwq;->AY:I

    iget v1, v1, Lwq;->AZ:I

    invoke-virtual {v0, v3, v4, v1}, Lcom/android/launcher3/CellLayout;->b([III)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 799
    :goto_1
    if-eqz v0, :cond_1

    .line 800
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Launcher;->S(Z)V

    .line 803
    :cond_1
    iput-boolean v2, p2, Luq;->Gh:Z

    .line 805
    :cond_2
    invoke-direct {p0, p4}, Lcom/android/launcher3/AppsCustomizePagedView;->y(Z)V

    .line 806
    iput-boolean v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yx:Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 795
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/android/launcher3/Launcher;F)V
    .locals 0

    .prologue
    .line 760
    return-void
.end method

.method public final a(Lcom/android/launcher3/Launcher;Lty;)V
    .locals 0

    .prologue
    .line 1393
    iput-object p1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    .line 1394
    iput-object p2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yg:Lty;

    .line 1395
    return-void
.end method

.method public final a(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 1

    .prologue
    .line 748
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yA:Z

    .line 749
    if-eqz p3, :cond_0

    .line 750
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eq()V

    .line 752
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 384
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v4

    .line 385
    iget-object v0, v4, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v5

    .line 388
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 389
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 390
    instance-of v1, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v1, :cond_5

    .line 391
    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 392
    iget-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget-object v7, v4, Lyu;->xo:Lrq;

    if-eqz v7, :cond_1

    iget-object v7, v4, Lyu;->xo:Lrq;

    invoke-virtual {v7, v1}, Lrq;->shouldShowApp(Landroid/content/ComponentName;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v3

    :goto_1
    if-eqz v1, :cond_0

    .line 393
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    if-lez v1, :cond_4

    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    if-lez v1, :cond_4

    .line 397
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher3/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v1

    .line 398
    iget-object v7, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v7, v0}, Lcom/android/launcher3/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v7

    .line 399
    aget v8, v1, v2

    aget v9, v7, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 400
    aget v1, v1, v3

    aget v7, v7, v3

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 401
    iget v7, v5, Ltu;->Dh:F

    float-to-int v7, v7

    if-gt v8, v7, :cond_3

    iget v7, v5, Ltu;->Dg:F

    float-to-int v7, v7

    if-gt v1, v7, :cond_3

    .line 403
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v1, v2

    .line 392
    goto :goto_1

    .line 405
    :cond_3
    const-string v1, "AppsCustomizePagedView"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Widget "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " can not fit on this device ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 409
    :cond_4
    const-string v1, "AppsCustomizePagedView"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Widget "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has invalid dimensions ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 414
    :cond_5
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 417
    :cond_6
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ei()V

    .line 418
    return-void
.end method

.method public final a(Lse;)V
    .locals 2

    .prologue
    .line 878
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    if-ne v0, p1, :cond_0

    sget-object v0, Lse;->yS:Lse;

    if-ne p1, v0, :cond_1

    .line 879
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    if-eq v0, p1, :cond_2

    const/4 v0, 0x0

    .line 880
    :goto_0
    iput-object p1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    .line 881
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->k(IZ)V

    .line 883
    :cond_1
    return-void

    .line 879
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v0

    goto :goto_0
.end method

.method final aN(I)V
    .locals 3

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->setPadding(IIII)V

    .line 266
    return-void
.end method

.method final aO(I)V
    .locals 0

    .prologue
    .line 329
    if-gez p1, :cond_0

    .line 331
    :goto_0
    return-void

    .line 330
    :cond_0
    iput p1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    goto :goto_0
.end method

.method final aS(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1313
    invoke-virtual {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aT(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final aT(I)I
    .locals 1

    .prologue
    .line 1318
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected final aU(I)V
    .locals 0

    .prologue
    .line 1324
    invoke-super {p0, p1}, Lacu;->aU(I)V

    .line 1325
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->et()V

    .line 1326
    return-void
.end method

.method protected final aV(I)I
    .locals 3

    .prologue
    .line 1534
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    .line 1535
    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1536
    add-int/lit8 v2, p1, -0x2

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1537
    return v0
.end method

.method protected final aW(I)I
    .locals 3

    .prologue
    .line 1540
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    .line 1541
    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1542
    add-int/lit8 v2, p1, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1544
    return v0
.end method

.method public final b(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 0

    .prologue
    .line 756
    return-void
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 1415
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1416
    iput-object p1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    .line 1417
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-static {}, Lzi;->iB()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1418
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ei()V

    .line 1420
    :cond_0
    return-void
.end method

.method public final c(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 764
    iput-boolean v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yA:Z

    .line 765
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsg;

    .line 766
    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lsg;Z)V

    goto :goto_0

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 769
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 770
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 772
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 773
    if-nez p3, :cond_2

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QF:Z

    .line 774
    return-void

    :cond_2
    move v0, v1

    .line 773
    goto :goto_2
.end method

.method public final d(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 1433
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1434
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->c(Ljava/util/ArrayList;)V

    .line 1435
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ei()V

    .line 1437
    :cond_0
    return-void
.end method

.method protected final ed()V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0}, Lacu;->ed()V

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QB:Z

    .line 246
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 248
    const v1, 0x7f0c001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lacu;->RL:F

    .line 249
    return-void
.end method

.method public final ee()Lafb;
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yD:Lafb;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lafb;

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-direct {v0, v1}, Lafb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yD:Lafb;

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yD:Lafb;

    return-object v0
.end method

.method final ef()I
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 307
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    if-ne v0, v1, :cond_1

    .line 308
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v2

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v3, Lse;->yR:Lse;

    if-ne v0, v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lrw;

    invoke-virtual {v0}, Lrw;->eZ()Ladg;

    move-result-object v0

    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QA:I

    mul-int/2addr v3, v4

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v0

    if-lez v0, :cond_4

    mul-int v1, v2, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    :goto_0
    move v1, v0

    :cond_0
    :goto_1
    iput v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    .line 310
    :cond_1
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    return v0

    .line 308
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v3, Lse;->yS:Lse;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lacr;

    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    iget v5, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    mul-int/2addr v4, v5

    invoke-virtual {v0}, Lacr;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    mul-int v1, v2, v4

    add-int/2addr v1, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v1, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid ContentType"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final eh()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v3, -0x80000000

    .line 342
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 343
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 344
    iget v2, v0, Ltu;->Ea:I

    iput v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    .line 345
    iget v0, v0, Ltu;->DZ:I

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QA:I

    .line 346
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eg()V

    .line 349
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yk:I

    .line 350
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yl:I

    .line 351
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yk:I

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 352
    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yl:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 353
    iget-object v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yo:Laco;

    invoke-virtual {v3, v0, v2}, Laco;->measure(II)V

    .line 355
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    const v2, 0x7f110244

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->eC()Z

    move-result v2

    .line 356
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    if-gez v0, :cond_0

    move v0, v1

    .line 357
    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->k(IZ)V

    .line 358
    return-void

    .line 356
    :cond_0
    iget-object v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QA:I

    mul-int/2addr v3, v4

    div-int/2addr v0, v3

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v0, v4

    div-int/2addr v0, v3

    goto :goto_0
.end method

.method public final ej()V
    .locals 1

    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yx:Z

    if-nez v0, :cond_0

    .line 608
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->y(Z)V

    .line 610
    :cond_0
    return-void
.end method

.method public final ek()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 812
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Landroid/view/View;ZZ)V

    .line 813
    invoke-direct {p0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->y(Z)V

    .line 814
    iput-boolean v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yx:Z

    .line 815
    return-void
.end method

.method public final el()Z
    .locals 1

    .prologue
    .line 819
    const/4 v0, 0x1

    return v0
.end method

.method public final em()Z
    .locals 1

    .prologue
    .line 824
    const/4 v0, 0x1

    return v0
.end method

.method public final en()Z
    .locals 1

    .prologue
    .line 829
    const/4 v0, 0x0

    return v0
.end method

.method public final eo()F
    .locals 2

    .prologue
    .line 834
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 835
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 836
    iget v1, v0, Ltu;->DN:I

    int-to-float v1, v1

    iget v0, v0, Ltu;->DI:I

    int-to-float v0, v0

    div-float v0, v1, v0

    return v0
.end method

.method public final ep()V
    .locals 4

    .prologue
    .line 846
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eq()V

    .line 847
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v2

    .line 848
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 849
    invoke-virtual {p0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    .line 850
    instance-of v3, v0, Lacr;

    if-eqz v3, :cond_0

    .line 851
    check-cast v0, Lacr;

    invoke-virtual {v0}, Lacr;->eb()V

    .line 852
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QJ:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 848
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 855
    :cond_1
    return-void
.end method

.method public final er()Lse;
    .locals 1

    .prologue
    .line 886
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    return-object v0
.end method

.method public final es()V
    .locals 11

    .prologue
    const/16 v3, 0xff

    const/4 v10, -0x1

    const/high16 v9, -0x80000000

    const/4 v1, 0x0

    .line 1273
    iput-boolean v1, p0, Lcom/android/launcher3/PagedView;->QU:Z

    .line 1275
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->removeAllViews()V

    .line 1276
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eq()V

    .line 1278
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1279
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v2, Lse;->yR:Lse;

    if-ne v0, v2, :cond_2

    move v0, v1

    .line 1280
    :goto_0
    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yp:I

    if-ge v0, v2, :cond_6

    .line 1281
    new-instance v5, Lrw;

    invoke-direct {v5, v4}, Lrw;-><init>(Landroid/content/Context;)V

    .line 1282
    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qz:I

    iget v6, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QA:I

    invoke-virtual {v5, v2, v6}, Lrw;->A(II)V

    const/16 v2, 0x8

    invoke-static {v5, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->b(Landroid/view/ViewGroup;I)V

    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yk:I

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v6, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yl:I

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v2, v6}, Lrw;->measure(II)V

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f02029c

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-boolean v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yy:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v6, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {v5, v6}, Lrw;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-static {v5, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->b(Landroid/view/ViewGroup;I)V

    .line 1283
    new-instance v2, Lacl;

    invoke-direct {v2, v10, v10}, Lacl;-><init>(II)V

    invoke-virtual {p0, v5, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1280
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 1282
    goto :goto_1

    .line 1286
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v2, Lse;->yS:Lse;

    if-ne v0, v2, :cond_5

    move v0, v1

    .line 1287
    :goto_2
    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yq:I

    if-ge v0, v2, :cond_6

    .line 1288
    new-instance v5, Lacr;

    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    iget v6, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    invoke-direct {v5, v4, v2, v6}, Lacr;-><init>(Landroid/content/Context;II)V

    .line 1290
    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yk:I

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yl:I

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f02029d

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    if-eqz v8, :cond_3

    iget-boolean v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yy:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_3
    invoke-virtual {v8, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {v5, v8}, Lacr;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    invoke-virtual {v5, v6, v7}, Lacr;->measure(II)V

    .line 1291
    new-instance v2, Lacl;

    invoke-direct {v2, v10, v10}, Lacl;-><init>(II)V

    invoke-virtual {p0, v5, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1287
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v2, v1

    .line 1290
    goto :goto_3

    .line 1295
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid ContentType"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1298
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QU:Z

    .line 1299
    return-void
.end method

.method protected final eu()V
    .locals 1

    .prologue
    .line 1382
    invoke-super {p0}, Lacu;->eu()V

    .line 1383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->QF:Z

    .line 1386
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    .line 1387
    return-void
.end method

.method public final ev()V
    .locals 4

    .prologue
    .line 1497
    const-string v0, "AppsCustomizePagedView"

    const-string v1, "mApps"

    iget-object v2, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lrr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1498
    const-string v0, "AppsCustomizePagedView"

    const-string v0, "mWidgets"

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " size="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   label=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" previewImage="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resizeMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " configure="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " initialLayout="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " minWidth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " minHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    instance-of v2, v0, Landroid/content/pm/ResolveInfo;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   label=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" icon="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Landroid/content/pm/ResolveInfo;->icon:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1499
    :cond_2
    return-void
.end method

.method public final ew()V
    .locals 0

    .prologue
    .line 1524
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eq()V

    .line 1525
    return-void
.end method

.method protected final ex()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1548
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qf:I

    .line 1549
    :goto_0
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v2, Lse;->yR:Lse;

    if-ne v1, v2, :cond_1

    .line 1553
    const v2, 0x7f0a00c2

    .line 1554
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yp:I

    .line 1562
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1548
    :cond_0
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    goto :goto_0

    .line 1555
    :cond_1
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v2, Lse;->yS:Lse;

    if-ne v1, v2, :cond_2

    .line 1556
    const v2, 0x7f0a00c3

    .line 1557
    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yq:I

    goto :goto_1

    .line 1559
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid ContentType"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f(IZ)V
    .locals 11

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v1, Lse;->yS:Lse;

    if-ne v0, v1, :cond_6

    .line 1304
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    mul-int v5, v0, v1

    invoke-virtual {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aS(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lacr;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yk:I

    invoke-virtual {v4}, Lacr;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v4}, Lacr;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    div-int v2, v0, v1

    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yl:I

    invoke-virtual {v4}, Lacr;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v4}, Lacr;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yn:I

    div-int v3, v0, v1

    mul-int v1, p1, v5

    move v0, v1

    :goto_0
    add-int v6, v1, v5

    iget-object v8, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-ge v0, v6, :cond_0

    iget-object v6, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Lacr;->jL()I

    move-result v0

    invoke-virtual {v4, v0}, Lacr;->setColumnCount(I)V

    const/4 v0, 0x0

    move v5, v0

    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f040020

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/PagedViewWidget;

    instance-of v6, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v6, :cond_4

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v6, Lacy;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v6, v0, v8, v9}, Lacy;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v8, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v8, v0}, Lcom/android/launcher3/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v8, v9

    iput v9, v6, Lacw;->AY:I

    const/4 v9, 0x1

    aget v9, v8, v9

    iput v9, v6, Lacw;->AZ:I

    iget-object v9, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v9, v0}, Lcom/android/launcher3/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v9

    const/4 v10, 0x0

    aget v10, v9, v10

    iput v10, v6, Lacw;->JB:I

    const/4 v10, 0x1

    aget v9, v9, v10

    iput v9, v6, Lacw;->JC:I

    const/4 v9, -0x1

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v10

    invoke-virtual {v1, v0, v9, v8, v10}, Lcom/android/launcher3/PagedViewWidget;->a(Landroid/appwidget/AppWidgetProviderInfo;I[ILafb;)V

    invoke-virtual {v1, v6}, Lcom/android/launcher3/PagedViewWidget;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, p0}, Lcom/android/launcher3/PagedViewWidget;->a(Lact;)V

    :cond_1
    :goto_2
    invoke-virtual {v1, p0}, Lcom/android/launcher3/PagedViewWidget;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p0}, Lcom/android/launcher3/PagedViewWidget;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v1, p0}, Lcom/android/launcher3/PagedViewWidget;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, p0}, Lcom/android/launcher3/PagedViewWidget;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    rem-int v0, v5, v0

    iget v6, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    div-int v6, v5, v6

    if-lez v0, :cond_2

    const v8, 0x7f1100a4

    invoke-virtual {v1, v8}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget v8, p0, Lcom/android/launcher3/AppsCustomizePagedView;->ym:I

    add-int/lit8 v8, v8, -0x1

    if-ge v0, v8, :cond_3

    const v8, 0x7f1100a6

    invoke-virtual {v1, v8}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    new-instance v8, Landroid/widget/GridLayout$LayoutParams;

    sget-object v9, Landroid/widget/GridLayout;->START:Landroid/widget/GridLayout$Alignment;

    invoke-static {v6, v9}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v6

    sget-object v9, Landroid/widget/GridLayout;->TOP:Landroid/widget/GridLayout$Alignment;

    invoke-static {v0, v9}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v0

    invoke-direct {v8, v6, v0}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    iput v2, v8, Landroid/widget/GridLayout$LayoutParams;->width:I

    iput v3, v8, Landroid/widget/GridLayout$LayoutParams;->height:I

    const v0, 0x800033

    invoke-virtual {v8, v0}, Landroid/widget/GridLayout$LayoutParams;->setGravity(I)V

    invoke-virtual {v4, v1, v8}, Lacr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_1

    :cond_4
    instance-of v6, v0, Landroid/content/pm/ResolveInfo;

    if-eqz v6, :cond_1

    check-cast v0, Landroid/content/pm/ResolveInfo;

    new-instance v6, Lacx;

    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-direct {v6, v8}, Lacx;-><init>(Landroid/content/pm/ActivityInfo;)V

    const/4 v8, 0x1

    iput v8, v6, Lacw;->Jz:I

    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v10, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v8, v6, Lacw;->xr:Landroid/content/ComponentName;

    iget-object v8, p0, Lcom/android/launcher3/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v9

    invoke-virtual {v1, v8, v0, v9}, Lcom/android/launcher3/PagedViewWidget;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lafb;)V

    invoke-virtual {v1, v6}, Lcom/android/launcher3/PagedViewWidget;->setTag(Ljava/lang/Object;)V

    goto :goto_2

    :cond_5
    new-instance v0, Lsd;

    move-object v1, p0

    move v5, p2

    move v6, p1

    invoke-direct/range {v0 .. v7}, Lsd;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;IILacr;ZILjava/util/ArrayList;)V

    invoke-virtual {v4, v0}, Lacr;->f(Ljava/lang/Runnable;)V

    .line 1308
    :goto_3
    return-void

    .line 1306
    :cond_6
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->aP(I)V

    goto :goto_3
.end method

.method public final f(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 1462
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1463
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->e(Ljava/util/ArrayList;)V

    .line 1464
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ei()V

    .line 1466
    :cond_0
    return-void
.end method

.method protected final g(III)V
    .locals 5

    .prologue
    .line 890
    invoke-super {p0, p1, p2, p3}, Lacu;->g(III)V

    .line 893
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 894
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 895
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    .line 896
    iget v2, v0, Lrv;->yb:I

    .line 897
    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qf:I

    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    if-le v3, v4, :cond_0

    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    if-ge v2, v3, :cond_1

    :cond_0
    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qf:I

    iget v4, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    if-ge v3, v4, :cond_2

    iget v3, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    if-gt v2, v3, :cond_2

    .line 899
    :cond_1
    invoke-direct {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->aR(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lrv;->setThreadPriority(I)V

    goto :goto_0

    .line 901
    :cond_2
    const/16 v2, 0x13

    invoke-virtual {v0, v2}, Lrv;->setThreadPriority(I)V

    goto :goto_0

    .line 904
    :cond_3
    return-void
.end method

.method public final g(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 1471
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1472
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->e(Ljava/util/ArrayList;)V

    .line 1473
    invoke-direct {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->c(Ljava/util/ArrayList;)V

    .line 1474
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ei()V

    .line 1476
    :cond_0
    return-void
.end method

.method protected final h(F)V
    .locals 0

    .prologue
    .line 1369
    invoke-virtual {p0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->u(F)V

    .line 1370
    return-void
.end method

.method protected final i(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 479
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 444
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kV()Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/android/launcher3/PagedViewWidget;

    if-nez v0, :cond_1

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yz:Landroid/widget/Toast;

    if-eqz v0, :cond_2

    .line 450
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yz:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 452
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a008a

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yz:Landroid/widget/Toast;

    .line 454
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yz:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 457
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 458
    const v0, 0x7f1100a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 459
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v2

    .line 460
    const-string v3, "translationY"

    new-array v4, v7, [F

    aput v1, v4, v6

    invoke-static {v0, v3, v4}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 461
    const-wide/16 v4, 0x7d

    invoke-virtual {v1, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 462
    const-string v3, "translationY"

    new-array v4, v7, [F

    const/4 v5, 0x0

    aput v5, v4, v6

    invoke-static {v0, v3, v4}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 463
    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 464
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 465
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 466
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 841
    invoke-super {p0}, Lacu;->onDetachedFromWindow()V

    .line 842
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->eq()V

    .line 843
    return-void
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    .line 252
    invoke-super {p0}, Lacu;->onFinishInflate()V

    .line 254
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 255
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 256
    iget v1, v0, Ltu;->Dv:I

    iget v2, v0, Ltu;->Dv:I

    mul-int/lit8 v2, v2, 0x2

    iget v3, v0, Ltu;->Dv:I

    iget v0, v0, Ltu;->Dv:I

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->setPadding(IIII)V

    .line 258
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 470
    invoke-static {p1, p2, p3}, Luy;->a(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 361
    invoke-super/range {p0 .. p5}, Lacu;->onLayout(ZIIII)V

    .line 363
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->je()Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yi:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yj:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 365
    new-instance v0, Lrx;

    invoke-direct {v0, p0}, Lrx;-><init>(Lcom/android/launcher3/AppsCustomizePagedView;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    .line 381
    :cond_1
    return-void
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 1480
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yh:I

    .line 1482
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yf:Lse;

    sget-object v1, Lse;->yR:Lse;

    if-eq v0, v1, :cond_0

    .line 1483
    sget-object v0, Lse;->yR:Lse;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lse;)V

    .line 1486
    :cond_0
    iget v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->Qc:I

    if-eqz v0, :cond_1

    .line 1487
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->bD(I)V

    .line 1489
    :cond_1
    return-void
.end method

.method public final x(Z)V
    .locals 1

    .prologue
    .line 421
    if-eqz p1, :cond_1

    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yE:Z

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yE:Z

    .line 425
    iget-boolean v0, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yF:Z

    if-eqz v0, :cond_0

    .line 426
    invoke-direct {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->ei()V

    goto :goto_0
.end method

.method public final z(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 937
    iput-boolean p1, p0, Lcom/android/launcher3/AppsCustomizePagedView;->yy:Z

    .line 938
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v3

    move v2, v1

    .line 939
    :goto_0
    if-ge v2, v3, :cond_2

    .line 940
    invoke-virtual {p0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 941
    if-eqz v4, :cond_0

    .line 942
    if-eqz p1, :cond_1

    const/16 v0, 0xff

    :goto_1
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 939
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 942
    goto :goto_1

    .line 945
    :cond_2
    return-void
.end method
