.class public Lcom/android/launcher3/BubbleTextView;
.super Landroid/widget/TextView;
.source "PG"


# static fields
.field private static zq:Landroid/util/SparseArray;


# instance fields
.field private zA:Ltf;

.field private zr:Lwf;

.field private zs:Landroid/graphics/Bitmap;

.field private zt:F

.field private zu:I

.field private final zv:Z

.field private zw:Z

.field private final zx:Landroid/graphics/drawable/Drawable;

.field private zy:Z

.field private zz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/android/launcher3/BubbleTextView;->zq:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/launcher3/BubbleTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/BubbleTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    sget-object v0, Ladb;->Ss:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 83
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/launcher3/BubbleTextView;->zv:Z

    .line 84
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 86
    iget-boolean v0, p0, Lcom/android/launcher3/BubbleTextView;->zv:Z

    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    .line 89
    invoke-virtual {p0, v3}, Lcom/android/launcher3/BubbleTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 93
    :goto_0
    new-instance v0, Ltf;

    invoke-direct {v0, p0}, Ltf;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zA:Ltf;

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lwf;->k(Landroid/content/Context;)Lwf;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zr:Lwf;

    iget-boolean v0, p0, Lcom/android/launcher3/BubbleTextView;->zv:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x40800000    # 4.0f

    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v3, -0x23000000

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/launcher3/BubbleTextView;->setShadowLayer(FFFI)V

    .line 94
    :cond_0
    return-void

    .line 91
    :cond_1
    iput-object v3, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private eF()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 190
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    .line 191
    instance-of v2, v0, Lus;

    if-eqz v2, :cond_1

    .line 192
    check-cast v0, Lus;

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->isPressed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/launcher3/BubbleTextView;->zy:Z

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Lus;->setPressed(Z)V

    .line 194
    :cond_1
    return-void

    .line 192
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private eH()Landroid/content/res/Resources$Theme;
    .locals 4

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 409
    if-eqz v0, :cond_1

    instance-of v1, v0, Ladh;

    if-eqz v1, :cond_1

    check-cast v0, Ladh;

    iget-wide v0, v0, Ladh;->JA:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const v0, 0x7f090042

    move v1, v0

    .line 412
    :goto_0
    sget-object v0, Lcom/android/launcher3/BubbleTextView;->zq:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources$Theme;

    .line 413
    if-nez v0, :cond_0

    .line 414
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 415
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 416
    sget-object v2, Lcom/android/launcher3/BubbleTextView;->zq:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 418
    :cond_0
    return-object v0

    .line 409
    :cond_1
    const v0, 0x7f090041

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method final B(Z)V
    .locals 3

    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/launcher3/BubbleTextView;->zy:Z

    .line 234
    if-nez p1, :cond_0

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    .line 239
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Ladg;

    if-eqz v0, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 241
    iget-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/android/launcher3/BubbleTextView;->zr:Lwf;

    iget v2, v2, Lwf;->IV:I

    invoke-virtual {v0, p0, v1, v2}, Lcom/android/launcher3/CellLayout;->a(Lcom/android/launcher3/BubbleTextView;Landroid/graphics/Bitmap;I)V

    .line 244
    :cond_1
    invoke-direct {p0}, Lcom/android/launcher3/BubbleTextView;->eF()V

    .line 245
    return-void
.end method

.method public final C(Z)V
    .locals 2

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    if-eqz p1, :cond_0

    .line 357
    iget v0, p0, Lcom/android/launcher3/BubbleTextView;->zu:I

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 361
    :goto_0
    return-void

    .line 359
    :cond_0
    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final D(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 381
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ladh;

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 383
    invoke-virtual {v0}, Ladh;->kg()Z

    move-result v1

    .line 384
    if-eqz v1, :cond_2

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ladh;->bH(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ladh;->kh()I

    move-result v0

    move v1, v0

    .line 388
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 389
    const/4 v0, 0x1

    aget-object v0, v4, v0

    .line 390
    if-eqz v0, :cond_0

    .line 392
    instance-of v3, v0, Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v3, :cond_3

    .line 393
    check-cast v0, Lcom/android/launcher3/PreloadIconDrawable;

    .line 399
    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/launcher3/PreloadIconDrawable;->setLevel(I)Z

    .line 400
    if-eqz p1, :cond_0

    .line 401
    invoke-virtual {v0}, Lcom/android/launcher3/PreloadIconDrawable;->jY()V

    .line 405
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 384
    goto :goto_0

    :cond_2
    const/16 v0, 0x64

    move v1, v0

    goto :goto_0

    .line 395
    :cond_3
    new-instance v3, Lcom/android/launcher3/PreloadIconDrawable;

    invoke-direct {p0}, Lcom/android/launcher3/BubbleTextView;->eH()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-direct {v3, v0, v5}, Lcom/android/launcher3/PreloadIconDrawable;-><init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources$Theme;)V

    .line 396
    aget-object v0, v4, v2

    const/4 v2, 0x2

    aget-object v2, v4, v2

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-virtual {p0, v0, v3, v2, v4}, Lcom/android/launcher3/BubbleTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v0, v3

    goto :goto_1
.end method

.method public final a(Ladh;Lwi;Z)V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/launcher3/BubbleTextView;->a(Ladh;Lwi;ZZ)V

    .line 117
    return-void
.end method

.method public final a(Ladh;Lwi;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    invoke-virtual {p1, p2}, Ladh;->b(Lwi;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 122
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v1

    .line 124
    invoke-static {v0}, Ladp;->i(Landroid/graphics/Bitmap;)Lus;

    move-result-object v0

    .line 125
    iget-boolean v2, p1, Ladh;->SV:Z

    invoke-virtual {v0, v2}, Lus;->M(Z)V

    .line 127
    invoke-virtual {p0, v3, v0, v3, v3}, Lcom/android/launcher3/BubbleTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 128
    if-eqz p3, :cond_0

    .line 129
    iget-object v0, v1, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 130
    iget v0, v0, Ltu;->DK:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setCompoundDrawablePadding(I)V

    .line 132
    :cond_0
    iget-object v0, p1, Ladh;->Jk:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p1, Ladh;->Jk:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 135
    :cond_1
    iget-object v0, p1, Ladh;->title:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {p0, p1}, Lcom/android/launcher3/BubbleTextView;->setTag(Ljava/lang/Object;)V

    .line 138
    if-nez p4, :cond_2

    invoke-virtual {p1}, Ladh;->kg()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 139
    :cond_2
    invoke-virtual {p0, p4}, Lcom/android/launcher3/BubbleTextView;->D(Z)V

    .line 141
    :cond_3
    return-void
.end method

.method public final b(Lrr;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 144
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 145
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 147
    iget-object v1, p1, Lrr;->xq:Landroid/graphics/Bitmap;

    invoke-static {v1}, Ladp;->i(Landroid/graphics/Bitmap;)Lus;

    move-result-object v1

    .line 148
    iget v2, v0, Ltu;->DN:I

    iget v3, v0, Ltu;->DN:I

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 149
    invoke-virtual {p0, v4, v1, v4, v4}, Lcom/android/launcher3/BubbleTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 150
    iget v0, v0, Ltu;->DK:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setCompoundDrawablePadding(I)V

    .line 151
    iget-object v0, p1, Lrr;->title:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p1, Lrr;->Jk:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p1, Lrr;->Jk:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 155
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/launcher3/BubbleTextView;->setTag(Ljava/lang/Object;)V

    .line 156
    return-void
.end method

.method public cancelLongPress()V
    .locals 1

    .prologue
    .line 375
    invoke-super {p0}, Landroid/widget/TextView;->cancelLongPress()V

    .line 377
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    .line 378
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 280
    iget-boolean v0, p0, Lcom/android/launcher3/BubbleTextView;->zv:Z

    if-nez v0, :cond_0

    .line 281
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 321
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    .line 286
    if-eqz v0, :cond_2

    .line 287
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getScrollX()I

    move-result v1

    .line 288
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getScrollY()I

    move-result v2

    .line 290
    iget-boolean v3, p0, Lcom/android/launcher3/BubbleTextView;->zw:Z

    if-eqz v3, :cond_1

    .line 291
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v7, v7, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 292
    iput-boolean v7, p0, Lcom/android/launcher3/BubbleTextView;->zw:Z

    .line 295
    :cond_1
    or-int v3, v1, v2

    if-nez v3, :cond_3

    .line 296
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 305
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getCurrentTextColor()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 306
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->clearShadowLayer()V

    .line 307
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 298
    :cond_3
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 299
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 300
    neg-int v0, v1

    int-to-float v0, v0

    neg-int v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_1

    .line 312
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v3, -0x23000000

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 313
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 314
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 315
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getScrollX()I

    move-result v0

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getExtendedPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getWidth()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getHeight()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 318
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/high16 v1, 0x3fe00000    # 1.75f

    const/high16 v2, -0x34000000    # -3.3554432E7f

    invoke-virtual {v0, v1, v6, v6, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 319
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 320
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method final eG()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 248
    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->setPressed(Z)V

    .line 249
    invoke-virtual {p0, v0}, Lcom/android/launcher3/BubbleTextView;->B(Z)V

    .line 250
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 325
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    .line 327
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 330
    instance-of v1, v0, Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v1, :cond_1

    .line 331
    check-cast v0, Lcom/android/launcher3/PreloadIconDrawable;

    invoke-direct {p0}, Lcom/android/launcher3/BubbleTextView;->eH()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PreloadIconDrawable;->applyTheme(Landroid/content/res/Resources$Theme;)V

    .line 333
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/BubbleTextView;->zt:F

    .line 334
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 338
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 339
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 340
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/TextView;->onFinishInflate()V

    .line 100
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 101
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 102
    const/4 v1, 0x0

    iget v0, v0, Ltu;->DJ:I

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/BubbleTextView;->setTextSize(IF)V

    .line 103
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 254
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zr:Lwf;

    invoke-virtual {v0, p0}, Lwf;->a(Lcom/android/launcher3/BubbleTextView;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    .line 259
    :cond_0
    const/4 v0, 0x1

    .line 261
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/BubbleTextView;->zz:Z

    .line 270
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 272
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    .line 273
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher3/BubbleTextView;->zz:Z

    .line 274
    invoke-direct {p0}, Lcom/android/launcher3/BubbleTextView;->eF()V

    .line 275
    return v0
.end method

.method protected onSetAlpha(I)Z
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 200
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 202
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 207
    :pswitch_0
    iget-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 208
    iget-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zr:Lwf;

    invoke-virtual {v1, p0}, Lwf;->a(Lcom/android/launcher3/BubbleTextView;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zA:Ltf;

    invoke-virtual {v1}, Ltf;->fo()V

    goto :goto_0

    .line 217
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->isPressed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 218
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zs:Landroid/graphics/Bitmap;

    .line 221
    :cond_2
    iget-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zA:Ltf;

    invoke-virtual {v1}, Ltf;->cancelLongPress()V

    goto :goto_0

    .line 224
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/android/launcher3/BubbleTextView;->zt:F

    invoke-static {p0, v1, v2, v3}, Ladp;->a(Landroid/view/View;FFF)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/android/launcher3/BubbleTextView;->zA:Ltf;

    invoke-virtual {v1}, Ltf;->cancelLongPress()V

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected setFrame(IIII)Z
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getLeft()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getRight()I

    move-result v0

    if-ne v0, p3, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getTop()I

    move-result v0

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/BubbleTextView;->getBottom()I

    move-result v0

    if-eq v0, p4, :cond_1

    .line 162
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/BubbleTextView;->zw:Z

    .line 164
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setFrame(IIII)Z

    move-result v0

    return v0
.end method

.method public setPressed(Z)V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0, p1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 184
    iget-boolean v0, p0, Lcom/android/launcher3/BubbleTextView;->zz:Z

    if-nez v0, :cond_0

    .line 185
    invoke-direct {p0}, Lcom/android/launcher3/BubbleTextView;->eF()V

    .line 187
    :cond_0
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 174
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 175
    check-cast v0, Lwq;

    invoke-static {v0}, Lzi;->e(Lwq;)V

    .line 177
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 178
    return-void
.end method

.method public setTextColor(I)V
    .locals 0

    .prologue
    .line 344
    iput p1, p0, Lcom/android/launcher3/BubbleTextView;->zu:I

    .line 345
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 346
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 350
    invoke-virtual {p1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/BubbleTextView;->zu:I

    .line 351
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 352
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/launcher3/BubbleTextView;->zx:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
