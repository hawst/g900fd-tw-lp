.class public Lcom/android/launcher3/DragLayer;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field private Ae:F

.field private EQ:I

.field private ER:I

.field private final ES:Ljava/util/ArrayList;

.field private ET:Lrs;

.field private EU:Landroid/animation/ValueAnimator;

.field private EV:Landroid/animation/ValueAnimator;

.field private EW:Landroid/animation/TimeInterpolator;

.field private EX:Luj;

.field private EY:I

.field private EZ:Landroid/view/View;

.field private Fa:Z

.field private Fb:Landroid/graphics/Rect;

.field private Fc:Lug;

.field private Fd:Landroid/view/View;

.field private Fe:I

.field private Ff:I

.field private Fg:Z

.field private Fh:Z

.field private Fi:Landroid/graphics/drawable/Drawable;

.field private Fj:Landroid/graphics/drawable/Drawable;

.field private Fk:Landroid/graphics/drawable/Drawable;

.field private Fl:Landroid/graphics/drawable/Drawable;

.field private xZ:Lcom/android/launcher3/Launcher;

.field private final yW:Landroid/graphics/Rect;

.field private yg:Lty;

.field private zT:[I

.field private zx:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 99
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->ES:Ljava/util/ArrayList;

    .line 59
    iput-object v2, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    .line 60
    iput-object v2, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    .line 61
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->EW:Landroid/animation/TimeInterpolator;

    .line 62
    iput-object v2, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    .line 63
    iput v3, p0, Lcom/android/launcher3/DragLayer;->EY:I

    .line 64
    iput-object v2, p0, Lcom/android/launcher3/DragLayer;->EZ:Landroid/view/View;

    .line 66
    iput-boolean v3, p0, Lcom/android/launcher3/DragLayer;->Fa:Z

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->Fb:Landroid/graphics/Rect;

    .line 74
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->yW:Landroid/graphics/Rect;

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/DragLayer;->Ff:I

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/DragLayer;->Ae:F

    .line 102
    invoke-virtual {p0, v3}, Lcom/android/launcher3/DragLayer;->setMotionEventSplittingEnabled(Z)V

    .line 103
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/DragLayer;->setChildrenDrawingOrderEnabled(Z)V

    .line 104
    invoke-virtual {p0, p0}, Lcom/android/launcher3/DragLayer;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 106
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 107
    const v1, 0x7f02027a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/DragLayer;->Fi:Landroid/graphics/drawable/Drawable;

    .line 108
    const v1, 0x7f02027c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/DragLayer;->Fj:Landroid/graphics/drawable/Drawable;

    .line 109
    const v1, 0x7f02027b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/DragLayer;->Fk:Landroid/graphics/drawable/Drawable;

    .line 110
    const v1, 0x7f02027d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/DragLayer;->Fl:Landroid/graphics/drawable/Drawable;

    .line 111
    const v1, 0x7f020006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->zx:Landroid/graphics/drawable/Drawable;

    .line 112
    return-void
.end method

.method private L(Z)V
    .locals 5

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 285
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    if-eqz p1, :cond_1

    const v1, 0x7f0a00d8

    .line 287
    :goto_0
    const/16 v2, 0x8

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 289
    invoke-virtual {p0, v2}, Lcom/android/launcher3/DragLayer;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 290
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 293
    :cond_0
    return-void

    .line 286
    :cond_1
    const v1, 0x7f0a00d7

    goto :goto_0
.end method

.method public static synthetic a(Lcom/android/launcher3/DragLayer;)Landroid/view/View;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EZ:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/android/launcher3/DragLayer;Luj;)Luj;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    return-object v0
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 160
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 161
    instance-of v1, p0, Lwm;

    if-eqz v1, :cond_0

    move-object v1, p0

    .line 162
    check-cast v1, Lwm;

    invoke-interface {v1, p1}, Lwm;->b(Landroid/graphics/Rect;)V

    .line 169
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    return-void

    .line 164
    :cond_0
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 165
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 166
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 167
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;Z)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 189
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 190
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 191
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    .line 193
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->ES:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrs;

    .line 194
    invoke-virtual {v0, v3}, Lrs;->getHitRect(Landroid/graphics/Rect;)V

    .line 195
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 196
    invoke-virtual {v0}, Lrs;->getLeft()I

    move-result v7

    sub-int v7, v4, v7

    invoke-virtual {v0}, Lrs;->getTop()I

    move-result v8

    sub-int v8, v5, v8

    invoke-virtual {v0, v7, v8}, Lrs;->x(II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 197
    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->ET:Lrs;

    .line 198
    iput v4, p0, Lcom/android/launcher3/DragLayer;->EQ:I

    .line 199
    iput v5, p0, Lcom/android/launcher3/DragLayer;->ER:I

    .line 200
    invoke-virtual {p0, v1}, Lcom/android/launcher3/DragLayer;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v1

    .line 221
    :goto_0
    return v0

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v4

    .line 207
    if-eqz v4, :cond_4

    if-eqz p2, :cond_4

    .line 208
    invoke-virtual {v4}, Lcom/android/launcher3/Folder;->gp()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 209
    invoke-virtual {v4}, Lcom/android/launcher3/Folder;->gr()Landroid/view/View;

    move-result-object v0

    iget-object v5, p0, Lcom/android/launcher3/DragLayer;->Fb:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v5}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fb:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 210
    invoke-virtual {v4}, Lcom/android/launcher3/Folder;->gq()V

    move v0, v1

    .line 211
    goto :goto_0

    :cond_2
    move v0, v2

    .line 209
    goto :goto_1

    .line 215
    :cond_3
    invoke-virtual {p0, v4, v3}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 216
    invoke-direct {p0, v4, p1}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/Folder;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 217
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hH()V

    move v0, v1

    .line 218
    goto :goto_0

    :cond_4
    move v0, v2

    .line 221
    goto :goto_0
.end method

.method private a(Lcom/android/launcher3/Folder;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fb:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 182
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fb:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x1

    .line 185
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/android/launcher3/DragLayer;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/android/launcher3/DragLayer;->EY:I

    return v0
.end method

.method public static synthetic c(Lcom/android/launcher3/DragLayer;)Luj;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    return-object v0
.end method

.method public static synthetic d(Lcom/android/launcher3/DragLayer;)V
    .locals 4

    .prologue
    .line 46
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    new-instance v1, Lue;

    invoke-direct {v1, p0}, Lue;-><init>(Lcom/android/launcher3/DragLayer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    new-instance v1, Luf;

    invoke-direct {v1, p0}, Luf;-><init>(Lcom/android/launcher3/DragLayer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static synthetic e(Lcom/android/launcher3/DragLayer;)Lty;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    return-object v0
.end method

.method private fO()V
    .locals 3

    .prologue
    .line 829
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/DragLayer;->Fe:I

    .line 830
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getChildCount()I

    move-result v1

    .line 831
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 832
    invoke-virtual {p0, v0}, Lcom/android/launcher3/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Luj;

    if-eqz v2, :cond_0

    .line 833
    iput v0, p0, Lcom/android/launcher3/DragLayer;->Fe:I

    .line 831
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 836
    :cond_1
    iput v1, p0, Lcom/android/launcher3/DragLayer;->Ff:I

    .line 837
    return-void
.end method


# virtual methods
.method public final N(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 146
    new-instance v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/android/launcher3/DragLayer$LayoutParams;-><init>(II)V

    .line 147
    iput-object p1, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    .line 148
    invoke-virtual {p0, p1, v0}, Lcom/android/launcher3/DragLayer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 153
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Rect;)F
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 371
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    aput v4, v0, v4

    .line 372
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    aput v4, v0, v5

    .line 373
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    invoke-static {p1, p0, v0, v4}, Ladp;->a(Landroid/view/View;Landroid/view/View;[IZ)F

    move-result v0

    .line 375
    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    aget v1, v1, v4

    iget-object v2, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    aget v2, v2, v5

    iget-object v3, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, Lcom/android/launcher3/DragLayer;->zT:[I

    aget v4, v4, v5

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 378
    return v0
.end method

.method public final a(Landroid/view/View;[I)F
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 382
    aput v1, p2, v1

    .line 383
    const/4 v0, 0x1

    aput v1, p2, v0

    .line 384
    invoke-static {p1, p0, p2, v1}, Ladp;->a(Landroid/view/View;Landroid/view/View;[IZ)F

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/View;[IZ)F
    .locals 1

    .prologue
    .line 405
    invoke-static {p1, p0, p2, p3}, Ladp;->a(Landroid/view/View;Landroid/view/View;[IZ)F

    move-result v0

    return v0
.end method

.method public final a(Lcom/android/launcher3/Launcher;Lty;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    .line 116
    iput-object p2, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    .line 117
    return-void
.end method

.method public final a(Lug;)V
    .locals 0

    .prologue
    .line 957
    iput-object p1, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    .line 958
    return-void
.end method

.method public final a(Luj;IIIIFFFFFLjava/lang/Runnable;IILandroid/view/View;)V
    .locals 17

    .prologue
    .line 619
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredWidth()I

    move-result v2

    add-int v2, v2, p2

    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredHeight()I

    move-result v3

    add-int v3, v3, p3

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 621
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredWidth()I

    move-result v2

    add-int v2, v2, p4

    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredHeight()I

    move-result v3

    add-int v3, v3, p5

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 622
    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v6, p6

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p13

    move-object/from16 v14, p11

    move/from16 v15, p12

    move-object/from16 v16, p14

    invoke-virtual/range {v2 .. v16}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 624
    return-void
.end method

.method public final a(Luj;Landroid/animation/ValueAnimator$AnimatorUpdateListener;ILandroid/animation/TimeInterpolator;Ljava/lang/Runnable;ILandroid/view/View;)V
    .locals 4

    .prologue
    .line 722
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EV:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 726
    :cond_1
    iput-object p1, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    .line 727
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    invoke-virtual {v0}, Luj;->ge()V

    .line 728
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    invoke-virtual {v0}, Luj;->gf()V

    .line 731
    if-eqz p7, :cond_2

    .line 732
    invoke-virtual {p7}, Landroid/view/View;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/DragLayer;->EY:I

    .line 734
    :cond_2
    iput-object p7, p0, Lcom/android/launcher3/DragLayer;->EZ:Landroid/view/View;

    .line 737
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    .line 738
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 739
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 740
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 741
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 742
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    new-instance v1, Lud;

    invoke-direct {v1, p0, p5, p6}, Lud;-><init>(Lcom/android/launcher3/DragLayer;Ljava/lang/Runnable;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 759
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 760
    return-void

    .line 740
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(Luj;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V
    .locals 17

    .prologue
    .line 654
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v3, v2

    .line 656
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 657
    const v2, 0x7f0c0025

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v5, v2

    .line 660
    if-gez p9, :cond_1

    .line 661
    const v2, 0x7f0c0021

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 662
    cmpg-float v6, v3, v5

    if-gez v6, :cond_0

    .line 663
    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/DragLayer;->EW:Landroid/animation/TimeInterpolator;

    div-float/2addr v3, v5

    invoke-interface {v6, v3}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 665
    :cond_0
    const v3, 0x7f0c0020

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result p9

    .line 669
    :cond_1
    const/4 v2, 0x0

    .line 670
    if-eqz p11, :cond_2

    if-nez p10, :cond_3

    .line 671
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/DragLayer;->EW:Landroid/animation/TimeInterpolator;

    move-object/from16 v16, v2

    .line 675
    :goto_0
    invoke-virtual/range {p1 .. p1}, Luj;->getAlpha()F

    move-result v13

    .line 676
    invoke-virtual/range {p1 .. p1}, Luj;->getScaleX()F

    move-result v8

    .line 677
    new-instance v2, Luc;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p11

    move-object/from16 v6, p10

    move/from16 v7, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move/from16 v12, p4

    move-object/from16 v14, p2

    move-object/from16 v15, p3

    invoke-direct/range {v2 .. v15}, Luc;-><init>(Lcom/android/launcher3/DragLayer;Luj;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;FFFFFFFLandroid/graphics/Rect;Landroid/graphics/Rect;)V

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object v5, v2

    move/from16 v6, p9

    move-object/from16 v7, v16

    move-object/from16 v8, p12

    move/from16 v9, p13

    move-object/from16 v10, p14

    .line 714
    invoke-virtual/range {v3 .. v10}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/animation/ValueAnimator$AnimatorUpdateListener;ILandroid/animation/TimeInterpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 716
    return-void

    :cond_3
    move-object/from16 v16, v2

    goto :goto_0
.end method

.method public final a(Luj;Landroid/view/View;ILjava/lang/Runnable;Landroid/view/View;)V
    .locals 18

    .prologue
    .line 550
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Ladg;

    .line 551
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 552
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ladg;->ad(Landroid/view/View;)V

    .line 554
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 555
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 557
    const/4 v3, 0x2

    new-array v5, v3, [I

    .line 558
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getScaleX()F

    move-result v7

    .line 559
    const/4 v3, 0x0

    iget v8, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v7

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    aput v8, v5, v3

    .line 560
    const/4 v3, 0x1

    iget v4, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v7

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    float-to-int v8, v8

    add-int/2addr v4, v8

    aput v4, v5, v3

    .line 564
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;[I)F

    move-result v3

    .line 567
    mul-float v4, v3, v7

    .line 568
    const/4 v3, 0x0

    aget v7, v5, v3

    .line 569
    const/4 v3, 0x1

    aget v5, v5, v3

    .line 571
    move-object/from16 v0, p2

    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_1

    move-object/from16 v3, p2

    .line 572
    check-cast v3, Landroid/widget/TextView;

    .line 575
    invoke-virtual/range {p1 .. p1}, Luj;->eo()F

    move-result v8

    div-float v12, v4, v8

    .line 580
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v12

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v3, v5

    .line 581
    int-to-float v3, v3

    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v8, v12

    mul-float/2addr v5, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v5, v8

    sub-float/2addr v3, v5

    float-to-int v3, v3

    .line 582
    invoke-virtual/range {p1 .. p1}, Luj;->fY()Landroid/graphics/Point;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 583
    invoke-virtual/range {p1 .. p1}, Luj;->fY()Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    mul-float/2addr v5, v12

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int/2addr v3, v5

    .line 586
    :cond_0
    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredWidth()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v4, v8

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int v4, v5, v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v7, v4

    move v8, v3

    .line 600
    :goto_0
    iget v5, v6, Landroid/graphics/Rect;->left:I

    .line 601
    iget v6, v6, Landroid/graphics/Rect;->top:I

    .line 602
    const/4 v3, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 603
    new-instance v14, Lub;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-direct {v14, v0, v1, v2}, Lub;-><init>(Lcom/android/launcher3/DragLayer;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 611
    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move v13, v12

    move/from16 v16, p3

    move-object/from16 v17, p5

    invoke-virtual/range {v3 .. v17}, Lcom/android/launcher3/DragLayer;->a(Luj;IIIIFFFFFLjava/lang/Runnable;IILandroid/view/View;)V

    .line 613
    return-void

    .line 587
    :cond_1
    move-object/from16 v0, p2

    instance-of v3, v0, Lcom/android/launcher3/FolderIcon;

    if-eqz v3, :cond_2

    .line 589
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Luj;->fX()I

    move-result v8

    sub-int/2addr v3, v8

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v3, v5

    .line 590
    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v4

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v5, v8

    sub-float/2addr v3, v5

    float-to-int v3, v3

    .line 591
    int-to-float v3, v3

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, v4

    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v5, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v5, v8

    sub-float/2addr v3, v5

    float-to-int v8, v3

    .line 593
    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredWidth()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int/2addr v3, v5

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v7, v3

    move v12, v4

    goto :goto_0

    .line 595
    :cond_2
    invoke-virtual/range {p1 .. p1}, Luj;->getHeight()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sub-int/2addr v3, v8

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v8, v5, v3

    .line 596
    invoke-virtual/range {p1 .. p1}, Luj;->getMeasuredWidth()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v7, v3

    move v12, v4

    goto/16 :goto_0
.end method

.method public final a(Luj;Landroid/view/View;Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 545
    const/4 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/view/View;ILjava/lang/Runnable;Landroid/view/View;)V

    .line 546
    return-void
.end method

.method public final a(Luj;[IFFFILjava/lang/Runnable;I)V
    .locals 17

    .prologue
    .line 534
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 535
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 536
    iget v4, v2, Landroid/graphics/Rect;->left:I

    .line 537
    iget v5, v2, Landroid/graphics/Rect;->top:I

    .line 539
    const/4 v2, 0x0

    aget v6, p2, v2

    const/4 v2, 0x1

    aget v7, p2, v2

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3dcccccd    # 0.1f

    const v12, 0x3dcccccd    # 0.1f

    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v13, p7

    move/from16 v15, p8

    invoke-virtual/range {v2 .. v16}, Lcom/android/launcher3/DragLayer;->a(Luj;IIIIFFFFFLjava/lang/Runnable;IILandroid/view/View;)V

    .line 541
    return-void
.end method

.method public final a(Lyx;Lcom/android/launcher3/CellLayout;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 515
    new-instance v0, Lrs;

    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p0}, Lrs;-><init>(Landroid/content/Context;Lyx;Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/DragLayer;)V

    .line 518
    new-instance v1, Lcom/android/launcher3/DragLayer$LayoutParams;

    invoke-direct {v1, v2, v2}, Lcom/android/launcher3/DragLayer$LayoutParams;-><init>(II)V

    .line 519
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    .line 521
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/DragLayer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 522
    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->ES:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 524
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrs;->w(Z)V

    .line 525
    return-void
.end method

.method public addChildrenForAccessibility(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addChildrenForAccessibility(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 142
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yW:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 143
    return-void
.end method

.method public final b(Landroid/view/View;[I)F
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-static {p1, p0, p2, v0}, Ladp;->a(Landroid/view/View;Landroid/view/View;[IZ)F

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 417
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 418
    invoke-virtual {p0, v0}, Lcom/android/launcher3/DragLayer;->getLocationInWindow([I)V

    .line 419
    aget v1, v0, v3

    .line 420
    aget v2, v0, v4

    .line 422
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 423
    aget v3, v0, v3

    .line 424
    aget v0, v0, v4

    .line 426
    sub-int v1, v3, v1

    .line 427
    sub-int/2addr v0, v2

    .line 428
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 429
    return-void
.end method

.method public bringChildToFront(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 819
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    .line 820
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 823
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 825
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/DragLayer;->fO()V

    .line 826
    return-void
.end method

.method public final c(Landroid/view/View;[I)F
    .locals 1

    .prologue
    .line 413
    invoke-static {p1, p0, p2}, Ladp;->a(Landroid/view/View;Landroid/view/View;[I)F

    move-result v0

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 895
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->zx:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/DragLayer;->Ae:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 896
    iget v0, p0, Lcom/android/launcher3/DragLayer;->Ae:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 897
    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->zx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 898
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->zx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 899
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->zx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 902
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 903
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    invoke-virtual {v0}, Lty;->fD()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    invoke-virtual {v0, p1, p2}, Lty;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 936
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v3

    .line 939
    instance-of v1, p2, Lcom/android/launcher3/Workspace;

    if-eqz v1, :cond_1

    .line 940
    iget-boolean v1, p0, Lcom/android/launcher3/DragLayer;->Fh:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getMeasuredWidth()I

    move-result v5

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1, v6}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v7

    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_2

    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    add-int/lit8 v0, v7, 0x1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    if-eqz v1, :cond_4

    add-int/lit8 v1, v7, -0x1

    :goto_2
    invoke-virtual {v4, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eQ()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/android/launcher3/DragLayer;->Fg:Z

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eR()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fk:Landroid/graphics/drawable/Drawable;

    :goto_3
    iget v4, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v4, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eQ()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/DragLayer;->Fg:Z

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eR()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fl:Landroid/graphics/drawable/Drawable;

    :goto_4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int v1, v5, v1

    iget v2, v6, Landroid/graphics/Rect;->top:I

    iget v4, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v5, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 942
    :cond_1
    return v3

    :cond_2
    move v1, v2

    .line 940
    goto :goto_0

    :cond_3
    add-int/lit8 v0, v7, -0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v7, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fi:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fj:Landroid/graphics/drawable/Drawable;

    goto :goto_4
.end method

.method public final fK()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    .line 157
    return-void
.end method

.method public final fL()V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->ES:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->ES:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrs;

    .line 498
    invoke-virtual {v0}, Lrs;->dY()V

    .line 499
    invoke-virtual {p0, v0}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->ES:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 503
    :cond_1
    return-void
.end method

.method public final fM()V
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EU:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    if-eqz v0, :cond_1

    .line 767
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    invoke-virtual {v0, v1}, Lty;->a(Luj;)V

    .line 769
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    .line 770
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->invalidate()V

    .line 771
    return-void
.end method

.method public final fN()Landroid/view/View;
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->EX:Luj;

    return-object v0
.end method

.method public final fP()V
    .locals 1

    .prologue
    .line 866
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/DragLayer;->Fg:Z

    .line 867
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->invalidate()V

    .line 868
    return-void
.end method

.method public final fQ()V
    .locals 1

    .prologue
    .line 871
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DragLayer;->Fg:Z

    .line 872
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->invalidate()V

    .line 873
    return-void
.end method

.method final fR()V
    .locals 1

    .prologue
    .line 876
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/DragLayer;->Fh:Z

    .line 877
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->invalidate()V

    .line 878
    return-void
.end method

.method final fS()V
    .locals 1

    .prologue
    .line 881
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DragLayer;->Fh:Z

    .line 882
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->invalidate()V

    .line 883
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 4

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getChildCount()I

    move-result v1

    .line 127
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 128
    invoke-virtual {p0, v0}, Lcom/android/launcher3/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 129
    iget-object v3, p0, Lcom/android/launcher3/DragLayer;->yW:Landroid/graphics/Rect;

    invoke-static {v2, p1, v3}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yW:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public final getBackgroundAlpha()F
    .locals 1

    .prologue
    .line 953
    iget v0, p0, Lcom/android/launcher3/DragLayer;->Ae:F

    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 841
    iget v0, p0, Lcom/android/launcher3/DragLayer;->Ff:I

    if-eq v0, p1, :cond_0

    .line 846
    invoke-direct {p0}, Lcom/android/launcher3/DragLayer;->fO()V

    .line 850
    :cond_0
    iget v0, p0, Lcom/android/launcher3/DragLayer;->Fe:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 861
    :cond_1
    :goto_0
    return p2

    .line 853
    :cond_2
    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_3

    .line 855
    iget p2, p0, Lcom/android/launcher3/DragLayer;->Fe:I

    goto :goto_0

    .line 856
    :cond_3
    iget v0, p0, Lcom/android/launcher3/DragLayer;->Fe:I

    if-lt p2, v0, :cond_1

    .line 861
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fd:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 809
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/DragLayer;->fO()V

    .line 810
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 814
    invoke-direct {p0}, Lcom/android/launcher3/DragLayer;->fO()V

    .line 815
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 244
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 279
    :goto_0
    return v0

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v3

    .line 248
    if-nez v3, :cond_2

    move v0, v1

    .line 249
    goto :goto_0

    .line 251
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "accessibility"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 253
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 256
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    move v0, v1

    .line 279
    goto :goto_0

    .line 258
    :pswitch_1
    invoke-direct {p0, v3, p1}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/Folder;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 259
    if-nez v0, :cond_4

    .line 260
    invoke-virtual {v3}, Lcom/android/launcher3/Folder;->gp()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/DragLayer;->L(Z)V

    .line 261
    iput-boolean v2, p0, Lcom/android/launcher3/DragLayer;->Fa:Z

    move v0, v2

    .line 262
    goto :goto_0

    .line 264
    :cond_4
    iput-boolean v1, p0, Lcom/android/launcher3/DragLayer;->Fa:Z

    goto :goto_1

    .line 267
    :pswitch_2
    invoke-direct {p0, v3, p1}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/Folder;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 268
    if-nez v0, :cond_5

    iget-boolean v4, p0, Lcom/android/launcher3/DragLayer;->Fa:Z

    if-nez v4, :cond_5

    .line 269
    invoke-virtual {v3}, Lcom/android/launcher3/Folder;->gp()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/DragLayer;->L(Z)V

    .line 270
    iput-boolean v2, p0, Lcom/android/launcher3/DragLayer;->Fa:Z

    move v0, v2

    .line 271
    goto :goto_0

    .line 272
    :cond_5
    if-nez v0, :cond_6

    move v0, v2

    .line 273
    goto :goto_0

    .line 275
    :cond_6
    iput-boolean v1, p0, Lcom/android/launcher3/DragLayer;->Fa:Z

    goto :goto_1

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 226
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 228
    if-nez v1, :cond_0

    .line 229
    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/MotionEvent;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 239
    :goto_0
    return v0

    .line 232
    :cond_0
    if-eq v1, v0, :cond_1

    const/4 v0, 0x3

    if-ne v1, v0, :cond_3

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    invoke-interface {v0}, Lug;->fT()V

    .line 236
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    .line 238
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->fL()V

    .line 239
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    invoke-virtual {v0, p1}, Lty;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 481
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 482
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->getChildCount()I

    move-result v2

    .line 483
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 484
    invoke-virtual {p0, v1}, Lcom/android/launcher3/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 485
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 486
    instance-of v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    if-eqz v4, :cond_0

    .line 487
    check-cast v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    .line 488
    iget-boolean v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    if-eqz v4, :cond_0

    .line 489
    iget v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    iget v5, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    iget v6, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    iget v7, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    add-int/2addr v6, v7

    iget v7, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    iget v0, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    add-int/2addr v0, v7

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    .line 483
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 493
    :cond_1
    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_1

    .line 299
    if-ne p1, v0, :cond_0

    .line 300
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 306
    :goto_0
    return v0

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 306
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 329
    .line 330
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 332
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 333
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 335
    if-nez v2, :cond_1

    .line 336
    invoke-direct {p0, p1, v1}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/MotionEvent;Z)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 360
    :cond_0
    :goto_0
    return v0

    .line 339
    :cond_1
    if-eq v2, v0, :cond_2

    const/4 v5, 0x3

    if-ne v2, v5, :cond_4

    .line 340
    :cond_2
    iget-object v5, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    if-eqz v5, :cond_3

    .line 341
    iget-object v5, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    invoke-interface {v5}, Lug;->fT()V

    .line 343
    :cond_3
    iput-object v6, p0, Lcom/android/launcher3/DragLayer;->Fc:Lug;

    .line 346
    :cond_4
    iget-object v5, p0, Lcom/android/launcher3/DragLayer;->ET:Lrs;

    if-eqz v5, :cond_5

    .line 348
    packed-switch v2, :pswitch_data_0

    :goto_1
    move v1, v0

    .line 359
    :cond_5
    :goto_2
    if-nez v1, :cond_0

    .line 360
    iget-object v0, p0, Lcom/android/launcher3/DragLayer;->yg:Lty;

    invoke-virtual {v0, p1}, Lty;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 350
    :pswitch_0
    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->ET:Lrs;

    iget v2, p0, Lcom/android/launcher3/DragLayer;->EQ:I

    sub-int v2, v3, v2

    iget v3, p0, Lcom/android/launcher3/DragLayer;->ER:I

    sub-int v3, v4, v3

    invoke-virtual {v1, v2, v3}, Lrs;->y(II)V

    move v1, v0

    .line 351
    goto :goto_2

    .line 354
    :pswitch_1
    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->ET:Lrs;

    iget v2, p0, Lcom/android/launcher3/DragLayer;->EQ:I

    sub-int v2, v3, v2

    iget v3, p0, Lcom/android/launcher3/DragLayer;->ER:I

    sub-int v3, v4, v3

    invoke-virtual {v1, v2, v3}, Lrs;->y(II)V

    .line 355
    iget-object v1, p0, Lcom/android/launcher3/DragLayer;->ET:Lrs;

    invoke-virtual {v1}, Lrs;->dZ()V

    .line 356
    iput-object v6, p0, Lcom/android/launcher3/DragLayer;->ET:Lrs;

    goto :goto_1

    .line 348
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final setBackgroundAlpha(F)V
    .locals 1

    .prologue
    .line 946
    iget v0, p0, Lcom/android/launcher3/DragLayer;->Ae:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 947
    iput p1, p0, Lcom/android/launcher3/DragLayer;->Ae:F

    .line 948
    invoke-virtual {p0}, Lcom/android/launcher3/DragLayer;->invalidate()V

    .line 950
    :cond_0
    return-void
.end method
