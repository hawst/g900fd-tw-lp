.class public Lcom/android/launcher3/FocusIndicatorView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# static fields
.field private static final GF:[I

.field private static final GG:[I


# instance fields
.field private final GH:[I

.field private final GI:[I

.field private GJ:Landroid/view/View;

.field private GK:Z

.field private GL:Landroid/util/Pair;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 32
    new-array v0, v1, [I

    sput-object v0, Lcom/android/launcher3/FocusIndicatorView;->GF:[I

    .line 33
    new-array v0, v1, [I

    sput-object v0, Lcom/android/launcher3/FocusIndicatorView;->GG:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/FocusIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GH:[I

    .line 36
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GI:[I

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/FocusIndicatorView;->setAlpha(F)V

    .line 50
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/FocusIndicatorView;->setBackgroundColor(I)V

    .line 51
    return-void
.end method

.method private static d(Landroid/view/View;[I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 127
    sget-object v0, Lcom/android/launcher3/FocusIndicatorView;->GG:[I

    invoke-static {p0, v0}, Lcom/android/launcher3/FocusIndicatorView;->e(Landroid/view/View;[I)V

    .line 128
    sget-object v0, Lcom/android/launcher3/FocusIndicatorView;->GF:[I

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 129
    sget-object v0, Lcom/android/launcher3/FocusIndicatorView;->GF:[I

    aget v0, v0, v2

    sget-object v1, Lcom/android/launcher3/FocusIndicatorView;->GG:[I

    aget v1, v1, v2

    add-int/2addr v0, v1

    aput v0, p1, v2

    .line 130
    sget-object v0, Lcom/android/launcher3/FocusIndicatorView;->GF:[I

    aget v0, v0, v3

    sget-object v1, Lcom/android/launcher3/FocusIndicatorView;->GG:[I

    aget v1, v1, v3

    add-int/2addr v0, v1

    aput v0, p1, v3

    .line 131
    return-void
.end method

.method private static e(Landroid/view/View;[I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 134
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 135
    instance-of v1, v0, Lcom/android/launcher3/PagedView;

    if-eqz v1, :cond_0

    .line 136
    check-cast v0, Landroid/view/View;

    .line 137
    sget-object v1, Lcom/android/launcher3/FocusIndicatorView;->GF:[I

    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 138
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    sget-object v1, Lcom/android/launcher3/FocusIndicatorView;->GF:[I

    aget v1, v1, v2

    sub-int/2addr v0, v1

    aput v0, p1, v2

    .line 139
    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    neg-int v0, v0

    aput v0, p1, v3

    .line 145
    :goto_0
    return-void

    .line 140
    :cond_0
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 141
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/android/launcher3/FocusIndicatorView;->e(Landroid/view/View;[I)V

    goto :goto_0

    .line 143
    :cond_1
    aput v2, p1, v3

    aput v2, p1, v2

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GL:Landroid/util/Pair;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GL:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher3/FocusIndicatorView;->GL:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/FocusIndicatorView;->onFocusChange(Landroid/view/View;Z)V

    .line 120
    :cond_0
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    .line 67
    iput-object v1, p0, Lcom/android/launcher3/FocusIndicatorView;->GL:Landroid/util/Pair;

    .line 68
    iget-boolean v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GK:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 71
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GL:Landroid/util/Pair;

    .line 72
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->invalidate()V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-boolean v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GK:Z

    if-nez v0, :cond_2

    .line 77
    iget-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GH:[I

    invoke-static {p0, v0}, Lcom/android/launcher3/FocusIndicatorView;->d(Landroid/view/View;[I)V

    .line 78
    iput-boolean v7, p0, Lcom/android/launcher3/FocusIndicatorView;->GK:Z

    .line 81
    :cond_2
    if-eqz p2, :cond_4

    .line 82
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->getWidth()I

    move-result v0

    .line 83
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->getHeight()I

    move-result v1

    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    int-to-float v3, v0

    div-float/2addr v2, v3

    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    int-to-float v4, v1

    div-float/2addr v3, v4

    .line 88
    iget-object v4, p0, Lcom/android/launcher3/FocusIndicatorView;->GI:[I

    invoke-static {p1, v4}, Lcom/android/launcher3/FocusIndicatorView;->d(Landroid/view/View;[I)V

    .line 89
    iget-object v4, p0, Lcom/android/launcher3/FocusIndicatorView;->GI:[I

    aget v4, v4, v9

    iget-object v5, p0, Lcom/android/launcher3/FocusIndicatorView;->GH:[I

    aget v5, v5, v9

    sub-int/2addr v4, v5

    int-to-float v4, v4

    sub-float v5, v6, v2

    int-to-float v0, v0

    mul-float/2addr v0, v5

    div-float/2addr v0, v8

    sub-float v0, v4, v0

    .line 90
    iget-object v4, p0, Lcom/android/launcher3/FocusIndicatorView;->GI:[I

    aget v4, v4, v7

    iget-object v5, p0, Lcom/android/launcher3/FocusIndicatorView;->GH:[I

    aget v5, v5, v7

    sub-int/2addr v4, v5

    int-to-float v4, v4

    sub-float v5, v6, v3

    int-to-float v1, v1

    mul-float/2addr v1, v5

    div-float/2addr v1, v8

    sub-float v1, v4, v1

    .line 92
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->getAlpha()F

    move-result v4

    const v5, 0x3e4ccccd    # 0.2f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 106
    :goto_1
    iput-object p1, p0, Lcom/android/launcher3/FocusIndicatorView;->GJ:Landroid/view/View;

    goto :goto_0

    .line 100
    :cond_3
    invoke-virtual {p0, v0}, Lcom/android/launcher3/FocusIndicatorView;->setTranslationX(F)V

    .line 101
    invoke-virtual {p0, v1}, Lcom/android/launcher3/FocusIndicatorView;->setTranslationY(F)V

    .line 102
    invoke-virtual {p0, v2}, Lcom/android/launcher3/FocusIndicatorView;->setScaleX(F)V

    .line 103
    invoke-virtual {p0, v3}, Lcom/android/launcher3/FocusIndicatorView;->setScaleY(F)V

    .line 104
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GJ:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 109
    iput-object v1, p0, Lcom/android/launcher3/FocusIndicatorView;->GJ:Landroid/view/View;

    .line 110
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 59
    iget-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GJ:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GJ:Landroid/view/View;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/FocusIndicatorView;->GL:Landroid/util/Pair;

    .line 61
    invoke-virtual {p0}, Lcom/android/launcher3/FocusIndicatorView;->invalidate()V

    .line 63
    :cond_0
    return-void
.end method
