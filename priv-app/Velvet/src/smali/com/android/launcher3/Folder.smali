.class public Lcom/android/launcher3/Folder;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lui;
.implements Luo;
.implements Lvz;


# static fields
.field private static Hs:Ljava/lang/String;

.field private static Ht:Ljava/lang/String;


# instance fields
.field private EI:Landroid/view/inputmethod/InputMethodManager;

.field public GM:Lvy;

.field private GN:I

.field private GO:I

.field private GP:I

.field public GQ:Lcom/android/launcher3/CellLayout;

.field private GR:Landroid/widget/ScrollView;

.field private final GS:Landroid/view/LayoutInflater;

.field private GT:Z

.field private GU:Lcom/android/launcher3/FolderIcon;

.field private GV:I

.field private GW:I

.field private GX:I

.field private GY:Ljava/util/ArrayList;

.field private GZ:Z

.field private HA:Lace;

.field private HB:Lace;

.field private Ha:Ladh;

.field private Hb:Landroid/view/View;

.field private Hc:Z

.field private Hd:Z

.field private He:[I

.field private Hf:[I

.field private Hg:[I

.field private Hh:Lro;

.field private Hi:Lro;

.field private Hj:I

.field private Hk:Z

.field private Hl:Z

.field private Hm:Z

.field private Hn:Z

.field public Ho:Lcom/android/launcher3/FolderEditText;

.field private Hp:F

.field private Hq:F

.field private Hr:Z

.field private Hu:Lcom/android/launcher3/FocusIndicatorView;

.field private Hv:Lja;

.field private Hw:Ljava/lang/Runnable;

.field private Hx:Z

.field private Hy:Z

.field private Hz:Landroid/view/ActionMode$Callback;

.field private ag:I

.field private bB:Z

.field private gn:Landroid/graphics/Rect;

.field public xZ:Lcom/android/launcher3/Launcher;

.field private final xn:Lwi;

.field public yg:Lty;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 141
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/Folder;->ag:I

    .line 87
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->GT:Z

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->GY:Ljava/util/ArrayList;

    .line 93
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 97
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hd:Z

    .line 98
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher3/Folder;->He:[I

    .line 99
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hf:[I

    .line 100
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    .line 101
    new-instance v0, Lro;

    invoke-direct {v0}, Lro;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hh:Lro;

    .line 102
    new-instance v0, Lro;

    invoke-direct {v0}, Lro;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hi:Lro;

    .line 104
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->gn:Landroid/graphics/Rect;

    .line 105
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hk:Z

    .line 106
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hl:Z

    .line 107
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hm:Z

    .line 108
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hn:Z

    .line 113
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hr:Z

    .line 216
    new-instance v0, Lvb;

    invoke-direct {v0, p0}, Lvb;-><init>(Lcom/android/launcher3/Folder;)V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hz:Landroid/view/ActionMode$Callback;

    .line 675
    new-instance v0, Lvj;

    invoke-direct {v0, p0}, Lvj;-><init>(Lcom/android/launcher3/Folder;)V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->HA:Lace;

    .line 800
    new-instance v0, Lvk;

    invoke-direct {v0, p0}, Lvk;-><init>(Lcom/android/launcher3/Folder;)V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->HB:Lace;

    .line 143
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 144
    iget-object v1, v0, Lyu;->Mk:Lur;

    invoke-virtual {v1}, Lur;->gl()Ltu;

    move-result-object v1

    .line 145
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Folder;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 146
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/android/launcher3/Folder;->GS:Landroid/view/LayoutInflater;

    .line 147
    iget-object v0, v0, Lyu;->xn:Lwi;

    iput-object v0, p0, Lcom/android/launcher3/Folder;->xn:Lwi;

    .line 149
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 150
    iget v0, v1, Ltu;->Dh:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/Folder;->GV:I

    .line 152
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    const v0, 0x7fffffff

    iput v0, p0, Lcom/android/launcher3/Folder;->GX:I

    iput v0, p0, Lcom/android/launcher3/Folder;->GW:I

    .line 159
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/launcher3/Folder;->EI:Landroid/view/inputmethod/InputMethodManager;

    .line 162
    const v0, 0x7f0c0022

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Folder;->GN:I

    .line 163
    const v0, 0x7f0c0023

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Folder;->GO:I

    .line 164
    const v0, 0x7f0c0024

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Folder;->GP:I

    .line 166
    sget-object v0, Lcom/android/launcher3/Folder;->Hs:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 167
    const v0, 0x7f0a0084

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/launcher3/Folder;->Hs:Ljava/lang/String;

    .line 169
    :cond_0
    sget-object v0, Lcom/android/launcher3/Folder;->Ht:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 170
    const v0, 0x7f0a00be

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/launcher3/Folder;->Ht:Ljava/lang/String;

    .line 172
    :cond_1
    check-cast p1, Lcom/android/launcher3/Launcher;

    iput-object p1, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    .line 176
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->setFocusableInTouchMode(Z)V

    .line 177
    return-void

    .line 155
    :cond_2
    iget v0, v1, Ltu;->Dg:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/Folder;->GW:I

    .line 156
    iget v0, p0, Lcom/android/launcher3/Folder;->GV:I

    iget v1, p0, Lcom/android/launcher3/Folder;->GW:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/Folder;->GX:I

    goto :goto_0
.end method

.method public static synthetic a(Lcom/android/launcher3/Folder;I)I
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/android/launcher3/Folder;->ag:I

    return p1
.end method

.method public static synthetic a(Lcom/android/launcher3/Folder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hw:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic a(Lcom/android/launcher3/Folder;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gG()V

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Folder;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0x20

    invoke-direct {p0, v0, p2}, Lcom/android/launcher3/Folder;->c(ILjava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Folder;[I[I)V
    .locals 12

    .prologue
    .line 60
    const/4 v5, 0x0

    const/high16 v8, 0x41f00000    # 30.0f

    const/4 v0, 0x1

    aget v0, p2, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    aget v0, p2, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    aget v0, p2, v0

    const/4 v1, 0x0

    aget v1, p1, v1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_7

    const/4 v0, 0x0

    aget v0, p1, v0

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    aget v0, p1, v0

    add-int/lit8 v0, v0, 0x1

    :goto_2
    move v9, v0

    :goto_3
    const/4 v0, 0x1

    aget v0, p2, v0

    if-gt v9, v0, :cond_d

    const/4 v0, 0x1

    aget v0, p1, v0

    if-ne v9, v0, :cond_4

    const/4 v0, 0x0

    aget v0, p1, v0

    add-int/lit8 v0, v0, 0x1

    :goto_4
    const/4 v1, 0x1

    aget v1, p2, v1

    if-ge v9, v1, :cond_5

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v10, v1

    :goto_5
    move v11, v0

    :goto_6
    if-gt v11, v10, :cond_6

    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v11, v9}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v2, 0x0

    aget v2, p1, v2

    const/4 v3, 0x1

    aget v3, p1, v3

    const/16 v4, 0xe6

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x0

    aput v11, p1, v0

    const/4 v0, 0x1

    aput v9, p1, v0

    int-to-float v0, v5

    add-float/2addr v0, v8

    float-to-int v5, v0

    float-to-double v0, v8

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v0, v2

    double-to-float v0, v0

    :goto_7
    add-int/lit8 v1, v11, 0x1

    move v8, v0

    move v11, v1

    goto :goto_6

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    aget v0, p1, v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    aget v1, p2, v1

    move v10, v1

    goto :goto_5

    :cond_6
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    aget v0, p1, v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_9

    const/4 v0, 0x1

    aget v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    :goto_9
    move v9, v0

    :goto_a
    const/4 v0, 0x1

    aget v0, p2, v0

    if-lt v9, v0, :cond_d

    const/4 v0, 0x1

    aget v0, p1, v0

    if-ne v9, v0, :cond_a

    const/4 v0, 0x0

    aget v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    :goto_b
    const/4 v1, 0x1

    aget v1, p2, v1

    if-le v9, v1, :cond_b

    const/4 v1, 0x0

    move v10, v1

    :goto_c
    move v11, v0

    :goto_d
    if-lt v11, v10, :cond_c

    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v11, v9}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v2, 0x0

    aget v2, p1, v2

    const/4 v3, 0x1

    aget v3, p1, v3

    const/16 v4, 0xe6

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    aput v11, p1, v0

    const/4 v0, 0x1

    aput v9, p1, v0

    int-to-float v0, v5

    add-float/2addr v0, v8

    float-to-int v5, v0

    float-to-double v0, v8

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v0, v2

    double-to-float v0, v0

    :goto_e
    add-int/lit8 v1, v11, -0x1

    move v8, v0

    move v11, v1

    goto :goto_d

    :cond_8
    const/4 v0, 0x0

    goto :goto_8

    :cond_9
    const/4 v0, 0x1

    aget v0, p1, v0

    goto :goto_9

    :cond_a
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    aget v1, p2, v1

    move v10, v1

    goto :goto_c

    :cond_c
    add-int/lit8 v0, v9, -0x1

    move v9, v0

    goto :goto_a

    :cond_d
    return-void

    :cond_e
    move v0, v8

    goto :goto_e

    :cond_f
    move v0, v8

    goto/16 :goto_7
.end method

.method public static synthetic b(Lcom/android/launcher3/Folder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v1, v1}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method private b(Ladh;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 631
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 632
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    iget v4, p1, Ladh;->AY:I

    iget v5, p1, Ladh;->AZ:I

    invoke-virtual {v3, v2, v4, v5}, Lcom/android/launcher3/CellLayout;->b([III)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 633
    aget v1, v2, v1

    iput v1, p1, Ladh;->Bb:I

    .line 634
    aget v1, v2, v0

    iput v1, p1, Ladh;->Bc:I

    .line 637
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private bd(I)V
    .locals 13

    .prologue
    .line 960
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v1

    .line 962
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v5

    .line 963
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v3

    .line 964
    const/4 v0, 0x0

    .line 966
    :goto_0
    if-nez v0, :cond_5

    .line 969
    mul-int v0, v5, v3

    if-ge v0, p1, :cond_2

    .line 971
    if-le v5, v3, :cond_0

    iget v0, p0, Lcom/android/launcher3/Folder;->GW:I

    if-ne v3, v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/launcher3/Folder;->GV:I

    if-ge v5, v0, :cond_1

    .line 972
    add-int/lit8 v2, v5, 0x1

    move v0, v3

    .line 976
    :goto_1
    if-nez v0, :cond_b

    add-int/lit8 v0, v0, 0x1

    move v4, v2

    move v2, v0

    .line 982
    :goto_2
    if-ne v4, v5, :cond_4

    if-ne v2, v3, :cond_4

    const/4 v0, 0x1

    :goto_3
    move v3, v2

    move v5, v4

    .line 983
    goto :goto_0

    .line 973
    :cond_1
    iget v0, p0, Lcom/android/launcher3/Folder;->GW:I

    if-ge v3, v0, :cond_c

    .line 974
    add-int/lit8 v0, v3, 0x1

    move v2, v5

    goto :goto_1

    .line 977
    :cond_2
    add-int/lit8 v0, v3, -0x1

    mul-int/2addr v0, v5

    if-lt v0, p1, :cond_3

    if-lt v3, v5, :cond_3

    .line 978
    const/4 v0, 0x0

    add-int/lit8 v2, v3, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v0

    move v4, v5

    goto :goto_2

    .line 979
    :cond_3
    add-int/lit8 v0, v5, -0x1

    mul-int/2addr v0, v3

    if-lt v0, p1, :cond_a

    .line 980
    const/4 v0, 0x0

    add-int/lit8 v2, v5, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v3

    move v4, v0

    goto :goto_2

    .line 982
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 984
    :cond_5
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v5, v3}, Lcom/android/launcher3/CellLayout;->A(II)V

    .line 985
    const/4 v0, 0x2

    new-array v12, v0, [I

    if-nez v1, :cond_9

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v0

    move-object v10, v0

    :goto_4
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->removeAllViews()V

    const/4 v0, 0x0

    move v11, v0

    :goto_5
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v11, v0, :cond_8

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/view/View;

    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v12, v1, v2}, Lcom/android/launcher3/CellLayout;->c([III)Z

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher3/CellLayout$LayoutParams;

    const/4 v0, 0x0

    aget v0, v12, v0

    iput v0, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    const/4 v0, 0x1

    aget v0, v12, v0

    iput v0, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwq;

    iget v0, v1, Lwq;->Bb:I

    const/4 v2, 0x0

    aget v2, v12, v2

    if-ne v0, v2, :cond_6

    iget v0, v1, Lwq;->Bc:I

    const/4 v2, 0x1

    aget v2, v12, v2

    if-eq v0, v2, :cond_7

    :cond_6
    const/4 v0, 0x0

    aget v0, v12, v0

    iput v0, v1, Lwq;->Bb:I

    const/4 v0, 0x1

    aget v0, v12, v0

    iput v0, v1, Lwq;->Bc:I

    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v2, Lvy;->id:J

    const-wide/16 v4, 0x0

    iget v6, v1, Lwq;->Bb:I

    iget v7, v1, Lwq;->Bc:I

    invoke-static/range {v0 .. v7}, Lzi;->a(Landroid/content/Context;Lwq;JJII)V

    :cond_7
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v2, -0x1

    iget-wide v4, v1, Lwq;->id:J

    long-to-int v3, v4

    const/4 v5, 0x1

    move-object v1, v9

    move-object v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_5

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 986
    return-void

    :cond_9
    move-object v10, v1

    goto :goto_4

    :cond_a
    move v2, v3

    move v4, v5

    goto/16 :goto_2

    :cond_b
    move v4, v2

    move v2, v0

    goto/16 :goto_2

    :cond_c
    move v0, v3

    move v2, v5

    goto/16 :goto_1
.end method

.method private be(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1059
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->bd(I)V

    .line 1061
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    .line 1062
    if-nez v0, :cond_0

    .line 1063
    new-instance v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/android/launcher3/DragLayer$LayoutParams;-><init>(II)V

    .line 1064
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    .line 1065
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1067
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gA()V

    .line 1068
    return-void
.end method

.method private bf(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0, p1}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c(Ladh;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 642
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f04008c

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/BubbleTextView;

    .line 644
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xn:Lwi;

    invoke-virtual {v1, p1, v0, v2}, Lcom/android/launcher3/BubbleTextView;->a(Ladh;Lwi;Z)V

    .line 646
    invoke-virtual {v1, p0}, Lcom/android/launcher3/BubbleTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 647
    invoke-virtual {v1, p0}, Lcom/android/launcher3/BubbleTextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 648
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hu:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/BubbleTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 652
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    iget v2, p1, Ladh;->Bb:I

    iget v3, p1, Ladh;->Bc:I

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p1, Ladh;->Bb:I

    if-ltz v0, :cond_0

    iget v0, p1, Ladh;->Bc:I

    if-ltz v0, :cond_0

    iget v0, p1, Ladh;->Bb:I

    iget-object v2, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget v0, p1, Ladh;->Bc:I

    iget-object v2, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 655
    :cond_0
    const-string v0, "Launcher.Folder"

    const-string v2, "Folder order not properly persisted during bind"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->b(Ladh;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 657
    const/4 v1, 0x0

    .line 666
    :goto_0
    return-object v1

    .line 661
    :cond_1
    new-instance v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    iget v0, p1, Ladh;->Bb:I

    iget v2, p1, Ladh;->Bc:I

    iget v3, p1, Ladh;->AY:I

    iget v5, p1, Ladh;->AZ:I

    invoke-direct {v4, v0, v2, v3, v5}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(IIII)V

    .line 663
    new-instance v0, Lwa;

    invoke-direct {v0}, Lwa;-><init>()V

    invoke-virtual {v1, v0}, Lcom/android/launcher3/BubbleTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 665
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v2, -0x1

    iget-wide v6, p1, Ladh;->id:J

    long-to-int v3, v6

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    goto :goto_0
.end method

.method private c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 578
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 580
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 582
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Folder;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 583
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 586
    :cond_0
    return-void
.end method

.method public static synthetic c(Lcom/android/launcher3/Folder;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Folder;->yg:Lty;

    invoke-virtual {v0, p0}, Lty;->c(Luo;)V

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->clearFocus()V

    iget-object v0, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->requestFocus()Z

    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->GT:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->be(I)V

    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->GT:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    if-gt v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hk:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hm:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gG()V

    :cond_2
    :goto_0
    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->Hm:Z

    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hk:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hl:Z

    goto :goto_0
.end method

.method public static synthetic d(Lcom/android/launcher3/Folder;)[I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    return-object v0
.end method

.method public static synthetic e(Lcom/android/launcher3/Folder;)[I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/launcher3/Folder;->He:[I

    return-object v0
.end method

.method public static synthetic f(Lcom/android/launcher3/Folder;)Lcom/android/launcher3/FolderIcon;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    return-object v0
.end method

.method private gA()V
    .locals 12

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v11, 0x3f800000    # 1.0f

    .line 993
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    .line 995
    iget-object v1, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    const v2, 0x7f11023c

    invoke-virtual {v1, v2}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/DragLayer;

    .line 996
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher3/CellLayout;->fh()I

    move-result v3

    add-int v4, v2, v3

    .line 997
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gF()I

    move-result v5

    .line 999
    iget-object v2, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    iget-object v3, p0, Lcom/android/launcher3/Folder;->gn:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result v2

    .line 1001
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v3

    .line 1002
    iget-object v3, v3, Lyu;->Mk:Lur;

    invoke-virtual {v3}, Lur;->gl()Ltu;

    move-result-object v6

    .line 1004
    iget-object v3, p0, Lcom/android/launcher3/Folder;->gn:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget-object v7, p0, Lcom/android/launcher3/Folder;->gn:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v2

    div-float/2addr v7, v9

    add-float/2addr v3, v7

    float-to-int v3, v3

    .line 1005
    iget-object v7, p0, Lcom/android/launcher3/Folder;->gn:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/android/launcher3/Folder;->gn:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v2, v8

    div-float/2addr v2, v9

    add-float/2addr v2, v7

    float-to-int v2, v2

    .line 1006
    div-int/lit8 v7, v4, 0x2

    sub-int v7, v3, v7

    .line 1007
    div-int/lit8 v3, v5, 0x2

    sub-int v8, v2, v3

    .line 1008
    iget-object v2, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v3

    .line 1011
    iget-object v2, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Workspace;->bN(I)V

    .line 1013
    iget-object v2, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout;

    .line 1014
    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v2

    .line 1015
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1016
    invoke-virtual {v1, v2, v9}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 1018
    iget-object v1, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/launcher3/Workspace;->bO(I)V

    .line 1021
    iget v1, v9, Landroid/graphics/Rect;->left:I

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, v9, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1023
    iget v2, v9, Landroid/graphics/Rect;->top:I

    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v10

    add-int/2addr v3, v10

    sub-int/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1025
    iget-boolean v3, v6, Ltu;->Dq:Z

    if-nez v3, :cond_2

    iget-boolean v3, v6, Ltu;->Dr:Z

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_3

    iget v3, v6, Ltu;->Dz:I

    sub-int/2addr v3, v4

    iget v10, v6, Ltu;->DI:I

    if-ge v3, v10, :cond_3

    .line 1027
    iget v1, v6, Ltu;->Dz:I

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    .line 1032
    :cond_0
    :goto_1
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-lt v5, v3, :cond_1

    .line 1033
    iget v2, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v3, v5

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 1036
    :cond_1
    div-int/lit8 v3, v4, 0x2

    sub-int v6, v7, v1

    add-int/2addr v3, v6

    .line 1037
    div-int/lit8 v6, v5, 0x2

    sub-int v7, v8, v2

    add-int/2addr v6, v7

    .line 1038
    int-to-float v7, v3

    invoke-virtual {p0, v7}, Lcom/android/launcher3/Folder;->setPivotX(F)V

    .line 1039
    int-to-float v7, v6

    invoke-virtual {p0, v7}, Lcom/android/launcher3/Folder;->setPivotY(F)V

    .line 1040
    iget-object v7, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v7}, Lcom/android/launcher3/FolderIcon;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    int-to-float v3, v3

    mul-float/2addr v3, v11

    int-to-float v8, v4

    div-float/2addr v3, v8

    mul-float/2addr v3, v7

    float-to-int v3, v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/launcher3/Folder;->Hp:F

    .line 1042
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v3}, Lcom/android/launcher3/FolderIcon;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    int-to-float v6, v6

    mul-float/2addr v6, v11

    int-to-float v7, v5

    div-float/2addr v6, v7

    mul-float/2addr v3, v6

    float-to-int v3, v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/launcher3/Folder;->Hq:F

    .line 1045
    iput v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    .line 1046
    iput v5, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    .line 1047
    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    .line 1048
    iput v2, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    .line 1049
    return-void

    .line 1025
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 1028
    :cond_3
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-lt v4, v3, :cond_0

    .line 1030
    iget v1, v9, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    goto :goto_1
.end method

.method private gD()I
    .locals 3

    .prologue
    .line 1071
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 1072
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v1

    .line 1073
    iget-boolean v0, v1, Ltu;->Dp:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ltu;->ba(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1075
    iget v1, v1, Ltu;->DA:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    iget v1, p0, Lcom/android/launcher3/Folder;->Hj:I

    sub-int/2addr v0, v1

    .line 1078
    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->fi()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1080
    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    .line 1073
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private gE()I
    .locals 2

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->fh()I

    move-result v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private gF()I
    .locals 2

    .prologue
    .line 1088
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gD()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/Folder;->Hj:I

    add-int/2addr v0, v1

    .line 1090
    return v0
.end method

.method private gG()V
    .locals 3

    .prologue
    .line 1173
    new-instance v0, Lvc;

    invoke-direct {v0, p0}, Lvc;-><init>(Lcom/android/launcher3/Folder;)V

    .line 1208
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/launcher3/Folder;->bf(I)Landroid/view/View;

    move-result-object v1

    .line 1209
    if-eqz v1, :cond_0

    .line 1210
    iget-object v2, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v2, v1, v0}, Lcom/android/launcher3/FolderIcon;->b(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1214
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->bB:Z

    .line 1215
    return-void

    .line 1212
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private gH()V
    .locals 3

    .prologue
    .line 1224
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->bf(I)Landroid/view/View;

    move-result-object v0

    .line 1225
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/android/launcher3/Folder;->bf(I)Landroid/view/View;

    .line 1226
    if-eqz v0, :cond_0

    .line 1227
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher3/FolderEditText;->setNextFocusDownId(I)V

    .line 1228
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher3/FolderEditText;->setNextFocusRightId(I)V

    .line 1229
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher3/FolderEditText;->setNextFocusLeftId(I)V

    .line 1230
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/launcher3/FolderEditText;->setNextFocusUpId(I)V

    .line 1232
    :cond_0
    return-void
.end method

.method private gx()V
    .locals 10

    .prologue
    .line 918
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v9

    .line 919
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 920
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 921
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwq;

    .line 922
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v2, Lvy;->id:J

    const-wide/16 v4, 0x0

    iget v6, v1, Lwq;->Bb:I

    iget v7, v1, Lwq;->Bc:I

    invoke-static/range {v0 .. v7}, Lzi;->b(Landroid/content/Context;Lwq;JJII)V

    .line 919
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 925
    :cond_0
    return-void
.end method

.method private gy()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 928
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v3

    .line 929
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 930
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 931
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 932
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 933
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 930
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 936
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v6, v1, Lvy;->id:J

    invoke-static {v0, v4, v6, v7, v2}, Lzi;->a(Landroid/content/Context;Ljava/util/ArrayList;JI)V

    .line 937
    return-void
.end method

.method private h(Ladh;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1347
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v2

    if-ge v0, v2, :cond_2

    move v2, v1

    .line 1348
    :goto_1
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1349
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v3, v2, v0}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v3

    .line 1350
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, p1, :cond_0

    move-object v0, v3

    .line 1355
    :goto_2
    return-object v0

    .line 1348
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1347
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1355
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private h(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 360
    .line 361
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    .line 362
    :goto_0
    if-ge v3, v4, :cond_0

    .line 363
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 364
    iget v5, v0, Ladh;->Bb:I

    if-le v5, v1, :cond_2

    .line 365
    iget v0, v0, Ladh;->Bb:I

    .line 362
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 369
    :cond_0
    new-instance v0, Lvm;

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, p0, v1}, Lvm;-><init>(Lcom/android/launcher3/Folder;I)V

    .line 370
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 371
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v3

    move v1, v2

    .line 372
    :goto_2
    if-ge v1, v4, :cond_1

    .line 373
    rem-int v2, v1, v3

    .line 374
    div-int v5, v1, v3

    .line 375
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 376
    iput v2, v0, Ladh;->Bb:I

    .line 377
    iput v5, v0, Ladh;->Bc:I

    .line 372
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 379
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static j(Landroid/content/Context;)Lcom/android/launcher3/Folder;
    .locals 3

    .prologue
    .line 437
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401b6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/Folder;

    return-object v0
.end method


# virtual methods
.method public final O(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    sget-object v1, Lcom/android/launcher3/Folder;->Ht:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 285
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iput-object v3, v4, Lvy;->title:Ljava/lang/CharSequence;

    move v1, v2

    :goto_0
    iget-object v0, v4, Lvy;->IB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, v4, Lvy;->IB:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvz;

    invoke-interface {v0, v3}, Lvz;->h(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-static {v0, v1}, Lzi;->a(Landroid/content/Context;Lwq;)V

    .line 288
    const/16 v0, 0x20

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0a00da

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Folder;->c(ILjava/lang/String;)V

    .line 294
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->requestFocus()Z

    .line 296
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, v2, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 297
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hr:Z

    .line 298
    return-void
.end method

.method public final P(Z)V
    .locals 1

    .prologue
    .line 881
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hx:Z

    .line 882
    iput-boolean p1, p0, Lcom/android/launcher3/Folder;->Hy:Z

    .line 883
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hw:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 884
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hw:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 886
    :cond_0
    return-void
.end method

.method public final a(Ladh;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 566
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->be(I)V

    .line 567
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->b(Ladh;)Z

    .line 569
    iput-object p1, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    .line 570
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    const/4 v1, 0x0

    iget v2, p1, Ladh;->Bb:I

    aput v2, v0, v1

    .line 571
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    iget v1, p1, Ladh;->Bc:I

    aput v1, v0, v3

    .line 572
    iput-boolean v3, p0, Lcom/android/launcher3/Folder;->Hc:Z

    .line 574
    iput-boolean v3, p0, Lcom/android/launcher3/Folder;->Hk:Z

    .line 575
    return-void
.end method

.method public final a(Landroid/view/View;Luq;ZZ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 829
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hx:Z

    if-eqz v0, :cond_0

    .line 830
    new-instance v0, Lvl;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lvl;-><init>(Lcom/android/launcher3/Folder;Landroid/view/View;Luq;ZZ)V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hw:Ljava/lang/Runnable;

    .line 874
    :goto_0
    return-void

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hw:Ljava/lang/Runnable;

    if-eqz v0, :cond_5

    move v0, v1

    .line 841
    :goto_1
    if-eqz p4, :cond_6

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hy:Z

    if-eqz v0, :cond_6

    :cond_1
    move v0, v1

    .line 844
    :goto_2
    if-eqz v0, :cond_7

    .line 845
    iget-boolean v3, p0, Lcom/android/launcher3/Folder;->Hl:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/android/launcher3/Folder;->Hn:Z

    if-nez v3, :cond_2

    if-eq p1, p0, :cond_2

    .line 846
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gG()V

    .line 854
    :cond_2
    :goto_3
    if-eq p1, p0, :cond_4

    .line 855
    iget-object v3, p0, Lcom/android/launcher3/Folder;->Hi:Lro;

    invoke-virtual {v3}, Lro;->dW()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 856
    iget-object v3, p0, Lcom/android/launcher3/Folder;->Hi:Lro;

    invoke-virtual {v3}, Lro;->dV()V

    .line 857
    if-nez v0, :cond_3

    .line 858
    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->Hm:Z

    .line 860
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->gv()V

    .line 864
    :cond_4
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hl:Z

    .line 865
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hk:Z

    .line 866
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hn:Z

    .line 867
    iput-object v4, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    .line 868
    iput-object v4, p0, Lcom/android/launcher3/Folder;->Hb:Landroid/view/View;

    .line 869
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hd:Z

    .line 873
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gy()V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 840
    goto :goto_1

    :cond_6
    move v0, v2

    .line 841
    goto :goto_2

    .line 849
    :cond_7
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/android/launcher3/Folder;->be(I)V

    .line 851
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v3, p2}, Lcom/android/launcher3/FolderIcon;->b(Luq;)V

    goto :goto_3
.end method

.method final a(Lcom/android/launcher3/FolderIcon;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    .line 330
    return-void
.end method

.method public final a(Luq;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 910
    return-void
.end method

.method final a(Lvy;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 382
    iput-object p1, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    .line 383
    iget-object v3, p1, Lvy;->IA:Ljava/util/ArrayList;

    .line 384
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 385
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/launcher3/Folder;->be(I)V

    .line 386
    invoke-direct {p0, v3}, Lcom/android/launcher3/Folder;->h(Ljava/util/ArrayList;)V

    move v1, v0

    move v2, v0

    .line 388
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 389
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 390
    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->c(Ladh;)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_0

    .line 391
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 393
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 398
    :cond_1
    invoke-direct {p0, v2}, Lcom/android/launcher3/Folder;->be(I)V

    .line 403
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 404
    iget-object v2, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-virtual {v2, v0}, Lvy;->k(Ladh;)V

    .line 405
    iget-object v2, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v2, v0}, Lzi;->b(Landroid/content/Context;Lwq;)V

    goto :goto_2

    .line 408
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 409
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gH()V

    .line 410
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-virtual {v0, p0}, Lvy;->a(Lvz;)V

    .line 412
    sget-object v0, Lcom/android/launcher3/Folder;->Hs:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-object v1, v1, Lvy;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 413
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-object v1, v1, Lvy;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    .line 417
    :goto_3
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gx()V

    .line 420
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GU:Lcom/android/launcher3/FolderIcon;

    new-instance v1, Lve;

    invoke-direct {v1, p0}, Lve;-><init>(Lcom/android/launcher3/Folder;)V

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderIcon;->post(Ljava/lang/Runnable;)Z

    .line 427
    return-void

    .line 415
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public final a(Luq;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 623
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Lwq;

    .line 624
    iget v0, v0, Lwq;->Jz:I

    .line 625
    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->isFull()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final animateOpen()V
    .locals 12

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 460
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher3/DragLayer;

    if-nez v0, :cond_1

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-nez v0, :cond_3

    .line 465
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher3/DragLayer;

    if-eqz v0, :cond_2

    const v0, 0x3f4ccccd    # 0.8f

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->setScaleX(F)V

    const v0, 0x3f4ccccd    # 0.8f

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->setScaleY(F)V

    invoke-virtual {p0, v9}, Lcom/android/launcher3/Folder;->setAlpha(F)V

    iput v8, p0, Lcom/android/launcher3/Folder;->ag:I

    .line 466
    :cond_2
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gA()V

    .line 468
    const-string v0, "alpha"

    new-array v1, v11, [F

    aput v4, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 469
    const-string v1, "scaleX"

    new-array v2, v11, [F

    aput v4, v2, v8

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 470
    const-string v2, "scaleY"

    new-array v3, v11, [F

    aput v4, v3, v8

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 471
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v8

    aput-object v1, v3, v11

    aput-object v2, v3, v10

    invoke-static {p0, v3}, Lyr;->a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 473
    iget v0, p0, Lcom/android/launcher3/Folder;->GN:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 476
    const/4 v0, 0x0

    invoke-virtual {p0, v10, v0}, Lcom/android/launcher3/Folder;->setLayerType(ILandroid/graphics/Paint;)V

    .line 477
    new-instance v0, Lvf;

    invoke-direct {v0, p0}, Lvf;-><init>(Lcom/android/launcher3/Folder;)V

    .line 538
    :goto_1
    new-instance v2, Lvh;

    invoke-direct {v2, p0, v0}, Lvh;-><init>(Lcom/android/launcher3/Folder;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 557
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 560
    iget-object v0, p0, Lcom/android/launcher3/Folder;->yg:Lty;

    invoke-virtual {v0}, Lty;->fE()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/android/launcher3/Folder;->yg:Lty;

    invoke-virtual {v0}, Lty;->fJ()V

    goto :goto_0

    .line 484
    :cond_3
    invoke-virtual {p0, v4}, Lcom/android/launcher3/Folder;->setScaleX(F)V

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Folder;->setScaleY(F)V

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Folder;->setAlpha(F)V

    iput v8, p0, Lcom/android/launcher3/Folder;->ag:I

    .line 485
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gA()V

    .line 487
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->fh()I

    move-result v1

    add-int/2addr v0, v1

    .line 488
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gF()I

    move-result v1

    .line 490
    const v2, -0x42666666    # -0.075f

    div-int/lit8 v3, v0, 0x2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotX()F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 491
    const v3, -0x42666666    # -0.075f

    div-int/lit8 v4, v1, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotY()F

    move-result v5

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    .line 492
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Folder;->setTranslationX(F)V

    .line 493
    invoke-virtual {p0, v3}, Lcom/android/launcher3/Folder;->setTranslationY(F)V

    .line 494
    const-string v4, "translationX"

    new-array v5, v10, [F

    aput v2, v5, v8

    aput v9, v5, v11

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 495
    const-string v4, "translationY"

    new-array v5, v10, [F

    aput v3, v5, v8

    aput v9, v5, v11

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 497
    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotX()F

    move-result v4

    sub-float/2addr v0, v4

    invoke-static {v0, v9}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotX()F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    .line 498
    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotY()F

    move-result v4

    sub-float/2addr v1, v4

    invoke-static {v1, v9}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotY()F

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 499
    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 500
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v1

    .line 501
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPivotY()F

    move-result v5

    float-to-int v5, v5

    invoke-static {p0, v4, v5, v9, v0}, Lyr;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 503
    iget v4, p0, Lcom/android/launcher3/Folder;->GO:I

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 504
    new-instance v4, Labx;

    const/16 v5, 0x64

    invoke-direct {v4, v5, v8}, Labx;-><init>(II)V

    invoke-virtual {v0, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 506
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v4, v9}, Lcom/android/launcher3/CellLayout;->setAlpha(F)V

    .line 507
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const-string v5, "alpha"

    new-array v6, v10, [F

    fill-array-data v6, :array_0

    invoke-static {v4, v5, v6}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 508
    iget v5, p0, Lcom/android/launcher3/Folder;->GO:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 509
    iget v5, p0, Lcom/android/launcher3/Folder;->GP:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 510
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v6, 0x3fc00000    # 1.5f

    invoke-direct {v5, v6}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 512
    iget-object v5, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v5, v9}, Lcom/android/launcher3/FolderEditText;->setAlpha(F)V

    .line 513
    iget-object v5, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    const-string v6, "alpha"

    new-array v7, v10, [F

    fill-array-data v7, :array_1

    invoke-static {v5, v6, v7}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 514
    iget v6, p0, Lcom/android/launcher3/Folder;->GO:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 515
    iget v6, p0, Lcom/android/launcher3/Folder;->GP:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 516
    new-instance v6, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v7, 0x3fc00000    # 1.5f

    invoke-direct {v6, v7}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v5, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 518
    new-array v6, v10, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v6, v8

    aput-object v3, v6, v11

    invoke-static {p0, v6}, Lyr;->a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 519
    iget v3, p0, Lcom/android/launcher3/Folder;->GO:I

    int-to-long v6, v3

    invoke-virtual {v2, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 520
    iget v3, p0, Lcom/android/launcher3/Folder;->GP:I

    int-to-long v6, v3

    invoke-virtual {v2, v6, v7}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 521
    new-instance v3, Labx;

    const/16 v6, 0x3c

    invoke-direct {v3, v6, v8}, Labx;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 523
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 524
    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 525
    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 526
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 530
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v10, v2}, Lcom/android/launcher3/CellLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 531
    new-instance v0, Lvg;

    invoke-direct {v0, p0}, Lvg;-><init>(Lcom/android/launcher3/Folder;)V

    goto/16 :goto_1

    .line 507
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 513
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final b(Luq;)V
    .locals 12

    .prologue
    .line 1235
    const/4 v0, 0x0

    .line 1239
    iget-object v1, p1, Luq;->Gf:Lui;

    iget-object v2, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v2

    if-eq v1, v2, :cond_3

    iget-object v1, p1, Luq;->Gf:Lui;

    instance-of v1, v1, Lcom/android/launcher3/Folder;

    if-nez v1, :cond_3

    .line 1240
    new-instance v0, Lvd;

    invoke-direct {v0, p0}, Lvd;-><init>(Lcom/android/launcher3/Folder;)V

    move-object v8, v0

    .line 1251
    :goto_0
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    .line 1252
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hc:Z

    if-eqz v0, :cond_1

    .line 1253
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    iput v0, v1, Ladh;->Bb:I

    .line 1254
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    const/4 v2, 0x1

    aget v0, v0, v2

    iput v0, v1, Ladh;->Bc:I

    .line 1258
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v2, Lvy;->id:J

    const-wide/16 v4, 0x0

    iget v6, v1, Ladh;->Bb:I

    iget v7, v1, Ladh;->Bc:I

    invoke-static/range {v0 .. v7}, Lzi;->a(Landroid/content/Context;Lwq;JJII)V

    .line 1262
    iget-object v0, p1, Luq;->Gf:Lui;

    if-eq v0, p0, :cond_0

    .line 1263
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gy()V

    .line 1265
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hc:Z

    .line 1267
    invoke-direct {p0, v1}, Lcom/android/launcher3/Folder;->c(Ladh;)Landroid/view/View;

    move-result-object v3

    .line 1276
    :goto_1
    iget-object v0, p1, Luq;->Gd:Luj;

    invoke-virtual {v0}, Luj;->gd()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1279
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getScaleX()F

    move-result v0

    .line 1280
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getScaleY()F

    move-result v2

    .line 1281
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Folder;->setScaleX(F)V

    .line 1282
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Folder;->setScaleY(F)V

    .line 1283
    iget-object v4, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v4

    iget-object v5, p1, Luq;->Gd:Luj;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v8, v6}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/view/View;Ljava/lang/Runnable;Landroid/view/View;)V

    .line 1285
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->setScaleX(F)V

    .line 1286
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Folder;->setScaleY(F)V

    .line 1291
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 1292
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->bd(I)V

    .line 1295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hd:Z

    .line 1296
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-virtual {v0, v1}, Lvy;->j(Ladh;)V

    .line 1297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hd:Z

    .line 1299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    .line 1300
    return-void

    .line 1269
    :cond_1
    iget-object v3, p0, Lcom/android/launcher3/Folder;->Hb:Landroid/view/View;

    .line 1270
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 1271
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    iput v0, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iput v0, v1, Ladh;->Bb:I

    .line 1272
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hg:[I

    const/4 v2, 0x1

    aget v0, v0, v2

    iput v0, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iput v0, v1, Ladh;->Bb:I

    .line 1273
    iget-object v2, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    const/4 v4, -0x1

    iget-wide v10, v1, Ladh;->id:J

    long-to-int v5, v10

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    goto :goto_1

    .line 1288
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p1, Luq;->Gh:Z

    .line 1289
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move-object v8, v0

    goto/16 :goto_0
.end method

.method public final c(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1393
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Folder;->getHitRect(Landroid/graphics/Rect;)V

    .line 1394
    return-void
.end method

.method public final c(Luq;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 670
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hf:[I

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 671
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hf:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 672
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hi:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 673
    return-void
.end method

.method public final d(Ladh;)V
    .locals 2

    .prologue
    .line 1306
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->h(Ladh;)Landroid/view/View;

    move-result-object v0

    .line 1307
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1308
    return-void
.end method

.method public final d(Lty;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/android/launcher3/Folder;->yg:Lty;

    .line 326
    return-void
.end method

.method public final d(Luq;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 738
    iget-object v0, p1, Luq;->Gd:Luj;

    .line 739
    iget-object v1, p0, Lcom/android/launcher3/Folder;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v9

    .line 740
    iget v1, p1, Luq;->x:I

    iget v2, p1, Luq;->y:I

    iget v3, p1, Luq;->Ga:I

    iget v5, p1, Luq;->Gb:I

    new-array v10, v4, [F

    sub-int/2addr v1, v3

    sub-int/2addr v2, v5

    invoke-virtual {v0}, Luj;->fZ()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    int-to-float v1, v1

    aput v1, v10, v7

    invoke-virtual {v0}, Luj;->fZ()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    int-to-float v0, v0

    aput v0, v10, v8

    .line 741
    aget v0, v10, v7

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, v10, v7

    .line 742
    aget v0, v10, v8

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, v10, v8

    .line 744
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 745
    iget v2, p1, Luq;->x:I

    int-to-float v5, v2

    iget v2, p1, Luq;->y:I

    int-to-float v6, v2

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 748
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hv:Lja;

    invoke-virtual {v1}, Lja;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 749
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hv:Lja;

    invoke-virtual {v1, v8}, Lja;->h(Z)Lja;

    .line 752
    :cond_0
    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hv:Lja;

    invoke-virtual {v1, p0, v0}, Lja;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    .line 753
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 755
    if-eqz v1, :cond_2

    .line 756
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hh:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 772
    :cond_1
    :goto_0
    return-void

    .line 758
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    aget v1, v10, v7

    float-to-int v1, v1

    aget v2, v10, v8

    float-to-int v2, v2

    add-int/2addr v2, v9

    iget-object v5, p0, Lcom/android/launcher3/Folder;->He:[I

    move v3, v8

    move v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->c(IIII[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Folder;->He:[I

    .line 760
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getLayoutDirection()I

    move-result v0

    if-ne v0, v8, :cond_5

    move v0, v8

    :goto_1
    if-eqz v0, :cond_3

    .line 761
    iget-object v0, p0, Lcom/android/launcher3/Folder;->He:[I

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v1

    iget-object v2, p0, Lcom/android/launcher3/Folder;->He:[I

    aget v2, v2, v7

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v7

    .line 763
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Folder;->He:[I

    aget v0, v0, v7

    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hf:[I

    aget v1, v1, v7

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/launcher3/Folder;->He:[I

    aget v0, v0, v8

    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hf:[I

    aget v1, v1, v8

    if-eq v0, v1, :cond_1

    .line 765
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hh:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 766
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hh:Lro;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->HA:Lace;

    invoke-virtual {v0, v1}, Lro;->a(Lace;)V

    .line 767
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hh:Lro;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Lro;->e(J)V

    .line 768
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hf:[I

    iget-object v1, p0, Lcom/android/launcher3/Folder;->He:[I

    aget v1, v1, v7

    aput v1, v0, v7

    .line 769
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hf:[I

    iget-object v1, p0, Lcom/android/launcher3/Folder;->He:[I

    aget v1, v1, v8

    aput v1, v0, v8

    goto :goto_0

    :cond_5
    move v0, v7

    .line 760
    goto :goto_1
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x1

    return v0
.end method

.method public final e(Ladh;)V
    .locals 2

    .prologue
    .line 1310
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->h(Ladh;)Landroid/view/View;

    move-result-object v0

    .line 1311
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1312
    return-void
.end method

.method public final e(Luq;)V
    .locals 4

    .prologue
    .line 817
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hv:Lja;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lja;->h(Z)Lja;

    .line 820
    iget-boolean v0, p1, Luq;->Gc:Z

    if-nez v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hi:Lro;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->HB:Lace;

    invoke-virtual {v0, v1}, Lro;->a(Lace;)V

    .line 822
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hi:Lro;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Lro;->e(J)V

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hh:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 825
    return-void
.end method

.method public final eJ()Z
    .locals 1

    .prologue
    .line 956
    const/4 v0, 0x1

    return v0
.end method

.method public final ek()V
    .locals 0

    .prologue
    .line 915
    return-void
.end method

.method public final el()Z
    .locals 1

    .prologue
    .line 895
    const/4 v0, 0x1

    return v0
.end method

.method public final em()Z
    .locals 1

    .prologue
    .line 900
    const/4 v0, 0x0

    return v0
.end method

.method public final en()Z
    .locals 1

    .prologue
    .line 905
    const/4 v0, 0x1

    return v0
.end method

.method public final eo()F
    .locals 1

    .prologue
    .line 890
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final f(Ladh;)V
    .locals 8

    .prologue
    .line 1315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 1318
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hd:Z

    if-eqz v0, :cond_0

    .line 1327
    :goto_0
    return-void

    .line 1319
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->b(Ladh;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1321
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->be(I)V

    .line 1322
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->b(Ladh;)Z

    .line 1324
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->c(Ladh;)Landroid/view/View;

    .line 1325
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v1, Lvy;->id:J

    const-wide/16 v4, 0x0

    iget v6, p1, Ladh;->Bb:I

    iget v7, p1, Ladh;->Bc:I

    move-object v1, p1

    invoke-static/range {v0 .. v7}, Lzi;->a(Landroid/content/Context;Lwq;JJII)V

    goto :goto_0
.end method

.method public final g(Ladh;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1330
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 1333
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    if-ne p1, v0, :cond_1

    .line 1344
    :cond_0
    :goto_0
    return-void

    .line 1334
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/launcher3/Folder;->h(Ladh;)Landroid/view/View;

    move-result-object v0

    .line 1335
    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 1336
    iget v0, p0, Lcom/android/launcher3/Folder;->ag:I

    if-ne v0, v2, :cond_2

    .line 1337
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->GT:Z

    .line 1341
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 1342
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gG()V

    goto :goto_0

    .line 1339
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Folder;->be(I)V

    goto :goto_1
.end method

.method final gB()F
    .locals 1

    .prologue
    .line 1052
    iget v0, p0, Lcom/android/launcher3/Folder;->Hp:F

    return v0
.end method

.method final gC()F
    .locals 1

    .prologue
    .line 1055
    iget v0, p0, Lcom/android/launcher3/Folder;->Hq:F

    return v0
.end method

.method public final gI()V
    .locals 0

    .prologue
    .line 1359
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gH()V

    .line 1360
    return-void
.end method

.method public final gJ()Ljava/util/ArrayList;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1366
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->GZ:Z

    if-eqz v0, :cond_3

    .line 1367
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v0, v1

    .line 1368
    :goto_0
    iget-object v2, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v2

    if-ge v0, v2, :cond_2

    move v2, v1

    .line 1369
    :goto_1
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1370
    iget-object v3, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v3, v2, v0}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v3

    .line 1371
    if-eqz v3, :cond_0

    .line 1372
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GY:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1369
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1368
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1376
    :cond_2
    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->GZ:Z

    .line 1378
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GY:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getItemCount()I
    .locals 1

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final gp()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hr:Z

    return v0
.end method

.method public final gq()V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Lcom/android/launcher3/Folder;->EI:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 277
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->O(Z)V

    .line 278
    return-void
.end method

.method public final gr()Landroid/view/View;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    return-object v0
.end method

.method public final gs()Lcom/android/launcher3/CellLayout;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    return-object v0
.end method

.method public final gt()Lvy;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    return-object v0
.end method

.method public final gu()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const v6, 0x3f666666    # 0.9f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 596
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher3/DragLayer;

    if-nez v0, :cond_0

    .line 620
    :goto_0
    return-void

    .line 597
    :cond_0
    const-string v0, "alpha"

    new-array v1, v5, [F

    const/4 v2, 0x0

    aput v2, v1, v4

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 598
    const-string v1, "scaleX"

    new-array v2, v5, [F

    aput v6, v2, v4

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 599
    const-string v2, "scaleY"

    new-array v3, v5, [F

    aput v6, v3, v4

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 600
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    aput-object v2, v3, v7

    invoke-static {p0, v3}, Lyr;->a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 603
    new-instance v1, Lvi;

    invoke-direct {v1, p0}, Lvi;-><init>(Lcom/android/launcher3/Folder;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 617
    iget v1, p0, Lcom/android/launcher3/Folder;->GN:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 618
    const/4 v1, 0x0

    invoke-virtual {p0, v7, v1}, Lcom/android/launcher3/Folder;->setLayerType(ILandroid/graphics/Paint;)V

    .line 619
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public final gv()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 807
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hH()V

    .line 808
    iput-object v2, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    .line 809
    iput-object v2, p0, Lcom/android/launcher3/Folder;->Hb:Landroid/view/View;

    .line 810
    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->Hd:Z

    .line 811
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->GT:Z

    .line 812
    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->Hc:Z

    .line 813
    return-void
.end method

.method public final gw()V
    .locals 1

    .prologue
    .line 877
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hx:Z

    .line 878
    return-void
.end method

.method public final gz()V
    .locals 1

    .prologue
    .line 950
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->Hk:Z

    if-eqz v0, :cond_0

    .line 951
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hn:Z

    .line 953
    :cond_0
    return-void
.end method

.method public final h(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1363
    return-void
.end method

.method final isDestroyed()Z
    .locals 1

    .prologue
    .line 1218
    iget-boolean v0, p0, Lcom/android/launcher3/Folder;->bB:Z

    return v0
.end method

.method public final isFull()Z
    .locals 2

    .prologue
    .line 989
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    iget v1, p0, Lcom/android/launcher3/Folder;->GX:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 235
    instance-of v0, v0, Ladh;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Launcher;->onClick(Landroid/view/View;)V

    .line 238
    :cond_0
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 301
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 302
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->gq()V

    .line 303
    const/4 v0, 0x1

    .line 305
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/16 v2, 0x64

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 181
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 182
    const v0, 0x7f110161

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/launcher3/Folder;->GR:Landroid/widget/ScrollView;

    .line 183
    const v0, 0x7f11048a

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    iput-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    .line 185
    new-instance v0, Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/launcher3/FocusIndicatorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hu:Lcom/android/launcher3/FocusIndicatorView;

    .line 186
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hu:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0, v1, v3}, Lcom/android/launcher3/CellLayout;->addView(Landroid/view/View;I)V

    .line 187
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hu:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0}, Lcom/android/launcher3/FocusIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 188
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Hu:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0}, Lcom/android/launcher3/FocusIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 190
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 191
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    iget v2, v0, Ltu;->DT:I

    iget v0, v0, Ltu;->DU:I

    invoke-virtual {v1, v2, v0}, Lcom/android/launcher3/CellLayout;->z(II)V

    .line 194
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v3, v3}, Lcom/android/launcher3/CellLayout;->A(II)V

    .line 195
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0, v3}, Ladg;->setMotionEventSplittingEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/CellLayout;->F(Z)V

    .line 197
    const v0, 0x7f1103a8

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/FolderEditText;

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    .line 198
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/FolderEditText;->g(Lcom/android/launcher3/Folder;)V

    .line 199
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/FolderEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 203
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0, v3, v3}, Lcom/android/launcher3/FolderEditText;->measure(II)V

    .line 205
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderEditText;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Folder;->Hj:I

    .line 208
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->Hz:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderEditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 209
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/FolderEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 210
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/FolderEditText;->setSelectAllOnFocus(Z)V

    .line 211
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {v1}, Lcom/android/launcher3/FolderEditText;->getInputType()I

    move-result v1

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    or-int/lit16 v1, v1, 0x2000

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderEditText;->setInputType(I)V

    .line 213
    new-instance v0, Lvn;

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GR:Landroid/widget/ScrollView;

    invoke-direct {v0, v1}, Lvn;-><init>(Landroid/widget/ScrollView;)V

    iput-object v0, p0, Lcom/android/launcher3/Folder;->Hv:Lja;

    .line 214
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 1387
    iget-object v0, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderEditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Folder;->Hr:Z

    .line 1389
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 242
    iget-object v0, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hh()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 263
    :goto_0
    return v0

    .line 244
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 245
    instance-of v3, v0, Ladh;

    if-eqz v3, :cond_2

    .line 246
    check-cast v0, Ladh;

    .line 247
    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v2

    .line 248
    goto :goto_0

    .line 251
    :cond_1
    iget-object v3, p0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v3

    invoke-virtual {v3, p1, p0}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Lui;)V

    .line 253
    iput-object v0, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    .line 254
    iget-object v3, p0, Lcom/android/launcher3/Folder;->Hg:[I

    iget v4, v0, Ladh;->Bb:I

    aput v4, v3, v2

    .line 255
    iget-object v3, p0, Lcom/android/launcher3/Folder;->Hg:[I

    iget v0, v0, Ladh;->Bc:I

    aput v0, v3, v1

    .line 256
    iput-object p1, p0, Lcom/android/launcher3/Folder;->Hb:Landroid/view/View;

    .line 258
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    iget-object v3, p0, Lcom/android/launcher3/Folder;->Hb:Landroid/view/View;

    invoke-virtual {v0, v3}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 259
    iget-object v0, p0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-object v3, p0, Lcom/android/launcher3/Folder;->Ha:Ladh;

    invoke-virtual {v0, v3}, Lvy;->k(Ladh;)V

    .line 260
    iput-boolean v1, p0, Lcom/android/launcher3/Folder;->Hk:Z

    .line 261
    iput-boolean v2, p0, Lcom/android/launcher3/Folder;->Hn:Z

    :cond_2
    move v0, v1

    .line 263
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 1094
    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Folder;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->fh()I

    move-result v1

    add-int/2addr v0, v1

    .line 1095
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gF()I

    move-result v1

    .line 1096
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gE()I

    move-result v2

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1098
    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gD()I

    move-result v3

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1101
    invoke-static {}, Lyu;->im()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1103
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gE()I

    move-result v5

    iget-object v6, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v6}, Lcom/android/launcher3/CellLayout;->fi()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/android/launcher3/CellLayout;->setFixedSize(II)V

    .line 1108
    :goto_0
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v4, v2, v3}, Landroid/widget/ScrollView;->measure(II)V

    .line 1109
    iget-object v3, p0, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    iget v4, p0, Lcom/android/launcher3/Folder;->Hj:I

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/android/launcher3/FolderEditText;->measure(II)V

    .line 1111
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Folder;->setMeasuredDimension(II)V

    .line 1112
    return-void

    .line 1105
    :cond_0
    iget-object v4, p0, Lcom/android/launcher3/Folder;->GQ:Lcom/android/launcher3/CellLayout;

    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gE()I

    move-result v5

    invoke-direct {p0}, Lcom/android/launcher3/Folder;->gD()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/android/launcher3/CellLayout;->setFixedSize(II)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    return v0
.end method
