.class public Lcom/android/launcher3/CellLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static final AM:Landroid/graphics/Paint;


# instance fields
.field private AA:Z

.field private final AB:[I

.field private AC:Z

.field private AD:Landroid/animation/TimeInterpolator;

.field private AE:Ladg;

.field private AF:Z

.field private AG:F

.field private AH:F

.field private AI:Ljava/util/ArrayList;

.field private AJ:Landroid/graphics/Rect;

.field private AK:[I

.field private AL:Lup;

.field private final AN:Ljava/util/Stack;

.field private Aa:Ljava/util/ArrayList;

.field private Ab:[I

.field private Ac:F

.field private Ad:I

.field private Ae:F

.field private Af:F

.field private Ag:Z

.field private Ah:Landroid/graphics/drawable/Drawable;

.field private Ai:Landroid/graphics/drawable/Drawable;

.field private Aj:Landroid/graphics/drawable/Drawable;

.field private Ak:Landroid/graphics/drawable/Drawable;

.field private Al:Landroid/graphics/drawable/Drawable;

.field private Am:Landroid/graphics/Rect;

.field private An:Landroid/graphics/Rect;

.field private Ao:I

.field private Ap:I

.field private Aq:Z

.field private Ar:Z

.field private As:[Landroid/graphics/Rect;

.field private At:[F

.field private Au:[Lwo;

.field private Av:I

.field private final Aw:Landroid/graphics/Paint;

.field private final Ax:Luu;

.field private Ay:Ljava/util/HashMap;

.field private Az:Ljava/util/HashMap;

.field private gn:Landroid/graphics/Rect;

.field private sg:I

.field private xW:[I

.field private xZ:Lcom/android/launcher3/Launcher;

.field private zG:I

.field private zH:I

.field private zI:I

.field private zJ:I

.field private zK:I

.field private zL:I

.field private zM:I

.field private zN:I

.field private zO:I

.field private zP:I

.field private zQ:I

.field private zR:Z

.field private zS:Z

.field private final zT:[I

.field private final zU:[I

.field private zV:[I

.field private zW:[[Z

.field private zX:[[Z

.field private zY:Z

.field private zZ:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/launcher3/CellLayout;->AM:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 178
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    const/4 v4, 0x1

    const/4 v2, 0x2

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 181
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->zR:Z

    .line 75
    iput-boolean v4, p0, Lcom/android/launcher3/CellLayout;->zS:Z

    .line 79
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zT:[I

    .line 80
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    .line 81
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    .line 85
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->zY:Z

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Aa:Ljava/util/ArrayList;

    .line 90
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    .line 92
    const v0, 0x3f266666    # 0.65f

    iput v0, p0, Lcom/android/launcher3/CellLayout;->Ac:F

    .line 93
    iput v1, p0, Lcom/android/launcher3/CellLayout;->Ad:I

    .line 95
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/launcher3/CellLayout;->Af:F

    .line 96
    iput-boolean v4, p0, Lcom/android/launcher3/CellLayout;->Ag:Z

    .line 108
    iput v5, p0, Lcom/android/launcher3/CellLayout;->Ap:I

    .line 109
    iput v5, p0, Lcom/android/launcher3/CellLayout;->sg:I

    .line 112
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->Aq:Z

    .line 113
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->Ar:Z

    .line 117
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    .line 118
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->At:[F

    .line 119
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    array-length v0, v0

    new-array v0, v0, [Lwo;

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    .line 123
    iput v1, p0, Lcom/android/launcher3/CellLayout;->Av:I

    .line 124
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Aw:Landroid/graphics/Paint;

    .line 128
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Ay:Ljava/util/HashMap;

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    .line 133
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    .line 136
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    .line 138
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AC:Z

    .line 143
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AF:Z

    .line 144
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/launcher3/CellLayout;->AG:F

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    .line 162
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AJ:Landroid/graphics/Rect;

    .line 163
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    .line 164
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    .line 168
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->gn:Landroid/graphics/Rect;

    .line 1216
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AN:Ljava/util/Stack;

    .line 182
    new-instance v0, Lup;

    invoke-direct {v0, p1}, Lup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AL:Lup;

    .line 186
    invoke-virtual {p0, v1}, Lcom/android/launcher3/CellLayout;->setWillNotDraw(Z)V

    .line 187
    invoke-virtual {p0, v1}, Lcom/android/launcher3/CellLayout;->setClipToPadding(Z)V

    move-object v0, p1

    .line 188
    check-cast v0, Lcom/android/launcher3/Launcher;

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->xZ:Lcom/android/launcher3/Launcher;

    .line 190
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 191
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v7

    .line 192
    sget-object v0, Ladb;->St:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 194
    iput v5, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iput v5, p0, Lcom/android/launcher3/CellLayout;->zG:I

    .line 195
    iput v5, p0, Lcom/android/launcher3/CellLayout;->zJ:I

    iput v5, p0, Lcom/android/launcher3/CellLayout;->zI:I

    .line 196
    iput v1, p0, Lcom/android/launcher3/CellLayout;->zM:I

    iput v1, p0, Lcom/android/launcher3/CellLayout;->zO:I

    .line 197
    iput v1, p0, Lcom/android/launcher3/CellLayout;->zN:I

    iput v1, p0, Lcom/android/launcher3/CellLayout;->zP:I

    .line 198
    const v0, 0x7fffffff

    iput v0, p0, Lcom/android/launcher3/CellLayout;->zQ:I

    .line 199
    iget v0, v7, Ltu;->Dh:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    .line 200
    iget v0, v7, Ltu;->Dg:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/CellLayout;->zL:I

    .line 201
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    .line 202
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    .line 203
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/16 v3, -0x64

    aput v3, v0, v1

    .line 204
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/16 v3, -0x64

    aput v3, v0, v4

    .line 206
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 208
    invoke-virtual {p0, v1}, Lcom/android/launcher3/CellLayout;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 210
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 211
    iget v0, v7, Ltu;->DW:I

    int-to-float v0, v0

    iget v3, v7, Ltu;->DI:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    iput v0, p0, Lcom/android/launcher3/CellLayout;->AG:F

    .line 213
    const v0, 0x7f0202a5

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Ah:Landroid/graphics/drawable/Drawable;

    .line 214
    const v0, 0x7f0202a6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Ai:Landroid/graphics/drawable/Drawable;

    .line 216
    const v0, 0x7f020278

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Ak:Landroid/graphics/drawable/Drawable;

    .line 217
    const v0, 0x7f020279

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Al:Landroid/graphics/drawable/Drawable;

    .line 218
    const v0, 0x7f0d0076

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/CellLayout;->Ao:I

    .line 221
    const v0, 0x3df5c28f    # 0.12f

    iget v3, v7, Ltu;->DI:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/android/launcher3/CellLayout;->AH:F

    .line 224
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ah:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 225
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ai:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 228
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40200000    # 2.5f

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AD:Landroid/animation/TimeInterpolator;

    .line 229
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    aput v5, v3, v4

    aput v5, v0, v1

    move v0, v1

    .line 230
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 231
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v4, v3, v0

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    const v0, 0x7f0c001e

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 239
    const v0, 0x7f0c001f

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v5, v0

    .line 242
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->At:[F

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    move v6, v1

    .line 244
    :goto_1
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    array-length v0, v0

    if-ge v6, v0, :cond_1

    .line 245
    new-instance v0, Lwo;

    int-to-long v2, v8

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lwo;-><init>(Landroid/view/View;JFF)V

    .line 247
    iget-object v1, v0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->AD:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 249
    iget-object v1, v0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    new-instance v2, Lst;

    invoke-direct {v2, p0, v0, v6}, Lst;-><init>(Lcom/android/launcher3/CellLayout;Lwo;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 273
    iget-object v1, v0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    new-instance v2, Lsu;

    invoke-direct {v2, p0, v0}, Lsu;-><init>(Lcom/android/launcher3/CellLayout;Lwo;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 281
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    aput-object v0, v1, v6

    .line 244
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    .line 284
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Am:Landroid/graphics/Rect;

    .line 285
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->An:Landroid/graphics/Rect;

    .line 287
    new-instance v0, Ladg;

    invoke-direct {v0, p1}, Ladg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    .line 288
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zO:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zP:I

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v6, p0, Lcom/android/launcher3/CellLayout;->zL:I

    invoke-virtual/range {v0 .. v6}, Ladg;->b(IIIIII)V

    .line 291
    new-instance v0, Luu;

    invoke-direct {v0, p1}, Luu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    .line 293
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    iget v1, v7, Ltu;->DL:I

    int-to-double v2, v1

    mul-double/2addr v2, v10

    double-to-int v1, v2

    iget v2, v7, Ltu;->DM:I

    int-to-double v2, v2

    mul-double/2addr v2, v10

    double-to-int v2, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher3/CellLayout;->addView(Landroid/view/View;II)V

    .line 294
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/CellLayout;->addView(Landroid/view/View;)V

    .line 295
    return-void

    .line 90
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method static I(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2861
    if-eqz p0, :cond_0

    .line 2862
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2863
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bv:Z

    .line 2864
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 2866
    :cond_0
    return-void
.end method

.method private J(Z)V
    .locals 3

    .prologue
    .line 2321
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v2

    .line 2322
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2323
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v1}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2324
    iput-boolean p1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    .line 2322
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2326
    :cond_0
    return-void
.end method

.method private a(IIIIIILsz;)Lsz;
    .locals 11

    .prologue
    .line 2330
    const/4 v1, 0x2

    new-array v9, v1, [I

    .line 2331
    const/4 v1, 0x2

    new-array v10, v1, [I

    .line 2332
    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[I)[I

    .line 2334
    const/4 v1, 0x0

    aget v1, v9, v1

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    aget v1, v9, v1

    if-ltz v1, :cond_0

    .line 2335
    const/4 v1, 0x0

    move-object/from16 v0, p7

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/CellLayout;->a(Lsz;Z)V

    .line 2336
    const/4 v1, 0x0

    aget v1, v9, v1

    move-object/from16 v0, p7

    iput v1, v0, Lsz;->Bj:I

    .line 2337
    const/4 v1, 0x1

    aget v1, v9, v1

    move-object/from16 v0, p7

    iput v1, v0, Lsz;->Bk:I

    .line 2338
    const/4 v1, 0x0

    aget v1, v10, v1

    move-object/from16 v0, p7

    iput v1, v0, Lsz;->Bl:I

    .line 2339
    const/4 v1, 0x1

    aget v1, v10, v1

    move-object/from16 v0, p7

    iput v1, v0, Lsz;->Bm:I

    .line 2340
    const/4 v1, 0x1

    move-object/from16 v0, p7

    iput-boolean v1, v0, Lsz;->Bi:Z

    .line 2344
    :goto_0
    return-object p7

    .line 2342
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p7

    iput-boolean v1, v0, Lsz;->Bi:Z

    goto :goto_0
.end method

.method private a(IIIIII[ILandroid/view/View;ZLsz;)Lsz;
    .locals 21

    .prologue
    .line 2035
    move-object/from16 v12, p8

    move-object/from16 v11, p7

    move/from16 v7, p6

    move/from16 v6, p5

    move/from16 v5, p2

    move/from16 v4, p1

    move-object/from16 v3, p0

    :goto_0
    const/4 v8, 0x0

    move-object/from16 v0, p10

    invoke-direct {v3, v0, v8}, Lcom/android/launcher3/CellLayout;->a(Lsz;Z)V

    .line 2038
    iget-object v10, v3, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v8, 0x0

    :goto_1
    iget v9, v3, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge v8, v9, :cond_1

    const/4 v9, 0x0

    :goto_2
    iget v13, v3, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge v9, v13, :cond_0

    aget-object v13, v10, v8

    iget-object v14, v3, Lcom/android/launcher3/CellLayout;->zW:[[Z

    aget-object v14, v14, v8

    aget-boolean v14, v14, v9

    aput-boolean v14, v13, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 2042
    :cond_1
    const/4 v8, 0x2

    new-array v8, v8, [I

    .line 2043
    invoke-virtual/range {v3 .. v8}, Lcom/android/launcher3/CellLayout;->c(IIII[I)[I

    move-result-object v14

    .line 2045
    const/4 v8, 0x0

    aget v9, v14, v8

    const/4 v8, 0x1

    aget v10, v14, v8

    if-ltz v9, :cond_2

    if-gez v10, :cond_4

    :cond_2
    const/4 v8, 0x0

    .line 2051
    :goto_3
    if-nez v8, :cond_d

    .line 2054
    move/from16 v0, p3

    if-le v6, v0, :cond_b

    move/from16 v0, p4

    if-eq v0, v7, :cond_3

    if-eqz p9, :cond_b

    .line 2055
    :cond_3
    add-int/lit8 v6, v6, -0x1

    const/16 p9, 0x0

    goto :goto_0

    .line 2045
    :cond_4
    iget-object v8, v3, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    iget-object v8, v3, Lcom/android/launcher3/CellLayout;->AJ:Landroid/graphics/Rect;

    add-int v13, v9, v6

    add-int v15, v10, v7

    invoke-virtual {v8, v9, v10, v13, v15}, Landroid/graphics/Rect;->set(IIII)V

    if-eqz v12, :cond_5

    move-object/from16 v0, p10

    iget-object v8, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v8, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lsx;

    if-eqz v8, :cond_5

    iput v9, v8, Lsx;->x:I

    iput v10, v8, Lsx;->y:I

    :cond_5
    new-instance v13, Landroid/graphics/Rect;

    add-int v8, v9, v6

    add-int v15, v10, v7

    invoke-direct {v13, v9, v10, v8, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p10

    iget-object v8, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_6
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Landroid/view/View;

    if-eq v9, v12, :cond_6

    move-object/from16 v0, p10

    iget-object v8, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v10, v8

    check-cast v10, Lsx;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Lcom/android/launcher3/CellLayout$LayoutParams;

    iget v0, v10, Lsx;->x:I

    move/from16 v17, v0

    iget v0, v10, Lsx;->y:I

    move/from16 v18, v0

    iget v0, v10, Lsx;->x:I

    move/from16 v19, v0

    iget v0, v10, Lsx;->AY:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v10, Lsx;->y:I

    move/from16 v20, v0

    iget v10, v10, Lsx;->AZ:I

    add-int v10, v10, v20

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v15, v0, v1, v2, v10}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {v13, v15}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-boolean v8, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    if-nez v8, :cond_7

    const/4 v8, 0x0

    goto/16 :goto_3

    :cond_7
    iget-object v8, v3, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    new-instance v8, Ljava/util/ArrayList;

    iget-object v9, v3, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p10

    iput-object v8, v0, Lsz;->Bh:Ljava/util/ArrayList;

    iget-object v9, v3, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    iget-object v10, v3, Lcom/android/launcher3/CellLayout;->AJ:Landroid/graphics/Rect;

    move-object v8, v3

    move-object/from16 v13, p10

    invoke-direct/range {v8 .. v13}, Lcom/android/launcher3/CellLayout;->b(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v8

    if-nez v8, :cond_a

    iget-object v8, v3, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    iget-object v9, v3, Lcom/android/launcher3/CellLayout;->AJ:Landroid/graphics/Rect;

    move-object/from16 v0, p10

    invoke-direct {v3, v8, v9, v11, v0}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILsz;)Z

    move-result v8

    if-nez v8, :cond_a

    iget-object v8, v3, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    iget-object v10, v3, Lcom/android/launcher3/CellLayout;->AJ:Landroid/graphics/Rect;

    move-object/from16 v0, p10

    invoke-direct {v3, v8, v10, v11, v0}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;Landroid/graphics/Rect;[ILsz;)Z

    move-result v8

    if-nez v8, :cond_9

    const/4 v8, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v8, 0x1

    goto/16 :goto_3

    .line 2057
    :cond_b
    move/from16 v0, p4

    if-le v7, v0, :cond_c

    .line 2058
    add-int/lit8 v7, v7, -0x1

    const/16 p9, 0x1

    goto/16 :goto_0

    .line 2061
    :cond_c
    const/4 v3, 0x0

    move-object/from16 v0, p10

    iput-boolean v3, v0, Lsz;->Bi:Z

    .line 2069
    :goto_5
    return-object p10

    .line 2063
    :cond_d
    const/4 v3, 0x1

    move-object/from16 v0, p10

    iput-boolean v3, v0, Lsz;->Bi:Z

    .line 2064
    const/4 v3, 0x0

    aget v3, v14, v3

    move-object/from16 v0, p10

    iput v3, v0, Lsz;->Bj:I

    .line 2065
    const/4 v3, 0x1

    aget v3, v14, v3

    move-object/from16 v0, p10

    iput v3, v0, Lsz;->Bk:I

    .line 2066
    move-object/from16 v0, p10

    iput v6, v0, Lsz;->Bl:I

    .line 2067
    move-object/from16 v0, p10

    iput v7, v0, Lsz;->Bm:I

    goto :goto_5
.end method

.method private a(IIIILandroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 775
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v0

    .line 776
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v1

    .line 777
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zO:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p1

    add-int/2addr v0, v2

    .line 778
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zP:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    .line 779
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v2, p3

    add-int/lit8 v3, p3, -0x1

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zO:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v2, v0

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zH:I

    mul-int/2addr v3, p4

    add-int/lit8 v4, p4, -0x1

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zP:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    add-int/2addr v3, v1

    invoke-virtual {p5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 781
    return-void
.end method

.method private a(IIIILandroid/view/View;Landroid/graphics/Rect;Ljava/util/ArrayList;)V
    .locals 11

    .prologue
    .line 2400
    if-eqz p6, :cond_0

    .line 2401
    add-int v1, p1, p3

    add-int v2, p2, p4

    move-object/from16 v0, p6

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 2403
    :cond_0
    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->clear()V

    .line 2404
    new-instance v3, Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-direct {v3, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2405
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2406
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v1}, Ladg;->getChildCount()I

    move-result v5

    .line 2407
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    .line 2408
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v1, v2}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2409
    move-object/from16 v0, p5

    if-eq v6, v0, :cond_1

    .line 2410
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2411
    iget v7, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v8, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v9, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v10, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    add-int/2addr v9, v10

    iget v10, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v1, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    add-int/2addr v1, v10

    invoke-virtual {v4, v7, v8, v9, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 2412
    invoke-static {v3, v4}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2413
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2414
    if-eqz p6, :cond_1

    .line 2415
    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 2407
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2419
    :cond_2
    return-void
.end method

.method private a(IIII[[ZZ)V
    .locals 3

    .prologue
    .line 3030
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 3036
    :cond_0
    return-void

    :cond_1
    move v1, p1

    .line 3031
    :goto_0
    add-int v0, p1, p3

    if-ge v1, v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge v1, v0, :cond_0

    move v0, p2

    .line 3032
    :goto_1
    add-int v2, p2, p4

    if-ge v0, v2, :cond_2

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge v0, v2, :cond_2

    .line 3033
    aget-object v2, p5, v1

    aput-boolean p6, v2, v0

    .line 3032
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3031
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(II[I)V
    .locals 5

    .prologue
    .line 731
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v0

    .line 732
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v1

    .line 734
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zO:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p1

    add-int/2addr v0, v3

    aput v0, p3, v2

    .line 735
    const/4 v0, 0x1

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zP:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    aput v1, p3, v0

    .line 736
    return-void
.end method

.method private a(Landroid/graphics/Rect;[[ZZ)V
    .locals 7

    .prologue
    .line 1850
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 1851
    return-void
.end method

.method private a(Landroid/view/View;[[Z)V
    .locals 7

    .prologue
    .line 3014
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    if-eq v0, v1, :cond_1

    .line 3017
    :cond_0
    :goto_0
    return-void

    .line 3015
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 3016
    iget v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_0
.end method

.method private a(Ljava/util/Stack;)V
    .locals 2

    .prologue
    .line 1226
    :goto_0
    invoke-virtual {p1}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1227
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AN:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1229
    :cond_0
    return-void
.end method

.method private a(Lsz;Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2088
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge v0, v2, :cond_1

    move v2, v1

    .line 2089
    :goto_1
    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge v2, v3, :cond_0

    .line 2090
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    aget-object v3, v3, v0

    aput-boolean v1, v3, v2

    .line 2089
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2088
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2094
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v8

    move v7, v1

    .line 2095
    :goto_2
    if-ge v7, v8, :cond_3

    .line 2096
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v7}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2097
    if-eq v1, p2, :cond_2

    .line 2098
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2099
    iget-object v2, p1, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lsx;

    .line 2100
    if-eqz v4, :cond_2

    .line 2101
    iget v1, v4, Lsx;->x:I

    iput v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    .line 2102
    iget v1, v4, Lsx;->y:I

    iput v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    .line 2103
    iget v1, v4, Lsx;->AY:I

    iput v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 2104
    iget v1, v4, Lsx;->AZ:I

    iput v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 2105
    iget v1, v4, Lsx;->x:I

    iget v2, v4, Lsx;->y:I

    iget v3, v4, Lsx;->AY:I

    iget v4, v4, Lsx;->AZ:I

    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 2095
    :cond_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_2

    .line 2108
    :cond_3
    iget v1, p1, Lsz;->Bj:I

    iget v2, p1, Lsz;->Bk:I

    iget v3, p1, Lsz;->Bl:I

    iget v4, p1, Lsz;->Bm:I

    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 2110
    return-void
.end method

.method private a(Lsz;Landroid/view/View;I)V
    .locals 12

    .prologue
    .line 2143
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v11

    .line 2144
    const/4 v0, 0x0

    move v10, v0

    :goto_0
    if-ge v10, v11, :cond_6

    .line 2145
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v10}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2146
    if-eq v2, p2, :cond_1

    .line 2147
    iget-object v0, p1, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lsx;

    .line 2148
    if-nez p3, :cond_2

    iget-object v0, p1, Lsz;->Bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lsz;->Bh:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v4, v0

    .line 2151
    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2152
    if-eqz v1, :cond_1

    if-nez v4, :cond_1

    .line 2153
    new-instance v0, Lta;

    iget v4, v3, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v5, v3, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v6, v1, Lsx;->x:I

    iget v7, v1, Lsx;->y:I

    iget v8, v1, Lsx;->AY:I

    iget v9, v1, Lsx;->AZ:I

    move-object v1, p0

    move v3, p3

    invoke-direct/range {v0 .. v9}, Lta;-><init>(Lcom/android/launcher3/CellLayout;Landroid/view/View;IIIIIII)V

    .line 2155
    iget-object v1, v0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    iget-object v1, v1, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    iget-object v2, v0, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    iget-object v1, v1, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    iget-object v2, v0, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lta;

    iget-object v2, v1, Lta;->BE:Landroid/animation/Animator;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lta;->BE:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    :cond_0
    iget-object v1, v0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    iget-object v1, v1, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    iget-object v2, v0, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v0, Lta;->Bx:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_3

    iget v1, v0, Lta;->By:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lta;->fl()V

    .line 2144
    :cond_1
    :goto_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_0

    .line 2148
    :cond_2
    const/4 v0, 0x0

    move v4, v0

    goto :goto_1

    .line 2155
    :cond_3
    iget v1, v0, Lta;->Bx:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_4

    iget v1, v0, Lta;->By:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    :cond_4
    iget-object v1, v0, Lta;->Bw:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, v0, Lta;->BE:Landroid/animation/Animator;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget v2, v0, Lta;->mode:I

    if-nez v2, :cond_5

    const-wide/16 v2, 0x15e

    :goto_3
    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v2, Ltb;

    invoke-direct {v2, v0}, Ltb;-><init>(Lta;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Ltc;

    invoke-direct {v2, v0}, Ltc;-><init>(Lta;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v2, v0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    iget-object v2, v2, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    iget-object v3, v0, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_2

    :cond_5
    const-wide/16 v2, 0x12c

    goto :goto_3

    .line 2158
    :cond_6
    return-void

    .line 2155
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Lsz;Landroid/view/View;Z)V
    .locals 12

    .prologue
    .line 2115
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    move-object v8, v0

    check-cast v8, [[Z

    .line 2116
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge v0, v1, :cond_1

    .line 2117
    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge v1, v2, :cond_0

    .line 2118
    aget-object v2, v8, v0

    const/4 v3, 0x0

    aput-boolean v3, v2, v1

    .line 2117
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2116
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2122
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v11

    .line 2123
    const/4 v0, 0x0

    move v10, v0

    :goto_2
    if-ge v10, v11, :cond_3

    .line 2124
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v10}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2125
    if-eq v1, p2, :cond_2

    .line 2126
    iget-object v0, p1, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lsx;

    .line 2127
    if-eqz v9, :cond_2

    .line 2128
    iget v2, v9, Lsx;->x:I

    iget v3, v9, Lsx;->y:I

    const/16 v4, 0x96

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    .line 2130
    iget v1, v9, Lsx;->x:I

    iget v2, v9, Lsx;->y:I

    iget v3, v9, Lsx;->AY:I

    iget v4, v9, Lsx;->AZ:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, v8

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 2123
    :cond_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    .line 2133
    :cond_3
    if-eqz p3, :cond_4

    .line 2134
    iget v1, p1, Lsz;->Bj:I

    iget v2, p1, Lsz;->Bk:I

    iget v3, p1, Lsz;->Bl:I

    iget v4, p1, Lsz;->Bm:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, v8

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 2137
    :cond_4
    return-void
.end method

.method private a(Lsz;Z)V
    .locals 9

    .prologue
    .line 2073
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v7

    .line 2074
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_0

    .line 2075
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v6}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 2076
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2078
    new-instance v0, Lsx;

    iget v2, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v3, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v4, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iget v5, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lsx;-><init>(Lcom/android/launcher3/CellLayout;IIII)V

    .line 2083
    iget-object v1, p1, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v1, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lsz;->Bf:Ljava/util/HashMap;

    new-instance v1, Lsx;

    iget-object v2, p1, Lsz;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-direct {v1, v2}, Lsx;-><init>(Lcom/android/launcher3/CellLayout;)V

    invoke-virtual {v0, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lsz;->Bg:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2074
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2085
    :cond_0
    return-void
.end method

.method public static a(Lwq;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2943
    instance-of v0, p0, Lyy;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 2944
    check-cast v0, Lyy;

    iget v1, v0, Lyy;->minWidth:I

    move-object v0, p0

    .line 2945
    check-cast v0, Lyy;

    iget v0, v0, Lyy;->minHeight:I

    .line 2954
    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/android/launcher3/CellLayout;->c(II[I)[I

    move-result-object v0

    .line 2955
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lwq;->AY:I

    .line 2956
    aget v0, v0, v3

    iput v0, p0, Lwq;->AZ:I

    .line 2957
    :goto_1
    return-void

    .line 2946
    :cond_0
    instance-of v0, p0, Lacy;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 2947
    check-cast v0, Lacy;

    iget v1, v0, Lacy;->minWidth:I

    move-object v0, p0

    .line 2948
    check-cast v0, Lacy;

    iget v0, v0, Lacy;->minHeight:I

    goto :goto_0

    .line 2951
    :cond_1
    iput v3, p0, Lwq;->AZ:I

    iput v3, p0, Lwq;->AY:I

    goto :goto_1
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;[ILsz;)Z
    .locals 11

    .prologue
    .line 1446
    iget-object v0, p4, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lsx;

    .line 1447
    const/4 v10, 0x0

    .line 1448
    iget v1, v9, Lsx;->x:I

    iget v2, v9, Lsx;->y:I

    iget v3, v9, Lsx;->AY:I

    iget v4, v9, Lsx;->AZ:I

    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 1449
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Lcom/android/launcher3/CellLayout;->a(Landroid/graphics/Rect;[[ZZ)V

    .line 1451
    iget v1, v9, Lsx;->x:I

    iget v2, v9, Lsx;->y:I

    iget v3, v9, Lsx;->AY:I

    iget v4, v9, Lsx;->AZ:I

    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/android/launcher3/CellLayout;->a(IIII[I[[Z[[Z[I)[I

    .line 1453
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-ltz v0, :cond_0

    .line 1454
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, v9, Lsx;->x:I

    .line 1455
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, v9, Lsx;->y:I

    .line 1456
    const/4 v0, 0x1

    move v7, v0

    .line 1458
    :goto_0
    iget v1, v9, Lsx;->x:I

    iget v2, v9, Lsx;->y:I

    iget v3, v9, Lsx;->AY:I

    iget v4, v9, Lsx;->AZ:I

    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    .line 1459
    return v7

    :cond_0
    move v7, v10

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z
    .locals 13

    .prologue
    .line 1700
    new-instance v11, Ltd;

    move-object/from16 v0, p5

    invoke-direct {v11, p0, p1, v0}, Ltd;-><init>(Lcom/android/launcher3/CellLayout;Ljava/util/ArrayList;Lsz;)V

    .line 1701
    invoke-virtual {v11}, Ltd;->fn()Landroid/graphics/Rect;

    move-result-object v2

    .line 1704
    const/4 v8, 0x0

    .line 1708
    const/4 v1, 0x0

    aget v1, p3, v1

    if-gez v1, :cond_1

    .line 1709
    const/4 v1, 0x0

    .line 1710
    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->left:I

    sub-int v9, v2, v3

    move v10, v1

    .line 1723
    :goto_0
    if-gtz v9, :cond_4

    .line 1724
    const/4 v8, 0x0

    .line 1789
    :cond_0
    return v8

    .line 1711
    :cond_1
    const/4 v1, 0x0

    aget v1, p3, v1

    if-lez v1, :cond_2

    .line 1712
    const/4 v1, 0x2

    .line 1713
    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v9, v3, v2

    move v10, v1

    goto :goto_0

    .line 1714
    :cond_2
    const/4 v1, 0x1

    aget v1, p3, v1

    if-gez v1, :cond_3

    .line 1715
    const/4 v1, 0x1

    .line 1716
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    sub-int v9, v2, v3

    move v10, v1

    goto :goto_0

    .line 1718
    :cond_3
    const/4 v1, 0x3

    .line 1719
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v9, v3, v2

    move v10, v1

    goto :goto_0

    .line 1728
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1729
    move-object/from16 v0, p5

    iget-object v2, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    .line 1730
    iget v2, v1, Lsx;->x:I

    iget v3, v1, Lsx;->y:I

    iget v4, v1, Lsx;->AY:I

    iget v5, v1, Lsx;->AZ:I

    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_1

    .line 1736
    :cond_5
    move-object/from16 v0, p5

    iget-object v1, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    move-object/from16 v0, p5

    iget-object v2, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    move-object/from16 v0, p5

    iget-object v4, v0, Lsz;->Bf:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    invoke-virtual {v2, v1}, Lsx;->a(Lsx;)V

    goto :goto_2

    .line 1741
    :cond_6
    iget-object v1, v11, Ltd;->BS:Lte;

    iput v10, v1, Lte;->BT:I

    iget-object v1, v11, Ltd;->BH:Lsz;

    iget-object v1, v1, Lsz;->Bg:Ljava/util/ArrayList;

    iget-object v2, v11, Ltd;->BS:Lte;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1743
    :goto_3
    if-lez v9, :cond_13

    if-nez v8, :cond_13

    .line 1744
    move-object/from16 v0, p5

    iget-object v1, v0, Lsz;->Bg:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_7
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/view/View;

    .line 1748
    iget-object v1, v11, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    move-object/from16 v0, p4

    if-eq v2, v0, :cond_7

    .line 1749
    iget-object v1, v11, Ltd;->BH:Lsz;

    iget-object v1, v1, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    packed-switch v10, :pswitch_data_0

    iget-boolean v3, v11, Ltd;->BQ:Z

    if-eqz v3, :cond_8

    const/4 v3, 0x3

    iget-object v4, v11, Ltd;->BM:[I

    invoke-virtual {v11, v3, v4}, Ltd;->a(I[I)V

    :cond_8
    iget-object v3, v11, Ltd;->BM:[I

    move-object v4, v3

    :goto_5
    packed-switch v10, :pswitch_data_1

    :cond_9
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_7

    .line 1750
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 1751
    iget-boolean v1, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    if-nez v1, :cond_11

    .line 1753
    const/4 v1, 0x1

    move v2, v1

    .line 1764
    :goto_7
    add-int/lit8 v3, v9, -0x1

    .line 1768
    iget-object v1, v11, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v5, v11, Ltd;->BH:Lsz;

    iget-object v5, v5, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    packed-switch v10, :pswitch_data_2

    iget v5, v1, Lsx;->y:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, Lsx;->y:I

    goto :goto_8

    .line 1749
    :pswitch_0
    iget-boolean v3, v11, Ltd;->BN:Z

    if-eqz v3, :cond_a

    const/4 v3, 0x0

    iget-object v4, v11, Ltd;->BJ:[I

    invoke-virtual {v11, v3, v4}, Ltd;->a(I[I)V

    :cond_a
    iget-object v3, v11, Ltd;->BJ:[I

    move-object v4, v3

    goto :goto_5

    :pswitch_1
    iget-boolean v3, v11, Ltd;->BO:Z

    if-eqz v3, :cond_b

    const/4 v3, 0x2

    iget-object v4, v11, Ltd;->BK:[I

    invoke-virtual {v11, v3, v4}, Ltd;->a(I[I)V

    :cond_b
    iget-object v3, v11, Ltd;->BK:[I

    move-object v4, v3

    goto :goto_5

    :pswitch_2
    iget-boolean v3, v11, Ltd;->BP:Z

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    iget-object v4, v11, Ltd;->BL:[I

    invoke-virtual {v11, v3, v4}, Ltd;->a(I[I)V

    :cond_c
    iget-object v3, v11, Ltd;->BL:[I

    move-object v4, v3

    goto :goto_5

    :pswitch_3
    iget v3, v1, Lsx;->y:I

    :goto_9
    iget v5, v1, Lsx;->y:I

    iget v6, v1, Lsx;->AZ:I

    add-int/2addr v5, v6

    if-ge v3, v5, :cond_9

    aget v5, v4, v3

    iget v6, v1, Lsx;->x:I

    iget v7, v1, Lsx;->AY:I

    add-int/2addr v6, v7

    if-ne v5, v6, :cond_d

    const/4 v1, 0x1

    goto :goto_6

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :pswitch_4
    iget v3, v1, Lsx;->y:I

    :goto_a
    iget v5, v1, Lsx;->y:I

    iget v6, v1, Lsx;->AZ:I

    add-int/2addr v5, v6

    if-ge v3, v5, :cond_9

    aget v5, v4, v3

    iget v6, v1, Lsx;->x:I

    if-ne v5, v6, :cond_e

    const/4 v1, 0x1

    goto/16 :goto_6

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :pswitch_5
    iget v3, v1, Lsx;->x:I

    :goto_b
    iget v5, v1, Lsx;->x:I

    iget v6, v1, Lsx;->AY:I

    add-int/2addr v5, v6

    if-ge v3, v5, :cond_9

    aget v5, v4, v3

    iget v6, v1, Lsx;->y:I

    iget v7, v1, Lsx;->AZ:I

    add-int/2addr v6, v7

    if-ne v5, v6, :cond_f

    const/4 v1, 0x1

    goto/16 :goto_6

    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :pswitch_6
    iget v3, v1, Lsx;->x:I

    :goto_c
    iget v5, v1, Lsx;->x:I

    iget v6, v1, Lsx;->AY:I

    add-int/2addr v5, v6

    if-ge v3, v5, :cond_9

    aget v5, v4, v3

    iget v6, v1, Lsx;->y:I

    if-ne v5, v6, :cond_10

    const/4 v1, 0x1

    goto/16 :goto_6

    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 1756
    :cond_11
    iget-object v1, v11, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v11}, Ltd;->fm()V

    .line 1757
    move-object/from16 v0, p5

    iget-object v1, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    .line 1760
    iget v2, v1, Lsx;->x:I

    iget v3, v1, Lsx;->y:I

    iget v4, v1, Lsx;->AY:I

    iget v5, v1, Lsx;->AZ:I

    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto/16 :goto_4

    .line 1768
    :pswitch_7
    iget v5, v1, Lsx;->x:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v1, Lsx;->x:I

    goto/16 :goto_8

    :pswitch_8
    iget v5, v1, Lsx;->x:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, Lsx;->x:I

    goto/16 :goto_8

    :pswitch_9
    iget v5, v1, Lsx;->y:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v1, Lsx;->y:I

    goto/16 :goto_8

    :cond_12
    invoke-virtual {v11}, Ltd;->fm()V

    move v8, v2

    move v9, v3

    goto/16 :goto_3

    .line 1771
    :cond_13
    const/4 v3, 0x0

    .line 1772
    invoke-virtual {v11}, Ltd;->fn()Landroid/graphics/Rect;

    move-result-object v1

    .line 1776
    if-nez v8, :cond_14

    iget v2, v1, Landroid/graphics/Rect;->left:I

    if-ltz v2, :cond_14

    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-gt v2, v4, :cond_14

    iget v2, v1, Landroid/graphics/Rect;->top:I

    if-ltz v2, :cond_14

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-gt v1, v2, :cond_14

    .line 1778
    const/4 v1, 0x1

    move v8, v1

    .line 1784
    :goto_d
    iget-object v1, v11, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_e
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1785
    move-object/from16 v0, p5

    iget-object v2, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    .line 1786
    iget v2, v1, Lsx;->x:I

    iget v3, v1, Lsx;->y:I

    iget v4, v1, Lsx;->AY:I

    iget v5, v1, Lsx;->AZ:I

    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v7, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_e

    .line 1780
    :cond_14
    move-object/from16 v0, p5

    iget-object v1, v0, Lsz;->Bf:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    move-object/from16 v0, p5

    iget-object v2, v0, Lsz;->Bf:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    move-object/from16 v0, p5

    iget-object v5, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsx;

    invoke-virtual {v2, v1}, Lsx;->a(Lsx;)V

    goto :goto_f

    :cond_15
    move v8, v3

    goto :goto_d

    :cond_16
    move v2, v8

    goto/16 :goto_7

    .line 1749
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 1768
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method private a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILsz;)Z
    .locals 19

    .prologue
    .line 1794
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v9, 0x1

    .line 1846
    :cond_0
    return v9

    .line 1796
    :cond_1
    const/16 v18, 0x0

    .line 1797
    const/4 v2, 0x0

    .line 1799
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object/from16 v17, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1800
    move-object/from16 v0, p4

    iget-object v3, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    .line 1801
    if-nez v17, :cond_2

    .line 1802
    new-instance v3, Landroid/graphics/Rect;

    iget v5, v2, Lsx;->x:I

    iget v6, v2, Lsx;->y:I

    iget v7, v2, Lsx;->x:I

    iget v8, v2, Lsx;->AY:I

    add-int/2addr v7, v8

    iget v8, v2, Lsx;->y:I

    iget v2, v2, Lsx;->AZ:I

    add-int/2addr v2, v8

    invoke-direct {v3, v5, v6, v7, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v17, v3

    goto :goto_0

    .line 1804
    :cond_2
    iget v3, v2, Lsx;->x:I

    iget v5, v2, Lsx;->y:I

    iget v6, v2, Lsx;->x:I

    iget v7, v2, Lsx;->AY:I

    add-int/2addr v6, v7

    iget v7, v2, Lsx;->y:I

    iget v2, v2, Lsx;->AZ:I

    add-int/2addr v2, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v5, v6, v2}, Landroid/graphics/Rect;->union(IIII)V

    goto :goto_0

    .line 1809
    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1810
    move-object/from16 v0, p4

    iget-object v3, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    .line 1811
    iget v3, v2, Lsx;->x:I

    iget v4, v2, Lsx;->y:I

    iget v5, v2, Lsx;->AY:I

    iget v6, v2, Lsx;->AZ:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_1

    .line 1814
    :cond_4
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v3

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [[Z

    .line 1815
    move-object/from16 v0, v17

    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 1816
    move-object/from16 v0, v17

    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 1819
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1820
    move-object/from16 v0, p4

    iget-object v3, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    .line 1821
    iget v3, v2, Lsx;->x:I

    sub-int/2addr v3, v10

    iget v4, v2, Lsx;->y:I

    sub-int/2addr v4, v9

    iget v5, v2, Lsx;->AY:I

    iget v6, v2, Lsx;->AZ:I

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_2

    .line 1824
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/android/launcher3/CellLayout;->a(Landroid/graphics/Rect;[[ZZ)V

    .line 1826
    move-object/from16 v0, v17

    iget v9, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v17

    iget v10, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v11

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/CellLayout;->zV:[I

    move-object/from16 v16, v0

    move-object/from16 v8, p0

    move-object/from16 v13, p3

    move-object v15, v7

    invoke-direct/range {v8 .. v16}, Lcom/android/launcher3/CellLayout;->a(IIII[I[[Z[[Z[I)[I

    .line 1830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-ltz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    if-ltz v2, :cond_7

    .line 1831
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int v3, v2, v3

    .line 1832
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/CellLayout;->zV:[I

    const/4 v4, 0x1

    aget v2, v2, v4

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int v4, v2, v4

    .line 1833
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1834
    move-object/from16 v0, p4

    iget-object v6, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    .line 1835
    iget v6, v2, Lsx;->x:I

    add-int/2addr v6, v3

    iput v6, v2, Lsx;->x:I

    .line 1836
    iget v6, v2, Lsx;->y:I

    add-int/2addr v6, v4

    iput v6, v2, Lsx;->y:I

    goto :goto_3

    .line 1838
    :cond_6
    const/4 v2, 0x1

    move v9, v2

    .line 1842
    :goto_4
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1843
    move-object/from16 v0, p4

    iget-object v3, v0, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsx;

    .line 1844
    iget v3, v2, Lsx;->x:I

    iget v4, v2, Lsx;->y:I

    iget v5, v2, Lsx;->AY:I

    iget v6, v2, Lsx;->AZ:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_5

    :cond_7
    move/from16 v9, v18

    goto :goto_4
.end method

.method public static a([IIIII[[Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2976
    move v4, v2

    :goto_0
    if-ge v4, p4, :cond_6

    move v6, v2

    .line 2977
    :goto_1
    if-ge v6, p3, :cond_5

    .line 2978
    aget-object v0, p5, v6

    aget-boolean v0, v0, v4

    if-nez v0, :cond_0

    move v0, v1

    :goto_2
    move v5, v6

    .line 2979
    :goto_3
    add-int v3, v6, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v5, v3, :cond_3

    if-ge v6, p3, :cond_3

    move v3, v4

    .line 2980
    :goto_4
    add-int v7, v4, p2

    add-int/lit8 v7, v7, -0x1

    if-ge v3, v7, :cond_2

    if-ge v4, p4, :cond_2

    .line 2981
    if-eqz v0, :cond_1

    aget-object v0, p5, v5

    aget-boolean v0, v0, v3

    if-nez v0, :cond_1

    move v0, v1

    .line 2982
    :goto_5
    if-eqz v0, :cond_3

    .line 2980
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_0
    move v0, v2

    .line 2978
    goto :goto_2

    :cond_1
    move v0, v2

    .line 2981
    goto :goto_5

    .line 2979
    :cond_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 2986
    :cond_3
    if-eqz v0, :cond_4

    .line 2987
    aput v6, p0, v2

    .line 2988
    aput v4, p0, v1

    .line 2994
    :goto_6
    return v1

    .line 2977
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2976
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_6
    move v1, v2

    .line 2994
    goto :goto_6
.end method

.method public static synthetic a(Lcom/android/launcher3/CellLayout;)[F
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->At:[F

    return-object v0
.end method

.method private a(IIIIIILandroid/view/View;Z[I[I[[Z)[I
    .locals 26

    .prologue
    .line 1250
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/CellLayout;->AN:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/launcher3/CellLayout;->zK:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/launcher3/CellLayout;->zL:I

    mul-int/2addr v5, v6

    if-ge v4, v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/CellLayout;->AN:Ljava/util/Stack;

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1252
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, p11

    invoke-direct {v0, v1, v2}, Lcom/android/launcher3/CellLayout;->b(Landroid/view/View;[[Z)V

    .line 1257
    move/from16 v0, p1

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/launcher3/CellLayout;->zG:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/launcher3/CellLayout;->zO:I

    add-int/2addr v5, v6

    add-int/lit8 v6, p5, -0x1

    mul-int/2addr v5, v6

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v14, v4

    .line 1258
    move/from16 v0, p2

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/launcher3/CellLayout;->zH:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/launcher3/CellLayout;->zP:I

    add-int/2addr v5, v6

    add-int/lit8 v6, p6, -0x1

    mul-int/2addr v5, v6

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v15, v4

    .line 1261
    if-eqz p9, :cond_2

    .line 1262
    :goto_1
    const-wide v10, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1263
    new-instance v16, Landroid/graphics/Rect;

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1264
    new-instance v17, Ljava/util/Stack;

    invoke-direct/range {v17 .. v17}, Ljava/util/Stack;-><init>()V

    .line 1266
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher3/CellLayout;->zK:I

    move/from16 v18, v0

    .line 1267
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher3/CellLayout;->zL:I

    move/from16 v19, v0

    .line 1269
    if-lez p3, :cond_1

    if-lez p4, :cond_1

    if-lez p5, :cond_1

    if-lez p6, :cond_1

    move/from16 v0, p5

    move/from16 v1, p3

    if-lt v0, v1, :cond_1

    move/from16 v0, p6

    move/from16 v1, p4

    if-ge v0, v1, :cond_3

    .line 1368
    :cond_1
    :goto_2
    return-object p9

    .line 1261
    :cond_2
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 p9, v0

    goto :goto_1

    .line 1274
    :cond_3
    const/4 v4, 0x0

    move v13, v4

    :goto_3
    add-int/lit8 v4, p4, -0x1

    sub-int v4, v19, v4

    if-ge v13, v4, :cond_19

    .line 1276
    const/4 v4, 0x0

    move v12, v4

    :goto_4
    add-int/lit8 v4, p3, -0x1

    sub-int v4, v18, v4

    if-ge v12, v4, :cond_18

    .line 1277
    const/4 v5, -0x1

    .line 1278
    const/4 v4, -0x1

    .line 1279
    if-eqz p8, :cond_20

    .line 1281
    const/4 v4, 0x0

    move v5, v4

    :goto_5
    move/from16 v0, p3

    if-ge v5, v0, :cond_5

    .line 1282
    const/4 v4, 0x0

    :goto_6
    move/from16 v0, p4

    if-ge v4, v0, :cond_4

    .line 1283
    add-int v6, v12, v5

    aget-object v6, p11, v6

    add-int v7, v13, v4

    aget-boolean v6, v6, v7

    if-nez v6, :cond_1b

    .line 1284
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 1281
    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 1294
    :cond_5
    const/4 v5, 0x1

    .line 1295
    move/from16 v0, p3

    move/from16 v1, p5

    if-lt v0, v1, :cond_9

    const/4 v8, 0x1

    .line 1296
    :goto_7
    move/from16 v0, p4

    move/from16 v1, p6

    if-lt v0, v1, :cond_a

    const/4 v4, 0x1

    :goto_8
    move v7, v4

    move v9, v5

    move/from16 v6, p4

    move/from16 v5, p3

    .line 1297
    :goto_9
    if-eqz v8, :cond_6

    if-nez v7, :cond_13

    .line 1298
    :cond_6
    if-eqz v9, :cond_c

    if-nez v8, :cond_c

    .line 1299
    const/4 v4, 0x0

    move/from16 v24, v4

    move v4, v8

    move/from16 v8, v24

    :goto_a
    if-ge v8, v6, :cond_b

    .line 1300
    add-int v20, v12, v5

    add-int/lit8 v21, v18, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_7

    add-int v20, v12, v5

    aget-object v20, p11, v20

    add-int v21, v13, v8

    aget-boolean v20, v20, v21

    if-eqz v20, :cond_8

    .line 1302
    :cond_7
    const/4 v4, 0x1

    .line 1299
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 1295
    :cond_9
    const/4 v8, 0x0

    goto :goto_7

    .line 1296
    :cond_a
    const/4 v4, 0x0

    goto :goto_8

    .line 1305
    :cond_b
    if-nez v4, :cond_1f

    .line 1306
    add-int/lit8 v5, v5, 0x1

    move v8, v6

    move v6, v4

    move/from16 v24, v5

    move v5, v7

    move/from16 v7, v24

    .line 1319
    :goto_b
    move/from16 v0, p5

    if-lt v7, v0, :cond_10

    const/4 v4, 0x1

    :goto_c
    or-int/2addr v6, v4

    .line 1320
    move/from16 v0, p6

    if-lt v8, v0, :cond_11

    const/4 v4, 0x1

    :goto_d
    or-int/2addr v5, v4

    .line 1321
    if-nez v9, :cond_12

    const/4 v4, 0x1

    :goto_e
    move v9, v4

    move/from16 v24, v6

    move v6, v8

    move/from16 v8, v24

    move/from16 v25, v7

    move v7, v5

    move/from16 v5, v25

    goto :goto_9

    .line 1308
    :cond_c
    if-nez v7, :cond_1e

    .line 1309
    const/4 v4, 0x0

    move/from16 v24, v4

    move v4, v7

    move/from16 v7, v24

    :goto_f
    if-ge v7, v5, :cond_f

    .line 1310
    add-int v20, v13, v6

    add-int/lit8 v21, v19, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_d

    add-int v20, v12, v7

    aget-object v20, p11, v20

    add-int v21, v13, v6

    aget-boolean v20, v20, v21

    if-eqz v20, :cond_e

    .line 1312
    :cond_d
    const/4 v4, 0x1

    .line 1309
    :cond_e
    add-int/lit8 v7, v7, 0x1

    goto :goto_f

    .line 1315
    :cond_f
    if-nez v4, :cond_1d

    .line 1316
    add-int/lit8 v6, v6, 0x1

    move v7, v5

    move v5, v4

    move/from16 v24, v8

    move v8, v6

    move/from16 v6, v24

    goto :goto_b

    .line 1319
    :cond_10
    const/4 v4, 0x0

    goto :goto_c

    .line 1320
    :cond_11
    const/4 v4, 0x0

    goto :goto_d

    .line 1321
    :cond_12
    const/4 v4, 0x0

    goto :goto_e

    :cond_13
    move v7, v6

    move v6, v5

    .line 1323
    :goto_10
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/CellLayout;->zT:[I

    .line 1328
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v9}, Lcom/android/launcher3/CellLayout;->b(II[I)V

    .line 1333
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/CellLayout;->AN:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 1334
    add-int v5, v12, v6

    add-int v8, v13, v7

    invoke-virtual {v4, v12, v13, v5, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 1335
    const/4 v8, 0x0

    .line 1336
    invoke-virtual/range {v17 .. v17}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_14
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Rect;

    .line 1337
    invoke-virtual {v5, v4}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1338
    const/4 v5, 0x1

    .line 1342
    :goto_11
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343
    const/4 v8, 0x0

    aget v8, v9, v8

    sub-int/2addr v8, v14

    int-to-double v0, v8

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v20

    const/4 v8, 0x1

    aget v8, v9, v8

    sub-int/2addr v8, v15

    int-to-double v8, v8

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    move-wide/from16 v0, v22

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    add-double v8, v8, v20

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 1346
    cmpg-double v20, v8, v10

    if-gtz v20, :cond_15

    if-eqz v5, :cond_16

    :cond_15
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1349
    :cond_16
    const/4 v5, 0x0

    aput v12, p9, v5

    .line 1350
    const/4 v5, 0x1

    aput v13, p9, v5

    .line 1351
    if-eqz p10, :cond_17

    .line 1352
    const/4 v5, 0x0

    aput v6, p10, v5

    .line 1353
    const/4 v5, 0x1

    aput v7, p10, v5

    .line 1355
    :cond_17
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    move-wide v4, v8

    .line 1276
    :goto_12
    add-int/lit8 v6, v12, 0x1

    move v12, v6

    move-wide v10, v4

    goto/16 :goto_4

    .line 1274
    :cond_18
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    goto/16 :goto_3

    .line 1360
    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, p11

    invoke-direct {v0, v1, v2}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;[[Z)V

    .line 1363
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v10, v4

    if-nez v4, :cond_1a

    .line 1364
    const/4 v4, 0x0

    const/4 v5, -0x1

    aput v5, p9, v4

    .line 1365
    const/4 v4, 0x1

    const/4 v5, -0x1

    aput v5, p9, v4

    .line 1367
    :cond_1a
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/Stack;)V

    goto/16 :goto_2

    :cond_1b
    move-wide v4, v10

    goto :goto_12

    :cond_1c
    move v5, v8

    goto :goto_11

    :cond_1d
    move v7, v5

    move v5, v4

    move/from16 v24, v8

    move v8, v6

    move/from16 v6, v24

    goto/16 :goto_b

    :cond_1e
    move/from16 v24, v7

    move v7, v5

    move/from16 v5, v24

    move/from16 v25, v8

    move v8, v6

    move/from16 v6, v25

    goto/16 :goto_b

    :cond_1f
    move v8, v6

    move v6, v4

    move/from16 v24, v5

    move v5, v7

    move/from16 v7, v24

    goto/16 :goto_b

    :cond_20
    move v6, v4

    move v7, v5

    goto/16 :goto_10
.end method

.method private a(IIIIIILandroid/view/View;[I[I)[I
    .locals 12

    .prologue
    .line 2683
    const/4 v7, 0x0

    const/4 v8, 0x1

    iget-object v11, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v11}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;Z[I[I[[Z)[I

    move-result-object v0

    return-object v0
.end method

.method private a(IIIILandroid/view/View;Z[I)[I
    .locals 12

    .prologue
    .line 1212
    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v11}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;Z[I[I[[Z)[I

    move-result-object v0

    return-object v0
.end method

.method private a(IIII[I[[Z[[Z[I)[I
    .locals 13

    .prologue
    .line 1395
    if-eqz p8, :cond_1

    .line 1396
    :goto_0
    const v4, 0x7f7fffff    # Float.MAX_VALUE

    .line 1397
    const/high16 v3, -0x80000000

    .line 1399
    iget v8, p0, Lcom/android/launcher3/CellLayout;->zK:I

    .line 1400
    iget v9, p0, Lcom/android/launcher3/CellLayout;->zL:I

    .line 1402
    const/4 v2, 0x0

    move v7, v2

    :goto_1
    add-int/lit8 v2, p4, -0x1

    sub-int v2, v9, v2

    if-ge v7, v2, :cond_6

    .line 1404
    const/4 v2, 0x0

    move v6, v2

    move v2, v3

    :goto_2
    add-int/lit8 v3, p3, -0x1

    sub-int v3, v8, v3

    if-ge v6, v3, :cond_5

    .line 1406
    const/4 v3, 0x0

    move v5, v3

    :goto_3
    move/from16 v0, p3

    if-ge v5, v0, :cond_3

    .line 1407
    const/4 v3, 0x0

    :goto_4
    move/from16 v0, p4

    if-ge v3, v0, :cond_2

    .line 1408
    add-int v10, v6, v5

    aget-object v10, p6, v10

    add-int v11, v7, v3

    aget-boolean v10, v10, v11

    if-eqz v10, :cond_0

    if-eqz p7, :cond_8

    aget-object v10, p7, v5

    aget-boolean v10, v10, v3

    if-nez v10, :cond_8

    .line 1409
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1395
    :cond_1
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 p8, v0

    goto :goto_0

    .line 1406
    :cond_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 1414
    :cond_3
    sub-int v3, v6, p1

    sub-int v5, v6, p1

    mul-int/2addr v3, v5

    sub-int v5, v7, p2

    sub-int v10, v7, p2

    mul-int/2addr v5, v10

    add-int/2addr v3, v5

    int-to-double v10, v3

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v5, v10

    .line 1416
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    .line 1417
    sub-int v10, v6, p1

    int-to-float v10, v10

    sub-int v11, v7, p2

    int-to-float v11, v11

    invoke-static {v10, v11, v3}, Lcom/android/launcher3/CellLayout;->b(FF[I)V

    .line 1420
    const/4 v10, 0x0

    aget v10, p5, v10

    const/4 v11, 0x0

    aget v11, v3, v11

    mul-int/2addr v10, v11

    const/4 v11, 0x1

    aget v11, p5, v11

    const/4 v12, 0x1

    aget v3, v3, v12

    mul-int/2addr v3, v11

    add-int/2addr v3, v10

    .line 1422
    invoke-static {v5, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v10

    if-ltz v10, :cond_4

    invoke-static {v5, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v10

    if-nez v10, :cond_8

    if-le v3, v2, :cond_8

    .line 1430
    :cond_4
    const/4 v2, 0x0

    aput v6, p8, v2

    .line 1431
    const/4 v2, 0x1

    aput v7, p8, v2

    move v2, v3

    move v3, v5

    .line 1404
    :goto_5
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    goto :goto_2

    .line 1402
    :cond_5
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v3, v2

    goto/16 :goto_1

    .line 1437
    :cond_6
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v2, v4, v2

    if-nez v2, :cond_7

    .line 1438
    const/4 v2, 0x0

    const/4 v3, -0x1

    aput v3, p8, v2

    .line 1439
    const/4 v2, 0x1

    const/4 v3, -0x1

    aput v3, p8, v2

    .line 1441
    :cond_7
    return-object p8

    :cond_8
    move v3, v4

    goto :goto_5
.end method

.method private static b(FF[I)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    const/4 v4, 0x0

    .line 2011
    div-float v0, p1, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    .line 2013
    aput v4, p2, v4

    .line 2014
    aput v4, p2, v5

    .line 2015
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v2, v2, v6

    if-lez v2, :cond_0

    .line 2016
    invoke-static {p0}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-int v2, v2

    aput v2, p2, v4

    .line 2018
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpl-double v0, v0, v6

    if-lez v0, :cond_1

    .line 2019
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    aput v0, p2, v5

    .line 2021
    :cond_1
    return-void
.end method

.method private b(II[I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 747
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(IIII[I)V

    .line 748
    return-void
.end method

.method private b(Landroid/view/View;[[Z)V
    .locals 7

    .prologue
    .line 3023
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    if-eq v0, v1, :cond_1

    .line 3026
    :cond_0
    :goto_0
    return-void

    .line 3024
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 3025
    iget v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/CellLayout;->a(IIII[[ZZ)V

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1858
    aget v2, p3, v1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    aget v3, p3, v0

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v2, v3

    if-le v2, v0, :cond_2

    .line 1861
    aget v2, p3, v0

    .line 1862
    aput v1, p3, v0

    .line 1864
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1947
    :cond_0
    :goto_0
    return v0

    .line 1868
    :cond_1
    aput v2, p3, v0

    .line 1869
    aget v2, p3, v1

    .line 1870
    aput v1, p3, v1

    .line 1872
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1877
    aput v2, p3, v1

    .line 1880
    aget v2, p3, v1

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v1

    .line 1881
    aget v2, p3, v0

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v0

    .line 1882
    aget v2, p3, v0

    .line 1883
    aput v1, p3, v0

    .line 1884
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1889
    aput v2, p3, v0

    .line 1890
    aget v2, p3, v1

    .line 1891
    aput v1, p3, v1

    .line 1892
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1897
    aput v2, p3, v1

    .line 1898
    aget v2, p3, v1

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v1

    .line 1899
    aget v2, p3, v0

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v0

    :goto_1
    move v0, v1

    .line 1947
    goto :goto_0

    .line 1904
    :cond_2
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1909
    aget v2, p3, v1

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v1

    .line 1910
    aget v2, p3, v0

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v0

    .line 1911
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1916
    aget v2, p3, v1

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v1

    .line 1917
    aget v2, p3, v0

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v0

    .line 1923
    aget v2, p3, v0

    .line 1924
    aget v3, p3, v1

    aput v3, p3, v0

    .line 1925
    aput v2, p3, v1

    .line 1926
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1932
    aget v2, p3, v1

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v1

    .line 1933
    aget v2, p3, v0

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v0

    .line 1934
    invoke-direct/range {p0 .. p5}, Lcom/android/launcher3/CellLayout;->a(Ljava/util/ArrayList;Landroid/graphics/Rect;[ILandroid/view/View;Lsz;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1939
    aget v2, p3, v1

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v1

    .line 1940
    aget v2, p3, v0

    mul-int/lit8 v2, v2, -0x1

    aput v2, p3, v0

    .line 1943
    aget v2, p3, v0

    .line 1944
    aget v3, p3, v1

    aput v3, p3, v0

    .line 1945
    aput v2, p3, v1

    goto :goto_1
.end method

.method public static synthetic b(Lcom/android/launcher3/CellLayout;)[Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    return-object v0
.end method

.method public static synthetic c(Lcom/android/launcher3/CellLayout;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ay:Ljava/util/HashMap;

    return-object v0
.end method

.method public static c(II[I)[I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2904
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 2905
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v3

    .line 2906
    iget-boolean v0, v3, Ltu;->Dp:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ltu;->ba(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2911
    iget v4, v3, Ltu;->Dx:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    iget v5, v3, Ltu;->Dh:F

    float-to-int v5, v5

    div-int/2addr v4, v5

    .line 2913
    iget v5, v3, Ltu;->Dy:I

    iget v6, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v5, v0

    iget v3, v3, Ltu;->Dg:F

    float-to-int v3, v3

    div-int/2addr v0, v3

    .line 2915
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2918
    int-to-float v3, p0

    int-to-float v4, v0

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 2919
    int-to-float v4, p1

    int-to-float v0, v0

    div-float v0, v4, v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 2921
    const/4 v4, 0x2

    new-array v4, v4, [I

    aput v3, v4, v1

    aput v0, v4, v2

    return-object v4

    :cond_0
    move v0, v2

    .line 2906
    goto :goto_0
.end method

.method public static synthetic d(Lcom/android/launcher3/CellLayout;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zL:I

    return v0
.end method

.method public static synthetic e(Lcom/android/launcher3/CellLayout;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    return v0
.end method

.method public static synthetic f(Lcom/android/launcher3/CellLayout;)[I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    return-object v0
.end method

.method private fb()V
    .locals 2

    .prologue
    .line 2287
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lta;

    .line 2288
    invoke-virtual {v0}, Lta;->fl()V

    goto :goto_0

    .line 2290
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Az:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2291
    return-void
.end method

.method private fc()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2294
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge v0, v2, :cond_1

    move v2, v1

    .line 2295
    :goto_1
    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge v2, v3, :cond_0

    .line 2296
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    aput-boolean v4, v3, v2

    .line 2295
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2294
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2299
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v3

    move v2, v1

    .line 2300
    :goto_2
    if-ge v2, v3, :cond_5

    .line 2301
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v2}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2302
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2303
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwq;

    .line 2306
    if-eqz v1, :cond_4

    .line 2307
    iget v4, v1, Lwq;->Bb:I

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    if-ne v4, v5, :cond_2

    iget v4, v1, Lwq;->Bc:I

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    if-ne v4, v5, :cond_2

    iget v4, v1, Lwq;->AY:I

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    if-ne v4, v5, :cond_2

    iget v4, v1, Lwq;->AZ:I

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    if-eq v4, v5, :cond_3

    .line 2309
    :cond_2
    const/4 v4, 0x1

    iput-boolean v4, v1, Lwq;->JD:Z

    .line 2311
    :cond_3
    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    iput v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iput v4, v1, Lwq;->Bb:I

    .line 2312
    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    iput v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iput v4, v1, Lwq;->Bc:I

    .line 2313
    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iput v4, v1, Lwq;->AY:I

    .line 2314
    iget v0, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    iput v0, v1, Lwq;->AZ:I

    .line 2300
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2317
    :cond_5
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/launcher3/Workspace;->l(Lcom/android/launcher3/CellLayout;)V

    .line 2318
    return-void
.end method

.method private fg()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2998
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge v0, v2, :cond_1

    move v2, v1

    .line 2999
    :goto_1
    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge v2, v3, :cond_0

    .line 3000
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    aget-object v3, v3, v0

    aput-boolean v1, v3, v2

    .line 2999
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2998
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3003
    :cond_1
    return-void
.end method

.method public static synthetic g(Lcom/android/launcher3/CellLayout;)F
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/android/launcher3/CellLayout;->AH:F

    return v0
.end method


# virtual methods
.method public final A(II)V
    .locals 7

    .prologue
    .line 317
    iput p1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    .line 318
    iput p2, p0, Lcom/android/launcher3/CellLayout;->zL:I

    .line 319
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zL:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    .line 320
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zL:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    .line 321
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AN:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 322
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zO:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zP:I

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v6, p0, Lcom/android/launcher3/CellLayout;->zL:I

    invoke-virtual/range {v0 .. v6}, Ladg;->b(IIIIII)V

    .line 324
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->requestLayout()V

    .line 325
    return-void
.end method

.method public final B(II)V
    .locals 2

    .prologue
    .line 537
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 538
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 539
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 540
    return-void
.end method

.method public final C(II)Landroid/view/View;
    .locals 7

    .prologue
    .line 970
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v3}, Ladg;->getChildCount()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    if-gt v5, p1, :cond_0

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v6, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    add-int/2addr v5, v6

    if-ge p1, v5, :cond_0

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    if-gt v5, p2, :cond_0

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v0, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    add-int/2addr v0, v5

    if-ge p2, v0, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final D(II)Z
    .locals 2

    .prologue
    .line 3049
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-ge p2, v0, :cond_0

    .line 3050
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    aget-object v0, v0, p1

    aget-boolean v0, v0, p2

    return v0

    .line 3052
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Position exceeds the bound of this CellLayout"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final E(Z)V
    .locals 3

    .prologue
    .line 298
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    sget-object v2, Lcom/android/launcher3/CellLayout;->AM:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v2}, Ladg;->setLayerType(ILandroid/graphics/Paint;)V

    .line 299
    return-void

    .line 298
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final F(Z)V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ladg;->SS:Z

    .line 330
    return-void
.end method

.method public final G(Z)V
    .locals 0

    .prologue
    .line 333
    iput-boolean p1, p0, Lcom/android/launcher3/CellLayout;->zR:Z

    .line 334
    return-void
.end method

.method public final H(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2348
    invoke-virtual {p0, p1}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 2349
    return-void
.end method

.method final H(Z)V
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Aq:Z

    if-eq v0, p1, :cond_0

    .line 391
    iput-boolean p1, p0, Lcom/android/launcher3/CellLayout;->Aq:Z

    .line 392
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Aq:Z

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Ar:Z

    .line 393
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 395
    :cond_0
    return-void
.end method

.method public final I(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 590
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AF:Z

    .line 591
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    iput-boolean v1, v0, Ladg;->SR:Z

    .line 592
    return-void
.end method

.method public final J(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 3011
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;[[Z)V

    .line 3012
    return-void
.end method

.method public final K(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 3020
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/CellLayout;->b(Landroid/view/View;[[Z)V

    .line 3021
    return-void
.end method

.method public final a(FF[I)F
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 784
    aget v0, p3, v3

    aget v1, p3, v6

    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher3/CellLayout;->b(II[I)V

    .line 785
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    aget v2, v2, v6

    int-to-float v2, v2

    sub-float v2, p2, v2

    float-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 787
    return v0
.end method

.method final a(FZ)V
    .locals 2

    .prologue
    .line 341
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Ak:Landroid/graphics/drawable/Drawable;

    if-eq v0, v1, :cond_1

    .line 342
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ak:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    .line 347
    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ac:F

    mul-float/2addr v0, p1

    .line 348
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/CellLayout;->Ad:I

    .line 349
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->Ad:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 350
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 351
    return-void

    .line 343
    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Al:Landroid/graphics/drawable/Drawable;

    if-eq v0, v1, :cond_0

    .line 344
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Al:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final a(IIII[I)V
    .locals 6

    .prologue
    .line 759
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v0

    .line 760
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v1

    .line 761
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zO:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p1

    add-int/2addr v0, v3

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v3, p3

    add-int/lit8 v4, p3, -0x1

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zO:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    aput v0, p5, v2

    .line 763
    const/4 v0, 0x1

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zP:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    mul-int/2addr v2, p4

    add-int/lit8 v3, p4, -0x1

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zP:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    aput v1, p5, v0

    .line 765
    return-void
.end method

.method public final a(Landroid/util/SparseArray;)V
    .locals 3

    .prologue
    .line 555
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/launcher3/CellLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    :goto_0
    return-void

    .line 556
    :catch_0
    move-exception v0

    .line 557
    invoke-static {}, Lyu;->isDogfoodBuild()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 558
    throw v0

    .line 561
    :cond_0
    const-string v1, "CellLayout"

    const-string v2, "Ignoring an error while restoring a view instance state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 578
    iput-object p1, p0, Lcom/android/launcher3/CellLayout;->zZ:Landroid/view/View$OnTouchListener;

    .line 579
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Bitmap;IIIIZLandroid/graphics/Point;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1090
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 1092
    if-nez p2, :cond_1

    if-nez p1, :cond_1

    .line 1149
    :cond_0
    :goto_0
    return-void

    .line 1096
    :cond_1
    if-ne p3, v0, :cond_2

    if-eq p4, v1, :cond_0

    .line 1097
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v1, 0x0

    aput p3, v0, v1

    .line 1098
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v1, 0x1

    aput p4, v0, v1

    .line 1100
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zU:[I

    .line 1101
    invoke-direct {p0, p3, p4, v0}, Lcom/android/launcher3/CellLayout;->a(II[I)V

    .line 1103
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 1104
    const/4 v2, 0x1

    aget v2, v0, v2

    .line 1106
    if-eqz p1, :cond_4

    if-nez p8, :cond_4

    .line 1109
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1110
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v1

    .line 1111
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v2

    .line 1116
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 1118
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v0, p5

    add-int/lit8 v2, p5, -0x1

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zO:I

    mul-int/2addr v2, v4

    add-int/2addr v0, v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    .line 1137
    :goto_1
    iget v2, p0, Lcom/android/launcher3/CellLayout;->Av:I

    .line 1138
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    aget-object v3, v3, v2

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lwo;->bj(I)V

    .line 1139
    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    array-length v3, v3

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/android/launcher3/CellLayout;->Av:I

    .line 1140
    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/launcher3/CellLayout;->Av:I

    aget-object v5, v2, v3

    .line 1141
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1142
    if-eqz p7, :cond_3

    move-object v0, p0

    move v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    .line 1143
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->b(IIIILandroid/graphics/Rect;)V

    .line 1146
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->Av:I

    aget-object v0, v0, v1

    iput-object p2, v0, Lwo;->Jx:Ljava/lang/Object;

    .line 1147
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->Av:I

    aget-object v0, v0, v1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lwo;->bj(I)V

    goto/16 :goto_0

    .line 1121
    :cond_4
    if-eqz p8, :cond_5

    if-eqz p9, :cond_5

    .line 1124
    iget v0, p8, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v3, p5

    add-int/lit8 v4, p5, -0x1

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zO:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    invoke-virtual {p9}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 1126
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v1}, Ladg;->kd()I

    move-result v1

    .line 1127
    const/4 v3, 0x0

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zH:I

    sub-int v1, v4, v1

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 1128
    iget v3, p8, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    .line 1129
    goto :goto_1

    .line 1131
    :cond_5
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v0, p5

    add-int/lit8 v3, p5, -0x1

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zO:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 1133
    iget v1, p0, Lcom/android/launcher3/CellLayout;->zH:I

    mul-int/2addr v1, p6

    add-int/lit8 v3, p6, -0x1

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zP:I

    mul-int/2addr v3, v4

    add-int/2addr v1, v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    goto/16 :goto_1
.end method

.method final a(Lcom/android/launcher3/BubbleTextView;Landroid/graphics/Bitmap;I)V
    .locals 6

    .prologue
    .line 354
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Luu;->f(Landroid/graphics/Bitmap;)Z

    .line 356
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    invoke-virtual {v0}, Luu;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 371
    :cond_1
    :goto_0
    return-void

    .line 358
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 360
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getLeft()I

    move-result v2

    int-to-float v0, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    add-int/2addr v0, v2

    sub-int/2addr v0, p3

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Luu;->setTranslationX(F)V

    .line 362
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getTop()I

    move-result v1

    sub-int/2addr v1, p3

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Luu;->setTranslationY(F)V

    .line 363
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    invoke-virtual {v0, p2}, Luu;->f(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Luu;->setAlpha(F)V

    .line 365
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ax:Luu;

    invoke-virtual {v0}, Luu;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lus;->Gn:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public final a(Lvs;)V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aa:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    return-void
.end method

.method final a(IIIILandroid/view/View;[I)Z
    .locals 8

    .prologue
    .line 2423
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->c(IIII[I)[I

    move-result-object v0

    .line 2424
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    move-object v0, p0

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/view/View;Landroid/graphics/Rect;Ljava/util/ArrayList;)V

    .line 2426
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIIILandroid/view/View;[IZ)Z
    .locals 13

    .prologue
    .line 2449
    const/4 v2, 0x2

    new-array v7, v2, [I

    move-object v2, p0

    move v3, p1

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    .line 2450
    invoke-virtual/range {v2 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIII[I)V

    .line 2453
    const/4 v2, 0x0

    aget v3, v7, v2

    const/4 v2, 0x1

    aget v4, v7, v2

    const/4 v11, 0x1

    new-instance v12, Lsz;

    const/4 v2, 0x0

    invoke-direct {v12, p0, v2}, Lsz;-><init>(Lcom/android/launcher3/CellLayout;B)V

    move-object v2, p0

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p5

    invoke-direct/range {v2 .. v12}, Lcom/android/launcher3/CellLayout;->a(IIIIII[ILandroid/view/View;ZLsz;)Lsz;

    move-result-object v2

    .line 2456
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/launcher3/CellLayout;->J(Z)V

    .line 2457
    if-eqz v2, :cond_0

    iget-boolean v3, v2, Lsz;->Bi:Z

    if-eqz v3, :cond_0

    .line 2461
    move-object/from16 v0, p5

    invoke-direct {p0, v2, v0}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;)V

    .line 2462
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    .line 2463
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-direct {p0, v2, v0, v1}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;Z)V

    .line 2465
    if-eqz p7, :cond_1

    .line 2466
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fc()V

    .line 2467
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fb()V

    .line 2468
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    .line 2473
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v3}, Ladg;->requestLayout()V

    .line 2475
    :cond_0
    iget-boolean v2, v2, Lsz;->Bi:Z

    return v2

    .line 2470
    :cond_1
    const/4 v3, 0x1

    move-object/from16 v0, p5

    invoke-direct {p0, v2, v0, v3}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;IIIIZZ)Z
    .locals 13

    .prologue
    .line 975
    iget-object v8, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    .line 976
    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    .line 977
    if-nez p6, :cond_5

    .line 978
    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->zX:[[Z

    move-object v5, v2

    .line 981
    :goto_0
    invoke-virtual {v8, p1}, Ladg;->indexOfChild(Landroid/view/View;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 982
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 983
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwq;

    .line 986
    iget-object v4, p0, Lcom/android/launcher3/CellLayout;->Ay:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 987
    iget-object v4, p0, Lcom/android/launcher3/CellLayout;->Ay:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/animation/Animator;

    invoke-virtual {v4}, Landroid/animation/Animator;->cancel()V

    .line 988
    iget-object v4, p0, Lcom/android/launcher3/CellLayout;->Ay:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 991
    :cond_0
    iget v10, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 992
    iget v11, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 993
    if-eqz p7, :cond_1

    .line 994
    iget v4, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    aget-object v4, v5, v4

    iget v6, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    const/4 v7, 0x0

    aput-boolean v7, v4, v6

    .line 995
    aget-object v4, v5, p2

    const/4 v5, 0x1

    aput-boolean v5, v4, p3

    .line 997
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 998
    if-eqz p6, :cond_2

    .line 999
    iput p2, v3, Lwq;->Bb:I

    iput p2, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    .line 1000
    move/from16 v0, p3

    iput v0, v3, Lwq;->Bc:I

    move/from16 v0, p3

    iput v0, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    .line 1005
    :goto_1
    iget v3, v8, Ladg;->zG:I

    iget v4, v8, Ladg;->zH:I

    iget v5, v8, Ladg;->zO:I

    iget v6, v8, Ladg;->zP:I

    invoke-virtual {v8}, Ladg;->ke()Z

    move-result v7

    iget v8, v8, Ladg;->zK:I

    invoke-virtual/range {v2 .. v8}, Lcom/android/launcher3/CellLayout$LayoutParams;->a(IIIIZI)V

    .line 1006
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 1007
    iget v7, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 1008
    iget v9, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 1010
    iput v10, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 1011
    iput v11, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 1014
    if-ne v10, v7, :cond_3

    if-ne v11, v9, :cond_3

    .line 1015
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 1016
    const/4 v2, 0x1

    .line 1054
    :goto_2
    return v2

    .line 1002
    :cond_2
    iput p2, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    .line 1003
    move/from16 v0, p3

    iput v0, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    goto :goto_1

    .line 1019
    :cond_3
    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v3}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v12

    .line 1020
    move/from16 v0, p4

    int-to-long v4, v0

    invoke-virtual {v12, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1021
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->Ay:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1023
    new-instance v3, Lsv;

    move-object v4, p0

    move-object v5, v2

    move v6, v10

    move v8, v11

    move-object v10, p1

    invoke-direct/range {v3 .. v10}, Lsv;-><init>(Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/CellLayout$LayoutParams;IIIILandroid/view/View;)V

    invoke-virtual {v12, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1032
    new-instance v3, Lsw;

    invoke-direct {v3, p0, v2, p1}, Lsw;-><init>(Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/CellLayout$LayoutParams;Landroid/view/View;)V

    invoke-virtual {v12, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1050
    move/from16 v0, p5

    int-to-long v2, v0

    invoke-virtual {v12, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1051
    invoke-virtual {v12}, Landroid/animation/ValueAnimator;->start()V

    .line 1052
    const/4 v2, 0x1

    goto :goto_2

    .line 1054
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    move-object v5, v2

    goto/16 :goto_0

    .line 1019
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 596
    .line 599
    instance-of v0, p1, Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 600
    check-cast v0, Lcom/android/launcher3/BubbleTextView;

    .line 601
    iget-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AF:Z

    if-nez v1, :cond_4

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/launcher3/BubbleTextView;->C(Z)V

    .line 604
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 605
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 609
    iget v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    if-ltz v0, :cond_5

    iget v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_5

    iget v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    if-ltz v0, :cond_5

    iget v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zL:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_5

    .line 612
    iget v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    if-gez v0, :cond_1

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iput v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 613
    :cond_1
    iget v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    if-gez v0, :cond_2

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zL:I

    iput v0, p4, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 615
    :cond_2
    invoke-virtual {p1, p3}, Landroid/view/View;->setId(I)V

    .line 617
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1, p2, p4}, Ladg;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 619
    if-eqz p5, :cond_3

    invoke-virtual {p0, p1}, Lcom/android/launcher3/CellLayout;->J(Landroid/view/View;)V

    .line 623
    :cond_3
    :goto_1
    return v2

    :cond_4
    move v1, v3

    .line 601
    goto :goto_0

    :cond_5
    move v2, v3

    .line 623
    goto :goto_1
.end method

.method public final a(IIIIIILandroid/view/View;[I[II)[I
    .locals 15

    .prologue
    .line 2481
    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p8

    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher3/CellLayout;->c(IIII[I)[I

    move-result-object v13

    .line 2483
    if-nez p9, :cond_0

    .line 2484
    const/4 v1, 0x2

    new-array v0, v1, [I

    move-object/from16 p9, v0

    .line 2490
    :cond_0
    const/4 v1, 0x2

    move/from16 v0, p10

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    move/from16 v0, p10

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    move/from16 v0, p10

    if-ne v0, v1, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v2, -0x64

    if-eq v1, v2, :cond_4

    .line 2492
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    aput v3, v1, v2

    .line 2493
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    aput v3, v1, v2

    .line 2495
    const/4 v1, 0x2

    move/from16 v0, p10

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    move/from16 v0, p10

    if-ne v0, v1, :cond_3

    .line 2496
    :cond_2
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v2, 0x0

    const/16 v3, -0x64

    aput v3, v1, v2

    .line 2497
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v2, 0x1

    const/16 v3, -0x64

    aput v3, v1, v2

    .line 2506
    :cond_3
    :goto_0
    iget-object v8, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    const/4 v10, 0x1

    new-instance v11, Lsz;

    const/4 v1, 0x0

    invoke-direct {v11, p0, v1}, Lsz;-><init>(Lcom/android/launcher3/CellLayout;B)V

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v11}, Lcom/android/launcher3/CellLayout;->a(IIIIII[ILandroid/view/View;ZLsz;)Lsz;

    move-result-object v9

    .line 2510
    new-instance v8, Lsz;

    const/4 v1, 0x0

    invoke-direct {v8, p0, v1}, Lsz;-><init>(Lcom/android/launcher3/CellLayout;B)V

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/android/launcher3/CellLayout;->a(IIIIIILsz;)Lsz;

    move-result-object v1

    .line 2513
    const/4 v2, 0x0

    .line 2517
    iget-boolean v3, v9, Lsz;->Bi:Z

    if-eqz v3, :cond_a

    invoke-virtual {v9}, Lsz;->fk()I

    move-result v3

    invoke-virtual {v1}, Lsz;->fk()I

    move-result v4

    if-lt v3, v4, :cond_a

    move-object v3, v9

    .line 2523
    :goto_1
    if-nez p10, :cond_c

    .line 2524
    if-eqz v3, :cond_b

    .line 2525
    const/4 v1, 0x0

    move-object/from16 v0, p7

    invoke-direct {p0, v3, v0, v1}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;I)V

    .line 2527
    const/4 v1, 0x0

    iget v2, v3, Lsz;->Bj:I

    aput v2, v13, v1

    .line 2528
    const/4 v1, 0x1

    iget v2, v3, Lsz;->Bk:I

    aput v2, v13, v1

    .line 2529
    const/4 v1, 0x0

    iget v2, v3, Lsz;->Bl:I

    aput v2, p9, v1

    .line 2530
    const/4 v1, 0x1

    iget v2, v3, Lsz;->Bm:I

    aput v2, p9, v1

    :goto_2
    move-object v1, v13

    .line 2578
    :goto_3
    return-object v1

    .line 2500
    :cond_4
    iget-object v14, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    const/4 v1, 0x2

    new-array v6, v1, [I

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p5

    move/from16 v5, p6

    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher3/CellLayout;->c(IIII[I)[I

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x0

    aget v8, v6, v1

    const/4 v1, 0x1

    aget v9, v6, v1

    move-object v7, p0

    move/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {v7 .. v12}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/graphics/Rect;)V

    invoke-virtual {v12}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v12}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x0

    aget v2, v6, v1

    const/4 v1, 0x1

    aget v3, v6, v1

    iget-object v8, p0, Lcom/android/launcher3/CellLayout;->AI:Ljava/util/ArrayList;

    move-object v1, p0

    move/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v8}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/view/View;Landroid/graphics/Rect;Ljava/util/ArrayList;)V

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget v3, v7, Landroid/graphics/Rect;->left:I

    iget v4, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v6

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/graphics/Rect;)V

    invoke-virtual {v7}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    sub-int v1, v1, p1

    div-int v1, v1, p5

    invoke-virtual {v7}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v2, v2, p2

    div-int v2, v2, p6

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zK:I

    if-eq v8, v3, :cond_5

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zK:I

    move/from16 v0, p5

    if-ne v0, v3, :cond_6

    :cond_5
    const/4 v1, 0x0

    :cond_6
    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    if-eq v9, v3, :cond_7

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zL:I

    move/from16 v0, p6

    if-ne v0, v3, :cond_8

    :cond_7
    const/4 v2, 0x0

    :cond_8
    if-nez v1, :cond_9

    if-nez v2, :cond_9

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput v2, v14, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v14, v1

    .line 2501
    :goto_4
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    aput v3, v1, v2

    .line 2502
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AK:[I

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->xW:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    aput v3, v1, v2

    goto/16 :goto_0

    .line 2500
    :cond_9
    int-to-float v1, v1

    int-to-float v2, v2

    invoke-static {v1, v2, v14}, Lcom/android/launcher3/CellLayout;->b(FF[I)V

    goto :goto_4

    .line 2519
    :cond_a
    iget-boolean v3, v1, Lsz;->Bi:Z

    if-eqz v3, :cond_15

    move-object v3, v1

    .line 2520
    goto/16 :goto_1

    .line 2532
    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, -0x1

    aput v5, p9, v4

    aput v5, p9, v3

    aput v5, v13, v2

    aput v5, v13, v1

    goto/16 :goto_2

    .line 2537
    :cond_c
    const/4 v2, 0x1

    .line 2539
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/launcher3/CellLayout;->J(Z)V

    .line 2542
    if-eqz v3, :cond_13

    .line 2543
    const/4 v1, 0x0

    iget v4, v3, Lsz;->Bj:I

    aput v4, v13, v1

    .line 2544
    const/4 v1, 0x1

    iget v4, v3, Lsz;->Bk:I

    aput v4, v13, v1

    .line 2545
    const/4 v1, 0x0

    iget v4, v3, Lsz;->Bl:I

    aput v4, p9, v1

    .line 2546
    const/4 v1, 0x1

    iget v4, v3, Lsz;->Bm:I

    aput v4, p9, v1

    .line 2551
    const/4 v1, 0x1

    move/from16 v0, p10

    if-eq v0, v1, :cond_d

    const/4 v1, 0x2

    move/from16 v0, p10

    if-eq v0, v1, :cond_d

    const/4 v1, 0x3

    move/from16 v0, p10

    if-ne v0, v1, :cond_14

    .line 2553
    :cond_d
    move-object/from16 v0, p7

    invoke-direct {p0, v3, v0}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;)V

    .line 2555
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    .line 2556
    const/4 v1, 0x2

    move/from16 v0, p10

    if-ne v0, v1, :cond_11

    const/4 v1, 0x1

    :goto_5
    move-object/from16 v0, p7

    invoke-direct {p0, v3, v0, v1}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;Z)V

    .line 2558
    const/4 v1, 0x2

    move/from16 v0, p10

    if-eq v0, v1, :cond_e

    const/4 v1, 0x3

    move/from16 v0, p10

    if-ne v0, v1, :cond_12

    .line 2560
    :cond_e
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fc()V

    .line 2561
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fb()V

    .line 2562
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    move v1, v2

    .line 2573
    :goto_6
    const/4 v2, 0x2

    move/from16 v0, p10

    if-eq v0, v2, :cond_f

    if-nez v1, :cond_10

    .line 2574
    :cond_f
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/launcher3/CellLayout;->J(Z)V

    .line 2577
    :cond_10
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v1}, Ladg;->requestLayout()V

    move-object v1, v13

    .line 2578
    goto/16 :goto_3

    .line 2556
    :cond_11
    const/4 v1, 0x0

    goto :goto_5

    .line 2564
    :cond_12
    const/4 v1, 0x1

    move-object/from16 v0, p7

    invoke-direct {p0, v3, v0, v1}, Lcom/android/launcher3/CellLayout;->a(Lsz;Landroid/view/View;I)V

    move v1, v2

    goto :goto_6

    .line 2569
    :cond_13
    const/4 v1, 0x0

    .line 2570
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, -0x1

    aput v6, p9, v5

    aput v6, p9, v4

    aput v6, v13, v3

    aput v6, v13, v2

    goto :goto_6

    :cond_14
    move v1, v2

    goto :goto_6

    :cond_15
    move-object v3, v2

    goto/16 :goto_1
.end method

.method final a(IIIIII[I[I)[I
    .locals 10

    .prologue
    .line 1192
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[I)[I

    move-result-object v0

    return-object v0
.end method

.method public final b(IIIILandroid/graphics/Rect;)V
    .locals 9

    .prologue
    .line 2878
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zG:I

    .line 2879
    iget v1, p0, Lcom/android/launcher3/CellLayout;->zH:I

    .line 2880
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zO:I

    .line 2881
    iget v3, p0, Lcom/android/launcher3/CellLayout;->zP:I

    .line 2883
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v4

    .line 2884
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v5

    .line 2886
    mul-int v6, p3, v0

    add-int/lit8 v7, p3, -0x1

    mul-int/2addr v7, v2

    add-int/2addr v6, v7

    .line 2887
    mul-int v7, p4, v1

    add-int/lit8 v8, p4, -0x1

    mul-int/2addr v8, v3

    add-int/2addr v7, v8

    .line 2889
    add-int/2addr v0, v2

    mul-int/2addr v0, p1

    add-int/2addr v0, v4

    .line 2890
    add-int/2addr v1, v3

    mul-int/2addr v1, p2

    add-int/2addr v1, v5

    .line 2892
    add-int v2, v0, v6

    add-int v3, v1, v7

    invoke-virtual {p5, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 2893
    return-void
.end method

.method public final b(Lvs;)V
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aa:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aa:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 533
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 534
    return-void
.end method

.method final b([III)Z
    .locals 10

    .prologue
    .line 2723
    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    const/4 v0, 0x0

    invoke-direct {p0, v0, v5}, Lcom/android/launcher3/CellLayout;->b(Landroid/view/View;[[Z)V

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    add-int/lit8 v2, p2, -0x1

    sub-int v6, v0, v2

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zL:I

    add-int/lit8 v2, p3, -0x1

    sub-int v7, v0, v2

    const/4 v0, 0x0

    move v4, v0

    move v0, v1

    :goto_0
    if-ge v4, v7, :cond_5

    if-nez v0, :cond_5

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v6, :cond_4

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, p2, :cond_2

    const/4 v1, 0x0

    :goto_3
    if-ge v1, p3, :cond_1

    add-int v8, v3, v2

    aget-object v8, v5, v8

    add-int v9, v4, v1

    aget-boolean v8, v8, v9

    if-eqz v8, :cond_0

    add-int v1, v3, v2

    add-int/lit8 v1, v1, 0x1

    move v3, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x0

    aput v3, p1, v0

    const/4 v0, 0x1

    aput v4, p1, v0

    :cond_3
    const/4 v0, 0x1

    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1, v5}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;[[Z)V

    return v0
.end method

.method final b(IIII[I)[I
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 1172
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v6, v3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/view/View;Z[I)[I

    move-result-object v0

    return-object v0
.end method

.method public final c([III)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2970
    iget v3, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zL:I

    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->zW:[[Z

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a([IIIII[[Z)Z

    move-result v0

    return v0
.end method

.method final c(IIII[I)[I
    .locals 8

    .prologue
    .line 2702
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/view/View;Z[I)[I

    move-result-object v0

    return-object v0
.end method

.method public cancelLongPress()V
    .locals 3

    .prologue
    .line 567
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 570
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getChildCount()I

    move-result v1

    .line 571
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 572
    invoke-virtual {p0, v0}, Lcom/android/launcher3/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 573
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 571
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 575
    :cond_0
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 3063
    instance-of v0, p1, Lcom/android/launcher3/CellLayout$LayoutParams;

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 518
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 519
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ad:I

    if-lez v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->An:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 521
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 523
    :cond_0
    return-void
.end method

.method public final eL()V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->buildLayer()V

    .line 303
    return-void
.end method

.method public final eM()F
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->AF:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/CellLayout;->AG:F

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final eN()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->zR:Z

    return v0
.end method

.method final eO()V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Ag:Z

    .line 379
    return-void
.end method

.method final eP()V
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout;->zS:Z

    .line 383
    return-void
.end method

.method final eQ()Z
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->zS:Z

    return v0
.end method

.method final eR()Z
    .locals 1

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Aq:Z

    return v0
.end method

.method public final eS()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 543
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 544
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 545
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 546
    return-void
.end method

.method public final eT()I
    .locals 1

    .prologue
    .line 582
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    return v0
.end method

.method public final eU()I
    .locals 1

    .prologue
    .line 586
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zL:I

    return v0
.end method

.method public final eV()I
    .locals 1

    .prologue
    .line 791
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zG:I

    return v0
.end method

.method public final eW()I
    .locals 1

    .prologue
    .line 795
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zH:I

    return v0
.end method

.method public final eX()I
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zO:I

    return v0
.end method

.method public final eY()I
    .locals 1

    .prologue
    .line 803
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zP:I

    return v0
.end method

.method public final eZ()Ladg;
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    return-object v0
.end method

.method public final fa()V
    .locals 5

    .prologue
    .line 1152
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Av:I

    .line 1153
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    aget-object v0, v1, v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lwo;->bj(I)V

    .line 1154
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v3, 0x1

    const/4 v4, -0x1

    aput v4, v2, v3

    aput v4, v0, v1

    .line 1155
    return-void
.end method

.method public final fd()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2430
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fb()V

    .line 2431
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    if-eqz v0, :cond_3

    .line 2432
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v9

    move v8, v5

    .line 2433
    :goto_0
    if-ge v8, v9, :cond_2

    .line 2434
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, v8}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2435
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2436
    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    if-eq v2, v3, :cond_1

    .line 2437
    :cond_0
    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iput v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    .line 2438
    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iput v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    .line 2439
    iget v2, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    const/16 v4, 0x96

    move-object v0, p0

    move v6, v5

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    .line 2433
    :cond_1
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 2443
    :cond_2
    iput-boolean v5, p0, Lcom/android/launcher3/CellLayout;->AA:Z

    .line 2445
    :cond_3
    return-void
.end method

.method final fe()V
    .locals 1

    .prologue
    .line 2829
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AL:Lup;

    invoke-virtual {v0}, Lup;->fe()V

    .line 2830
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout;->AC:Z

    .line 2831
    return-void
.end method

.method final ff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2837
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AL:Lup;

    invoke-virtual {v0}, Lup;->ff()V

    .line 2841
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->AC:Z

    if-eqz v0, :cond_0

    .line 2842
    iput-boolean v4, p0, Lcom/android/launcher3/CellLayout;->AC:Z

    .line 2846
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AB:[I

    const/4 v2, 0x1

    const/4 v3, -0x1

    aput v3, v1, v2

    aput v3, v0, v4

    .line 2847
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->Av:I

    aget-object v0, v0, v1

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lwo;->bj(I)V

    .line 2848
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Av:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/CellLayout;->Av:I

    .line 2849
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->fd()V

    .line 2850
    invoke-virtual {p0, v4}, Lcom/android/launcher3/CellLayout;->H(Z)V

    .line 2851
    return-void
.end method

.method public final fh()I
    .locals 3

    .prologue
    .line 3039
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zO:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public final fi()I
    .locals 3

    .prologue
    .line 3044
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zL:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zL:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zP:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public final fj()Z
    .locals 1

    .prologue
    .line 3267
    const/4 v0, 0x0

    return v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 3058
    new-instance v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 3068
    new-instance v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getBackgroundAlpha()F
    .locals 1

    .prologue
    .line 939
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ae:F

    return v0
.end method

.method public final i(F)V
    .locals 1

    .prologue
    .line 944
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Af:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 945
    iput p1, p0, Lcom/android/launcher3/CellLayout;->Af:F

    .line 946
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 948
    :cond_0
    return-void
.end method

.method public final j(F)V
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->setAlpha(F)V

    .line 963
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 408
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Ag:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ae:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_0

    .line 411
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout;->Ar:Z

    if-eqz v0, :cond_2

    .line 413
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ai:Landroid/graphics/drawable/Drawable;

    .line 418
    :goto_0
    iget v1, p0, Lcom/android/launcher3/CellLayout;->Ae:F

    iget v3, p0, Lcom/android/launcher3/CellLayout;->Af:F

    mul-float/2addr v1, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 419
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Am:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 420
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 423
    :cond_0
    iget-object v3, p0, Lcom/android/launcher3/CellLayout;->Aw:Landroid/graphics/Paint;

    move v1, v2

    .line 424
    :goto_1
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 425
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->At:[F

    aget v4, v0, v1

    .line 426
    cmpl-float v0, v4, v6

    if-lez v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->As:[Landroid/graphics/Rect;

    aget-object v0, v0, v1

    .line 428
    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->gn:Landroid/graphics/Rect;

    invoke-virtual {v5, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 429
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->gn:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v5

    invoke-static {v0, v5}, Ladp;->a(Landroid/graphics/Rect;F)V

    .line 430
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Au:[Lwo;

    aget-object v0, v0, v1

    iget-object v0, v0, Lwo;->Jx:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 431
    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 432
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/launcher3/CellLayout;->gn:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 424
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ah:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 453
    :cond_3
    sget v3, Lvs;->Ip:I

    .line 456
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 457
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v4

    move v1, v2

    .line 458
    :goto_2
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aa:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 459
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Aa:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvs;

    .line 463
    iget v5, v0, Lvs;->Ij:I

    iget v6, v0, Lvs;->Ik:I

    iget-object v7, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    invoke-direct {p0, v5, v6, v7}, Lcom/android/launcher3/CellLayout;->a(II[I)V

    .line 464
    iget v5, v0, Lvs;->Ij:I

    iget v6, v0, Lvs;->Ik:I

    invoke-virtual {p0, v5, v6}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v5

    .line 466
    if-eqz v5, :cond_4

    .line 467
    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    aget v6, v6, v2

    iget v7, p0, Lcom/android/launcher3/CellLayout;->zG:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    .line 468
    iget-object v7, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    aget v7, v7, v11

    div-int/lit8 v8, v3, 0x2

    add-int/2addr v7, v8

    invoke-virtual {v5}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    add-int/2addr v5, v7

    iget v7, v4, Ltu;->DR:I

    add-int/2addr v5, v7

    .line 473
    sget-object v7, Lvs;->In:Landroid/graphics/drawable/Drawable;

    .line 474
    invoke-virtual {v0}, Lvs;->gS()F

    move-result v8

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v9

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 476
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 477
    div-int/lit8 v9, v8, 0x2

    sub-int v9, v6, v9

    int-to-float v9, v9

    div-int/lit8 v10, v8, 0x2

    sub-int v10, v5, v10

    int-to-float v10, v10

    invoke-virtual {p1, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 478
    invoke-virtual {v7, v2, v2, v8, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 479
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 480
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 484
    sget-object v7, Lvs;->Io:Landroid/graphics/drawable/Drawable;

    .line 485
    invoke-virtual {v0}, Lvs;->gT()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v8

    mul-float/2addr v0, v8

    float-to-int v0, v0

    .line 487
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 488
    div-int/lit8 v8, v0, 0x2

    sub-int/2addr v6, v8

    int-to-float v6, v6

    div-int/lit8 v8, v0, 0x2

    sub-int/2addr v5, v8

    int-to-float v5, v5

    invoke-virtual {p1, v6, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 489
    invoke-virtual {v7, v2, v2, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 490
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 491
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 458
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 495
    :cond_5
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    aget v0, v0, v2

    if-ltz v0, :cond_6

    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    aget v0, v0, v11

    if-ltz v0, :cond_6

    .line 496
    sget-object v0, Lcom/android/launcher3/FolderIcon;->HK:Landroid/graphics/drawable/Drawable;

    .line 497
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 498
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 500
    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    aget v6, v6, v2

    iget-object v7, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    aget v7, v7, v11

    iget-object v8, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    invoke-direct {p0, v6, v7, v8}, Lcom/android/launcher3/CellLayout;->a(II[I)V

    .line 501
    iget-object v6, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    aget v6, v6, v2

    iget-object v7, p0, Lcom/android/launcher3/CellLayout;->Ab:[I

    aget v7, v7, v11

    invoke-virtual {p0, v6, v7}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v6

    .line 502
    if-eqz v6, :cond_6

    .line 503
    iget-object v7, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    aget v7, v7, v2

    iget v8, p0, Lcom/android/launcher3/CellLayout;->zG:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    .line 504
    iget-object v8, p0, Lcom/android/launcher3/CellLayout;->zV:[I

    aget v8, v8, v11

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v8

    invoke-virtual {v6}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    add-int/2addr v3, v6

    iget v4, v4, Ltu;->DR:I

    add-int/2addr v3, v4

    .line 507
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 508
    div-int/lit8 v4, v1, 0x2

    sub-int v4, v7, v4

    int-to-float v4, v4

    div-int/lit8 v6, v1, 0x2

    sub-int/2addr v3, v6

    int-to-float v3, v3

    invoke-virtual {p1, v4, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 509
    invoke-virtual {v0, v2, v2, v1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 510
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 511
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 514
    :cond_6
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zZ:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->zZ:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    const/4 v0, 0x1

    .line 687
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 902
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zG:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 904
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v1

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    add-int/2addr v1, v0

    .line 905
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v2

    .line 906
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getChildCount()I

    move-result v3

    .line 907
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 908
    invoke-virtual {p0, v0}, Lcom/android/launcher3/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 909
    add-int v5, v1, p4

    sub-int/2addr v5, p2

    add-int v6, v2, p5

    sub-int/2addr v6, p3

    invoke-virtual {v4, v1, v2, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 907
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 913
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    .line 835
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 836
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    .line 838
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    .line 839
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 840
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 841
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 842
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    sub-int v8, v11, v0

    .line 843
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    sub-int v7, v12, v0

    .line 844
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zI:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/CellLayout;->zJ:I

    if-gez v0, :cond_2

    .line 845
    :cond_0
    iget v0, p0, Lcom/android/launcher3/CellLayout;->zK:I

    div-int v0, v8, v0

    .line 846
    iget v1, p0, Lcom/android/launcher3/CellLayout;->zL:I

    div-int v1, v7, v1

    .line 847
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zG:I

    if-ne v0, v2, :cond_1

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    if-eq v1, v2, :cond_2

    .line 848
    :cond_1
    iput v0, p0, Lcom/android/launcher3/CellLayout;->zG:I

    .line 849
    iput v1, p0, Lcom/android/launcher3/CellLayout;->zH:I

    .line 850
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zO:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zP:I

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v6, p0, Lcom/android/launcher3/CellLayout;->zL:I

    invoke-virtual/range {v0 .. v6}, Ladg;->b(IIIIII)V

    .line 857
    :cond_2
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ap:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/android/launcher3/CellLayout;->sg:I

    if-lez v0, :cond_3

    .line 858
    iget v1, p0, Lcom/android/launcher3/CellLayout;->Ap:I

    .line 859
    iget v0, p0, Lcom/android/launcher3/CellLayout;->sg:I

    .line 864
    :goto_0
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zK:I

    .line 865
    iget v2, p0, Lcom/android/launcher3/CellLayout;->zL:I

    .line 867
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/launcher3/CellLayout;->zO:I

    .line 878
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/launcher3/CellLayout;->zP:I

    .line 880
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->getChildCount()I

    move-result v5

    .line 881
    const/4 v4, 0x0

    .line 882
    const/4 v3, 0x0

    .line 883
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_5

    .line 884
    invoke-virtual {p0, v2}, Lcom/android/launcher3/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 885
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 887
    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 889
    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    .line 890
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 891
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 883
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 860
    :cond_3
    if-eqz v9, :cond_4

    if-nez v10, :cond_7

    .line 861
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 893
    :cond_5
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ap:I

    if-lez v0, :cond_6

    iget v0, p0, Lcom/android/launcher3/CellLayout;->sg:I

    if-lez v0, :cond_6

    .line 894
    invoke-virtual {p0, v4, v3}, Lcom/android/launcher3/CellLayout;->setMeasuredDimension(II)V

    .line 898
    :goto_2
    return-void

    .line 896
    :cond_6
    invoke-virtual {p0, v11, v12}, Lcom/android/launcher3/CellLayout;->setMeasuredDimension(II)V

    goto :goto_2

    :cond_7
    move v0, v7

    move v1, v8

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 5

    .prologue
    .line 917
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 920
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 921
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Ah:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 922
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->Am:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p2

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 924
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->An:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->Ao:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->Ao:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->Ao:I

    sub-int v3, p1, v3

    iget v4, p0, Lcom/android/launcher3/CellLayout;->Ao:I

    sub-int v4, p2, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 926
    return-void
.end method

.method public removeAllViews()V
    .locals 1

    .prologue
    .line 628
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fg()V

    .line 629
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->removeAllViews()V

    .line 630
    return-void
.end method

.method public removeAllViewsInLayout()V
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 635
    invoke-direct {p0}, Lcom/android/launcher3/CellLayout;->fg()V

    .line 636
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0}, Ladg;->removeAllViewsInLayout()V

    .line 638
    :cond_0
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 646
    invoke-virtual {p0, p1}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 647
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->removeView(Landroid/view/View;)V

    .line 648
    return-void
.end method

.method public removeViewAt(I)V
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 653
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->removeViewAt(I)V

    .line 654
    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 658
    invoke-virtual {p0, p1}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 659
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->removeViewInLayout(Landroid/view/View;)V

    .line 660
    return-void
.end method

.method public removeViews(II)V
    .locals 2

    .prologue
    .line 664
    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 665
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v1, v0}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 664
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1, p2}, Ladg;->removeViews(II)V

    .line 668
    return-void
.end method

.method public removeViewsInLayout(II)V
    .locals 2

    .prologue
    .line 672
    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v1, v0}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 672
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1, p2}, Ladg;->removeViewsInLayout(II)V

    .line 676
    return-void
.end method

.method public setBackgroundAlpha(F)V
    .locals 1

    .prologue
    .line 955
    iget v0, p0, Lcom/android/launcher3/CellLayout;->Ae:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 956
    iput p1, p0, Lcom/android/launcher3/CellLayout;->Ae:F

    .line 957
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 959
    :cond_0
    return-void
.end method

.method protected setChildrenDrawingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 930
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->setChildrenDrawingCacheEnabled(Z)V

    .line 931
    return-void
.end method

.method protected setChildrenDrawnWithCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    invoke-virtual {v0, p1}, Ladg;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 936
    return-void
.end method

.method public final setFixedSize(II)V
    .locals 0

    .prologue
    .line 829
    iput p1, p0, Lcom/android/launcher3/CellLayout;->Ap:I

    .line 830
    iput p2, p0, Lcom/android/launcher3/CellLayout;->sg:I

    .line 831
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 550
    const/4 v0, 0x0

    return v0
.end method

.method public final z(II)V
    .locals 7

    .prologue
    .line 310
    iput p1, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iput p1, p0, Lcom/android/launcher3/CellLayout;->zI:I

    .line 311
    iput p2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iput p2, p0, Lcom/android/launcher3/CellLayout;->zJ:I

    .line 312
    iget-object v0, p0, Lcom/android/launcher3/CellLayout;->AE:Ladg;

    iget v1, p0, Lcom/android/launcher3/CellLayout;->zG:I

    iget v2, p0, Lcom/android/launcher3/CellLayout;->zH:I

    iget v3, p0, Lcom/android/launcher3/CellLayout;->zO:I

    iget v4, p0, Lcom/android/launcher3/CellLayout;->zP:I

    iget v5, p0, Lcom/android/launcher3/CellLayout;->zK:I

    iget v6, p0, Lcom/android/launcher3/CellLayout;->zL:I

    invoke-virtual/range {v0 .. v6}, Ladg;->b(IIIIII)V

    .line 314
    return-void
.end method
