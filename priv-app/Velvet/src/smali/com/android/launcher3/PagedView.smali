.class public abstract Lcom/android/launcher3/PagedView;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field private EN:I

.field private PS:Z

.field private PT:I

.field private PU:I

.field protected PV:I

.field private PW:I

.field private PX:I

.field protected PY:F

.field protected PZ:F

.field protected QA:I

.field protected QB:Z

.field private QC:Z

.field protected QD:I

.field protected QE:[I

.field protected QF:Z

.field QG:Z

.field protected QH:I

.field private QI:Lacm;

.field protected QJ:Ljava/util/ArrayList;

.field protected QK:Z

.field protected QL:Z

.field protected QM:Z

.field protected QN:Z

.field private QO:Z

.field protected QP:Z

.field private QQ:Z

.field protected QR:Z

.field private QS:I

.field QT:Lcom/android/launcher3/PageIndicator;

.field QU:Z

.field QV:Landroid/graphics/Rect;

.field private QW:I

.field public QX:I

.field private QY:I

.field QZ:Z

.field protected Qa:F

.field Qb:I

.field protected Qc:I

.field protected Qd:I

.field private Qe:I

.field protected Qf:I

.field protected Qg:I

.field protected Qh:Labj;

.field private Qi:Landroid/view/animation/Interpolator;

.field Qj:I

.field private Qk:F

.field private Ql:F

.field private Qm:F

.field private Qn:F

.field private Qo:F

.field private Qp:F

.field private Qq:F

.field private Qr:F

.field private Qs:I

.field private Qt:Z

.field private Qu:[I

.field protected Qv:I

.field private Qw:Z

.field protected Qx:Landroid/view/View$OnLongClickListener;

.field private Qy:I

.field protected Qz:I

.field public Ra:Landroid/view/View;

.field private Rb:Ljava/lang/Runnable;

.field private Rc:I

.field private Rd:Z

.field private Re:Z

.field private Rf:I

.field private Rg:I

.field private Rh:Ljava/lang/Runnable;

.field private Ri:Landroid/graphics/Matrix;

.field private Rj:[F

.field private Rk:[I

.field private Rl:Landroid/graphics/Rect;

.field private Rm:Z

.field private Rn:I

.field private Ro:I

.field private fW:Landroid/view/VelocityTracker;

.field protected gI:F

.field protected gJ:F

.field protected gM:I

.field private gO:I

.field protected gT:Z

.field private iA:Landroid/graphics/Rect;

.field protected mTouchSlop:I

.field ss:F

.field protected final yW:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 275
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 278
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->PS:Z

    .line 102
    iput v1, p0, Lcom/android/launcher3/PagedView;->PT:I

    .line 103
    iput v1, p0, Lcom/android/launcher3/PagedView;->PU:I

    .line 115
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->gT:Z

    .line 119
    const/16 v0, -0x3e9

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qd:I

    .line 122
    iput v1, p0, Lcom/android/launcher3/PagedView;->Qf:I

    .line 127
    iput v2, p0, Lcom/android/launcher3/PagedView;->Qj:I

    .line 139
    iput v1, p0, Lcom/android/launcher3/PagedView;->Qs:I

    .line 153
    iput v2, p0, Lcom/android/launcher3/PagedView;->Qv:I

    .line 154
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    .line 163
    iput v2, p0, Lcom/android/launcher3/PagedView;->Qz:I

    .line 164
    iput v2, p0, Lcom/android/launcher3/PagedView;->QA:I

    .line 166
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->QC:Z

    .line 168
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    .line 170
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QG:Z

    .line 179
    iput v1, p0, Lcom/android/launcher3/PagedView;->gM:I

    .line 186
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->QK:Z

    .line 189
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QL:Z

    .line 193
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->QM:Z

    .line 197
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QN:Z

    .line 198
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QO:Z

    .line 200
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QP:Z

    .line 203
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QQ:Z

    .line 205
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->QR:Z

    .line 210
    iput-boolean v3, p0, Lcom/android/launcher3/PagedView;->QU:Z

    .line 214
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    .line 219
    const/16 v0, 0xc8

    iput v0, p0, Lcom/android/launcher3/PagedView;->QW:I

    .line 220
    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/launcher3/PagedView;->QX:I

    .line 221
    const/16 v0, 0x50

    iput v0, p0, Lcom/android/launcher3/PagedView;->QY:I

    .line 223
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/launcher3/PagedView;->ss:F

    .line 224
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QZ:Z

    .line 228
    iput v1, p0, Lcom/android/launcher3/PagedView;->Rc:I

    .line 230
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->Rd:Z

    .line 235
    iput v4, p0, Lcom/android/launcher3/PagedView;->Rf:I

    .line 240
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Ri:Landroid/graphics/Matrix;

    .line 241
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    .line 242
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    .line 243
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->iA:Landroid/graphics/Rect;

    .line 244
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Rl:Landroid/graphics/Rect;

    .line 247
    const/16 v0, -0x578

    iput v0, p0, Lcom/android/launcher3/PagedView;->EN:I

    .line 253
    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->Rm:Z

    .line 254
    const/16 v0, 0xfa

    iput v0, p0, Lcom/android/launcher3/PagedView;->Rn:I

    .line 255
    const/16 v0, 0x15e

    iput v0, p0, Lcom/android/launcher3/PagedView;->Ro:I

    .line 261
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    .line 280
    sget-object v0, Ladb;->Sx:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 283
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 285
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 287
    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->QS:I

    .line 288
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 290
    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->setHapticFeedbackEnabled(Z)V

    .line 291
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->ed()V

    .line 292
    return-void
.end method

.method private N(II)Z
    .locals 5

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->iA:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1362
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->iA:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/android/launcher3/PagedView;I)I
    .locals 1

    .prologue
    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/PagedView;->Rc:I

    return v0
.end method

.method public static synthetic a(Lcom/android/launcher3/PagedView;)Lcom/android/launcher3/PageIndicator;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    return-object v0
.end method

.method private a(IIIZLandroid/animation/TimeInterpolator;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2243
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->bt(I)I

    move-result v0

    .line 2245
    iput v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    .line 2246
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 2247
    if-eqz v1, :cond_0

    iget v3, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 2249
    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 2252
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jr()V

    .line 2254
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jn()V

    .line 2255
    invoke-virtual {p0, p3}, Lcom/android/launcher3/PagedView;->awakenScrollBars(I)Z

    .line 2256
    if-eqz p4, :cond_3

    move v5, v2

    .line 2262
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2263
    invoke-direct {p0, v2}, Lcom/android/launcher3/PagedView;->ak(Z)V

    .line 2266
    :cond_1
    if-eqz p5, :cond_4

    .line 2267
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0, p5}, Labj;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2272
    :goto_1
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    iget v1, p0, Lcom/android/launcher3/PagedView;->QD:I

    move v3, p2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Labj;->startScroll(IIIII)V

    .line 2274
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jm()V

    .line 2277
    if-eqz p4, :cond_2

    .line 2278
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->computeScroll()V

    .line 2282
    :cond_2
    iput-boolean v6, p0, Lcom/android/launcher3/PagedView;->QO:Z

    .line 2284
    iput-boolean v6, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    .line 2285
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    .line 2286
    return-void

    .line 2258
    :cond_3
    if-nez p3, :cond_5

    .line 2259
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    goto :goto_0

    .line 2269
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Qi:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Labj;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_1

    :cond_5
    move v5, p3

    goto :goto_0
.end method

.method private a(IIZLandroid/animation/TimeInterpolator;)V
    .locals 6

    .prologue
    .line 2229
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->bt(I)I

    move-result v1

    .line 2231
    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    .line 2232
    iget v2, p0, Lcom/android/launcher3/PagedView;->QD:I

    .line 2233
    sub-int v2, v0, v2

    move-object v0, p0

    move v3, p2

    move v4, p3

    move-object v5, p4

    .line 2234
    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/PagedView;->a(IIIZLandroid/animation/TimeInterpolator;)V

    .line 2235
    return-void
.end method

.method private ak(Z)V
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->abortAnimation()V

    .line 528
    if-eqz p1, :cond_0

    .line 529
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    .line 531
    :cond_0
    return-void
.end method

.method private al(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1678
    iput-boolean p1, p0, Lcom/android/launcher3/PagedView;->PS:Z

    .line 1680
    iget-boolean v2, p0, Lcom/android/launcher3/PagedView;->PS:Z

    if-eqz v2, :cond_0

    .line 1681
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jx()V

    .line 1682
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->e([I)V

    .line 1683
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qc:I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v3, v3, v1

    if-ge v2, v3, :cond_1

    .line 1684
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v2, v2, v1

    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->bu(I)V

    .line 1690
    :cond_0
    :goto_0
    if-nez p1, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QC:Z

    .line 1691
    return-void

    .line 1685
    :cond_1
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qc:I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v3, v3, v0

    if-le v2, v3, :cond_0

    .line 1686
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->bu(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1690
    goto :goto_1
.end method

.method private am(Z)Z
    .locals 3

    .prologue
    .line 2536
    iget-boolean v1, p0, Lcom/android/launcher3/PagedView;->Re:Z

    .line 2537
    if-eqz p1, :cond_1

    .line 2538
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    .line 2540
    :goto_1
    return v0

    .line 2538
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static synthetic b(Lcom/android/launcher3/PagedView;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jJ()V

    return-void
.end method

.method private bt(I)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 541
    .line 543
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->PS:Z

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->e([I)V

    .line 545
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v3

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QE:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 549
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 550
    return v0
.end method

.method private bx(I)V
    .locals 2

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->am(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1087
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    iget-boolean v1, p0, Lcom/android/launcher3/PagedView;->QU:Z

    invoke-virtual {v0, p1, v1}, Lcom/android/launcher3/PageIndicator;->i(IZ)V

    .line 1089
    :cond_0
    return-void
.end method

.method private c(Landroid/view/View;FF)[F
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 371
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    aput p2, v0, v3

    .line 372
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    aput p3, v0, v4

    .line 373
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 374
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    aget v1, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    aput v1, v0, v3

    .line 375
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    aget v1, v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    aput v1, v0, v4

    .line 376
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    return-object v0
.end method

.method private d(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 2079
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 2081
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 2082
    iget v2, p0, Lcom/android/launcher3/PagedView;->gM:I

    if-ne v1, v2, :cond_0

    .line 2086
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2087
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qm:F

    iput v1, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 2088
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->gJ:F

    .line 2089
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qq:F

    .line 2090
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gM:I

    .line 2091
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2092
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2095
    :cond_0
    return-void

    .line 2086
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/view/View;FF)[F
    .locals 3

    .prologue
    .line 379
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, p2, v2

    aput v2, v0, v1

    .line 380
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, p3, v2

    aput v2, v0, v1

    .line 381
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Ri:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 382
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ri:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 383
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rj:[F

    return-object v0
.end method

.method private j(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 2064
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2065
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    .line 2067
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2068
    return-void
.end method

.method private jA()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2021
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jB()V

    .line 2022
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->Rd:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/android/launcher3/PagedView;->Rd:Z

    new-instance v0, Lacj;

    invoke-direct {v0, p0}, Lacj;-><init>(Lcom/android/launcher3/PagedView;)V

    iget-boolean v1, p0, Lcom/android/launcher3/PagedView;->Rm:Z

    if-nez v1, :cond_0

    new-instance v1, Lack;

    invoke-direct {v1, p0, v0}, Lack;-><init>(Lcom/android/launcher3/PagedView;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/android/launcher3/PagedView;->Rh:Ljava/lang/Runnable;

    iget v0, p0, Lcom/android/launcher3/PagedView;->Rf:I

    iput v0, p0, Lcom/android/launcher3/PagedView;->Rg:I

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/android/launcher3/PagedView;->P(II)V

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget v1, p0, Lcom/android/launcher3/PagedView;->QW:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const/4 v1, 0x4

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    const-string v3, "translationX"

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    const-string v3, "translationY"

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    const-string v4, "scaleX"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    const-string v4, "scaleY"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance v1, Laci;

    invoke-direct {v1, p0}, Laci;-><init>(Lcom/android/launcher3/PagedView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2023
    :cond_0
    iput-boolean v6, p0, Lcom/android/launcher3/PagedView;->Qt:Z

    .line 2024
    iput v6, p0, Lcom/android/launcher3/PagedView;->Qv:I

    .line 2025
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/PagedView;->gM:I

    .line 2026
    return-void
.end method

.method private jB()V
    .locals 1

    .prologue
    .line 2071
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2072
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2073
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2074
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    .line 2076
    :cond_0
    return-void
.end method

.method private jD()Z
    .locals 2

    .prologue
    .line 2142
    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    iget v1, p0, Lcom/android/launcher3/PagedView;->Qg:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jE()I
    .locals 1

    .prologue
    .line 2146
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2147
    const/16 v0, 0x15e

    .line 2149
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2ee

    goto :goto_0
.end method

.method private jJ()V
    .locals 1

    .prologue
    .line 2498
    iget v0, p0, Lcom/android/launcher3/PagedView;->Rg:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/launcher3/PagedView;->Rg:I

    .line 2499
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rh:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Rg:I

    if-nez v0, :cond_0

    .line 2501
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rh:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2502
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Rh:Ljava/lang/Runnable;

    .line 2504
    :cond_0
    return-void
.end method

.method private ja()V
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 388
    iget v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    iget v1, p0, Lcom/android/launcher3/PagedView;->Qm:F

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/android/launcher3/PagedView;->Qo:F

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/PagedView;->Qp:F

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 390
    iget v1, p0, Lcom/android/launcher3/PagedView;->gJ:F

    iget v2, p0, Lcom/android/launcher3/PagedView;->Qn:F

    sub-float/2addr v1, v2

    .line 391
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 392
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 397
    :cond_0
    return-void
.end method

.method private jj()V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Labj;->forceFinished(Z)V

    .line 537
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    .line 538
    return-void
.end method

.method private jm()V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->iY()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PageIndicator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 599
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->am(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PageIndicator;->br(I)V

    .line 603
    :cond_0
    return-void
.end method

.method private jn()V
    .locals 1

    .prologue
    .line 605
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QP:Z

    if-nez v0, :cond_0

    .line 606
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QP:Z

    .line 607
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jq()V

    .line 609
    :cond_0
    return-void
.end method

.method private jo()V
    .locals 1

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QP:Z

    if-eqz v0, :cond_0

    .line 613
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QP:Z

    .line 614
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->eu()V

    .line 616
    :cond_0
    return-void
.end method

.method private jr()V
    .locals 4

    .prologue
    const/16 v1, 0x1000

    .line 699
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 701
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 704
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 705
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 706
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 709
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    iget v3, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-lt v0, v3, :cond_1

    move v0, v1

    .line 715
    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setAction(I)V

    .line 716
    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 718
    :cond_0
    return-void

    .line 712
    :cond_1
    const/16 v0, 0x2000

    goto :goto_0
.end method

.method private static jt()Lacl;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 789
    new-instance v0, Lacl;

    invoke-direct {v0, v1, v1}, Lacl;-><init>(II)V

    return-object v0
.end method

.method private jx()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1667
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->e([I)V

    .line 1668
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1669
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PT:I

    .line 1670
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PU:I

    .line 1675
    :goto_0
    return-void

    .line 1672
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PT:I

    .line 1673
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PU:I

    goto :goto_0
.end method

.method public static jy()V
    .locals 0

    .prologue
    .line 2016
    return-void
.end method

.method public static jz()V
    .locals 0

    .prologue
    .line 2018
    return-void
.end method


# virtual methods
.method public O(II)V
    .locals 10

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 2178
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->bt(I)I

    move-result v0

    .line 2179
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 2181
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v2

    .line 2182
    iget v3, p0, Lcom/android/launcher3/PagedView;->QD:I

    sub-int/2addr v2, v3

    .line 2183
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Lcom/android/launcher3/PagedView;->PW:I

    if-lt v3, v4, :cond_0

    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jD()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2188
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jE()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->P(II)V

    .line 2209
    :goto_0
    return-void

    .line 2196
    :cond_1
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v5

    mul-int/lit8 v4, v1, 0x2

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 2197
    int-to-float v4, v1

    int-to-float v1, v1

    const/high16 v5, 0x3f000000    # 0.5f

    sub-float/2addr v3, v5

    float-to-double v6, v3

    const-wide v8, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v6, v8

    double-to-float v3, v6

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v3, v6

    mul-float/2addr v1, v3

    add-float/2addr v1, v4

    .line 2200
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 2201
    iget v4, p0, Lcom/android/launcher3/PagedView;->PX:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2206
    const/high16 v4, 0x447a0000    # 1000.0f

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    .line 2208
    invoke-virtual {p0, v0, v2, v1}, Lcom/android/launcher3/PagedView;->g(III)V

    goto :goto_0
.end method

.method protected final P(II)V
    .locals 2

    .prologue
    .line 2220
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/launcher3/PagedView;->a(IIZLandroid/animation/TimeInterpolator;)V

    .line 2221
    return-void
.end method

.method public final Z(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 793
    invoke-static {}, Lcom/android/launcher3/PagedView;->jt()Lacl;

    move-result-object v0

    .line 794
    const/4 v1, 0x1

    iput-boolean v1, v0, Lacl;->Rs:Z

    .line 795
    const/4 v1, 0x0

    invoke-super {p0, p1, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 796
    return-void
.end method

.method protected final a(IILandroid/animation/TimeInterpolator;)V
    .locals 1

    .prologue
    .line 2224
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/android/launcher3/PagedView;->a(IIZLandroid/animation/TimeInterpolator;)V

    .line 2225
    return-void
.end method

.method public final a(Lacm;)V
    .locals 3

    .prologue
    .line 447
    iput-object p1, p0, Lcom/android/launcher3/PagedView;->QI:Lacm;

    .line 448
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QI:Lacm;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QI:Lacm;

    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-interface {v0, v1, v2}, Lacm;->i(Landroid/view/View;I)V

    .line 451
    :cond_0
    return-void
.end method

.method protected final a(Landroid/view/MotionEvent;F)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1491
    iget v2, p0, Lcom/android/launcher3/PagedView;->gM:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    .line 1492
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 1519
    :cond_0
    :goto_0
    return-void

    .line 1495
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    .line 1496
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 1497
    float-to-int v3, v4

    float-to-int v5, v2

    invoke-direct {p0, v3, v5}, Lcom/android/launcher3/PagedView;->N(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1499
    iget v3, p0, Lcom/android/launcher3/PagedView;->gI:F

    sub-float v3, v4, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v5, v3

    .line 1500
    iget v3, p0, Lcom/android/launcher3/PagedView;->gJ:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v6, v2

    .line 1502
    iget v2, p0, Lcom/android/launcher3/PagedView;->mTouchSlop:I

    int-to-float v2, v2

    mul-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1503
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qy:I

    if-le v5, v2, :cond_4

    move v3, v1

    .line 1504
    :goto_1
    if-le v5, v7, :cond_5

    move v2, v1

    .line 1505
    :goto_2
    if-le v6, v7, :cond_2

    move v0, v1

    .line 1507
    :cond_2
    if-nez v2, :cond_3

    if-nez v3, :cond_3

    if-eqz v0, :cond_0

    .line 1508
    :cond_3
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QM:Z

    if-eqz v0, :cond_6

    if-eqz v3, :cond_0

    .line 1510
    :goto_3
    iput v1, p0, Lcom/android/launcher3/PagedView;->Qv:I

    .line 1511
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qr:F

    iget v1, p0, Lcom/android/launcher3/PagedView;->gI:F

    sub-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qr:F

    .line 1512
    iput v4, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 1513
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qq:F

    .line 1514
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qa:F

    .line 1515
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/PagedView;->PZ:F

    .line 1516
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jn()V

    goto :goto_0

    :cond_4
    move v3, v0

    .line 1503
    goto :goto_1

    :cond_5
    move v2, v0

    .line 1504
    goto :goto_2

    .line 1508
    :cond_6
    if-eqz v2, :cond_0

    goto :goto_3
.end method

.method protected final a(Landroid/view/animation/Interpolator;)V
    .locals 2

    .prologue
    .line 322
    iput-object p1, p0, Lcom/android/launcher3/PagedView;->Qi:Landroid/view/animation/Interpolator;

    .line 323
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Qi:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Labj;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 324
    return-void
.end method

.method public aS(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 490
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public aT(I)I
    .locals 0

    .prologue
    .line 494
    return p1
.end method

.method protected aU(I)V
    .locals 1

    .prologue
    .line 1035
    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    .line 1037
    :cond_0
    return-void
.end method

.method protected aV(I)I
    .locals 2

    .prologue
    .line 2399
    const/4 v0, 0x0

    add-int/lit8 v1, p1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected aW(I)I
    .locals 2

    .prologue
    .line 2402
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    .line 2403
    add-int/lit8 v1, p1, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method protected aa(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1184
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ab(Landroid/view/View;)I
    .locals 4

    .prologue
    .line 2297
    if-eqz p1, :cond_1

    .line 2299
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2300
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    .line 2301
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 2302
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v3

    if-ne v1, v3, :cond_0

    .line 2307
    :goto_1
    return v0

    .line 2301
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2307
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final ac(Landroid/view/View;)Z
    .locals 6

    .prologue
    const v5, 0x3f933333    # 1.15f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2511
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 2513
    iget v3, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-nez v3, :cond_0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 2532
    :goto_0
    return v0

    .line 2515
    :cond_1
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aput v1, v3, v1

    .line 2516
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    aput v4, v3, v0

    .line 2517
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v3}, Lcom/android/launcher3/PagedView;->e([I)V

    .line 2518
    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->Rd:Z

    .line 2521
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v3, v3, v1

    if-gt v3, v2, :cond_2

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v3, v3, v0

    if-gt v2, v3, :cond_2

    .line 2524
    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    .line 2525
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2526
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/launcher3/PagedView;->Qp:F

    .line 2527
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jC()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 2528
    invoke-direct {p0, v1}, Lcom/android/launcher3/PagedView;->al(Z)V

    .line 2529
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jI()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2532
    goto :goto_0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2

    .prologue
    .line 1280
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1281
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1283
    :cond_0
    const/16 v0, 0x11

    if-ne p2, v0, :cond_2

    .line 1284
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-lez v0, :cond_1

    .line 1285
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1292
    :cond_1
    :goto_0
    return-void

    .line 1287
    :cond_2
    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    .line 1288
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 1289
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method public final bA(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1583
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 1595
    :cond_0
    :goto_0
    return v1

    .line 1586
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1589
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lacl;

    .line 1590
    iget-boolean v0, v0, Lacl;->Rs:Z

    if-nez v0, :cond_3

    .line 1591
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingRight()I

    move-result v0

    .line 1594
    :goto_1
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    aget v1, v1, p1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v1

    add-int/2addr v0, v1

    .line 1595
    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v1

    int-to-float v0, v0

    sub-float v0, v1, v0

    float-to-int v1, v0

    goto :goto_0

    .line 1591
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingLeft()I

    move-result v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public bB(I)V
    .locals 1

    .prologue
    .line 2212
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jE()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher3/PagedView;->P(II)V

    .line 2213
    return-void
.end method

.method protected final bC(I)V
    .locals 1

    .prologue
    .line 2362
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher3/PagedView;->j(IZ)V

    .line 2363
    return-void
.end method

.method protected final bD(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2424
    invoke-virtual {p0, v0, v0}, Lcom/android/launcher3/PagedView;->k(IZ)V

    .line 2425
    return-void
.end method

.method protected bs(I)Lacg;
    .locals 1

    .prologue
    .line 439
    new-instance v0, Lacg;

    invoke-direct {v0}, Lacg;-><init>()V

    return-object v0
.end method

.method public final bu(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 557
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558
    invoke-direct {p0, v1}, Lcom/android/launcher3/PagedView;->ak(Z)V

    .line 562
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 570
    :goto_0
    return-void

    .line 565
    :cond_1
    iput-boolean v1, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    .line 566
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->bt(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    .line 567
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jh()V

    .line 568
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jl()V

    .line 569
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    goto :goto_0
.end method

.method final bv(I)V
    .locals 0

    .prologue
    .line 577
    iput p1, p0, Lcom/android/launcher3/PagedView;->Qd:I

    .line 578
    return-void
.end method

.method public final bw(I)V
    .locals 0

    .prologue
    .line 1030
    iput p1, p0, Lcom/android/launcher3/PagedView;->Qj:I

    .line 1031
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->requestLayout()V

    .line 1032
    return-void
.end method

.method public final by(I)I
    .locals 2

    .prologue
    .line 1124
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 1128
    :goto_0
    return v0

    .line 1126
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1128
    goto :goto_0
.end method

.method public final bz(I)I
    .locals 1

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 1574
    :cond_0
    const/4 v0, 0x0

    .line 1576
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 0

    .prologue
    .line 766
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->js()Z

    .line 767
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v4, -0x1

    const/4 v10, 0x0

    .line 1190
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    .line 1191
    if-lez v0, :cond_6

    .line 1192
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 1195
    iget v2, p0, Lcom/android/launcher3/PagedView;->QH:I

    add-int/2addr v1, v2

    .line 1197
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qs:I

    if-ne v1, v2, :cond_0

    iget-boolean v2, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    if-eqz v2, :cond_1

    .line 1200
    :cond_0
    iput-boolean v10, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    .line 1201
    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->aU(I)V

    .line 1202
    iput v1, p0, Lcom/android/launcher3/PagedView;->Qs:I

    .line 1205
    :cond_1
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->f([I)V

    .line 1206
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v1, v1, v10

    .line 1207
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QE:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    .line 1208
    if-eq v1, v4, :cond_6

    if-eq v2, v4, :cond_6

    .line 1209
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getDrawingTime()J

    move-result-wide v4

    .line 1211
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1212
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v7

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getRight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getBottom()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p1, v3, v6, v7, v8}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 1216
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_4

    .line 1217
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v3

    .line 1218
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    if-eq v3, v6, :cond_3

    .line 1219
    iget-boolean v6, p0, Lcom/android/launcher3/PagedView;->QF:Z

    if-nez v6, :cond_2

    if-gt v1, v0, :cond_3

    if-gt v0, v2, :cond_3

    invoke-virtual {p0, v3}, Lcom/android/launcher3/PagedView;->aa(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1221
    :cond_2
    invoke-virtual {p0, p1, v3, v4, v5}, Lcom/android/launcher3/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 1216
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1225
    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 1226
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {p0, p1, v0, v4, v5}, Lcom/android/launcher3/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 1229
    :cond_5
    iput-boolean v10, p0, Lcom/android/launcher3/PagedView;->QF:Z

    .line 1230
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1233
    :cond_6
    return-void
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1263
    const/16 v1, 0x11

    if-ne p2, v1, :cond_0

    .line 1264
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-lez v1, :cond_1

    .line 1265
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 1274
    :goto_0
    return v0

    .line 1268
    :cond_0
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    .line 1269
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 1270
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->bB(I)V

    goto :goto_0

    .line 1274
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method protected e([I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1132
    aput v2, p1, v2

    .line 1133
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    aput v1, p1, v0

    .line 1134
    return-void
.end method

.method public ed()V
    .locals 2

    .prologue
    .line 298
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    .line 299
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 300
    new-instance v0, Labj;

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Labj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    .line 301
    new-instance v0, Lacn;

    invoke-direct {v0}, Lacn;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->a(Landroid/view/animation/Interpolator;)V

    .line 302
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QB:Z

    .line 305
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->mTouchSlop:I

    .line 307
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qy:I

    .line 308
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gO:I

    .line 309
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/launcher3/PagedView;->PY:F

    .line 312
    iget v0, p0, Lcom/android/launcher3/PagedView;->EN:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/launcher3/PagedView;->PY:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->EN:I

    .line 315
    const/high16 v0, 0x43fa0000    # 500.0f

    iget v1, p0, Lcom/android/launcher3/PagedView;->PY:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PV:I

    .line 316
    const/high16 v0, 0x437a0000    # 250.0f

    iget v1, p0, Lcom/android/launcher3/PagedView;->PY:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PW:I

    .line 317
    const v0, 0x44bb8000    # 1500.0f

    iget v1, p0, Lcom/android/launcher3/PagedView;->PY:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->PX:I

    .line 318
    invoke-virtual {p0, p0}, Lcom/android/launcher3/PagedView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 319
    return-void
.end method

.method public abstract es()V
.end method

.method protected eu()V
    .locals 0

    .prologue
    .line 628
    return-void
.end method

.method protected ex()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2880
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00c0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract f(IZ)V
.end method

.method protected final f([I)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v2, 0x0

    .line 1137
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    .line 1138
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    aput v2, v3, v9

    aput v2, v1, v2

    .line 1140
    aput v8, p1, v2

    .line 1141
    aput v8, p1, v9

    .line 1143
    if-lez v0, :cond_5

    .line 1144
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 1147
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v4

    move v1, v2

    move v0, v2

    .line 1148
    :goto_0
    if-ge v1, v4, :cond_2

    .line 1149
    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v5

    .line 1151
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    aput v2, v6, v2

    .line 1152
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    invoke-static {v5, p0, v6, v2}, Ladp;->a(Landroid/view/View;Landroid/view/View;[IZ)F

    .line 1153
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    aget v6, v6, v2

    if-le v6, v3, :cond_1

    .line 1154
    aget v5, p1, v2

    if-ne v5, v8, :cond_2

    .line 1148
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1161
    :cond_1
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    aput v7, v6, v2

    .line 1162
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    invoke-static {v5, p0, v6, v2}, Ladp;->a(Landroid/view/View;Landroid/view/View;[IZ)F

    .line 1163
    iget-object v5, p0, Lcom/android/launcher3/PagedView;->Rk:[I

    aget v5, v5, v2

    if-gez v5, :cond_3

    .line 1164
    aget v5, p1, v2

    if-eq v5, v8, :cond_0

    .line 1176
    :cond_2
    aput v0, p1, v9

    .line 1181
    :goto_2
    return-void

    .line 1171
    :cond_3
    aget v0, p1, v2

    if-gez v0, :cond_4

    .line 1172
    aput v1, p1, v2

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1178
    :cond_5
    aput v8, p1, v2

    .line 1179
    aput v8, p1, v9

    goto :goto_2
.end method

.method public fU()V
    .locals 1

    .prologue
    .line 2289
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 2290
    :cond_0
    return-void
.end method

.method public fV()V
    .locals 2

    .prologue
    .line 2293
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 2294
    :cond_0
    return-void
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1303
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v1

    move-object v0, p1

    .line 1306
    :goto_0
    if-ne v0, v1, :cond_1

    .line 1307
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    .line 1317
    :cond_0
    return-void

    .line 1310
    :cond_1
    if-eq v0, p0, :cond_0

    .line 1313
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 1314
    instance-of v2, v2, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1315
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method protected g(III)V
    .locals 6

    .prologue
    .line 2238
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/PagedView;->a(IIIZLandroid/animation/TimeInterpolator;)V

    .line 2239
    return-void
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/android/launcher3/PagedView;->jt()Lacl;

    move-result-object v0

    return-object v0
.end method

.method protected h(F)V
    .locals 0

    .prologue
    .line 1647
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->u(F)V

    .line 1648
    return-void
.end method

.method protected iY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->ex()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected iZ()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final j(IZ)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2365
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QK:Z

    if-eqz v0, :cond_7

    .line 2366
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v4

    .line 2367
    if-ge p1, v4, :cond_7

    .line 2368
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->aV(I)I

    move-result v5

    .line 2369
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->aW(I)I

    move-result v6

    move v3, v2

    .line 2373
    :goto_0
    if-ge v3, v4, :cond_3

    .line 2374
    invoke-virtual {p0, v3}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lacf;

    .line 2375
    if-lt v3, v5, :cond_0

    if-le v3, v6, :cond_2

    .line 2376
    :cond_0
    invoke-interface {v0}, Lacf;->ec()I

    move-result v7

    if-lez v7, :cond_1

    .line 2377
    invoke-interface {v0}, Lacf;->eb()V

    .line 2379
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v3, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2373
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    move v3, v2

    .line 2383
    :goto_1
    if-ge v3, v4, :cond_7

    .line 2384
    if-eq v3, p1, :cond_4

    if-nez p2, :cond_5

    .line 2385
    :cond_4
    if-gt v5, v3, :cond_5

    if-gt v3, v6, :cond_5

    .line 2388
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2389
    if-ne v3, p1, :cond_6

    if-eqz p2, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {p0, v3, v0}, Lcom/android/launcher3/PagedView;->f(IZ)V

    .line 2390
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v3, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2383
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_6
    move v0, v2

    .line 2389
    goto :goto_2

    .line 2396
    :cond_7
    return-void
.end method

.method protected final jC()I
    .locals 8

    .prologue
    .line 2123
    const v3, 0x7fffffff

    .line 2124
    const/4 v0, -0x1

    .line 2125
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int v4, v1, v2

    .line 2126
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v5

    .line 2127
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    .line 2128
    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v2

    .line 2129
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 2130
    div-int/lit8 v2, v2, 0x2

    .line 2131
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v6

    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->by(I)I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v2, v6

    .line 2132
    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 2133
    if-ge v2, v3, :cond_1

    move v0, v1

    .line 2127
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_0

    .line 2138
    :cond_0
    return v0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public jF()V
    .locals 2

    .prologue
    .line 2154
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jC()I

    move-result v0

    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jE()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->P(II)V

    .line 2155
    return-void
.end method

.method public final jG()Z
    .locals 1

    .prologue
    .line 2314
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QR:Z

    return v0
.end method

.method protected final jH()V
    .locals 2

    .prologue
    .line 2421
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->k(IZ)V

    .line 2422
    return-void
.end method

.method protected jI()V
    .locals 1

    .prologue
    .line 2488
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    .line 2489
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->Re:Z

    .line 2493
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    .line 2494
    return-void
.end method

.method public jK()V
    .locals 1

    .prologue
    .line 2507
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->Re:Z

    .line 2508
    return-void
.end method

.method public final jb()I
    .locals 2

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final jc()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 457
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jd()V
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QQ:Z

    .line 466
    return-void
.end method

.method protected final je()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QQ:Z

    return v0
.end method

.method public final jf()I
    .locals 1

    .prologue
    .line 478
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    return v0
.end method

.method public final jg()I
    .locals 2

    .prologue
    .line 482
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    goto :goto_0
.end method

.method public final jh()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 504
    .line 505
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 506
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    .line 508
    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->scrollTo(II)V

    .line 509
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v1, v0}, Labj;->setFinalX(I)V

    .line 510
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jj()V

    .line 511
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method final ji()V
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    .line 520
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jl()V

    .line 521
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jj()V

    .line 522
    return-void
.end method

.method final jk()I
    .locals 1

    .prologue
    .line 580
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qd:I

    return v0
.end method

.method protected jl()V
    .locals 3

    .prologue
    .line 588
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QI:Lacm;

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QI:Lacm;

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lacm;->i(Landroid/view/View;I)V

    .line 592
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jm()V

    .line 593
    return-void
.end method

.method public final jp()Z
    .locals 1

    .prologue
    .line 619
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QP:Z

    return v0
.end method

.method protected jq()V
    .locals 0

    .prologue
    .line 624
    return-void
.end method

.method protected final js()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 722
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 724
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v0

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->getCurrX()I

    move-result v3

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v0

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->getCurrY()I

    move-result v3

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->getCurrX()I

    move-result v3

    if-eq v0, v3, :cond_1

    .line 727
    :cond_0
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->PS:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScaleX()F

    move-result v0

    .line 728
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->getCurrX()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v1, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 729
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v1}, Labj;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->scrollTo(II)V

    .line 731
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    move v0, v2

    .line 761
    :cond_2
    :goto_1
    return v0

    :cond_3
    move v0, v1

    .line 727
    goto :goto_0

    .line 733
    :cond_4
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qf:I

    if-eq v1, v4, :cond_2

    .line 734
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jr()V

    .line 736
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qf:I

    invoke-direct {p0, v1}, Lcom/android/launcher3/PagedView;->bt(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    .line 737
    iput v4, p0, Lcom/android/launcher3/PagedView;->Qf:I

    .line 738
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jl()V

    .line 741
    iget-boolean v1, p0, Lcom/android/launcher3/PagedView;->QO:Z

    if-eqz v1, :cond_5

    .line 742
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/PagedView;->j(IZ)V

    .line 743
    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QO:Z

    .line 748
    :cond_5
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-nez v0, :cond_6

    .line 749
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jo()V

    .line 752
    :cond_6
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jJ()V

    .line 753
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 755
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 757
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->ex()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    :cond_7
    move v0, v2

    .line 759
    goto :goto_1
.end method

.method protected final ju()V
    .locals 1

    .prologue
    .line 1526
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QR:Z

    if-eqz v0, :cond_0

    .line 1531
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    .line 1532
    if-eqz v0, :cond_0

    .line 1533
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 1536
    :cond_0
    return-void
.end method

.method public final jv()V
    .locals 1

    .prologue
    .line 1659
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->al(Z)V

    .line 1660
    return-void
.end method

.method protected final jw()V
    .locals 1

    .prologue
    .line 1663
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->al(Z)V

    .line 1664
    return-void
.end method

.method protected final k(IZ)V
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 2427
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QQ:Z

    if-nez v0, :cond_1

    .line 2464
    :cond_0
    :goto_0
    return-void

    .line 2431
    :cond_1
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QK:Z

    if-eqz v0, :cond_4

    .line 2433
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jj()V

    .line 2436
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->es()V

    .line 2440
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->measure(II)V

    .line 2444
    if-ltz p1, :cond_2

    .line 2445
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bu(I)V

    .line 2449
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    .line 2450
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2451
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 2452
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QJ:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2451
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2456
    :cond_3
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0, p2}, Lcom/android/launcher3/PagedView;->j(IZ)V

    .line 2457
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->requestLayout()V

    .line 2459
    :cond_4
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QP:Z

    if-eqz v0, :cond_0

    .line 2462
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jF()V

    goto :goto_0
.end method

.method public k(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 1482
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher3/PagedView;->a(Landroid/view/MotionEvent;F)V

    .line 1483
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 327
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 330
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 331
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 332
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/launcher3/PagedView;->QS:I

    if-ltz v1, :cond_2

    .line 333
    iget v1, p0, Lcom/android/launcher3/PagedView;->QS:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PageIndicator;

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    .line 334
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    iget-boolean v1, p0, Lcom/android/launcher3/PagedView;->QU:Z

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PageIndicator;->ah(Z)V

    .line 336
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 338
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 339
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bs(I)Lacg;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    iget-boolean v2, p0, Lcom/android/launcher3/PagedView;->QU:Z

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/PageIndicator;->a(Ljava/util/ArrayList;Z)V

    .line 344
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->iZ()Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_1

    .line 346
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/PageIndicator;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->iY()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PageIndicator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 350
    :cond_2
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->am(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1063
    invoke-virtual {p0, p2}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 1064
    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bs(I)Lacg;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/launcher3/PagedView;->QU:Z

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/launcher3/PageIndicator;->a(ILacg;Z)V

    .line 1071
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    .line 1072
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jx()V

    .line 1073
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    .line 1074
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1078
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->Qw:Z

    .line 1079
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jx()V

    .line 1080
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    .line 1081
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    .line 363
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/16 v3, 0x9

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 2034
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 2035
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2060
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 2040
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    .line 2042
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    move v3, v4

    .line 2047
    :goto_1
    cmpl-float v5, v2, v4

    if-nez v5, :cond_1

    cmpl-float v5, v3, v4

    if-eqz v5, :cond_0

    .line 2048
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v5

    if-eqz v5, :cond_5

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_2

    cmpg-float v2, v3, v4

    if-gez v2, :cond_3

    :cond_2
    move v1, v0

    .line 2050
    :cond_3
    :goto_2
    if-eqz v1, :cond_7

    .line 2051
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->fV()V

    goto :goto_0

    .line 2044
    :cond_4
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    neg-float v3, v2

    .line 2045
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    goto :goto_1

    .line 2048
    :cond_5
    cmpl-float v2, v2, v4

    if-gtz v2, :cond_6

    cmpl-float v2, v3, v4

    if-lez v2, :cond_3

    :cond_6
    move v1, v0

    goto :goto_2

    .line 2053
    :cond_7
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->fU()V

    goto :goto_0

    .line 2035
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2886
    const/4 v0, 0x1

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 2853
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2854
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 2855
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2833
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 2834
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    if-le v1, v0, :cond_2

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 2835
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 2836
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 2838
    :cond_0
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-lez v0, :cond_1

    .line 2839
    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 2841
    :cond_1
    return-void

    .line 2834
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1376
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->j(Landroid/view/MotionEvent;)V

    .line 1379
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    if-gtz v2, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1478
    :cond_0
    :goto_0
    return v0

    .line 1386
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1387
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    iget v3, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-eq v3, v0, :cond_0

    .line 1392
    :cond_2
    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 1478
    :cond_3
    :goto_1
    :pswitch_0
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1398
    :pswitch_1
    iget v2, p0, Lcom/android/launcher3/PagedView;->gM:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 1399
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->k(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 1410
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1411
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1413
    iput v2, p0, Lcom/android/launcher3/PagedView;->Qm:F

    .line 1414
    iput v3, p0, Lcom/android/launcher3/PagedView;->Qn:F

    .line 1415
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v4

    int-to-float v4, v4

    iput v4, p0, Lcom/android/launcher3/PagedView;->Qo:F

    .line 1416
    iput v2, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 1417
    iput v3, p0, Lcom/android/launcher3/PagedView;->gJ:F

    .line 1418
    invoke-direct {p0, p0, v2, v3}, Lcom/android/launcher3/PagedView;->c(Landroid/view/View;FF)[F

    move-result-object v2

    .line 1419
    aget v3, v2, v1

    iput v3, p0, Lcom/android/launcher3/PagedView;->Qk:F

    .line 1420
    aget v2, v2, v0

    iput v2, p0, Lcom/android/launcher3/PagedView;->Ql:F

    .line 1421
    iput v5, p0, Lcom/android/launcher3/PagedView;->Qq:F

    .line 1422
    iput v5, p0, Lcom/android/launcher3/PagedView;->Qr:F

    .line 1423
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iput v2, p0, Lcom/android/launcher3/PagedView;->gM:I

    .line 1430
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v2}, Labj;->getFinalX()I

    move-result v2

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->getCurrX()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 1431
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v3}, Labj;->isFinished()Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p0, Lcom/android/launcher3/PagedView;->mTouchSlop:I

    div-int/lit8 v3, v3, 0x3

    if-ge v2, v3, :cond_5

    :cond_4
    move v2, v0

    .line 1433
    :goto_2
    if-eqz v2, :cond_6

    .line 1434
    iput v1, p0, Lcom/android/launcher3/PagedView;->Qv:I

    .line 1435
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v2}, Labj;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/android/launcher3/PagedView;->PS:Z

    if-nez v2, :cond_3

    .line 1436
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedView;->bu(I)V

    .line 1437
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jo()V

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1431
    goto :goto_2

    .line 1440
    :cond_6
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qm:F

    float-to-int v2, v2

    iget v3, p0, Lcom/android/launcher3/PagedView;->Qn:F

    float-to-int v3, v3

    invoke-direct {p0, v2, v3}, Lcom/android/launcher3/PagedView;->N(II)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1441
    iput v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    goto/16 :goto_1

    .line 1443
    :cond_7
    iput v1, p0, Lcom/android/launcher3/PagedView;->Qv:I

    goto/16 :goto_1

    .line 1465
    :pswitch_3
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jA()V

    goto/16 :goto_1

    .line 1469
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->d(Landroid/view/MotionEvent;)V

    .line 1470
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jB()V

    goto/16 :goto_1

    .line 1392
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 14

    .prologue
    .line 928
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QQ:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 933
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v8

    .line 935
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v9

    .line 936
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v3, v0, 0x2

    .line 939
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0, v9, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 941
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v2

    .line 943
    if-eqz v2, :cond_5

    add-int/lit8 v1, v8, -0x1

    .line 944
    :goto_1
    if-eqz v2, :cond_6

    const/4 v0, -0x1

    move v7, v0

    .line 945
    :goto_2
    if-eqz v2, :cond_7

    const/4 v0, -0x1

    move v2, v0

    .line 947
    :goto_3
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingBottom()I

    move-result v4

    add-int v10, v0, v4

    .line 949
    invoke-virtual {p0, v1}, Lcom/android/launcher3/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lacl;

    .line 952
    iget-boolean v0, v0, Lacl;->Rs:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_4
    add-int v5, v9, v0

    .line 953
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    iget v4, p0, Lcom/android/launcher3/PagedView;->Qe:I

    if-eq v0, v4, :cond_3

    .line 954
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    :cond_3
    move v6, v1

    .line 957
    :goto_5
    if-eq v6, v7, :cond_d

    .line 958
    invoke-virtual {p0, v6}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v4

    .line 959
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_13

    .line 960
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lacl;

    .line 962
    iget-boolean v0, v1, Lacl;->Rs:Z

    if-eqz v0, :cond_9

    move v0, v3

    .line 971
    :cond_4
    :goto_6
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    .line 972
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    .line 975
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    add-int/2addr v13, v5

    add-int/2addr v12, v0

    invoke-virtual {v4, v5, v0, v13, v12}, Landroid/view/View;->layout(IIII)V

    .line 978
    iget-boolean v0, v1, Lacl;->Rs:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    .line 979
    :goto_7
    iget-object v4, p0, Lcom/android/launcher3/PagedView;->Qu:[I

    sub-int v0, v5, v0

    sub-int/2addr v0, v9

    aput v0, v4, v6

    .line 981
    iget v4, p0, Lcom/android/launcher3/PagedView;->Qj:I

    .line 982
    add-int v0, v6, v2

    .line 983
    if-eq v0, v7, :cond_b

    .line 984
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lacl;

    .line 991
    :goto_8
    iget-boolean v1, v1, Lacl;->Rs:Z

    if-eqz v1, :cond_c

    .line 992
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingLeft()I

    move-result v0

    .line 997
    :goto_9
    add-int/2addr v0, v11

    add-int/2addr v0, v5

    .line 957
    :goto_a
    add-int v1, v6, v2

    move v6, v1

    move v5, v0

    goto :goto_5

    .line 943
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_6
    move v7, v8

    .line 944
    goto/16 :goto_2

    .line 945
    :cond_7
    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_3

    .line 952
    :cond_8
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingLeft()I

    move-result v0

    goto :goto_4

    .line 965
    :cond_9
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v3

    iget-object v11, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v11

    .line 966
    iget-boolean v11, p0, Lcom/android/launcher3/PagedView;->QB:Z

    if-eqz v11, :cond_4

    .line 967
    iget-object v11, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v11

    iget-object v12, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    sub-int/2addr v11, v12

    iget-object v12, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v11, v12

    sub-int/2addr v11, v10

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v0, v11

    goto :goto_6

    .line 978
    :cond_a
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingLeft()I

    move-result v0

    goto :goto_7

    .line 986
    :cond_b
    const/4 v0, 0x0

    goto :goto_8

    .line 993
    :cond_c
    if-eqz v0, :cond_14

    iget-boolean v0, v0, Lacl;->Rs:Z

    if-eqz v0, :cond_14

    .line 994
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingRight()I

    move-result v0

    goto :goto_9

    .line 1001
    :cond_d
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->gT:Z

    if-eqz v0, :cond_e

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-ltz v0, :cond_e

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_e

    .line 1002
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jh()V

    .line 1003
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->gT:Z

    .line 1006
    :cond_e
    if-lez v8, :cond_11

    .line 1007
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x0

    .line 1008
    :goto_b
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    .line 1013
    :goto_c
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qe:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_f

    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->Rm:Z

    if-nez v0, :cond_f

    .line 1015
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qd:I

    const/16 v1, -0x3e9

    if-eq v0, v1, :cond_12

    .line 1016
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qd:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bu(I)V

    .line 1017
    const/16 v0, -0x3e9

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qd:I

    .line 1022
    :cond_f
    :goto_d
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qe:I

    .line 1024
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->am(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1025
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->ja()V

    goto/16 :goto_0

    .line 1007
    :cond_10
    add-int/lit8 v0, v8, -0x1

    goto :goto_b

    .line 1010
    :cond_11
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    goto :goto_c

    .line 1019
    :cond_12
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bu(I)V

    goto :goto_d

    :cond_13
    move v0, v5

    goto/16 :goto_a

    :cond_14
    move v0, v4

    goto/16 :goto_9
.end method

.method protected onMeasure(II)V
    .locals 14

    .prologue
    .line 804
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QQ:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 805
    :cond_0
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 917
    :goto_0
    return-void

    .line 811
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 812
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 813
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 814
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 817
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 818
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 821
    const/high16 v1, 0x40000000    # 2.0f

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 822
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QZ:Z

    if-eqz v0, :cond_3

    .line 825
    int-to-float v0, v1

    iget v3, p0, Lcom/android/launcher3/PagedView;->ss:F

    div-float/2addr v0, v3

    float-to-int v0, v0

    .line 826
    int-to-float v1, v1

    iget v3, p0, Lcom/android/launcher3/PagedView;->ss:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    move v3, v1

    move v1, v0

    .line 831
    :goto_1
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 833
    if-eqz v5, :cond_2

    if-nez v6, :cond_4

    .line 834
    :cond_2
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    goto :goto_0

    :cond_3
    move v1, v2

    move v3, v4

    .line 829
    goto :goto_1

    .line 839
    :cond_4
    if-lez v2, :cond_5

    if-gtz v4, :cond_6

    .line 840
    :cond_5
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    goto :goto_0

    .line 849
    :cond_6
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingBottom()I

    move-result v2

    add-int v9, v0, v2

    .line 850
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getPaddingRight()I

    move-result v2

    add-int v10, v0, v2

    .line 852
    const/4 v4, 0x0

    .line 860
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v11

    .line 861
    const/4 v0, 0x0

    move v5, v0

    :goto_2
    if-ge v5, v11, :cond_a

    .line 863
    invoke-virtual {p0, v5}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v12

    .line 864
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_e

    .line 865
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lacl;

    .line 872
    iget-boolean v2, v0, Lacl;->Rs:Z

    if-nez v2, :cond_9

    .line 873
    iget v2, v0, Lacl;->width:I

    const/4 v6, -0x2

    if-ne v2, v6, :cond_7

    .line 874
    const/high16 v2, -0x80000000

    .line 879
    :goto_3
    iget v0, v0, Lacl;->height:I

    const/4 v6, -0x2

    if-ne v0, v6, :cond_8

    .line 880
    const/high16 v0, -0x80000000

    .line 885
    :goto_4
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v6, v10

    iget-object v7, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sub-int v7, v6, v7

    .line 887
    iget-object v6, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    sub-int/2addr v6, v9

    iget-object v8, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v8

    iget-object v8, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v8

    .line 889
    iput v6, p0, Lcom/android/launcher3/PagedView;->Qb:I

    move v8, v0

    move v13, v2

    move v2, v7

    move v7, v13

    .line 897
    :goto_5
    if-nez v4, :cond_d

    move v0, v2

    .line 901
    :goto_6
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 903
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 905
    invoke-virtual {v12, v2, v4}, Landroid/view/View;->measure(II)V

    .line 861
    :goto_7
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v4, v0

    goto :goto_2

    .line 876
    :cond_7
    const/high16 v2, 0x40000000    # 2.0f

    goto :goto_3

    .line 882
    :cond_8
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_4

    .line 891
    :cond_9
    const/high16 v6, 0x40000000    # 2.0f

    .line 892
    const/high16 v7, 0x40000000    # 2.0f

    .line 894
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v2, v0, v2

    .line 895
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    move v8, v7

    move v7, v6

    move v6, v0

    goto :goto_5

    .line 908
    :cond_a
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QG:Z

    if-eqz v0, :cond_c

    .line 909
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->yW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v2

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    .line 911
    if-ltz v0, :cond_b

    .line 912
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bw(I)V

    .line 914
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->QG:Z

    .line 916
    :cond_c
    invoke-virtual {p0, v1, v3}, Lcom/android/launcher3/PagedView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_d
    move v0, v4

    goto :goto_6

    :cond_e
    move v0, v4

    goto :goto_7
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 1248
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1249
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qf:I

    .line 1253
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    .line 1254
    if-eqz v0, :cond_1

    .line 1255
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 1257
    :goto_1
    return v0

    .line 1251
    :cond_0
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    goto :goto_0

    .line 1257
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 1724
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1727
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    .line 2012
    :cond_0
    :goto_0
    return v9

    .line 1729
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->j(Landroid/view/MotionEvent;)V

    .line 1731
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1733
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1739
    :pswitch_1
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1740
    invoke-direct {p0, v4}, Lcom/android/launcher3/PagedView;->ak(Z)V

    .line 1744
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qm:F

    .line 1745
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gJ:F

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qn:F

    .line 1746
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qo:F

    .line 1747
    iget v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    iget v1, p0, Lcom/android/launcher3/PagedView;->gJ:F

    invoke-direct {p0, p0, v0, v1}, Lcom/android/launcher3/PagedView;->c(Landroid/view/View;FF)[F

    move-result-object v0

    .line 1748
    aget v1, v0, v4

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qk:F

    .line 1749
    aget v0, v0, v9

    iput v0, p0, Lcom/android/launcher3/PagedView;->Ql:F

    .line 1750
    iput v2, p0, Lcom/android/launcher3/PagedView;->Qq:F

    .line 1751
    iput v2, p0, Lcom/android/launcher3/PagedView;->Qr:F

    .line 1752
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gM:I

    .line 1754
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-ne v0, v9, :cond_0

    .line 1755
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jn()V

    goto :goto_0

    .line 1760
    :pswitch_2
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-ne v0, v9, :cond_5

    .line 1762
    iget v0, p0, Lcom/android/launcher3/PagedView;->gM:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1764
    if-eq v0, v5, :cond_0

    .line 1766
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 1767
    iget v1, p0, Lcom/android/launcher3/PagedView;->gI:F

    iget v2, p0, Lcom/android/launcher3/PagedView;->Qq:F

    add-float/2addr v1, v2

    sub-float/2addr v1, v0

    .line 1769
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qr:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/launcher3/PagedView;->Qr:F

    .line 1774
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    .line 1775
    iget v2, p0, Lcom/android/launcher3/PagedView;->Qa:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/android/launcher3/PagedView;->Qa:F

    .line 1776
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    long-to-float v2, v2

    const v3, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/launcher3/PagedView;->PZ:F

    .line 1777
    iget-boolean v2, p0, Lcom/android/launcher3/PagedView;->QN:Z

    if-nez v2, :cond_3

    .line 1778
    float-to-int v2, v1

    invoke-virtual {p0, v2, v4}, Lcom/android/launcher3/PagedView;->scrollBy(II)V

    .line 1783
    :goto_1
    iput v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 1784
    float-to-int v0, v1

    int-to-float v0, v0

    sub-float v0, v1, v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qq:F

    goto/16 :goto_0

    .line 1781
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    goto :goto_1

    .line 1786
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->awakenScrollBars()Z

    goto/16 :goto_0

    .line 1788
    :cond_5
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-ne v0, v3, :cond_a

    .line 1790
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 1791
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gJ:F

    .line 1795
    iget v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    iget v1, p0, Lcom/android/launcher3/PagedView;->gJ:F

    invoke-direct {p0, p0, v0, v1}, Lcom/android/launcher3/PagedView;->c(Landroid/view/View;FF)[F

    move-result-object v0

    .line 1796
    aget v1, v0, v4

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qk:F

    .line 1797
    aget v0, v0, v9

    iput v0, p0, Lcom/android/launcher3/PagedView;->Ql:F

    .line 1798
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->ja()V

    .line 1801
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v6

    .line 1804
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qk:F

    float-to-int v0, v0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Ql:F

    float-to-int v0, v0

    .line 1806
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v7, v0

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->e([I)V

    const v2, 0x7fffffff

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v4

    :goto_2
    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v3, v3, v9

    if-gt v0, v3, :cond_8

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v8

    sub-int v3, v7, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v3, v2, :cond_6

    move v1, v0

    move v2, v3

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v1, v5

    .line 1814
    :cond_8
    if-ltz v1, :cond_9

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-eq v1, v0, :cond_9

    .line 1816
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aput v4, v0, v4

    .line 1817
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v9

    .line 1818
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->e([I)V

    .line 1819
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v4

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QE:[I

    aget v0, v0, v9

    if-gt v1, v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Rc:I

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822
    iput v1, p0, Lcom/android/launcher3/PagedView;->Rc:I

    .line 1823
    new-instance v0, Lach;

    invoke-direct {v0, p0, v1, v6}, Lach;-><init>(Lcom/android/launcher3/PagedView;II)V

    iput-object v0, p0, Lcom/android/launcher3/PagedView;->Rb:Ljava/lang/Runnable;

    .line 1871
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rb:Ljava/lang/Runnable;

    iget v1, p0, Lcom/android/launcher3/PagedView;->QY:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/launcher3/PagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1874
    :cond_9
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rb:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1875
    iput v5, p0, Lcom/android/launcher3/PagedView;->Rc:I

    goto/16 :goto_0

    .line 1878
    :cond_a
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->k(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 1883
    :pswitch_3
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-ne v0, v9, :cond_1d

    .line 1884
    iget v0, p0, Lcom/android/launcher3/PagedView;->gM:I

    .line 1885
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 1886
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 1887
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->fW:Landroid/view/VelocityTracker;

    .line 1888
    const/16 v3, 0x3e8

    iget v5, p0, Lcom/android/launcher3/PagedView;->gO:I

    int-to-float v5, v5

    invoke-virtual {v2, v3, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1889
    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v5, v0

    .line 1890
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qm:F

    sub-float v0, v1, v0

    float-to-int v3, v0

    .line 1891
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 1892
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v6, v2

    const v7, 0x3ecccccd    # 0.4f

    mul-float/2addr v6, v7

    cmpl-float v0, v0, v6

    if-lez v0, :cond_f

    move v0, v9

    .line 1895
    :goto_3
    iget v6, p0, Lcom/android/launcher3/PagedView;->Qr:F

    iget v7, p0, Lcom/android/launcher3/PagedView;->gI:F

    iget v8, p0, Lcom/android/launcher3/PagedView;->Qq:F

    add-float/2addr v7, v8

    sub-float v1, v7, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v1, v6

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qr:F

    .line 1897
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qr:F

    const/high16 v6, 0x41c80000    # 25.0f

    cmpl-float v1, v1, v6

    if-lez v1, :cond_10

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v6, p0, Lcom/android/launcher3/PagedView;->PV:I

    if-le v1, v6, :cond_10

    move v1, v9

    .line 1900
    :goto_4
    iget-boolean v6, p0, Lcom/android/launcher3/PagedView;->PS:Z

    if-nez v6, :cond_1b

    .line 1905
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    int-to-float v2, v2

    const v7, 0x3ea8f5c3    # 0.33f

    mul-float/2addr v2, v7

    cmpl-float v2, v6, v2

    if-lez v2, :cond_24

    int-to-float v2, v5

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    int-to-float v6, v3

    invoke-static {v6}, Ljava/lang/Math;->signum(F)F

    move-result v6

    cmpl-float v2, v2, v6

    if-eqz v2, :cond_24

    if-eqz v1, :cond_24

    move v2, v9

    .line 1914
    :goto_5
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v6

    .line 1915
    if-eqz v6, :cond_12

    if-lez v3, :cond_11

    move v3, v9

    .line 1916
    :goto_6
    if-eqz v6, :cond_14

    if-lez v5, :cond_b

    move v4, v9

    .line 1917
    :cond_b
    :goto_7
    if-eqz v0, :cond_c

    if-nez v3, :cond_c

    if-eqz v1, :cond_d

    :cond_c
    if-eqz v1, :cond_16

    if-nez v4, :cond_16

    :cond_d
    iget v6, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-lez v6, :cond_16

    .line 1919
    if-eqz v2, :cond_15

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    .line 1920
    :goto_8
    invoke-virtual {p0, v0, v5}, Lcom/android/launcher3/PagedView;->O(II)V

    .line 1994
    :cond_e
    :goto_9
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Rb:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1996
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jA()V

    goto/16 :goto_0

    :cond_f
    move v0, v4

    .line 1892
    goto :goto_3

    :cond_10
    move v1, v4

    .line 1897
    goto :goto_4

    :cond_11
    move v3, v4

    .line 1915
    goto :goto_6

    :cond_12
    if-gez v3, :cond_13

    move v3, v9

    goto :goto_6

    :cond_13
    move v3, v4

    goto :goto_6

    .line 1916
    :cond_14
    if-gez v5, :cond_b

    move v4, v9

    goto :goto_7

    .line 1919
    :cond_15
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 1921
    :cond_16
    if-eqz v0, :cond_17

    if-eqz v3, :cond_17

    if-eqz v1, :cond_18

    :cond_17
    if-eqz v1, :cond_1a

    if-eqz v4, :cond_1a

    :cond_18
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1a

    .line 1924
    if-eqz v2, :cond_19

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    .line 1925
    :goto_a
    invoke-virtual {p0, v0, v5}, Lcom/android/launcher3/PagedView;->O(II)V

    goto :goto_9

    .line 1924
    :cond_19
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1927
    :cond_1a
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jF()V

    goto :goto_9

    .line 1930
    :cond_1b
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1931
    invoke-direct {p0, v9}, Lcom/android/launcher3/PagedView;->ak(Z)V

    .line 1934
    :cond_1c
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScaleX()F

    move-result v0

    .line 1935
    neg-int v1, v5

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v3, v1

    .line 1936
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 1938
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    iget-object v2, p0, Lcom/android/launcher3/PagedView;->Qi:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v2}, Labj;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1939
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v2

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v7, v4

    move v8, v4

    invoke-virtual/range {v0 .. v8}, Labj;->fling(IIIIIIII)V

    .line 1941
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    goto :goto_9

    .line 1943
    :cond_1d
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1f

    .line 1947
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1948
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-eq v0, v1, :cond_1e

    .line 1949
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bB(I)V

    goto/16 :goto_9

    .line 1951
    :cond_1e
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jF()V

    goto/16 :goto_9

    .line 1953
    :cond_1f
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_21

    .line 1957
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1958
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-eq v0, v1, :cond_20

    .line 1959
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bB(I)V

    goto/16 :goto_9

    .line 1961
    :cond_20
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jF()V

    goto/16 :goto_9

    .line 1963
    :cond_21
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-ne v0, v3, :cond_22

    .line 1965
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 1966
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PagedView;->gJ:F

    .line 1970
    iget v0, p0, Lcom/android/launcher3/PagedView;->gI:F

    iget v1, p0, Lcom/android/launcher3/PagedView;->gJ:F

    invoke-direct {p0, p0, v0, v1}, Lcom/android/launcher3/PagedView;->c(Landroid/view/View;FF)[F

    move-result-object v0

    .line 1971
    aget v1, v0, v4

    iput v1, p0, Lcom/android/launcher3/PagedView;->Qk:F

    .line 1972
    aget v0, v0, v9

    iput v0, p0, Lcom/android/launcher3/PagedView;->Ql:F

    .line 1973
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->ja()V

    .line 1974
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qk:F

    float-to-int v0, v0

    iget v0, p0, Lcom/android/launcher3/PagedView;->Ql:F

    float-to-int v0, v0

    goto/16 :goto_9

    .line 1988
    :cond_22
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->Qt:Z

    if-nez v0, :cond_e

    .line 1989
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/Launcher;->onClick(Landroid/view/View;)V

    goto/16 :goto_9

    .line 2000
    :pswitch_4
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qv:I

    if-ne v0, v9, :cond_23

    .line 2001
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jF()V

    .line 2003
    :cond_23
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jA()V

    goto/16 :goto_0

    .line 2007
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/android/launcher3/PagedView;->d(Landroid/view/MotionEvent;)V

    .line 2008
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->jB()V

    goto/16 :goto_0

    :cond_24
    move v2, v4

    goto/16 :goto_5

    .line 1733
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2859
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2876
    :goto_0
    return v0

    .line 2862
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 2876
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2864
    :sswitch_0
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 2865
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->fV()V

    goto :goto_0

    .line 2870
    :sswitch_1
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-lez v1, :cond_1

    .line 2871
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->fU()V

    goto :goto_0

    .line 2862
    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public performLongClick()Z
    .locals 1

    .prologue
    .line 2319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PagedView;->Qt:Z

    .line 2320
    invoke-super {p0}, Landroid/view/ViewGroup;->performLongClick()Z

    move-result v0

    return v0
.end method

.method public removeAllViewsInLayout()V
    .locals 2

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_0

    .line 1117
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    iget-boolean v1, p0, Lcom/android/launcher3/PagedView;->QU:Z

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PageIndicator;->ah(Z)V

    .line 1120
    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 1121
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1095
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->bx(I)V

    .line 1096
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1097
    return-void
.end method

.method public removeViewAt(I)V
    .locals 0

    .prologue
    .line 1109
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->removeViewAt(I)V

    .line 1110
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1111
    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1102
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher3/PagedView;->bx(I)V

    .line 1103
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 1104
    return-void
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2099
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 2100
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aT(I)I

    move-result v0

    .line 2101
    if-ltz v0, :cond_0

    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2102
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 2104
    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2

    .prologue
    .line 1237
    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aT(I)I

    move-result v0

    .line 1238
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qc:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher3/PagedView;->Qh:Labj;

    invoke-virtual {v1}, Labj;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1239
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 1240
    const/4 v0, 0x1

    .line 1242
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 1327
    if-eqz p1, :cond_0

    .line 1330
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    .line 1331
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 1333
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1334
    return-void
.end method

.method public scrollBy(II)V
    .locals 2

    .prologue
    .line 647
    iget v0, p0, Lcom/android/launcher3/PagedView;->QD:I

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/PagedView;->scrollTo(II)V

    .line 648
    return-void
.end method

.method public scrollTo(II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 653
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->PS:Z

    if-eqz v0, :cond_0

    .line 654
    iget v0, p0, Lcom/android/launcher3/PagedView;->PU:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 655
    iget v3, p0, Lcom/android/launcher3/PagedView;->PT:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 658
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v4

    .line 659
    iput p1, p0, Lcom/android/launcher3/PagedView;->QD:I

    .line 661
    if-eqz v4, :cond_4

    iget v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    if-le p1, v0, :cond_3

    move v3, v1

    .line 662
    :goto_0
    if-eqz v4, :cond_7

    if-gez p1, :cond_6

    move v0, v1

    .line 663
    :goto_1
    if-eqz v3, :cond_a

    .line 664
    invoke-super {p0, v2, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 665
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QC:Z

    if-eqz v0, :cond_1

    .line 666
    if-eqz v4, :cond_9

    .line 667
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->h(F)V

    .line 686
    :cond_1
    :goto_2
    int-to-float v0, p1

    iput v0, p0, Lcom/android/launcher3/PagedView;->Qa:F

    .line 687
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    long-to-float v0, v4

    const v3, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v0, v3

    iput v0, p0, Lcom/android/launcher3/PagedView;->PZ:F

    .line 690
    invoke-direct {p0, v1}, Lcom/android/launcher3/PagedView;->am(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 691
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qk:F

    iget v3, p0, Lcom/android/launcher3/PagedView;->Ql:F

    invoke-direct {p0, p0, v0, v3}, Lcom/android/launcher3/PagedView;->d(Landroid/view/View;FF)[F

    move-result-object v0

    .line 692
    aget v2, v0, v2

    iput v2, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 693
    aget v0, v0, v1

    iput v0, p0, Lcom/android/launcher3/PagedView;->gJ:F

    .line 694
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->ja()V

    .line 696
    :cond_2
    return-void

    :cond_3
    move v3, v2

    .line 661
    goto :goto_0

    :cond_4
    if-gez p1, :cond_5

    move v3, v1

    goto :goto_0

    :cond_5
    move v3, v2

    goto :goto_0

    :cond_6
    move v0, v2

    .line 662
    goto :goto_1

    :cond_7
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    if-le p1, v0, :cond_8

    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_1

    .line 669
    :cond_9
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->h(F)V

    goto :goto_2

    .line 672
    :cond_a
    if-eqz v0, :cond_c

    .line 673
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    invoke-super {p0, v0, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 674
    iget-boolean v0, p0, Lcom/android/launcher3/PagedView;->QC:Z

    if-eqz v0, :cond_1

    .line 675
    if-eqz v4, :cond_b

    .line 676
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->h(F)V

    goto :goto_2

    .line 678
    :cond_b
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qg:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->h(F)V

    goto :goto_2

    .line 682
    :cond_c
    iput p1, p0, Lcom/android/launcher3/PagedView;->QH:I

    .line 683
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_2
.end method

.method public sendAccessibilityEvent(I)V
    .locals 1

    .prologue
    .line 2846
    const/16 v0, 0x1000

    if-eq p1, v0, :cond_0

    .line 2847
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 2849
    :cond_0
    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 3

    .prologue
    .line 637
    iput-object p1, p0, Lcom/android/launcher3/PagedView;->Qx:Landroid/view/View$OnLongClickListener;

    .line 638
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v1

    .line 639
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 640
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 639
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 642
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 643
    return-void
.end method

.method public setScaleX(F)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 407
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 408
    invoke-direct {p0, v2}, Lcom/android/launcher3/PagedView;->am(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget v0, p0, Lcom/android/launcher3/PagedView;->Qk:F

    iget v1, p0, Lcom/android/launcher3/PagedView;->Ql:F

    invoke-direct {p0, p0, v0, v1}, Lcom/android/launcher3/PagedView;->d(Landroid/view/View;FF)[F

    move-result-object v0

    .line 410
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/android/launcher3/PagedView;->gI:F

    .line 411
    aget v0, v0, v2

    iput v0, p0, Lcom/android/launcher3/PagedView;->gJ:F

    .line 412
    invoke-direct {p0}, Lcom/android/launcher3/PagedView;->ja()V

    .line 414
    :cond_0
    return-void
.end method

.method protected final u(F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1623
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 1625
    int-to-float v0, v1

    div-float v0, p1, v0

    .line 1627
    cmpl-float v2, v0, v5

    if-nez v2, :cond_0

    .line 1644
    :goto_0
    return-void

    .line 1628
    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v2, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float/2addr v0, v4

    mul-float v3, v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v4

    mul-float/2addr v0, v2

    .line 1631
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_1

    .line 1632
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v0, v2

    .line 1635
    :cond_1
    const v2, 0x3d8f5c29    # 0.07f

    mul-float/2addr v0, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1636
    cmpg-float v1, p1, v5

    if-gez v1, :cond_2

    .line 1637
    iput v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    .line 1638
    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 1643
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->invalidate()V

    goto :goto_0

    .line 1640
    :cond_2
    iget v1, p0, Lcom/android/launcher3/PagedView;->Qg:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    .line 1641
    iget v0, p0, Lcom/android/launcher3/PagedView;->QH:I

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getScrollY()I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_1
.end method
