.class public Lcom/android/launcher3/HolographicImageView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field private final IC:Lwg;

.field private ID:Z

.field private IE:Z

.field private IF:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/HolographicImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/HolographicImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Lwg;

    invoke-direct {v0, p1}, Lwg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/HolographicImageView;->IC:Lwg;

    .line 48
    sget-object v0, Ladb;->Sv:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/launcher3/HolographicImageView;->ID:Z

    .line 51
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    new-instance v0, Lwb;

    invoke-direct {v0, p0}, Lwb;-><init>(Lcom/android/launcher3/HolographicImageView;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/HolographicImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 64
    new-instance v0, Lwc;

    invoke-direct {v0, p0}, Lwc;-><init>(Lcom/android/launcher3/HolographicImageView;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/HolographicImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 73
    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/HolographicImageView;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/android/launcher3/HolographicImageView;->IE:Z

    return v0
.end method

.method public static synthetic a(Lcom/android/launcher3/HolographicImageView;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/android/launcher3/HolographicImageView;->IE:Z

    return p1
.end method

.method public static synthetic b(Lcom/android/launcher3/HolographicImageView;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/android/launcher3/HolographicImageView;->IF:Z

    return v0
.end method

.method public static synthetic b(Lcom/android/launcher3/HolographicImageView;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/android/launcher3/HolographicImageView;->IF:Z

    return p1
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 83
    iget-object v0, p0, Lcom/android/launcher3/HolographicImageView;->IC:Lwg;

    invoke-virtual {v0, p0}, Lwg;->a(Landroid/widget/ImageView;)V

    .line 84
    invoke-virtual {p0}, Lcom/android/launcher3/HolographicImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 85
    instance-of v1, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v1, :cond_0

    .line 86
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    .line 87
    invoke-virtual {p0}, Lcom/android/launcher3/HolographicImageView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 88
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->invalidateSelf()V

    .line 90
    :cond_0
    return-void
.end method

.method final gV()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/launcher3/HolographicImageView;->IC:Lwg;

    invoke-virtual {v0, p0}, Lwg;->b(Landroid/widget/ImageView;)V

    .line 77
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 4

    .prologue
    .line 115
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/ImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 116
    iget-boolean v1, p0, Lcom/android/launcher3/HolographicImageView;->ID:Z

    if-eqz v1, :cond_0

    .line 117
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f01005d

    aput v3, v1, v2

    invoke-static {v0, v1}, Lcom/android/launcher3/HolographicImageView;->mergeDrawableStates([I[I)[I

    .line 119
    :cond_0
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 98
    iget-object v0, p0, Lcom/android/launcher3/HolographicImageView;->IC:Lwg;

    invoke-virtual {v0, p0}, Lwg;->a(Landroid/widget/ImageView;)V

    .line 99
    return-void
.end method
