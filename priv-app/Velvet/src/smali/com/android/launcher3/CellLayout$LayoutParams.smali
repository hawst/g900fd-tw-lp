.class public Lcom/android/launcher3/CellLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "PG"


# instance fields
.field public Bb:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public Bc:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public Bn:I

.field public Bo:I

.field public Bp:Z

.field public Bq:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public Br:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public Bs:Z

.field public Bt:Z

.field public Bu:Z

.field public Bv:Z

.field public x:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public y:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIII)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 3169
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 3126
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 3131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bt:Z

    .line 3137
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    .line 3170
    iput p1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    .line 3171
    iput p2, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    .line 3172
    iput p3, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 3173
    iput p4, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 3174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3149
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3126
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 3131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bt:Z

    .line 3137
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    .line 3150
    iput v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 3151
    iput v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 3152
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3155
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3126
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 3131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bt:Z

    .line 3137
    iput-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    .line 3156
    iput v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 3157
    iput v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 3158
    return-void
.end method


# virtual methods
.method public final a(IIIIZI)V
    .locals 5

    .prologue
    .line 3178
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    if-eqz v0, :cond_1

    .line 3179
    iget v2, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 3180
    iget v3, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 3181
    iget-boolean v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    .line 3182
    :goto_0
    iget-boolean v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    .line 3184
    :goto_1
    if-eqz p5, :cond_0

    .line 3185
    sub-int v0, p6, v0

    iget v4, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    sub-int/2addr v0, v4

    .line 3188
    :cond_0
    mul-int v4, v2, p1

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v2, p3

    add-int/2addr v2, v4

    iget v4, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v4

    iget v4, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    .line 3190
    mul-int v2, v3, p2

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, p4

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->topMargin:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->bottomMargin:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    .line 3192
    add-int v2, p1, p3

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 3193
    add-int v0, p2, p4

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 3195
    :cond_1
    return-void

    .line 3181
    :cond_2
    iget v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    goto :goto_0

    .line 3182
    :cond_3
    iget v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    goto :goto_1
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 3214
    iget v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 3206
    iget v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 3222
    iget v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 3230
    iget v0, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    return v0
.end method

.method public setHeight(I)V
    .locals 0

    .prologue
    .line 3210
    iput p1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    .line 3211
    return-void
.end method

.method public setWidth(I)V
    .locals 0

    .prologue
    .line 3202
    iput p1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    .line 3203
    return-void
.end method

.method public setX(I)V
    .locals 0

    .prologue
    .line 3218
    iput p1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 3219
    return-void
.end method

.method public setY(I)V
    .locals 0

    .prologue
    .line 3226
    iput p1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 3227
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3198
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
