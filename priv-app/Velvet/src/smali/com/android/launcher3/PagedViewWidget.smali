.class public Lcom/android/launcher3/PagedViewWidget;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field public static RD:Lcom/android/launcher3/PagedViewWidget;

.field private static Rx:Z

.field private static Ry:Z


# instance fields
.field private RA:Lacs;

.field public RB:Lact;

.field public RC:Z

.field private RE:Z

.field private final RF:Landroid/graphics/Rect;

.field private Rz:Ljava/lang/String;

.field private hr:Ljava/lang/Object;

.field private yD:Lafb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 41
    sput-boolean v0, Lcom/android/launcher3/PagedViewWidget;->Rx:Z

    .line 42
    sput-boolean v0, Lcom/android/launcher3/PagedViewWidget;->Ry:Z

    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher3/PagedViewWidget;->RD:Lcom/android/launcher3/PagedViewWidget;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/PagedViewWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/PagedViewWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RA:Lacs;

    .line 46
    iput-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RB:Lact;

    .line 47
    iput-boolean v2, p0, Lcom/android/launcher3/PagedViewWidget;->RC:Z

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    const v1, 0x7f0a008c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->Rz:Ljava/lang/String;

    .line 68
    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedViewWidget;->setWillNotDraw(Z)V

    .line 69
    invoke-virtual {p0, v2}, Lcom/android/launcher3/PagedViewWidget;->setClipToPadding(Z)V

    .line 70
    return-void
.end method

.method private jO()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RA:Lacs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RA:Lacs;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 223
    :cond_0
    iget-boolean v0, p0, Lcom/android/launcher3/PagedViewWidget;->RC:Z

    if-eqz v0, :cond_2

    .line 224
    iget-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RB:Lact;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RB:Lact;

    invoke-interface {v0}, Lact;->ej()V

    .line 227
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/PagedViewWidget;->RC:Z

    .line 229
    :cond_2
    return-void
.end method

.method static jP()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher3/PagedViewWidget;->RD:Lcom/android/launcher3/PagedViewWidget;

    .line 233
    return-void
.end method


# virtual methods
.method final a(Lact;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/launcher3/PagedViewWidget;->RB:Lact;

    .line 186
    return-void
.end method

.method public final a(Landroid/appwidget/AppWidgetProviderInfo;I[ILafb;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 122
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 123
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v1

    .line 125
    iput-boolean v5, p0, Lcom/android/launcher3/PagedViewWidget;->RE:Z

    .line 126
    iput-object p1, p0, Lcom/android/launcher3/PagedViewWidget;->hr:Ljava/lang/Object;

    .line 127
    const v0, 0x7f1100a5

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    .line 128
    const v0, 0x7f1100a7

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 132
    invoke-virtual {p0}, Lcom/android/launcher3/PagedViewWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lahh;->A(Landroid/content/Context;)Lahh;

    move-result-object v2

    invoke-virtual {v2, p1}, Lahh;->b(Landroid/appwidget/AppWidgetProviderInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    const v0, 0x7f1100a8

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 134
    if-eqz v0, :cond_0

    .line 135
    aget v2, p3, v6

    iget v3, v1, Ltu;->Dh:F

    float-to-int v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 136
    aget v3, p3, v5

    iget v1, v1, Ltu;->Dg:F

    float-to-int v1, v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 137
    iget-object v3, p0, Lcom/android/launcher3/PagedViewWidget;->Rz:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :cond_0
    iput-object p4, p0, Lcom/android/launcher3/PagedViewWidget;->yD:Lafb;

    .line 140
    return-void
.end method

.method public final a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lafb;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 144
    iput-boolean v5, p0, Lcom/android/launcher3/PagedViewWidget;->RE:Z

    .line 145
    iput-object p2, p0, Lcom/android/launcher3/PagedViewWidget;->hr:Ljava/lang/Object;

    .line 146
    invoke-virtual {p2, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 147
    const v0, 0x7f1100a7

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    const v0, 0x7f1100a8

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 150
    if-eqz v0, :cond_0

    .line 151
    iget-object v1, p0, Lcom/android/launcher3/PagedViewWidget;->Rz:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    :cond_0
    iput-object p3, p0, Lcom/android/launcher3/PagedViewWidget;->yD:Lafb;

    .line 154
    return-void
.end method

.method final a(Lus;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 165
    const v0, 0x7f1100a5

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PagedViewWidgetImageView;

    .line 167
    if-eqz p1, :cond_1

    .line 168
    iput-boolean v2, v0, Lcom/android/launcher3/PagedViewWidgetImageView;->RH:Z

    .line 169
    invoke-virtual {v0, p1}, Lcom/android/launcher3/PagedViewWidgetImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    iget-boolean v1, p0, Lcom/android/launcher3/PagedViewWidget;->RE:Z

    if-eqz v1, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/android/launcher3/PagedViewWidget;->jN()[I

    move-result-object v1

    .line 173
    aget v1, v1, v2

    invoke-virtual {p1}, Lus;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 174
    iget-object v2, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher3/PagedViewWidgetImageView;->setPadding(IIII)V

    .line 179
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PagedViewWidgetImageView;->setAlpha(F)V

    .line 180
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher3/PagedViewWidgetImageView;->RH:Z

    .line 182
    :cond_1
    return-void
.end method

.method public final jN()[I
    .locals 5

    .prologue
    .line 157
    const v0, 0x7f1100a5

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 158
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 159
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    aput v3, v1, v2

    .line 160
    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v3

    aput v0, v1, v2

    .line 161
    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .prologue
    .line 105
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 107
    sget-boolean v0, Lcom/android/launcher3/PagedViewWidget;->Rx:Z

    if-eqz v0, :cond_1

    .line 108
    const v0, 0x7f1100a5

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 109
    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Lus;

    .line 111
    sget-boolean v2, Lcom/android/launcher3/PagedViewWidget;->Ry:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/launcher3/PagedViewWidget;->hr:Ljava/lang/Object;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lus;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/android/launcher3/PagedViewWidget;->yD:Lafb;

    iget-object v3, p0, Lcom/android/launcher3/PagedViewWidget;->hr:Ljava/lang/Object;

    invoke-virtual {v1}, Lus;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lafb;->a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 115
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 118
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 76
    const v0, 0x7f1100a5

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 77
    iget-object v1, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 78
    iget-object v1, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 79
    iget-object v1, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 80
    iget-object v1, p0, Lcom/android/launcher3/PagedViewWidget;->RF:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 83
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 84
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v1

    .line 85
    const v0, 0x7f1100a7

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    if-eqz v0, :cond_0

    .line 87
    iget v2, v1, Ltu;->DJ:I

    int-to-float v2, v2

    invoke-virtual {v0, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 89
    :cond_0
    const v0, 0x7f1100a8

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 90
    if-eqz v0, :cond_1

    .line 91
    iget v1, v1, Ltu;->DJ:I

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 93
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 237
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 239
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 248
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 241
    :pswitch_1
    invoke-direct {p0}, Lcom/android/launcher3/PagedViewWidget;->jO()V

    goto :goto_0

    .line 244
    :pswitch_2
    sget-object v0, Lcom/android/launcher3/PagedViewWidget;->RD:Lcom/android/launcher3/PagedViewWidget;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RA:Lacs;

    if-nez v0, :cond_1

    new-instance v0, Lacs;

    invoke-direct {v0, p0}, Lacs;-><init>(Lcom/android/launcher3/PagedViewWidget;)V

    iput-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RA:Lacs;

    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/PagedViewWidget;->RA:Lacs;

    const-wide/16 v2, 0x78

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/launcher3/PagedViewWidget;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 247
    :pswitch_3
    invoke-direct {p0}, Lcom/android/launcher3/PagedViewWidget;->jO()V

    goto :goto_0

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
