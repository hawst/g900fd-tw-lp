.class public Lcom/android/launcher3/AppsCustomizeTabHost;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Labp;
.implements Lwm;


# instance fields
.field private yA:Z

.field private yU:Lcom/android/launcher3/AppsCustomizePagedView;

.field private yV:Landroid/view/View;

.field private final yW:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yA:Z

    .line 51
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yW:Landroid/graphics/Rect;

    .line 55
    return-void
.end method

.method private aX(I)V
    .locals 7

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizeTabHost;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 218
    if-nez v0, :cond_1

    .line 223
    :cond_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizeTabHost;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hq()Landroid/view/ViewGroup;

    move-result-object v2

    .line 221
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 222
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizeTabHost;->isChildrenDrawingOrderEnabled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 223
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 224
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 225
    if-eq v4, p0, :cond_0

    .line 226
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_2

    if-eq v4, v2, :cond_2

    .line 229
    invoke-virtual {v4, p1}, Landroid/view/View;->setVisibility(I)V

    .line 223
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed; can\'t get z-order of views"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(Lse;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lse;->yR:Lse;

    if-ne p0, v0, :cond_0

    .line 112
    const-string v0, "APPS"

    .line 116
    :goto_0
    return-object v0

    .line 113
    :cond_0
    sget-object v0, Lse;->yS:Lse;

    if-ne p0, v0, :cond_1

    .line 114
    const-string v0, "WIDGETS"

    goto :goto_0

    .line 116
    :cond_1
    const-string v0, "APPS"

    goto :goto_0
.end method

.method public static q(Ljava/lang/String;)Lse;
    .locals 1

    .prologue
    .line 99
    const-string v0, "APPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lse;->yR:Lse;

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_0
    const-string v0, "WIDGETS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    sget-object v0, Lse;->yS:Lse;

    goto :goto_0

    .line 104
    :cond_1
    sget-object v0, Lse;->yR:Lse;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/android/launcher3/Launcher;F)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/Launcher;F)V

    .line 189
    return-void
.end method

.method public final a(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 164
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/Launcher;ZZ)V

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yA:Z

    .line 167
    if-eqz p3, :cond_0

    .line 169
    invoke-direct {p0, v1}, Lcom/android/launcher3/AppsCustomizeTabHost;->aX(I)V

    .line 179
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yV:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->bC(I)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yW:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 74
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yV:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 75
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 76
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 77
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 78
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 79
    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yV:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    return-void
.end method

.method public final b(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher3/AppsCustomizePagedView;->b(Lcom/android/launcher3/Launcher;ZZ)V

    .line 184
    return-void
.end method

.method final b(Lse;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lse;)V

    .line 65
    return-void
.end method

.method public final c(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher3/AppsCustomizePagedView;->c(Lcom/android/launcher3/Launcher;ZZ)V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yA:Z

    .line 196
    if-nez p3, :cond_1

    .line 199
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->bC(I)V

    .line 202
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizeTabHost;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 204
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->ex()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 212
    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->aX(I)V

    .line 214
    :cond_1
    return-void
.end method

.method public final eA()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yV:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->ep()V

    .line 150
    return-void
.end method

.method public final eB()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    return-object v0
.end method

.method public final eC()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yA:Z

    return v0
.end method

.method public final ey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->er()Lse;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->c(Lse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ez()V
    .locals 3

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizeTabHost;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yV:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->j(IZ)V

    .line 141
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->jf()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->bC(I)V

    .line 143
    :cond_0
    return-void
.end method

.method public getDescendantFocusability()I
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/android/launcher3/AppsCustomizeTabHost;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const/high16 v0, 0x60000

    .line 127
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->getDescendantFocusability()I

    move-result v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 87
    const v0, 0x7f1100a1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/AppsCustomizePagedView;

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    .line 88
    const v0, 0x7f11009e

    invoke-virtual {p0, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yV:Landroid/view/View;

    .line 89
    return-void
.end method

.method final reset()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/launcher3/AppsCustomizeTabHost;->yU:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->reset()V

    .line 133
    return-void
.end method
