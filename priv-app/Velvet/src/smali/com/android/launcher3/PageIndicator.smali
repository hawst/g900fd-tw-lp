.class public Lcom/android/launcher3/PageIndicator;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private PK:[I

.field private PL:I

.field private PM:Ljava/util/ArrayList;

.field private PN:I

.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    .line 66
    sget-object v0, Ladb;->Sw:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    const/16 v1, 0xf

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PageIndicator;->PL:I

    .line 69
    iget-object v1, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    aput v3, v1, v3

    .line 70
    iget-object v1, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    const/4 v2, 0x1

    aput v3, v1, v2

    .line 71
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/PageIndicator;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    invoke-virtual {p0}, Lcom/android/launcher3/PageIndicator;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 76
    const-wide/16 v2, 0xaf

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 77
    return-void
.end method

.method private h(IZ)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 96
    if-gez p1, :cond_0

    .line 97
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/launcher3/PageIndicator;->PL:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 100
    div-int/lit8 v1, v0, 0x2

    .line 101
    sub-int v1, p1, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 103
    iget-object v3, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v5, p0, Lcom/android/launcher3/PageIndicator;->PL:I

    add-int/2addr v1, v5

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 104
    iget-object v1, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v5, v0

    .line 105
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 108
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    aget v0, v0, v2

    if-ne v0, v6, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    aget v0, v0, v4

    if-eq v0, v5, :cond_5

    :cond_1
    move v3, v4

    .line 111
    :goto_0
    if-nez p2, :cond_2

    .line 112
    invoke-virtual {p0}, Lcom/android/launcher3/PageIndicator;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    invoke-virtual {v0, v9}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/PageIndicator;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_6

    .line 117
    invoke-virtual {p0, v1}, Lcom/android/launcher3/PageIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PageIndicatorMarker;

    .line 118
    iget-object v7, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 119
    if-lt v7, v6, :cond_3

    if-lt v7, v5, :cond_4

    .line 120
    :cond_3
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PageIndicator;->removeView(Landroid/view/View;)V

    .line 116
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_5
    move v3, v2

    .line 108
    goto :goto_0

    :cond_6
    move v1, v2

    .line 125
    :goto_2
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 126
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PageIndicatorMarker;

    .line 127
    if-gt v6, v1, :cond_9

    if-ge v1, v5, :cond_9

    .line 128
    invoke-virtual {p0, v0}, Lcom/android/launcher3/PageIndicator;->indexOfChild(Landroid/view/View;)I

    move-result v7

    if-gez v7, :cond_7

    .line 129
    sub-int v7, v1, v6

    invoke-virtual {p0, v0, v7}, Lcom/android/launcher3/PageIndicator;->addView(Landroid/view/View;I)V

    .line 131
    :cond_7
    if-ne v1, p1, :cond_8

    .line 132
    invoke-virtual {v0, v3}, Lcom/android/launcher3/PageIndicatorMarker;->ai(Z)V

    .line 125
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 134
    :cond_8
    invoke-virtual {v0, v3}, Lcom/android/launcher3/PageIndicatorMarker;->aj(Z)V

    goto :goto_3

    .line 137
    :cond_9
    invoke-virtual {v0, v4}, Lcom/android/launcher3/PageIndicatorMarker;->aj(Z)V

    goto :goto_3

    .line 154
    :cond_a
    if-nez p2, :cond_b

    .line 155
    invoke-virtual {p0}, Lcom/android/launcher3/PageIndicator;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {v0, v9}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 158
    :cond_b
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    aput v6, v0, v2

    .line 159
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PK:[I

    aput v5, v0, v4

    .line 160
    return-void
.end method


# virtual methods
.method final a(ILacg;)V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PageIndicatorMarker;

    .line 181
    iget v1, p2, Lacg;->PO:I

    iget v2, p2, Lacg;->PP:I

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/PageIndicatorMarker;->M(II)V

    .line 182
    return-void
.end method

.method final a(ILacg;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 163
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 165
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0400e5

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PageIndicatorMarker;

    .line 168
    iget v2, p2, Lacg;->PO:I

    iget v3, p2, Lacg;->PP:I

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher3/PageIndicatorMarker;->M(II)V

    .line 170
    iget-object v2, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 171
    iget v0, p0, Lcom/android/launcher3/PageIndicator;->PN:I

    invoke-direct {p0, v0, p3}, Lcom/android/launcher3/PageIndicator;->h(IZ)V

    .line 172
    return-void
.end method

.method final a(Ljava/util/ArrayList;Z)V
    .locals 3

    .prologue
    .line 174
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 175
    const v2, 0x7fffffff

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacg;

    invoke-virtual {p0, v2, v0, p2}, Lcom/android/launcher3/PageIndicator;->a(ILacg;Z)V

    .line 174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method final ah(Z)V
    .locals 1

    .prologue
    .line 192
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 193
    const v0, 0x7fffffff

    invoke-virtual {p0, v0, p1}, Lcom/android/launcher3/PageIndicator;->i(IZ)V

    goto :goto_0

    .line 195
    :cond_0
    return-void
.end method

.method public final br(I)V
    .locals 1

    .prologue
    .line 199
    iput p1, p0, Lcom/android/launcher3/PageIndicator;->PN:I

    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/PageIndicator;->h(IZ)V

    .line 201
    return-void
.end method

.method final i(IZ)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 186
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 187
    iget-object v1, p0, Lcom/android/launcher3/PageIndicator;->PM:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 188
    iget v0, p0, Lcom/android/launcher3/PageIndicator;->PN:I

    invoke-direct {p0, v0, p2}, Lcom/android/launcher3/PageIndicator;->h(IZ)V

    .line 190
    :cond_0
    return-void
.end method
