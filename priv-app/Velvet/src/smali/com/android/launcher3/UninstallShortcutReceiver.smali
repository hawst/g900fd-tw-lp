.class public Lcom/android/launcher3/UninstallShortcutReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field private static Tn:Ljava/util/ArrayList;

.field private static To:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher3/UninstallShortcutReceiver;->Tn:Ljava/util/ArrayList;

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher3/UninstallShortcutReceiver;->To:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 43
    return-void
.end method

.method private static a(Landroid/content/Context;Lado;)V
    .locals 16

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 79
    move-object/from16 v0, p1

    iget-object v3, v0, Lado;->Jp:Landroid/content/Intent;

    .line 81
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lyu;->n(Landroid/content/Context;)V

    .line 82
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v11

    .line 83
    monitor-enter v11

    .line 84
    :try_start_0
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/content/Intent;

    move-object v8, v0

    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v2, "duplicate"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v8, :cond_1

    if-eqz v12, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Labn;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "intent"

    aput-object v6, v4, v5

    const-string v5, "title=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v12, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    const-string v3, "intent"

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    const-string v3, "_id"

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v6

    move v3, v10

    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-eqz v7, :cond_3

    :try_start_2
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    invoke-static {v7, v10}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const/4 v7, 0x0

    invoke-static {v14, v15, v7}, Labn;->a(JZ)Landroid/net/Uri;

    move-result-object v7

    const/4 v10, 0x0

    const/4 v14, 0x0

    invoke-virtual {v2, v7, v10, v14}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v13, :cond_2

    :goto_1
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    if-eqz v9, :cond_1

    sget-object v3, Labn;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const v2, 0x7f0a009d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v12, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 85
    :cond_1
    monitor-exit v11

    return-void

    .line 84
    :catchall_0
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 85
    :catchall_1
    move-exception v2

    monitor-exit v11

    throw v2

    .line 84
    :catch_0
    move-exception v7

    goto :goto_0

    :cond_2
    move v3, v9

    goto :goto_0

    :cond_3
    move v9, v3

    goto :goto_1
.end method

.method static kl()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher3/UninstallShortcutReceiver;->To:Z

    .line 66
    return-void
.end method

.method static x(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher3/UninstallShortcutReceiver;->To:Z

    .line 70
    sget-object v0, Lcom/android/launcher3/UninstallShortcutReceiver;->Tn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 71
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lado;

    invoke-static {p0, v0}, Lcom/android/launcher3/UninstallShortcutReceiver;->a(Landroid/content/Context;Lado;)V

    .line 73
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 52
    const-string v0, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v0, Lado;

    invoke-direct {v0, p2}, Lado;-><init>(Landroid/content/Intent;)V

    .line 57
    sget-boolean v1, Lcom/android/launcher3/UninstallShortcutReceiver;->To:Z

    if-eqz v1, :cond_1

    .line 58
    sget-object v1, Lcom/android/launcher3/UninstallShortcutReceiver;->Tn:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_1
    invoke-static {p1, v0}, Lcom/android/launcher3/UninstallShortcutReceiver;->a(Landroid/content/Context;Lado;)V

    goto :goto_0
.end method
