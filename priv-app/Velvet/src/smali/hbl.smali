.class final Lhbl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field private dbD:I

.field private dbE:Ljava/util/LinkedList;

.field private dbF:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584
    const/16 v0, 0x28

    iput v0, p0, Lhbl;->dbD:I

    .line 585
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhbl;->dbE:Ljava/util/LinkedList;

    .line 586
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbl;->dbF:Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized add(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 589
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhbl;->dbE:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 590
    iget-object v0, p0, Lhbl;->dbE:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lhbl;->dbD:I

    if-le v0, v1, :cond_0

    .line 591
    iget-object v0, p0, Lhbl;->dbE:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbl;->dbF:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 594
    :cond_0
    monitor-exit p0

    return-void

    .line 589
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 4

    .prologue
    .line 598
    iget-boolean v0, p0, Lhbl;->dbF:Z

    if-eqz v0, :cond_0

    .line 599
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[History log trimmed to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lhbl;->dbD:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " elements]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Likr;->j([Ljava/lang/Object;)Lirv;

    move-result-object v0

    .line 601
    iget-object v1, p0, Lhbl;->dbE:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, Likr;->b(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    .line 603
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhbl;->dbE:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method
