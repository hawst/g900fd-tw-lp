.class public final Lhlb;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lhle;


# instance fields
.field private bWJ:Landroid/view/View;

.field private djA:Landroid/view/View;

.field private djB:Landroid/widget/TextView;

.field private djC:Landroid/widget/TextView;

.field private djD:Landroid/widget/TextView;

.field private djE:Landroid/view/View;

.field private djF:Landroid/widget/TextView;

.field private djG:Landroid/widget/TextView;

.field private djH:Landroid/widget/TextView;

.field djI:Landroid/graphics/drawable/Drawable;

.field private djJ:Landroid/view/View;

.field private final djt:Landroid/view/View;

.field private dju:Landroid/widget/TextView;

.field private djv:Landroid/widget/TextView;

.field djw:Lcom/google/android/search/shared/ui/WebImageView;

.field private djx:Lcom/google/android/search/shared/ui/WebImageView;

.field private djy:Landroid/widget/TextView;

.field private djz:Landroid/view/View;

.field public mImageLoader:Lesm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 61
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 64
    const v1, 0x7f04009f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhlb;->djt:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f1101fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->dju:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f1101ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djv:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f11020b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djy:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lhlb;->djy:Landroid/widget/TextView;

    const v1, 0x7f0c010f

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 71
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110200

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhlb;->djz:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110209

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v0, p0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    .line 75
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f11020a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v0, p0, Lhlb;->djx:Lcom/google/android/search/shared/ui/WebImageView;

    .line 78
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhlb;->djA:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110203

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djC:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110204

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djD:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110202

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djB:Landroid/widget/TextView;

    .line 85
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110205

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhlb;->djE:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110206

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djF:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110207

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djG:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f110208

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhlb;->djH:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    const v1, 0x7f1101f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhlb;->bWJ:Landroid/view/View;

    .line 93
    iget-object v0, p0, Lhlb;->djt:Landroid/view/View;

    invoke-virtual {p0, v0}, Lhlb;->addView(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method private bO(Landroid/view/View;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1f4

    .line 110
    iget-object v0, p0, Lhlb;->djJ:Landroid/view/View;

    if-eq v0, p1, :cond_0

    .line 111
    iget-object v0, p0, Lhlb;->djJ:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lhlb;->djJ:Landroid/view/View;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lelv;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 114
    invoke-static {p1}, Lelv;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 118
    :goto_0
    iput-object p1, p0, Lhlb;->djJ:Landroid/view/View;

    .line 120
    :cond_0
    return-void

    .line 116
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/utils/ExampleContact;)V
    .locals 6

    .prologue
    .line 166
    new-instance v0, Lhlc;

    invoke-direct {v0, p0}, Lhlc;-><init>(Lhlb;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    iget-wide v4, p1, Lcom/google/android/search/shared/actions/utils/ExampleContact;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lhlc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 194
    return-void
.end method

.method public final a(Lesm;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lhlb;->mImageLoader:Lesm;

    .line 103
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f400000    # 0.75f

    .line 198
    iget-object v0, p0, Lhlb;->djJ:Landroid/view/View;

    iget-object v1, p0, Lhlb;->djA:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 199
    const-wide/16 v0, 0x64

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/ViewPropertyAnimator;

    const/4 v4, 0x0

    iget-object v5, p0, Lhlb;->djB:Landroid/widget/TextView;

    invoke-static {v5, p1, v6}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lhlb;->djC:Landroid/widget/TextView;

    invoke-static {v5, p2, v6}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lhlb;->djD:Landroid/widget/TextView;

    invoke-static {v5, p3, v6}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lelv;->a(JF[Landroid/view/ViewPropertyAnimator;)V

    .line 209
    :goto_0
    iget-object v0, p0, Lhlb;->djA:Landroid/view/View;

    invoke-direct {p0, v0}, Lhlb;->bO(Landroid/view/View;)V

    .line 210
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lhlb;->djB:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    iget-object v0, p0, Lhlb;->djC:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lhlb;->djD:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method final aQy()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lhlb;->djx:Lcom/google/android/search/shared/ui/WebImageView;

    .line 148
    iget-object v1, p0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v1, p0, Lhlb;->djx:Lcom/google/android/search/shared/ui/WebImageView;

    .line 149
    iput-object v0, p0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    .line 152
    iget-object v0, p0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-direct {p0, v0}, Lhlb;->bO(Landroid/view/View;)V

    .line 154
    return-void
.end method

.method public final e(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lhlb;->djy:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    return-void
.end method

.method public final ej(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lhlb;->dju:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-void
.end method

.method public final ek(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 130
    invoke-virtual {p0}, Lhlb;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a065c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lhlb;->djv:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lhlb;->djv:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v1, p0, Lhlb;->djv:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/String;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final el(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lhlb;->aQy()V

    .line 159
    iget-object v0, p0, Lhlb;->mImageLoader:Lesm;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    iget-object v1, p0, Lhlb;->mImageLoader:Lesm;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 161
    return-void
.end method

.method public final f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f400000    # 0.75f

    .line 214
    iget-object v0, p0, Lhlb;->djJ:Landroid/view/View;

    iget-object v1, p0, Lhlb;->djE:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 215
    const-wide/16 v0, 0x64

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/ViewPropertyAnimator;

    const/4 v4, 0x0

    iget-object v5, p0, Lhlb;->djF:Landroid/widget/TextView;

    invoke-static {v5, p1, v7}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lhlb;->djG:Landroid/widget/TextView;

    iget-object v6, p0, Lhlb;->djG:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v5, v6, v7}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lhlb;->djH:Landroid/widget/TextView;

    invoke-static {v5, p2, v7}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lelv;->a(JF[Landroid/view/ViewPropertyAnimator;)V

    .line 225
    :goto_0
    iget-object v0, p0, Lhlb;->djE:Landroid/view/View;

    invoke-direct {p0, v0}, Lhlb;->bO(Landroid/view/View;)V

    .line 226
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lhlb;->djF:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v0, p0, Lhlb;->djH:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 235
    invoke-virtual {p0, p1}, Lhlb;->ej(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lhlb;->djv:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v0, p0, Lhlb;->bWJ:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lhlb;->djy:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lhlb;->djz:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 240
    return-void
.end method
