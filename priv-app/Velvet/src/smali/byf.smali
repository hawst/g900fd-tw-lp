.class public final Lbyf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field public aOJ:Lcom/google/android/shared/search/Query;

.field private aOK:Landroid/content/ComponentName;

.field private final mActionState:Ldbd;

.field public final mEventBus:Ldda;

.field private final mHandler:Landroid/os/Handler;

.field final mIntentStarter:Leqp;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mScreenStateHelper:Ldmh;

.field private final mTtsState:Ldcw;


# direct methods
.method public constructor <init>(Ldda;Leqp;Ldmh;)V
    .locals 1
    .param p1    # Ldda;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Leqp;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ldmh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lbyf;->mEventBus:Ldda;

    .line 57
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Lbyf;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 58
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    iput-object v0, p0, Lbyf;->mActionState:Ldbd;

    .line 59
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    iput-object v0, p0, Lbyf;->mTtsState:Ldcw;

    .line 60
    iput-object p2, p0, Lbyf;->mIntentStarter:Leqp;

    .line 61
    iput-object p3, p0, Lbyf;->mScreenStateHelper:Ldmh;

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbyf;->mHandler:Landroid/os/Handler;

    .line 63
    return-void
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 170
    const/16 v0, 0xb

    return v0
.end method

.method public final a(Lddb;)V
    .locals 6

    .prologue
    .line 81
    iget-object v0, p0, Lbyf;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 82
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbyf;->aOJ:Lcom/google/android/shared/search/Query;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbyf;->aOJ:Lcom/google/android/shared/search/Query;

    if-eq v0, v1, :cond_1

    .line 85
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbyf;->mTtsState:Ldcw;

    invoke-virtual {v1}, Ldcw;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbyf;->aOJ:Lcom/google/android/shared/search/Query;

    if-eq v0, v1, :cond_2

    .line 97
    iput-object v0, p0, Lbyf;->aOJ:Lcom/google/android/shared/search/Query;

    .line 99
    iget-object v0, p0, Lbyf;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->ads()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lbyf;->aOK:Landroid/content/ComponentName;

    .line 105
    iget-object v0, p0, Lbyf;->aOK:Landroid/content/ComponentName;

    if-nez v0, :cond_2

    .line 106
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    goto :goto_0

    .line 113
    :cond_2
    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyf;->mActionState:Ldbd;

    invoke-virtual {v0}, Ldbd;->VJ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyf;->aOK:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lbyf;->mActionState:Ldbd;

    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_4

    instance-of v1, v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-eqz v1, :cond_4

    .line 127
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->auq()Landroid/content/Intent;

    move-result-object v0

    .line 131
    :goto_1
    if-eqz v0, :cond_4

    const-string v1, "google.maps:?act=9"

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    goto :goto_0

    .line 128
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 141
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbyf;->aOK:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lbyf;->mScreenStateHelper:Ldmh;

    invoke-virtual {v1}, Ldmh;->adq()I

    move-result v1

    if-nez v1, :cond_5

    .line 152
    iget-object v1, p0, Lbyf;->mHandler:Landroid/os/Handler;

    new-instance v2, Lbyg;

    invoke-direct {v2, p0, v0}, Lbyg;-><init>(Lbyf;Landroid/content/Intent;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 164
    :goto_2
    iget-object v0, p0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    goto/16 :goto_0

    .line 161
    :cond_5
    iget-object v1, p0, Lbyf;->mIntentStarter:Leqp;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Leqp;->b([Landroid/content/Intent;)Z

    goto :goto_2
.end method
