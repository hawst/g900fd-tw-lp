.class public final Lcev;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic aTM:Landroid/net/Uri;

.field private synthetic aTN:Ligi;

.field private synthetic aTO:Lceu;


# direct methods
.method public varargs constructor <init>(Lceu;Ljava/lang/String;[ILandroid/net/Uri;Ligi;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcev;->aTO:Lceu;

    iput-object p4, p0, Lcev;->aTM:Landroid/net/Uri;

    iput-object p5, p0, Lcev;->aTN:Ligi;

    invoke-direct {p0, p2, p3}, Lepm;-><init>(Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 55
    iget-object v1, p0, Lcev;->aTO:Lceu;

    iget-object v4, p0, Lcev;->aTM:Landroid/net/Uri;

    iget-object v3, p0, Lcev;->aTN:Ligi;

    invoke-interface {v3}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/api/UriRequest;

    new-instance v2, Ldlb;

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v2, v5, v0}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ldlb;->setFollowRedirects(Z)V

    const/4 v5, 0x0

    :try_start_0
    iget-object v0, v1, Lceu;->mHttpHelper:Ldkx;

    const/4 v6, 0x5

    invoke-virtual {v0, v2, v6}, Ldkx;->b(Ldlb;I)Ljava/lang/String;

    const-string v0, "Velvet.AdClickHandler"

    const-string v2, "Did not receive a redirect from an ad click!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ldlc; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v6, v1, Lceu;->mUiThread:Leqo;

    new-instance v0, Lcew;

    const-string v2, "logAdClickAndGetRedirectDestination"

    invoke-direct/range {v0 .. v5}, Lcew;-><init>(Lceu;Ljava/lang/String;Ligi;Landroid/net/Uri;Landroid/net/Uri;)V

    invoke-interface {v6, v0}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 56
    return-void

    .line 55
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ldlc;->acR()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "Velvet.AdClickHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Ad click failed: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
