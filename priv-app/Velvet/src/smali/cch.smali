.class public final Lcch;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aRN:Landroid/app/NotificationManager;

.field private aRO:Lhzc;

.field final aRP:Lhll;

.field final mContext:Landroid/content/Context;

.field private final mCoreServices:Lcfo;

.field private final mGsaConfigFlags:Lchk;

.field final mIntentStarter:Leqp;

.field private final mSearchSettings:Lcke;

.field private final mVelvetServices:Lgql;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 30

    .prologue
    .line 120
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->mVelvetServices:Lgql;

    .line 81
    move-object/from16 v0, p0

    iget-object v2, v0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->mGsaConfigFlags:Lchk;

    .line 82
    move-object/from16 v0, p0

    iget-object v2, v0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->mCoreServices:Lcfo;

    .line 87
    move-object/from16 v0, p0

    iget-object v2, v0, Lcch;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->mSearchSettings:Lcke;

    .line 88
    move-object/from16 v0, p0

    iget-object v2, v0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJU()Lgpu;

    move-result-object v2

    new-instance v3, Lcci;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcci;-><init>(Lcch;)V

    invoke-virtual {v2, v3}, Lgpu;->b(Leoj;)Lhll;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->aRP:Lhll;

    .line 121
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcch;->mContext:Landroid/content/Context;

    .line 122
    new-instance v2, Lenh;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcch;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lenh;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->mIntentStarter:Leqp;

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcch;->aRO:Lhzc;

    if-nez v2, :cond_0

    new-instance v2, Lhzc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcch;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJU()Lgpu;

    move-result-object v4

    invoke-virtual {v4}, Lgpu;->aJM()Ldmh;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcch;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcch;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->Em()Lcha;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->aJV()Lcjg;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v8}, Lgql;->aKa()Lciy;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcch;->mSearchSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcch;->mGsaConfigFlags:Lchk;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcch;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcch;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcch;->mCoreServices:Lcfo;

    invoke-virtual {v14}, Lcfo;->DX()Lenm;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcfo;->Eg()Ligi;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcfo;->DL()Lcrh;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    new-instance v21, Licp;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcfo;->Es()Libo;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcfo;->DL()Lcrh;

    move-result-object v23

    invoke-direct/range {v21 .. v23}, Licp;-><init>(Libo;Lglm;)V

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v22

    new-instance v23, Lgru;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcfo;->DC()Lemp;

    move-result-object v25

    invoke-direct/range {v23 .. v25}, Lgru;-><init>(Landroid/content/Context;Lemp;)V

    new-instance v24, Lgtj;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcfo;->DC()Lemp;

    move-result-object v26

    invoke-direct/range {v24 .. v26}, Lgtj;-><init>(Landroid/content/Context;Lemp;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mCoreServices:Lcfo;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcfo;->DF()Lcin;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mVelvetServices:Lgql;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lgql;->aJZ()Lciu;

    move-result-object v26

    const/16 v27, 0x0

    new-instance v28, Lbye;

    invoke-direct/range {v28 .. v28}, Lbye;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcch;->mVelvetServices:Lgql;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lgql;->aJR()Lcgh;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Lcgh;->ET()Ldgm;

    move-result-object v29

    invoke-direct/range {v2 .. v29}, Lhzc;-><init>(Landroid/content/Context;Ldmh;Ldyv;Lcha;Lcjg;Lciy;Lcke;Lchk;Ligi;Lghy;Landroid/content/ContentResolver;Lenm;ZLigi;Lcrh;Lbxy;Libv;Lgpc;Licp;ILgru;Lgtj;Lcin;Lciu;Lbwo;Lbye;Ldgm;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->aRO:Lhzc;

    .line 124
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcch;->mContext:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcch;->aRN:Landroid/app/NotificationManager;

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhzc;Landroid/app/NotificationManager;)V
    .locals 2

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iput-object v0, p0, Lcch;->mVelvetServices:Lgql;

    .line 81
    iget-object v0, p0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcch;->mGsaConfigFlags:Lchk;

    .line 82
    iget-object v0, p0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    iput-object v0, p0, Lcch;->mCoreServices:Lcfo;

    .line 87
    iget-object v0, p0, Lcch;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lcch;->mSearchSettings:Lcke;

    .line 88
    iget-object v0, p0, Lcch;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJU()Lgpu;

    move-result-object v0

    new-instance v1, Lcci;

    invoke-direct {v1, p0}, Lcci;-><init>(Lcch;)V

    invoke-virtual {v0, v1}, Lgpu;->b(Leoj;)Lhll;

    move-result-object v0

    iput-object v0, p0, Lcch;->aRP:Lhll;

    .line 135
    iput-object p1, p0, Lcch;->mContext:Landroid/content/Context;

    .line 136
    new-instance v0, Lenh;

    iget-object v1, p0, Lcch;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lenh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcch;->mIntentStarter:Leqp;

    .line 137
    iput-object p2, p0, Lcch;->aRO:Lhzc;

    .line 138
    iput-object p3, p0, Lcch;->aRN:Landroid/app/NotificationManager;

    .line 139
    return-void
.end method

.method private static b(Ljcl;)Ljkt;
    .locals 5

    .prologue
    .line 262
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 265
    :try_start_0
    invoke-virtual {p0}, Ljcl;->bgE()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    return-object v0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    const-string v1, "PhonelinkPushDelegate"

    const-string v2, "Error decoding ActionV2: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljsq;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 269
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljcl;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 150
    invoke-static {p1}, Lcch;->b(Ljcl;)Ljkt;

    move-result-object v3

    .line 152
    if-nez v3, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    sget-object v0, Ljmp;->eui:Ljsm;

    invoke-virtual {v3, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    .line 164
    :goto_1
    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcch;->aRO:Lhzc;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    new-instance v2, Lccj;

    invoke-direct {v2, p0}, Lccj;-><init>(Lcch;)V

    invoke-virtual {v0, v3, v1, v2}, Lhzc;->a(Ljkt;Lcom/google/android/shared/search/Query;Lefk;)Z

    goto :goto_0

    .line 159
    :cond_2
    sget-object v0, Ljmp;->eui:Ljsm;

    invoke-virtual {v3, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmp;

    invoke-virtual {v0}, Ljmp;->getBody()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Landroid/app/Notification$Builder;

    iget-object v5, p0, Lcch;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f020120

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    iget-object v5, p0, Lcch;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0502

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v6, "android.intent.action.SEND"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "com.google.android.voicesearch.SELF_NOTE"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "text/plain"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcch;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v6

    const-class v7, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;

    invoke-virtual {v6, v7}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    invoke-virtual {v6, v5}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    const/high16 v5, 0x8000000

    invoke-virtual {v6, v1, v5}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v5

    new-instance v6, Lcck;

    invoke-direct {v6, p0, v0}, Lcck;-><init>(Lcch;Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v7, "com.google.android.phonelink.ACTION_COPY"

    invoke-direct {v0, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcch;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v6, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string v6, "com.google.android.phonelink.ACTION_COPY"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcch;->mContext:Landroid/content/Context;

    const/high16 v7, 0x10000000

    invoke-static {v6, v1, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v1, 0x7f020110

    iget-object v6, p0, Lcch;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0504

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v1, v6, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const v0, 0x7f020170

    iget-object v1, p0, Lcch;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0503

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1, v5}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcch;->aRN:Landroid/app/NotificationManager;

    const/16 v1, 0x270f

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    move v0, v2

    goto/16 :goto_1
.end method
