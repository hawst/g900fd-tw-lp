.class final Lfki;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic ctI:Lfmt;

.field private synthetic ctJ:Landroid/view/View;

.field private synthetic ctT:Lfkf;


# direct methods
.method constructor <init>(Lfkf;Lfmt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lfki;->ctT:Lfkf;

    iput-object p2, p0, Lfki;->ctI:Lfmt;

    iput-object p3, p0, Lfki;->ctJ:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 268
    iget-object v0, p0, Lfki;->ctT:Lfkf;

    invoke-virtual {v0}, Lfkf;->aAB()I

    move-result v0

    .line 269
    packed-switch v0, :pswitch_data_0

    .line 281
    const-string v1, "InferredEventEntryAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected step: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :goto_0
    return-void

    .line 272
    :pswitch_0
    iget-object v0, p0, Lfki;->ctI:Lfmt;

    iget-object v1, p0, Lfki;->ctT:Lfkf;

    invoke-virtual {v1}, Lfkf;->getEntry()Lizj;

    move-result-object v1

    invoke-interface {v0, v1}, Lfmt;->t(Lizj;)V

    .line 273
    iget-object v0, p0, Lfki;->ctI:Lfmt;

    iget-object v1, p0, Lfki;->ctT:Lfkf;

    invoke-virtual {v1}, Lfkf;->getEntry()Lizj;

    move-result-object v1

    const/16 v2, 0xcc

    invoke-interface {v0, v1, v2, v3, v3}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    goto :goto_0

    .line 277
    :pswitch_1
    iget-object v0, p0, Lfki;->ctJ:Landroid/view/View;

    const v1, 0x7f110229

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lfki;->ctT:Lfkf;

    iget-object v1, p0, Lfki;->ctJ:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfkf;->x(Landroid/view/View;I)V

    goto :goto_0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
