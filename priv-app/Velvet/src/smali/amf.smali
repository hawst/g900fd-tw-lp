.class public final Lamf;
.super Ljsl;
.source "PG"


# instance fields
.field private aeA:J

.field public aeB:Ljbp;

.field public aeC:Lizn;

.field private aeD:Z

.field private aeE:Ljava/lang/String;

.field private aeF:Z

.field private aez:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 111
    iput v2, p0, Lamf;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamf;->aeA:J

    iput-object v3, p0, Lamf;->aeB:Ljbp;

    iput-object v3, p0, Lamf;->aeC:Lizn;

    iput-boolean v2, p0, Lamf;->aeD:Z

    const-string v0, ""

    iput-object v0, p0, Lamf;->aeE:Ljava/lang/String;

    iput-boolean v2, p0, Lamf;->aeF:Z

    iput-object v3, p0, Lamf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamf;->eCz:I

    .line 112
    return-void
.end method


# virtual methods
.method public final P(Ljava/lang/String;)Lamf;
    .locals 1

    .prologue
    .line 75
    if-nez p1, :cond_0

    .line 76
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 78
    :cond_0
    iput-object p1, p0, Lamf;->aeE:Ljava/lang/String;

    .line 79
    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamf;->aez:I

    .line 80
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamf;->aeA:J

    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamf;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lamf;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Lamf;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Lamf;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lamf;->aeC:Lizn;

    if-nez v0, :cond_2

    new-instance v0, Lizn;

    invoke-direct {v0}, Lizn;-><init>()V

    iput-object v0, p0, Lamf;->aeC:Lizn;

    :cond_2
    iget-object v0, p0, Lamf;->aeC:Lizn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamf;->aeD:Z

    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamf;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamf;->aeE:Ljava/lang/String;

    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamf;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamf;->aeF:Z

    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 130
    iget v0, p0, Lamf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    iget-wide v2, p0, Lamf;->aeA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 133
    :cond_0
    iget-object v0, p0, Lamf;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 134
    const/4 v0, 0x2

    iget-object v1, p0, Lamf;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 136
    :cond_1
    iget-object v0, p0, Lamf;->aeC:Lizn;

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x3

    iget-object v1, p0, Lamf;->aeC:Lizn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 139
    :cond_2
    iget v0, p0, Lamf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 140
    const/4 v0, 0x4

    iget-boolean v1, p0, Lamf;->aeD:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 142
    :cond_3
    iget v0, p0, Lamf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 143
    const/4 v0, 0x5

    iget-object v1, p0, Lamf;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 145
    :cond_4
    iget v0, p0, Lamf;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 146
    const/4 v0, 0x6

    iget-boolean v1, p0, Lamf;->aeF:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 148
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 149
    return-void
.end method

.method public final aD(Z)Lamf;
    .locals 1

    .prologue
    .line 56
    iput-boolean p1, p0, Lamf;->aeD:Z

    .line 57
    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamf;->aez:I

    .line 58
    return-object p0
.end method

.method public final aE(Z)Lamf;
    .locals 1

    .prologue
    .line 97
    iput-boolean p1, p0, Lamf;->aeF:Z

    .line 98
    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamf;->aez:I

    .line 99
    return-object p0
.end method

.method public final getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lamf;->aeE:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 153
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 154
    iget v1, p0, Lamf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 155
    const/4 v1, 0x1

    iget-wide v2, p0, Lamf;->aeA:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_0
    iget-object v1, p0, Lamf;->aeB:Ljbp;

    if-eqz v1, :cond_1

    .line 159
    const/4 v1, 0x2

    iget-object v2, p0, Lamf;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_1
    iget-object v1, p0, Lamf;->aeC:Lizn;

    if-eqz v1, :cond_2

    .line 163
    const/4 v1, 0x3

    iget-object v2, p0, Lamf;->aeC:Lizn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_2
    iget v1, p0, Lamf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 167
    const/4 v1, 0x4

    iget-boolean v2, p0, Lamf;->aeD:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 170
    :cond_3
    iget v1, p0, Lamf;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 171
    const/4 v1, 0x5

    iget-object v2, p0, Lamf;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_4
    iget v1, p0, Lamf;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 175
    const/4 v1, 0x6

    iget-boolean v2, p0, Lamf;->aeF:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 178
    :cond_5
    return v0
.end method

.method public final mM()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lamf;->aeA:J

    return-wide v0
.end method

.method public final mN()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lamf;->aeD:Z

    return v0
.end method

.method public final mO()Z
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lamf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mP()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lamf;->aeF:Z

    return v0
.end method

.method public final o(J)Lamf;
    .locals 1

    .prologue
    .line 31
    iput-wide p1, p0, Lamf;->aeA:J

    .line 32
    iget v0, p0, Lamf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamf;->aez:I

    .line 33
    return-object p0
.end method
