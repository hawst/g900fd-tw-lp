.class public final Ldzt;
.super Ldzl;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ldzl;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Person;)Lcom/google/android/search/shared/contact/Person;
    .locals 2
    .param p1    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 165
    invoke-virtual {p1, p2}, Lcom/google/android/search/shared/contact/Person;->a(Ldzk;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-object v0

    .line 168
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->ame()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->ame()Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->aiI()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->aiI()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    :cond_5
    invoke-virtual {p1, p2}, Lcom/google/android/search/shared/contact/Person;->h(Lcom/google/android/search/shared/contact/Person;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v0

    goto :goto_0
.end method
