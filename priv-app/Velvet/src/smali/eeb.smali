.class public final Leeb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;
.implements Ljava/io/Serializable;


# static fields
.field public static final serialVersionUID:J = 0x1L


# instance fields
.field public final bWw:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bWx:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Leeb;->bWw:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Leeb;->bWx:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static c(Ldyj;)Leeb;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    if-eqz p0, :cond_0

    .line 45
    const-string v0, "Content-Type"

    invoke-virtual {p0, v0}, Ldyj;->aF(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Leeb;->kM(Ljava/lang/String;)Leeb;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Leeb;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 83
    iget-object v0, p0, Leeb;->bWw:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 84
    const-string v0, ""

    .line 88
    :goto_0
    return-object v0

    .line 86
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "charset"

    iget-object v4, p0, Leeb;->bWx:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 87
    new-instance v1, Lorg/apache/http/message/BasicHeaderElement;

    iget-object v2, p0, Leeb;->bWw:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lorg/apache/http/message/BasicHeaderElement;-><init>(Ljava/lang/String;Ljava/lang/String;[Lorg/apache/http/NameValuePair;)V

    .line 88
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static kM(Ljava/lang/String;)Leeb;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 59
    .line 63
    :try_start_0
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-Type"

    invoke-direct {v0, v2, p0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {v0}, Lorg/apache/http/message/BasicHeader;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_3

    .line 65
    invoke-virtual {v0}, Lorg/apache/http/message/BasicHeader;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 66
    invoke-interface {v0}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 67
    :try_start_1
    const-string v3, "charset"

    invoke-interface {v0, v3}, Lorg/apache/http/HeaderElement;->getParameterByName(Ljava/lang/String;)Lorg/apache/http/NameValuePair;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_2

    .line 69
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 76
    :goto_0
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 77
    :cond_0
    new-instance v1, Leeb;

    invoke-direct {v1, v2, v0}, Leeb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_1
    return-object v1

    .line 73
    :catch_0
    move-exception v0

    move-object v2, v1

    :goto_1
    const-string v0, "MimeTypeAndCharSet"

    const-string v3, "Missing content type header"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    move-object v2, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Leeb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 93
    instance-of v1, p1, Leeb;

    if-eqz v1, :cond_0

    .line 94
    check-cast p1, Leeb;

    .line 95
    iget-object v1, p0, Leeb;->bWw:Ljava/lang/String;

    iget-object v2, p1, Leeb;->bWw:Ljava/lang/String;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leeb;->bWx:Ljava/lang/String;

    iget-object v2, p1, Leeb;->bWx:Ljava/lang/String;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 98
    :cond_0
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{mimeType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leeb;->bWw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", charset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Leeb;->bWx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
