.class final Lfmi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfjv;


# instance fields
.field private final cvS:Lfml;


# direct methods
.method constructor <init>(Lfml;)V
    .locals 0

    .prologue
    .line 669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670
    iput-object p1, p0, Lfmi;->cvS:Lfml;

    .line 671
    return-void
.end method


# virtual methods
.method public final P(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lfmi;->cvS:Lfml;

    invoke-virtual {v0, p1}, Lfml;->V(Ljava/util/List;)V

    .line 693
    return-void
.end method

.method public final a(Lizj;Liwk;JIZ)V
    .locals 7

    .prologue
    .line 681
    invoke-virtual {p2}, Liwk;->getType()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 682
    iget-object v0, p0, Lfmi;->cvS:Lfml;

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    move-wide v2, p3

    move v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lfor;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;JIZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 683
    :cond_0
    :goto_1
    return-void

    .line 681
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 682
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making record view-action request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final a(Lizj;Liwk;Lixx;)V
    .locals 0

    .prologue
    .line 688
    return-void
.end method

.method public final e(Lizj;Liwk;)V
    .locals 3

    .prologue
    .line 675
    iget-object v0, p0, Lfmi;->cvS:Lfml;

    invoke-virtual {p2}, Liwk;->getType()I

    move-result v1

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lfor;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making record action request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
