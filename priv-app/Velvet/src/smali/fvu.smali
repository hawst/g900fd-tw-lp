.class public final Lfvu;
.super Lfus;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;

.field private final mPlaceDataHelper:Lgbe;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgan;Lgbe;Lgbr;)V
    .locals 6

    .prologue
    .line 37
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 39
    iput-object p4, p0, Lfvu;->mFifeImageUrlUtil:Lgan;

    .line 40
    iput-object p5, p0, Lfvu;->mPlaceDataHelper:Lgbe;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 45
    new-instance v7, Lanh;

    invoke-direct {v7}, Lanh;-><init>()V

    .line 46
    const/16 v1, 0xa

    invoke-virtual {v7, v1}, Lanh;->cm(I)Lanh;

    .line 47
    iput-object p2, v7, Lanh;->ahu:Lizj;

    .line 49
    iget-object v1, p2, Lizj;->dSz:Liwy;

    iget-object v1, v1, Liwy;->dMG:Liwx;

    .line 50
    iget-object v3, v1, Liwx;->dME:Lixn;

    .line 52
    iget-object v2, v1, Liwx;->dMF:[Liyg;

    array-length v2, v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Liwx;->dMF:[Liyg;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Liyg;->bbE()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v1, v1, Liwx;->dMF:[Liyg;

    aget-object v0, v1, v0

    invoke-virtual {v0}, Liyg;->bbE()I

    move-result v0

    move v6, v0

    .line 55
    :goto_0
    const-string v0, " \u00b7 "

    iget-object v1, v3, Lixn;->dNH:[Ljava/lang/String;

    invoke-static {v0, v1}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 57
    new-instance v0, Lfud;

    invoke-virtual {p0}, Lfvu;->aDy()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v4, p0, Lfvu;->mPlaceDataHelper:Lgbe;

    iget-object v5, p0, Lfvu;->mFifeImageUrlUtil:Lgan;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfud;-><init>(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lixn;Lgbe;Lgan;)V

    invoke-virtual {v3}, Lixn;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lfud;->bLI:Ljava/lang/String;

    iput v6, v0, Lfud;->cCd:I

    iput-object v8, v0, Lfud;->kO:Ljava/lang/String;

    invoke-virtual {v0}, Lfud;->aDt()Laoc;

    move-result-object v0

    iput-object v0, v7, Lanh;->agH:Laoc;

    .line 64
    invoke-virtual {v3}, Lixn;->bar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v3}, Lixn;->baq()J

    move-result-wide v0

    invoke-static {v0, v1}, Lgbe;->bt(J)Ljava/lang/String;

    move-result-object v0

    .line 66
    new-instance v1, Lfsa;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 67
    iput-object v0, v7, Lanh;->ahs:Lani;

    .line 70
    :cond_0
    return-object v7

    :cond_1
    move v6, v0

    .line 52
    goto :goto_0
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 4

    .prologue
    .line 81
    const/4 v0, 0x0

    .line 82
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v1, v1, Lizq;->dUZ:Lizj;

    .line 83
    iget-object v2, v1, Lizj;->dSS:Ljca;

    if-eqz v2, :cond_0

    .line 84
    iget-object v1, v1, Lizj;->dSS:Ljca;

    .line 85
    iget-object v2, v1, Ljca;->dZR:Lixx;

    if-eqz v2, :cond_0

    .line 86
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 87
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 88
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 89
    new-instance v2, Lfsa;

    const/16 v3, 0x8b

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0200ed

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    iget-object v3, v1, Ljca;->dZR:Lixx;

    invoke-virtual {v2, v3}, Lfsa;->b(Lixx;)Lani;

    move-result-object v2

    iput-object v2, v0, Lanh;->ahs:Lani;

    .line 93
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    .line 94
    iget-object v1, v1, Ljca;->dZR:Lixx;

    invoke-virtual {v1}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 95
    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 99
    :cond_0
    return-object v0
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 76
    const v0, 0x7f0a0372

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
