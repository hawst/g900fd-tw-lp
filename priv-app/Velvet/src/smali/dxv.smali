.class public final Ldxv;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/search/shared/actions/modular/ModularAction;Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)Ldzb;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getId()I

    move-result v1

    invoke-static {v0, v1}, Ldxv;->a(Ljrl;I)Ldzb;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljrl;I)Ldzb;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 70
    new-instance v2, Letg;

    invoke-direct {v2}, Letg;-><init>()V

    .line 71
    iget-object v0, p0, Ljrl;->eAH:[Ljqs;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, v2, Letg;->ciy:Ljava/lang/Object;

    check-cast v0, Ldzb;

    .line 88
    :goto_0
    return-object v0

    .line 75
    :cond_0
    iget-object v0, p0, Ljrl;->eAH:[Ljqs;

    aget-object v0, v0, v1

    sget-object v3, Ljre;->eAh:Ljsm;

    invoke-virtual {v0, v3}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljre;

    .line 77
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljre;->btc()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 78
    sget-object v0, Ldzb;->bRt:Ldzb;

    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Ljrl;->eAH:[Ljqs;

    aget-object v0, v0, v1

    invoke-static {v0}, Ldwc;->b(Ljqs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqv;

    .line 83
    iget-object v4, v0, Ljqv;->ezC:[Ljqu;

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_4

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljqu;->bsF()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6}, Ljqu;->bqD()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6}, Ljqu;->bqC()I

    move-result v7

    if-ne v7, p1, :cond_3

    invoke-virtual {v6}, Ljqu;->bsE()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :sswitch_0
    sget-object v6, Ldzb;->bRp:Ldzb;

    invoke-virtual {v2, v6}, Letg;->aR(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    .line 84
    const/4 v0, 0x0

    goto :goto_0

    .line 83
    :sswitch_1
    sget-object v6, Ldzb;->bRq:Ldzb;

    invoke-virtual {v2, v6}, Letg;->aR(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    move v0, v1

    goto :goto_2

    :sswitch_2
    sget-object v6, Ldzb;->bRs:Ldzb;

    invoke-virtual {v2, v6}, Letg;->aR(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    move v0, v1

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    .line 88
    :cond_5
    iget-object v0, v2, Letg;->ciy:Ljava/lang/Object;

    check-cast v0, Ldzb;

    goto :goto_0

    .line 83
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch
.end method

.method public static d(Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v2

    .line 34
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajh()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 35
    instance-of v1, v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 36
    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v0

    invoke-static {v2, v0}, Ldxv;->a(Ljrl;I)Ldzb;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->a(Ldzb;)V

    goto :goto_0

    .line 40
    :cond_1
    return-void
.end method
