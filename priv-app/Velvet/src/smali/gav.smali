.class public final Lgav;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lgbd;Lizj;Ljbh;I)Lang;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 128
    invoke-static {p0, p1, p2, p3}, Lgav;->a(Landroid/content/Context;Lgbd;Lizj;Ljbh;)Lanh;

    move-result-object v1

    .line 130
    invoke-static {p2, p3, p4}, Lgav;->a(Lizj;Ljbh;I)Lanh;

    move-result-object v2

    .line 133
    if-eqz v1, :cond_0

    .line 134
    invoke-virtual {v2, v4}, Lanh;->aW(Z)Lanh;

    .line 137
    :cond_0
    new-instance v3, Lang;

    invoke-direct {v3}, Lang;-><init>()V

    .line 138
    if-eqz v1, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Lanh;

    aput-object v1, v0, v5

    aput-object v2, v0, v4

    :goto_0
    iput-object v0, v3, Lang;->ags:[Lanh;

    .line 141
    return-object v3

    .line 138
    :cond_1
    new-array v0, v4, [Lanh;

    aput-object v2, v0, v5

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lgbd;Lizj;Ljbh;)Lanh;
    .locals 4

    .prologue
    .line 148
    iget-object v0, p3, Ljbh;->dWT:[Ljcn;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 163
    :goto_0
    return-object v0

    .line 150
    :cond_0
    iget-object v0, p3, Ljbh;->dWT:[Ljcn;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 151
    new-instance v1, Lanz;

    invoke-direct {v1}, Lanz;-><init>()V

    .line 152
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lanz;->ba(Z)Lanz;

    .line 155
    const v2, 0x7f0d0172

    const v3, 0x7f0d0130

    invoke-virtual {p1, p0, v0, v2, v3}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v0

    iput-object v0, v1, Lanz;->aiU:Laoi;

    .line 158
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 159
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 160
    iput-object v1, v0, Lanh;->agy:Lanz;

    .line 161
    iput-object p2, v0, Lanh;->ahu:Lizj;

    goto :goto_0
.end method

.method public static a(Lizj;Ljbh;I)Lanh;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 169
    new-instance v1, Lany;

    invoke-direct {v1}, Lany;-><init>()V

    .line 170
    invoke-virtual {p1}, Ljbh;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 171
    invoke-virtual {p1}, Ljbh;->hasText()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    invoke-virtual {p1}, Ljbh;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 174
    :cond_0
    invoke-virtual {p1}, Ljbh;->pf()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    invoke-virtual {p1}, Ljbh;->oo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lany;->bj(Ljava/lang/String;)Lany;

    .line 178
    :cond_1
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 179
    invoke-virtual {v2, v0}, Lanh;->cm(I)Lanh;

    .line 180
    iput-object v1, v2, Lanh;->agw:Lany;

    .line 181
    iput-object p0, v2, Lanh;->ahu:Lizj;

    .line 183
    new-array v1, v0, [I

    invoke-static {p0, p2, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 184
    if-eqz v1, :cond_2

    .line 185
    new-instance v3, Lfsa;

    invoke-direct {v3, p2}, Lfsa;-><init>(I)V

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    iget-object v4, p1, Ljbh;->ajE:Ljei;

    invoke-virtual {v3, p0, v1, v4}, Lfsa;->a(Lizj;Liwk;Ljei;)Lani;

    move-result-object v1

    iput-object v1, v2, Lanh;->ahs:Lani;

    .line 189
    invoke-virtual {p1}, Ljbh;->pb()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 190
    iget-object v1, v2, Lanh;->ahs:Lani;

    iget-object v1, v1, Lani;->ahH:Laom;

    invoke-virtual {p1}, Ljbh;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Laom;->bY(Ljava/lang/String;)Laom;

    .line 196
    :cond_2
    iget-object v1, p1, Ljbh;->dWT:[Ljcn;

    array-length v1, v1

    if-lez v1, :cond_4

    iget-object v1, p1, Ljbh;->ajE:Ljei;

    if-eqz v1, :cond_3

    iget-object v1, v1, Ljei;->eeu:Ljcn;

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    if-nez v0, :cond_4

    iget-object v0, v2, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahH:Laom;

    iget-object v0, v0, Laom;->ajH:[Ljcn;

    array-length v0, v0

    if-nez v0, :cond_4

    .line 199
    iget-object v0, v2, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahH:Laom;

    iget-object v1, p1, Ljbh;->dWT:[Ljcn;

    iput-object v1, v0, Laom;->ajH:[Ljcn;

    .line 202
    :cond_4
    return-object v2
.end method
