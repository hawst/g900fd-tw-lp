.class public abstract Ldlg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lbzh;[B)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 38
    if-eqz p1, :cond_0

    .line 39
    invoke-interface {p0, p1}, Lbzh;->i([B)V

    .line 41
    :cond_0
    invoke-static {p0}, Ldmc;->d(Lbzh;)V

    .line 42
    const/4 v2, -0x1

    .line 48
    :try_start_0
    invoke-interface {p0}, Lbzh;->getResponseCode()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    move v3, v2

    .line 55
    :goto_0
    const/16 v2, 0xc8

    if-eq v3, v2, :cond_1

    const/16 v2, 0xcc

    if-ne v3, v2, :cond_4

    :cond_1
    move v2, v1

    :goto_1
    if-nez v2, :cond_6

    .line 56
    const-string v2, ""

    .line 58
    :try_start_1
    invoke-interface {p0}, Lbzh;->getResponseMessage()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    .line 63
    :goto_2
    const/16 v4, 0x12e

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12d

    if-ne v3, v4, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-eqz v0, :cond_5

    .line 65
    const-string v0, "Location"

    invoke-interface {p0, v0}, Lbzh;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    new-instance v1, Ldlc;

    invoke-direct {v1, v3, v2, v0}, Ldlc;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 49
    :catch_0
    move-exception v0

    .line 50
    new-instance v1, Lefs;

    const v2, 0x40005

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 51
    :catch_1
    move-exception v3

    .line 52
    const v4, 0x40004

    invoke-static {v3, v4}, Ldmc;->b(Ljava/io/IOException;I)V

    move v3, v2

    goto :goto_0

    :cond_4
    move v2, v0

    .line 55
    goto :goto_1

    .line 61
    :catch_2
    move-exception v4

    const-string v4, "HttpResponseFetcher"

    const-string v5, "Failed to get response message"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 68
    :cond_5
    new-instance v0, Left;

    invoke-direct {v0, v3, v2}, Left;-><init>(ILjava/lang/String;)V

    throw v0

    .line 70
    :cond_6
    return-void
.end method


# virtual methods
.method public abstract b(Lbzh;[BI)Ljava/lang/Object;
    .param p1    # Lbzh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
.end method
