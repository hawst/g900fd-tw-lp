.class public final Lfcm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bCq:Ljava/util/Locale;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final coI:Ljava/lang/String;

.field public final mApplicationVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfcm;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V

    .line 231
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 0
    .param p3    # Ljava/util/Locale;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput-object p2, p0, Lfcm;->coI:Ljava/lang/String;

    .line 223
    iput-object p1, p0, Lfcm;->mApplicationVersion:Ljava/lang/String;

    .line 224
    iput-object p3, p0, Lfcm;->bCq:Ljava/util/Locale;

    .line 225
    return-void
.end method

.method public static lI(Ljava/lang/String;)Lfcm;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 280
    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 281
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 283
    const/4 v0, 0x0

    aget-object v3, v2, v0

    .line 284
    const-string v0, ""

    .line 285
    array-length v1, v2

    if-le v1, v4, :cond_0

    .line 286
    aget-object v0, v2, v4

    .line 288
    :cond_0
    const/4 v1, 0x0

    .line 289
    array-length v4, v2

    if-le v4, v5, :cond_1

    .line 290
    aget-object v1, v2, v5

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, v2}, Lepb;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v1

    .line 292
    :cond_1
    new-instance v2, Lfcm;

    invoke-direct {v2, v0, v3, v1}, Lfcm;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V

    return-object v2
.end method


# virtual methods
.method public final axv()Ljava/lang/String;
    .locals 2

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "+"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfcm;->coI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lfcm;->mApplicationVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 272
    iget-object v1, p0, Lfcm;->bCq:Ljava/util/Locale;

    if-eqz v1, :cond_0

    .line 273
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lfcm;->bCq:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 253
    if-ne p0, p1, :cond_1

    .line 266
    :cond_0
    :goto_0
    return v0

    .line 255
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 256
    goto :goto_0

    .line 257
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 258
    goto :goto_0

    .line 259
    :cond_3
    check-cast p1, Lfcm;

    .line 260
    iget-object v2, p0, Lfcm;->mApplicationVersion:Ljava/lang/String;

    iget-object v3, p1, Lfcm;->mApplicationVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 261
    goto :goto_0

    .line 262
    :cond_4
    iget-object v2, p0, Lfcm;->coI:Ljava/lang/String;

    iget-object v3, p1, Lfcm;->coI:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 263
    goto :goto_0

    .line 264
    :cond_5
    iget-object v2, p0, Lfcm;->bCq:Ljava/util/Locale;

    iget-object v3, p1, Lfcm;->bCq:Ljava/util/Locale;

    invoke-static {v2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 265
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lfcm;->coI:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 241
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfcm;->mApplicationVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    iget-object v1, p0, Lfcm;->bCq:Ljava/util/Locale;

    if-eqz v1, :cond_0

    .line 243
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfcm;->bCq:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_0
    return v0
.end method
