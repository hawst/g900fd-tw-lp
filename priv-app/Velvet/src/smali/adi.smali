.class public abstract Ladi;
.super Lcom/android/launcher3/PagedView;
.source "PG"


# static fields
.field private static final SY:F


# instance fields
.field private SZ:F

.field private Ta:F

.field private Tb:I

.field private Tc:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    const-wide v0, 0x3f90624dd2f1a9fcL    # 0.016

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Ladi;->SY:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ladi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher3/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    iput-boolean v1, p0, Ladi;->QM:Z

    .line 83
    iget v2, p0, Ladi;->Tb:I

    if-eq v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Ladi;->QN:Z

    .line 84
    return-void

    :cond_0
    move v0, v1

    .line 83
    goto :goto_0
.end method

.method private b(IIZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 127
    invoke-virtual {p0}, Ladi;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 129
    const/4 v0, 0x1

    iget v2, p0, Ladi;->Qc:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 130
    invoke-virtual {p0, v1}, Ladi;->bz(I)I

    move-result v0

    .line 131
    iget v3, p0, Ladi;->QD:I

    sub-int v3, v0, v3

    .line 132
    add-int/lit8 v0, v2, 0x1

    mul-int/lit8 v4, v0, 0x64

    .line 134
    iget-object v0, p0, Ladi;->Qh:Labj;

    invoke-virtual {v0}, Labj;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Ladi;->Qh:Labj;

    invoke-virtual {v0}, Labj;->abortAnimation()V

    .line 138
    :cond_0
    if-eqz p3, :cond_1

    .line 139
    iget-object v0, p0, Ladi;->Tc:Landroid/view/animation/Interpolator;

    check-cast v0, Ladj;

    invoke-virtual {v0, v2}, Ladj;->bJ(I)V

    .line 144
    :goto_0
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 145
    if-lez v0, :cond_2

    .line 146
    int-to-float v2, v4

    int-to-float v4, v4

    int-to-float v0, v0

    iget v5, p0, Ladi;->SZ:F

    div-float/2addr v0, v5

    div-float v0, v4, v0

    iget v4, p0, Ladi;->Ta:F

    mul-float/2addr v0, v4

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 151
    :goto_1
    invoke-virtual {p0, v1, v3, v0}, Ladi;->g(III)V

    .line 152
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Ladi;->Tc:Landroid/view/animation/Interpolator;

    check-cast v0, Ladj;

    invoke-virtual {v0}, Ladj;->ki()V

    goto :goto_0

    .line 148
    :cond_2
    add-int/lit8 v0, v4, 0x64

    goto :goto_1
.end method


# virtual methods
.method protected final O(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 117
    iget v0, p0, Ladi;->Tb:I

    if-ne v0, v1, :cond_0

    .line 118
    invoke-super {p0, p1, p2}, Lcom/android/launcher3/PagedView;->O(II)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Ladi;->b(IIZ)V

    goto :goto_0
.end method

.method protected final bB(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156
    iget v0, p0, Ladi;->Tb:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 157
    invoke-super {p0, p1}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-direct {p0, p1, v2, v2}, Ladi;->b(IIZ)V

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 165
    iget v0, p0, Ladi;->Tb:I

    if-ne v0, v1, :cond_1

    .line 166
    invoke-super {p0}, Lcom/android/launcher3/PagedView;->computeScroll()V

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    invoke-virtual {p0}, Ladi;->js()Z

    move-result v0

    .line 170
    if-nez v0, :cond_0

    iget v0, p0, Ladi;->Qv:I

    if-ne v0, v1, :cond_0

    .line 171
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v0, v1

    .line 172
    iget v1, p0, Ladi;->PZ:F

    sub-float v1, v0, v1

    sget v2, Ladi;->SY:F

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 174
    iget v2, p0, Ladi;->Qa:F

    iget v3, p0, Ladi;->QD:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 175
    iget v3, p0, Ladi;->QD:I

    int-to-float v3, v3

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0}, Ladi;->getScrollY()I

    move-result v3

    invoke-virtual {p0, v1, v3}, Ladi;->scrollTo(II)V

    .line 176
    iput v0, p0, Ladi;->PZ:F

    .line 179
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    const/high16 v0, -0x40800000    # -1.0f

    cmpg-float v0, v2, v0

    if-gez v0, :cond_0

    .line 180
    :cond_2
    invoke-virtual {p0}, Ladi;->invalidate()V

    goto :goto_0
.end method

.method protected final ed()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lcom/android/launcher3/PagedView;->ed()V

    .line 97
    const/4 v0, 0x1

    iput v0, p0, Ladi;->Tb:I

    .line 98
    iget v0, p0, Ladi;->Tb:I

    if-nez v0, :cond_0

    .line 99
    const v0, 0x451c4000    # 2500.0f

    iput v0, p0, Ladi;->SZ:F

    .line 100
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Ladi;->Ta:F

    .line 101
    new-instance v0, Ladj;

    invoke-direct {v0}, Ladj;-><init>()V

    iput-object v0, p0, Ladi;->Tc:Landroid/view/animation/Interpolator;

    .line 102
    iget-object v0, p0, Ladi;->Tc:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, v0}, Ladi;->a(Landroid/view/animation/Interpolator;)V

    .line 104
    :cond_0
    return-void
.end method

.method protected final jF()V
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Ladi;->Tb:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 109
    invoke-super {p0}, Lcom/android/launcher3/PagedView;->jF()V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p0}, Ladi;->jC()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ladi;->O(II)V

    goto :goto_0
.end method
