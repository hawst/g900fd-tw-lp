.class public final Lbcj;
.super Lbci;


# instance fields
.field private final auP:Ljava/lang/String;

.field private final auQ:Ljava/lang/String;

.field private synthetic awU:Lcom/google/android/gms/appdatasearch/SearchResults;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lbcj;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0, p1}, Lbci;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    iput-object p2, p0, Lbcj;->auP:Ljava/lang/String;

    iput-object p3, p0, Lbcj;->auQ:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lbcj;->awV:I

    invoke-virtual {p0}, Lbcj;->xk()V

    return-void
.end method

.method private dz(I)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lbcj;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/SearchResults;->awP:[Ljava/lang/String;

    iget-object v3, p0, Lbcj;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    aget v3, v3, p1

    aget-object v3, v2, v3

    const/16 v2, 0x2d

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    iget-object v2, p0, Lbcj;->auP:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v4, v2, :cond_0

    iget-object v2, p0, Lbcj;->auP:Ljava/lang/String;

    invoke-virtual {v3, v1, v2, v1, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    iget-object v5, p0, Lbcj;->auQ:Ljava/lang/String;

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    iget-object v5, p0, Lbcj;->auQ:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v2, v5, :cond_1

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lbcj;->auQ:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v1, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final getCount()I
    .locals 4

    const/4 v0, 0x0

    invoke-super {p0}, Lbci;->getCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-direct {p0, v1}, Lbcj;->dz(I)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method protected final xk()V
    .locals 2

    :cond_0
    iget v0, p0, Lbcj;->awV:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbcj;->awV:I

    iget v0, p0, Lbcj;->awV:I

    iget-object v1, p0, Lbcj;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->wX()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lbcj;->awV:I

    invoke-direct {p0, v0}, Lbcj;->dz(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method
