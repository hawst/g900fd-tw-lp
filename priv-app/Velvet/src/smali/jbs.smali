.class public final Ljbs;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dUn:Ljava/lang/String;

.field private dZg:Ljava/lang/String;

.field private dZh:Z

.field private dZi:J

.field private dZj:I

.field private dZk:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18293
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 18294
    iput v2, p0, Ljbs;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbs;->dUn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbs;->dZg:Ljava/lang/String;

    iput-boolean v2, p0, Ljbs;->dZh:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbs;->dZi:J

    iput v2, p0, Ljbs;->dZj:I

    iput v2, p0, Ljbs;->dZk:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljbs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbs;->eCz:I

    .line 18295
    return-void
.end method


# virtual methods
.method public final AD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18198
    iget-object v0, p0, Ljbs;->dZg:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 18154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbs;->dUn:Ljava/lang/String;

    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbs;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbs;->dZg:Ljava/lang/String;

    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbs;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbs;->dZh:Z

    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbs;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljbs;->dZi:J

    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbs;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbs;->dZj:I

    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbs;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbs;->dZk:I

    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbs;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 18313
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 18314
    const/4 v0, 0x1

    iget-object v1, p0, Ljbs;->dUn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 18316
    :cond_0
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 18317
    const/4 v0, 0x2

    iget-object v1, p0, Ljbs;->dZg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 18319
    :cond_1
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 18320
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljbs;->dZh:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 18322
    :cond_2
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 18323
    const/4 v0, 0x4

    iget-wide v2, p0, Ljbs;->dZi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 18325
    :cond_3
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 18326
    const/4 v0, 0x5

    iget v1, p0, Ljbs;->dZj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 18328
    :cond_4
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 18329
    const/4 v0, 0x6

    iget v1, p0, Ljbs;->dZk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 18331
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 18332
    return-void
.end method

.method public final bfq()Z
    .locals 1

    .prologue
    .line 18228
    iget v0, p0, Ljbs;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfr()J
    .locals 2

    .prologue
    .line 18239
    iget-wide v0, p0, Ljbs;->dZi:J

    return-wide v0
.end method

.method public final bfs()I
    .locals 1

    .prologue
    .line 18258
    iget v0, p0, Ljbs;->dZj:I

    return v0
.end method

.method public final bft()I
    .locals 1

    .prologue
    .line 18277
    iget v0, p0, Ljbs;->dZk:I

    return v0
.end method

.method public final cG(J)Ljbs;
    .locals 2

    .prologue
    .line 18242
    const-wide/16 v0, 0x5

    iput-wide v0, p0, Ljbs;->dZi:J

    .line 18243
    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbs;->aez:I

    .line 18244
    return-object p0
.end method

.method public final getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18176
    iget-object v0, p0, Ljbs;->dUn:Ljava/lang/String;

    return-object v0
.end method

.method public final hE(Z)Ljbs;
    .locals 1

    .prologue
    .line 18223
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbs;->dZh:Z

    .line 18224
    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbs;->aez:I

    .line 18225
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 18336
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 18337
    iget v1, p0, Ljbs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 18338
    const/4 v1, 0x1

    iget-object v2, p0, Ljbs;->dUn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18341
    :cond_0
    iget v1, p0, Ljbs;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 18342
    const/4 v1, 0x2

    iget-object v2, p0, Ljbs;->dZg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18345
    :cond_1
    iget v1, p0, Ljbs;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 18346
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljbs;->dZh:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 18349
    :cond_2
    iget v1, p0, Ljbs;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 18350
    const/4 v1, 0x4

    iget-wide v2, p0, Ljbs;->dZi:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 18353
    :cond_3
    iget v1, p0, Ljbs;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 18354
    const/4 v1, 0x5

    iget v2, p0, Ljbs;->dZj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 18357
    :cond_4
    iget v1, p0, Ljbs;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 18358
    const/4 v1, 0x6

    iget v2, p0, Ljbs;->dZk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 18361
    :cond_5
    return v0
.end method

.method public final ox(I)Ljbs;
    .locals 1

    .prologue
    .line 18261
    const/4 v0, 0x5

    iput v0, p0, Ljbs;->dZj:I

    .line 18262
    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbs;->aez:I

    .line 18263
    return-object p0
.end method

.method public final oy(I)Ljbs;
    .locals 1

    .prologue
    .line 18280
    const/16 v0, 0xa

    iput v0, p0, Ljbs;->dZk:I

    .line 18281
    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbs;->aez:I

    .line 18282
    return-object p0
.end method

.method public final sy(Ljava/lang/String;)Ljbs;
    .locals 1

    .prologue
    .line 18179
    if-nez p1, :cond_0

    .line 18180
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18182
    :cond_0
    iput-object p1, p0, Ljbs;->dUn:Ljava/lang/String;

    .line 18183
    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbs;->aez:I

    .line 18184
    return-object p0
.end method

.method public final sz(Ljava/lang/String;)Ljbs;
    .locals 1

    .prologue
    .line 18201
    if-nez p1, :cond_0

    .line 18202
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18204
    :cond_0
    iput-object p1, p0, Ljbs;->dZg:Ljava/lang/String;

    .line 18205
    iget v0, p0, Ljbs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbs;->aez:I

    .line 18206
    return-object p0
.end method
