.class public final Lkel;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public eUG:Ljrp;

.field private eUH:I

.field private eUI:Z

.field public eUJ:Ljyw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 473
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 474
    iput v0, p0, Lkel;->aez:I

    iput-object v1, p0, Lkel;->eUG:Ljrp;

    iput v0, p0, Lkel;->eUH:I

    iput-boolean v0, p0, Lkel;->eUI:Z

    iput-object v1, p0, Lkel;->eUJ:Ljyw;

    iput-object v1, p0, Lkel;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkel;->eCz:I

    .line 475
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 410
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkel;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lkel;->eUG:Ljrp;

    if-nez v0, :cond_1

    new-instance v0, Ljrp;

    invoke-direct {v0}, Ljrp;-><init>()V

    iput-object v0, p0, Lkel;->eUG:Ljrp;

    :cond_1
    iget-object v0, p0, Lkel;->eUG:Ljrp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkel;->eUH:I

    iget v0, p0, Lkel;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkel;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lkel;->eUI:Z

    iget v0, p0, Lkel;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkel;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lkel;->eUJ:Ljyw;

    if-nez v0, :cond_2

    new-instance v0, Ljyw;

    invoke-direct {v0}, Ljyw;-><init>()V

    iput-object v0, p0, Lkel;->eUJ:Ljyw;

    :cond_2
    iget-object v0, p0, Lkel;->eUJ:Ljyw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lkel;->eUG:Ljrp;

    if-eqz v0, :cond_0

    .line 492
    const/4 v0, 0x1

    iget-object v1, p0, Lkel;->eUG:Ljrp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 494
    :cond_0
    iget v0, p0, Lkel;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 495
    const/4 v0, 0x2

    iget v1, p0, Lkel;->eUH:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 497
    :cond_1
    iget v0, p0, Lkel;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 498
    const/4 v0, 0x3

    iget-boolean v1, p0, Lkel;->eUI:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 500
    :cond_2
    iget-object v0, p0, Lkel;->eUJ:Ljyw;

    if-eqz v0, :cond_3

    .line 501
    const/4 v0, 0x4

    iget-object v1, p0, Lkel;->eUJ:Ljyw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 503
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 504
    return-void
.end method

.method public final bzx()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lkel;->eUH:I

    return v0
.end method

.method public final bzy()Z
    .locals 1

    .prologue
    .line 454
    iget-boolean v0, p0, Lkel;->eUI:Z

    return v0
.end method

.method public final jN(Z)Lkel;
    .locals 1

    .prologue
    .line 457
    iput-boolean p1, p0, Lkel;->eUI:Z

    .line 458
    iget v0, p0, Lkel;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkel;->aez:I

    .line 459
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 508
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 509
    iget-object v1, p0, Lkel;->eUG:Ljrp;

    if-eqz v1, :cond_0

    .line 510
    const/4 v1, 0x1

    iget-object v2, p0, Lkel;->eUG:Ljrp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    :cond_0
    iget v1, p0, Lkel;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 514
    const/4 v1, 0x2

    iget v2, p0, Lkel;->eUH:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 517
    :cond_1
    iget v1, p0, Lkel;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 518
    const/4 v1, 0x3

    iget-boolean v2, p0, Lkel;->eUI:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 521
    :cond_2
    iget-object v1, p0, Lkel;->eUJ:Ljyw;

    if-eqz v1, :cond_3

    .line 522
    const/4 v1, 0x4

    iget-object v2, p0, Lkel;->eUJ:Ljyw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_3
    return v0
.end method

.method public final tX(I)Lkel;
    .locals 1

    .prologue
    .line 438
    iput p1, p0, Lkel;->eUH:I

    .line 439
    iget v0, p0, Lkel;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkel;->aez:I

    .line 440
    return-object p0
.end method
