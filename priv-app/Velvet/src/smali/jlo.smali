.class public final Ljlo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private esw:Ljava/lang/String;

.field private esx:[Ljlp;

.field private esy:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10151
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10152
    iput v1, p0, Ljlo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlo;->esw:Ljava/lang/String;

    invoke-static {}, Ljlp;->bpp()[Ljlp;

    move-result-object v0

    iput-object v0, p0, Ljlo;->esx:[Ljlp;

    iput v1, p0, Ljlo;->esy:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljlo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlo;->eCz:I

    .line 10153
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10084
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlo;->esw:Ljava/lang/String;

    iget v0, p0, Ljlo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlo;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlo;->esx:[Ljlp;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlp;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljlo;->esx:[Ljlp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljlp;

    invoke-direct {v3}, Ljlp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljlo;->esx:[Ljlp;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljlp;

    invoke-direct {v3}, Ljlp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljlo;->esx:[Ljlp;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljlo;->esy:I

    iget v0, p0, Ljlo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 10168
    iget v0, p0, Ljlo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10169
    const/4 v0, 0x1

    iget-object v1, p0, Ljlo;->esw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 10171
    :cond_0
    iget-object v0, p0, Ljlo;->esx:[Ljlp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljlo;->esx:[Ljlp;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 10172
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljlo;->esx:[Ljlp;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 10173
    iget-object v1, p0, Ljlo;->esx:[Ljlp;

    aget-object v1, v1, v0

    .line 10174
    if-eqz v1, :cond_1

    .line 10175
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 10172
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10179
    :cond_2
    iget v0, p0, Ljlo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 10180
    const/4 v0, 0x3

    iget v1, p0, Ljlo;->esy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10182
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10183
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 10187
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10188
    iget v1, p0, Ljlo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10189
    const/4 v1, 0x1

    iget-object v2, p0, Ljlo;->esw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10192
    :cond_0
    iget-object v1, p0, Ljlo;->esx:[Ljlp;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljlo;->esx:[Ljlp;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 10193
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljlo;->esx:[Ljlp;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 10194
    iget-object v2, p0, Ljlo;->esx:[Ljlp;

    aget-object v2, v2, v0

    .line 10195
    if-eqz v2, :cond_1

    .line 10196
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10193
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 10201
    :cond_3
    iget v1, p0, Ljlo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 10202
    const/4 v1, 0x3

    iget v2, p0, Ljlo;->esy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10205
    :cond_4
    return v0
.end method
