.class public final Lfer;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public ag:I

.field private final mAlarmHelper:Ldjx;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mGmsLocationProvider:Lguh;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mLoginHelper:Lcrh;

.field private final mPrefController:Lchr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldjx;Lemp;Lguh;Leue;Lcrh;Lchr;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput v0, p0, Lfer;->ag:I

    .line 62
    iput-object p1, p0, Lfer;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lfer;->mAlarmHelper:Ldjx;

    .line 64
    iput-object p3, p0, Lfer;->mClock:Lemp;

    .line 65
    iput-object p4, p0, Lfer;->mGmsLocationProvider:Lguh;

    .line 66
    iput-object p5, p0, Lfer;->mGmsLocationReportingHelper:Leue;

    .line 67
    iput-object p6, p0, Lfer;->mLoginHelper:Lcrh;

    .line 68
    iput-object p7, p0, Lfer;->mPrefController:Lchr;

    .line 69
    return-void
.end method

.method private getState()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 102
    iget v0, p0, Lfer;->ag:I

    if-ne v0, v2, :cond_0

    .line 103
    iget-object v0, p0, Lfer;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "VEHICLE_ACTIVITY_STATE"

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfer;->ag:I

    .line 106
    :cond_0
    iget v0, p0, Lfer;->ag:I

    return v0
.end method

.method private iF(I)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 151
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.sidekick.main.location.GPS_TIMEOUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    iget-object v1, p0, Lfer;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0, p1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 154
    return-object v0
.end method

.method private setState(I)V
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lfer;->ag:I

    if-ne v0, p1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iput p1, p0, Lfer;->ag:I

    .line 114
    iget-object v0, p0, Lfer;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "VEHICLE_ACTIVITY_STATE"

    invoke-interface {v0, v1, p1}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public final ayN()V
    .locals 2

    .prologue
    .line 75
    const/high16 v0, 0x20000000

    invoke-direct {p0, v0}, Lfer;->iF(I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    .line 77
    iget-object v1, p0, Lfer;->mAlarmHelper:Ldjx;

    invoke-virtual {v1, v0}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lfer;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKz()Lcgs;

    .line 80
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfer;->setState(I)V

    .line 81
    return-void
.end method

.method public ayO()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 125
    invoke-virtual {p0}, Lfer;->ayN()V

    .line 133
    iget-object v0, p0, Lfer;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 134
    const-string v1, "Now Vehicle Exit"

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/location/reporting/UploadRequest;->a(Landroid/accounts/Account;Ljava/lang/String;J)Lbsg;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lbsg;->Q(J)Lbsg;

    move-result-object v0

    invoke-virtual {v0}, Lbsg;->AE()Lcom/google/android/gms/location/reporting/UploadRequest;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lfer;->mGmsLocationReportingHelper:Leue;

    invoke-virtual {v1, v0}, Leue;->b(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcgs;

    .line 139
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfer;->setState(I)V

    .line 140
    return-void
.end method

.method public final b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 84
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->Ab()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lfer;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lfer;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKy()Lcgs;

    iget-object v0, p0, Lfer;->mAlarmHelper:Ldjx;

    iget-object v1, p0, Lfer;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    const/high16 v1, 0x8000000

    invoke-direct {p0, v1}, Lfer;->iF(I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v6, v2, v3, v1}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    .line 88
    invoke-direct {p0, v6}, Lfer;->setState(I)V

    .line 90
    :cond_0
    return-void
.end method

.method public final i(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Lfer;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v1, 0x41700000    # 15.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lfer;->ayO()V

    .line 99
    :cond_0
    return-void
.end method
