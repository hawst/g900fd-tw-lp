.class public final Lcnq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# static fields
.field private static final bcU:Ljava/util/regex/Pattern;


# instance fields
.field private final bcV:Z

.field private final bcW:[Lcox;

.field private final mDebugFeatures:Lckw;

.field private final mHttpHelper:Ldkx;

.field private final mSettings:Lcke;

.field private final mUrlHelper:Lcpn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-string v0, " (\\d+)/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcnq;->bcU:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcke;Lcpn;Ldkx;Z[Lcox;Lckw;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p3, p0, Lcnq;->mHttpHelper:Ldkx;

    .line 80
    iput-object p2, p0, Lcnq;->mUrlHelper:Lcpn;

    .line 81
    iput-object p1, p0, Lcnq;->mSettings:Lcke;

    .line 82
    iput-boolean p4, p0, Lcnq;->bcV:Z

    .line 83
    iput-object p5, p0, Lcnq;->bcW:[Lcox;

    .line 84
    iput-object p6, p0, Lcnq;->mDebugFeatures:Lckw;

    .line 86
    return-void
.end method

.method private a(Ljje;Lijp;)V
    .locals 5
    .param p1    # Ljje;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lijp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 223
    iget-object v1, p0, Lcnq;->bcW:[Lcox;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 224
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {v3}, Lcox;->Ez()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 226
    iget-boolean v4, p0, Lcnq;->bcV:Z

    invoke-interface {v3, p1, v4}, Lcox;->a(Ljje;Z)V

    .line 223
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :cond_0
    iget-boolean v4, p0, Lcnq;->bcV:Z

    invoke-interface {v3, v4}, Lcox;->ci(Z)V

    goto :goto_1

    .line 231
    :cond_1
    return-void
.end method


# virtual methods
.method public final QI()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 134
    iget-boolean v0, p0, Lcnq;->bcV:Z

    if-nez v0, :cond_0

    .line 138
    invoke-static {}, Lenu;->auQ()V

    .line 141
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcnq;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ol()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "F/1 2/JT_oHF1-spc="

    .line 148
    :cond_1
    iget-object v1, p0, Lcnq;->mDebugFeatures:Lckw;

    invoke-virtual {v1}, Lckw;->Pd()Z

    move-result v1

    if-nez v1, :cond_2

    .line 149
    iget-object v1, p0, Lcnq;->mDebugFeatures:Lckw;

    iget-object v2, p0, Lcnq;->mSettings:Lcke;

    invoke-virtual {v1, v2}, Lckw;->a(Lcke;)V

    .line 151
    :cond_2
    iget-object v1, p0, Lcnq;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->hL(Ljava/lang/String;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 152
    new-instance v1, Ldlb;

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 157
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldlb;->fY(I)V

    .line 158
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldlb;->setUseCaches(Z)V

    .line 172
    iget-object v0, p0, Lcnq;->mHttpHelper:Ldkx;

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Ldkx;->d(Ldlb;I)Ldjy;

    move-result-object v1

    .line 175
    iget-object v2, v1, Ldjy;->mResponseHeaders:Ldyj;

    const-string v0, "ETag"

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "ETag"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ETag"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcnq;->mSettings:Lcke;

    invoke-interface {v2, v0}, Lcke;->hc(Ljava/lang/String;)V

    invoke-static {}, Lijp;->aXc()Lijr;

    move-result-object v2

    if-eqz v0, :cond_4

    const-string v3, "F/1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcnq;->bcU:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lijr;->bv(Ljava/lang/Object;)Lijr;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    instance-of v1, v0, Left;

    if-eqz v1, :cond_8

    .line 199
    check-cast v0, Left;

    .line 200
    invoke-virtual {v0}, Left;->getErrorCode()I

    move-result v0

    const/16 v1, 0x130

    if-ne v0, v1, :cond_8

    .line 201
    const-string v0, "Search.DownloadExperimentConfigTask"

    const-string v1, "Experiment config has not changed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_3
    :goto_1
    return-void

    .line 175
    :cond_4
    :try_start_1
    const-string v3, "Search.DownloadExperimentConfigTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expected ETag header starting with F/1 but got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_6

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". Clearing stored configurations."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {v2}, Lijr;->aXd()Lijp;

    move-result-object v0

    .line 176
    :goto_3
    iget-object v1, v1, Ldjy;->bAA:[B

    .line 178
    invoke-static {v1}, Ljje;->ao([B)Ljje;

    move-result-object v1

    .line 188
    invoke-direct {p0, v1, v0}, Lcnq;->a(Ljje;Lijp;)V

    .line 190
    iget-boolean v0, p0, Lcnq;->bcV:Z

    if-eqz v0, :cond_3

    .line 191
    const-string v0, "Search.DownloadExperimentConfigTask"

    const-string v1, "****RECEIVED A FORCE_RESTART: CRASHING THE APP ****"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_1

    .line 175
    :cond_6
    const-string v0, "null"

    goto :goto_2

    :cond_7
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lijp;->bu(Ljava/lang/Object;)Lijp;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_3

    .line 205
    :cond_8
    iget-boolean v0, p0, Lcnq;->bcV:Z

    if-eqz v0, :cond_3

    .line 206
    const-string v0, "Search.DownloadExperimentConfigTask"

    const-string v1, "Received a force_restart *and* /ajax/searchapp failed. Notify listeners to clear so we use baked in default values"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-direct {p0, v7, v7}, Lcnq;->a(Ljje;Lijp;)V

    goto :goto_1
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcnq;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final ub()Ljava/lang/Void;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lcnq;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->OF()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lcnq;->QI()V

    .line 97
    :cond_0
    return-object v1
.end method
