.class public Lema;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lerm;
.implements Leti;


# static fields
.field private static cfa:Lema;


# instance fields
.field private final atG:Leqo;

.field private final ceX:Ljava/util/concurrent/ScheduledExecutorService;

.field private final ceY:Ljava/util/concurrent/ExecutorService;

.field private final ceZ:Ljava/util/List;

.field private final mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(II)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x1

    const/16 v7, 0xa

    const/4 v6, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lema;->ceZ:Ljava/util/List;

    .line 81
    new-instance v0, Leoe;

    invoke-direct {v0}, Leoe;-><init>()V

    iput-object v0, p0, Lema;->atG:Leqo;

    .line 91
    const-string v0, "User-Facing Non-Blocking"

    const/4 v1, 0x5

    invoke-static {v0, v1, v6, v6}, Lema;->a(Ljava/lang/String;IZI)Lemw;

    move-result-object v0

    .line 93
    const-string v1, "User-Facing Blocking"

    invoke-static {v1, v6}, Lema;->w(Ljava/lang/String;I)Lemw;

    move-result-object v1

    .line 95
    const-string v2, "Background Non-Blocking"

    invoke-static {v2, v8, v5, v7}, Lema;->a(Ljava/lang/String;IZI)Lemw;

    move-result-object v2

    .line 97
    const-string v3, "Background Blocking"

    invoke-static {v3, v7}, Lema;->w(Ljava/lang/String;I)Lemw;

    move-result-object v3

    .line 100
    const/4 v4, 0x4

    new-array v4, v4, [Lemw;

    aput-object v0, v4, v6

    aput-object v1, v4, v5

    aput-object v2, v4, v8

    const/4 v5, 0x3

    aput-object v3, v4, v5

    invoke-direct {p0, v4}, Lema;->a([Lemw;)V

    .line 101
    new-instance v4, Lems;

    invoke-direct {v4, v0, v1, v2, v3}, Lems;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v4, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    .line 105
    iput-object v2, p0, Lema;->mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 108
    new-instance v0, Leoa;

    const-string v1, "BackgroundExecutorService"

    invoke-direct {v0, v1, v7, v6}, Leoa;-><init>(Ljava/lang/String;IZ)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lema;->ceY:Ljava/util/concurrent/ExecutorService;

    .line 110
    return-void
.end method

.method public constructor <init>(Leqo;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lema;->ceZ:Ljava/util/List;

    .line 70
    iput-object p1, p0, Lema;->atG:Leqo;

    .line 71
    iput-object p2, p0, Lema;->mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 72
    iput-object p3, p0, Lema;->ceY:Ljava/util/concurrent/ExecutorService;

    .line 73
    iput-object p4, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    .line 74
    return-void
.end method

.method private static final a(Ljava/lang/String;IZI)Lemw;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Leoa;

    invoke-direct {v0, p0, p3}, Leoa;-><init>(Ljava/lang/String;I)V

    .line 115
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, p2, v1}, Lemv;->a(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZ)Lemw;

    move-result-object v0

    return-object v0
.end method

.method private varargs a([Lemw;)V
    .locals 5

    .prologue
    .line 223
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 224
    iget-object v3, p0, Lema;->ceZ:Ljava/util/List;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    return-void
.end method

.method public static declared-synchronized aur()Lema;
    .locals 4

    .prologue
    .line 59
    const-class v1, Lema;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lema;->cfa:Lema;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lema;

    const/4 v2, 0x5

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, Lema;-><init>(II)V

    sput-object v0, Lema;->cfa:Lema;

    .line 62
    :cond_0
    sget-object v0, Lema;->cfa:Lema;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static final w(Ljava/lang/String;I)Lemw;
    .locals 2

    .prologue
    .line 121
    new-instance v0, Leoa;

    invoke-direct {v0, p0, p1}, Leoa;-><init>(Ljava/lang/String;I)V

    .line 122
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lemv;->a(Ljava/lang/String;Ljava/util/concurrent/ThreadFactory;Z)Lemw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic Ct()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lema;->atG:Leqo;

    return-object v0
.end method

.method public final a(Leri;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 184
    return-void
.end method

.method public final a(Leri;J)V
    .locals 4

    .prologue
    .line 188
    iget-object v0, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x1388

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p1, v2, v3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 189
    return-void
.end method

.method public final a(Lesk;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lema;->atG:Leqo;

    invoke-interface {v0, p1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 169
    return-void
.end method

.method public final a(Lesk;J)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lema;->atG:Leqo;

    invoke-interface {v0, p1, p2, p3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 174
    return-void
.end method

.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 212
    const-string v0, "AsyncServices state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 213
    invoke-static {p1}, Lemv;->d(Letj;)V

    .line 214
    iget-object v0, p0, Lema;->ceZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 215
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemw;

    .line 216
    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    goto :goto_0

    .line 220
    :cond_1
    return-void
.end method

.method public final aus()Leqo;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lema;->atG:Leqo;

    return-object v0
.end method

.method public final aut()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lema;->mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public final auu()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lema;->ceY:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public final auv()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public final b(Lesk;)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lema;->atG:Leqo;

    invoke-interface {v0, p1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 179
    return-void
.end method

.method public gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 195
    invoke-static {p1, v2}, Lemv;->q(Ljava/lang/String;Z)Lemw;

    move-result-object v0

    .line 197
    const/4 v1, 0x1

    new-array v1, v1, [Lemw;

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lema;->a([Lemw;)V

    .line 198
    return-object v0
.end method

.method public final ig(I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x7d0

    .line 232
    iget-object v0, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 233
    iget-object v0, p0, Lema;->ceY:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 236
    iget-object v0, p0, Lema;->ceX:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v5, v1}, Ljava/util/concurrent/ScheduledExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 237
    iget-object v0, p0, Lema;->ceY:Ljava/util/concurrent/ExecutorService;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v5, v1}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 240
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Livb;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Livb;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 241
    iget-object v1, p0, Lema;->atG:Leqo;

    invoke-interface {v1, v0}, Leqo;->j(Ljava/lang/Runnable;)V

    .line 244
    iget-object v1, p0, Lema;->atG:Leqo;

    new-instance v2, Lemb;

    const-string v3, "Wait for idle before shutdown"

    invoke-direct {v2, p0, v3}, Lemb;-><init>(Lema;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 250
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    .line 251
    return-void
.end method

.method public final ld(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 3

    .prologue
    .line 205
    invoke-static {p1}, Lemv;->lg(Ljava/lang/String;)Lemw;

    move-result-object v0

    .line 206
    const/4 v1, 0x1

    new-array v1, v1, [Lemw;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lema;->a([Lemw;)V

    .line 207
    return-object v0
.end method
