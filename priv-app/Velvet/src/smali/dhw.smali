.class public final Ldhw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldhu;


# instance fields
.field private final byr:Ljava/util/Map;

.field private final bys:Ldib;

.field private byt:Lijp;

.field private final byu:Ldhx;

.field private final mContext:Landroid/content/Context;

.field private final mObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>(Ldib;Landroid/content/Context;)V
    .locals 1
    .param p1    # Ldib;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 47
    new-instance v0, Ldhx;

    invoke-direct {v0}, Ldhx;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Ldhw;-><init>(Ldib;Landroid/content/Context;Ldhx;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Ldib;Landroid/content/Context;Ldhx;)V
    .locals 1
    .param p1    # Ldib;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Ldhw;->mObservable:Landroid/database/DataSetObservable;

    .line 53
    iput-object p1, p0, Ldhw;->bys:Ldib;

    .line 54
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ldhw;->byr:Ljava/util/Map;

    .line 55
    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v0

    iput-object v0, p0, Ldhw;->byt:Lijp;

    .line 56
    iput-object p2, p0, Ldhw;->mContext:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Ldhw;->byu:Ldhx;

    .line 58
    return-void
.end method

.method private acc()V
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 125
    iget-object v1, p0, Ldhw;->byr:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 126
    iget-object v2, p0, Ldhw;->byt:Lijp;

    invoke-interface {v1, v2}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    invoke-static {v1}, Lijp;->G(Ljava/util/Collection;)Lijp;

    move-result-object v0

    iput-object v0, p0, Ldhw;->byt:Lijp;

    .line 128
    const/4 v0, 0x1

    .line 131
    :cond_0
    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Ldhw;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 135
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcke;Lchk;Ldgm;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    .line 191
    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {p2}, Lchk;->GS()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 192
    iget-object v0, p0, Ldhw;->byt:Lijp;

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 196
    invoke-virtual {v0}, Ldht;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 197
    iget-object v1, p0, Ldhw;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    invoke-interface {p1, v0}, Lcke;->d(Ldgb;)Z

    move-result v1

    .line 208
    invoke-interface {p1, v0}, Lcke;->b(Ldgb;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 209
    invoke-interface {p1, v0}, Lcke;->c(Ldgb;)Z

    move-result v6

    .line 210
    if-nez v6, :cond_1

    move v1, v2

    .line 226
    :cond_1
    invoke-virtual {v0}, Ldht;->isEnabled()Z

    move-result v7

    if-eq v7, v6, :cond_3

    .line 227
    invoke-virtual {p3, v5, v6, v8}, Ldgm;->a(Ljava/lang/String;ZLjava/lang/Runnable;)V

    .line 228
    invoke-virtual {v0, v6}, Ldht;->setEnabled(Z)V

    .line 235
    :cond_2
    :goto_1
    if-nez v1, :cond_0

    .line 236
    iget-object v1, p0, Ldhw;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 237
    iget-object v6, p0, Ldhw;->byu:Ldhx;

    invoke-static {v1, v0}, Ldhx;->a(Landroid/content/pm/PackageManager;Ldht;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 238
    invoke-virtual {v0}, Ldht;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 240
    invoke-virtual {p3, v5, v2, v8}, Ldgm;->a(Ljava/lang/String;ZLjava/lang/Runnable;)V

    .line 241
    invoke-virtual {v0, v2}, Ldht;->setEnabled(Z)V

    .line 242
    invoke-interface {p1, v0, v2}, Lcke;->a(Ldgb;Z)V

    goto :goto_0

    .line 230
    :cond_3
    invoke-interface {p1, v0}, Lcke;->a(Ldht;)V

    goto :goto_1

    .line 252
    :cond_4
    invoke-interface {p1}, Lcke;->NL()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    .line 254
    :goto_2
    invoke-virtual {v0}, Ldht;->isEnabled()Z

    move-result v6

    if-eq v6, v1, :cond_0

    .line 255
    invoke-virtual {p3, v5, v1, v8}, Ldgm;->a(Ljava/lang/String;ZLjava/lang/Runnable;)V

    .line 256
    invoke-virtual {v0, v1}, Ldht;->setEnabled(Z)V

    .line 257
    invoke-interface {p1, v0, v1}, Lcke;->a(Ldgb;Z)V

    goto :goto_0

    .line 252
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 266
    :cond_6
    return-void
.end method

.method public final declared-synchronized a(Ldhr;Lemy;)V
    .locals 5

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Ldhw;->byt:Lijp;

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    invoke-virtual {v0}, Ldht;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ldht;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ldht;->acb()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Ldhr;->ao(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 82
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    invoke-static {}, Ldib;->acd()Ldid;

    move-result-object v0

    invoke-interface {p2, v0}, Lemy;->aj(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :goto_1
    monitor-exit p0

    return-void

    .line 85
    :cond_2
    :try_start_2
    iget-object v0, p0, Ldhw;->bys:Ldib;

    invoke-virtual {v0, v1, p1, p2}, Ldib;->a(Ljava/util/Collection;Ldhr;Lemy;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 150
    const-string v0, "IcingSources"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Ldhw;->byt:Lijp;

    invoke-virtual {p1, v0}, Letj;->f(Ljava/lang/Iterable;)V

    .line 152
    return-void
.end method

.method public final declared-synchronized abS()Ljava/util/Collection;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldhw;->byt:Lijp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clear()V
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldhw;->byr:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 119
    invoke-direct {p0}, Ldhw;->acc()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j(Ljava/util/Collection;)V
    .locals 4

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 92
    iget-object v2, p0, Ldhw;->byr:Ljava/util/Map;

    invoke-virtual {v0}, Ldht;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 95
    :cond_0
    :try_start_1
    invoke-direct {p0}, Ldhw;->acc()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized jF(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldhw;->byr:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    invoke-virtual {v0}, Ldht;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_1
    :try_start_1
    invoke-direct {p0}, Ldhw;->acc()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    monitor-exit p0

    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ldhw;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Ldhw;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 75
    return-void
.end method
