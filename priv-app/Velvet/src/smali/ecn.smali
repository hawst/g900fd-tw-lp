.class public abstract Lecn;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Lecm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 22
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p0, p0, v0}, Lecn;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static G(Landroid/os/IBinder;)Lecm;
    .locals 2

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    .line 33
    :cond_0
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_1

    instance-of v1, v0, Lecm;

    if-eqz v1, :cond_1

    .line 35
    check-cast v0, Lecm;

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Leco;

    invoke-direct {v0, p0}, Leco;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 45
    sparse-switch p1, :sswitch_data_0

    .line 286
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 49
    :sswitch_0
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :sswitch_1
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lecn;->UK()V

    goto :goto_0

    .line 60
    :sswitch_2
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 67
    :goto_1
    invoke-virtual {p0, v3, v4, v0}, Lecn;->d(IIZ)V

    goto :goto_0

    :cond_0
    move v0, v2

    .line 66
    goto :goto_1

    .line 72
    :sswitch_3
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 80
    :goto_2
    invoke-virtual {p0, v0}, Lecn;->z(Lcom/google/android/shared/search/Query;)V

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 78
    goto :goto_2

    .line 85
    :sswitch_4
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-virtual {p0, v0, v2}, Lecn;->e(ILjava/lang/String;)V

    goto :goto_0

    .line 95
    :sswitch_5
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 98
    sget-object v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    .line 103
    :goto_3
    invoke-virtual {p0, v0}, Lecn;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V

    goto :goto_0

    :cond_2
    move-object v0, v3

    .line 101
    goto :goto_3

    .line 108
    :sswitch_6
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 111
    invoke-virtual {p0, v0}, Lecn;->fv(I)V

    goto :goto_0

    .line 116
    :sswitch_7
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 119
    invoke-virtual {p0, v0}, Lecn;->en(I)V

    goto/16 :goto_0

    .line 124
    :sswitch_8
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {p0, v0, v2}, Lecn;->x(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    :sswitch_9
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-virtual {p0, v0}, Lecn;->gb(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    :sswitch_a
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 145
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    move-object v4, v0

    .line 151
    :goto_4
    sget-object v0, Lcom/google/android/search/shared/api/SuggestionsGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v5

    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v2, v1

    .line 155
    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 156
    sget-object v0, Lcom/google/android/shared/search/SuggestionLogInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 161
    :goto_5
    invoke-virtual {p0, v4, v5, v2, v0}, Lecn;->a(Lcom/google/android/shared/search/Query;Ljava/util/List;ZLcom/google/android/shared/search/SuggestionLogInfo;)V

    goto/16 :goto_0

    :cond_4
    move-object v4, v3

    .line 148
    goto :goto_4

    :cond_5
    move-object v0, v3

    .line 159
    goto :goto_5

    .line 166
    :sswitch_b
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 168
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 169
    invoke-virtual {p0, v0}, Lecn;->a([Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 174
    :sswitch_c
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 177
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 182
    :goto_6
    invoke-virtual {p0, v0}, Lecn;->w(Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v3

    .line 180
    goto :goto_6

    .line 187
    :sswitch_d
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 190
    invoke-virtual {p0, v0}, Lecn;->eo(I)V

    goto/16 :goto_0

    .line 195
    :sswitch_e
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 198
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    move-object v2, v0

    .line 204
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 205
    sget-object v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    move-object v4, v0

    .line 211
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 212
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 217
    :goto_9
    invoke-virtual {p0, v2, v4, v0}, Lecn;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    goto/16 :goto_0

    :cond_7
    move-object v2, v3

    .line 201
    goto :goto_7

    :cond_8
    move-object v4, v3

    .line 208
    goto :goto_8

    :cond_9
    move-object v0, v3

    .line 215
    goto :goto_9

    .line 222
    :sswitch_f
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 225
    invoke-virtual {p0, v0}, Lecn;->j([B)V

    goto/16 :goto_0

    .line 230
    :sswitch_10
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 233
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 239
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->createFloatArray()[F

    move-result-object v3

    .line 242
    invoke-virtual {p0, v0, v2, v3}, Lecn;->a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V

    goto/16 :goto_0

    :cond_a
    move-object v0, v3

    .line 236
    goto :goto_a

    .line 247
    :sswitch_11
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 250
    sget-object v0, Lcom/google/android/shared/speech/HotwordResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/HotwordResult;

    .line 255
    :goto_b
    invoke-virtual {p0, v0}, Lecn;->a(Lcom/google/android/shared/speech/HotwordResult;)V

    goto/16 :goto_0

    :cond_b
    move-object v0, v3

    .line 253
    goto :goto_b

    .line 260
    :sswitch_12
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lecn;->CV()V

    goto/16 :goto_0

    .line 266
    :sswitch_13
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p0}, Lecn;->ua()V

    goto/16 :goto_0

    .line 272
    :sswitch_14
    const-string v0, "com.google.android.search.shared.service.ISearchServiceUiCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 276
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 277
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 282
    :goto_c
    invoke-virtual {p0, v2, v0}, Lecn;->c(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_c
    move-object v0, v3

    .line 280
    goto :goto_c

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
