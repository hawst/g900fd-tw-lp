.class public final Leqh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(ILjava/io/InputStream;)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 444
    and-int/lit16 v0, p0, 0x80

    if-nez v0, :cond_1

    .line 467
    :cond_0
    :goto_0
    return p0

    .line 448
    :cond_1
    and-int/lit8 p0, p0, 0x7f

    .line 449
    const/4 v0, 0x7

    .line 450
    :goto_1
    const/16 v1, 0x20

    if-ge v0, v1, :cond_4

    .line 451
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 452
    if-ne v1, v3, :cond_2

    .line 453
    new-instance v0, Leqk;

    const-string v1, "EOF before 32 bits"

    invoke-direct {v0, v1}, Leqk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455
    :cond_2
    and-int/lit8 v2, v1, 0x7f

    shl-int/2addr v2, v0

    or-int/2addr p0, v2

    .line 456
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    .line 450
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 461
    :cond_3
    add-int/lit8 v0, v0, 0x7

    :cond_4
    const/16 v1, 0x40

    if-ge v0, v1, :cond_6

    .line 462
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 463
    if-ne v1, v3, :cond_5

    .line 464
    new-instance v0, Leqk;

    const-string v1, "EOF before 64 bits"

    invoke-direct {v0, v1}, Leqk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466
    :cond_5
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_3

    goto :goto_0

    .line 470
    :cond_6
    new-instance v0, Leqk;

    const-string v1, "Finished without valid varint"

    invoke-direct {v0, v1}, Leqk;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 362
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    .line 363
    if-nez v2, :cond_0

    move-object v0, v1

    .line 374
    :goto_0
    return-object v0

    .line 366
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v0, v2}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 367
    :catch_0
    move-exception v0

    .line 368
    const-string v2, "ProtoUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error instantiating proto: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    :goto_1
    move-object v0, v1

    .line 374
    goto :goto_0

    .line 369
    :catch_1
    move-exception v0

    .line 370
    const-string v2, "ProtoUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error instantiating proto: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 371
    :catch_2
    move-exception v0

    .line 372
    const-string v2, "ProtoUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error parsing proto extra: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static a(Ljsr;Ljava/io/InputStream;)Ljsr;
    .locals 1

    .prologue
    .line 162
    invoke-static {p1}, Leoo;->j(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-static {p0, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljsr;Ljava/lang/String;)Ljsr;
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x3

    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {p0, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 118
    return-object p0
.end method

.method public static final a(Ljsr;[B)Ljsr;
    .locals 3
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 136
    const/16 v0, 0x8

    :try_start_0
    invoke-static {p1, v0}, Landroid/util/Base64;->decode([BI)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 141
    invoke-static {p0, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 142
    return-object p0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    const-string v1, "ProtoUtils"

    const-string v2, "Could not decode base64 string"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 139
    new-instance v1, Lefs;

    const v2, 0x30015

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V
    .locals 1
    .param p2    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346
    if-eqz p2, :cond_0

    .line 347
    invoke-static {p2}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 349
    :cond_0
    return-void
.end method

.method public static a([Ljava/lang/Object;Lifw;)[Ljava/lang/Object;
    .locals 5

    .prologue
    .line 264
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 265
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 266
    invoke-interface {p1, v3}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 267
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 210
    const/4 v0, 0x1

    invoke-static {p0, v0}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 211
    array-length v1, p0

    aput-object p1, v0, v1

    .line 212
    return-object v0
.end method

.method public static a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 233
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-object p0

    .line 237
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {p0, v0}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    .line 238
    array-length v0, p0

    .line 239
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 240
    aput-object v3, v1, v0

    .line 241
    add-int/lit8 v0, v0, 0x1

    .line 242
    goto :goto_1

    :cond_2
    move-object p0, v1

    .line 243
    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 5

    .prologue
    .line 248
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-object p0

    .line 252
    :cond_1
    array-length v0, p1

    invoke-static {p0, v0}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 253
    array-length v2, p0

    .line 254
    array-length v3, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, p1, v1

    .line 255
    aput-object v4, v0, v2

    .line 256
    add-int/lit8 v2, v2, 0x1

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object p0, v0

    .line 258
    goto :goto_0
.end method

.method public static b(Ljsr;[B)Ljsr;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 183
    if-nez p1, :cond_0

    .line 184
    const/4 p0, 0x0

    .line 191
    :goto_0
    return-object p0

    .line 187
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    const-string v1, "ProtoUtils"

    const-string v2, "Error parsing proto"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Ljsr;Ljsr;)V
    .locals 3

    .prologue
    .line 175
    :try_start_0
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-static {p0, v0}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_0
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 177
    const-string v1, "ProtoUtils"

    const-string v2, "Re-encoding protobuf should not cause exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static b([II)[I
    .locals 2

    .prologue
    .line 196
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    .line 197
    array-length v1, p0

    aput p1, v0, v1

    .line 198
    return-object v0
.end method

.method public static b([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 225
    array-length v0, p0

    add-int/2addr v0, p1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 216
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lipz;->c([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    aput-object p1, v0, v3

    const/4 v1, 0x1

    array-length v2, p0

    invoke-static {p0, v3, v0, v1, v2}, Liqb;->a([Ljava/lang/Object;I[Ljava/lang/Object;II)V

    return-object v0
.end method

.method public static c(Ljsr;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Ljsr;Ljsr;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    if-ne p0, p1, :cond_1

    .line 300
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_4

    .line 287
    :cond_2
    if-nez p0, :cond_3

    if-eqz p1, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 289
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v0, v2, :cond_5

    move v0, v1

    .line 290
    goto :goto_0

    .line 292
    :cond_5
    invoke-virtual {p0}, Ljsr;->buf()I

    move-result v0

    .line 293
    invoke-virtual {p1}, Ljsr;->buf()I

    move-result v2

    if-eq v2, v0, :cond_6

    move v0, v1

    .line 294
    goto :goto_0

    .line 296
    :cond_6
    new-array v2, v0, [B

    .line 297
    new-array v3, v0, [B

    .line 298
    invoke-static {p0, v2, v1, v0}, Ljsr;->a(Ljsr;[BII)V

    .line 299
    invoke-static {p1, v3, v1, v0}, Ljsr;->a(Ljsr;[BII)V

    .line 300
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(Ljsr;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 71
    if-nez p0, :cond_0

    .line 72
    const-string v0, "null"

    .line 75
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[Slow toString() output omitted; L.DBG is false]"

    goto :goto_0
.end method

.method public static d(Ljsr;Ljsr;)Ljsr;
    .locals 2
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 327
    if-nez p0, :cond_0

    .line 328
    const/4 v0, 0x0

    .line 331
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-static {p1, v0}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 332
    :catch_0
    move-exception v0

    .line 334
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final e(Ljsr;)[B
    .locals 2
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 152
    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljsr;)Ljsr;
    .locals 2
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306
    if-nez p0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 318
    :goto_0
    return-object v0

    .line 311
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 318
    invoke-static {p0, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    goto :goto_0

    .line 312
    :catch_0
    move-exception v0

    .line 314
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 315
    :catch_1
    move-exception v0

    .line 316
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
