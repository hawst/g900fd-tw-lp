.class public final Likm;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/lang/Iterable;Lifw;)Z
    .locals 1

    .prologue
    .line 183
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 184
    check-cast p0, Ljava/util/List;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifw;

    invoke-static {p0, v0}, Likm;->a(Ljava/util/List;Lifw;)Z

    move-result v0

    .line 187
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Likr;->a(Ljava/util/Iterator;Lifw;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lifw;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 194
    move v0, v1

    move v2, v1

    .line 197
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_6

    .line 198
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 199
    invoke-interface {p1, v4}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 200
    if-le v2, v0, :cond_0

    .line 202
    :try_start_0
    invoke-interface {p0, v0, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 197
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 204
    :catch_0
    move-exception v1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-le v1, v2, :cond_3

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v2, -0x1

    :goto_2
    if-lt v1, v0, :cond_4

    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_4
    move v1, v3

    .line 214
    :cond_5
    :goto_3
    return v1

    .line 213
    :cond_6
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p0, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 214
    if-eq v2, v0, :cond_5

    move v1, v3

    goto :goto_3
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 293
    invoke-static {p0}, Likm;->u(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 294
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {p1, v1}, Lipz;->a(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v1

    .line 295
    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 704
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    new-instance v0, Likp;

    invoke-direct {v0, p0, p1}, Likp;-><init>(Ljava/lang/Iterable;Lifg;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 580
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    new-instance v0, Liko;

    invoke-direct {v0, p0, p1}, Liko;-><init>(Ljava/lang/Iterable;Lifw;)V

    return-object v0
.end method

.method public static t(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 268
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Likr;->e(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static u(Ljava/lang/Iterable;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 315
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/Collection;

    :goto_0
    return-object p0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object p0

    goto :goto_0
.end method

.method public static v(Ljava/lang/Iterable;)Z
    .locals 1

    .prologue
    .line 1049
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    .line 1050
    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    .line 1052
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
