.class public final Lcuk;
.super Lcuo;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 31
    const-string v0, "contacts"

    const-string v1, "contact_id"

    invoke-direct {p0, v0, v1}, Lcuo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected final a(Lben;)Lbem;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 36
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v3

    .line 37
    const-string v0, "score"

    invoke-virtual {p1, v0}, Lben;->eY(Ljava/lang/String;)Lben;

    move-result-object v0

    const-string v4, "lookup_key"

    new-instance v5, Lbcg;

    const-string v6, "lookup_key"

    invoke-direct {v5, v6}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    move-result-object v0

    const-string v4, "icon_uri"

    new-instance v5, Lbcg;

    const-string v6, "icon_uri"

    invoke-direct {v5, v6}, Lbcg;-><init>(Ljava/lang/String;)V

    iput-boolean v1, v5, Lbcg;->awA:Z

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    move-result-object v0

    const-string v4, "display_name"

    new-instance v5, Lbcg;

    const-string v6, "name"

    invoke-direct {v5, v6}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gl()I

    move-result v6

    iput v6, v5, Lbcg;->awB:I

    invoke-virtual {v3}, Lchk;->Gu()Z

    move-result v6

    iput-boolean v6, v5, Lbcg;->awC:Z

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 52
    const-string v0, "givennames"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->eO(Ljava/lang/String;)Lbcg;

    move-result-object v0

    invoke-virtual {v3}, Lchk;->Gm()I

    move-result v4

    iput v4, v0, Lbcg;->awB:I

    invoke-virtual {v3}, Lchk;->Gw()Z

    move-result v4

    iput-boolean v4, v0, Lbcg;->awC:Z

    .line 56
    invoke-virtual {v3}, Lchk;->Gv()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 57
    invoke-static {}, Lbcl;->xl()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v4

    invoke-virtual {v0, v4}, Lbcg;->a(Lcom/google/android/gms/appdatasearch/Feature;)Lbcg;

    .line 59
    :cond_0
    const-string v4, "given_names"

    invoke-virtual {v0}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 60
    const-string v4, "emails"

    new-instance v5, Lbcg;

    const-string v0, "email"

    invoke-direct {v5, v0}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Go()I

    move-result v0

    iput v0, v5, Lbcg;->awB:I

    const-string v0, "rfc822"

    iput-object v0, v5, Lbcg;->awz:Ljava/lang/String;

    const-string v0, "\u0085"

    iput-object v0, v5, Lbcg;->awD:Ljava/lang/String;

    invoke-virtual {v3}, Lchk;->Gz()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, v5, Lbcg;->awA:Z

    invoke-virtual {v3}, Lchk;->GA()Z

    move-result v0

    iput-boolean v0, v5, Lbcg;->awC:Z

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 68
    const-string v0, "nickname"

    new-instance v4, Lbcg;

    const-string v5, "nickname"

    invoke-direct {v4, v5}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gp()I

    move-result v5

    iput v5, v4, Lbcg;->awB:I

    const-string v5, "\u0085"

    iput-object v5, v4, Lbcg;->awD:Ljava/lang/String;

    invoke-virtual {v3}, Lchk;->GB()Z

    move-result v5

    iput-boolean v5, v4, Lbcg;->awC:Z

    invoke-static {}, Lbcl;->xl()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v5

    invoke-virtual {v4, v5}, Lbcg;->a(Lcom/google/android/gms/appdatasearch/Feature;)Lbcg;

    move-result-object v4

    invoke-virtual {v4}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 77
    invoke-virtual {v3}, Lchk;->GC()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    const-string v0, "note"

    new-instance v4, Lbcg;

    const-string v5, "note"

    invoke-direct {v4, v5}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gq()I

    move-result v5

    iput v5, v4, Lbcg;->awB:I

    const-string v5, "\u0085"

    iput-object v5, v4, Lbcg;->awD:Ljava/lang/String;

    invoke-virtual {v3}, Lchk;->GD()Z

    move-result v5

    iput-boolean v5, v4, Lbcg;->awC:Z

    invoke-virtual {v4}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 85
    :cond_1
    invoke-virtual {v3}, Lchk;->GE()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    const-string v0, "organization"

    new-instance v4, Lbcg;

    const-string v5, "organization"

    invoke-direct {v4, v5}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gr()I

    move-result v5

    iput v5, v4, Lbcg;->awB:I

    const-string v5, "\u0085"

    iput-object v5, v4, Lbcg;->awD:Ljava/lang/String;

    invoke-virtual {v3}, Lchk;->GF()Z

    move-result v5

    iput-boolean v5, v4, Lbcg;->awC:Z

    invoke-virtual {v4}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 93
    :cond_2
    const-string v4, "phone_numbers"

    new-instance v5, Lbcg;

    const-string v0, "number"

    invoke-direct {v5, v0}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gs()I

    move-result v0

    iput v0, v5, Lbcg;->awB:I

    const-string v0, "\u0085"

    iput-object v0, v5, Lbcg;->awD:Ljava/lang/String;

    invoke-virtual {v3}, Lchk;->GG()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, v5, Lbcg;->awA:Z

    invoke-virtual {v3}, Lchk;->GH()Z

    move-result v0

    iput-boolean v0, v5, Lbcg;->awC:Z

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 100
    const-string v4, "postal_address"

    new-instance v5, Lbcg;

    const-string v0, "address"

    invoke-direct {v5, v0}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gt()I

    move-result v0

    iput v0, v5, Lbcg;->awB:I

    const-string v0, "\u0085"

    iput-object v0, v5, Lbcg;->awD:Ljava/lang/String;

    invoke-virtual {v3}, Lchk;->GI()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, v5, Lbcg;->awA:Z

    invoke-virtual {v3}, Lchk;->GJ()Z

    move-result v0

    iput-boolean v0, v5, Lbcg;->awC:Z

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 107
    const-string v4, "phonetic_name"

    new-instance v5, Lbcg;

    const-string v0, "phoneticname"

    invoke-direct {v5, v0}, Lbcg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lchk;->Gn()I

    move-result v0

    iput v0, v5, Lbcg;->awB:I

    invoke-virtual {v3}, Lchk;->Gx()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, v5, Lbcg;->awA:Z

    invoke-virtual {v3}, Lchk;->Gy()Z

    move-result v0

    iput-boolean v0, v5, Lbcg;->awC:Z

    invoke-virtual {v5}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    .line 113
    const-string v0, "text1"

    const v3, 0x7f0a0661

    invoke-virtual {p1, v0, v3}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v3, "text2"

    const v4, 0x7f0a0662

    invoke-virtual {v0, v3, v4}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v3, "intent_action"

    const v4, 0x7f0a0663

    invoke-virtual {v0, v3, v4}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v3, "intent_data"

    const v4, 0x7f0a0664

    invoke-virtual {v0, v3, v4}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v3, "icon"

    const v4, 0x7f0a0665

    invoke-virtual {v0, v3, v4}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    iput-boolean v2, v0, Lben;->axZ:Z

    const-string v2, "2"

    iput-object v2, v0, Lben;->axW:Ljava/lang/String;

    iput-boolean v1, v0, Lben;->aya:Z

    invoke-virtual {v0}, Lben;->xN()Lbem;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v2

    .line 60
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 93
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 100
    goto :goto_2

    :cond_6
    move v0, v2

    .line 107
    goto :goto_3
.end method
