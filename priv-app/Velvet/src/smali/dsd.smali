.class public final Ldsd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldtt;


# instance fields
.field private synthetic bIp:Lcom/google/android/search/searchplate/SearchPlate;


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/SearchPlate;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    iget-object v1, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, v1, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->afJ()Z

    move-result v1

    invoke-interface {v0, p1, p2, v1}, Ldsj;->a(Ljava/lang/CharSequence;IZ)V

    .line 335
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0}, Ldsj;->afr()V

    .line 358
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0, p2}, Ldsj;->dZ(Z)V

    .line 367
    :cond_0
    return-void
.end method

.method public final afk()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afJ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0}, Ldsj;->afn()V

    .line 342
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;II)V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0, p1, p2, p3}, Ldsj;->b(Ljava/lang/CharSequence;II)V

    .line 349
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0}, Ldsj;->afs()V

    .line 374
    :cond_0
    return-void
.end method

.method public final dY(Z)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0, p1}, Ldsj;->dY(Z)V

    .line 388
    :cond_0
    return-void
.end method

.method public final q(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Ldsd;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    invoke-interface {v0}, Ldsj;->aft()V

    .line 381
    :cond_0
    return-void
.end method
