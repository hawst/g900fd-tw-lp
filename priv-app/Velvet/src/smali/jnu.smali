.class public final Ljnu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afg:J

.field private aiK:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private alT:Ljava/lang/String;

.field public dOo:[Ljava/lang/String;

.field private evF:I

.field private evG:Ljava/lang/String;

.field private evH:I

.field private evI:Ljava/lang/String;

.field private evJ:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2453
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2454
    iput v2, p0, Ljnu;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnu;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnu;->alT:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljnu;->dOo:[Ljava/lang/String;

    iput v2, p0, Ljnu;->evF:I

    const-string v0, ""

    iput-object v0, p0, Ljnu;->ajk:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljnu;->afg:J

    const-string v0, ""

    iput-object v0, p0, Ljnu;->evG:Ljava/lang/String;

    iput v2, p0, Ljnu;->evH:I

    const-string v0, ""

    iput-object v0, p0, Ljnu;->evI:Ljava/lang/String;

    iput-boolean v2, p0, Ljnu;->evJ:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljnu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnu;->eCz:I

    .line 2455
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnu;->aiK:Ljava/lang/String;

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnu;->alT:Ljava/lang/String;

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnu;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljnu;->dOo:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljnu;->dOo:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljnu;->dOo:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljnu;->dOo:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnu;->ajk:Ljava/lang/String;

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnu;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljnu;->afg:J

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljnu;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnu;->evG:Ljava/lang/String;

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljnu;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljnu;->evJ:Z

    iget v0, p0, Ljnu;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljnu;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljnu;->evH:I

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljnu;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnu;->evI:Ljava/lang/String;

    iget v0, p0, Ljnu;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljnu;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljnu;->evF:I

    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnu;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 2477
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2478
    const/4 v0, 0x1

    iget-object v1, p0, Ljnu;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2480
    :cond_0
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2481
    const/4 v0, 0x2

    iget-object v1, p0, Ljnu;->alT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2483
    :cond_1
    iget-object v0, p0, Ljnu;->dOo:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljnu;->dOo:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2484
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljnu;->dOo:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 2485
    iget-object v1, p0, Ljnu;->dOo:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 2486
    if-eqz v1, :cond_2

    .line 2487
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2484
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2491
    :cond_3
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 2492
    const/4 v0, 0x4

    iget-object v1, p0, Ljnu;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2494
    :cond_4
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 2495
    const/4 v0, 0x5

    iget-wide v2, p0, Ljnu;->afg:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2497
    :cond_5
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 2498
    const/4 v0, 0x6

    iget-object v1, p0, Ljnu;->evG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2500
    :cond_6
    iget v0, p0, Ljnu;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_7

    .line 2501
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljnu;->evJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2503
    :cond_7
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 2504
    const/16 v0, 0x8

    iget v1, p0, Ljnu;->evH:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2506
    :cond_8
    iget v0, p0, Ljnu;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_9

    .line 2507
    const/16 v0, 0x9

    iget-object v1, p0, Ljnu;->evI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2509
    :cond_9
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_a

    .line 2510
    const/16 v0, 0xa

    iget v1, p0, Ljnu;->evF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2512
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2513
    return-void
.end method

.method public final bqZ()I
    .locals 1

    .prologue
    .line 2314
    iget v0, p0, Ljnu;->evF:I

    return v0
.end method

.method public final bra()Z
    .locals 1

    .prologue
    .line 2363
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2374
    iget-object v0, p0, Ljnu;->evG:Ljava/lang/String;

    return-object v0
.end method

.method public final dD(J)Ljnu;
    .locals 1

    .prologue
    .line 2358
    iput-wide p1, p0, Ljnu;->afg:J

    .line 2359
    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljnu;->aez:I

    .line 2360
    return-object p0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2267
    iget-object v0, p0, Ljnu;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2289
    iget-object v0, p0, Ljnu;->alT:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2517
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2518
    iget v2, p0, Ljnu;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 2519
    const/4 v2, 0x1

    iget-object v3, p0, Ljnu;->aiK:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2522
    :cond_0
    iget v2, p0, Ljnu;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 2523
    const/4 v2, 0x2

    iget-object v3, p0, Ljnu;->alT:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2526
    :cond_1
    iget-object v2, p0, Ljnu;->dOo:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljnu;->dOo:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 2529
    :goto_0
    iget-object v4, p0, Ljnu;->dOo:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 2530
    iget-object v4, p0, Ljnu;->dOo:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 2531
    if-eqz v4, :cond_2

    .line 2532
    add-int/lit8 v3, v3, 0x1

    .line 2533
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2529
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2537
    :cond_3
    add-int/2addr v0, v2

    .line 2538
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 2540
    :cond_4
    iget v1, p0, Ljnu;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 2541
    const/4 v1, 0x4

    iget-object v2, p0, Ljnu;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2544
    :cond_5
    iget v1, p0, Ljnu;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 2545
    const/4 v1, 0x5

    iget-wide v2, p0, Ljnu;->afg:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2548
    :cond_6
    iget v1, p0, Ljnu;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    .line 2549
    const/4 v1, 0x6

    iget-object v2, p0, Ljnu;->evG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2552
    :cond_7
    iget v1, p0, Ljnu;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    .line 2553
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljnu;->evJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2556
    :cond_8
    iget v1, p0, Ljnu;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 2557
    const/16 v1, 0x8

    iget v2, p0, Ljnu;->evH:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2560
    :cond_9
    iget v1, p0, Ljnu;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_a

    .line 2561
    const/16 v1, 0x9

    iget-object v2, p0, Ljnu;->evI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2564
    :cond_a
    iget v1, p0, Ljnu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_b

    .line 2565
    const/16 v1, 0xa

    iget v2, p0, Ljnu;->evF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2568
    :cond_b
    return v0
.end method

.method public final nj()J
    .locals 2

    .prologue
    .line 2355
    iget-wide v0, p0, Ljnu;->afg:J

    return-wide v0
.end method

.method public final pX()Z
    .locals 1

    .prologue
    .line 2278
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qX(I)Ljnu;
    .locals 1

    .prologue
    .line 2317
    iput p1, p0, Ljnu;->evF:I

    .line 2318
    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnu;->aez:I

    .line 2319
    return-object p0
.end method

.method public final tv()Z
    .locals 1

    .prologue
    .line 2300
    iget v0, p0, Ljnu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final xt(Ljava/lang/String;)Ljnu;
    .locals 1

    .prologue
    .line 2270
    if-nez p1, :cond_0

    .line 2271
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2273
    :cond_0
    iput-object p1, p0, Ljnu;->aiK:Ljava/lang/String;

    .line 2274
    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnu;->aez:I

    .line 2275
    return-object p0
.end method

.method public final xu(Ljava/lang/String;)Ljnu;
    .locals 1

    .prologue
    .line 2292
    if-nez p1, :cond_0

    .line 2293
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2295
    :cond_0
    iput-object p1, p0, Ljnu;->alT:Ljava/lang/String;

    .line 2296
    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnu;->aez:I

    .line 2297
    return-object p0
.end method

.method public final xv(Ljava/lang/String;)Ljnu;
    .locals 1

    .prologue
    .line 2377
    if-nez p1, :cond_0

    .line 2378
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2380
    :cond_0
    iput-object p1, p0, Ljnu;->evG:Ljava/lang/String;

    .line 2381
    iget v0, p0, Ljnu;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljnu;->aez:I

    .line 2382
    return-object p0
.end method
