.class final Lcst;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcsq;


# instance fields
.field private abz:Landroid/content/ServiceConnection;

.field private anD:Ljava/lang/String;

.field bia:Lhgw;

.field bib:Z

.field bic:Ljava/util/List;

.field private final mContext:Landroid/content/Context;

.field private final mIntentStarter:Leoj;

.field private final mSettings:Lhym;

.field rE:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lhym;Leoj;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcst;->mContext:Landroid/content/Context;

    .line 54
    iput-object p3, p0, Lcst;->mIntentStarter:Leoj;

    .line 55
    iput-object p2, p0, Lcst;->mSettings:Lhym;

    .line 56
    return-void
.end method

.method private SP()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 282
    iget-boolean v1, p0, Lcst;->rE:Z

    if-eqz v1, :cond_0

    .line 284
    :try_start_0
    iget-object v1, p0, Lcst;->bia:Lhgw;

    iget-object v2, p0, Lcst;->anD:Ljava/lang/String;

    invoke-interface {v1, v2}, Lhgw;->nT(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 293
    :cond_0
    :goto_0
    return v0

    .line 287
    :catch_0
    move-exception v1

    .line 288
    const-string v2, "VISHotwordAdapter"

    const-string v3, "Exception retrieving recognition modes"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private SQ()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 297
    iget-boolean v1, p0, Lcst;->rE:Z

    if-eqz v1, :cond_0

    .line 299
    :try_start_0
    iget-object v1, p0, Lcst;->bia:Lhgw;

    iget-object v2, p0, Lcst;->anD:Ljava/lang/String;

    invoke-interface {v1, v2}, Lhgw;->nR(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 308
    :cond_0
    :goto_0
    return v0

    .line 302
    :catch_0
    move-exception v1

    .line 303
    const-string v2, "VISHotwordAdapter"

    const-string v3, "Exception retrieving hotword availability"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(ILcsr;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 312
    iget-boolean v2, p0, Lcst;->rE:Z

    if-eqz v2, :cond_0

    .line 314
    packed-switch p1, :pswitch_data_0

    .line 326
    :try_start_0
    const-string v1, "VISHotwordAdapter"

    const-string v2, "Unknown manage action %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 350
    :cond_0
    :goto_0
    return v0

    .line 317
    :pswitch_0
    iget-object v2, p0, Lcst;->bia:Lhgw;

    iget-object v3, p0, Lcst;->anD:Ljava/lang/String;

    invoke-interface {v2, v3}, Lhgw;->nU(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 329
    :goto_1
    iget-object v3, p0, Lcst;->mIntentStarter:Leoj;

    new-instance v4, Lcsv;

    invoke-direct {v4, p0, p1, p2}, Lcsv;-><init>(Lcst;ILcsr;)V

    invoke-interface {v3, v2, v4}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    move v0, v1

    .line 342
    goto :goto_0

    .line 320
    :pswitch_1
    iget-object v2, p0, Lcst;->bia:Lhgw;

    iget-object v3, p0, Lcst;->anD:Ljava/lang/String;

    invoke-interface {v2, v3}, Lhgw;->nW(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_1

    .line 323
    :pswitch_2
    iget-object v2, p0, Lcst;->bia:Lhgw;

    iget-object v3, p0, Lcst;->anD:Ljava/lang/String;

    invoke-interface {v2, v3}, Lhgw;->nV(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .line 343
    :catch_0
    move-exception v1

    .line 344
    const-string v2, "VISHotwordAdapter"

    const-string v3, "Exception sending the manage intent"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final SI()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 134
    iget-object v2, p0, Lcst;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    .line 136
    iget-boolean v3, p0, Lcst;->rE:Z

    if-eqz v3, :cond_1

    .line 138
    :try_start_0
    iget-object v3, p0, Lcst;->bia:Lhgw;

    invoke-interface {v3, v2}, Lhgw;->nQ(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 139
    iput-object v2, p0, Lcst;->anD:Ljava/lang/String;

    .line 155
    :goto_0
    return v0

    .line 146
    :cond_0
    invoke-virtual {p0}, Lcst;->disconnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    move v0, v1

    .line 155
    goto :goto_0

    .line 148
    :catch_0
    move-exception v2

    .line 149
    const-string v3, "VISHotwordAdapter"

    const-string v4, "Remote Exception %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v4, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final SJ()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 163
    invoke-virtual {p0}, Lcst;->SM()Z

    move-result v1

    if-nez v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    invoke-direct {p0}, Lcst;->SP()I

    move-result v1

    .line 168
    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final SK()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-virtual {p0}, Lcst;->SM()Z

    move-result v1

    if-nez v1, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v0

    .line 180
    :cond_1
    invoke-direct {p0}, Lcst;->SP()I

    move-result v1

    .line 181
    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final SL()Z
    .locals 2

    .prologue
    .line 189
    iget-boolean v0, p0, Lcst;->rE:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcst;->SQ()I

    move-result v0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final SM()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 196
    iget-boolean v2, p0, Lcst;->rE:Z

    if-nez v2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 202
    :cond_1
    invoke-direct {p0}, Lcst;->SQ()I

    move-result v2

    .line 203
    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    if-ne v2, v1, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final SN()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 210
    iget-boolean v1, p0, Lcst;->rE:Z

    if-nez v1, :cond_1

    .line 215
    :cond_0
    :goto_0
    return v0

    .line 214
    :cond_1
    invoke-direct {p0}, Lcst;->SQ()I

    move-result v1

    .line 215
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, -0x2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final SO()Z
    .locals 2

    .prologue
    .line 222
    invoke-direct {p0}, Lcst;->SQ()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcsr;)V
    .locals 4
    .param p1    # Lcsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 63
    iget-boolean v0, p0, Lcst;->rE:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcst;->bib:Z

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcst;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->bJ(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 66
    new-instance v1, Lcsu;

    invoke-direct {v1, p0}, Lcsu;-><init>(Lcst;)V

    iput-object v1, p0, Lcst;->abz:Landroid/content/ServiceConnection;

    .line 94
    iget-object v1, p0, Lcst;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcst;->abz:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcst;->bic:Ljava/util/List;

    .line 96
    iput-boolean v3, p0, Lcst;->bib:Z

    .line 99
    :cond_0
    if-eqz p1, :cond_1

    .line 100
    invoke-virtual {p0, p1}, Lcst;->b(Lcsr;)V

    .line 102
    :cond_1
    return-void
.end method

.method public final b(Lcsr;)V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcst;->bib:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcst;->bic:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-boolean v0, p0, Lcst;->rE:Z

    invoke-interface {p1, v0}, Lcsr;->bo(Z)V

    goto :goto_0
.end method

.method public final c(Lcsr;)V
    .locals 0
    .param p1    # Lcsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 229
    return-void
.end method

.method public final d(Lcsr;)Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcst;->a(ILcsr;)Z

    move-result v0

    return v0
.end method

.method public final disconnect()V
    .locals 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lcst;->rE:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcst;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcst;->abz:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcst;->rE:Z

    .line 114
    :cond_0
    return-void
.end method

.method public final e(Lcsr;)Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcst;->a(ILcsr;)Z

    move-result v0

    return v0
.end method

.method public final f(Lcsr;)Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcst;->a(ILcsr;)Z

    move-result v0

    return v0
.end method

.method public final isConnected()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcst;->rE:Z

    return v0
.end method

.method public final j(ZZ)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 237
    iget-boolean v2, p0, Lcst;->rE:Z

    if-eqz v2, :cond_3

    .line 239
    if-eqz p2, :cond_0

    .line 240
    :try_start_0
    iget-object v2, p0, Lcst;->mSettings:Lhym;

    invoke-virtual {v2, p1}, Lhym;->gN(Z)V

    .line 241
    iget-object v2, p0, Lcst;->bia:Lhgw;

    invoke-interface {v2, p1}, Lhgw;->gq(Z)V

    .line 243
    :cond_0
    if-eqz p1, :cond_2

    .line 244
    iget-object v2, p0, Lcst;->bia:Lhgw;

    iget-object v3, p0, Lcst;->anD:Ljava/lang/String;

    invoke-interface {v2, v3}, Lhgw;->nS(Ljava/lang/String;)Z

    move-result v0

    .line 256
    :cond_1
    :goto_0
    return v0

    .line 247
    :cond_2
    invoke-virtual {p0}, Lcst;->SO()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248
    iget-object v2, p0, Lcst;->bia:Lhgw;

    invoke-interface {v2}, Lhgw;->aOH()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 252
    :catch_0
    move-exception v2

    .line 253
    const-string v3, "VISHotwordAdapter"

    const-string v4, "Remote Exception %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v4, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_3
    move v0, v1

    .line 256
    goto :goto_0
.end method
