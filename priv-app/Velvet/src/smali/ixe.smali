.class public final Lixe;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dMM:Ljcn;

.field private dMN:Ljava/lang/String;

.field public dMO:Lixf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63787
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 63788
    const/4 v0, 0x0

    iput v0, p0, Lixe;->aez:I

    iput-object v1, p0, Lixe;->dMM:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Lixe;->dMN:Ljava/lang/String;

    iput-object v1, p0, Lixe;->dMO:Lixf;

    iput-object v1, p0, Lixe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixe;->eCz:I

    .line 63789
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 62750
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lixe;->dMM:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Lixe;->dMM:Ljcn;

    :cond_1
    iget-object v0, p0, Lixe;->dMM:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixe;->dMN:Ljava/lang/String;

    iget v0, p0, Lixe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixe;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lixe;->dMO:Lixf;

    if-nez v0, :cond_2

    new-instance v0, Lixf;

    invoke-direct {v0}, Lixf;-><init>()V

    iput-object v0, p0, Lixe;->dMO:Lixf;

    :cond_2
    iget-object v0, p0, Lixe;->dMO:Lixf;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 63804
    iget-object v0, p0, Lixe;->dMM:Ljcn;

    if-eqz v0, :cond_0

    .line 63805
    const/4 v0, 0x1

    iget-object v1, p0, Lixe;->dMM:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 63807
    :cond_0
    iget v0, p0, Lixe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 63808
    const/4 v0, 0x2

    iget-object v1, p0, Lixe;->dMN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 63810
    :cond_1
    iget-object v0, p0, Lixe;->dMO:Lixf;

    if-eqz v0, :cond_2

    .line 63811
    const/4 v0, 0x3

    iget-object v1, p0, Lixe;->dMO:Lixf;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 63813
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 63814
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 63818
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 63819
    iget-object v1, p0, Lixe;->dMM:Ljcn;

    if-eqz v1, :cond_0

    .line 63820
    const/4 v1, 0x1

    iget-object v2, p0, Lixe;->dMM:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63823
    :cond_0
    iget v1, p0, Lixe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 63824
    const/4 v1, 0x2

    iget-object v2, p0, Lixe;->dMN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63827
    :cond_1
    iget-object v1, p0, Lixe;->dMO:Lixf;

    if-eqz v1, :cond_2

    .line 63828
    const/4 v1, 0x3

    iget-object v2, p0, Lixe;->dMO:Lixf;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63831
    :cond_2
    return v0
.end method

.method public final qg(Ljava/lang/String;)Lixe;
    .locals 1

    .prologue
    .line 63768
    if-nez p1, :cond_0

    .line 63769
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63771
    :cond_0
    iput-object p1, p0, Lixe;->dMN:Ljava/lang/String;

    .line 63772
    iget v0, p0, Lixe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixe;->aez:I

    .line 63773
    return-object p0
.end method
