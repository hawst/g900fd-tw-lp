.class public final Ljev;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private egc:I

.field private egd:[Ljew;

.field private ege:Z

.field private egf:Z

.field private egg:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 13826
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13827
    const/4 v0, 0x0

    iput v0, p0, Ljev;->aez:I

    iput-object v2, p0, Ljev;->efN:Ljez;

    iput v1, p0, Ljev;->egc:I

    invoke-static {}, Ljew;->bjg()[Ljew;

    move-result-object v0

    iput-object v0, p0, Ljev;->egd:[Ljew;

    iput-boolean v1, p0, Ljev;->ege:Z

    iput-boolean v1, p0, Ljev;->egf:Z

    iput-boolean v1, p0, Ljev;->egg:Z

    iput-object v2, p0, Ljev;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljev;->eCz:I

    .line 13828
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13580
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljev;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljev;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljev;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljev;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljev;->egc:I

    iget v0, p0, Ljev;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljev;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljev;->egd:[Ljew;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljew;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljev;->egd:[Ljew;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljew;

    invoke-direct {v3}, Ljew;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljev;->egd:[Ljew;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljew;

    invoke-direct {v3}, Ljew;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljev;->egd:[Ljew;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljev;->ege:Z

    iget v0, p0, Ljev;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljev;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljev;->egf:Z

    iget v0, p0, Ljev;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljev;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljev;->egg:Z

    iget v0, p0, Ljev;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljev;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 13846
    iget-object v0, p0, Ljev;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 13847
    const/4 v0, 0x1

    iget-object v1, p0, Ljev;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 13849
    :cond_0
    iget v0, p0, Ljev;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 13850
    const/4 v0, 0x2

    iget v1, p0, Ljev;->egc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 13852
    :cond_1
    iget-object v0, p0, Ljev;->egd:[Ljew;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljev;->egd:[Ljew;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 13853
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljev;->egd:[Ljew;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 13854
    iget-object v1, p0, Ljev;->egd:[Ljew;

    aget-object v1, v1, v0

    .line 13855
    if-eqz v1, :cond_2

    .line 13856
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 13853
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13860
    :cond_3
    iget v0, p0, Ljev;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 13861
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljev;->ege:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13863
    :cond_4
    iget v0, p0, Ljev;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 13864
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljev;->egf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13866
    :cond_5
    iget v0, p0, Ljev;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 13867
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljev;->egg:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13869
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13870
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 13874
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13875
    iget-object v1, p0, Ljev;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 13876
    const/4 v1, 0x1

    iget-object v2, p0, Ljev;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13879
    :cond_0
    iget v1, p0, Ljev;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 13880
    const/4 v1, 0x2

    iget v2, p0, Ljev;->egc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13883
    :cond_1
    iget-object v1, p0, Ljev;->egd:[Ljew;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljev;->egd:[Ljew;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 13884
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljev;->egd:[Ljew;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 13885
    iget-object v2, p0, Ljev;->egd:[Ljew;

    aget-object v2, v2, v0

    .line 13886
    if-eqz v2, :cond_2

    .line 13887
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13884
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 13892
    :cond_4
    iget v1, p0, Ljev;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 13893
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljev;->ege:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13896
    :cond_5
    iget v1, p0, Ljev;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 13897
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljev;->egf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13900
    :cond_6
    iget v1, p0, Ljev;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 13901
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljev;->egg:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13904
    :cond_7
    return v0
.end method
