.class public final Ljhm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile elW:[Ljhm;


# instance fields
.field public aeB:Ljbp;

.field private aeH:Ljava/lang/String;

.field private aeN:Ljava/lang/String;

.field private aez:I

.field private dHR:I

.field private dRq:J

.field private elX:I

.field private elY:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 24829
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 24830
    iput v2, p0, Ljhm;->aez:I

    iput-object v3, p0, Ljhm;->aeB:Ljbp;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhm;->dRq:J

    iput v2, p0, Ljhm;->elX:I

    iput v2, p0, Ljhm;->elY:I

    const-string v0, ""

    iput-object v0, p0, Ljhm;->aeH:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljhm;->aeN:Ljava/lang/String;

    iput v2, p0, Ljhm;->dHR:I

    iput-object v3, p0, Ljhm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhm;->eCz:I

    .line 24831
    return-void
.end method

.method public static blV()[Ljhm;
    .locals 2

    .prologue
    .line 24693
    sget-object v0, Ljhm;->elW:[Ljhm;

    if-nez v0, :cond_1

    .line 24694
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 24696
    :try_start_0
    sget-object v0, Ljhm;->elW:[Ljhm;

    if-nez v0, :cond_0

    .line 24697
    const/4 v0, 0x0

    new-array v0, v0, [Ljhm;

    sput-object v0, Ljhm;->elW:[Ljhm;

    .line 24699
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24701
    :cond_1
    sget-object v0, Ljhm;->elW:[Ljhm;

    return-object v0

    .line 24699
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 24678
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljhm;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljhm;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljhm;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljhm;->dRq:J

    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhm;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhm;->elX:I

    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhm;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhm;->elY:I

    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhm;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhm;->aeH:Ljava/lang/String;

    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhm;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhm;->aeN:Ljava/lang/String;

    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljhm;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljhm;->dHR:I

    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljhm;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 24850
    iget-object v0, p0, Ljhm;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 24851
    const/4 v0, 0x1

    iget-object v1, p0, Ljhm;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 24853
    :cond_0
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 24854
    const/4 v0, 0x2

    iget-wide v2, p0, Ljhm;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 24856
    :cond_1
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 24857
    const/4 v0, 0x3

    iget v1, p0, Ljhm;->elX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 24859
    :cond_2
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 24860
    const/4 v0, 0x4

    iget v1, p0, Ljhm;->elY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 24862
    :cond_3
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 24863
    const/4 v0, 0x5

    iget-object v1, p0, Ljhm;->aeH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 24865
    :cond_4
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 24866
    const/4 v0, 0x6

    iget-object v1, p0, Ljhm;->aeN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 24868
    :cond_5
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 24869
    const/4 v0, 0x7

    iget v1, p0, Ljhm;->dHR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 24871
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 24872
    return-void
.end method

.method public final bcB()J
    .locals 2

    .prologue
    .line 24712
    iget-wide v0, p0, Ljhm;->dRq:J

    return-wide v0
.end method

.method public final blC()Z
    .locals 1

    .prologue
    .line 24720
    iget v0, p0, Ljhm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final blW()I
    .locals 1

    .prologue
    .line 24731
    iget v0, p0, Ljhm;->elX:I

    return v0
.end method

.method public final blX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24791
    iget-object v0, p0, Ljhm;->aeN:Ljava/lang/String;

    return-object v0
.end method

.method public final dl(J)Ljhm;
    .locals 1

    .prologue
    .line 24715
    iput-wide p1, p0, Ljhm;->dRq:J

    .line 24716
    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhm;->aez:I

    .line 24717
    return-object p0
.end method

.method public final getProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24769
    iget-object v0, p0, Ljhm;->aeH:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 24876
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 24877
    iget-object v1, p0, Ljhm;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 24878
    const/4 v1, 0x1

    iget-object v2, p0, Ljhm;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24881
    :cond_0
    iget v1, p0, Ljhm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 24882
    const/4 v1, 0x2

    iget-wide v2, p0, Ljhm;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 24885
    :cond_1
    iget v1, p0, Ljhm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 24886
    const/4 v1, 0x3

    iget v2, p0, Ljhm;->elX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24889
    :cond_2
    iget v1, p0, Ljhm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 24890
    const/4 v1, 0x4

    iget v2, p0, Ljhm;->elY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24893
    :cond_3
    iget v1, p0, Ljhm;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 24894
    const/4 v1, 0x5

    iget-object v2, p0, Ljhm;->aeH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24897
    :cond_4
    iget v1, p0, Ljhm;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 24898
    const/4 v1, 0x6

    iget-object v2, p0, Ljhm;->aeN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24901
    :cond_5
    iget v1, p0, Ljhm;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 24902
    const/4 v1, 0x7

    iget v2, p0, Ljhm;->dHR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24905
    :cond_6
    return v0
.end method

.method public final pV(I)Ljhm;
    .locals 1

    .prologue
    .line 24734
    iput p1, p0, Ljhm;->elX:I

    .line 24735
    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhm;->aez:I

    .line 24736
    return-object p0
.end method

.method public final ve(Ljava/lang/String;)Ljhm;
    .locals 1

    .prologue
    .line 24772
    if-nez p1, :cond_0

    .line 24773
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24775
    :cond_0
    iput-object p1, p0, Ljhm;->aeH:Ljava/lang/String;

    .line 24776
    iget v0, p0, Ljhm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhm;->aez:I

    .line 24777
    return-object p0
.end method
