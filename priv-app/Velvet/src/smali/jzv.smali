.class public final Ljzv;
.super Ljsl;
.source "PG"


# instance fields
.field public eOw:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 556
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 557
    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljzv;->eOw:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljzv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzv;->eCz:I

    .line 558
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 536
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzv;->eOw:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzv;->eOw:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzv;->eOw:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljzv;->eOw:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 570
    iget-object v0, p0, Ljzv;->eOw:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljzv;->eOw:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 571
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljzv;->eOw:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 572
    iget-object v1, p0, Ljzv;->eOw:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 573
    if-eqz v1, :cond_0

    .line 574
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 571
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 578
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 579
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 583
    invoke-super {p0}, Ljsl;->lF()I

    move-result v3

    .line 584
    iget-object v1, p0, Ljzv;->eOw:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljzv;->eOw:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v0

    move v2, v0

    .line 587
    :goto_0
    iget-object v4, p0, Ljzv;->eOw:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 588
    iget-object v4, p0, Ljzv;->eOw:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 589
    if-eqz v4, :cond_0

    .line 590
    add-int/lit8 v2, v2, 0x1

    .line 591
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 587
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 595
    :cond_1
    add-int v0, v3, v1

    .line 596
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 598
    :goto_1
    return v0

    :cond_2
    move v0, v3

    goto :goto_1
.end method
