.class final Lhar;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final daW:Livy;

.field private final daX:Lhau;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private synthetic daY:Lhap;

.field private final dz:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lhap;Livy;Landroid/net/Uri;Lhau;)V
    .locals 1
    .param p4    # Lhau;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 324
    iput-object p1, p0, Lhar;->daY:Lhap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 325
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livy;

    iput-object v0, p0, Lhar;->daW:Livy;

    .line 326
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lhar;->dz:Landroid/net/Uri;

    .line 327
    iput-object p4, p0, Lhar;->daX:Lhau;

    .line 328
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 333
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lhar;->dz:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lhar;->daY:Lhap;

    iget-object v1, p0, Lhar;->daX:Lhau;

    new-instance v2, Ldls;

    const-string v3, "Got InputStream for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lhar;->dz:Landroid/net/Uri;

    aput-object v6, v4, v5

    invoke-direct {v2, v3, v4}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v1, v2}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    .line 335
    new-instance v1, Ldyo;

    new-instance v2, Lcom/google/android/search/shared/api/UriRequest;

    iget-object v3, p0, Lhar;->dz:Landroid/net/Uri;

    invoke-direct {v2, v3}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;)V

    new-instance v3, Ldyj;

    invoke-direct {v3}, Ldyj;-><init>()V

    invoke-static {v3}, Leeb;->c(Ldyj;)Leeb;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    .line 339
    iget-object v2, p0, Lhar;->daW:Livy;

    new-instance v3, Lhao;

    invoke-direct {v3, v1, v0}, Lhao;-><init>(Ldyo;Ljava/io/Closeable;)V

    invoke-virtual {v2, v3}, Livy;->bB(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_0
    return-void

    .line 340
    :catch_0
    move-exception v0

    .line 341
    iget-object v1, p0, Lhar;->daY:Lhap;

    iget-object v1, p0, Lhar;->daX:Lhau;

    new-instance v2, Ldls;

    const-string v3, "Exception reading file: %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-direct {v2, v3, v4}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v1, v2}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    .line 342
    iget-object v1, p0, Lhar;->daW:Livy;

    invoke-virtual {v1, v0}, Livy;->h(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
