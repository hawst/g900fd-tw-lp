.class public Ldby;
.super Lddj;
.source "PG"


# instance fields
.field private final bqt:Lcom/google/android/shared/util/BitFlags;

.field private final brb:Ljava/util/Set;

.field private brc:I

.field private brd:J

.field private bre:Z

.field private brf:Z

.field private brg:Lgcq;

.field private brh:Lent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bri:Z

.field private brj:Lcom/google/android/shared/speech/HotwordResult;

.field private final mClock:Lemp;

.field private final mEventBus:Ldda;

.field private final mGsaConfigFlags:Lchk;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Ldda;Lchk;Lhym;Lemp;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 152
    const/16 v0, 0x20

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 130
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 137
    iput v2, p0, Ldby;->brc:I

    .line 142
    iput-boolean v2, p0, Ldby;->brf:Z

    .line 143
    sget-object v0, Lgcq;->cGb:Lgcq;

    iput-object v0, p0, Ldby;->brg:Lgcq;

    .line 154
    iput-object p1, p0, Ldby;->mEventBus:Ldda;

    .line 155
    iput-object p2, p0, Ldby;->mGsaConfigFlags:Lchk;

    .line 156
    iput-object p3, p0, Ldby;->mVoiceSettings:Lhym;

    .line 157
    iput-object p4, p0, Ldby;->mClock:Lemp;

    .line 158
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldby;->brb:Ljava/util/Set;

    .line 159
    return-void
.end method

.method private WM()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x20

    const-wide/16 v2, 0x10

    .line 597
    iget-object v0, p0, Ldby;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTP()Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    .line 617
    :goto_0
    return v0

    .line 602
    :cond_0
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 607
    iget-object v1, p0, Ldby;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTF()Z

    move-result v1

    if-nez v1, :cond_1

    .line 611
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto :goto_0

    .line 613
    :cond_1
    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 617
    goto :goto_0
.end method

.method public static fC(I)Z
    .locals 1

    .prologue
    .line 706
    if-eqz p0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Nv()V
    .locals 6

    .prologue
    .line 292
    iget-object v0, p0, Ldby;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v2

    .line 293
    iget-wide v0, p0, Ldby;->brd:J

    sub-long v0, v2, v0

    iget-object v4, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v4}, Lchk;->IN()I

    move-result v4

    int-to-long v4, v4

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 294
    :goto_0
    iput-wide v2, p0, Ldby;->brd:J

    .line 295
    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 298
    :cond_0
    return-void

    .line 293
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final WA()Z
    .locals 4

    .prologue
    .line 336
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0xc0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    return v0
.end method

.method public final WB()Z
    .locals 4

    .prologue
    .line 346
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x700

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    return v0
.end method

.method public final WC()Z
    .locals 4

    .prologue
    .line 363
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final WD()Z
    .locals 4

    .prologue
    .line 368
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final WE()Z
    .locals 4

    .prologue
    .line 376
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x6

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    return v0
.end method

.method public final WF()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 395
    invoke-virtual {p0}, Ldby;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v0

    .line 399
    :cond_1
    invoke-virtual {p0}, Ldby;->WE()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 403
    invoke-virtual {p0}, Ldby;->WA()Z

    move-result v1

    if-nez v1, :cond_0

    .line 407
    invoke-virtual {p0}, Ldby;->WB()Z

    move-result v1

    if-nez v1, :cond_0

    .line 411
    iget-object v1, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v1}, Lgcq;->yo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final WG()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 428
    iget-object v2, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v2}, Lgcq;->aEU()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 463
    :cond_0
    :goto_0
    return v0

    .line 433
    :cond_1
    invoke-virtual {p0}, Ldby;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 438
    iget-object v2, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v2}, Lgcq;->yo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 443
    iget-object v2, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->HU()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 445
    goto :goto_0

    .line 448
    :cond_2
    invoke-virtual {p0}, Ldby;->WO()Lent;

    move-result-object v2

    invoke-virtual {v2}, Lent;->auK()I

    move-result v2

    iget-object v3, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->Jd()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 453
    iget-object v2, p0, Ldby;->mVoiceSettings:Lhym;

    invoke-virtual {v2}, Lhym;->Of()J

    move-result-wide v2

    .line 454
    iget-object v4, p0, Ldby;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 455
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_4

    .line 456
    iget-object v0, p0, Ldby;->mVoiceSettings:Lhym;

    iget-object v2, p0, Ldby;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lhym;->W(J)V

    :cond_3
    move v0, v1

    .line 463
    goto :goto_0

    .line 457
    :cond_4
    sub-long v2, v4, v2

    iget-object v4, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v4}, Lchk;->Jf()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    goto :goto_0
.end method

.method public final WH()Z
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v0}, Lgcq;->aET()Z

    move-result v0

    return v0
.end method

.method public final WI()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 487
    iget-boolean v2, p0, Ldby;->brf:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v2}, Lgcq;->aEV()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Ldby;->bre:Z

    if-nez v2, :cond_1

    .line 488
    :cond_0
    iput-boolean v1, p0, Ldby;->bre:Z

    .line 489
    iput-boolean v0, p0, Ldby;->brf:Z

    move v0, v1

    .line 492
    :cond_1
    return v0
.end method

.method public final WJ()I
    .locals 4

    .prologue
    .line 503
    invoke-virtual {p0}, Ldby;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldby;->WB()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldby;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldby;->WE()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    :cond_0
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    .line 511
    const/4 v0, 0x0

    .line 518
    :goto_0
    iget v1, p0, Ldby;->brc:I

    if-eq v1, v0, :cond_3

    .line 519
    iput v0, p0, Ldby;->brc:I

    .line 524
    :goto_1
    return v0

    .line 512
    :cond_1
    iget-boolean v0, p0, Ldby;->bri:Z

    if-eqz v0, :cond_2

    .line 513
    const/4 v0, 0x3

    goto :goto_0

    .line 515
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 524
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final WK()Lcom/google/android/shared/speech/HotwordResult;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 533
    iget-object v0, p0, Ldby;->brj:Lcom/google/android/shared/speech/HotwordResult;

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Ldby;->brj:Lcom/google/android/shared/speech/HotwordResult;

    .line 535
    iput-object v1, p0, Ldby;->brj:Lcom/google/android/shared/speech/HotwordResult;

    .line 538
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final WL()V
    .locals 1

    .prologue
    .line 571
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldby;->brf:Z

    .line 572
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 573
    return-void
.end method

.method public final WN()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x8

    .line 622
    iget-object v0, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 624
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 625
    iget-object v0, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aau()Ldbh;

    move-result-object v0

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    .line 667
    :goto_0
    return v0

    .line 632
    :cond_0
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0

    .line 633
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xh()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 634
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XU()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XV()Z

    move-result v1

    if-nez v1, :cond_4

    .line 636
    iget-object v1, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v1

    if-nez v1, :cond_2

    .line 638
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto :goto_0

    .line 640
    :cond_2
    iget-object v1, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aau()Ldbh;

    move-result-object v1

    invoke-virtual {v1}, Ldbh;->Wg()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 642
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto :goto_0

    .line 644
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yh()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 646
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto :goto_0

    .line 649
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v0

    if-nez v0, :cond_5

    .line 653
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto :goto_0

    .line 657
    :cond_5
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0

    .line 659
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v1

    if-nez v1, :cond_7

    .line 661
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto :goto_0

    .line 662
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v0

    if-nez v0, :cond_8

    .line 664
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    goto/16 :goto_0

    .line 667
    :cond_8
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public WO()Lent;
    .locals 7

    .prologue
    .line 693
    iget-object v0, p0, Ldby;->brh:Lent;

    if-nez v0, :cond_0

    .line 694
    iget-object v0, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Je()J

    move-result-wide v2

    .line 695
    iget-object v0, p0, Ldby;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->Oe()Ljava/lang/String;

    move-result-object v6

    .line 696
    const-wide/16 v0, 0x3e8

    const-wide/16 v4, 0x5

    div-long v4, v2, v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 697
    new-instance v0, Lent;

    iget-object v1, p0, Ldby;->mClock:Lemp;

    invoke-direct/range {v0 .. v6}, Lent;-><init>(Lemp;JJLjava/lang/String;)V

    iput-object v0, p0, Ldby;->brh:Lent;

    .line 699
    :cond_0
    iget-object v0, p0, Ldby;->brh:Lent;

    return-object v0
.end method

.method public final Wy()Z
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v0}, Lgcq;->Wy()Z

    move-result v0

    return v0
.end method

.method public final Wz()Z
    .locals 4

    .prologue
    .line 327
    iget-object v0, p0, Ldby;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ldby;->brd:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->IN()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lddb;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x100

    const-wide/16 v2, 0x4

    .line 163
    const/4 v0, 0x0

    .line 165
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lddb;->aaE()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    :cond_0
    invoke-direct {p0}, Ldby;->WM()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 167
    invoke-virtual {p0}, Ldby;->WN()Z

    move-result v1

    or-int/2addr v0, v1

    .line 169
    :cond_1
    iget-object v1, p0, Ldby;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->FC()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lddb;->aaE()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    iget-object v1, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aau()Ldbh;

    move-result-object v1

    invoke-virtual {v1}, Ldbh;->Wf()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 171
    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 176
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 177
    iget-object v1, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aao()Ldcu;

    move-result-object v1

    invoke-virtual {v1}, Ldcu;->Zj()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    :goto_1
    iget-object v2, p0, Ldby;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aao()Ldcu;

    move-result-object v2

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->amW()Z

    move-result v2

    iget-boolean v3, p0, Ldby;->bri:Z

    if-eq v3, v2, :cond_3

    iput-boolean v2, p0, Ldby;->bri:Z

    const/4 v1, 0x1

    :cond_3
    or-int/2addr v0, v1

    .line 179
    :cond_4
    if-eqz v0, :cond_5

    .line 180
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 182
    :cond_5
    return-void

    .line 173
    :cond_6
    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 177
    :cond_7
    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    goto :goto_1
.end method

.method public final a(Lgcq;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 550
    .line 551
    iget-object v0, p0, Ldby;->brg:Lgcq;

    if-eq p1, v0, :cond_3

    .line 552
    iget-object v0, p0, Ldby;->brg:Lgcq;

    invoke-virtual {p1, v0}, Lgcq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    or-int/lit8 v0, v0, 0x0

    .line 553
    iput-object p1, p0, Ldby;->brg:Lgcq;

    .line 554
    iget-object v3, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v3}, Lgcq;->aEV()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Ldby;->bre:Z

    if-eqz v3, :cond_2

    .line 555
    iput-boolean v2, p0, Ldby;->bre:Z

    .line 559
    :goto_1
    if-eqz v1, :cond_0

    .line 560
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 562
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 552
    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x400

    const/4 v0, 0x0

    .line 221
    invoke-static {p2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    const-string v1, "HotwordState"

    const-string v2, "No package name specified"

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v1, v2, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    if-eqz p1, :cond_3

    .line 228
    iget-object v0, p0, Ldby;->brb:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    .line 240
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    goto :goto_0

    .line 232
    :cond_3
    iget-object v1, p0, Ldby;->brb:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 235
    iget-object v1, p0, Ldby;->brb:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 237
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 1

    .prologue
    .line 282
    iput-object p1, p0, Ldby;->brj:Lcom/google/android/shared/speech/HotwordResult;

    .line 283
    const/4 v0, 0x0

    iput v0, p0, Ldby;->brc:I

    .line 284
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 285
    return-void
.end method

.method public final dc(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x2

    .line 199
    if-eqz p1, :cond_1

    .line 201
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 206
    :goto_0
    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0}, Ldby;->WM()Z

    .line 208
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 210
    :cond_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method

.method public final dd(Z)V
    .locals 6

    .prologue
    .line 266
    if-eqz p1, :cond_0

    const/16 v0, 0x80

    .line 268
    :goto_0
    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    int-to-long v4, v0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    .line 269
    const/4 v0, 0x0

    iput v0, p0, Ldby;->brc:I

    .line 270
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 271
    return-void

    .line 266
    :cond_0
    const/16 v0, 0x40

    goto :goto_0
.end method

.method public final de(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x200

    .line 583
    if-eqz p1, :cond_1

    .line 585
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    .line 589
    :goto_0
    if-eqz v0, :cond_0

    .line 590
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 592
    :cond_0
    return-void

    .line 587
    :cond_1
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0
.end method

.method public final getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v0}, Lgcq;->getPrompt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isActive()Z
    .locals 4

    .prologue
    .line 305
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final isAvailable()Z
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x18

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v0}, Lgcq;->yo()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled()Z
    .locals 4

    .prologue
    .line 312
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v0}, Lgcq;->aEU()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onReady()V
    .locals 6

    .prologue
    .line 250
    iget-object v0, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0xc0

    const-wide/16 v4, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p0}, Ldby;->notifyChanged()V

    .line 255
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 711
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 712
    invoke-virtual {p0}, Ldby;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    const-string v0, "active"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 715
    :cond_0
    invoke-virtual {p0}, Ldby;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 716
    const-string v0, "enabled"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 718
    :cond_1
    invoke-virtual {p0}, Ldby;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 719
    const-string v0, "available"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 721
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mCurrentState="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldby;->brc:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 722
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mHotwordStats="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldby;->brh:Lent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mDataManagerInitialized="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Ldby;->bre:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 724
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mUseMusicDetection="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Ldby;->bri:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 725
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mMusicLastDetectedAt="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Ldby;->brd:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mModelInfo="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldby;->brg:Lgcq;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 727
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "mHotwordStats="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Ldby;->brh:Lent;

    if-nez v0, :cond_3

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mPausingPackageNames="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldby;->brb:Ljava/util/Set;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 730
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mForceUpdateDataManager="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Ldby;->brf:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 732
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "HotwordState(state="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldby;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 727
    :cond_3
    iget-object v0, p0, Ldby;->brh:Lent;

    invoke-virtual {v0}, Lent;->auK()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
