.class public final Leaz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private synthetic bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 208
    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget v2, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    if-eqz v2, :cond_1

    .line 209
    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iput-boolean v1, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 216
    if-eqz v2, :cond_2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    .line 217
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 218
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-boolean v2, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    if-nez v2, :cond_4

    .line 219
    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-object v2, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 221
    :cond_4
    if-nez v0, :cond_5

    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-boolean v2, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    if-eqz v2, :cond_5

    .line 222
    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-object v2, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 224
    :cond_5
    iget-object v2, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iput-boolean v0, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    .line 228
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 229
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-object v0, v0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-ne p1, v0, :cond_8

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :goto_2
    iput v0, v2, Landroid/graphics/Rect;->left:I

    .line 230
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-object v0, v0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-ne p1, v0, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :goto_3
    iput v0, v2, Landroid/graphics/Rect;->top:I

    .line 231
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 232
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iput v3, v0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Qa:F

    .line 233
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iput v2, v0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSS:F

    .line 237
    :cond_6
    :goto_4
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-virtual {v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->invalidate()V

    move v0, v1

    .line 238
    goto :goto_0

    :cond_7
    move v0, v1

    .line 217
    goto :goto_1

    .line 229
    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    goto :goto_2

    .line 230
    :cond_9
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    goto :goto_3

    .line 234
    :cond_a
    const/4 v3, 0x3

    if-eq v2, v3, :cond_b

    if-ne v2, v0, :cond_6

    .line 235
    :cond_b
    iget-object v0, p0, Leaz;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iput-boolean v1, v0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    goto :goto_4
.end method
