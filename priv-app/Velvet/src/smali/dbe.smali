.class final Ldbe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

.field bqC:Lcom/google/android/search/shared/actions/VoiceAction;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbd;)V
    .locals 0

    .prologue
    .line 1250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final VZ()Ljava/util/List;
    .locals 3

    .prologue
    .line 1253
    iget-object v0, p0, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v0, :cond_0

    .line 1254
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v1, 0x0

    iget-object v2, p0, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    aput-object v2, v0, v1

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1256
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final Wa()Ljava/util/List;
    .locals 3

    .prologue
    .line 1260
    iget-object v0, p0, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz v0, :cond_0

    .line 1261
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v1, 0x0

    iget-object v2, p0, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    aput-object v2, v0, v1

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1263
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1313
    const-string v0, " Error="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1314
    iget-object v0, p0, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz v0, :cond_0

    .line 1315
    const-string v0, " + ButtonCard"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1317
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1313
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
