.class public final Ljld;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eri:[Ljld;


# instance fields
.field private aez:I

.field private dMI:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2441
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2442
    const/4 v0, 0x0

    iput v0, p0, Ljld;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljld;->dMI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljld;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljld;->eCz:I

    .line 2443
    return-void
.end method

.method public static boO()[Ljld;
    .locals 2

    .prologue
    .line 2406
    sget-object v0, Ljld;->eri:[Ljld;

    if-nez v0, :cond_1

    .line 2407
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2409
    :try_start_0
    sget-object v0, Ljld;->eri:[Ljld;

    if-nez v0, :cond_0

    .line 2410
    const/4 v0, 0x0

    new-array v0, v0, [Ljld;

    sput-object v0, Ljld;->eri:[Ljld;

    .line 2412
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2414
    :cond_1
    sget-object v0, Ljld;->eri:[Ljld;

    return-object v0

    .line 2412
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2400
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljld;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljld;->dMI:Ljava/lang/String;

    iget v0, p0, Ljld;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljld;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2456
    iget v0, p0, Ljld;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2457
    const/4 v0, 0x1

    iget-object v1, p0, Ljld;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2459
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2460
    return-void
.end method

.method public final boP()Ljld;
    .locals 1

    .prologue
    .line 2436
    const-string v0, ""

    iput-object v0, p0, Ljld;->dMI:Ljava/lang/String;

    .line 2437
    iget v0, p0, Ljld;->aez:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Ljld;->aez:I

    .line 2438
    return-object p0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2422
    iget-object v0, p0, Ljld;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2464
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2465
    iget v1, p0, Ljld;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2466
    const/4 v1, 0x1

    iget-object v2, p0, Ljld;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2469
    :cond_0
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 2433
    iget v0, p0, Ljld;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wD(Ljava/lang/String;)Ljld;
    .locals 1

    .prologue
    .line 2425
    if-nez p1, :cond_0

    .line 2426
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2428
    :cond_0
    iput-object p1, p0, Ljld;->dMI:Ljava/lang/String;

    .line 2429
    iget v0, p0, Ljld;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljld;->aez:I

    .line 2430
    return-object p0
.end method
