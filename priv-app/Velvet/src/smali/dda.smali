.class public Ldda;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private final brr:Ldcf;

.field private final bsm:Laqb;

.field private btA:Z

.field private btB:Lddb;

.field private btC:Z

.field private final bty:Lddb;

.field private final btz:[Lddj;

.field private final mActionState:Ldbd;

.field private final mAudioState:Ldbh;

.field private final mClock:Lemp;

.field private final mClockworkState:Ldbi;

.field private final mDiscoveryState:Ldbj;

.field private final mHotwordState:Ldby;

.field private final mPumpkinState:Ldcg;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mSearchPlateState:Ldct;

.field private final mServiceState:Ldcu;

.field private final mStreamParsingState:Ldcv;

.field private final mTtsState:Ldcw;

.field private final mUiState:Ldcy;

.field private final mWebAppState:Lddk;


# direct methods
.method public constructor <init>(Lgpu;Lemp;Lhym;)V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lddb;

    const/16 v1, 0x1fff

    invoke-direct {v0, p0, v1}, Lddb;-><init>(Ldda;I)V

    iput-object v0, p0, Ldda;->bty:Lddb;

    .line 67
    iput-object p2, p0, Ldda;->mClock:Lemp;

    .line 69
    invoke-virtual {p1, p0}, Lgpu;->f(Ldda;)Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldda;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 70
    invoke-static {p0}, Lgpu;->s(Ldda;)Ldbh;

    move-result-object v0

    iput-object v0, p0, Ldda;->mAudioState:Ldbh;

    .line 71
    invoke-virtual {p1, p0}, Lgpu;->l(Ldda;)Ldbd;

    move-result-object v0

    iput-object v0, p0, Ldda;->mActionState:Ldbd;

    .line 72
    invoke-static {p0, p3}, Lgpu;->a(Ldda;Lhym;)Ldcw;

    move-result-object v0

    iput-object v0, p0, Ldda;->mTtsState:Ldcw;

    .line 73
    invoke-virtual {p1, p0, p3}, Lgpu;->b(Ldda;Lhym;)Ldby;

    move-result-object v0

    iput-object v0, p0, Ldda;->mHotwordState:Ldby;

    .line 74
    invoke-virtual {p1, p0}, Lgpu;->r(Ldda;)Ldcy;

    move-result-object v0

    iput-object v0, p0, Ldda;->mUiState:Ldcy;

    .line 75
    invoke-virtual {p1, p0}, Lgpu;->p(Ldda;)Ldcg;

    move-result-object v0

    iput-object v0, p0, Ldda;->mPumpkinState:Ldcg;

    .line 76
    invoke-virtual {p1, p0}, Lgpu;->g(Ldda;)Ldbj;

    move-result-object v0

    iput-object v0, p0, Ldda;->mDiscoveryState:Ldbj;

    .line 77
    invoke-virtual {p1, p0, p3}, Lgpu;->c(Ldda;Lhym;)Ldcu;

    move-result-object v0

    iput-object v0, p0, Ldda;->mServiceState:Ldcu;

    .line 78
    invoke-virtual {p1, p0}, Lgpu;->m(Ldda;)Ldct;

    move-result-object v0

    iput-object v0, p0, Ldda;->mSearchPlateState:Ldct;

    .line 79
    invoke-static {p0}, Lgpu;->n(Ldda;)Ldcv;

    move-result-object v0

    iput-object v0, p0, Ldda;->mStreamParsingState:Ldcv;

    .line 80
    invoke-static {}, Lgpu;->aJJ()Ldcf;

    move-result-object v0

    iput-object v0, p0, Ldda;->brr:Ldcf;

    .line 81
    invoke-static {p0}, Lgpu;->o(Ldda;)Ldbi;

    move-result-object v0

    iput-object v0, p0, Ldda;->mClockworkState:Ldbi;

    .line 82
    invoke-virtual {p1, p0}, Lgpu;->q(Ldda;)Lddk;

    move-result-object v0

    iput-object v0, p0, Ldda;->mWebAppState:Lddk;

    .line 84
    const/16 v0, 0xd

    new-array v0, v0, [Lddj;

    const/4 v1, 0x0

    iget-object v2, p0, Ldda;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Ldda;->mPumpkinState:Ldcg;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Ldda;->mActionState:Ldbd;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Ldda;->mWebAppState:Lddk;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Ldda;->mStreamParsingState:Ldcv;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Ldda;->mTtsState:Ldcw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Ldda;->mHotwordState:Ldby;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Ldda;->mAudioState:Ldbh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Ldda;->mUiState:Ldcy;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Ldda;->mDiscoveryState:Ldbj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Ldda;->mClockworkState:Ldbi;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Ldda;->mServiceState:Ldcu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Ldda;->mSearchPlateState:Ldct;

    aput-object v2, v0, v1

    iput-object v0, p0, Ldda;->btz:[Lddj;

    .line 88
    new-instance v0, Laqb;

    invoke-direct {v0}, Laqb;-><init>()V

    iput-object v0, p0, Ldda;->bsm:Laqb;

    .line 89
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 5

    .prologue
    .line 385
    const-string v0, "VelvetEventBus"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 386
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v1

    .line 387
    const-string v0, "Observers"

    invoke-virtual {v1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 388
    iget-object v0, p0, Ldda;->bsm:Laqb;

    invoke-virtual {v0}, Laqb;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddc;

    .line 389
    instance-of v3, v0, Leti;

    if-eqz v3, :cond_0

    .line 390
    check-cast v0, Leti;

    invoke-virtual {v1, v0}, Letj;->b(Leti;)V

    goto :goto_0

    .line 394
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 398
    :cond_1
    iget-object v1, p0, Ldda;->btz:[Lddj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 399
    invoke-virtual {p1, v3}, Letj;->b(Leti;)V

    .line 398
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 401
    :cond_2
    return-void
.end method

.method public final aaf()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 120
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 121
    iget-object v2, p0, Ldda;->btz:[Lddj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 122
    invoke-virtual {v4, v1}, Lddj;->s(Landroid/os/Bundle;)V

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_0
    return-object v1
.end method

.method public final aag()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 271
    iget-boolean v0, p0, Ldda;->btC:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 272
    iput-boolean v1, p0, Ldda;->btC:Z

    .line 273
    return-void

    .line 271
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aah()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 281
    iget-boolean v0, p0, Ldda;->btC:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 282
    iput-boolean v1, p0, Ldda;->btC:Z

    .line 283
    iget-object v0, p0, Ldda;->btB:Lddb;

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0, v1}, Ldda;->fG(I)V

    .line 286
    :cond_0
    return-void
.end method

.method public final aai()Lcom/google/android/search/core/state/QueryState;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Ldda;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    return-object v0
.end method

.method public final aaj()Ldbd;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Ldda;->mActionState:Ldbd;

    return-object v0
.end method

.method public final aak()Ldcw;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Ldda;->mTtsState:Ldcw;

    return-object v0
.end method

.method public final aal()Ldby;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Ldda;->mHotwordState:Ldby;

    return-object v0
.end method

.method public final aam()Ldcg;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Ldda;->mPumpkinState:Ldcg;

    return-object v0
.end method

.method public final aan()Ldcy;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Ldda;->mUiState:Ldcy;

    return-object v0
.end method

.method public final aao()Ldcu;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Ldda;->mServiceState:Ldcu;

    return-object v0
.end method

.method public final aap()Ldct;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Ldda;->mSearchPlateState:Ldct;

    return-object v0
.end method

.method public final aaq()Ldbi;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Ldda;->mClockworkState:Ldbi;

    return-object v0
.end method

.method public final aar()Lddk;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Ldda;->mWebAppState:Lddk;

    return-object v0
.end method

.method public final aas()Ldbj;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Ldda;->mDiscoveryState:Ldbj;

    return-object v0
.end method

.method public final aat()Ldcf;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Ldda;->brr:Ldcf;

    return-object v0
.end method

.method public final aau()Ldbh;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Ldda;->mAudioState:Ldbh;

    return-object v0
.end method

.method public final aav()Ldcv;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Ldda;->mStreamParsingState:Ldcv;

    return-object v0
.end method

.method public final au(Lcom/google/android/shared/search/Query;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 97
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 98
    iget-object v2, p0, Ldda;->btz:[Lddj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 99
    invoke-virtual {v4, p1, v1}, Lddj;->a(Lcom/google/android/shared/search/Query;Landroid/os/Bundle;)V

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    return-object v1
.end method

.method public final c(Landroid/os/Bundle;I)V
    .locals 4

    .prologue
    .line 112
    iget-object v1, p0, Ldda;->btz:[Lddj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 113
    invoke-virtual {v3, p1, p2}, Lddj;->b(Landroid/os/Bundle;I)V

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_0
    return-void
.end method

.method public final c(Lddc;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 227
    invoke-static {}, Lenu;->auR()V

    .line 229
    iget-object v0, p0, Ldda;->bsm:Laqb;

    invoke-virtual {v0, p1}, Laqb;->ai(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Ldda;->bsm:Laqb;

    invoke-virtual {v0, p1}, Laqb;->ag(Ljava/lang/Object;)V

    .line 231
    iget-boolean v0, p0, Ldda;->btC:Z

    if-eqz v0, :cond_1

    .line 234
    new-instance v0, Lddb;

    invoke-direct {v0, p0, v2}, Lddb;-><init>(Ldda;I)V

    iput-object v0, p0, Ldda;->btB:Lddb;

    .line 235
    iget-object v0, p0, Ldda;->btB:Lddb;

    iget-object v0, v0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    iget-object v1, p0, Ldda;->bty:Lddb;

    iget-object v1, v1, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/util/BitFlags;->d(Lcom/google/android/shared/util/BitFlags;)Z

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-boolean v0, p0, Ldda;->btA:Z

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Ldda;->bty:Lddb;

    invoke-interface {p1, v0}, Lddc;->a(Lddb;)V

    goto :goto_0

    .line 243
    :cond_2
    iget-object v0, p0, Ldda;->btB:Lddb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 244
    iput-boolean v1, p0, Ldda;->btA:Z

    .line 245
    iget-object v0, p0, Ldda;->bty:Lddb;

    invoke-interface {p1, v0}, Lddc;->a(Lddb;)V

    .line 246
    iput-boolean v2, p0, Ldda;->btA:Z

    .line 247
    iget-object v0, p0, Ldda;->btB:Lddb;

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p0, v2}, Ldda;->fG(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 243
    goto :goto_1
.end method

.method public final d(Landroid/os/Bundle;I)V
    .locals 4

    .prologue
    .line 130
    iget-object v1, p0, Ldda;->btz:[Lddj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 131
    invoke-virtual {v3, p1, p2}, Lddj;->a(Landroid/os/Bundle;I)V

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method public final d(Lddc;)V
    .locals 1

    .prologue
    .line 260
    invoke-static {}, Lenu;->auR()V

    .line 262
    iget-object v0, p0, Ldda;->bsm:Laqb;

    invoke-virtual {v0, p1}, Laqb;->ah(Ljava/lang/Object;)V

    .line 263
    return-void
.end method

.method public final fG(I)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 155
    iget-object v0, p0, Ldda;->btB:Lddb;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lddb;

    invoke-direct {v0, p0, p1}, Lddb;-><init>(Ldda;I)V

    iput-object v0, p0, Ldda;->btB:Lddb;

    .line 161
    :goto_0
    iget-boolean v0, p0, Ldda;->btA:Z

    if-eqz v0, :cond_1

    .line 219
    :goto_1
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Ldda;->btB:Lddb;

    iget-object v0, v0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Ldda;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    .line 179
    invoke-static {}, Lenu;->auR()V

    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldda;->btA:Z

    .line 182
    :cond_2
    :goto_2
    iget-object v0, p0, Ldda;->btB:Lddb;

    if-eqz v0, :cond_5

    .line 185
    iget-object v2, p0, Ldda;->btB:Lddb;

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Ldda;->btB:Lddb;

    .line 188
    iget-object v3, p0, Ldda;->btz:[Lddj;

    array-length v4, v3

    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_4

    aget-object v5, v3, v0

    .line 189
    invoke-virtual {v5, v2}, Lddj;->a(Lddb;)V

    .line 192
    invoke-virtual {v5}, Lddj;->aaJ()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 193
    iget-object v6, v2, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v5}, Lddj;->getId()I

    move-result v5

    int-to-long v8, v5

    invoke-virtual {v6, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 188
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 197
    :cond_4
    iget-object v0, p0, Ldda;->btB:Lddb;

    if-nez v0, :cond_8

    .line 198
    iget-boolean v0, p0, Ldda;->btC:Z

    if-eqz v0, :cond_6

    .line 200
    iput-object v2, p0, Ldda;->btB:Lddb;

    .line 211
    :cond_5
    iput-boolean v1, p0, Ldda;->btA:Z

    goto :goto_1

    .line 203
    :cond_6
    iget-object v0, p0, Ldda;->bsm:Laqb;

    invoke-virtual {v0}, Laqb;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddc;

    iget-object v4, v2, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-interface {v0}, Lddc;->Bd()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0, v2}, Lddc;->a(Lddb;)V

    goto :goto_4

    .line 208
    :cond_8
    iget-object v0, p0, Ldda;->btB:Lddb;

    iget-object v0, v0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    iget-object v2, v2, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/util/BitFlags;->d(Lcom/google/android/shared/util/BitFlags;)Z

    goto :goto_2
.end method
