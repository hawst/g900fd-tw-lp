.class public Lcmk;
.super Lcmy;
.source "PG"

# interfaces
.implements Lcoi;
.implements Lcpt;
.implements Lemy;


# static fields
.field private static final bbS:Leiq;


# instance fields
.field private final bbT:Ljava/util/concurrent/ExecutorService;

.field private final bbU:Lcom/google/android/search/shared/api/UriRequest;

.field private final bbV:Ljava/util/concurrent/atomic/AtomicInteger;

.field private bbW:Lcmg;

.field private bbX:Ljava/io/InputStream;

.field private bbY:Ljava/io/InputStream;

.field private final bbk:Legl;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mCookies:Lgpf;

.field private final mGsaConfig:Lchk;

.field private final mHeaderProcessor:Lcmo;

.field private final mSdchManager:Lczz;

.field private final mSearchUrlHelper:Lcpn;

.field private final mStaticContentCache:Lcpq;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Leir;

    const-string v1, "cancelled"

    invoke-direct {v0, v1}, Leir;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcmk;->bbS:Leiq;

    return-void
.end method

.method public constructor <init>(Lema;Lchk;Lcpn;Lczz;Legl;Lcom/google/android/search/shared/api/UriRequest;Lcmo;Lgpf;Lcpq;)V
    .locals 2
    .param p5    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 133
    invoke-direct {p0}, Lcmy;-><init>()V

    .line 92
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcmk;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 134
    invoke-virtual {p1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcmk;->bbT:Ljava/util/concurrent/ExecutorService;

    .line 135
    iput-object p2, p0, Lcmk;->mGsaConfig:Lchk;

    .line 136
    iput-object p3, p0, Lcmk;->mSearchUrlHelper:Lcpn;

    .line 137
    iput-object p4, p0, Lcmk;->mSdchManager:Lczz;

    .line 138
    iput-object p5, p0, Lcmk;->bbk:Legl;

    .line 139
    iput-object p6, p0, Lcmk;->bbU:Lcom/google/android/search/shared/api/UriRequest;

    .line 140
    iput-object p7, p0, Lcmk;->mHeaderProcessor:Lcmo;

    .line 141
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v0, p0}, Lcmo;->c(Lemy;)V

    .line 142
    iput-object p8, p0, Lcmk;->mCookies:Lgpf;

    .line 143
    iput-object p9, p0, Lcmk;->mStaticContentCache:Lcpq;

    .line 144
    return-void
.end method

.method public constructor <init>(Lema;Lchk;Lcpn;Lczz;Legl;Lcom/google/android/search/shared/api/UriRequest;Lgpf;Lcpq;)V
    .locals 10

    .prologue
    .line 120
    new-instance v7, Lcmo;

    invoke-direct {v7}, Lcmo;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcmk;-><init>(Lema;Lchk;Lcpn;Lczz;Legl;Lcom/google/android/search/shared/api/UriRequest;Lcmo;Lgpf;Lcpq;)V

    .line 122
    return-void
.end method

.method private Qd()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 181
    iget-object v1, p0, Lcmk;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0}, Lcmz;->Qx()V

    .line 183
    const/4 v0, 0x1

    .line 185
    :cond_0
    return v0
.end method

.method private a(Ldyj;)Z
    .locals 4

    .prologue
    .line 463
    monitor-enter p0

    .line 464
    :try_start_0
    iget-object v0, p0, Lcmk;->bbX:Ljava/io/InputStream;

    .line 465
    iget-object v1, p0, Lcmk;->bbY:Ljava/io/InputStream;

    .line 466
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    iget-object v2, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v2}, Lcmo;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 469
    new-instance v0, Left;

    iget-object v1, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v1}, Lcmo;->getResponseCode()I

    move-result v1

    const-string v2, "Bad HTTP response code."

    invoke-direct {v0, v1, v2}, Left;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z

    .line 481
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 471
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 472
    :cond_1
    new-instance v0, Lcna;

    const-string v1, "No response body received."

    const v2, 0x10012

    invoke-direct {v0, v1, v2}, Lcna;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z

    goto :goto_0

    .line 475
    :cond_2
    iget-object v0, p0, Lcmk;->mCookies:Lgpf;

    iget-object v2, p0, Lcmk;->bbU:Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Lgpf;->a(Ljava/lang/String;Ldyj;)V

    .line 477
    new-instance v0, Ldyo;

    iget-object v2, p0, Lcmk;->bbU:Lcom/google/android/search/shared/api/UriRequest;

    invoke-static {p1}, Leeb;->c(Ldyj;)Leeb;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    .line 479
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v1

    invoke-interface {v1, v0}, Lcmz;->b(Ldyo;)V

    goto :goto_0
.end method

.method private ho(Ljava/lang/String;)Ldkj;
    .locals 1

    .prologue
    .line 412
    new-instance v0, Lcml;

    invoke-direct {v0, p0, p1}, Lcml;-><init>(Lcmk;Ljava/lang/String;)V

    return-object v0
.end method

.method private isFailed()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 190
    iget-object v1, p0, Lcmk;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final N(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 245
    const-string v0, "Velvet.S3FetchTask"

    const-string v1, "Received suggestions from S3, which is not expected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 246
    return-void
.end method

.method public final PB()V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0}, Lcmz;->Qz()V

    .line 235
    return-void
.end method

.method public final PC()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method public final Qe()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 198
    iget-object v1, p0, Lcmk;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 199
    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ldkc;)Lcmg;
    .locals 2

    .prologue
    .line 430
    new-instance v0, Lcmg;

    iget-object v1, p0, Lcmk;->bbk:Legl;

    invoke-direct {v0, p1, v1}, Lcmg;-><init>(Ldkc;Legl;)V

    return-object v0
.end method

.method public final a(Lcms;)V
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->b(Lcms;)V

    .line 220
    return-void
.end method

.method public final a(Lcom/google/android/velvet/ActionData;)V
    .locals 0

    .prologue
    .line 207
    invoke-virtual {p0, p1}, Lcmk;->c(Lcom/google/android/velvet/ActionData;)V

    .line 210
    return-void
.end method

.method public final declared-synchronized a(Ljwp;)V
    .locals 9

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcmk;->Qe()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 276
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljwp;->bvp()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljwp;->bvo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 277
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {p1}, Ljwp;->bvo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcmo;->hp(Ljava/lang/String;)V

    .line 280
    :cond_2
    invoke-virtual {p1}, Ljwp;->bvq()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 283
    iget-object v0, p0, Lcmk;->bbX:Ljava/io/InputStream;

    if-nez v0, :cond_4

    .line 286
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v0}, Lcmo;->Qf()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :try_start_2
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v0}, Lcmo;->PQ()Ldyj;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v0, Left;

    iget-object v1, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v1}, Lcmo;->getResponseCode()I

    move-result v1

    const-string v2, "S3 Headers are null. Response was unparsable or had an error status."

    invoke-direct {v0, v1, v2}, Left;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lefq; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 289
    :catch_0
    move-exception v0

    .line 290
    :try_start_3
    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 288
    :cond_3
    :try_start_4
    new-instance v0, Ldki;

    const-string v2, "ForwardingChunkProducer"

    invoke-direct {p0, v2}, Lcmk;->ho(Ljava/lang/String;)Ldkj;

    move-result-object v2

    invoke-direct {v0, v2}, Ldki;-><init>(Ldkj;)V

    invoke-virtual {p0, v0}, Lcmk;->a(Ldkc;)Lcmg;

    move-result-object v2

    iput-object v2, p0, Lcmk;->bbW:Lcmg;
    :try_end_4
    .catch Lefq; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcmk;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v3}, Lcpn;->RC()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lefq; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v3, p0, Lcmk;->mSdchManager:Lczz;

    invoke-virtual {v3, v1, v2, v0}, Lczz;->a(Ldyj;Ljava/net/URL;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcmk;->bbX:Ljava/io/InputStream;

    new-instance v2, Ldki;

    const-string v0, "InputStreamChunkProducer"

    invoke-direct {p0, v0}, Lcmk;->ho(Ljava/lang/String;)Ldkj;

    move-result-object v0

    invoke-direct {v2, v0}, Ldki;-><init>(Ldkj;)V

    iput-object v2, p0, Lcmk;->bbY:Ljava/io/InputStream;

    new-instance v0, Lcnr;

    iget-object v3, p0, Lcmk;->bbX:Ljava/io/InputStream;

    const/4 v4, 0x2

    iget-object v5, p0, Lcmk;->mGsaConfig:Lchk;

    invoke-direct {v0, v1, v3, v4, v5}, Lcnr;-><init>(Ldyj;Ljava/io/InputStream;ILchk;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ldkc;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcmk;->bbT:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lcmk;->mGsaConfig:Lchk;

    iget-object v4, p0, Lcmk;->bbk:Legl;

    iget-object v5, p0, Lcmk;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v5}, Lcpn;->RC()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcmk;->mStaticContentCache:Lcpq;

    move-object v5, p0

    move-object v6, p0

    invoke-static/range {v0 .. v8}, Ldlh;->a(Lcnr;Ljava/util/List;Ljava/util/concurrent/ExecutorService;Lchk;Legl;Lcoi;Lcpt;Ljava/lang/String;Lcpq;)Ldlh;
    :try_end_6
    .catch Lefq; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 297
    :try_start_7
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v0}, Lcmo;->Qg()V

    .line 303
    :cond_4
    invoke-direct {p0}, Lcmk;->isFailed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    :cond_5
    invoke-virtual {p1}, Ljwp;->bvs()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Ljwp;->bvr()[B

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Ljwp;->bvr()[B

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_7

    .line 312
    iget-object v0, p0, Lcmk;->bbW:Lcmg;

    if-nez v0, :cond_6

    .line 313
    new-instance v0, Lcna;

    const-string v1, "Missing response producer. (Out of order message ?)"

    const v2, 0x10013

    invoke-direct {v0, v1, v2}, Lcna;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 288
    :catch_1
    move-exception v0

    :try_start_8
    new-instance v0, Lefs;

    const v1, 0x10001

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0
    :try_end_8
    .catch Lefq; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 319
    :cond_6
    :try_start_9
    invoke-virtual {p1}, Ljwp;->bvr()[B
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    .line 322
    :try_start_a
    iget-object v1, p0, Lcmk;->bbW:Lcmg;

    invoke-virtual {v1, v0}, Lcmg;->w([B)V
    :try_end_a
    .catch Lefs; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 328
    :cond_7
    :goto_1
    :try_start_b
    invoke-virtual {p1}, Ljwp;->bvt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcmk;->bbW:Lcmg;

    if-nez v0, :cond_8

    .line 332
    new-instance v0, Lcna;

    const-string v1, "Missing response producer. (Out of order message ?)"

    const v2, 0x10013

    invoke-direct {v0, v1, v2}, Lcna;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z

    goto/16 :goto_0

    .line 323
    :catch_2
    move-exception v0

    .line 324
    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z

    goto :goto_1

    .line 339
    :cond_8
    iget-object v0, p0, Lcmk;->bbW:Lcmg;

    invoke-virtual {v0}, Lcmg;->PY()V

    .line 340
    invoke-direct {p0}, Lcmk;->Qd()Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Ljyl;)V
    .locals 5

    .prologue
    .line 346
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Ljyl;->eLN:Ljye;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ljyl;->eLN:Ljye;

    iget-object v0, v0, Ljye;->eBt:[Ljkt;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 347
    new-instance v1, Ljrp;

    invoke-direct {v1}, Ljrp;-><init>()V

    .line 348
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljrp;->iY(Z)Ljrp;

    .line 349
    iget-object v0, p1, Ljyl;->eLN:Ljye;

    iget-object v0, v0, Ljye;->eBt:[Ljkt;

    invoke-virtual {v0}, [Ljkt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljkt;

    iput-object v0, v1, Ljrp;->eBt:[Ljkt;

    .line 350
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/velvet/ActionData;->a(Ljrp;Ljyw;Ljava/lang/String;Z)Lcom/google/android/velvet/ActionData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcmz;->b(Lcom/google/android/velvet/ActionData;)V

    .line 352
    :cond_0
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->b(Ljyl;)V

    .line 353
    invoke-direct {p0}, Lcmk;->Qd()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    monitor-exit p0

    return-void

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljyw;)V
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->b(Ljyw;)V

    .line 225
    return-void
.end method

.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 67
    check-cast p1, Ldyj;

    invoke-direct {p0, p1}, Lcmk;->a(Ldyj;)Z

    move-result v0

    return v0
.end method

.method final b(Lefq;)Z
    .locals 4
    .param p1    # Lefq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    invoke-virtual {p1}, Lefq;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 170
    const-string v3, "Velvet.S3FetchTask"

    invoke-static {v3, v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 172
    :cond_0
    iget-object v2, p0, Lcmk;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v1

    invoke-interface {v1, p1}, Lcmz;->c(Lefq;)V

    .line 176
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final cancel()V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Lcna;

    const-string v1, "Cancelled."

    const v2, 0x10014

    invoke-direct {v0, v1, v2}, Lcna;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    sget-object v1, Lcmk;->bbS:Leiq;

    invoke-virtual {v0, v1}, Lcmo;->d(Leiq;)V

    .line 155
    monitor-enter p0

    .line 156
    :try_start_0
    iget-object v0, p0, Lcmk;->bbW:Lcmg;

    .line 157
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0}, Lcmg;->PY()V

    .line 162
    :cond_0
    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final cu(Z)V
    .locals 1

    .prologue
    .line 214
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    :goto_0
    invoke-virtual {p0, v0}, Lcmk;->c(Lcom/google/android/velvet/ActionData;)V

    .line 215
    return-void

    .line 214
    :cond_0
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    goto :goto_0
.end method

.method public final d(Leiq;)V
    .locals 3

    .prologue
    .line 437
    const-string v0, "Velvet.S3FetchTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed S3ResultPage: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Leiq;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-virtual {p0}, Lcmk;->Qe()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    new-instance v0, Lcna;

    invoke-virtual {p1}, Leiq;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Leiq;->getErrorCode()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcna;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcmk;->b(Lefq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcmk;->mHeaderProcessor:Lcmo;

    invoke-virtual {v0, p1}, Lcmo;->d(Leiq;)V

    .line 447
    monitor-enter p0

    .line 448
    :try_start_0
    iget-object v0, p0, Lcmk;->bbW:Lcmg;

    .line 449
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    if-eqz v0, :cond_0

    .line 451
    invoke-virtual {v0, p1}, Lcmg;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final fe(I)V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->fe(I)V

    .line 230
    return-void
.end method

.method public final g([I)V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcmk;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->h([I)V

    .line 240
    return-void
.end method

.method public final hn(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 488
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "S3FetchTask{complete="

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcmk;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", failed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcmk;->isFailed()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
