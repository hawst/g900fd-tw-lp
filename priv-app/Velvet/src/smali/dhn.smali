.class public Ldhn;
.super Ldgi;
.source "PG"


# static fields
.field private static final bxQ:Ljava/util/Set;


# instance fields
.field final aUv:Lddv;

.field final aVS:Ldhu;

.field private final buj:Lepo;

.field private final bxR:Z

.field private final mConfig:Lcjs;

.field private final mGsaConfig:Lchk;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    const-string v1, "com.google.android.googlequicksearchbox"

    sget-object v2, Lcum;->bjr:Lcum;

    invoke-virtual {v2}, Lcum;->xF()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Ldhn;->bxQ:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcjs;Lchk;Ldfz;Lepo;Leqo;Ldhu;Lddv;)V
    .locals 9

    .prologue
    .line 100
    sget-object v0, Lcgg;->aVC:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Ldhn;-><init>(Lcjs;Lchk;Ldfz;Lepo;Leqo;Ldhu;ZLddv;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Lcjs;Lchk;Ldfz;Lepo;Leqo;Ldhu;ZLddv;)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0, p3, p5}, Ldgi;-><init>(Ldfz;Leqo;)V

    .line 88
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjs;

    iput-object v0, p0, Ldhn;->mConfig:Lcjs;

    .line 89
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Ldhn;->mGsaConfig:Lchk;

    .line 90
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepo;

    iput-object v0, p0, Ldhn;->buj:Lepo;

    .line 91
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhu;

    iput-object v0, p0, Ldhn;->aVS:Ldhu;

    .line 92
    iput-boolean p7, p0, Ldhn;->bxR:Z

    .line 93
    iput-object p8, p0, Ldhn;->aUv:Lddv;

    .line 94
    return-void
.end method

.method static synthetic a(Ldhn;)Leqo;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ldhn;->bwW:Leqo;

    return-object v0
.end method

.method private static a(Lcom/google/android/shared/search/Query;ILdgk;ILjava/util/Set;ILjava/util/List;Ljava/util/Set;)V
    .locals 4

    .prologue
    .line 201
    invoke-static {p4}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    .line 202
    invoke-interface {v0, p7}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 203
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    invoke-interface {p2, p1}, Ldgk;->fL(I)V

    .line 213
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-interface {p7, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 208
    invoke-static {p0, p5, p3}, Ldhr;->b(Lcom/google/android/shared/search/Query;II)Ldhs;

    move-result-object v1

    .line 209
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 210
    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->auC:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Ldhs;->ap(Ljava/lang/String;Ljava/lang/String;)Ldhs;

    goto :goto_1

    .line 212
    :cond_1
    invoke-virtual {v1}, Ldhs;->abZ()Ldhr;

    move-result-object v0

    new-instance v1, Ldhq;

    invoke-direct {v1, p2, p1}, Ldhq;-><init>(Ldgk;I)V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {p6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/shared/search/Query;Ldgk;)V
    .locals 9

    .prologue
    .line 107
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 108
    iget-object v0, p0, Ldhn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Gi()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v8, v0

    .line 110
    :goto_0
    iget-object v0, p0, Ldhn;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abC()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    const/4 v0, 0x5

    invoke-interface {p2, v0}, Ldgk;->fL(I)V

    .line 112
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Ldgk;->fL(I)V

    .line 113
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ldgk;->fL(I)V

    .line 180
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Ldhn;->buj:Lepo;

    new-instance v1, Ldhp;

    invoke-direct {v1, p0, v6}, Ldhp;-><init>(Ldhn;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lepo;->a(Lepn;)V

    .line 196
    :cond_0
    return-void

    .line 108
    :cond_1
    const/4 v0, 0x0

    move v8, v0

    goto :goto_0

    .line 116
    :cond_2
    iget-object v0, p0, Ldhn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Iu()I

    move-result v0

    .line 117
    invoke-static {p1, v0, v8}, Ldhr;->b(Lcom/google/android/shared/search/Query;II)Ldhs;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Ldhs;->byd:Z

    invoke-virtual {v0}, Ldhs;->abZ()Ldhr;

    move-result-object v0

    .line 119
    new-instance v1, Ldhq;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, Ldhq;-><init>(Ldgk;I)V

    .line 121
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 126
    :cond_3
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 128
    new-instance v2, Ldho;

    invoke-direct {v2, p0, p2}, Ldho;-><init>(Ldhn;Ldgk;)V

    .line 156
    const/4 v1, 0x5

    const/4 v3, 0x3

    iget-boolean v0, p0, Ldhn;->bxR:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldhn;->mShouldQueryStrategy:Ldfz;

    iget-object v0, v0, Ldfz;->bwM:Ljava/lang/String;

    invoke-static {v0}, Lhgn;->nP(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v4, p0, Ldhn;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v4}, Ldfz;->abI()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v4

    :goto_2
    iget-object v0, p0, Ldhn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Iv()I

    move-result v5

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Ldhn;->a(Lcom/google/android/shared/search/Query;ILdgk;ILjava/util/Set;ILjava/util/List;Ljava/util/Set;)V

    .line 161
    iget-object v0, p0, Ldhn;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abG()Z

    move-result v0

    if-nez v0, :cond_8

    .line 164
    const/4 v1, 0x3

    iget-object v0, p0, Ldhn;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abF()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldhn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->GP()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v4, Ldhn;->bxQ:Ljava/util/Set;

    :goto_3
    iget-object v0, p0, Ldhn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->MP()I

    move-result v5

    move-object v0, p1

    move-object v2, p2

    move v3, v8

    invoke-static/range {v0 .. v7}, Ldhn;->a(Lcom/google/android/shared/search/Query;ILdgk;ILjava/util/Set;ILjava/util/List;Ljava/util/Set;)V

    .line 168
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 169
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ldgk;->fL(I)V

    goto/16 :goto_1

    .line 156
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    goto :goto_2

    .line 164
    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    goto :goto_3

    .line 171
    :cond_6
    iget-object v0, p0, Ldhn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->It()I

    move-result v0

    invoke-static {p1, v0, v8}, Ldhr;->b(Lcom/google/android/shared/search/Query;II)Ldhs;

    move-result-object v1

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->auC:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Ldhs;->aq(Ljava/lang/String;Ljava/lang/String;)Ldhs;

    goto :goto_4

    :cond_7
    invoke-virtual {v1}, Ldhs;->abZ()Ldhr;

    move-result-object v0

    new-instance v1, Ldhq;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, Ldhq;-><init>(Ldgk;I)V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 175
    :cond_8
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ldgk;->fL(I)V

    .line 176
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Ldgk;->fL(I)V

    goto/16 :goto_1
.end method
