.class final enum Lakt;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum acf:Lakt;

.field public static final enum acg:Lakt;

.field public static final enum ach:Lakt;

.field private static enum aci:Lakt;

.field public static final enum acj:Lakt;

.field public static final enum ack:Lakt;

.field public static final enum acl:Lakt;

.field private static final synthetic acm:[Lakt;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lakt;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->acf:Lakt;

    .line 31
    new-instance v0, Lakt;

    const-string v1, "CONNECTED_SERVICE"

    invoke-direct {v0, v1, v4}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->acg:Lakt;

    .line 32
    new-instance v0, Lakt;

    const-string v1, "CONNECTED_LOCAL"

    invoke-direct {v0, v1, v5}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->ach:Lakt;

    .line 33
    new-instance v0, Lakt;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v6}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->aci:Lakt;

    .line 34
    new-instance v0, Lakt;

    const-string v1, "PENDING_CONNECTION"

    invoke-direct {v0, v1, v7}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->acj:Lakt;

    .line 35
    new-instance v0, Lakt;

    const-string v1, "PENDING_DISCONNECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->ack:Lakt;

    .line 36
    new-instance v0, Lakt;

    const-string v1, "DISCONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakt;->acl:Lakt;

    .line 29
    const/4 v0, 0x7

    new-array v0, v0, [Lakt;

    sget-object v1, Lakt;->acf:Lakt;

    aput-object v1, v0, v3

    sget-object v1, Lakt;->acg:Lakt;

    aput-object v1, v0, v4

    sget-object v1, Lakt;->ach:Lakt;

    aput-object v1, v0, v5

    sget-object v1, Lakt;->aci:Lakt;

    aput-object v1, v0, v6

    sget-object v1, Lakt;->acj:Lakt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lakt;->ack:Lakt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lakt;->acl:Lakt;

    aput-object v2, v0, v1

    sput-object v0, Lakt;->acm:[Lakt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lakt;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lakt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lakt;

    return-object v0
.end method

.method public static values()[Lakt;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lakt;->acm:[Lakt;

    invoke-virtual {v0}, [Lakt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lakt;

    return-object v0
.end method
