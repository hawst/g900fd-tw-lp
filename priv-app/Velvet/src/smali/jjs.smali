.class public final Ljjs;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile epo:[Ljjs;


# instance fields
.field private aez:I

.field private epp:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 930
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 931
    const/4 v0, 0x0

    iput v0, p0, Ljjs;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljjs;->epp:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljjs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjs;->eCz:I

    .line 932
    return-void
.end method

.method public static boc()[Ljjs;
    .locals 2

    .prologue
    .line 898
    sget-object v0, Ljjs;->epo:[Ljjs;

    if-nez v0, :cond_1

    .line 899
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 901
    :try_start_0
    sget-object v0, Ljjs;->epo:[Ljjs;

    if-nez v0, :cond_0

    .line 902
    const/4 v0, 0x0

    new-array v0, v0, [Ljjs;

    sput-object v0, Ljjs;->epo:[Ljjs;

    .line 904
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 906
    :cond_1
    sget-object v0, Ljjs;->epo:[Ljjs;

    return-object v0

    .line 904
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 892
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljjs;->epp:J

    iget v0, p0, Ljjs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjs;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 945
    iget v0, p0, Ljjs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 946
    const/4 v0, 0x1

    iget-wide v2, p0, Ljjs;->epp:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 948
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 949
    return-void
.end method

.method public final bod()J
    .locals 2

    .prologue
    .line 914
    iget-wide v0, p0, Ljjs;->epp:J

    return-wide v0
.end method

.method public final dx(J)Ljjs;
    .locals 1

    .prologue
    .line 917
    iput-wide p1, p0, Ljjs;->epp:J

    .line 918
    iget v0, p0, Ljjs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjs;->aez:I

    .line 919
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 953
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 954
    iget v1, p0, Ljjs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 955
    const/4 v1, 0x1

    iget-wide v2, p0, Ljjs;->epp:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 958
    :cond_0
    return v0
.end method
