.class public final Lhip;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aOw:Ljava/util/concurrent/Executor;

.field volatile bf:Z

.field dgx:Landroid/media/MediaPlayer;

.field final dig:[B
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final dih:Landroid/os/ConditionVariable;

.field final dii:Landroid/os/ConditionVariable;

.field volatile dij:Lhiu;

.field final mAudioRouter:Lhhu;

.field final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lhhu;[BLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 4
    .param p1    # Lhhu;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lhip;->dig:[B

    .line 51
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lhip;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 52
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lhip;->aOw:Ljava/util/concurrent/Executor;

    .line 53
    iput-object p1, p0, Lhip;->mAudioRouter:Lhhu;

    .line 55
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lhip;->dih:Landroid/os/ConditionVariable;

    .line 56
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lhip;->dii:Landroid/os/ConditionVariable;

    .line 62
    iget-object v0, p0, Lhip;->aOw:Ljava/util/concurrent/Executor;

    new-instance v1, Lhiq;

    const-string v2, "Play byte array"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lhiq;-><init>(Lhip;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 68
    return-void

    .line 62
    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method


# virtual methods
.method public final a(Lhiu;)V
    .locals 1

    .prologue
    .line 72
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhiu;

    iput-object v0, p0, Lhip;->dij:Lhiu;

    .line 73
    iget-object v0, p0, Lhip;->dih:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 74
    return-void
.end method

.method finish()V
    .locals 1

    .prologue
    .line 148
    :try_start_0
    iget-object v0, p0, Lhip;->dgx:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    iget-object v0, p0, Lhip;->dgx:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 153
    iget-object v0, p0, Lhip;->mAudioRouter:Lhhu;

    invoke-interface {v0}, Lhhu;->aPp()V

    .line 154
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
