.class public final Lkbh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final eLq:Ljava/lang/String;

.field private synthetic eQq:Lkbb;

.field private final eQy:J

.field private final eQz:[Ljava/io/InputStream;


# direct methods
.method private constructor <init>(Lkbb;Ljava/lang/String;J[Ljava/io/InputStream;[J)V
    .locals 1

    .prologue
    .line 671
    iput-object p1, p0, Lkbh;->eQq:Lkbb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672
    iput-object p2, p0, Lkbh;->eLq:Ljava/lang/String;

    .line 673
    iput-wide p3, p0, Lkbh;->eQy:J

    .line 674
    iput-object p5, p0, Lkbh;->eQz:[Ljava/io/InputStream;

    .line 675
    return-void
.end method

.method synthetic constructor <init>(Lkbb;Ljava/lang/String;J[Ljava/io/InputStream;[JB)V
    .locals 1

    .prologue
    .line 665
    invoke-direct/range {p0 .. p6}, Lkbh;-><init>(Lkbb;Ljava/lang/String;J[Ljava/io/InputStream;[J)V

    return-void
.end method


# virtual methods
.method public final byc()Lkbe;
    .locals 4

    .prologue
    .line 684
    iget-object v0, p0, Lkbh;->eQq:Lkbb;

    iget-object v1, p0, Lkbh;->eLq:Ljava/lang/String;

    iget-wide v2, p0, Lkbh;->eQy:J

    invoke-static {v0, v1, v2, v3}, Lkbb;->a(Lkbb;Ljava/lang/String;J)Lkbe;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 703
    iget-object v1, p0, Lkbh;->eQz:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 704
    invoke-static {v3}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 706
    :cond_0
    return-void
.end method

.method public final tJ(I)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lkbh;->eQz:[Ljava/io/InputStream;

    aget-object v0, v0, p1

    return-object v0
.end method
