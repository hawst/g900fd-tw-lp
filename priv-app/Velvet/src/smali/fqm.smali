.class public final Lfqm;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static k(Ljava/lang/Iterable;)Liwl;
    .locals 10

    .prologue
    .line 21
    new-instance v1, Liwl;

    invoke-direct {v1}, Liwl;-><init>()V

    .line 22
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 23
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapz;

    .line 24
    iget-object v4, v0, Lapz;->ame:Ljde;

    .line 25
    new-instance v5, Liwk;

    invoke-direct {v5}, Liwk;-><init>()V

    .line 26
    const/16 v6, 0x21

    invoke-virtual {v5, v6}, Liwk;->nb(I)Liwk;

    .line 27
    new-instance v6, Lizv;

    invoke-direct {v6}, Lizv;-><init>()V

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4}, Ljde;->bha()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lizv;->cw(J)Lizv;

    move-result-object v6

    .line 31
    iput-object v5, v6, Lizv;->dVr:Liwk;

    .line 32
    iput-object v4, v6, Lizv;->dVI:Ljde;

    .line 33
    iget-object v4, v0, Lapz;->entry:Lizj;

    if-eqz v4, :cond_0

    .line 34
    iget-object v0, v0, Lapz;->entry:Lizj;

    iput-object v0, v6, Lizv;->entry:Lizj;

    .line 36
    :cond_0
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizv;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizv;

    iput-object v0, v1, Liwl;->afC:[Lizv;

    .line 39
    return-object v1
.end method
