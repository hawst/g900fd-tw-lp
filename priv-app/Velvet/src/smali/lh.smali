.class final Llh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lml;
.implements Lmy;


# instance fields
.field private final kA:Llj;

.field private final kB:Lli;

.field final kC:Lmo;

.field private final kD:Z

.field kE:Lmi;

.field private kF:Llm;

.field kG:Llm;

.field kH:Lkx;

.field private kj:Lks;

.field final kp:Ljava/util/ArrayList;

.field final kw:Ljava/util/ArrayList;

.field final kx:Ljava/util/ArrayList;

.field private final ky:Ljava/util/ArrayList;

.field final kz:Lmn;

.field final mApplicationContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1503
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    .line 1505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    .line 1506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    .line 1508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llh;->ky:Ljava/util/ArrayList;

    .line 1510
    new-instance v0, Lmn;

    invoke-direct {v0}, Lmn;-><init>()V

    iput-object v0, p0, Llh;->kz:Lmn;

    .line 1512
    new-instance v0, Llj;

    invoke-direct {v0, p0, v1}, Llj;-><init>(Llh;B)V

    iput-object v0, p0, Llh;->kA:Llj;

    .line 1513
    new-instance v0, Lli;

    invoke-direct {v0, p0, v1}, Lli;-><init>(Llh;B)V

    iput-object v0, p0, Llh;->kB:Lli;

    .line 1526
    iput-object p1, p0, Llh;->mApplicationContext:Landroid/content/Context;

    .line 1527
    invoke-static {p1}, Ldb;->b(Landroid/content/Context;)Ldb;

    .line 1528
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Llh;->kD:Z

    .line 1535
    invoke-static {p1, p0}, Lmo;->a(Landroid/content/Context;Lmy;)Lmo;

    move-result-object v0

    iput-object v0, p0, Llh;->kC:Lmo;

    .line 1536
    iget-object v0, p0, Llh;->kC:Lmo;

    invoke-virtual {p0, v0}, Llh;->a(Lkt;)V

    .line 1537
    return-void

    :cond_0
    move v0, v1

    .line 1528
    goto :goto_0
.end method

.method private static b(Llm;)Z
    .locals 1

    .prologue
    .line 1992
    iget-object v0, p0, Llm;->kW:Lkq;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Llm;->hO:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bQ()Llm;
    .locals 4

    .prologue
    .line 1973
    iget-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    .line 1974
    iget-object v1, p0, Llh;->kF:Llm;

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Llm;->bS()Lkt;

    move-result-object v1

    iget-object v3, p0, Llh;->kC:Lmo;

    if-ne v1, v3, :cond_1

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Llm;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Llm;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v0}, Llh;->b(Llm;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1980
    :goto_1
    return-object v0

    .line 1974
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1980
    :cond_2
    iget-object v0, p0, Llh;->kF:Llm;

    goto :goto_1
.end method

.method private bR()V
    .locals 3

    .prologue
    .line 2086
    iget-object v0, p0, Llh;->kG:Llm;

    if-eqz v0, :cond_0

    .line 2087
    iget-object v0, p0, Llh;->kz:Lmn;

    iget-object v1, p0, Llh;->kG:Llm;

    iget v1, v1, Llm;->kT:I

    iput v1, v0, Lmn;->lH:I

    .line 2088
    iget-object v0, p0, Llh;->kz:Lmn;

    iget-object v1, p0, Llh;->kG:Llm;

    iget v1, v1, Llm;->kU:I

    iput v1, v0, Lmn;->lI:I

    .line 2089
    iget-object v0, p0, Llh;->kz:Lmn;

    iget-object v1, p0, Llh;->kG:Llm;

    iget v1, v1, Llm;->kS:I

    iput v1, v0, Lmn;->lJ:I

    .line 2090
    iget-object v0, p0, Llh;->kz:Lmn;

    iget-object v0, p0, Llh;->kG:Llm;

    iget v0, v0, Llm;->kR:I

    .line 2091
    iget-object v0, p0, Llh;->kz:Lmn;

    iget-object v0, p0, Llh;->kG:Llm;

    iget v0, v0, Llm;->kQ:I

    .line 2093
    iget-object v0, p0, Llh;->ky:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2094
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2095
    iget-object v0, p0, Llh;->ky:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llk;

    .line 2096
    iget-object v0, v0, Llk;->kJ:Llh;

    iget-object v0, v0, Llh;->kz:Lmn;

    .line 2094
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2098
    :cond_0
    return-void
.end method

.method private c(Llm;)V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Llh;->kG:Llm;

    if-eq v0, p1, :cond_5

    .line 2003
    iget-object v0, p0, Llh;->kG:Llm;

    if-eqz v0, :cond_1

    .line 2004
    invoke-static {}, Lld;->bO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route unselected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Llh;->kG:Llm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2007
    :cond_0
    iget-object v0, p0, Llh;->kB:Lli;

    const/16 v1, 0x107

    iget-object v2, p0, Llh;->kG:Llm;

    invoke-virtual {v0, v1, v2}, Lli;->b(ILjava/lang/Object;)V

    .line 2008
    iget-object v0, p0, Llh;->kH:Lkx;

    if-eqz v0, :cond_1

    .line 2009
    iget-object v0, p0, Llh;->kH:Lkx;

    invoke-virtual {v0}, Lkx;->bE()V

    .line 2010
    iget-object v0, p0, Llh;->kH:Lkx;

    invoke-virtual {v0}, Lkx;->onRelease()V

    .line 2011
    const/4 v0, 0x0

    iput-object v0, p0, Llh;->kH:Lkx;

    .line 2015
    :cond_1
    iput-object p1, p0, Llh;->kG:Llm;

    .line 2017
    iget-object v0, p0, Llh;->kG:Llm;

    if-eqz v0, :cond_4

    .line 2018
    invoke-virtual {p1}, Llm;->bS()Lkt;

    move-result-object v0

    iget-object v1, p1, Llm;->kM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lkt;->g(Ljava/lang/String;)Lkx;

    move-result-object v0

    iput-object v0, p0, Llh;->kH:Lkx;

    .line 2020
    iget-object v0, p0, Llh;->kH:Lkx;

    if-eqz v0, :cond_2

    .line 2021
    iget-object v0, p0, Llh;->kH:Lkx;

    invoke-virtual {v0}, Lkx;->bD()V

    .line 2023
    :cond_2
    invoke-static {}, Lld;->bO()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2024
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route selected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Llh;->kG:Llm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2026
    :cond_3
    iget-object v0, p0, Llh;->kB:Lli;

    const/16 v1, 0x106

    iget-object v2, p0, Llh;->kG:Llm;

    invoke-virtual {v0, v1, v2}, Lli;->b(ILjava/lang/Object;)V

    .line 2029
    :cond_4
    invoke-direct {p0}, Llh;->bR()V

    .line 2031
    :cond_5
    return-void
.end method

.method private i(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1925
    iget-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1926
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1927
    iget-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    iget-object v0, v0, Llm;->kN:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1931
    :goto_1
    return v0

    .line 1926
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1931
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private k(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1936
    iget-object v0, p0, Llh;->kF:Llm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llh;->kF:Llm;

    invoke-static {v0}, Llh;->b(Llm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Clearing the default route because it is no longer selectable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Llh;->kF:Llm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    iput-object v4, p0, Llh;->kF:Llm;

    .line 1941
    :cond_0
    iget-object v0, p0, Llh;->kF:Llm;

    if-nez v0, :cond_2

    iget-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1942
    iget-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    .line 1943
    invoke-virtual {v0}, Llm;->bS()Lkt;

    move-result-object v1

    iget-object v3, p0, Llh;->kC:Lmo;

    if-ne v1, v3, :cond_5

    iget-object v1, v0, Llm;->kM:Ljava/lang/String;

    const-string v3, "DEFAULT_ROUTE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v0}, Llh;->b(Llm;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1944
    iput-object v0, p0, Llh;->kF:Llm;

    .line 1945
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found default route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Llh;->kF:Llm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    :cond_2
    iget-object v0, p0, Llh;->kG:Llm;

    if-eqz v0, :cond_3

    iget-object v0, p0, Llh;->kG:Llm;

    invoke-static {v0}, Llh;->b(Llm;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1953
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unselecting the current route because it is no longer selectable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Llh;->kG:Llm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    invoke-direct {p0, v4}, Llh;->c(Llm;)V

    .line 1957
    :cond_3
    iget-object v0, p0, Llh;->kG:Llm;

    if-nez v0, :cond_6

    .line 1961
    invoke-direct {p0}, Llh;->bQ()Llm;

    move-result-object v0

    invoke-direct {p0, v0}, Llh;->c(Llm;)V

    .line 1966
    :cond_4
    :goto_1
    return-void

    .line 1943
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 1962
    :cond_6
    if-eqz p1, :cond_4

    .line 1964
    invoke-direct {p0}, Llh;->bR()V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lkt;)V
    .locals 3

    .prologue
    .line 1742
    invoke-virtual {p0, p1}, Llh;->c(Lkt;)I

    move-result v0

    .line 1743
    if-gez v0, :cond_1

    .line 1745
    new-instance v0, Lll;

    invoke-direct {v0, p1}, Lll;-><init>(Lkt;)V

    .line 1746
    iget-object v1, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1747
    invoke-static {}, Lld;->bO()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1748
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provider added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1750
    :cond_0
    iget-object v1, p0, Llh;->kB:Lli;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, Lli;->b(ILjava/lang/Object;)V

    .line 1752
    iget-object v1, p1, Lkt;->kl:Lky;

    invoke-virtual {p0, v0, v1}, Llh;->a(Lll;Lky;)V

    .line 1754
    iget-object v0, p0, Llh;->kA:Llj;

    invoke-virtual {p1, v0}, Lkt;->a(Lku;)V

    .line 1756
    iget-object v0, p0, Llh;->kj:Lks;

    invoke-virtual {p1, v0}, Lkt;->a(Lks;)V

    .line 1758
    :cond_1
    return-void
.end method

.method a(Lll;Lky;)V
    .locals 16

    .prologue
    .line 1802
    move-object/from16 v0, p1

    iget-object v2, v0, Lll;->kl:Lky;

    move-object/from16 v0, p2

    if-eq v2, v0, :cond_1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    iput-object v0, v1, Lll;->kl:Lky;

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_12

    .line 1805
    const/4 v7, 0x0

    .line 1806
    const/4 v6, 0x0

    .line 1807
    if-eqz p2, :cond_d

    .line 1808
    invoke-virtual/range {p2 .. p2}, Lky;->isValid()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1809
    invoke-virtual/range {p2 .. p2}, Lky;->getRoutes()Ljava/util/List;

    move-result-object v9

    .line 1811
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    .line 1812
    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v10, :cond_d

    .line 1813
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkq;

    .line 1814
    invoke-virtual {v2}, Lkq;->getId()Ljava/lang/String;

    move-result-object v11

    .line 1815
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lll;->k(Ljava/lang/String;)I

    move-result v5

    .line 1816
    if-gez v5, :cond_4

    .line 1818
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v4, v0, Lll;->kg:Lkw;

    iget-object v4, v4, Lkw;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Llh;->i(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_2

    move-object v3, v4

    .line 1819
    :goto_2
    new-instance v4, Llm;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v11, v3}, Llm;-><init>(Lll;Ljava/lang/String;Ljava/lang/String;)V

    .line 1820
    move-object/from16 v0, p1

    iget-object v5, v0, Lll;->kp:Ljava/util/ArrayList;

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v5, v7, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1821
    move-object/from16 v0, p0

    iget-object v5, v0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1823
    invoke-virtual {v4, v2}, Llm;->b(Lkq;)I

    .line 1825
    invoke-static {}, Lld;->bO()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1826
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Route added: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1828
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Llh;->kB:Lli;

    const/16 v5, 0x101

    invoke-virtual {v2, v5, v4}, Lli;->b(ILjava/lang/Object;)V

    move v2, v6

    .line 1812
    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v6, v2

    move v7, v3

    goto :goto_1

    .line 1802
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1818
    :cond_2
    const/4 v3, 0x2

    :goto_4
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "%s_%d"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v5, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Llh;->i(Ljava/lang/String;)I

    move-result v12

    if-gez v12, :cond_3

    move-object v3, v5

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1829
    :cond_4
    if-ge v5, v7, :cond_5

    .line 1830
    const-string v3, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Ignoring route descriptor with duplicate id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v6

    move v3, v7

    goto :goto_3

    .line 1834
    :cond_5
    move-object/from16 v0, p1

    iget-object v3, v0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llm;

    .line 1835
    move-object/from16 v0, p1

    iget-object v11, v0, Lll;->kp:Ljava/util/ArrayList;

    add-int/lit8 v4, v7, 0x1

    invoke-static {v11, v5, v7}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1838
    invoke-virtual {v3, v2}, Llm;->b(Lkq;)I

    move-result v2

    .line 1840
    if-eqz v2, :cond_13

    .line 1841
    and-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_7

    .line 1842
    invoke-static {}, Lld;->bO()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1843
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Route changed: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1845
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Llh;->kB:Lli;

    const/16 v7, 0x103

    invoke-virtual {v5, v7, v3}, Lli;->b(ILjava/lang/Object;)V

    .line 1848
    :cond_7
    and-int/lit8 v5, v2, 0x2

    if-eqz v5, :cond_9

    .line 1849
    invoke-static {}, Lld;->bO()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1850
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Route volume changed: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1852
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Llh;->kB:Lli;

    const/16 v7, 0x104

    invoke-virtual {v5, v7, v3}, Lli;->b(ILjava/lang/Object;)V

    .line 1855
    :cond_9
    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_b

    .line 1856
    invoke-static {}, Lld;->bO()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1857
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Route presentation display changed: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1860
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Llh;->kB:Lli;

    const/16 v5, 0x105

    invoke-virtual {v2, v5, v3}, Lli;->b(ILjava/lang/Object;)V

    .line 1863
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Llh;->kG:Llm;

    if-ne v3, v2, :cond_13

    .line 1864
    const/4 v2, 0x1

    move v3, v4

    goto/16 :goto_3

    .line 1870
    :cond_c
    const-string v2, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring invalid provider descriptor: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    :cond_d
    move-object/from16 v0, p1

    iget-object v2, v0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_5
    if-lt v3, v7, :cond_e

    .line 1877
    move-object/from16 v0, p1

    iget-object v2, v0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llm;

    .line 1878
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Llm;->b(Lkq;)I

    .line 1880
    move-object/from16 v0, p0

    iget-object v4, v0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1875
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_5

    .line 1884
    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Llh;->k(Z)V

    .line 1891
    move-object/from16 v0, p1

    iget-object v2, v0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_6
    if-lt v3, v7, :cond_10

    .line 1892
    move-object/from16 v0, p1

    iget-object v2, v0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llm;

    .line 1893
    invoke-static {}, Lld;->bO()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1894
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Route removed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1896
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Llh;->kB:Lli;

    const/16 v5, 0x102

    invoke-virtual {v4, v5, v2}, Lli;->b(ILjava/lang/Object;)V

    .line 1891
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_6

    .line 1900
    :cond_10
    invoke-static {}, Lld;->bO()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1901
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider changed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1903
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Llh;->kB:Lli;

    const/16 v3, 0x203

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Lli;->b(ILjava/lang/Object;)V

    .line 1905
    :cond_12
    return-void

    :cond_13
    move v2, v6

    move v3, v4

    goto/16 :goto_3
.end method

.method public final a(Llm;)V
    .locals 3

    .prologue
    .line 1637
    iget-object v0, p0, Llh;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1638
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select removed route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    :goto_0
    return-void

    .line 1641
    :cond_0
    iget-boolean v0, p1, Llm;->hO:Z

    if-nez v0, :cond_1

    .line 1642
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select disabled route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1646
    :cond_1
    invoke-direct {p0, p1}, Llh;->c(Llm;)V

    goto :goto_0
.end method

.method public final b(Lkt;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1762
    invoke-virtual {p0, p1}, Llh;->c(Lkt;)I

    move-result v1

    .line 1763
    if-ltz v1, :cond_1

    .line 1765
    invoke-virtual {p1, v2}, Lkt;->a(Lku;)V

    .line 1767
    invoke-virtual {p1, v2}, Lkt;->a(Lks;)V

    .line 1769
    iget-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lll;

    .line 1770
    invoke-virtual {p0, v0, v2}, Llh;->a(Lll;Lky;)V

    .line 1772
    invoke-static {}, Lld;->bO()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1773
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider removed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1775
    :cond_0
    iget-object v2, p0, Llh;->kB:Lli;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, Lli;->b(ILjava/lang/Object;)V

    .line 1776
    iget-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1778
    :cond_1
    return-void
.end method

.method public final bL()Llm;
    .locals 2

    .prologue
    .line 1615
    iget-object v0, p0, Llh;->kF:Llm;

    if-nez v0, :cond_0

    .line 1619
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1622
    :cond_0
    iget-object v0, p0, Llh;->kF:Llm;

    return-object v0
.end method

.method public final bM()Llm;
    .locals 2

    .prologue
    .line 1626
    iget-object v0, p0, Llh;->kG:Llm;

    if-nez v0, :cond_0

    .line 1630
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1633
    :cond_0
    iget-object v0, p0, Llh;->kG:Llm;

    return-object v0
.end method

.method public final bP()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1678
    .line 1680
    new-instance v8, Llc;

    invoke-direct {v8}, Llc;-><init>()V

    .line 1681
    iget-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v5

    move v4, v5

    :goto_0
    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_5

    .line 1682
    iget-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lld;

    .line 1683
    if-nez v0, :cond_0

    .line 1684
    iget-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v7

    goto :goto_0

    .line 1686
    :cond_0
    iget-object v1, v0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v5

    .line 1687
    :goto_1
    if-ge v6, v9, :cond_4

    .line 1688
    iget-object v1, v0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llf;

    .line 1689
    iget-object v10, v1, Llf;->jv:Llb;

    invoke-virtual {v8, v10}, Llc;->d(Llb;)Llc;

    .line 1690
    iget v10, v1, Llf;->cZ:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    move v2, v3

    move v4, v3

    .line 1694
    :cond_1
    iget v10, v1, Llf;->cZ:I

    and-int/lit8 v10, v10, 0x4

    if-eqz v10, :cond_2

    .line 1695
    iget-boolean v10, p0, Llh;->kD:Z

    if-nez v10, :cond_2

    move v4, v3

    .line 1699
    :cond_2
    iget v1, v1, Llf;->cZ:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    move v4, v3

    .line 1687
    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_4
    move v0, v7

    .line 1704
    goto :goto_0

    .line 1705
    :cond_5
    if-eqz v4, :cond_7

    invoke-virtual {v8}, Llc;->bK()Llb;

    move-result-object v0

    .line 1708
    :goto_2
    iget-object v1, p0, Llh;->kj:Lks;

    if-eqz v1, :cond_8

    iget-object v1, p0, Llh;->kj:Lks;

    invoke-virtual {v1}, Lks;->bA()Llb;

    move-result-object v1

    invoke-virtual {v1, v0}, Llb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Llh;->kj:Lks;

    invoke-virtual {v1}, Lks;->bC()Z

    move-result v1

    if-ne v1, v2, :cond_8

    .line 1738
    :cond_6
    return-void

    .line 1705
    :cond_7
    sget-object v0, Llb;->kr:Llb;

    goto :goto_2

    .line 1713
    :cond_8
    invoke-virtual {v0}, Llb;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    if-nez v2, :cond_b

    .line 1715
    iget-object v0, p0, Llh;->kj:Lks;

    if-eqz v0, :cond_6

    .line 1718
    const/4 v0, 0x0

    iput-object v0, p0, Llh;->kj:Lks;

    .line 1723
    :goto_3
    invoke-static {}, Lld;->bO()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1724
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Updated discovery request: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Llh;->kj:Lks;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1726
    :cond_9
    if-eqz v4, :cond_a

    if-nez v2, :cond_a

    iget-boolean v0, p0, Llh;->kD:Z

    if-eqz v0, :cond_a

    .line 1727
    const-string v0, "MediaRouter"

    const-string v1, "Forcing passive route discovery on a low-RAM device, system performance may be affected.  Please consider using CALLBACK_FLAG_REQUEST_DISCOVERY instead of CALLBACK_FLAG_FORCE_DISCOVERY."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    :cond_a
    iget-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    .line 1735
    :goto_4
    if-ge v1, v2, :cond_6

    .line 1736
    iget-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lll;

    iget-object v0, v0, Lll;->kK:Lkt;

    iget-object v3, p0, Llh;->kj:Lks;

    invoke-virtual {v0, v3}, Lkt;->a(Lks;)V

    .line 1735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1721
    :cond_b
    new-instance v1, Lks;

    invoke-direct {v1, v0, v2}, Lks;-><init>(Llb;Z)V

    iput-object v1, p0, Llh;->kj:Lks;

    goto :goto_3
.end method

.method c(Lkt;)I
    .locals 3

    .prologue
    .line 1791
    iget-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1792
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1793
    iget-object v0, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lll;

    iget-object v0, v0, Lll;->kK:Lkt;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 1797
    :goto_1
    return v0

    .line 1792
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1797
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final e(Landroid/content/Context;)Lld;
    .locals 3

    .prologue
    .line 1549
    iget-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 1550
    iget-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lld;

    .line 1551
    if-nez v0, :cond_0

    .line 1552
    iget-object v0, p0, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 1553
    :cond_0
    iget-object v2, v0, Lld;->mContext:Landroid/content/Context;

    if-ne v2, p1, :cond_2

    .line 1559
    :goto_1
    return-object v0

    .line 1557
    :cond_1
    new-instance v0, Lld;

    invoke-direct {v0, p1}, Lld;-><init>(Landroid/content/Context;)V

    .line 1558
    iget-object v1, p0, Llh;->kw:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final j(Ljava/lang/String;)Llm;
    .locals 2

    .prologue
    .line 2035
    iget-object v0, p0, Llh;->kC:Lmo;

    invoke-virtual {p0, v0}, Llh;->c(Lkt;)I

    move-result v0

    .line 2036
    if-ltz v0, :cond_0

    .line 2037
    iget-object v1, p0, Llh;->kx:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lll;

    .line 2038
    invoke-virtual {v0, p1}, Lll;->k(Ljava/lang/String;)I

    move-result v1

    .line 2039
    if-ltz v1, :cond_0

    .line 2040
    iget-object v0, v0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    .line 2043
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
