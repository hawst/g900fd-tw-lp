.class public Ldjk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgh;


# instance fields
.field private final aVQ:Ldgc;

.field private final aXe:Ljava/util/concurrent/Executor;

.field private final bAb:Ldjo;

.field final bAf:Landroid/app/SearchManager;

.field private final bAg:Ljava/util/concurrent/Executor;

.field private final bAh:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final bAi:Ljava/util/Map;

.field bAj:Lijj;

.field final mConfig:Lcjs;

.field private final mContext:Landroid/content/Context;

.field final mObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcjs;Ldjo;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ldgc;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Ldjk;->mContext:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Ldjk;->mConfig:Lcjs;

    .line 92
    iput-object p3, p0, Ldjk;->bAb:Ldjo;

    .line 93
    iput-object p4, p0, Ldjk;->bAg:Ljava/util/concurrent/Executor;

    .line 94
    iput-object p5, p0, Ldjk;->aXe:Ljava/util/concurrent/Executor;

    .line 95
    iput-object p6, p0, Ldjk;->aVQ:Ldgc;

    .line 97
    const-string v0, "search"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iput-object v0, p0, Ldjk;->bAf:Landroid/app/SearchManager;

    .line 98
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ldjk;->bAi:Ljava/util/Map;

    .line 99
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Ldjk;->bAj:Lijj;

    .line 100
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Ldjk;->mObservable:Landroid/database/DataSetObservable;

    .line 101
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Ldjk;->bAh:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 102
    return-void
.end method

.method protected constructor <init>(Ljava/util/List;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object v1, p0, Ldjk;->mContext:Landroid/content/Context;

    .line 107
    iput-object v1, p0, Ldjk;->mConfig:Lcjs;

    .line 108
    iput-object v1, p0, Ldjk;->bAf:Landroid/app/SearchManager;

    .line 109
    iput-object v1, p0, Ldjk;->bAb:Ldjo;

    .line 110
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ldjk;->bAi:Ljava/util/Map;

    .line 111
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Ldjk;->mObservable:Landroid/database/DataSetObservable;

    .line 112
    iput-object v1, p0, Ldjk;->bAg:Ljava/util/concurrent/Executor;

    .line 113
    iput-object v1, p0, Ldjk;->aXe:Ljava/util/concurrent/Executor;

    .line 114
    iput-object v1, p0, Ldjk;->aVQ:Ldgc;

    .line 115
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Ldjk;->bAh:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 117
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldiq;

    .line 118
    iget-object v2, p0, Ldjk;->bAi:Ljava/util/Map;

    invoke-interface {v0}, Ldiq;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Ldjk;->bAi:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Ldjk;->bAj:Lijj;

    .line 121
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 281
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 283
    :catch_0
    move-exception v1

    .line 286
    const-string v2, "Search.SearchableSources"

    const-string v3, "Error calling context.checkPermission() - platform bug!?"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private e(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 241
    iget-object v2, p0, Ldjk;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v2

    .line 243
    if-nez v2, :cond_1

    .line 244
    const-string v1, "Search.SearchableSources"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has bad suggestion authority "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    :goto_0
    return v0

    .line 247
    :cond_1
    iget-object v3, v2, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    .line 248
    if-nez v3, :cond_2

    move v0, v1

    .line 250
    goto :goto_0

    .line 252
    :cond_2
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    .line 253
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    .line 254
    iget-object v6, p0, Ldjk;->mContext:Landroid/content/Context;

    invoke-static {v6, v3, v4, v5}, Ldjk;->a(Landroid/content/Context;Ljava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 256
    goto :goto_0

    .line 258
    :cond_3
    iget-object v3, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    .line 259
    if-eqz v3, :cond_0

    array-length v2, v3

    if-eqz v2, :cond_0

    .line 264
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 265
    array-length v7, v3

    move v2, v0

    :goto_1
    if-ge v2, v7, :cond_0

    aget-object v8, v3, v2

    .line 266
    invoke-virtual {v8}, Landroid/content/pm/PathPermission;->getReadPermission()Ljava/lang/String;

    move-result-object v9

    .line 267
    if-eqz v9, :cond_4

    invoke-virtual {v8, v6}, Landroid/content/pm/PathPermission;->match(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Ldjk;->mContext:Landroid/content/Context;

    invoke-static {v8, v9, v4, v5}, Ldjk;->a(Landroid/content/Context;Ljava/lang/String;II)Z

    move-result v8

    if-eqz v8, :cond_4

    move v0, v1

    .line 271
    goto :goto_0

    .line 265
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method a(Landroid/app/SearchableInfo;)Ldiq;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 215
    if-nez p1, :cond_0

    move-object v0, v8

    .line 230
    :goto_0
    return-object v0

    .line 217
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 218
    iget-object v1, p0, Ldjk;->aVQ:Ldgc;

    invoke-static {v0}, Ldgc;->d(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v4

    .line 220
    if-nez p1, :cond_1

    move-object v6, v8

    .line 221
    :goto_1
    if-eqz v6, :cond_4

    invoke-direct {p0, v6, v4}, Ldjk;->e(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    move-object v0, v8

    goto :goto_0

    .line 220
    :cond_1
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v6, v8

    goto :goto_1

    :cond_2
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    const-string v2, "search_suggest_query"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    goto :goto_1

    .line 222
    :cond_4
    iget-object v1, p0, Ldjk;->aVQ:Ldgc;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Ldgc;->d(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 224
    iget-object v1, p0, Ldjk;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 226
    new-instance v0, Ldjj;

    iget-object v2, p0, Ldjk;->mConfig:Lcjs;

    iget-object v7, p0, Ldjk;->bAb:Ldjo;

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Ldjj;-><init>(Landroid/content/pm/ActivityInfo;Lcjs;Landroid/app/SearchableInfo;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ldjo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    const-string v1, "Search.SearchableSources"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Source not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    .line 230
    goto :goto_0
.end method

.method public a(Letj;)V
    .locals 1

    .prologue
    .line 322
    const-string v0, "All CP sources"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Ldjk;->abS()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->f(Ljava/lang/Iterable;)V

    .line 324
    return-void
.end method

.method public abS()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 125
    iget-object v1, p0, Ldjk;->bAi:Ljava/util/Map;

    monitor-enter v1

    .line 126
    :try_start_0
    iget-object v0, p0, Ldjk;->bAj:Lijj;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public acg()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 133
    iget-object v0, p0, Ldjk;->bAh:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 135
    const-string v1, "android.search.action.SEARCHABLES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "android.search.action.SETTINGS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Ldjk;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ldjm;

    invoke-direct {v2, p0}, Ldjm;-><init>(Ldjk;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 140
    :cond_0
    new-instance v0, Ldjl;

    const-string v2, "UpdateSearchableSources"

    iget-object v3, p0, Ldjk;->aXe:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Ldjk;->bAg:Ljava/util/concurrent/Executor;

    new-array v5, v5, [I

    const/4 v1, 0x2

    aput v1, v5, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldjl;-><init>(Ldjk;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldjl;->a([Ljava/lang/Object;)Lenp;

    .line 159
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Ldjk;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 295
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Ldjk;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 301
    return-void
.end method
