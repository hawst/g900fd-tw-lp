.class public final Lfil;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final PROTO_FACTORY:Ligi;


# instance fields
.field private final csR:Lfcf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lfim;

    invoke-direct {v0}, Lfim;-><init>()V

    sput-object v0, Lfil;->PROTO_FACTORY:Ligi;

    return-void
.end method

.method public constructor <init>(Lfcj;Lfck;)V
    .locals 6

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lfcf;

    sget-object v1, Lfil;->PROTO_FACTORY:Ligi;

    const-string v2, "freshen_request_repository_store"

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lfcf;-><init>(Ligi;Ljava/lang/String;Lfcj;Lfck;Z)V

    iput-object v0, p0, Lfil;->csR:Lfcf;

    .line 54
    return-void
.end method

.method static a(Lams;Ljbj;Ljie;)V
    .locals 8
    .param p2    # Ljie;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 160
    invoke-virtual {p1}, Ljbj;->nK()I

    move-result v3

    iget-object v4, p0, Lams;->afx:[Lamt;

    array-length v5, v4

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_4

    aget-object v0, v4, v1

    invoke-virtual {v0}, Lamt;->nL()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lamt;->nK()I

    move-result v6

    if-ne v6, v3, :cond_3

    move-object v1, v0

    .line 163
    :goto_1
    iget-object v0, v1, Lamt;->afA:Ljbj;

    if-nez v0, :cond_5

    .line 167
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    invoke-virtual {p1}, Ljbj;->nL()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljbj;->nK()I

    move-result v2

    invoke-virtual {v0, v2}, Ljbj;->om(I)Ljbj;

    :cond_0
    iget-object v2, p1, Ljbj;->dYx:[I

    iget-object v3, p1, Ljbj;->dYx:[I

    array-length v3, v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    iput-object v2, v0, Ljbj;->dYx:[I

    iput-object v0, v1, Lamt;->afA:Ljbj;

    .line 188
    :cond_1
    if-eqz p2, :cond_2

    .line 189
    iget-object v0, v1, Lamt;->afB:[Ljie;

    invoke-static {v0, p2}, Leqh;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljie;

    iput-object v0, v1, Lamt;->afB:[Ljie;

    .line 192
    :cond_2
    return-void

    .line 160
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    new-instance v0, Lamt;

    invoke-direct {v0}, Lamt;-><init>()V

    invoke-virtual {v0, v3}, Lamt;->cf(I)Lamt;

    move-result-object v1

    iget-object v0, p0, Lams;->afx:[Lamt;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamt;

    iput-object v0, p0, Lams;->afx:[Lamt;

    goto :goto_1

    .line 169
    :cond_5
    iget-object v3, v1, Lamt;->afA:Ljbj;

    .line 173
    new-instance v4, Landroid/util/SparseIntArray;

    iget-object v0, v3, Ljbj;->dYx:[I

    array-length v0, v0

    iget-object v5, p1, Ljbj;->dYx:[I

    array-length v5, v5

    add-int/2addr v0, v5

    invoke-direct {v4, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 175
    iget-object v5, v3, Ljbj;->dYx:[I

    array-length v6, v5

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_6

    aget v7, v5, v0

    .line 176
    invoke-virtual {v4, v7, v7}, Landroid/util/SparseIntArray;->append(II)V

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 178
    :cond_6
    iget-object v5, p1, Ljbj;->dYx:[I

    array-length v6, v5

    move v0, v2

    :goto_3
    if-ge v0, v6, :cond_7

    aget v7, v5, v0

    .line 179
    invoke-virtual {v4, v7, v7}, Landroid/util/SparseIntArray;->append(II)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 182
    :cond_7
    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, v3, Ljbj;->dYx:[I

    move v0, v2

    .line 183
    :goto_4
    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 184
    iget-object v2, v3, Ljbj;->dYx:[I

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v5

    aput v5, v2, v0

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method private a(Landroid/content/Context;Ljbj;ZLjie;)V
    .locals 3
    .param p4    # Ljie;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    if-nez p2, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-virtual {p2}, Ljbj;->nL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lfil;->csR:Lfcf;

    new-instance v1, Lfip;

    invoke-static {p2}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v2

    invoke-direct {v1, v2, p4}, Lfip;-><init>(Ljava/lang/Iterable;Ljie;)V

    invoke-virtual {v0, v1}, Lfcf;->a(Lfcg;)V

    .line 95
    const/16 v0, 0x2c

    invoke-static {v0}, Lgal;->jP(I)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "com.google.android.apps.sidekick.PARTIAL_REFRESH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p3, :cond_2

    const/4 v0, 0x6

    :goto_1
    const-string v2, "com.google.android.apps.sidekick.TYPE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 95
    :cond_2
    const/4 v0, 0x5

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljbj;Ljie;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lfil;->a(Landroid/content/Context;Ljbj;ZLjie;)V

    .line 67
    return-void
.end method

.method public final a(Landroid/content/Context;Ljbj;Z)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lfil;->a(Landroid/content/Context;Ljbj;ZLjie;)V

    .line 80
    return-void
.end method

.method public final a(Lfin;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lfil;->csR:Lfcf;

    new-instance v1, Lfio;

    invoke-direct {v1, p1}, Lfio;-><init>(Lfin;)V

    invoke-virtual {v0, v1}, Lfcf;->a(Lfcg;)V

    .line 128
    return-void
.end method

.method public final aAg()Lijj;
    .locals 6

    .prologue
    .line 104
    iget-object v0, p0, Lfil;->csR:Lfcf;

    invoke-virtual {v0}, Lfcf;->axq()Ljsr;

    move-result-object v0

    check-cast v0, Lams;

    .line 105
    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    .line 111
    :cond_0
    iget-object v1, v0, Lams;->afx:[Lamt;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 112
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_1
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v1

    .line 116
    iget-object v2, v0, Lams;->afx:[Lamt;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 117
    new-instance v5, Lfin;

    invoke-direct {v5, v4}, Lfin;-><init>(Lamt;)V

    invoke-virtual {v1, v5}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 119
    :cond_2
    iget-object v0, v1, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method
