.class public final Ljfr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private eiO:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12548
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12549
    const/4 v0, 0x0

    iput v0, p0, Ljfr;->aez:I

    iput-object v1, p0, Ljfr;->efN:Ljez;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljfr;->eiO:Z

    iput-object v1, p0, Ljfr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfr;->eCz:I

    .line 12550
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 12507
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljfr;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfr;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljfr;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfr;->eiO:Z

    iget v0, p0, Ljfr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfr;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12564
    iget-object v0, p0, Ljfr;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 12565
    const/4 v0, 0x1

    iget-object v1, p0, Ljfr;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 12567
    :cond_0
    iget v0, p0, Ljfr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 12568
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfr;->eiO:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12570
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12571
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12575
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12576
    iget-object v1, p0, Ljfr;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 12577
    const/4 v1, 0x1

    iget-object v2, p0, Ljfr;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12580
    :cond_0
    iget v1, p0, Ljfr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 12581
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfr;->eiO:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12584
    :cond_1
    return v0
.end method
