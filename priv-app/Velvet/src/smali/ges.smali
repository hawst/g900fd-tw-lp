.class public Lges;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private anB:I

.field private cHW:I

.field private cHX:I

.field private cHY:I

.field private cHZ:I

.field private cIa:Lgev;

.field private mAudioStore:Lgfb;

.field private mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method protected a(IILjava/io/InputStream;I)Lgev;
    .locals 1

    .prologue
    .line 196
    new-instance v0, Lgeu;

    invoke-direct {v0, p1, p2, p3, p4}, Lgeu;-><init>(IILjava/io/InputStream;I)V

    return-object v0
.end method

.method public final a(Ljava/io/InputStream;IILgfb;Ljava/lang/String;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lges;->cIa:Lgev;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 70
    iput-object p4, p0, Lges;->mAudioStore:Lgfb;

    .line 71
    iput p2, p0, Lges;->anB:I

    .line 72
    mul-int/lit8 v0, p2, 0x2

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lges;->cHW:I

    .line 73
    iput-object p5, p0, Lges;->mRequestId:Ljava/lang/String;

    .line 74
    iput v1, p0, Lges;->cHY:I

    .line 75
    const v0, 0x7fffffff

    iput v0, p0, Lges;->cHZ:I

    .line 76
    iget v0, p0, Lges;->cHW:I

    mul-int/2addr v0, p6

    .line 77
    iget v1, p0, Lges;->cHW:I

    mul-int/2addr v1, p7

    iput v1, p0, Lges;->cHX:I

    .line 78
    iget v1, p0, Lges;->cHX:I

    invoke-virtual {p0, v0, v1, p1, p3}, Lges;->a(IILjava/io/InputStream;I)Lgev;

    move-result-object v0

    iput-object v0, p0, Lges;->cIa:Lgev;

    .line 80
    iget-object v0, p0, Lges;->cIa:Lgev;

    invoke-interface {v0}, Lgev;->start()V

    .line 81
    return-void

    :cond_0
    move v0, v1

    .line 69
    goto :goto_0
.end method

.method public final aFq()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lges;->cIa:Lgev;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lges;->cIa:Lgev;

    invoke-interface {v0}, Lgev;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lges;->cIa:Lgev;

    invoke-interface {v0}, Lgev;->aFs()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lges;->cIa:Lgev;

    invoke-interface {v0}, Lgev;->aFu()[B

    move-result-object v0

    iget-object v2, p0, Lges;->cIa:Lgev;

    invoke-interface {v2}, Lgev;->aFv()I

    move-result v2

    iget v3, p0, Lges;->cHZ:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lges;->cHZ:I

    :goto_0
    iget v2, p0, Lges;->cHY:I

    iget v3, p0, Lges;->cHZ:I

    if-lt v2, v3, :cond_0

    move-object v0, v1

    :cond_0
    if-eqz v0, :cond_1

    iget v2, p0, Lges;->cHY:I

    if-nez v2, :cond_4

    iget v2, p0, Lges;->cHZ:I

    array-length v3, v0

    if-ne v2, v3, :cond_4

    :cond_1
    move-object v2, v0

    :goto_1
    if-eqz v2, :cond_5

    new-instance v0, Lgfc;

    iget v3, p0, Lges;->anB:I

    invoke-direct {v0, v3, v2}, Lgfc;-><init>(I[B)V

    :goto_2
    iget-object v2, p0, Lges;->mAudioStore:Lgfb;

    iget-object v3, p0, Lges;->mRequestId:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lgfb;->a(Ljava/lang/String;Lgfc;)V

    iput-object v1, p0, Lges;->mAudioStore:Lgfb;

    iput-object v1, p0, Lges;->mRequestId:Ljava/lang/String;

    iput-object v1, p0, Lges;->cIa:Lgev;

    .line 100
    :cond_2
    :goto_3
    return-void

    .line 99
    :catch_0
    move-exception v0

    iput-object v1, p0, Lges;->mAudioStore:Lgfb;

    iput-object v1, p0, Lges;->mRequestId:Ljava/lang/String;

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lges;->cIa:Lgev;

    invoke-interface {v0}, Lgev;->aFt()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lges;->cHZ:I

    iget v2, p0, Lges;->cHX:I

    if-gt v0, v2, :cond_6

    iget-object v0, p0, Lges;->cIa:Lgev;

    invoke-interface {v0}, Lgev;->aFu()[B

    move-result-object v0

    goto :goto_0

    :cond_4
    iget v2, p0, Lges;->cHY:I

    div-int/lit8 v2, v2, 0x2

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lges;->cHZ:I

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method public final aFr()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lges;->cIa:Lgev;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final by(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lges;->cIa:Lgev;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 123
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 124
    iget v0, p0, Lges;->cHW:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lges;->cHY:I

    .line 125
    return-void

    :cond_0
    move v0, v2

    .line 122
    goto :goto_0

    :cond_1
    move v1, v2

    .line 123
    goto :goto_1
.end method
