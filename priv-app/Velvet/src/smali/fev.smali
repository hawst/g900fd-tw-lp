.class public final Lfev;
.super Lfeu;
.source "PG"


# instance fields
.field private final cmw:Lixr;

.field private final cpU:Ljbp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cpV:Ljhk;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mCalendarData:Lamk;

.field private final mDestination:Ljbp;

.field private final mDirectionsLauncher:Lgah;


# direct methods
.method public constructor <init>(Lizj;Lamk;Ljbp;Ljbp;Lgah;)V
    .locals 1
    .param p3    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 48
    iget-object v0, p1, Lizj;->dSf:Lixr;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixr;

    iput-object v0, p0, Lfev;->cmw:Lixr;

    .line 49
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    iput-object v0, p0, Lfev;->mCalendarData:Lamk;

    .line 50
    iput-object p3, p0, Lfev;->cpU:Ljbp;

    .line 51
    iput-object p4, p0, Lfev;->mDestination:Ljbp;

    .line 52
    iput-object p5, p0, Lfev;->mDirectionsLauncher:Lgah;

    .line 53
    iget-object v0, p1, Lizj;->dUv:Ljhk;

    iput-object v0, p0, Lfev;->cpV:Ljhk;

    .line 54
    return-void
.end method

.method private aJ(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 111
    iget-object v1, p0, Lfev;->cmw:Lixr;

    invoke-virtual {v1}, Lixr;->baG()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    iget-object v0, p0, Lfev;->cmw:Lixr;

    invoke-virtual {v0}, Lixr;->baF()I

    move-result v0

    .line 118
    :cond_0
    :goto_0
    iget-object v1, p0, Lfev;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nk()J

    move-result-wide v2

    int-to-long v0, v0

    sub-long v0, v2, v0

    .line 120
    new-instance v2, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 121
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 113
    :cond_1
    iget-object v1, p0, Lfev;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeR:Lamp;

    invoke-virtual {v1}, Lamp;->nA()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v0, p0, Lfev;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeR:Lamp;

    invoke-virtual {v0}, Lamp;->nz()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 70
    invoke-virtual {p0}, Lfev;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lfev;->ayX()Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 73
    :cond_0
    invoke-virtual {p0}, Lfev;->getEntry()Lizj;

    move-result-object v0

    .line 74
    iget-object v1, v0, Lizj;->dUr:Ljcg;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v1}, Ljcg;->bgd()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->bgd()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_1
    const v0, 0x7f0a01e1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfev;->mCalendarData:Lamk;

    iget-object v3, v3, Lamk;->aeQ:Lamo;

    invoke-virtual {v3}, Lamo;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 158
    new-instance v0, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0499

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lfev;->mCalendarData:Lamk;

    iget-object v5, v5, Lamk;->aeQ:Lamo;

    invoke-virtual {v5}, Lamo;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Lfev;->aJ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfev;->mDestination:Ljbp;

    invoke-virtual {v0, v1, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const v4, 0x7f0a01e2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 86
    invoke-virtual {p0}, Lfev;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 100
    :goto_0
    return-object v0

    .line 89
    :cond_0
    iget-object v0, p0, Lfev;->cpV:Ljhk;

    if-nez v0, :cond_1

    .line 90
    new-array v0, v7, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lfev;->aJ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p1, v4, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lfev;->cpV:Ljhk;

    iget-object v0, v0, Ljhk;->elQ:Ljhj;

    invoke-virtual {v0}, Ljhj;->blN()I

    move-result v0

    .line 95
    if-nez v0, :cond_2

    .line 96
    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lfev;->cpV:Ljhk;

    invoke-virtual {v1}, Ljhk;->blO()J

    move-result-wide v2

    invoke-static {p1, v2, v3, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p1, v4, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 100
    :cond_2
    const v1, 0x7f0a01e3

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lfev;->cpV:Ljhk;

    invoke-virtual {v3}, Ljhk;->blO()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    const-string v0, "event"

    return-object v0
.end method

.method public final ayW()I
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lfev;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lfeu;->ayW()I

    move-result v0

    goto :goto_0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lfev;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0202d2

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020224

    goto :goto_0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lfev;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfgb;->cqz:Lfgb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfgb;->cqy:Lfgb;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lfev;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lfev;->cmw:Lixr;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfev;->cmw:Lixr;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 59
    :goto_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v1

    .line 61
    new-instance v2, Lffj;

    iget-object v3, p0, Lfev;->mDirectionsLauncher:Lgah;

    iget-object v4, p0, Lfev;->cpU:Ljbp;

    iget-object v4, p0, Lfev;->mDestination:Ljbp;

    invoke-direct {v2, v1, v3, v4, v0}, Lffj;-><init>(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Lgah;Ljbp;Liyg;)V

    .line 63
    new-instance v0, Lfex;

    iget-object v1, p0, Lfev;->mCalendarData:Lamk;

    invoke-direct {v0, v1}, Lfex;-><init>(Lamk;)V

    .line 64
    invoke-static {v2, v0}, Lijj;->r(Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
