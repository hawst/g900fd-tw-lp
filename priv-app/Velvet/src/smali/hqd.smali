.class public final Lhqd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lekh;


# instance fields
.field private synthetic dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hZ(I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 157
    iget-object v0, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    invoke-virtual {v0, p1}, Lelg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    .line 160
    iget-boolean v1, v0, Lelh;->ceC:Z

    if-eqz v1, :cond_1

    .line 165
    iget-object v0, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-virtual {v0, v4}, Lhpn;->dj(I)V

    .line 166
    iget-object v0, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, v0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v2, v0, Lhpn;->dnE:Lhpq;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lhpn;->dnE:Lhpq;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-interface {v0, v2, v3, v1}, Lhpq;->n(III)V

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v1, v0, Lelh;->ceB:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, v0, Lelh;->ceB:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 177
    :goto_1
    iget-object v0, v0, Lelh;->ceB:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 178
    iget-object v0, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lhpn;->dj(I)V

    goto :goto_0

    .line 171
    :pswitch_0
    iget-object v1, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhpn;->b(Ljava/util/Calendar;)V

    goto :goto_1

    .line 174
    :pswitch_1
    iget-object v1, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1, v2}, Lhpn;->b(Ljava/util/Calendar;)V

    goto :goto_1

    .line 180
    :cond_2
    iget-object v0, p0, Lhqd;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-virtual {v0, v4}, Lhpn;->dj(I)V

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
