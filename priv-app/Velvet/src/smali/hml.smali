.class public final Lhml;
.super Lhlu;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Leoj;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 23
    iput-object p2, p0, Lhml;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 24
    return-void
.end method

.method private static b(Lcom/google/android/search/shared/actions/SetTimerAction;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_TIMER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetTimerAction;->aiD()I

    move-result v1

    if-lez v1, :cond_0

    .line 29
    const-string v1, "android.intent.extra.alarm.LENGTH"

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetTimerAction;->aiD()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 31
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 17
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SET_TIMER"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SetTimerAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetTimerAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    iget-object v2, p0, Lhml;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v3, [Landroid/content/Intent;

    goto :goto_0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SetTimerAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-static {p1}, Lhml;->b(Lcom/google/android/search/shared/actions/SetTimerAction;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SetTimerAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-static {p1}, Lhml;->b(Lcom/google/android/search/shared/actions/SetTimerAction;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method
