.class public abstract Lffx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cqu:Ljava/util/Set;

.field public final cqv:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lffx;->cqu:Ljava/util/Set;

    .line 1163
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lffx;->cqv:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method final a(Lamx;Lemp;)V
    .locals 6

    .prologue
    .line 1188
    invoke-virtual {p1}, Lamx;->nU()Z

    move-result v0

    .line 1189
    iget-object v1, p1, Lamx;->entry:Lizj;

    .line 1190
    invoke-virtual {p0, v1, v0}, Lffx;->b(Lizj;Z)Z

    move-result v2

    .line 1191
    if-eqz v2, :cond_1

    .line 1192
    if-nez v0, :cond_0

    .line 1193
    invoke-interface {p2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {p1, v2, v3}, Lamx;->y(J)Lamx;

    .line 1195
    iget-object v0, p0, Lffx;->cqu:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1203
    :cond_0
    :goto_0
    return-void

    .line 1198
    :cond_1
    if-eqz v0, :cond_0

    .line 1199
    invoke-virtual {p1}, Lamx;->nV()Lamx;

    .line 1200
    iget-object v0, p0, Lffx;->cqv:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final azx()Z
    .locals 1

    .prologue
    .line 1184
    iget-object v0, p0, Lffx;->cqu:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lffx;->cqv:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(Lizj;Z)Z
.end method
