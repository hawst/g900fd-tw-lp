.class public final Lieo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAA:I

.field private dAB:J

.field private dAC:Z

.field private dAv:I

.field private dAw:I

.field private dAx:I

.field private dAy:Liep;

.field private dAz:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 275
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 276
    iput v2, p0, Lieo;->aez:I

    iput v2, p0, Lieo;->dAv:I

    iput v2, p0, Lieo;->dAw:I

    iput v2, p0, Lieo;->dAx:I

    iput-object v3, p0, Lieo;->dAy:Liep;

    iput v0, p0, Lieo;->dAz:I

    iput v0, p0, Lieo;->dAA:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lieo;->dAB:J

    iput-boolean v2, p0, Lieo;->dAC:Z

    iput-object v3, p0, Lieo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lieo;->eCz:I

    .line 277
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lieo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lieo;->dAv:I

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lieo;->dAw:I

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lieo;->dAx:I

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lieo;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lieo;->dAy:Liep;

    if-nez v0, :cond_1

    new-instance v0, Liep;

    invoke-direct {v0}, Liep;-><init>()V

    iput-object v0, p0, Lieo;->dAy:Liep;

    :cond_1
    iget-object v0, p0, Lieo;->dAy:Liep;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lieo;->dAz:I

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lieo;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lieo;->dAA:I

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lieo;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Lieo;->dAB:J

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lieo;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lieo;->dAC:Z

    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lieo;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 297
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 298
    const/4 v0, 0x1

    iget v1, p0, Lieo;->dAv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 300
    :cond_0
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 301
    const/4 v0, 0x2

    iget v1, p0, Lieo;->dAw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 303
    :cond_1
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 304
    const/4 v0, 0x3

    iget v1, p0, Lieo;->dAx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 306
    :cond_2
    iget-object v0, p0, Lieo;->dAy:Liep;

    if-eqz v0, :cond_3

    .line 307
    const/4 v0, 0x4

    iget-object v1, p0, Lieo;->dAy:Liep;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 309
    :cond_3
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 310
    const/4 v0, 0x5

    iget v1, p0, Lieo;->dAz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 312
    :cond_4
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 313
    const/4 v0, 0x6

    iget v1, p0, Lieo;->dAA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 315
    :cond_5
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 316
    const/4 v0, 0x7

    iget-wide v2, p0, Lieo;->dAB:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 318
    :cond_6
    iget v0, p0, Lieo;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 319
    const/16 v0, 0x8

    iget-boolean v1, p0, Lieo;->dAC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 321
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 322
    return-void
.end method

.method public final getDay()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lieo;->dAx:I

    return v0
.end method

.method public final getMonth()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lieo;->dAw:I

    return v0
.end method

.method public final getYear()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lieo;->dAv:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 326
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 327
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 328
    const/4 v1, 0x1

    iget v2, p0, Lieo;->dAv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_0
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 332
    const/4 v1, 0x2

    iget v2, p0, Lieo;->dAw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    :cond_1
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 336
    const/4 v1, 0x3

    iget v2, p0, Lieo;->dAx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    :cond_2
    iget-object v1, p0, Lieo;->dAy:Liep;

    if-eqz v1, :cond_3

    .line 340
    const/4 v1, 0x4

    iget-object v2, p0, Lieo;->dAy:Liep;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    :cond_3
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 344
    const/4 v1, 0x5

    iget v2, p0, Lieo;->dAz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 347
    :cond_4
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 348
    const/4 v1, 0x6

    iget v2, p0, Lieo;->dAA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_5
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 352
    const/4 v1, 0x7

    iget-wide v2, p0, Lieo;->dAB:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 355
    :cond_6
    iget v1, p0, Lieo;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 356
    const/16 v1, 0x8

    iget-boolean v2, p0, Lieo;->dAC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 359
    :cond_7
    return v0
.end method

.method public final lP(I)Lieo;
    .locals 1

    .prologue
    .line 145
    iput p1, p0, Lieo;->dAv:I

    .line 146
    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieo;->aez:I

    .line 147
    return-object p0
.end method

.method public final lQ(I)Lieo;
    .locals 1

    .prologue
    .line 164
    iput p1, p0, Lieo;->dAw:I

    .line 165
    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieo;->aez:I

    .line 166
    return-object p0
.end method

.method public final lR(I)Lieo;
    .locals 1

    .prologue
    .line 183
    iput p1, p0, Lieo;->dAx:I

    .line 184
    iget v0, p0, Lieo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lieo;->aez:I

    .line 185
    return-object p0
.end method
