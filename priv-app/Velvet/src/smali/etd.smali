.class public Letd;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static K(Ljava/lang/CharSequence;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {p0, v0, v1}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a(Ljava/lang/CharSequence;IIC)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 85
    invoke-static {p0, p1, p2}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v1

    .line 86
    if-ne v1, v5, :cond_1

    .line 87
    if-ne p2, p1, :cond_0

    const-string v0, ""

    .line 124
    :goto_0
    return-object v0

    .line 87
    :cond_0
    invoke-static {p3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_1
    const/4 v0, 0x0

    .line 95
    if-eq v1, p1, :cond_2

    .line 97
    add-int/lit8 v2, v1, -0x1

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, p3, :cond_3

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    sub-int v2, p2, v1

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 105
    :cond_2
    :goto_1
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, v2, p2}, Letd;->d(Ljava/lang/CharSequence;II)I

    move-result v2

    .line 106
    if-ne v2, v5, :cond_5

    .line 107
    if-nez v0, :cond_4

    .line 108
    invoke-interface {p0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 100
    :cond_3
    add-int/lit8 p1, v1, -0x1

    goto :goto_1

    .line 110
    :cond_4
    invoke-virtual {v0, p0, v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 112
    :cond_5
    if-eqz v0, :cond_6

    .line 113
    invoke-virtual {v0, p0, v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 115
    :cond_6
    add-int/lit8 v1, v2, 0x1

    invoke-static {p0, v1, p2}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v1

    .line 116
    if-ne v1, v5, :cond_9

    .line 117
    if-nez v0, :cond_8

    .line 119
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, p3, :cond_7

    .line 120
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p0, p1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    sub-int v1, v2, p1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0, p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 124
    :cond_8
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_9
    if-nez v0, :cond_b

    sub-int v3, v1, v2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-eq v3, p3, :cond_b

    .line 128
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    sub-int v3, p2, p1

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0, p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 130
    :cond_b
    if-eqz v0, :cond_2

    .line 131
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static c(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {p0, v0, v1}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v0

    .line 31
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 32
    const-string v0, ""

    .line 34
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/16 v2, 0x20

    invoke-static {p0, v0, v1, v2}, Letd;->a(Ljava/lang/CharSequence;IIC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(C)Z
    .locals 1

    .prologue
    .line 22
    invoke-static {p0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    return v0
.end method

.method static d(Ljava/lang/CharSequence;II)I
    .locals 2

    .prologue
    .line 57
    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_1

    .line 58
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    :goto_1
    return v0

    .line 57
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static d(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-static {p0, v2, v0}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v0

    .line 39
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 40
    const-string v0, ""

    .line 43
    :goto_0
    return-object v0

    .line 42
    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {p0, v2, v1}, Letd;->f(Ljava/lang/CharSequence;II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 43
    const/16 v2, 0x20

    invoke-static {p0, v0, v1, v2}, Letd;->a(Ljava/lang/CharSequence;IIC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static e(Ljava/lang/CharSequence;II)I
    .locals 2

    .prologue
    .line 66
    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_1

    .line 67
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    :goto_1
    return v0

    .line 66
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static f(Ljava/lang/CharSequence;II)I
    .locals 2

    .prologue
    .line 75
    add-int/lit8 v0, p2, -0x1

    :goto_0
    if-lt v0, p1, :cond_1

    .line 76
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    :goto_1
    return v0

    .line 75
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 80
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
