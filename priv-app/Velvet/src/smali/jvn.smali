.class public final Ljvn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ail:I

.field public eHL:Ljvf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50
    iput v0, p0, Ljvn;->aez:I

    iput v0, p0, Ljvn;->ail:I

    iput-object v1, p0, Ljvn;->eHL:Ljvf;

    iput-object v1, p0, Ljvn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvn;->eCz:I

    .line 51
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljvn;->ail:I

    iget v0, p0, Ljvn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvn;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljvn;->eHL:Ljvf;

    if-nez v0, :cond_1

    new-instance v0, Ljvf;

    invoke-direct {v0}, Ljvf;-><init>()V

    iput-object v0, p0, Ljvn;->eHL:Ljvf;

    :cond_1
    iget-object v0, p0, Ljvn;->eHL:Ljvf;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Ljvn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 66
    const/4 v0, 0x1

    iget v1, p0, Ljvn;->ail:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 68
    :cond_0
    iget-object v0, p0, Ljvn;->eHL:Ljvf;

    if-eqz v0, :cond_1

    .line 69
    const/4 v0, 0x2

    iget-object v1, p0, Ljvn;->eHL:Ljvf;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 71
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 72
    return-void
.end method

.method public final getStatus()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Ljvn;->ail:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 77
    iget v1, p0, Ljvn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 78
    const/4 v1, 0x1

    iget v2, p0, Ljvn;->ail:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_0
    iget-object v1, p0, Ljvn;->eHL:Ljvf;

    if-eqz v1, :cond_1

    .line 82
    const/4 v1, 0x2

    iget-object v2, p0, Ljvn;->eHL:Ljvf;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_1
    return v0
.end method

.method public final sA(I)Ljvn;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x2

    iput v0, p0, Ljvn;->ail:I

    .line 34
    iget v0, p0, Ljvn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvn;->aez:I

    .line 35
    return-object p0
.end method
