.class public final Lfbh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mEntryValidator:Lfbp;


# direct methods
.method public constructor <init>(Lfbp;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lfbh;->mEntryValidator:Lfbp;

    .line 23
    return-void
.end method

.method private c(Lizq;)V
    .locals 8

    .prologue
    .line 51
    iget-object v0, p1, Lizq;->dUW:[Lizq;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 52
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 53
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 54
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizq;

    .line 55
    iget-object v1, v0, Lizq;->dUZ:Lizj;

    if-eqz v1, :cond_1

    .line 58
    iget-object v1, p0, Lfbh;->mEntryValidator:Lfbp;

    invoke-virtual {v1, v0}, Lfbp;->e(Lizq;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 63
    :cond_1
    iget-object v1, v0, Lizq;->dUX:[Lizj;

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    .line 64
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 65
    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 66
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizj;

    .line 67
    invoke-virtual {v1}, Lizj;->getType()I

    move-result v2

    const/16 v7, 0x49

    if-eq v2, v7, :cond_3

    const/4 v2, 0x1

    .line 68
    :goto_2
    if-eqz v2, :cond_2

    iget-object v2, p0, Lfbh;->mEntryValidator:Lfbp;

    invoke-virtual {v2, v1}, Lfbp;->d(Lizj;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 69
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 67
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 72
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, v0, Lizq;->dUX:[Lizj;

    array-length v2, v2

    if-eq v1, v2, :cond_5

    .line 73
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lizj;

    invoke-interface {v5, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lizj;

    iput-object v1, v0, Lizq;->dUX:[Lizj;

    .line 77
    :cond_5
    invoke-direct {p0, v0}, Lfbh;->c(Lizq;)V

    .line 80
    iget-object v1, v0, Lizq;->dUX:[Lizj;

    array-length v1, v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lizq;->dUW:[Lizq;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 81
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 85
    :cond_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, Lizq;->dUW:[Lizq;

    array-length v1, v1

    if-eq v0, v1, :cond_7

    .line 86
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizq;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizq;

    iput-object v0, p1, Lizq;->dUW:[Lizq;

    .line 88
    :cond_7
    return-void
.end method


# virtual methods
.method public final d(Lizn;)V
    .locals 2
    .param p1    # Lizn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 30
    if-eqz p1, :cond_0

    iget-object v0, p1, Lizn;->dUI:[Lizo;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lizn;->dUI:[Lizo;

    aget-object v0, v0, v1

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    if-nez v0, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    iget-object v0, p1, Lizn;->dUI:[Lizo;

    aget-object v0, v0, v1

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    .line 37
    invoke-direct {p0, v0}, Lfbh;->c(Lizq;)V

    goto :goto_0
.end method

.method public final g(Lizo;)Lizo;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    invoke-direct {p0, v0}, Lfbh;->c(Lizq;)V

    .line 47
    :cond_0
    return-object p1
.end method
