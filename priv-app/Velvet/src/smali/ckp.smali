.class public final Lckp;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile aZW:[Lckp;


# instance fields
.field private aZX:Ljava/lang/String;

.field private aZY:[B

.field private aZZ:Ljava/lang/String;

.field private aez:I

.field private baa:J

.field private bab:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 136
    const/4 v0, 0x0

    iput v0, p0, Lckp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lckp;->aZX:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lckp;->aZY:[B

    const-string v0, ""

    iput-object v0, p0, Lckp;->aZZ:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lckp;->baa:J

    const-string v0, ""

    iput-object v0, p0, Lckp;->bab:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lckp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lckp;->eCz:I

    .line 137
    return-void
.end method

.method public static OS()[Lckp;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lckp;->aZW:[Lckp;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lckp;->aZW:[Lckp;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lckp;

    sput-object v0, Lckp;->aZW:[Lckp;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lckp;->aZW:[Lckp;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final ON()[B
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lckp;->aZY:[B

    return-object v0
.end method

.method public final OT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lckp;->aZX:Ljava/lang/String;

    return-object v0
.end method

.method public final Y(J)Lckp;
    .locals 1

    .prologue
    .line 100
    iput-wide p1, p0, Lckp;->baa:J

    .line 101
    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lckp;->aez:I

    .line 102
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lckp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lckp;->aZX:Ljava/lang/String;

    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lckp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lckp;->aZY:[B

    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lckp;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lckp;->aZZ:Ljava/lang/String;

    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lckp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lckp;->baa:J

    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lckp;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lckp;->bab:Ljava/lang/String;

    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lckp;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 154
    iget v0, p0, Lckp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x1

    iget-object v1, p0, Lckp;->aZX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 157
    :cond_0
    iget v0, p0, Lckp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 158
    const/4 v0, 0x2

    iget-object v1, p0, Lckp;->aZY:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 160
    :cond_1
    iget v0, p0, Lckp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 161
    const/4 v0, 0x3

    iget-object v1, p0, Lckp;->aZZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 163
    :cond_2
    iget v0, p0, Lckp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 164
    const/4 v0, 0x4

    iget-wide v2, p0, Lckp;->baa:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 166
    :cond_3
    iget v0, p0, Lckp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 167
    const/4 v0, 0x5

    iget-object v1, p0, Lckp;->bab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 169
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 170
    return-void
.end method

.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lckp;->bab:Ljava/lang/String;

    return-object v0
.end method

.method public final getCreationTime()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lckp;->baa:J

    return-wide v0
.end method

.method public final hi(Ljava/lang/String;)Lckp;
    .locals 1

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lckp;->aZX:Ljava/lang/String;

    .line 38
    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lckp;->aez:I

    .line 39
    return-object p0
.end method

.method public final hj(Ljava/lang/String;)Lckp;
    .locals 1

    .prologue
    .line 119
    if-nez p1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 122
    :cond_0
    iput-object p1, p0, Lckp;->bab:Ljava/lang/String;

    .line 123
    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lckp;->aez:I

    .line 124
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 174
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 175
    iget v1, p0, Lckp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 176
    const/4 v1, 0x1

    iget-object v2, p0, Lckp;->aZX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_0
    iget v1, p0, Lckp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 180
    const/4 v1, 0x2

    iget-object v2, p0, Lckp;->aZY:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_1
    iget v1, p0, Lckp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 184
    const/4 v1, 0x3

    iget-object v2, p0, Lckp;->aZZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_2
    iget v1, p0, Lckp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 188
    const/4 v1, 0x4

    iget-wide v2, p0, Lckp;->baa:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_3
    iget v1, p0, Lckp;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 192
    const/4 v1, 0x5

    iget-object v2, p0, Lckp;->bab:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_4
    return v0
.end method

.method public final u([B)Lckp;
    .locals 1

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iput-object p1, p0, Lckp;->aZY:[B

    .line 60
    iget v0, p0, Lckp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lckp;->aez:I

    .line 61
    return-object p0
.end method
