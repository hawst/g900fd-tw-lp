.class final Lcmi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcmc;


# instance fields
.field private synthetic bbQ:Lcmh;


# direct methods
.method constructor <init>(Lcmh;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcmi;->bbQ:Lcmh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final PW()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 115
    .line 120
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->a(Lcmh;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 124
    :try_start_0
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->b(Lcmh;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->c(Lcmh;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 125
    :cond_0
    monitor-exit v4

    .line 170
    :cond_1
    :goto_0
    return-void

    .line 127
    :cond_2
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    iget-object v5, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v5}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v5

    invoke-virtual {v5}, Lcma;->isFailed()Z

    move-result v5

    invoke-static {v0, v5}, Lcmh;->a(Lcmh;Z)Z

    .line 128
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    iget-object v5, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v5}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v5

    invoke-virtual {v5}, Lcma;->isComplete()Z

    move-result v5

    invoke-static {v0, v5}, Lcmh;->b(Lcmh;Z)Z

    .line 129
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->c(Lcmh;)Z

    move-result v5

    .line 130
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->b(Lcmh;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->c(Lcmh;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move v0, v3

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 131
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->b(Lcmh;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v0

    invoke-virtual {v0}, Lcma;->PR()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 132
    :cond_4
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v0

    invoke-virtual {v0}, Lcma;->PR()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v0

    invoke-virtual {v0}, Lcma;->PS()Lefq;

    move-result-object v0

    :goto_2
    move-object v3, v0

    move-object v0, v1

    .line 148
    :goto_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    if-eqz v3, :cond_8

    .line 151
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-virtual {v0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, v3}, Lcmz;->c(Lefq;)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 130
    goto :goto_1

    .line 132
    :cond_6
    :try_start_1
    new-instance v0, Lefs;

    const-string v3, "Failed HttpResponse."

    const v6, 0x30008

    invoke-direct {v0, v3, v6}, Lefs;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 138
    :cond_7
    :try_start_2
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->e(Lcmh;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v0

    invoke-virtual {v0}, Lcma;->hasHeaders()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 141
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcmh;->c(Lcmh;Z)Z

    .line 144
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->d(Lcmh;)Lcma;

    move-result-object v0

    invoke-virtual {v0}, Lcma;->PQ()Ldyj;

    move-result-object v2

    .line 145
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->f(Lcmh;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v7, v2

    move v2, v3

    move-object v3, v1

    move-object v1, v7

    goto :goto_3

    .line 155
    :cond_8
    if-eqz v2, :cond_9

    .line 156
    iget-object v2, p0, Lcmi;->bbQ:Lcmh;

    iget-object v2, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v2}, Lcmh;->g(Lcmh;)Lgpf;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcmh;->a(Lgpf;Ldyj;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    iget-object v2, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v1}, Lcmh;->b(Ldyj;)Leeb;

    move-result-object v1

    invoke-static {v0, v1}, Lcmh;->a(Lcmh;Leeb;)Leeb;

    .line 158
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->h(Lcmh;)V

    .line 159
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->i(Lcmh;)Lcpq;

    move-result-object v0

    invoke-virtual {v0}, Lcpq;->PE()Leeb;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v1}, Lcmh;->j(Lcmh;)Leeb;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v1}, Lcmh;->j(Lcmh;)Leeb;

    move-result-object v1

    invoke-virtual {v1, v0}, Leeb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 162
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->i(Lcmh;)Lcpq;

    move-result-object v0

    iget-object v1, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v1}, Lcmh;->j(Lcmh;)Leeb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpq;->a(Leeb;)V

    .line 163
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-static {v0}, Lcmh;->k(Lcmh;)Z

    .line 167
    :cond_9
    if-eqz v5, :cond_1

    .line 168
    iget-object v0, p0, Lcmi;->bbQ:Lcmh;

    invoke-virtual {v0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0}, Lcmz;->Qx()V

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    move-object v3, v1

    goto/16 :goto_3
.end method
