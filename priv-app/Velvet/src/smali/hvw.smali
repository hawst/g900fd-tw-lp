.class public final Lhvw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dss:Lhvx;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsu:[Ljzh;

    array-length v0, v0

    if-ge p2, v0, :cond_1

    .line 143
    iget-object v0, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dss:Lhvx;

    iget-object v1, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v1, v1, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsu:[Ljzh;

    aget-object v1, v1, p2

    invoke-interface {v0, v1}, Lhvx;->a(Ljzh;)V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-static {v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->a(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 146
    iget-object v0, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhuz;->bO(Landroid/content/Context;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lhvw;->dsx:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "input_method_id"

    invoke-static {v0}, Lhuz;->bN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x14200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
