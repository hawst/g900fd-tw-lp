.class public final Lilf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lilv;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private transient cnE:Ljava/util/List;

.field private transient dCd:Ljava/util/Set;

.field private transient dDL:Lils;

.field private transient dDM:Lils;

.field private transient dDN:Lipn;

.field private transient dDO:Ljava/util/Map;

.field private transient dDP:Ljava/util/Map;

.field private transient dDQ:Ljava/util/List;

.field private transient map:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    invoke-static {}, Lile;->aXj()Lile;

    move-result-object v0

    iput-object v0, p0, Lilf;->dDN:Lipn;

    .line 168
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    .line 169
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    .line 170
    return-void
.end method

.method static synthetic a(Lilf;)Lils;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lilf;->dDL:Lils;

    return-object v0
.end method

.method static synthetic a(Lilf;Ljava/lang/Object;Ljava/lang/Object;Lils;)Lils;
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Lilf;->a(Ljava/lang/Object;Ljava/lang/Object;Lils;)Lils;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lils;)Lils;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lils;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 191
    new-instance v1, Lils;

    invoke-direct {v1, p1, p2}, Lils;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lilf;->dDL:Lils;

    if-nez v0, :cond_0

    .line 193
    iput-object v1, p0, Lilf;->dDM:Lils;

    iput-object v1, p0, Lilf;->dDL:Lils;

    .line 194
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :goto_0
    iget-object v0, p0, Lilf;->dDN:Lipn;

    invoke-interface {v0, p1}, Lipn;->add(Ljava/lang/Object;)Z

    .line 227
    return-object v1

    .line 196
    :cond_0
    if-nez p3, :cond_2

    .line 197
    iget-object v0, p0, Lilf;->dDM:Lils;

    iput-object v1, v0, Lils;->dEa:Lils;

    .line 198
    iget-object v0, p0, Lilf;->dDM:Lils;

    iput-object v0, v1, Lils;->dEc:Lils;

    .line 199
    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lils;

    .line 200
    if-nez v0, :cond_1

    .line 201
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    :goto_1
    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iput-object v1, p0, Lilf;->dDM:Lils;

    goto :goto_0

    .line 203
    :cond_1
    iput-object v1, v0, Lils;->dEd:Lils;

    .line 204
    iput-object v0, v1, Lils;->dEe:Lils;

    goto :goto_1

    .line 209
    :cond_2
    iget-object v0, p3, Lils;->dEc:Lils;

    iput-object v0, v1, Lils;->dEc:Lils;

    .line 210
    iget-object v0, p3, Lils;->dEe:Lils;

    iput-object v0, v1, Lils;->dEe:Lils;

    .line 211
    iput-object p3, v1, Lils;->dEa:Lils;

    .line 212
    iput-object p3, v1, Lils;->dEd:Lils;

    .line 213
    iget-object v0, p3, Lils;->dEe:Lils;

    if-nez v0, :cond_3

    .line 214
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :goto_2
    iget-object v0, p3, Lils;->dEc:Lils;

    if-nez v0, :cond_4

    .line 219
    iput-object v1, p0, Lilf;->dDL:Lils;

    .line 223
    :goto_3
    iput-object v1, p3, Lils;->dEc:Lils;

    .line 224
    iput-object v1, p3, Lils;->dEe:Lils;

    goto :goto_0

    .line 216
    :cond_3
    iget-object v0, p3, Lils;->dEe:Lils;

    iput-object v1, v0, Lils;->dEd:Lils;

    goto :goto_2

    .line 221
    :cond_4
    iget-object v0, p3, Lils;->dEc:Lils;

    iput-object v1, v0, Lils;->dEa:Lils;

    goto :goto_3
.end method

.method static synthetic a(Lils;)Ljava/util/Map$Entry;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lilk;

    invoke-direct {v0, p0}, Lilk;-><init>(Lils;)V

    return-object v0
.end method

.method static synthetic a(Lilf;Lils;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p1, Lils;->dEc:Lils;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lils;->dEc:Lils;

    iget-object v1, p1, Lils;->dEa:Lils;

    iput-object v1, v0, Lils;->dEa:Lils;

    :goto_0
    iget-object v0, p1, Lils;->dEa:Lils;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lils;->dEa:Lils;

    iget-object v1, p1, Lils;->dEc:Lils;

    iput-object v1, v0, Lils;->dEc:Lils;

    :goto_1
    iget-object v0, p1, Lils;->dEe:Lils;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lils;->dEe:Lils;

    iget-object v1, p1, Lils;->dEd:Lils;

    iput-object v1, v0, Lils;->dEd:Lils;

    :goto_2
    iget-object v0, p1, Lils;->dEd:Lils;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lils;->dEd:Lils;

    iget-object v1, p1, Lils;->dEe:Lils;

    iput-object v1, v0, Lils;->dEe:Lils;

    :goto_3
    iget-object v0, p0, Lilf;->dDN:Lipn;

    iget-object v1, p1, Lils;->dCo:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lipn;->remove(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v0, p1, Lils;->dEa:Lils;

    iput-object v0, p0, Lilf;->dDL:Lils;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lils;->dEc:Lils;

    iput-object v0, p0, Lilf;->dDM:Lils;

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lils;->dEd:Lils;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    iget-object v1, p1, Lils;->dCo:Ljava/lang/Object;

    iget-object v2, p1, Lils;->dEd:Lils;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    iget-object v1, p1, Lils;->dCo:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    iget-object v0, p1, Lils;->dEe:Lils;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    iget-object v1, p1, Lils;->dCo:Ljava/lang/Object;

    iget-object v2, p1, Lils;->dEe:Lils;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    iget-object v1, p1, Lils;->dCo:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3
.end method

.method static synthetic a(Lilf;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lilf;->by(Ljava/lang/Object;)V

    return-void
.end method

.method public static aXk()Lilf;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lilf;

    invoke-direct {v0}, Lilf;-><init>()V

    return-object v0
.end method

.method private aXl()Ljava/util/List;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lilf;->cnE:Ljava/util/List;

    .line 936
    if-nez v0, :cond_0

    .line 937
    new-instance v0, Lill;

    invoke-direct {v0, p0}, Lill;-><init>(Lilf;)V

    iput-object v0, p0, Lilf;->cnE:Ljava/util/List;

    .line 993
    :cond_0
    return-object v0
.end method

.method static synthetic b(Lilf;)Lils;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lilf;->dDM:Lils;

    return-object v0
.end method

.method static synthetic bA(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 102
    if-nez p0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method private by(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 265
    new-instance v0, Lilu;

    invoke-direct {v0, p0, p1}, Lilu;-><init>(Lilf;Ljava/lang/Object;)V

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 267
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 269
    :cond_0
    return-void
.end method

.method static synthetic c(Lilf;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lilf;)Lipn;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lilf;->dDN:Lipn;

    return-object v0
.end method

.method static synthetic e(Lilf;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 4

    .prologue
    .line 1130
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1131
    invoke-static {}, Lile;->aXj()Lile;

    move-result-object v0

    iput-object v0, p0, Lilf;->dDN:Lipn;

    .line 1132
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    .line 1133
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    .line 1134
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 1135
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1137
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    .line 1139
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .line 1140
    invoke-virtual {p0, v2, v3}, Lilf;->q(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1142
    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3

    .prologue
    .line 1119
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 1120
    invoke-virtual {p0}, Lilf;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 1121
    invoke-direct {p0}, Lilf;->aXl()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1122
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1123
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    .line 1125
    :cond_0
    return-void
.end method


# virtual methods
.method public final aF(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 658
    new-instance v0, Lilg;

    invoke-direct {v0, p0, p1}, Lilg;-><init>(Lilf;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final synthetic aWn()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Lilf;->aXl()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final aWp()Ljava/util/Map;
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lilf;->map:Ljava/util/Map;

    .line 1038
    if-nez v0, :cond_0

    .line 1039
    new-instance v0, Liln;

    invoke-direct {v0, p0}, Liln;-><init>(Lilf;)V

    iput-object v0, p0, Lilf;->map:Ljava/util/Map;

    .line 1069
    :cond_0
    return-object v0
.end method

.method public final synthetic bk(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lilf;->bz(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bl(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lilf;->aF(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bz(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 631
    new-instance v0, Lilu;

    invoke-direct {v0, p0, p1}, Lilu;-><init>(Lilf;Ljava/lang/Object;)V

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 632
    invoke-direct {p0, p1}, Lilf;->by(Ljava/lang/Object;)V

    .line 633
    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 638
    iput-object v0, p0, Lilf;->dDL:Lils;

    .line 639
    iput-object v0, p0, Lilf;->dDM:Lils;

    .line 640
    iget-object v0, p0, Lilf;->dDN:Lipn;

    invoke-interface {v0}, Lipn;->clear()V

    .line 641
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 642
    iget-object v0, p0, Lilf;->dDP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 643
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 513
    iget-object v0, p0, Lilf;->dDO:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 518
    new-instance v1, Lilt;

    invoke-direct {v1, p0}, Lilt;-><init>(Lilf;)V

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lils;

    iget-object v0, v0, Lils;->ciy:Ljava/lang/Object;

    invoke-static {v0, p1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x1

    .line 523
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1082
    if-ne p1, p0, :cond_0

    .line 1083
    const/4 v0, 0x1

    .line 1089
    :goto_0
    return v0

    .line 1085
    :cond_0
    instance-of v0, p1, Liph;

    if-eqz v0, :cond_1

    .line 1086
    check-cast p1, Liph;

    .line 1087
    invoke-virtual {p0}, Lilf;->aWp()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Liph;->aWp()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1089
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1099
    invoke-virtual {p0}, Lilf;->aWp()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lilf;->dCd:Ljava/util/Set;

    .line 679
    if-nez v0, :cond_0

    .line 680
    new-instance v0, Lilh;

    invoke-direct {v0, p0}, Lilh;-><init>(Lilf;)V

    iput-object v0, p0, Lilf;->dCd:Ljava/util/Set;

    .line 696
    :cond_0
    return-object v0
.end method

.method public final p(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 528
    new-instance v0, Lilu;

    invoke-direct {v0, p0, p1}, Lilu;-><init>(Lilf;Ljava/lang/Object;)V

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 529
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530
    const/4 v0, 0x1

    .line 533
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 547
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lilf;->a(Ljava/lang/Object;Ljava/lang/Object;Lils;)Lils;

    .line 548
    const/4 v0, 0x1

    return v0
.end method

.method public final remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 553
    new-instance v0, Lilu;

    invoke-direct {v0, p0, p1}, Lilu;-><init>(Lilf;Ljava/lang/Object;)V

    .line 554
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 555
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 557
    const/4 v0, 0x1

    .line 560
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lilf;->dDN:Lipn;

    invoke-interface {v0}, Lipn;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109
    invoke-virtual {p0}, Lilf;->aWp()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lilf;->dDQ:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Lili;

    invoke-direct {v0, p0}, Lili;-><init>(Lilf;)V

    iput-object v0, p0, Lilf;->dDQ:Ljava/util/List;

    :cond_0
    return-object v0
.end method
