.class public final Lhsz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public dqb:Lhtj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final dqm:Lhsm;

.field final dqn:Leqo;

.field dqo:Lhtf;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field dqp:Z

.field dqq:Leqx;

.field final mGreco3DataManager:Lgix;

.field final mLocalTtsManager:Lice;

.field final mOfflineActionsManager:Lgkg;

.field final mSettings:Lhym;

.field final mSoundManager:Lhik;

.field private final mViewDisplayer:Lhul;


# direct methods
.method public constructor <init>(Lhsm;Lice;Lhul;Lgix;Lgkg;Leqo;Lhik;Lhym;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-string v0, "InitializeController"

    sget-object v1, Lhte;->dqs:Lhte;

    invoke-static {v0, v1}, Leqx;->a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lhte;->dqs:Lhte;

    new-array v2, v5, [Lhte;

    sget-object v3, Lhte;->dqt:Lhte;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lhte;->dqs:Lhte;

    new-array v2, v5, [Lhte;

    sget-object v3, Lhte;->dqw:Lhte;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lhte;->dqt:Lhte;

    new-array v2, v5, [Lhte;

    sget-object v3, Lhte;->dqu:Lhte;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lhte;->dqt:Lhte;

    new-array v2, v5, [Lhte;

    sget-object v3, Lhte;->dqw:Lhte;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lhte;->dqu:Lhte;

    new-array v2, v5, [Lhte;

    sget-object v3, Lhte;->dqv:Lhte;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lhte;->dqu:Lhte;

    new-array v2, v5, [Lhte;

    sget-object v3, Lhte;->dqw:Lhte;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    iput-boolean v4, v0, Leqy;->chK:Z

    iput-boolean v5, v0, Leqy;->chN:Z

    iput-boolean v4, v0, Leqy;->chJ:Z

    invoke-virtual {v0}, Leqy;->avx()Leqx;

    move-result-object v0

    iput-object v0, p0, Lhsz;->dqq:Leqx;

    .line 88
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lice;

    iput-object v0, p0, Lhsz;->mLocalTtsManager:Lice;

    .line 89
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhul;

    iput-object v0, p0, Lhsz;->mViewDisplayer:Lhul;

    .line 90
    iput-object p1, p0, Lhsz;->dqm:Lhsm;

    .line 91
    iput-object p6, p0, Lhsz;->dqn:Leqo;

    .line 92
    iput-object p4, p0, Lhsz;->mGreco3DataManager:Lgix;

    .line 93
    iput-object p5, p0, Lhsz;->mOfflineActionsManager:Lgkg;

    .line 94
    iput-object p8, p0, Lhsz;->mSettings:Lhym;

    .line 95
    iput-object p7, p0, Lhsz;->mSoundManager:Lhik;

    .line 96
    return-void
.end method


# virtual methods
.method public final start()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lhsz;->dqq:Leqx;

    sget-object v1, Lhte;->dqs:Lhte;

    invoke-virtual {v0, v1}, Leqx;->d(Ljava/lang/Enum;)V

    .line 104
    iget-object v0, p0, Lhsz;->mViewDisplayer:Lhul;

    invoke-virtual {v0}, Lhul;->aSh()Lhtf;

    move-result-object v0

    iput-object v0, p0, Lhsz;->dqo:Lhtf;

    .line 105
    iget-object v0, p0, Lhsz;->dqm:Lhsm;

    new-instance v1, Lhtc;

    invoke-direct {v1, p0}, Lhtc;-><init>(Lhsz;)V

    invoke-virtual {v0, v1}, Lhsm;->a(Lhsp;)V

    .line 106
    return-void
.end method
