.class public final Ldmt;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/shared/search/Query;Ljtg;)Landroid/text/Spanned;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 126
    :cond_0
    return-object v0

    .line 82
    :cond_1
    if-eqz p1, :cond_0

    iget-object v2, p1, Ljtg;->eDo:[Ljava/lang/String;

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 86
    iget-object v2, p1, Ljtg;->eDp:[Ljti;

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 91
    iget-object v2, p1, Ljtg;->eDo:[Ljava/lang/String;

    .line 97
    const-string v3, "\\s+"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 99
    invoke-static {v2, v3}, Ldmt;->b([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 108
    invoke-static {v1, v3}, Ldmt;->c(Ljava/lang/String;[Ljava/lang/String;)[I

    move-result-object v2

    .line 110
    iget-object v1, p1, Ljtg;->eDp:[Ljti;

    array-length v4, v1

    .line 111
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 112
    iget-object v5, p1, Ljtg;->eDp:[Ljti;

    aget-object v5, v5, v1

    .line 114
    iget-object v6, v5, Ljti;->eDt:Ljth;

    .line 115
    iget-object v5, v5, Ljti;->eDu:[Ljava/lang/String;

    .line 117
    invoke-virtual {v6}, Ljth;->bur()I

    move-result v7

    .line 118
    invoke-virtual {v6}, Ljth;->bus()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    .line 119
    aget v7, v2, v7

    .line 120
    aget v8, v2, v6

    aget-object v6, v3, v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v8

    .line 122
    new-instance v8, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-direct {v8, v5}, Lcom/google/android/shared/util/VoiceCorrectionSpan;-><init>([Ljava/lang/String;)V

    const/16 v5, 0x11

    invoke-virtual {v0, v8, v7, v6, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static b([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    array-length v0, p0

    array-length v2, p1

    if-eq v0, v2, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 35
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 36
    aget-object v2, p0, v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    aget-object v3, p1, v0

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;[Ljava/lang/String;)[I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 52
    array-length v0, p1

    new-array v1, v0, [I

    .line 53
    aget-object v0, p1, v2

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    aput v0, v1, v2

    .line 54
    const/4 v0, 0x1

    .line 55
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 56
    aget-object v2, p1, v0

    add-int/lit8 v3, v0, -0x1

    aget v3, v1, v3

    add-int/lit8 v4, v0, -0x1

    aget-object v4, p1, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v0

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    return-object v1
.end method
