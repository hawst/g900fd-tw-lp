.class public final Lfxy;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 7

    .prologue
    const v6, 0x7f11033b

    const v5, 0x7f11033a

    .line 33
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 35
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agE:Lant;

    .line 36
    iget-object v2, v1, Lant;->aiz:Laos;

    .line 38
    invoke-virtual {v2}, Laos;->rn()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 39
    const v3, 0x7f110082

    invoke-virtual {v2}, Laos;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 42
    :cond_0
    invoke-virtual {v2}, Laos;->rp()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 43
    const v3, 0x7f110339

    invoke-virtual {v2}, Laos;->ro()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 46
    :cond_1
    invoke-virtual {v1}, Lant;->pP()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 47
    const v3, 0x7f110337

    invoke-virtual {v1}, Lant;->pO()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 50
    :cond_2
    invoke-virtual {v2}, Laos;->rr()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 51
    invoke-virtual {v2}, Laos;->rq()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v5, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 54
    :cond_3
    invoke-virtual {v2}, Laos;->rt()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 55
    invoke-virtual {v2}, Laos;->rs()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v6, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 58
    :cond_4
    invoke-virtual {v2}, Laos;->rv()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 59
    invoke-virtual {v2}, Laos;->ru()I

    move-result v3

    invoke-static {v0, v5, v3}, Lgab;->i(Landroid/view/View;II)Landroid/widget/TextView;

    .line 60
    invoke-virtual {v2}, Laos;->ru()I

    move-result v2

    invoke-static {v0, v6, v2}, Lgab;->i(Landroid/view/View;II)Landroid/widget/TextView;

    .line 63
    :cond_5
    invoke-virtual {v1}, Lant;->pR()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 64
    const v2, 0x7f11033c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 65
    invoke-virtual {v1}, Lant;->pQ()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfxy;->mCardContainer:Lfmt;

    invoke-interface {v2}, Lfmt;->aAD()Lfml;

    move-result-object v2

    iget-object v2, v2, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 67
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 69
    :cond_6
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f040129

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
