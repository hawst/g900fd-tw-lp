.class final Link;
.super Ljava/util/AbstractQueue;
.source "PG"


# instance fields
.field final dFe:Linr;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3284
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3285
    new-instance v0, Linl;

    invoke-direct {v0, p0}, Linl;-><init>(Link;)V

    iput-object v0, p0, Link;->dFe:Linr;

    return-void
.end method

.method private aXM()Linr;
    .locals 2

    .prologue
    .line 3336
    iget-object v0, p0, Link;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v0

    .line 3337
    iget-object v1, p0, Link;->dFe:Linr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 3386
    iget-object v0, p0, Link;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v0

    .line 3387
    :goto_0
    iget-object v1, p0, Link;->dFe:Linr;

    if-eq v0, v1, :cond_0

    .line 3388
    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v1

    .line 3389
    invoke-static {v0}, Limn;->d(Linr;)V

    move-object v0, v1

    .line 3391
    goto :goto_0

    .line 3393
    :cond_0
    iget-object v0, p0, Link;->dFe:Linr;

    iget-object v1, p0, Link;->dFe:Linr;

    invoke-interface {v0, v1}, Linr;->f(Linr;)V

    .line 3394
    iget-object v0, p0, Link;->dFe:Linr;

    iget-object v1, p0, Link;->dFe:Linr;

    invoke-interface {v0, v1}, Linr;->g(Linr;)V

    .line 3395
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3366
    check-cast p1, Linr;

    .line 3367
    invoke-interface {p1}, Linr;->aXI()Linr;

    move-result-object v0

    sget-object v1, Linq;->dFs:Linq;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 3372
    iget-object v0, p0, Link;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v0

    iget-object v1, p0, Link;->dFe:Linr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 3399
    new-instance v0, Linm;

    invoke-direct {p0}, Link;->aXM()Linr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Linm;-><init>(Link;Linr;)V

    return-object v0
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3284
    check-cast p1, Linr;

    invoke-interface {p1}, Linr;->aXJ()Linr;

    move-result-object v0

    invoke-interface {p1}, Linr;->aXI()Linr;

    move-result-object v1

    invoke-static {v0, v1}, Limn;->a(Linr;Linr;)V

    iget-object v0, p0, Link;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXJ()Linr;

    move-result-object v0

    invoke-static {v0, p1}, Limn;->a(Linr;Linr;)V

    iget-object v0, p0, Link;->dFe:Linr;

    invoke-static {p1, v0}, Limn;->a(Linr;Linr;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3284
    invoke-direct {p0}, Link;->aXM()Linr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3284
    iget-object v0, p0, Link;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v0

    iget-object v1, p0, Link;->dFe:Linr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Link;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3354
    check-cast p1, Linr;

    .line 3355
    invoke-interface {p1}, Linr;->aXJ()Linr;

    move-result-object v0

    .line 3356
    invoke-interface {p1}, Linr;->aXI()Linr;

    move-result-object v1

    .line 3357
    invoke-static {v0, v1}, Limn;->a(Linr;Linr;)V

    .line 3358
    invoke-static {p1}, Limn;->d(Linr;)V

    .line 3360
    sget-object v0, Linq;->dFs:Linq;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 3377
    const/4 v1, 0x0

    .line 3378
    iget-object v0, p0, Link;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Link;->dFe:Linr;

    if-eq v0, v2, :cond_0

    .line 3379
    add-int/lit8 v1, v1, 0x1

    .line 3378
    invoke-interface {v0}, Linr;->aXI()Linr;

    move-result-object v0

    goto :goto_0

    .line 3381
    :cond_0
    return v1
.end method
