.class public final Liau;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# instance fields
.field private synthetic dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

.field private final dwS:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    iput-object p2, p0, Liau;->dwS:Ljava/util/List;

    .line 369
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;IZ)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 373
    iget-object v0, p0, Liau;->dwS:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 374
    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3, v0}, Lgnq;->a(Ljze;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 412
    :goto_0
    return-void

    .line 380
    :cond_0
    if-eqz p3, :cond_1

    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget v4, v4, Lcom/google/android/voicesearch/ui/LanguagePreference;->drQ:I

    if-lt v3, v4, :cond_1

    .line 382
    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p2, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 383
    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a086f

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 388
    :cond_1
    if-eqz p3, :cond_4

    .line 389
    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-static {v0}, Lgnq;->an(Ljava/util/List;)V

    .line 397
    :goto_1
    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_3

    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 400
    :cond_2
    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    .line 404
    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-static {v0, v3}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406
    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v4, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v4, v4, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-static {v3, v0, v4}, Lcom/google/android/voicesearch/ui/LanguagePreference;->a(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_3
    check-cast p1, Landroid/app/AlertDialog;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    iget-object v0, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 392
    :cond_4
    iget-object v3, p0, Liau;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move v0, v2

    .line 410
    goto :goto_2
.end method
