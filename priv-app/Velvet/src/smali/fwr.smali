.class public final Lfwr;
.super Lfus;
.source "PG"


# instance fields
.field private final mViewUtils:Lfks;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    .line 56
    new-instance v0, Lfks;

    iget-object v1, p1, Lizj;->dSw:Ljgz;

    iget-object v1, v1, Ljgz;->elr:[Ljgx;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lfks;-><init>(Lemp;Ljava/util/List;)V

    iput-object v0, p0, Lfwr;->mViewUtils:Lfks;

    .line 58
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 6

    .prologue
    .line 43
    const/4 v5, 0x5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 44
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 45
    iget-object v2, p1, Lizq;->dUX:[Lizj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 46
    iget-object v4, v4, Lizj;->dSx:Ljgy;

    iget-object v4, v4, Ljgy;->elq:Ljgx;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    new-instance v0, Lfks;

    invoke-direct {v0, p3, v1}, Lfks;-><init>(Lemp;Ljava/util/List;)V

    iput-object v0, p0, Lfwr;->mViewUtils:Lfks;

    .line 49
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;Lfks;)V
    .locals 6

    .prologue
    .line 68
    const/4 v5, 0x5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 69
    iput-object p5, p0, Lfwr;->mViewUtils:Lfks;

    .line 70
    return-void
.end method

.method private static a(Landroid/content/Context;Lfks;Ljgx;)Laos;
    .locals 4

    .prologue
    .line 88
    new-instance v0, Laos;

    invoke-direct {v0}, Laos;-><init>()V

    .line 89
    invoke-static {p2}, Lfks;->e(Ljgx;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laos;->cn(Ljava/lang/String;)Laos;

    .line 91
    invoke-virtual {p1, p2}, Lfks;->a(Ljgx;)Ljava/lang/String;

    move-result-object v1

    .line 92
    if-eqz v1, :cond_0

    .line 93
    invoke-virtual {v0, v1}, Laos;->co(Ljava/lang/String;)Laos;

    .line 96
    :cond_0
    invoke-virtual {p1, p2}, Lfks;->b(Ljgx;)Ljava/lang/String;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_1

    .line 98
    invoke-virtual {v0, v1}, Laos;->cp(Ljava/lang/String;)Laos;

    .line 101
    :cond_1
    invoke-static {p2}, Lfks;->c(Ljgx;)Ljava/lang/String;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_2

    .line 103
    invoke-virtual {v0, v1}, Laos;->cq(Ljava/lang/String;)Laos;

    .line 106
    :cond_2
    invoke-virtual {p2}, Ljgx;->blp()F

    move-result v1

    float-to-double v2, v1

    invoke-static {p0, v2, v3}, Lfks;->a(Landroid/content/Context;D)I

    move-result v1

    invoke-virtual {v0, v1}, Laos;->cJ(I)Laos;

    .line 109
    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Laoj;)V
    .locals 2

    .prologue
    .line 183
    const v0, 0x7f0a0327

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Laoj;->bT(Ljava/lang/String;)Laoj;

    .line 184
    new-instance v0, Lfsa;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, p2, Laoj;->ajx:Lani;

    .line 187
    return-void
.end method

.method private aDL()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 173
    const/4 v0, 0x0

    .line 174
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    .line 175
    iget-object v1, v1, Lizq;->dUZ:Lizj;

    iget-object v1, v1, Lizj;->dSw:Ljgz;

    .line 176
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljgz;->blv()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    invoke-virtual {v1}, Ljgz;->aDL()Ljava/lang/String;

    move-result-object v0

    .line 179
    :cond_0
    return-object v0
.end method

.method private static e(Landroid/content/Context;Lizj;)Lani;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 113
    iget-object v0, p1, Lizj;->dSx:Ljgy;

    iget-object v0, v0, Ljgy;->elq:Ljgx;

    .line 114
    iget-object v1, v0, Ljgx;->ahD:Lixx;

    if-eqz v1, :cond_0

    .line 115
    new-instance v1, Lfsa;

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    iget-object v0, v0, Ljgx;->ahD:Lixx;

    invoke-virtual {v1, v0}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    .line 117
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljgx;->getSymbol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0a0368

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    new-instance v1, Lfsa;

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    invoke-virtual {v1, v0, v3, v3}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lfus;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 77
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 78
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lfwr;->cCl:Lftx;

    invoke-virtual {v0, p1, p3}, Lftx;->j(Landroid/content/Context;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 125
    iget-object v0, p2, Lizj;->dSx:Ljgy;

    .line 126
    iget-object v0, v0, Ljgy;->elq:Ljgx;

    .line 127
    iget-object v1, p0, Lfwr;->mViewUtils:Lfks;

    invoke-static {p1, v1, v0}, Lfwr;->a(Landroid/content/Context;Lfks;Ljgx;)Laos;

    move-result-object v1

    .line 129
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 130
    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 131
    iput-object v1, v2, Lanh;->agD:Laos;

    .line 132
    invoke-static {p1, p2}, Lfwr;->e(Landroid/content/Context;Lizj;)Lani;

    move-result-object v1

    iput-object v1, v2, Lanh;->ahs:Lani;

    .line 133
    iget-object v1, v2, Lanh;->ahs:Lani;

    new-instance v3, Lanb;

    invoke-direct {v3}, Lanb;-><init>()V

    iput-object v3, v1, Lani;->ahC:Lanb;

    .line 136
    invoke-virtual {v0}, Ljgx;->blp()F

    move-result v0

    .line 137
    iget-object v1, v2, Lanh;->ahs:Lani;

    iget-object v1, v1, Lani;->ahC:Lanb;

    cmpl-float v3, v0, v4

    if-lez v3, :cond_0

    const v0, 0x7f020226

    :goto_0
    invoke-virtual {v1, v0}, Lanb;->ch(I)Lanb;

    .line 140
    iput-object p2, v2, Lanh;->ahu:Lizj;

    .line 142
    return-object v2

    .line 137
    :cond_0
    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    const v0, 0x7f020225

    goto :goto_0

    :cond_1
    const v0, 0x7f020113

    goto :goto_0
.end method

.method public final aT(Landroid/content/Context;)Lanh;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 147
    iget-object v0, p0, Lfwr;->mViewUtils:Lfks;

    iget-object v2, v0, Lfks;->cuh:Ljava/util/List;

    const-wide/16 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgx;

    invoke-virtual {v0}, Ljgx;->blm()J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-lez v1, :cond_2

    invoke-virtual {v0}, Ljgx;->blm()J

    move-result-wide v0

    :goto_1
    move-wide v2, v0

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 148
    iget-object v0, p0, Lfwr;->cCl:Lftx;

    iget-object v0, p0, Lfwr;->mViewUtils:Lfks;

    iget-object v0, v0, Lfks;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lesi;->i(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0364

    :goto_2
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfwr;->mViewUtils:Lfks;

    const v4, 0x7f0a0366

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v1, v1, Lfks;->mClock:Lemp;

    invoke-static {p1, v1, v2, v3}, Lfks;->a(Landroid/content/Context;Lemp;J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lftx;->aN(Ljava/lang/String;Ljava/lang/String;)Lanh;

    move-result-object v0

    .line 153
    invoke-virtual {v0, v8}, Lanh;->aV(Z)Lanh;

    .line 155
    return-object v0

    .line 148
    :cond_1
    const v0, 0x7f0a0369

    goto :goto_2

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 3

    .prologue
    .line 232
    invoke-direct {p0}, Lfwr;->aDL()Ljava/lang/String;

    move-result-object v1

    .line 233
    if-eqz v1, :cond_0

    .line 234
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 235
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 236
    iget-object v2, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v2, v2, Lizq;->dUZ:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 237
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 238
    iget-object v2, v0, Lanh;->agz:Laoj;

    invoke-static {p1, v1, v2}, Lfwr;->a(Landroid/content/Context;Ljava/lang/String;Laoj;)V

    .line 241
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/util/List;)Lanh;
    .locals 3

    .prologue
    .line 160
    invoke-super {p0, p1, p2}, Lfus;->b(Landroid/content/Context;Ljava/util/List;)Lanh;

    move-result-object v0

    .line 163
    invoke-direct {p0}, Lfwr;->aDL()Ljava/lang/String;

    move-result-object v1

    .line 164
    if-eqz v1, :cond_0

    .line 165
    iget-object v2, v0, Lanh;->agz:Laoj;

    invoke-static {p1, v1, v2}, Lfwr;->a(Landroid/content/Context;Ljava/lang/String;Laoj;)V

    .line 168
    :cond_0
    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 193
    iget-object v0, p2, Lizj;->dSx:Ljgy;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lizj;->dSx:Ljgy;

    iget-object v0, v0, Ljgy;->elq:Ljgx;

    if-nez v0, :cond_1

    .line 194
    :cond_0
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    .line 197
    :cond_1
    iget-object v0, p2, Lizj;->dSx:Ljgy;

    iget-object v5, v0, Ljgy;->elq:Ljgx;

    .line 200
    new-instance v6, Lant;

    invoke-direct {v6}, Lant;-><init>()V

    .line 201
    iget-object v0, p0, Lfwr;->mViewUtils:Lfks;

    invoke-static {p1, v0, v5}, Lfwr;->a(Landroid/content/Context;Lfks;Ljgx;)Laos;

    move-result-object v0

    iput-object v0, v6, Lant;->aiz:Laos;

    .line 204
    iget-object v0, v6, Lant;->aiz:Laos;

    iget-object v3, p0, Lfwr;->mViewUtils:Lfks;

    invoke-static {v5}, Lfks;->d(Ljgx;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Laos;->cn(Ljava/lang/String;)Laos;

    .line 205
    iget-object v0, p0, Lfwr;->mViewUtils:Lfks;

    invoke-virtual {v0, p1, v5}, Lfks;->a(Landroid/content/Context;Ljgx;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lant;->aV(Ljava/lang/String;)Lant;

    .line 206
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d01d1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v3, v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d01d0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v7, 0xf0

    if-eq v4, v7, :cond_2

    const/16 v7, 0x140

    if-eq v4, v7, :cond_2

    const/16 v7, 0x1e0

    if-ne v4, v7, :cond_4

    :cond_2
    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v0, v0, 0x2

    move v4, v3

    move v3, v0

    move v0, v2

    :goto_1
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5}, Ljgx;->bls()Ljava/lang/String;

    move-result-object v5

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-static {v7, v5, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lant;->aW(Ljava/lang/String;)Lant;

    .line 208
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    .line 209
    const/4 v0, 0x7

    invoke-virtual {v3, v0}, Lanh;->cm(I)Lanh;

    .line 210
    iput-object v6, v3, Lanh;->agE:Lant;

    .line 211
    invoke-static {p1, p2}, Lfwr;->e(Landroid/content/Context;Lizj;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    .line 212
    iput-object p2, v3, Lanh;->ahu:Lizj;

    .line 214
    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    .line 215
    invoke-virtual {v4, v10}, Lanh;->cm(I)Lanh;

    .line 216
    iput-object p2, v4, Lanh;->ahu:Lizj;

    .line 217
    new-instance v0, Laoj;

    invoke-direct {v0}, Laoj;-><init>()V

    iput-object v0, v4, Lanh;->agz:Laoj;

    .line 218
    iget-object v0, v4, Lanh;->agz:Laoj;

    const v5, 0x7f0a0506

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 219
    invoke-static {p1, p2}, Lfwr;->e(Landroid/content/Context;Lizj;)Lani;

    move-result-object v0

    iput-object v0, v4, Lanh;->ahs:Lani;

    .line 220
    iget-object v0, v4, Lanh;->ahs:Lani;

    new-instance v5, Lanb;

    invoke-direct {v5}, Lanb;-><init>()V

    iput-object v5, v0, Lani;->ahC:Lanb;

    .line 221
    iget-object v0, v4, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahC:Lanb;

    const v5, 0x7f0200ed

    invoke-virtual {v0, v5}, Lanb;->ch(I)Lanb;

    .line 222
    invoke-direct {p0}, Lfwr;->aDL()Ljava/lang/String;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_3

    .line 224
    iget-object v5, v4, Lanh;->agz:Laoj;

    invoke-static {p1, v0, v5}, Lfwr;->a(Landroid/content/Context;Ljava/lang/String;Laoj;)V

    .line 227
    :cond_3
    new-array v0, v2, [Lanh;

    aput-object v3, v0, v9

    aput-object v4, v0, v1

    goto/16 :goto_0

    :cond_4
    move v4, v3

    move v3, v0

    move v0, v1

    goto :goto_1
.end method
