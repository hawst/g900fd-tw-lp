.class public final Ljjk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eoV:[Ljjk;


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private aiC:Ljava/lang/String;

.field private eoW:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Ljjk;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljjk;->aeE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljjk;->aiC:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljjk;->eoW:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjk;->eCz:I

    .line 106
    return-void
.end method

.method public static bnS()[Ljjk;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Ljjk;->eoV:[Ljjk;

    if-nez v0, :cond_1

    .line 26
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 28
    :try_start_0
    sget-object v0, Ljjk;->eoV:[Ljjk;

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Ljjk;

    sput-object v0, Ljjk;->eoV:[Ljjk;

    .line 31
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Ljjk;->eoV:[Ljjk;

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 19
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjk;->aeE:Ljava/lang/String;

    iget v0, p0, Ljjk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjk;->aiC:Ljava/lang/String;

    iget v0, p0, Ljjk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjk;->eoW:Ljava/lang/String;

    iget v0, p0, Ljjk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Ljjk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x1

    iget-object v1, p0, Ljjk;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 124
    :cond_0
    iget v0, p0, Ljjk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 125
    const/4 v0, 0x2

    iget-object v1, p0, Ljjk;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 127
    :cond_1
    iget v0, p0, Ljjk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 128
    const/4 v0, 0x3

    iget-object v1, p0, Ljjk;->eoW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 130
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 131
    return-void
.end method

.method public final bnT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Ljjk;->eoW:Ljava/lang/String;

    return-object v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ljjk;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 135
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 136
    iget v1, p0, Ljjk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 137
    const/4 v1, 0x1

    iget-object v2, p0, Ljjk;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    :cond_0
    iget v1, p0, Ljjk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 141
    const/4 v1, 0x2

    iget-object v2, p0, Ljjk;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_1
    iget v1, p0, Ljjk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 145
    const/4 v1, 0x3

    iget-object v2, p0, Ljjk;->eoW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_2
    return v0
.end method

.method public final wi(Ljava/lang/String;)Ljjk;
    .locals 1

    .prologue
    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47
    :cond_0
    iput-object p1, p0, Ljjk;->aeE:Ljava/lang/String;

    .line 48
    iget v0, p0, Ljjk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjk;->aez:I

    .line 49
    return-object p0
.end method

.method public final wj(Ljava/lang/String;)Ljjk;
    .locals 1

    .prologue
    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 69
    :cond_0
    iput-object p1, p0, Ljjk;->aiC:Ljava/lang/String;

    .line 70
    iget v0, p0, Ljjk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjk;->aez:I

    .line 71
    return-object p0
.end method
