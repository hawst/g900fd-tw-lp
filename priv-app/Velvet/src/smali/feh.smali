.class final enum Lfeh;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cpG:Lfeh;

.field public static final enum cpH:Lfeh;

.field public static final enum cpI:Lfeh;

.field private static final synthetic cpJ:[Lfeh;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 130
    new-instance v0, Lfeh;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, Lfeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfeh;->cpG:Lfeh;

    new-instance v0, Lfeh;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v3}, Lfeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfeh;->cpH:Lfeh;

    new-instance v0, Lfeh;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v4}, Lfeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfeh;->cpI:Lfeh;

    const/4 v0, 0x3

    new-array v0, v0, [Lfeh;

    sget-object v1, Lfeh;->cpG:Lfeh;

    aput-object v1, v0, v2

    sget-object v1, Lfeh;->cpH:Lfeh;

    aput-object v1, v0, v3

    sget-object v1, Lfeh;->cpI:Lfeh;

    aput-object v1, v0, v4

    sput-object v0, Lfeh;->cpJ:[Lfeh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfeh;
    .locals 1

    .prologue
    .line 130
    const-class v0, Lfeh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfeh;

    return-object v0
.end method

.method public static values()[Lfeh;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lfeh;->cpJ:[Lfeh;

    invoke-virtual {v0}, [Lfeh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfeh;

    return-object v0
.end method
