.class public Lent;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private amp:I

.field private bbz:Z

.field private final cgl:J

.field private final cgm:J

.field private cgn:J

.field private cgo:[I

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lemp;JJ)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lent;->mClock:Lemp;

    .line 50
    iput-wide p2, p0, Lent;->cgl:J

    .line 51
    iput-wide p4, p0, Lent;->cgm:J

    .line 52
    iget-wide v0, p0, Lent;->cgl:J

    iget-wide v2, p0, Lent;->cgm:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lent;->cgo:[I

    .line 53
    return-void
.end method

.method public constructor <init>(Lemp;JJLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct/range {p0 .. p5}, Lent;-><init>(Lemp;JJ)V

    .line 57
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p6}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lent;->cgn:J

    move v0, v1

    :goto_0
    iget-object v3, p0, Lent;->cgo:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lent;->cgo:[I

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optInt(I)I

    move-result v4

    aput v4, v3, v0

    iget v3, p0, Lent;->amp:I

    iget-object v4, p0, Lent;->cgo:[I

    aget v4, v4, v0

    add-int/2addr v3, v4

    iput v3, p0, Lent;->amp:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ExpiringSum"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error reading expiring sum from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v2, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0}, Lent;->auM()V

    .line 60
    :cond_1
    return-void
.end method

.method public constructor <init>(Lemp;JJLjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 64
    const-wide/32 v4, 0xa4cb800

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lent;-><init>(Lemp;JJLjava/lang/String;)V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lent;->bbz:Z

    .line 66
    return-void
.end method

.method private auM()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 108
    iget-object v0, p0, Lent;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lent;->cgm:J

    div-long v2, v0, v2

    .line 109
    iget-wide v0, p0, Lent;->cgn:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 110
    iput v10, p0, Lent;->amp:I

    .line 111
    iget-wide v0, p0, Lent;->cgn:J

    sub-long v4, v2, v0

    .line 112
    iget-object v0, p0, Lent;->cgo:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 113
    int-to-long v6, v0

    sub-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-gez v1, :cond_0

    .line 114
    iget-object v1, p0, Lent;->cgo:[I

    aput v10, v1, v0

    .line 112
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 116
    :cond_0
    iget-object v1, p0, Lent;->cgo:[I

    iget-object v6, p0, Lent;->cgo:[I

    int-to-long v8, v0

    sub-long/2addr v8, v4

    long-to-int v7, v8

    aget v6, v6, v7

    aput v6, v1, v0

    .line 117
    iget v1, p0, Lent;->amp:I

    iget-object v6, p0, Lent;->cgo:[I

    aget v6, v6, v0

    add-int/2addr v1, v6

    iput v1, p0, Lent;->amp:I

    goto :goto_1

    .line 120
    :cond_1
    iput-wide v2, p0, Lent;->cgn:J

    .line 122
    :cond_2
    return-void
.end method


# virtual methods
.method public final declared-synchronized aU(J)V
    .locals 5

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lent;->auM()V

    .line 99
    iget-wide v0, p0, Lent;->cgn:J

    iget-wide v2, p0, Lent;->cgm:J

    div-long v2, p1, v2

    sub-long/2addr v0, v2

    .line 100
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    iget-object v2, p0, Lent;->cgo:[I

    array-length v2, v2

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 101
    iget-object v2, p0, Lent;->cgo:[I

    long-to-int v0, v0

    aget v1, v2, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, v2, v0

    .line 102
    iget v0, p0, Lent;->amp:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lent;->amp:I

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lent;->bbz:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized auK()I
    .locals 1

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lent;->auM()V

    .line 90
    iget v0, p0, Lent;->amp:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized auL()V
    .locals 2

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lent;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lent;->aU(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized auN()[J
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 125
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lent;->auM()V

    .line 126
    iget v0, p0, Lent;->amp:I

    new-array v5, v0, [J

    .line 127
    iget-object v0, p0, Lent;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 129
    iget-object v8, p0, Lent;->cgo:[I

    array-length v9, v8

    move v4, v1

    move v0, v1

    move-wide v6, v2

    :goto_0
    if-ge v4, v9, :cond_1

    aget v10, v8, v4

    move v2, v0

    move v0, v1

    .line 130
    :goto_1
    if-ge v0, v10, :cond_0

    .line 131
    add-int/lit8 v3, v2, 0x1

    aput-wide v6, v5, v2

    .line 130
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    .line 133
    :cond_0
    iget-wide v10, p0, Lent;->cgm:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v6, v10

    .line 129
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v2

    goto :goto_0

    .line 135
    :cond_1
    monitor-exit p0

    return-object v5

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized auO()Ljava/lang/String;
    .locals 5

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lent;->amp:I

    if-nez v0, :cond_0

    .line 154
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :goto_0
    monitor-exit p0

    return-object v0

    .line 156
    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 157
    iget-wide v2, p0, Lent;->cgn:J

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    .line 158
    iget-object v2, p0, Lent;->cgo:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    .line 159
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :cond_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized auP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lent;->bbz:Z

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lent;->bbz:Z

    .line 168
    invoke-virtual {p0}, Lent;->auO()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 170
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ExpiringSum["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lent;->cgo:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
