.class public final Lelu;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/text/format/Time;)I
    .locals 4

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v2, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/text/format/Time;Landroid/text/format/Time;)I
    .locals 2
    .param p0    # Landroid/text/format/Time;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134
    if-nez p0, :cond_0

    .line 135
    new-instance p0, Landroid/text/format/Time;

    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    .line 136
    invoke-virtual {p0}, Landroid/text/format/Time;->setToNow()V

    .line 138
    :cond_0
    invoke-static {p1}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v0

    invoke-static {p0}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public static a(Ljno;)I
    .locals 2

    .prologue
    .line 154
    invoke-static {p0}, Lelu;->c(Ljno;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lelu;->aM(J)Landroid/text/format/Time;

    move-result-object v0

    invoke-static {v0}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v0

    return v0
.end method

.method public static a(Ljno;Ljno;)I
    .locals 4

    .prologue
    .line 149
    invoke-static {p0}, Lelu;->c(Ljno;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1}, Lelu;->c(Ljno;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljno;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    invoke-static {p1}, Lelu;->c(Ljno;)J

    move-result-wide v0

    invoke-static {p0, v0, v1, p2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljnm;Lemp;)Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ljnm;->euZ:Ljno;

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Ljnm;->euY:Ljno;

    invoke-static {v0, p1}, Lelu;->a(Ljno;Lemp;)Z

    move-result v0

    .line 115
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljnm;->euZ:Ljno;

    invoke-static {v0, p1}, Lelu;->a(Ljno;Lemp;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljno;Lemp;)Z
    .locals 4

    .prologue
    .line 119
    invoke-static {p0}, Lelu;->c(Ljno;)J

    move-result-wide v0

    invoke-interface {p1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aM(J)Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 177
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 178
    return-object v0
.end method

.method public static aN(J)Ljno;
    .locals 2

    .prologue
    .line 199
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 200
    new-instance v1, Ljno;

    invoke-direct {v1}, Ljno;-><init>()V

    invoke-virtual {v1, p0, p1}, Ljno;->dA(J)Ljno;

    move-result-object v1

    invoke-virtual {v0, p0, p1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    invoke-virtual {v1, v0}, Ljno;->qT(I)Ljno;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljno;)Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 169
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 170
    invoke-static {p0}, Lelu;->c(Ljno;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 171
    return-object v0
.end method

.method public static c(Ljno;)J
    .locals 6

    .prologue
    .line 188
    invoke-virtual {p0}, Ljno;->bqO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Ljno;->Pu()J

    move-result-wide v0

    .line 193
    :goto_0
    return-wide v0

    .line 192
    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 193
    invoke-virtual {p0}, Ljno;->Pu()J

    move-result-wide v2

    invoke-virtual {p0}, Ljno;->Pu()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v0, v0

    sub-long v0, v2, v0

    goto :goto_0
.end method
