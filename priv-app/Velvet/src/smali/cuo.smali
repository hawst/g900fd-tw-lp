.class public abstract Lcuo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final bjM:Lcuo;

.field static final bjN:Lcuo;

.field static final bjO:Lcuo;

.field static final bjP:Lcuo;

.field static final bjQ:Lcuo;

.field private static bjR:[Lcuo;


# instance fields
.field private final axN:Ljava/lang/String;

.field private final bjS:Ljava/lang/String;

.field private bjT:Lbem;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    new-instance v0, Lcta;

    invoke-direct {v0}, Lcta;-><init>()V

    sput-object v0, Lcuo;->bjM:Lcuo;

    .line 17
    new-instance v0, Lcuk;

    invoke-direct {v0}, Lcuk;-><init>()V

    sput-object v0, Lcuo;->bjN:Lcuo;

    .line 18
    new-instance v0, Lcul;

    const-string v1, "emails"

    const-string v2, "email"

    invoke-direct {v0, v1, v2}, Lcul;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcuo;->bjO:Lcuo;

    .line 20
    new-instance v0, Lcul;

    const-string v1, "phones"

    const-string v2, "phone"

    invoke-direct {v0, v1, v2}, Lcul;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcuo;->bjP:Lcuo;

    .line 22
    new-instance v0, Lcul;

    const-string v1, "postals"

    const-string v2, "postal"

    invoke-direct {v0, v1, v2}, Lcul;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcuo;->bjQ:Lcuo;

    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [Lcuo;

    const/4 v1, 0x0

    sget-object v2, Lcuo;->bjM:Lcuo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcuo;->bjN:Lcuo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcuo;->bjO:Lcuo;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcuo;->bjP:Lcuo;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcuo;->bjQ:Lcuo;

    aput-object v2, v0, v1

    sput-object v0, Lcuo;->bjR:[Lcuo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcuo;->axN:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcuo;->bjS:Ljava/lang/String;

    .line 63
    return-void
.end method

.method static Tc()[Lbem;
    .locals 3

    .prologue
    .line 33
    sget-object v0, Lcuo;->bjR:[Lcuo;

    array-length v0, v0

    new-array v1, v0, [Lbem;

    .line 34
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcuo;->bjR:[Lcuo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 35
    sget-object v2, Lcuo;->bjR:[Lcuo;

    aget-object v2, v2, v0

    .line 36
    invoke-virtual {v2}, Lcuo;->SZ()Lbem;

    move-result-object v2

    aput-object v2, v1, v0

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    return-object v1
.end method

.method public static xE()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    sget-object v0, Lcuo;->bjR:[Lcuo;

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcuo;->bjR:[Lcuo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 47
    sget-object v2, Lcuo;->bjR:[Lcuo;

    aget-object v2, v2, v0

    .line 48
    invoke-virtual {v2}, Lcuo;->xF()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    return-object v1
.end method


# virtual methods
.method final SZ()Lbem;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcuo;->bjT:Lbem;

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcuo;->axN:Ljava/lang/String;

    new-instance v1, Lben;

    invoke-direct {v1, v0}, Lben;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcuo;->bjS:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lben;->eX(Ljava/lang/String;)Lben;

    move-result-object v0

    .line 81
    invoke-virtual {p0, v0}, Lcuo;->a(Lben;)Lbem;

    move-result-object v0

    iput-object v0, p0, Lcuo;->bjT:Lbem;

    .line 83
    :cond_0
    iget-object v0, p0, Lcuo;->bjT:Lbem;

    return-object v0
.end method

.method protected abstract a(Lben;)Lbem;
.end method

.method public final xF()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcuo;->axN:Ljava/lang/String;

    iget-object v1, p0, Lcuo;->bjS:Ljava/lang/String;

    invoke-static {v0, v1}, Lbem;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
