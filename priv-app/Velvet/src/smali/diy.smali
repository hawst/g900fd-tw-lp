.class final Ldiy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lepn;


# instance fields
.field private synthetic aSa:Lcom/google/android/shared/search/Query;

.field private synthetic anv:Lemy;

.field private synthetic bzj:Ldiu;

.field private synthetic bzk:Ldiq;

.field private synthetic bzl:I

.field private synthetic bzm:Landroid/os/CancellationSignal;


# direct methods
.method constructor <init>(Ldiu;Ldiq;Lcom/google/android/shared/search/Query;ILandroid/os/CancellationSignal;Lemy;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Ldiy;->bzj:Ldiu;

    iput-object p2, p0, Ldiy;->bzk:Ldiq;

    iput-object p3, p0, Ldiy;->aSa:Lcom/google/android/shared/search/Query;

    iput p4, p0, Ldiy;->bzl:I

    iput-object p5, p0, Ldiy;->bzm:Landroid/os/CancellationSignal;

    iput-object p6, p0, Ldiy;->anv:Lemy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Ldiy;->bzk:Ldiq;

    invoke-interface {v0}, Ldiq;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 89
    iget-object v0, p0, Ldiy;->bzk:Ldiq;

    iget-object v1, p0, Ldiy;->bzj:Ldiu;

    iget-object v1, v1, Ldiu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldiy;->aSa:Lcom/google/android/shared/search/Query;

    iget v3, p0, Ldiy;->bzl:I

    iget-object v4, p0, Ldiy;->bzm:Landroid/os/CancellationSignal;

    invoke-interface {v0, v1, v2, v3, v4}, Ldiq;->a(Landroid/content/Context;Lcom/google/android/shared/search/Query;ILandroid/os/CancellationSignal;)Ldef;

    move-result-object v1

    .line 90
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ldef;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 91
    :goto_0
    iget-object v2, p0, Ldiy;->bzj:Ldiu;

    iget-boolean v2, v2, Ldiu;->bzc:Z

    if-eqz v2, :cond_2

    if-nez v0, :cond_2

    .line 92
    iget-object v0, p0, Ldiy;->bzj:Ldiu;

    iget-object v0, v0, Ldiu;->buk:Leqo;

    iget-object v2, p0, Ldiy;->bzj:Ldiu;

    iget-object v2, v2, Ldiu;->bzh:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 96
    :goto_1
    iget-object v0, p0, Ldiy;->bzj:Ldiu;

    iget-object v0, v0, Ldiu;->buk:Leqo;

    iget-object v2, p0, Ldiy;->anv:Lemy;

    invoke-static {v0, v2, v1}, Lemz;->a(Ljava/util/concurrent/Executor;Lemy;Ljava/lang/Object;)V

    .line 97
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Ldiy;->bzj:Ldiu;

    iget-object v0, v0, Ldiu;->buk:Leqo;

    iget-object v2, p0, Ldiy;->bzj:Ldiu;

    iget-object v2, v2, Ldiu;->bzi:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
