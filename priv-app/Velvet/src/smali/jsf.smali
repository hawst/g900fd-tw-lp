.class public final Ljsf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eCe:[Ljsf;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ajm:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 821
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 822
    const/4 v0, 0x0

    iput v0, p0, Ljsf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljsf;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljsf;->ajm:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljsf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljsf;->eCz:I

    .line 823
    return-void
.end method

.method public static btJ()[Ljsf;
    .locals 2

    .prologue
    .line 764
    sget-object v0, Ljsf;->eCe:[Ljsf;

    if-nez v0, :cond_1

    .line 765
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 767
    :try_start_0
    sget-object v0, Ljsf;->eCe:[Ljsf;

    if-nez v0, :cond_0

    .line 768
    const/4 v0, 0x0

    new-array v0, v0, [Ljsf;

    sput-object v0, Ljsf;->eCe:[Ljsf;

    .line 770
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    :cond_1
    sget-object v0, Ljsf;->eCe:[Ljsf;

    return-object v0

    .line 770
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljsf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsf;->agq:Ljava/lang/String;

    iget v0, p0, Ljsf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsf;->ajm:Ljava/lang/String;

    iget v0, p0, Ljsf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 837
    iget v0, p0, Ljsf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 838
    const/4 v0, 0x1

    iget-object v1, p0, Ljsf;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 840
    :cond_0
    iget v0, p0, Ljsf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 841
    const/4 v0, 0x2

    iget-object v1, p0, Ljsf;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 843
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 844
    return-void
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Ljsf;->ajm:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 848
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 849
    iget v1, p0, Ljsf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 850
    const/4 v1, 0x1

    iget-object v2, p0, Ljsf;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 853
    :cond_0
    iget v1, p0, Ljsf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 854
    const/4 v1, 0x2

    iget-object v2, p0, Ljsf;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 857
    :cond_1
    return v0
.end method

.method public final yE(Ljava/lang/String;)Ljsf;
    .locals 1

    .prologue
    .line 805
    if-nez p1, :cond_0

    .line 806
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 808
    :cond_0
    iput-object p1, p0, Ljsf;->ajm:Ljava/lang/String;

    .line 809
    iget v0, p0, Ljsf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsf;->aez:I

    .line 810
    return-object p0
.end method
