.class public Ldyq;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final bQV:Landroid/widget/ImageView;

.field private final bQW:Landroid/view/View;

.field private final bQX:Z

.field private final bQY:Z

.field private bQZ:I

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/widget/ImageView;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 35
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, Ldyq;-><init>(Landroid/content/res/Resources;Landroid/widget/ImageView;Landroid/view/View;ZZ)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/widget/ImageView;Landroid/view/View;ZZ)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Ldyq;->bQZ:I

    .line 45
    iput-object p1, p0, Ldyq;->mResources:Landroid/content/res/Resources;

    .line 46
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldyq;->bQV:Landroid/widget/ImageView;

    .line 47
    iput-object p3, p0, Ldyq;->bQW:Landroid/view/View;

    .line 48
    iput-boolean p4, p0, Ldyq;->bQX:Z

    .line 49
    iput-boolean p5, p0, Ldyq;->bQY:Z

    .line 50
    return-void
.end method


# virtual methods
.method protected alN()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 21
    check-cast p1, [Lcom/google/android/search/shared/contact/Person;

    const/4 v0, 0x0

    aget-object v1, p1, v0

    iget-object v0, p0, Ldyq;->bQV:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Leng;->a(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Ldyq;->bQW:Landroid/view/View;

    if-eqz v0, :cond_1

    const/16 v0, 0x10

    invoke-static {v2, v0}, Lkm;->b(Landroid/graphics/Bitmap;I)Lkm;

    move-result-object v1

    iget-object v0, v1, Lkm;->jQ:Lkp;

    if-nez v0, :cond_0

    iget-object v0, v1, Lkm;->jS:Lkp;

    if-nez v0, :cond_0

    iget-object v0, v1, Lkm;->jT:Lkp;

    :cond_0
    if-eqz v0, :cond_1

    iget v0, v0, Lkp;->kb:I

    iput v0, p0, Ldyq;->bQZ:I

    :cond_1
    iget-boolean v0, p0, Ldyq;->bQX:Z

    if-eqz v0, :cond_3

    new-instance v0, Lejc;

    invoke-direct {v0, v2}, Lejc;-><init>(Landroid/graphics/Bitmap;)V

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Ldyq;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_4
    new-instance v0, Ldze;

    iget-object v2, p0, Ldyq;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v1

    iget-boolean v4, p0, Ldyq;->bQX:Z

    invoke-direct {v0, v2, v3, v1, v4}, Ldze;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p0, Ldyq;->bQW:Landroid/view/View;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ldze;->getBackgroundColor()I

    move-result v1

    iput v1, p0, Ldyq;->bQZ:I

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 21
    check-cast p1, Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_2

    iget-object v0, p0, Ldyq;->bQV:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Ldyq;->bQV:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Ldyq;->bQW:Landroid/view/View;

    if-eqz v0, :cond_1

    iget v0, p0, Ldyq;->bQZ:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyq;->bQW:Landroid/view/View;

    iget v1, p0, Ldyq;->bQZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    invoke-virtual {p0}, Ldyq;->alN()V

    return-void

    :cond_2
    iget-boolean v0, p0, Ldyq;->bQY:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldyq;->bQV:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
