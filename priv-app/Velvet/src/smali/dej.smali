.class public final Ldej;
.super Lddx;
.source "PG"


# instance fields
.field private final bvp:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldej;->bvp:Ljava/util/HashSet;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/shared/search/Suggestion;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Ldej;->bvp:Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Ldej;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 51
    iget-object v0, p0, Ldej;->bvp:Ljava/util/HashSet;

    invoke-virtual {p2}, Lcom/google/android/shared/search/Suggestion;->asG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-super {p0, p1, p2}, Lddx;->a(ILcom/google/android/shared/search/Suggestion;)V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-virtual {p0, p1}, Ldej;->remove(I)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asG()Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v2, p0, Ldej;->bvp:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    invoke-super {p0, p1}, Lddx;->d(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v1

    .line 92
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asG()Ljava/lang/String;

    move-result-object v2

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Ldej;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_5

    invoke-virtual {p0, v0}, Ldej;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Suggestion;->asG()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 77
    :goto_2
    invoke-virtual {p0, v0}, Ldej;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v3

    .line 78
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asE()J

    move-result-wide v4

    invoke-virtual {v3}, Lcom/google/android/shared/search/Suggestion;->asE()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-lez v2, :cond_6

    move-object v2, p1

    .line 80
    :goto_3
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v4

    if-nez v4, :cond_3

    .line 82
    new-instance v4, Lded;

    invoke-direct {v4}, Lded;-><init>()V

    invoke-virtual {v4, v2}, Lded;->h(Lcom/google/android/shared/search/Suggestion;)Lded;

    move-result-object v2

    const/4 v4, 0x1

    iput-boolean v4, v2, Lded;->buN:Z

    invoke-virtual {v2}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v2

    .line 86
    :cond_3
    if-eq v3, v2, :cond_0

    .line 88
    invoke-virtual {p0, v0, v2}, Ldej;->a(ILcom/google/android/shared/search/Suggestion;)V

    goto :goto_0

    .line 76
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, -0x1

    goto :goto_2

    :cond_6
    move-object v2, v3

    .line 78
    goto :goto_3
.end method

.method public final remove(I)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Ldej;->bvp:Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Ldej;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 45
    invoke-super {p0, p1}, Lddx;->remove(I)V

    .line 46
    return-void
.end method
