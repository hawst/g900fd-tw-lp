.class final Laec;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic Nm:Landroid/net/Uri;

.field private synthetic Np:Landroid/content/Context;

.field private synthetic Un:Ladz;

.field private synthetic Up:Landroid/graphics/Point;

.field private synthetic Uq:Landroid/widget/ImageView;

.field private synthetic Ur:Landroid/widget/FrameLayout;


# direct methods
.method constructor <init>(Ladz;Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Point;Landroid/widget/ImageView;Landroid/widget/FrameLayout;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Laec;->Un:Ladz;

    iput-object p2, p0, Laec;->Np:Landroid/content/Context;

    iput-object p3, p0, Laec;->Nm:Landroid/net/Uri;

    iput-object p4, p0, Laec;->Up:Landroid/graphics/Point;

    iput-object p5, p0, Laec;->Uq:Landroid/widget/ImageView;

    iput-object p6, p0, Laec;->Ur:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs kB()Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 844
    :try_start_0
    iget-object v0, p0, Laec;->Np:Landroid/content/Context;

    iget-object v1, p0, Laec;->Nm:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v6

    .line 845
    iget-object v0, p0, Laec;->Up:Landroid/graphics/Point;

    iget-object v1, p0, Laec;->Np:Landroid/content/Context;

    iget-object v2, p0, Laec;->Nm:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Ladz;->b(Landroid/graphics/Point;Landroid/content/Context;Landroid/net/Uri;[BLandroid/content/res/Resources;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 858
    :goto_0
    return-object v0

    .line 846
    :catch_0
    move-exception v0

    .line 847
    iget-object v1, p0, Laec;->Un:Ladz;

    invoke-virtual {v1}, Ladz;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 853
    invoke-virtual {p0, v9}, Laec;->cancel(Z)Z

    move-object v0, v8

    .line 858
    goto :goto_0

    .line 856
    :cond_0
    throw v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 841
    invoke-direct {p0}, Laec;->kB()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 841
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Laec;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Laec;->Uq:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Laec;->Uq:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    iget-object v0, p0, Laec;->Ur:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Launcher.WallpaperPickerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error loading thumbnail for uri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laec;->Nm:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
