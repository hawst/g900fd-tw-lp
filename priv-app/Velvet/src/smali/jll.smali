.class public final Ljll;
.super Ljsl;
.source "PG"


# static fields
.field public static final erS:Ljsm;


# instance fields
.field private aez:I

.field private dIF:I

.field private erT:[Ljky;

.field private erU:Ljava/lang/String;

.field private erV:Ljava/lang/String;

.field private erW:[B

.field private erX:Ljava/lang/String;

.field private erY:Ljava/lang/String;

.field private erZ:Z

.field private erm:Ljava/lang/String;

.field private esa:Ljava/lang/String;

.field private esb:[Ljlu;

.field private esc:Ljava/lang/String;

.field private esd:Ljava/lang/String;

.field private ese:Ljava/lang/String;

.field private esf:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15235
    const/16 v0, 0xb

    const-class v1, Ljll;

    const v2, 0x1977653a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljll;->erS:Ljsm;

    .line 15252
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15545
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15546
    iput v1, p0, Ljll;->aez:I

    invoke-static {}, Ljky;->boH()[Ljky;

    move-result-object v0

    iput-object v0, p0, Ljll;->erT:[Ljky;

    const-string v0, ""

    iput-object v0, p0, Ljll;->erm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljll;->erU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljll;->erV:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljll;->erW:[B

    const-string v0, ""

    iput-object v0, p0, Ljll;->erX:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljll;->erY:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljll;->dIF:I

    iput-boolean v1, p0, Ljll;->erZ:Z

    const-string v0, ""

    iput-object v0, p0, Ljll;->esa:Ljava/lang/String;

    invoke-static {}, Ljlu;->bpv()[Ljlu;

    move-result-object v0

    iput-object v0, p0, Ljll;->esb:[Ljlu;

    const-string v0, ""

    iput-object v0, p0, Ljll;->esc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljll;->esd:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljll;->ese:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljll;->esf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljll;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljll;->eCz:I

    .line 15547
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15228
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljll;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljll;->erT:[Ljky;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljky;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljll;->erT:[Ljky;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljky;

    invoke-direct {v3}, Ljky;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljll;->erT:[Ljky;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljky;

    invoke-direct {v3}, Ljky;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljll;->erT:[Ljky;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->erm:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljll;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljll;->erW:[B

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljll;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljll;->esb:[Ljlu;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlu;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljll;->esb:[Ljlu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljlu;

    invoke-direct {v3}, Ljlu;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljll;->esb:[Ljlu;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljlu;

    invoke-direct {v3}, Ljlu;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljll;->esb:[Ljlu;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->erX:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->erU:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->erV:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljll;->dIF:I

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljll;->erZ:Z

    iget v0, p0, Ljll;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->erY:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->esa:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->esc:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->esd:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->ese:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljll;->esf:Ljava/lang/String;

    iget v0, p0, Ljll;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljll;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15574
    iget-object v0, p0, Ljll;->erT:[Ljky;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljll;->erT:[Ljky;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 15575
    :goto_0
    iget-object v2, p0, Ljll;->erT:[Ljky;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 15576
    iget-object v2, p0, Ljll;->erT:[Ljky;

    aget-object v2, v2, v0

    .line 15577
    if-eqz v2, :cond_0

    .line 15578
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 15575
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15582
    :cond_1
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 15583
    const/4 v0, 0x2

    iget-object v2, p0, Ljll;->erm:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 15585
    :cond_2
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 15586
    const/4 v0, 0x3

    iget-object v2, p0, Ljll;->erW:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    .line 15588
    :cond_3
    iget-object v0, p0, Ljll;->esb:[Ljlu;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljll;->esb:[Ljlu;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 15589
    :goto_1
    iget-object v0, p0, Ljll;->esb:[Ljlu;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 15590
    iget-object v0, p0, Ljll;->esb:[Ljlu;

    aget-object v0, v0, v1

    .line 15591
    if-eqz v0, :cond_4

    .line 15592
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 15589
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 15596
    :cond_5
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 15597
    const/4 v0, 0x5

    iget-object v1, p0, Ljll;->erX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15599
    :cond_6
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 15600
    const/4 v0, 0x6

    iget-object v1, p0, Ljll;->erU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15602
    :cond_7
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 15603
    const/4 v0, 0x7

    iget-object v1, p0, Ljll;->erV:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15605
    :cond_8
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 15606
    const/16 v0, 0x8

    iget v1, p0, Ljll;->dIF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 15608
    :cond_9
    iget v0, p0, Ljll;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_a

    .line 15609
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljll;->erZ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15611
    :cond_a
    iget v0, p0, Ljll;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_b

    .line 15612
    const/16 v0, 0xa

    iget-object v1, p0, Ljll;->erY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15614
    :cond_b
    iget v0, p0, Ljll;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_c

    .line 15615
    const/16 v0, 0xb

    iget-object v1, p0, Ljll;->esa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15617
    :cond_c
    iget v0, p0, Ljll;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_d

    .line 15618
    const/16 v0, 0xc

    iget-object v1, p0, Ljll;->esc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15620
    :cond_d
    iget v0, p0, Ljll;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_e

    .line 15621
    const/16 v0, 0xd

    iget-object v1, p0, Ljll;->esd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15623
    :cond_e
    iget v0, p0, Ljll;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_f

    .line 15624
    const/16 v0, 0xe

    iget-object v1, p0, Ljll;->ese:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15626
    :cond_f
    iget v0, p0, Ljll;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_10

    .line 15627
    const/16 v0, 0xf

    iget-object v1, p0, Ljll;->esf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15629
    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15630
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 15634
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 15635
    iget-object v2, p0, Ljll;->erT:[Ljky;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljll;->erT:[Ljky;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 15636
    :goto_0
    iget-object v3, p0, Ljll;->erT:[Ljky;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 15637
    iget-object v3, p0, Ljll;->erT:[Ljky;

    aget-object v3, v3, v0

    .line 15638
    if-eqz v3, :cond_0

    .line 15639
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 15636
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 15644
    :cond_2
    iget v2, p0, Ljll;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 15645
    const/4 v2, 0x2

    iget-object v3, p0, Ljll;->erm:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 15648
    :cond_3
    iget v2, p0, Ljll;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_4

    .line 15649
    const/4 v2, 0x3

    iget-object v3, p0, Ljll;->erW:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 15652
    :cond_4
    iget-object v2, p0, Ljll;->esb:[Ljlu;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljll;->esb:[Ljlu;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 15653
    :goto_1
    iget-object v2, p0, Ljll;->esb:[Ljlu;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 15654
    iget-object v2, p0, Ljll;->esb:[Ljlu;

    aget-object v2, v2, v1

    .line 15655
    if-eqz v2, :cond_5

    .line 15656
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 15653
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 15661
    :cond_6
    iget v1, p0, Ljll;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 15662
    const/4 v1, 0x5

    iget-object v2, p0, Ljll;->erX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15665
    :cond_7
    iget v1, p0, Ljll;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_8

    .line 15666
    const/4 v1, 0x6

    iget-object v2, p0, Ljll;->erU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15669
    :cond_8
    iget v1, p0, Ljll;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_9

    .line 15670
    const/4 v1, 0x7

    iget-object v2, p0, Ljll;->erV:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15673
    :cond_9
    iget v1, p0, Ljll;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_a

    .line 15674
    const/16 v1, 0x8

    iget v2, p0, Ljll;->dIF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15677
    :cond_a
    iget v1, p0, Ljll;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_b

    .line 15678
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljll;->erZ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 15681
    :cond_b
    iget v1, p0, Ljll;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_c

    .line 15682
    const/16 v1, 0xa

    iget-object v2, p0, Ljll;->erY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15685
    :cond_c
    iget v1, p0, Ljll;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_d

    .line 15686
    const/16 v1, 0xb

    iget-object v2, p0, Ljll;->esa:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15689
    :cond_d
    iget v1, p0, Ljll;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_e

    .line 15690
    const/16 v1, 0xc

    iget-object v2, p0, Ljll;->esc:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15693
    :cond_e
    iget v1, p0, Ljll;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_f

    .line 15694
    const/16 v1, 0xd

    iget-object v2, p0, Ljll;->esd:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15697
    :cond_f
    iget v1, p0, Ljll;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_10

    .line 15698
    const/16 v1, 0xe

    iget-object v2, p0, Ljll;->ese:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15701
    :cond_10
    iget v1, p0, Ljll;->aez:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_11

    .line 15702
    const/16 v1, 0xf

    iget-object v2, p0, Ljll;->esf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15705
    :cond_11
    return v0
.end method
