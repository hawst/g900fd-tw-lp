.class public final Ljfi;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eib:[Ljfi;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dXd:I

.field private ehX:Z

.field private ehZ:Ljava/lang/String;

.field private eia:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11526
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 11527
    iput v1, p0, Ljfi;->aez:I

    iput v1, p0, Ljfi;->dXd:I

    const-string v0, ""

    iput-object v0, p0, Ljfi;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljfi;->ehZ:Ljava/lang/String;

    iput-boolean v1, p0, Ljfi;->ehX:Z

    iput-boolean v1, p0, Ljfi;->eia:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfi;->eCz:I

    .line 11528
    return-void
.end method

.method public static bjp()[Ljfi;
    .locals 2

    .prologue
    .line 11412
    sget-object v0, Ljfi;->eib:[Ljfi;

    if-nez v0, :cond_1

    .line 11413
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 11415
    :try_start_0
    sget-object v0, Ljfi;->eib:[Ljfi;

    if-nez v0, :cond_0

    .line 11416
    const/4 v0, 0x0

    new-array v0, v0, [Ljfi;

    sput-object v0, Ljfi;->eib:[Ljfi;

    .line 11418
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11420
    :cond_1
    sget-object v0, Ljfi;->eib:[Ljfi;

    return-object v0

    .line 11418
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfi;->dXd:I

    iget v0, p0, Ljfi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfi;->agq:Ljava/lang/String;

    iget v0, p0, Ljfi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfi;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfi;->ehZ:Ljava/lang/String;

    iget v0, p0, Ljfi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfi;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfi;->ehX:Z

    iget v0, p0, Ljfi;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfi;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfi;->eia:Z

    iget v0, p0, Ljfi;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfi;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 11545
    iget v0, p0, Ljfi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11546
    const/4 v0, 0x1

    iget v1, p0, Ljfi;->dXd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 11548
    :cond_0
    iget v0, p0, Ljfi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 11549
    const/4 v0, 0x2

    iget-object v1, p0, Ljfi;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11551
    :cond_1
    iget v0, p0, Ljfi;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 11552
    const/4 v0, 0x3

    iget-object v1, p0, Ljfi;->ehZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11554
    :cond_2
    iget v0, p0, Ljfi;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 11555
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfi;->ehX:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11557
    :cond_3
    iget v0, p0, Ljfi;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 11558
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljfi;->eia:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11560
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 11561
    return-void
.end method

.method public final bew()I
    .locals 1

    .prologue
    .line 11428
    iget v0, p0, Ljfi;->dXd:I

    return v0
.end method

.method public final bjo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11469
    iget-object v0, p0, Ljfi;->ehZ:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 11565
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 11566
    iget v1, p0, Ljfi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11567
    const/4 v1, 0x1

    iget v2, p0, Ljfi;->dXd:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11570
    :cond_0
    iget v1, p0, Ljfi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 11571
    const/4 v1, 0x2

    iget-object v2, p0, Ljfi;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11574
    :cond_1
    iget v1, p0, Ljfi;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 11575
    const/4 v1, 0x3

    iget-object v2, p0, Ljfi;->ehZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11578
    :cond_2
    iget v1, p0, Ljfi;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 11579
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfi;->ehX:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11582
    :cond_3
    iget v1, p0, Ljfi;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 11583
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljfi;->eia:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11586
    :cond_4
    return v0
.end method
