.class public final Lfvs;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfyj;


# instance fields
.field private final cCF:Ljal;

.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 52
    iget-object v0, p1, Lizj;->dTi:Ljal;

    iput-object v0, p0, Lfvs;->cCF:Ljal;

    .line 53
    iput-object p4, p0, Lfvs;->mTimeToLeaveFactory:Lfyk;

    .line 54
    iput-object p5, p0, Lfvs;->mStringEvaluator:Lgbr;

    .line 55
    return-void
.end method

.method private d(Landroid/content/Context;J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 219
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v1}, Ljal;->beo()Ljava/lang/String;

    move-result-object v1

    .line 221
    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 222
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e(Landroid/content/Context;J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 227
    iget-object v1, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v1}, Ljal;->beo()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 229
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final M(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {v0}, Lgbf;->a(Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    .line 59
    new-instance v7, Lang;

    invoke-direct {v7}, Lang;-><init>()V

    .line 60
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lftf;

    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    iget-object v3, p0, Lfvs;->mStringEvaluator:Lgbr;

    invoke-direct {v9, v0, v1, v2, v3}, Lftf;-><init>(Ljal;Lizj;Lfmt;Lgbr;)V

    iget-object v0, p0, Lfuz;->mClock:Lemp;

    iget-object v1, p0, Lfvs;->mTimeToLeaveFactory:Lfyk;

    invoke-virtual {v9, p1, v0, v1}, Lftf;->a(Landroid/content/Context;Lemp;Lfyk;)Lanh;

    move-result-object v0

    invoke-static {v8, v0}, Lfvs;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-virtual {v9}, Lftf;->aDp()Lanh;

    move-result-object v0

    invoke-static {v8, v0}, Lfvs;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-virtual {v9, p1}, Lftf;->aR(Landroid/content/Context;)Lanh;

    move-result-object v0

    invoke-static {v8, v0}, Lfvs;->a(Ljava/util/List;Ljava/lang/Object;)V

    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v10, v0, Ljal;->dWL:Ljak;

    new-instance v11, Laou;

    invoke-direct {v11}, Laou;-><init>()V

    const v0, 0x7f0a0357

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {v0}, Lgbf;->b(Ljak;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Laou;->ct(Ljava/lang/String;)Laou;

    :cond_0
    iget-object v0, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v0}, Ljal;->bep()Z

    move-result v0

    if-eqz v0, :cond_5

    const v6, 0x80016

    :goto_0
    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-virtual {v0}, Ljak;->bej()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljak;->bei()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_7

    new-instance v0, Lfyg;

    invoke-direct {v0, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v1}, Ljal;->bek()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    move-object v1, p1

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0358

    invoke-virtual {v0, v2, v1}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v1}, Ljal;->ben()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v1}, Ljal;->bem()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    move-object v1, p1

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0359

    invoke-virtual {v0, v2, v1}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    :cond_1
    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    :goto_2
    const/4 v1, 0x1

    new-array v1, v1, [Laow;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    iput-object v1, v11, Laou;->akm:[Laow;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    iput-object v11, v0, Lanh;->agN:Laou;

    invoke-virtual {v10}, Ljak;->beh()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lfsa;

    const/16 v2, 0x6f

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0201e1

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    invoke-virtual {v10}, Ljak;->beg()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    :cond_2
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v1, v0, Ljal;->dWL:Ljak;

    invoke-virtual {v1}, Ljak;->bdT()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_3
    invoke-static {v8, v0}, Lfvs;->a(Ljava/util/List;Ljava/lang/Object;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    invoke-virtual {v1}, Ljak;->bei()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->bfa()Ljbp;

    :cond_3
    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    const/4 v2, 0x1

    invoke-virtual {v9, p1, v1, v2}, Lftf;->a(Landroid/content/Context;Lfmt;Z)Lanh;

    move-result-object v1

    invoke-static {v8, v1}, Lfvs;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    invoke-virtual {v1, v0}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    :cond_4
    iget-object v0, p0, Lfvs;->mEntry:Lizj;

    iget-object v1, p0, Lfvs;->cCF:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->dMX:[Ljbg;

    const/16 v2, 0x6f

    invoke-static {p1, v0, v1, v2}, Lfsx;->a(Landroid/content/Context;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    invoke-static {v8, v0}, Lfvs;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v7, Lang;->ags:[Lanh;

    .line 61
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lang;->aS(Z)Lang;

    .line 62
    return-object v7

    .line 60
    :cond_5
    const v6, 0x18013

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v0}, Ljal;->bek()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-object v2, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v2}, Ljal;->getEventType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x0

    goto/16 :goto_2

    :pswitch_0
    iget-object v2, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v2}, Ljal;->bep()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0, p1, v0, v1}, Lfvs;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    :goto_4
    new-instance v1, Lfyg;

    invoke-direct {v1, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0358

    invoke-virtual {v1, v2, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    goto/16 :goto_2

    :cond_8
    const v2, 0x7f0a0246

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0, p1, v0, v1}, Lfvs;->e(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0, p1, v0, v1}, Lfvs;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_9
    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_a

    const v0, 0x7f0a03fe

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_a
    invoke-direct {p0, p1, v0, v1}, Lfvs;->e(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :pswitch_1
    iget-object v2, p0, Lfvs;->cCF:Ljal;

    invoke-virtual {v2}, Ljal;->bep()Z

    move-result v2

    if-nez v2, :cond_c

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-direct {p0, p1, v0, v1}, Lfvs;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    :goto_5
    new-instance v1, Lfyg;

    invoke-direct {v1, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0359

    invoke-virtual {v1, v2, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    goto/16 :goto_2

    :cond_b
    const v2, 0x7f0a0246

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0, p1, v0, v1}, Lfvs;->e(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0, p1, v0, v1}, Lfvs;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_c
    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_d

    const v0, 0x7f0a03fe

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_d
    invoke-direct {p0, p1, v0, v1}, Lfvs;->e(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_e
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    const v0, 0x7f0a03be

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    new-instance v3, Lfsa;

    const/16 v4, 0xcf

    invoke-direct {v3, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f0201e1

    invoke-virtual {v3, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    invoke-virtual {v1}, Ljak;->bdS()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    iput-object v2, v0, Lanh;->agz:Laoj;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final aDC()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {v0}, Lgbf;->b(Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bd(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lfvs;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {p1, v0}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final be(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 240
    const v0, 0x7f0a0357

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
