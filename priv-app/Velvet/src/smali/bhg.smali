.class public abstract Lbhg;
.super Lbhe;

# interfaces
.implements Lbhz;


# instance fields
.field private final aAT:Lbhc;

.field private aBf:Lbhx;


# direct methods
.method public constructor <init>(Lbhc;)V
    .locals 1

    invoke-direct {p0}, Lbhe;-><init>()V

    invoke-static {p1}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhc;

    iput-object v0, p0, Lbhg;->aAT:Lbhc;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lbhg;->c(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method protected abstract a(Lbha;)V
.end method

.method public final a(Lbhx;)V
    .locals 0

    iput-object p1, p0, Lbhg;->aBf:Lbhx;

    return-void
.end method

.method public final b(Lbha;)V
    .locals 2

    iget-object v0, p0, Lbhg;->aAW:Lbhf;

    if-nez v0, :cond_0

    new-instance v0, Lbhf;

    invoke-interface {p1}, Lbha;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lbhf;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lbhg;->a(Lbhf;)V

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lbhg;->a(Lbha;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lbhg;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lbhg;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->yk()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, Lbjr;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lbhg;->a(Lcom/google/android/gms/common/api/Status;)Lbho;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbhg;->c(Lbho;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final yr()V
    .locals 1

    invoke-super {p0}, Lbhe;->yr()V

    iget-object v0, p0, Lbhg;->aBf:Lbhx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhg;->aBf:Lbhx;

    invoke-interface {v0, p0}, Lbhx;->b(Lbhz;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbhg;->aBf:Lbhx;

    :cond_0
    return-void
.end method

.method public final yv()Lbhc;
    .locals 1

    iget-object v0, p0, Lbhg;->aAT:Lbhc;

    return-object v0
.end method
