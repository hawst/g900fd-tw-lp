.class public final Ljqo;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezd:Ljsm;


# instance fields
.field private aez:I

.field private agv:I

.field private dON:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3234
    const/16 v0, 0xb

    const-class v1, Ljqo;

    const v2, 0x1dc91f1a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqo;->ezd:Ljsm;

    .line 3243
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3291
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3292
    const/4 v0, 0x0

    iput v0, p0, Ljqo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqo;->dON:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljqo;->agv:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqo;->eCz:I

    .line 3293
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3227
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqo;->dON:Ljava/lang/String;

    iget v0, p0, Ljqo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqo;->agv:I

    iget v0, p0, Ljqo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3307
    iget v0, p0, Ljqo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3308
    const/4 v0, 0x1

    iget-object v1, p0, Ljqo;->dON:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3310
    :cond_0
    iget v0, p0, Ljqo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3311
    const/4 v0, 0x2

    iget v1, p0, Ljqo;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3313
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3314
    return-void
.end method

.method public final baX()Z
    .locals 1

    .prologue
    .line 3264
    iget v0, p0, Ljqo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 3275
    iget v0, p0, Ljqo;->agv:I

    return v0
.end method

.method public final getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3253
    iget-object v0, p0, Ljqo;->dON:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3318
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3319
    iget v1, p0, Ljqo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3320
    const/4 v1, 0x1

    iget-object v2, p0, Ljqo;->dON:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3323
    :cond_0
    iget v1, p0, Ljqo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3324
    const/4 v1, 0x2

    iget v2, p0, Ljqo;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3327
    :cond_1
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 3283
    iget v0, p0, Ljqo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rx(I)Ljqo;
    .locals 1

    .prologue
    .line 3278
    iput p1, p0, Ljqo;->agv:I

    .line 3279
    iget v0, p0, Ljqo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqo;->aez:I

    .line 3280
    return-object p0
.end method

.method public final yi(Ljava/lang/String;)Ljqo;
    .locals 1

    .prologue
    .line 3256
    if-nez p1, :cond_0

    .line 3257
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3259
    :cond_0
    iput-object p1, p0, Ljqo;->dON:Ljava/lang/String;

    .line 3260
    iget v0, p0, Ljqo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqo;->aez:I

    .line 3261
    return-object p0
.end method
