.class public final Lcva;
.super Lerd;
.source "PG"


# instance fields
.field private final mHttpHelper:Ldkx;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Ldkx;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lerd;-><init>()V

    .line 36
    iput-object p1, p0, Lcva;->mHttpHelper:Ldkx;

    .line 37
    iput-object p2, p0, Lcva;->mResources:Landroid/content/res/Resources;

    .line 38
    return-void
.end method

.method private static p(III)I
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x1

    .line 128
    if-gt p1, p2, :cond_0

    if-le p0, p2, :cond_1

    .line 129
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 130
    int-to-float v1, p0

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 131
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 134
    :cond_1
    return v0
.end method

.method private w(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 42
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "https"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 44
    :try_start_0
    new-instance v0, Ldlb;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ldlb;-><init>(Ljava/lang/String;)V

    .line 47
    const v3, 0x15180

    invoke-virtual {v0, v3}, Ldlb;->fX(I)V

    .line 48
    iget-object v3, p0, Lcva;->mHttpHelper:Ldkx;

    const/4 v4, 0x7

    invoke-virtual {v3, v0, v4}, Ldkx;->c(Ldlb;I)[B

    move-result-object v3

    .line 49
    if-nez v3, :cond_2

    move-object v0, v2

    .line 119
    :goto_0
    return-object v0

    .line 56
    :cond_2
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 58
    const/4 v0, 0x0

    array-length v5, v3

    invoke-static {v3, v0, v5, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 59
    iget-object v0, p0, Lcva;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 60
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 62
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v0, :cond_3

    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lez v0, :cond_3

    .line 63
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v6, v5}, Lcva;->p(III)I

    move-result v0

    .line 65
    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 72
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 77
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 78
    mul-int/lit8 v6, v0, 0x10

    .line 79
    :goto_1
    if-gt v0, v6, :cond_7

    .line 80
    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 82
    const/4 v7, 0x0

    :try_start_1
    array-length v8, v3

    invoke-static {v3, v7, v8, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Left; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 94
    :goto_2
    if-eqz v0, :cond_6

    :try_start_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-gt v3, v5, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v3, v5, :cond_6

    .line 96
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v3, v4, v5}, Lcva;->p(III)I

    move-result v3

    .line 97
    if-le v3, v1, :cond_6

    .line 104
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int v3, v4, v3

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 109
    :goto_3
    if-nez v1, :cond_5

    .line 110
    const-string v0, "Search.NetworkImageLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Failed to decode "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 111
    goto/16 :goto_0

    .line 79
    :catch_0
    move-exception v7

    mul-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 113
    :cond_5
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcva;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Left; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 114
    :catch_1
    move-exception v0

    .line 115
    const-string v1, "Search.NetworkImageLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[1] Failed to load "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v2

    .line 116
    goto/16 :goto_0

    .line 117
    :catch_2
    move-exception v0

    .line 118
    const-string v1, "Search.NetworkImageLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[2] Failed to load "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v2

    .line 119
    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public final clearCache()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 145
    const-string v1, "http"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic v(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcva;->w(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
