.class final Lipe;
.super Lioy;
.source "PG"

# interfaces
.implements Ljava/util/SortedMap;


# direct methods
.method constructor <init>(Ljava/util/SortedMap;Lifw;)V
    .locals 0

    .prologue
    .line 1709
    invoke-direct {p0, p1, p2}, Lioy;-><init>(Ljava/util/Map;Lifw;)V

    .line 1710
    return-void
.end method


# virtual methods
.method final aYe()Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 1713
    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 1717
    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1722
    invoke-virtual {p0}, Lipe;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1738
    new-instance v1, Lipe;

    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lipe;->dBv:Lifw;

    invoke-direct {v1, v0, v2}, Lipe;-><init>(Ljava/util/SortedMap;Lifw;)V

    return-object v1
.end method

.method public final lastKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1726
    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 1729
    :goto_0
    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v1

    .line 1730
    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lipe;->y(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1731
    return-object v1

    .line 1733
    :cond_0
    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1742
    new-instance v1, Lipe;

    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lipe;->dBv:Lifw;

    invoke-direct {v1, v0, v2}, Lipe;-><init>(Ljava/util/SortedMap;Lifw;)V

    return-object v1
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1747
    new-instance v1, Lipe;

    iget-object v0, p0, Lipe;->dFP:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lipe;->dBv:Lifw;

    invoke-direct {v1, v0, v2}, Lipe;-><init>(Ljava/util/SortedMap;Lifw;)V

    return-object v1
.end method
