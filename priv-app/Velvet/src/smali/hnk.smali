.class public final Lhnk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldwu;


# instance fields
.field private bNR:Ldvt;

.field private synthetic dlF:Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;

.field private dlG:Lhop;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;Lhop;Ldvt;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lhnk;->dlF:Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    iput-object p2, p0, Lhnk;->dlG:Lhop;

    .line 224
    iput-object p3, p0, Lhnk;->bNR:Ldvt;

    .line 225
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for date argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for device settings argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for document argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for group argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lhnk;->dlF:Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;

    iget-object v1, p0, Lhnk;->dlG:Lhop;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->a(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;Lhop;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lhnk;->dlF:Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;

    iget-object v1, p0, Lhnk;->dlG:Lhop;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lhop;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for recurrence argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for string argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for time duration argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    const-string v0, "DisambiguationContent"

    const-string v1, "Can\'t show ambiguous UI for time of day argument"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lhnk;->dlF:Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;

    iget-object v1, p0, Lhnk;->dlG:Lhop;

    iget-object v2, p0, Lhnk;->bNR:Ldvt;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;Lhop;Ldvt;)V

    const/4 v0, 0x0

    return-object v0
.end method
