.class public abstract Lfkb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfjs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljal;)I
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljal;->dWL:Ljak;

    invoke-virtual {v0}, Ljak;->bee()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Ljal;->dWL:Ljak;

    invoke-virtual {v0}, Ljak;->bed()I

    move-result v0

    .line 75
    invoke-virtual {p0}, Ljal;->getEventType()I

    move-result v1

    .line 76
    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 77
    packed-switch v0, :pswitch_data_0

    .line 89
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 79
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 81
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 83
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract b(Lizj;Ljal;I)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c(ILizj;)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c(ILizq;)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final f(Lizq;)Ljava/lang/Object;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 100
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 101
    :cond_0
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 102
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lizj;->getType()I

    move-result v0

    .line 103
    const-string v1, "BaseEntryAdapterFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skipping EntryTreeNode with no group entry, and child of type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :goto_0
    const/4 v0, 0x0

    .line 111
    :goto_1
    return-object v0

    .line 106
    :cond_1
    const-string v0, "BaseEntryAdapterFactory"

    const-string v1, "Skipping EntryTreeNode with no group entry and no children"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    invoke-virtual {v0}, Lizj;->getType()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lfkb;->c(ILizq;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public g(Lizj;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public final r(Lizj;)Ljava/lang/Object;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p1, Lizj;->dSc:Ljal;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p1, Lizj;->dSc:Ljal;

    iget-object v1, p1, Lizj;->dSc:Ljal;

    invoke-static {v1}, Lfkb;->a(Ljal;)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lfkb;->b(Lizj;Ljal;I)Ljava/lang/Object;

    move-result-object v0

    .line 59
    :goto_0
    if-eqz v0, :cond_4

    :goto_1
    return-object v0

    .line 46
    :cond_0
    iget-object v0, p1, Lizj;->dSt:Ljal;

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p1, Lizj;->dSt:Ljal;

    iget-object v1, p1, Lizj;->dSt:Ljal;

    invoke-static {v1}, Lfkb;->a(Ljal;)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lfkb;->b(Lizj;Ljal;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p1, Lizj;->dTi:Ljal;

    if-eqz v0, :cond_2

    .line 50
    iget-object v0, p1, Lizj;->dTi:Ljal;

    const/4 v1, 0x3

    invoke-virtual {p0, p1, v0, v1}, Lfkb;->b(Lizj;Ljal;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_2
    iget-object v0, p1, Lizj;->dTj:Ljal;

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, p1, Lizj;->dTj:Ljal;

    const/4 v1, 0x4

    invoke-virtual {p0, p1, v0, v1}, Lfkb;->b(Lizj;Ljal;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_3
    invoke-virtual {p1}, Lizj;->getType()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lfkb;->c(ILizj;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_4
    invoke-virtual {p0, p1}, Lfkb;->g(Lizj;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method
