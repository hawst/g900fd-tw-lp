.class public Ldem;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bvq:[I

.field public static final bvr:Ldem;


# instance fields
.field private final aZG:J

.field private final bvs:Ldfd;

.field private final bvt:Landroid/util/SparseArray;

.field private final bvu:Leqn;

.field private bvv:Z

.field private mClosed:Z

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldem;->bvq:[I

    .line 46
    new-instance v0, Lden;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lden;-><init>(Lcom/google/android/shared/search/Query;JLdfd;)V

    sput-object v0, Ldem;->bvr:Ldem;

    return-void

    .line 36
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;JLdfd;)V
    .locals 8
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Landroid/util/SparseArray;

    sget-object v2, Ldem;->bvq:[I

    array-length v2, v2

    invoke-direct {v0, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    .line 68
    new-instance v0, Leqn;

    invoke-direct {v0}, Leqn;-><init>()V

    iput-object v0, p0, Ldem;->bvu:Leqn;

    .line 70
    iput-boolean v1, p0, Ldem;->bvv:Z

    .line 72
    iput-boolean v1, p0, Ldem;->mClosed:Z

    .line 77
    iput-object p1, p0, Ldem;->mQuery:Lcom/google/android/shared/search/Query;

    .line 78
    iput-wide p2, p0, Ldem;->aZG:J

    .line 79
    iput-object p4, p0, Ldem;->bvs:Ldfd;

    .line 81
    sget-object v2, Ldem;->bvq:[I

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 82
    iget-object v5, p0, Ldem;->bvt:Landroid/util/SparseArray;

    new-instance v6, Ldeo;

    invoke-direct {v6, v1}, Ldeo;-><init>(B)V

    invoke-virtual {v5, v4, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 364
    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iget-object v3, v0, Ldeo;->bvw:Ljava/util/List;

    .line 365
    const-string v0, "  [%s isDone=%b]\n"

    new-array v1, v7, [Ljava/lang/Object;

    aput-object p3, v1, v2

    invoke-virtual {p0, p2}, Ldem;->fK(I)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 366
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 367
    const-string v4, "    result #%d: source=%s, suggestions=%d:\n"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 370
    const-string v5, "      "

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 366
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 373
    :cond_1
    return-void
.end method

.method private a(Ldeo;Ldef;)Z
    .locals 3

    .prologue
    .line 157
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ldef;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Ldeo;->bvx:Z

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    const/4 v0, 0x0

    .line 171
    :goto_0
    return v0

    .line 164
    :cond_1
    iget-object v0, p0, Ldem;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-interface {p2}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/shared/search/Query;->k(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    const-string v0, "QSB.Suggestions"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got result for wrong query %s != %s"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldem;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_2
    iget-object v0, p1, Ldeo;->bvw:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private abg()V
    .locals 5

    .prologue
    .line 104
    sget-object v2, Ldem;->bvq:[I

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v0, v2, v1

    .line 105
    iget-object v4, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    const/4 v4, 0x1

    iput-boolean v4, v0, Ldeo;->bvx:Z

    .line 104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method

.method private fM(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 145
    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    .line 146
    iget-boolean v2, v0, Ldeo;->bvx:Z

    if-nez v2, :cond_0

    .line 147
    iput-boolean v1, v0, Ldeo;->bvx:Z

    move v0, v1

    .line 150
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILdef;Z)V
    .locals 2

    .prologue
    .line 179
    iget-boolean v0, p0, Ldem;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldem;->bvv:Z

    if-eqz v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    const/4 v0, 0x0

    .line 185
    if-eqz p2, :cond_2

    .line 186
    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    .line 187
    invoke-direct {p0, v0, p2}, Ldem;->a(Ldeo;Ldef;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 190
    :cond_2
    if-eqz p3, :cond_3

    .line 191
    invoke-direct {p0, p1}, Ldem;->fM(I)Z

    move-result v1

    or-int/2addr v0, v1

    .line 194
    :cond_3
    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->notifyChanged()V

    goto :goto_0
.end method

.method public final a(ILjava/util/List;Z)V
    .locals 4

    .prologue
    .line 204
    iget-boolean v0, p0, Ldem;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldem;->bvv:Z

    if-eqz v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    const/4 v1, 0x0

    .line 210
    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 211
    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    .line 212
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldef;

    .line 213
    invoke-direct {p0, v0, v1}, Ldem;->a(Ldeo;Ldef;)Z

    move-result v1

    or-int/2addr v1, v2

    move v2, v1

    .line 214
    goto :goto_1

    :cond_2
    move v0, v2

    .line 217
    :goto_2
    if-eqz p3, :cond_3

    .line 218
    invoke-direct {p0, p1}, Ldem;->fM(I)Z

    move-result v1

    or-int/2addr v0, v1

    .line 221
    :cond_3
    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->notifyChanged()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final abf()Ldfd;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Ldem;->bvs:Ldfd;

    return-object v0
.end method

.method public final abh()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ldem;->abg()V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldem;->bvv:Z

    .line 118
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->notifyChanged()V

    .line 119
    return-void
.end method

.method public final abi()Ldem;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 294
    iget-boolean v0, p0, Ldem;->mClosed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldem;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 295
    new-instance v3, Ldem;

    iget-object v0, p0, Ldem;->mQuery:Lcom/google/android/shared/search/Query;

    iget-wide v4, p0, Ldem;->aZG:J

    iget-object v2, p0, Ldem;->bvs:Ldfd;

    invoke-direct {v3, v0, v4, v5, v2}, Ldem;-><init>(Lcom/google/android/shared/search/Query;JLdfd;)V

    .line 297
    sget-object v4, Ldem;->bvq:[I

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget v6, v4, v2

    .line 298
    new-instance v7, Ldeo;

    invoke-direct {v7, v1}, Ldeo;-><init>(B)V

    .line 299
    iget-object v8, v7, Ldeo;->bvw:Ljava/util/List;

    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iget-object v0, v0, Ldeo;->bvw:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 300
    iget-object v0, v3, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 297
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    .line 294
    goto :goto_0

    .line 302
    :cond_1
    invoke-direct {v3}, Ldem;->abg()V

    .line 303
    return-object v3
.end method

.method public final abj()Lcom/google/android/shared/search/Query;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Ldem;->mQuery:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final abk()Z
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldem;->fK(I)Z

    move-result v0

    return v0
.end method

.method public final abl()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 332
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldem;->fK(I)Z

    move-result v0

    return v0
.end method

.method public final abm()Ldef;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 336
    invoke-virtual {p0, v2}, Ldem;->fN(I)Ljava/util/List;

    move-result-object v0

    .line 337
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    goto :goto_0
.end method

.method public final abn()Ljava/util/List;
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldem;->fN(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Ldem;->mClosed:Z

    if-eqz v0, :cond_0

    .line 284
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->unregisterAll()V

    .line 283
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldem;->mClosed:Z

    goto :goto_0
.end method

.method public fK(I)Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iget-boolean v0, v0, Ldeo;->bvx:Z

    return v0
.end method

.method public final fL(I)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0, p1}, Ldem;->fM(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->notifyChanged()V

    .line 142
    :cond_0
    return-void
.end method

.method public final fN(I)Ljava/util/List;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iget-object v0, v0, Ldeo;->bvw:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->avv()I

    move-result v0

    if-lez v0, :cond_0

    .line 314
    const-string v0, "QSB.Suggestions"

    const-string v1, "***LEAK *** : Some observers have not been unregistered !!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_0
    return-void
.end method

.method public final getCreationTime()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Ldem;->aZG:J

    return-wide v0
.end method

.method public final isClosed()Z
    .locals 1

    .prologue
    .line 307
    iget-boolean v0, p0, Ldem;->mClosed:Z

    return v0
.end method

.method public isDone()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 126
    sget-object v3, Ldem;->bvq:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget v0, v3, v2

    .line 127
    iget-object v5, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iget-boolean v0, v0, Ldeo;->bvx:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 131
    :goto_1
    return v0

    .line 126
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 131
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0}, Leqn;->notifyChanged()V

    .line 268
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Ldem;->mClosed:Z

    if-eqz v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0, p1}, Leqn;->registerObserver(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 346
    .line 347
    sget-object v4, Ldem;->bvq:[I

    array-length v5, v4

    move v1, v2

    move v3, v2

    :goto_0
    if-ge v1, v5, :cond_0

    aget v0, v4, v1

    .line 348
    iget-object v6, p0, Ldem;->bvt:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iget-object v0, v0, Ldeo;->bvw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v3, v0

    .line 347
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 350
    :cond_0
    iget-object v0, p0, Ldem;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    .line 351
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 352
    const-string v4, "Suggestions@%x, query=%s, lists=%d\n"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v0, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    const-string v0, "web"

    invoke-direct {p0, v1, v2, v0}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 354
    const-string v0, "summons"

    invoke-direct {p0, v1, v7, v0}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 355
    const-string v0, "promoted-summons"

    invoke-direct {p0, v1, v9, v0}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 356
    const/4 v0, 0x4

    const-string v2, "action-discovery"

    invoke-direct {p0, v1, v0, v2}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 357
    const/4 v0, 0x5

    const-string v2, "browser"

    invoke-direct {p0, v1, v0, v2}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 358
    const/4 v0, 0x6

    const-string v2, "chrome-context"

    invoke-direct {p0, v1, v0, v2}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 359
    const-string v0, "now-promo"

    invoke-direct {p0, v1, v8, v0}, Ldem;->a(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    .line 360
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Ldem;->mClosed:Z

    if-eqz v0, :cond_0

    .line 260
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Ldem;->bvu:Leqn;

    invoke-virtual {v0, p1}, Leqn;->unregisterObserver(Ljava/lang/Object;)V

    goto :goto_0
.end method
