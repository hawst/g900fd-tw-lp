.class public final Ljjw;
.super Ljsl;
.source "PG"


# instance fields
.field private epA:[Ljjv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 410
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 411
    invoke-static {}, Ljjv;->boe()[Ljjv;

    move-result-object v0

    iput-object v0, p0, Ljjw;->epA:[Ljjv;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjw;->eCz:I

    .line 412
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 390
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjw;->epA:[Ljjv;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljjv;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljjw;->epA:[Ljjv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljjv;

    invoke-direct {v3}, Ljjv;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljjw;->epA:[Ljjv;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljjv;

    invoke-direct {v3}, Ljjv;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjw;->epA:[Ljjv;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 424
    iget-object v0, p0, Ljjw;->epA:[Ljjv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljjw;->epA:[Ljjv;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 425
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljjw;->epA:[Ljjv;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 426
    iget-object v1, p0, Ljjw;->epA:[Ljjv;

    aget-object v1, v1, v0

    .line 427
    if-eqz v1, :cond_0

    .line 428
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 425
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 432
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 433
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 437
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 438
    iget-object v0, p0, Ljjw;->epA:[Ljjv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljjw;->epA:[Ljjv;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 439
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljjw;->epA:[Ljjv;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 440
    iget-object v2, p0, Ljjw;->epA:[Ljjv;

    aget-object v2, v2, v0

    .line 441
    if-eqz v2, :cond_0

    .line 442
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 439
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 447
    :cond_1
    return v1
.end method
