.class public Lceb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgmj;


# instance fields
.field private final aTp:Ligi;

.field private final aTq:Lcei;

.field private final aTr:Ljava/lang/String;

.field private aTs:Ljava/lang/String;

.field private mApplicationVersion:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcei;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lceb;->mContext:Landroid/content/Context;

    .line 77
    invoke-virtual {p0}, Lceb;->Dk()Ligi;

    move-result-object v0

    iput-object v0, p0, Lceb;->aTp:Ligi;

    .line 78
    iput-object p2, p0, Lceb;->aTq:Lcei;

    .line 79
    iput-object p3, p0, Lceb;->aTr:Ljava/lang/String;

    .line 80
    return-void
.end method

.method private Dm()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lceb;->mApplicationVersion:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 160
    :try_start_0
    iget-object v0, p0, Lceb;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lceb;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lceb;->mApplicationVersion:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :cond_0
    :goto_0
    iget-object v0, p0, Lceb;->mApplicationVersion:Ljava/lang/String;

    return-object v0

    .line 163
    :catch_0
    move-exception v0

    const-string v0, "AudioS3RequestProducerFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not get application version for"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lceb;->xU()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private xU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lceb;->aTs:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lceb;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lceb;->aTs:Ljava/lang/String;

    .line 153
    :cond_0
    iget-object v0, p0, Lceb;->aTs:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected Dk()Ligi;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcec;

    invoke-direct {v0, p0}, Lcec;-><init>(Lceb;)V

    return-object v0
.end method

.method public final Dl()Lgmk;
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 94
    new-instance v0, Lceh;

    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v1

    new-instance v2, Ljwu;

    invoke-direct {v2}, Ljwu;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljwu;->zz(Ljava/lang/String;)Ljwu;

    move-result-object v2

    const-string v3, "Android"

    invoke-virtual {v2, v3}, Ljwu;->zB(Ljava/lang/String;)Ljwu;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljwu;->zC(Ljava/lang/String;)Ljwu;

    move-result-object v2

    invoke-direct {p0}, Lceb;->xU()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljwu;->zD(Ljava/lang/String;)Ljwu;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljwu;->zF(Ljava/lang/String;)Ljwu;

    move-result-object v4

    invoke-direct {p0}, Lceb;->Dm()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v4, v2}, Ljwu;->zE(Ljava/lang/String;)Ljwu;

    :cond_0
    iget-object v2, p0, Lceb;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v4, v3}, Ljwu;->sN(I)Ljwu;

    move-result-object v3

    iget v5, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v3, v5}, Ljwu;->sO(I)Ljwu;

    move-result-object v3

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v3, v2}, Ljwu;->sP(I)Ljwu;

    :cond_1
    invoke-virtual {v1, v4}, Livy;->bB(Ljava/lang/Object;)Z

    new-instance v2, Ljwe;

    invoke-direct {v2}, Ljwe;-><init>()V

    invoke-virtual {v2, v6}, Ljwe;->sF(I)Ljwe;

    move-result-object v2

    const/high16 v3, 0x45fa0000    # 8000.0f

    invoke-virtual {v2, v3}, Ljwe;->ac(F)Ljwe;

    move-result-object v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lceb;->aTr:Ljava/lang/String;

    iget-object v5, p0, Lceb;->aTq:Lcei;

    invoke-direct/range {v0 .. v5}, Lceh;-><init>(Ljava/util/concurrent/Future;Ljwe;Ljava/lang/String;Ljava/lang/String;Lcei;)V

    .line 96
    new-instance v2, Lgmh;

    iget-object v1, p0, Lceb;->aTp:Ligi;

    invoke-interface {v1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/InputStream;

    invoke-static {v6}, Lgfd;->kh(I)I

    move-result v3

    invoke-direct {v2, v1, v6, v3}, Lgmh;-><init>(Ljava/io/InputStream;II)V

    .line 100
    new-instance v1, Lgmk;

    const/4 v3, 0x2

    new-array v3, v3, [Lceq;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v2, v3, v0

    invoke-direct {v1, v3}, Lgmk;-><init>([Lceq;)V

    return-object v1

    .line 94
    :cond_2
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    move-object v2, v3

    goto :goto_0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public final refresh()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method
