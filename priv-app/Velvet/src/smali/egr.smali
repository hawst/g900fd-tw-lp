.class public final Legr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bYH:Ljava/util/WeakHashMap;

.field private static bYI:J

.field private static bYJ:J

.field private static bYK:J

.field private static bYL:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Legr;->bYH:Ljava/util/WeakHashMap;

    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;IZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-static {}, Lenu;->auR()V

    .line 74
    if-eqz p3, :cond_2

    .line 75
    invoke-static {p0}, Legr;->d(Landroid/app/Activity;)J

    move-result-wide v0

    .line 79
    :goto_0
    const/16 v3, 0xf8

    invoke-static {v3}, Lege;->hs(I)Litu;

    move-result-object v3

    invoke-virtual {v3, p2}, Litu;->mL(I)Litu;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Litu;->bW(J)Litu;

    move-result-object v1

    .line 84
    if-eqz p1, :cond_0

    const-string v0, "com.google.android.googlequicksearchbox.extra.request_elapsed_realtime_millis"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move-object v0, v2

    .line 85
    :goto_1
    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Litu;->bX(J)Litu;

    .line 89
    :cond_1
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 90
    return-void

    .line 77
    :cond_2
    invoke-static {p0}, Legr;->c(Landroid/app/Activity;)J

    move-result-wide v0

    goto :goto_0

    .line 84
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.google.android.googlequicksearchbox.extra.request_elapsed_realtime_millis"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Ljava/lang/Long;

    if-nez v3, :cond_4

    const-string v3, "VelvetLifecycleTracker"

    const-string v4, "Request elapsed time set incorrectly in Intent - found %s, expected Long"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x5

    invoke-static {v0, v3, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_1

    :cond_4
    check-cast v0, Ljava/lang/Long;

    goto :goto_1
.end method

.method public static aoD()V
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lenu;->auR()V

    .line 42
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Legr;->bYI:J

    .line 43
    const/16 v0, 0xf5

    invoke-static {v0}, Lege;->ht(I)V

    .line 45
    return-void
.end method

.method public static aoE()J
    .locals 4

    .prologue
    .line 126
    sget-wide v0, Legr;->bYI:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 127
    const-string v0, "VelvetLifecycleTracker"

    const-string v1, "Application not created yet.. what? "

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 128
    const-wide/16 v0, -0x1

    .line 130
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Legr;->bYI:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static aoF()J
    .locals 4

    .prologue
    .line 138
    sget-wide v0, Legr;->bYK:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 139
    const-wide/16 v0, -0x1

    .line 141
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Legr;->bYK:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static aoG()J
    .locals 4

    .prologue
    .line 149
    sget-wide v0, Legr;->bYL:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 150
    const-wide/16 v0, -0x1

    .line 152
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Legr;->bYL:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;)J
    .locals 2

    .prologue
    .line 160
    invoke-static {}, Lenu;->auR()V

    .line 161
    invoke-static {p0}, Legr;->c(Landroid/app/Activity;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    .line 48
    invoke-static {}, Lenu;->auR()V

    .line 49
    invoke-static {p0}, Legr;->d(Landroid/app/Activity;)J

    move-result-wide v0

    sput-wide v0, Legr;->bYJ:J

    .line 50
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Legr;->bYK:J

    .line 51
    const-wide/16 v0, 0x0

    sput-wide v0, Legr;->bYL:J

    .line 52
    const/16 v0, 0xf6

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v0, p1}, Litu;->mL(I)Litu;

    move-result-object v0

    sget-wide v2, Legr;->bYJ:J

    invoke-virtual {v0, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 56
    return-void
.end method

.method private static c(Landroid/app/Activity;)J
    .locals 3

    .prologue
    .line 165
    sget-object v0, Legr;->bYH:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 166
    if-nez v0, :cond_0

    .line 167
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No latencyId present for Activity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    .line 59
    invoke-static {}, Lenu;->auR()V

    .line 60
    invoke-static {p0}, Legr;->c(Landroid/app/Activity;)J

    move-result-wide v0

    .line 61
    sget-wide v2, Legr;->bYJ:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 62
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sput-wide v2, Legr;->bYL:J

    .line 64
    :cond_0
    const/16 v2, 0xf7

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    invoke-virtual {v2, p1}, Litu;->mL(I)Litu;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 68
    return-void
.end method

.method private static d(Landroid/app/Activity;)J
    .locals 4

    .prologue
    .line 173
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v0

    .line 174
    sget-object v2, Legr;->bYH:Ljava/util/WeakHashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    return-wide v0
.end method

.method public static d(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    .line 96
    invoke-static {}, Lenu;->auR()V

    .line 97
    invoke-static {p0}, Legr;->c(Landroid/app/Activity;)J

    move-result-wide v0

    .line 98
    sget-wide v2, Legr;->bYJ:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 99
    const-wide/16 v2, 0x0

    sput-wide v2, Legr;->bYL:J

    .line 101
    :cond_0
    const/16 v2, 0xf9

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    invoke-virtual {v2, p1}, Litu;->mL(I)Litu;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 105
    return-void
.end method

.method public static e(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    .line 111
    invoke-static {}, Lenu;->auR()V

    .line 112
    invoke-static {p0}, Legr;->c(Landroid/app/Activity;)J

    move-result-wide v0

    .line 113
    sget-wide v2, Legr;->bYJ:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 114
    const-wide/16 v2, 0x0

    sput-wide v2, Legr;->bYK:J

    .line 116
    :cond_0
    const/16 v2, 0xfa

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    invoke-virtual {v2, p1}, Litu;->mL(I)Litu;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 120
    return-void
.end method
