.class public final Ljaf;
.super Ljsl;
.source "PG"


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private air:Ljava/lang/String;

.field public dMF:[Liyg;

.field private dWb:Ljava/lang/String;

.field private dWc:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42969
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 42970
    iput v1, p0, Ljaf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljaf;->air:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljaf;->dWb:Ljava/lang/String;

    iput-object v2, p0, Ljaf;->aeB:Ljbp;

    invoke-static {}, Liyg;->bbv()[Liyg;

    move-result-object v0

    iput-object v0, p0, Ljaf;->dMF:[Liyg;

    iput-boolean v1, p0, Ljaf;->dWc:Z

    iput-object v2, p0, Ljaf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljaf;->eCz:I

    .line 42971
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 42881
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljaf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaf;->air:Ljava/lang/String;

    iget v0, p0, Ljaf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljaf;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljaf;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljaf;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljaf;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljaf;->dMF:[Liyg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liyg;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljaf;->dMF:[Liyg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljaf;->dMF:[Liyg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljaf;->dMF:[Liyg;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljaf;->dWc:Z

    iget v0, p0, Ljaf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljaf;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaf;->dWb:Ljava/lang/String;

    iget v0, p0, Ljaf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljaf;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 42988
    iget v0, p0, Ljaf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 42989
    const/4 v0, 0x1

    iget-object v1, p0, Ljaf;->air:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 42991
    :cond_0
    iget-object v0, p0, Ljaf;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 42992
    const/4 v0, 0x2

    iget-object v1, p0, Ljaf;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 42994
    :cond_1
    iget-object v0, p0, Ljaf;->dMF:[Liyg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljaf;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 42995
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljaf;->dMF:[Liyg;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 42996
    iget-object v1, p0, Ljaf;->dMF:[Liyg;

    aget-object v1, v1, v0

    .line 42997
    if-eqz v1, :cond_2

    .line 42998
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 42995
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43002
    :cond_3
    iget v0, p0, Ljaf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 43003
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljaf;->dWc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 43005
    :cond_4
    iget v0, p0, Ljaf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 43006
    const/4 v0, 0x5

    iget-object v1, p0, Ljaf;->dWb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 43008
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 43009
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 43013
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 43014
    iget v1, p0, Ljaf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 43015
    const/4 v1, 0x1

    iget-object v2, p0, Ljaf;->air:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43018
    :cond_0
    iget-object v1, p0, Ljaf;->aeB:Ljbp;

    if-eqz v1, :cond_1

    .line 43019
    const/4 v1, 0x2

    iget-object v2, p0, Ljaf;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43022
    :cond_1
    iget-object v1, p0, Ljaf;->dMF:[Liyg;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljaf;->dMF:[Liyg;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 43023
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljaf;->dMF:[Liyg;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 43024
    iget-object v2, p0, Ljaf;->dMF:[Liyg;

    aget-object v2, v2, v0

    .line 43025
    if-eqz v2, :cond_2

    .line 43026
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 43023
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 43031
    :cond_4
    iget v1, p0, Ljaf;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 43032
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljaf;->dWc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 43035
    :cond_5
    iget v1, p0, Ljaf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 43036
    const/4 v1, 0x5

    iget-object v2, p0, Ljaf;->dWb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43039
    :cond_6
    return v0
.end method

.method public final pG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42903
    iget-object v0, p0, Ljaf;->air:Ljava/lang/String;

    return-object v0
.end method

.method public final rV(Ljava/lang/String;)Ljaf;
    .locals 1

    .prologue
    .line 42906
    if-nez p1, :cond_0

    .line 42907
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42909
    :cond_0
    iput-object p1, p0, Ljaf;->air:Ljava/lang/String;

    .line 42910
    iget v0, p0, Ljaf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljaf;->aez:I

    .line 42911
    return-object p0
.end method
