.class public Lcmw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mCache:Lcmt;

.field private final mClock:Lemp;

.field private final mCookies:Lgpf;

.field private final mGsaConfig:Lchk;

.field private final mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mHttpHelper:Ldkx;

.field private final mLoginHelper:Lcrh;

.field private final mSdchManager:Lczz;

.field private final mStaticContentCache:Lcpq;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lchk;Lemp;Lcrh;Lcpn;Ldkx;Lcmt;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lczz;Lgpf;Lcpq;)V
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lcmw;->mGsaConfig:Lchk;

    .line 200
    iput-object p2, p0, Lcmw;->mClock:Lemp;

    .line 201
    iput-object p3, p0, Lcmw;->mLoginHelper:Lcrh;

    .line 202
    iput-object p4, p0, Lcmw;->mUrlHelper:Lcpn;

    .line 203
    iput-object p5, p0, Lcmw;->mHttpHelper:Ldkx;

    .line 204
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmt;

    iput-object v0, p0, Lcmw;->mCache:Lcmt;

    .line 205
    iput-object p7, p0, Lcmw;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 206
    iput-object p8, p0, Lcmw;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 207
    iput-object p9, p0, Lcmw;->mSdchManager:Lczz;

    .line 208
    iput-object p10, p0, Lcmw;->mCookies:Lgpf;

    .line 209
    iput-object p11, p0, Lcmw;->mStaticContentCache:Lcpq;

    .line 210
    return-void
.end method

.method private a(Lcom/google/android/shared/search/Query;ZLegl;)Lcmq;
    .locals 14
    .param p3    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 243
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 246
    iget-object v0, p0, Lcmw;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v12

    .line 247
    iget-object v0, p0, Lcmw;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v2

    .line 248
    new-instance v0, Lcmh;

    iget-object v4, p0, Lcmw;->mGsaConfig:Lchk;

    iget-object v5, p0, Lcmw;->mHttpHelper:Ldkx;

    iget-object v6, p0, Lcmw;->mUrlHelper:Lcpn;

    iget-object v7, p0, Lcmw;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v8, p0, Lcmw;->mSdchManager:Lczz;

    iget-object v10, p0, Lcmw;->mCookies:Lgpf;

    iget-object v11, p0, Lcmw;->mStaticContentCache:Lcpq;

    move-object v1, p1

    move/from16 v3, p2

    move-object/from16 v9, p3

    invoke-direct/range {v0 .. v11}, Lcmh;-><init>(Lcom/google/android/shared/search/Query;Ljava/lang/String;ZLchk;Ldkx;Lcpn;Ljava/util/concurrent/ExecutorService;Lczz;Legl;Lgpf;Lcpq;)V

    .line 251
    if-eqz p2, :cond_0

    .line 252
    iget-object v5, p0, Lcmw;->mUiExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcmw;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->HS()I

    move-result v6

    move-object v1, p1

    move-wide v2, v12

    move-object v4, v0

    invoke-static/range {v1 .. v6}, Lcmq;->b(Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)Lcmq;

    move-result-object v0

    .line 255
    :goto_0
    return-object v0

    :cond_0
    iget-object v5, p0, Lcmw;->mUiExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcmw;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->HS()I

    move-result v6

    move-object v1, p1

    move-wide v2, v12

    move-object v4, v0

    invoke-static/range {v1 .. v6}, Lcmq;->a(Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)Lcmq;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcmw;)Lemp;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcmw;->mClock:Lemp;

    return-object v0
.end method

.method private a(Lcmq;Z)V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 267
    iget-object v0, p0, Lcmw;->mCache:Lcmt;

    invoke-virtual {v0, p1, v2, v3}, Lcmt;->a(Lcmq;J)J

    move-result-wide v0

    .line 269
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 273
    new-instance v2, Lcmx;

    const-string v3, "Throttled prefetch"

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-direct {v2, p0, v3, v4}, Lcmx;-><init>(Lcmw;Ljava/lang/String;[I)V

    .line 283
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    .line 284
    iget-object v0, p0, Lcmw;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Throttling prefetch: waiting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-wide/16 v4, 0x3e8

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 288
    const-string v3, "Velvet.SearchResultFetcher"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Large delay ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms). Is this an error?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_2
    iget-object v3, p0, Lcmw;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v2, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    .line 273
    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method static synthetic b(Lcmw;)Lcmt;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcmw;->mCache:Lcmt;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Query;Legl;)Lcmq;
    .locals 2
    .param p2    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 236
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcmw;->a(Lcom/google/android/shared/search/Query;ZLegl;)Lcmq;

    move-result-object v0

    .line 237
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcmw;->a(Lcmq;Z)V

    .line 238
    return-object v0
.end method

.method public final g(Lcom/google/android/shared/search/Query;)Lcmq;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 303
    iget-object v2, p0, Lcmw;->mCache:Lcmt;

    iget-object v3, p0, Lcmw;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v2, p1, v4, v5, v0}, Lcmt;->a(Lcom/google/android/shared/search/Query;JZ)Lcmq;

    move-result-object v2

    .line 306
    if-nez v2, :cond_2

    .line 309
    new-instance v2, Legl;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v4

    sget-object v3, Leoi;->cgG:Leoi;

    invoke-virtual {v3}, Leoi;->auU()J

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Legl;-><init>(JJ)V

    .line 315
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqx()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 330
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 315
    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Legm;->kR(Ljava/lang/String;)Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    const/4 v0, 0x6

    invoke-static {v0}, Legm;->hu(I)Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    const/16 v0, 0x13

    invoke-static {v0}, Lege;->ht(I)V

    invoke-direct {p0, p1, v1, v2}, Lcmw;->a(Lcom/google/android/shared/search/Query;ZLegl;)Lcmq;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcmw;->a(Lcmq;Z)V

    goto :goto_1

    .line 322
    :cond_2
    const/16 v0, 0xdc

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v2}, Lcmq;->Ql()Lcnb;

    move-result-object v1

    iget-wide v4, v1, Lcnb;->amT:J

    invoke-static {v4, v5}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pN(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v2}, Lcmq;->Ql()Lcnb;

    move-result-object v1

    iget-wide v4, v1, Lcnb;->bcK:J

    invoke-static {v4, v5}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pM(Ljava/lang/String;)Litu;

    move-result-object v0

    .line 327
    invoke-static {v0}, Lege;->a(Litu;)V

    move-object v0, v2

    goto :goto_1
.end method
