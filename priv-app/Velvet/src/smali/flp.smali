.class public Lflp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfmt;


# instance fields
.field private final ckm:Z

.field private final cul:Lfkx;

.field public final mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

.field private final mRemoteClient:Lfml;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lfml;Z)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lfkx;

    invoke-direct {v0}, Lfkx;-><init>()V

    iput-object v0, p0, Lflp;->cul:Lfkx;

    .line 30
    iput-object p1, p0, Lflp;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 31
    iput-object p2, p0, Lflp;->mRemoteClient:Lfml;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflp;->ckm:Z

    .line 33
    return-void
.end method


# virtual methods
.method public final W(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public a(Landroid/content/Context;Lfzw;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 125
    return-void
.end method

.method public final varargs a(Landroid/view/View;[Lizj;)V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public final a(Lejw;Lizj;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public final a(Lizj;I)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public final a(Lizj;ILixx;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public final a(Lizj;Lizj;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final a(Lizj;Z)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/sidekick/shared/client/NowSearchOptions;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final aAD()Lfml;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lflp;->mRemoteClient:Lfml;

    return-object v0
.end method

.method public final aAE()Landroid/database/Observable;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lflp;->cul:Lfkx;

    return-object v0
.end method

.method public final c(Lizj;Z)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public final dt(I)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public final getVisibility()I
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public final t(Lizj;)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public final u(Lizj;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public final v(Lizj;)V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public final wj()Lekf;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public final wq()Lfnm;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public final wr()Leoj;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public final ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lflp;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    return-object v0
.end method

.method public final wt()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public final wu()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public final wv()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lflp;->ckm:Z

    return v0
.end method
