.class public final Lcdh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdq;


# instance fields
.field private final aSA:Z

.field private aSB:Lcdf;

.field private aSC:Lcdj;

.field private aSD:Lcej;

.field private aSE:Lbzh;

.field private aSF:Ljava/net/URL;

.field private aSG:Lcdl;

.field private aSH:Lcdk;

.field private final aSy:Ljzt;

.field private final aSz:Lenw;

.field private final mConnectionFactory:Lccz;


# direct methods
.method public constructor <init>(Ljzt;Lccz;Z)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lcdh;->aSz:Lenw;

    .line 105
    iput-object p1, p0, Lcdh;->aSy:Ljzt;

    .line 106
    iput-object p2, p0, Lcdh;->mConnectionFactory:Lccz;

    .line 107
    iput-boolean p3, p0, Lcdh;->aSA:Z

    .line 108
    return-void
.end method

.method static a(Lbzh;Ljzm;)V
    .locals 3

    .prologue
    .line 199
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Ljzm;->eNQ:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 200
    iget-object v1, p1, Ljzm;->eNQ:[Ljava/lang/String;

    aget-object v1, v1, v0

    iget-object v2, p1, Ljzm;->eNR:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {p0, v1, v2}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_0
    return-void
.end method

.method private a(Ljava/io/IOException;I)V
    .locals 3

    .prologue
    .line 177
    const-string v0, "PairHttpConnection"

    const-string v1, "[Upload] Connection error"

    invoke-static {v0, v1, p1}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    invoke-virtual {p0}, Lcdh;->close()V

    .line 182
    instance-of v0, p1, Lefq;

    if-eqz v0, :cond_1

    .line 183
    instance-of v0, p1, Left;

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Leif;

    invoke-direct {v0, p2}, Leif;-><init>(I)V

    throw v0

    .line 186
    :cond_0
    instance-of v0, p1, Lefs;

    if-nez v0, :cond_1

    .line 187
    const-string v0, "PairHttpConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception should be Http or Gsa exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 190
    :cond_1
    new-instance v0, Leie;

    invoke-direct {v0, p1, p2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v0
.end method

.method static b(Lbzh;Ljzm;)V
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p1}, Ljzm;->bxg()Z

    move-result v0

    if-nez v0, :cond_0

    .line 358
    const-string v0, "X-S3-Send-Compressible"

    const-string v1, "1"

    invoke-interface {p0, v0, v1}, Lbzh;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcdf;Ljwv;)V
    .locals 7

    .prologue
    .line 114
    iget-object v0, p0, Lcdh;->aSz:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 116
    sget-object v0, Lcdk;->aSN:Lcdk;

    invoke-virtual {p0, v0}, Lcdh;->a(Lcdk;)Z

    .line 117
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdf;

    iput-object v0, p0, Lcdh;->aSB:Lcdf;

    .line 118
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    .line 121
    :try_start_0
    new-instance v0, Lcdj;

    new-instance v2, Ljava/net/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcdh;->aSy:Ljzt;

    iget-object v3, v3, Ljzt;->eOr:Ljzm;

    invoke-virtual {v3}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcdh;->aSy:Ljzt;

    iget-object v4, p0, Lcdh;->mConnectionFactory:Lccz;

    iget-object v5, p0, Lcdh;->aSB:Lcdf;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcdj;-><init>(Lcdh;Ljava/net/URL;Ljzt;Lccz;Lcdf;)V

    iput-object v0, p0, Lcdh;->aSC:Lcdj;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    iget-object v0, p0, Lcdh;->aSC:Lcdj;

    invoke-virtual {v0}, Lcdj;->start()V

    .line 131
    iget-object v0, p0, Lcdh;->aSy:Ljzt;

    iget-object v1, v0, Ljzt;->eOs:Ljzm;

    .line 133
    :try_start_1
    new-instance v0, Ljava/net/URL;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcdh;->aSF:Ljava/net/URL;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcdh;->mConnectionFactory:Lccz;

    iget-object v2, p0, Lcdh;->aSF:Ljava/net/URL;

    invoke-interface {v0, v2}, Lccz;->a(Ljava/net/URL;)Lbzh;

    move-result-object v0

    iput-object v0, p0, Lcdh;->aSE:Lbzh;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 143
    :goto_2
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    invoke-static {v0, v1}, Lcdh;->a(Lbzh;Ljzm;)V

    .line 144
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    invoke-static {v0, v1}, Lcdh;->b(Lbzh;Ljzm;)V

    .line 145
    iget-object v2, p0, Lcdh;->aSE:Lbzh;

    invoke-virtual {v1}, Ljzm;->bxf()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljzm;->bxe()I

    move-result v0

    :goto_3
    invoke-interface {v2, v0}, Lbzh;->es(I)V

    .line 147
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lbzh;->setUseCaches(Z)V

    .line 150
    :try_start_3
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    invoke-static {v0}, Ldmc;->d(Lbzh;)V
    :try_end_3
    .catch Lefq; {:try_start_3 .. :try_end_3} :catch_3

    .line 158
    :goto_4
    new-instance v0, Lcej;

    iget-object v2, p0, Lcdh;->aSE:Lbzh;

    invoke-virtual {v1}, Ljzm;->rA()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcej;-><init>(Lbzh;Ljava/lang/String;)V

    iput-object v0, p0, Lcdh;->aSD:Lcej;

    .line 161
    :try_start_4
    iget-object v0, p0, Lcdh;->aSD:Lcej;

    invoke-virtual {v0, p2}, Lcej;->c(Ljwv;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 167
    :goto_5
    iget-object v0, p0, Lcdh;->aSC:Lcdj;

    invoke-virtual {v0}, Lcdj;->De()Z

    move-result v0

    if-nez v0, :cond_1

    .line 171
    new-instance v0, Leie;

    const v1, 0x10007

    invoke-direct {v0, v1}, Leie;-><init>(I)V

    throw v0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    const v1, 0x10001

    invoke-direct {p0, v0, v1}, Lcdh;->a(Ljava/io/IOException;I)V

    goto :goto_0

    .line 134
    :catch_1
    move-exception v0

    .line 135
    const v2, 0x10002

    invoke-direct {p0, v0, v2}, Lcdh;->a(Ljava/io/IOException;I)V

    goto :goto_1

    .line 140
    :catch_2
    move-exception v0

    .line 141
    const v2, 0x10003

    invoke-direct {p0, v0, v2}, Lcdh;->a(Ljava/io/IOException;I)V

    goto :goto_2

    .line 145
    :cond_0
    const/16 v0, 0x400

    goto :goto_3

    .line 151
    :catch_3
    move-exception v0

    .line 153
    invoke-virtual {v0}, Lefq;->getErrorCode()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcdh;->a(Ljava/io/IOException;I)V

    goto :goto_4

    .line 162
    :catch_4
    move-exception v0

    .line 163
    const v1, 0x10006

    invoke-direct {p0, v0, v1}, Lcdh;->a(Ljava/io/IOException;I)V

    goto :goto_5

    .line 173
    :cond_1
    return-void
.end method

.method public final a(Ljwv;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 220
    iget-object v0, p0, Lcdh;->aSz:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 221
    iget-object v0, p0, Lcdh;->aSD:Lcej;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v4, "call to send() after close() / error / end of data"

    invoke-static {v0, v4}, Lifv;->d(ZLjava/lang/Object;)V

    .line 225
    :try_start_0
    iget-object v0, p0, Lcdh;->aSD:Lcej;

    const/4 v4, 0x1

    invoke-virtual {p1}, Ljwv;->bvB()Z

    move-result v5

    invoke-virtual {v0, p1, v4, v5}, Lcej;->a(Ljwv;ZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    invoke-virtual {p1}, Ljwv;->bvB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iput-object v3, p0, Lcdh;->aSD:Lcej;

    :try_start_1
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    const-string v4, "Upload"

    const v5, 0x10009

    invoke-static {v0, v4, v5}, Lcds;->a(Lbzh;Ljava/lang/String;I)I

    move-result v0

    iget-object v4, p0, Lcdh;->aSF:Ljava/net/URL;

    iget-object v5, p0, Lcdh;->aSE:Lbzh;

    const-string v6, "Upload"

    invoke-static {v0, v4, v5, v6}, Lcds;->a(ILjava/net/URL;Lbzh;Ljava/lang/String;)V
    :try_end_1
    .catch Leie; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    invoke-static {v0}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    :try_start_3
    new-instance v0, Lcem;

    invoke-direct {v0, v4}, Lcem;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :try_start_4
    invoke-virtual {v0}, Lcem;->Dq()Ljww;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    :goto_2
    if-eqz v1, :cond_2

    sget-object v1, Lcdk;->aSO:Lcdk;

    invoke-virtual {p0, v1}, Lcdh;->a(Lcdk;)Z

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcdl;

    iget-object v4, p0, Lcdh;->aSE:Lbzh;

    iget-object v5, p0, Lcdh;->aSB:Lcdf;

    invoke-direct {v1, v2, v4, v5, v0}, Lcdl;-><init>(Lcem;Lbzh;Lcdf;Ljww;)V

    iput-object v1, p0, Lcdh;->aSG:Lcdl;

    iput-object v3, p0, Lcdh;->aSE:Lbzh;

    iget-object v0, p0, Lcdh;->aSG:Lcdl;

    invoke-virtual {v0}, Lcdl;->start()V

    .line 234
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 221
    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    iget-object v1, p0, Lcdh;->aSB:Lcdf;

    new-instance v2, Leie;

    const v3, 0x1000a

    invoke-direct {v2, v0, v3}, Leie;-><init>(Ljava/lang/Throwable;I)V

    invoke-interface {v1, v2}, Lcdf;->c(Leie;)V

    goto :goto_3

    .line 232
    :catch_1
    move-exception v0

    iget-object v4, p0, Lcdh;->aSB:Lcdf;

    invoke-interface {v4, v0}, Lcdf;->c(Leie;)V

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v0, v3

    move-object v1, v3

    :goto_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    move v1, v2

    move-object v2, v0

    move-object v0, v3

    goto :goto_2

    :cond_2
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v0, v3

    move-object v1, v4

    goto :goto_4

    :catch_4
    move-exception v1

    move-object v1, v4

    goto :goto_4
.end method

.method public final declared-synchronized a(Lcdk;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 304
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcdi;->aSI:[I

    invoke-virtual {p1}, Lcdk;->ordinal()I

    move-result v3

    aget v2, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 351
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 306
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcdh;->aSH:Lcdk;

    sget-object v3, Lcdk;->aSP:Lcdk;

    if-ne v2, v3, :cond_1

    .line 307
    const-string v1, "PairHttpConnection"

    const-string v2, "The response is sent in the up and down"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 311
    :cond_1
    :try_start_2
    sget-object v0, Lcdk;->aSO:Lcdk;

    iput-object v0, p0, Lcdh;->aSH:Lcdk;

    move v0, v1

    .line 312
    goto :goto_0

    .line 317
    :pswitch_1
    iget-object v2, p0, Lcdh;->aSH:Lcdk;

    sget-object v3, Lcdk;->aSQ:Lcdk;

    if-eq v2, v3, :cond_2

    move v2, v1

    :goto_1
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 318
    iget-object v2, p0, Lcdh;->aSH:Lcdk;

    sget-object v3, Lcdk;->aSO:Lcdk;

    if-ne v2, v3, :cond_3

    .line 319
    const-string v1, "PairHttpConnection"

    const-string v2, "The response is sent in the up and down"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 317
    goto :goto_1

    .line 323
    :cond_3
    sget-object v0, Lcdk;->aSP:Lcdk;

    iput-object v0, p0, Lcdh;->aSH:Lcdk;

    move v0, v1

    .line 324
    goto :goto_0

    .line 328
    :pswitch_2
    iget-boolean v2, p0, Lcdh;->aSA:Z

    if-eqz v2, :cond_0

    .line 329
    iget-object v2, p0, Lcdh;->aSH:Lcdk;

    sget-object v3, Lcdk;->aSP:Lcdk;

    if-ne v2, v3, :cond_4

    .line 330
    sget-object v1, Lcdk;->aSQ:Lcdk;

    iput-object v1, p0, Lcdh;->aSH:Lcdk;

    goto :goto_0

    .line 332
    :cond_4
    iget-object v0, p0, Lcdh;->aSH:Lcdk;

    sget-object v2, Lcdk;->aSO:Lcdk;

    if-ne v0, v2, :cond_5

    move v0, v1

    .line 335
    goto :goto_0

    .line 337
    :cond_5
    sget-object v0, Lcdk;->aSQ:Lcdk;

    iput-object v0, p0, Lcdh;->aSH:Lcdk;

    move v0, v1

    .line 338
    goto :goto_0

    .line 347
    :pswitch_3
    sget-object v0, Lcdk;->aSN:Lcdk;

    iput-object v0, p0, Lcdh;->aSH:Lcdk;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 348
    goto :goto_0

    .line 304
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcdh;->aSz:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 209
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcdh;->aSE:Lbzh;

    invoke-interface {v0}, Lbzh;->disconnect()V

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lcdh;->aSE:Lbzh;

    .line 214
    :cond_0
    iget-object v0, p0, Lcdh;->aSG:Lcdl;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 215
    iget-object v0, p0, Lcdh;->aSC:Lcdj;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 216
    return-void
.end method
