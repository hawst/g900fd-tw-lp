.class public final Ldze;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# static fields
.field private static final AM:Landroid/graphics/Paint;

.field private static bRA:I

.field private static bRB:I

.field private static bRC:F

.field private static bRD:Landroid/graphics/Bitmap;

.field private static final bRw:Landroid/graphics/Rect;

.field private static final bRx:[C

.field private static bRy:Landroid/content/res/TypedArray;

.field private static bRz:I


# instance fields
.field private bQZ:I

.field private bRE:Ljava/lang/String;

.field private bRF:Z

.field private bRG:I

.field private bjf:Ljava/lang/String;

.field private final mResources:Landroid/content/res/Resources;

.field private final ob:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Ldze;->AM:Landroid/graphics/Paint;

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Ldze;->bRw:Landroid/graphics/Rect;

    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [C

    sput-object v0, Ldze;->bRx:[C

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v1, v0}, Ldze;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 85
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 66
    iput-boolean v3, p0, Ldze;->bRF:Z

    .line 86
    iput-object p1, p0, Ldze;->mResources:Landroid/content/res/Resources;

    .line 88
    iput-object p2, p0, Ldze;->bjf:Ljava/lang/String;

    .line 89
    iput-object p3, p0, Ldze;->bRE:Ljava/lang/String;

    .line 90
    iput-boolean p4, p0, Ldze;->bRF:Z

    .line 91
    invoke-direct {p0, p2}, Ldze;->kz(Ljava/lang/String;)V

    .line 93
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldze;->ob:Landroid/graphics/Paint;

    .line 94
    iget-object v0, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 95
    iget-object v0, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 97
    sget-object v0, Ldze;->bRy:Landroid/content/res/TypedArray;

    if-nez v0, :cond_0

    .line 98
    const v0, 0x7f0f0013

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    sput-object v0, Ldze;->bRy:Landroid/content/res/TypedArray;

    .line 99
    const v0, 0x7f0c002c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Ldze;->bRz:I

    .line 100
    const v0, 0x7f0b009c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ldze;->bRA:I

    .line 101
    const v0, 0x7f0b009d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ldze;->bRB:I

    .line 102
    const v0, 0x7f0d00da

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    sput v0, Ldze;->bRC:F

    .line 103
    sget-object v0, Ldze;->AM:Landroid/graphics/Paint;

    const-string v1, "sans-serif-thin"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 104
    sget-object v0, Ldze;->AM:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 105
    sget-object v0, Ldze;->AM:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 108
    :cond_0
    iget-object v0, p0, Ldze;->bRE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v0, Ldze;->bRA:I

    :goto_0
    iput v0, p0, Ldze;->bQZ:I

    .line 109
    sget v0, Ldze;->bRB:I

    iput v0, p0, Ldze;->bRG:I

    .line 110
    return-void

    .line 108
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Ldze;->bRz:I

    rem-int/2addr v0, v1

    sget-object v1, Ldze;->bRy:Landroid/content/res/TypedArray;

    sget v2, Ldze;->bRA:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    goto :goto_0
.end method

.method private static ky(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 200
    if-eqz p0, :cond_3

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x41

    if-gt v3, v2, :cond_0

    const/16 v3, 0x5a

    if-le v2, v3, :cond_1

    :cond_0
    const/16 v3, 0x61

    if-gt v3, v2, :cond_2

    const/16 v3, 0x7a

    if-gt v2, v3, :cond_2

    :cond_1
    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    :goto_1
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private kz(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 233
    sget-object v0, Ldze;->bRD:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p1}, Ldze;->ky(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Ldze;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0201b4

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Ldze;->bRD:Landroid/graphics/Bitmap;

    .line 239
    :cond_0
    return-void
.end method


# virtual methods
.method public final aw(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0, p1}, Ldze;->kz(Ljava/lang/String;)V

    .line 228
    iput-object p1, p0, Ldze;->bjf:Ljava/lang/String;

    .line 229
    iput-object p2, p0, Ldze;->bRE:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 114
    invoke-virtual {p0}, Ldze;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 119
    :cond_0
    sget-object v0, Ldze;->AM:Landroid/graphics/Paint;

    iget v1, p0, Ldze;->bQZ:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Ldze;->AM:Landroid/graphics/Paint;

    iget-object v1, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    sget-object v0, Ldze;->AM:Landroid/graphics/Paint;

    iget-object v1, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    invoke-virtual {p0}, Ldze;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-boolean v4, p0, Ldze;->bRF:Z

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    div-int/lit8 v6, v1, 0x2

    int-to-float v6, v6

    sget-object v7, Ldze;->AM:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :goto_1
    iget-object v4, p0, Ldze;->bjf:Ljava/lang/String;

    invoke-static {v4}, Ldze;->ky(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Ldze;->bRx:[C

    iget-object v5, p0, Ldze;->bjf:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v5

    aput-char v5, v4, v2

    sget-object v4, Ldze;->AM:Landroid/graphics/Paint;

    sget v5, Ldze;->bRC:F

    int-to-float v1, v1

    mul-float/2addr v1, v5

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v1, Ldze;->AM:Landroid/graphics/Paint;

    sget-object v4, Ldze;->bRx:[C

    sget-object v5, Ldze;->bRw:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v2, v3, v5}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    sget-object v1, Ldze;->AM:Landroid/graphics/Paint;

    iget v4, p0, Ldze;->bRG:I

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Ldze;->bRx:[C

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    sget-object v5, Ldze;->bRw:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    int-to-float v5, v0

    sget-object v6, Ldze;->AM:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_1
    sget-object v4, Ldze;->AM:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    :cond_2
    sget-object v0, Ldze;->bRD:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Ldze;->copyBounds()Landroid/graphics/Rect;

    move-result-object v4

    const v5, 0x3ecccccd    # 0.4f

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    sub-int/2addr v6, v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    sub-int/2addr v7, v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    add-int/2addr v5, v9

    invoke-virtual {v4, v6, v7, v8, v5}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v5, Ldze;->bRw:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v1, Ldze;->bRw:Landroid/graphics/Rect;

    iget-object v2, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public final getBackgroundColor()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Ldze;->bQZ:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 215
    const/4 v0, -0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 206
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Ldze;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 211
    return-void
.end method
