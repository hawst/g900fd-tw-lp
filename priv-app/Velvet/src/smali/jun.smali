.class public final Ljun;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFw:[Ljun;


# instance fields
.field private aez:I

.field private agv:I

.field private dHr:Ljava/lang/String;

.field private dMy:F

.field private eFx:Ljava/lang/String;

.field private eFy:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 153
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Ljun;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljun;->name:Ljava/lang/String;

    iput v1, p0, Ljun;->agv:I

    const-string v0, ""

    iput-object v0, p0, Ljun;->eFx:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljun;->dMy:F

    const-string v0, ""

    iput-object v0, p0, Ljun;->dHr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljun;->eFy:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljun;->eCq:Ljsn;

    iput v1, p0, Ljun;->eCz:I

    .line 155
    return-void
.end method

.method public static buL()[Ljun;
    .locals 2

    .prologue
    .line 33
    sget-object v0, Ljun;->eFw:[Ljun;

    if-nez v0, :cond_1

    .line 34
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 36
    :try_start_0
    sget-object v0, Ljun;->eFw:[Ljun;

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [Ljun;

    sput-object v0, Ljun;->eFw:[Ljun;

    .line 39
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :cond_1
    sget-object v0, Ljun;->eFw:[Ljun;

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 27
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljun;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljun;->name:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljun;->agv:I

    iget v0, p0, Ljun;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljun;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljun;->eFx:Ljava/lang/String;

    iget v0, p0, Ljun;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljun;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljun;->dMy:F

    iget v0, p0, Ljun;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljun;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljun;->dHr:Ljava/lang/String;

    iget v0, p0, Ljun;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljun;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljun;->eFy:Ljava/lang/String;

    iget v0, p0, Ljun;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljun;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 173
    const/4 v0, 0x1

    iget-object v1, p0, Ljun;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 174
    iget v0, p0, Ljun;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 175
    const/4 v0, 0x2

    iget v1, p0, Ljun;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 177
    :cond_0
    iget v0, p0, Ljun;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 178
    const/4 v0, 0x3

    iget-object v1, p0, Ljun;->eFx:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 180
    :cond_1
    iget v0, p0, Ljun;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 181
    const/4 v0, 0x4

    iget v1, p0, Ljun;->dMy:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 183
    :cond_2
    iget v0, p0, Ljun;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 184
    const/4 v0, 0x5

    iget-object v1, p0, Ljun;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 186
    :cond_3
    iget v0, p0, Ljun;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 187
    const/4 v0, 0x6

    iget-object v1, p0, Ljun;->eFy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 189
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 190
    return-void
.end method

.method public final buM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Ljun;->eFy:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ljun;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 194
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 195
    const/4 v1, 0x1

    iget-object v2, p0, Ljun;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    iget v1, p0, Ljun;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 198
    const/4 v1, 0x2

    iget v2, p0, Ljun;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    :cond_0
    iget v1, p0, Ljun;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 202
    const/4 v1, 0x3

    iget-object v2, p0, Ljun;->eFx:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_1
    iget v1, p0, Ljun;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 206
    const/4 v1, 0x4

    iget v2, p0, Ljun;->dMy:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 209
    :cond_2
    iget v1, p0, Ljun;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 210
    const/4 v1, 0x5

    iget-object v2, p0, Ljun;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    :cond_3
    iget v1, p0, Ljun;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 214
    const/4 v1, 0x6

    iget-object v2, p0, Ljun;->eFy:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    :cond_4
    return v0
.end method

.method public final za(Ljava/lang/String;)Ljun;
    .locals 1

    .prologue
    .line 115
    if-nez p1, :cond_0

    .line 116
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 118
    :cond_0
    iput-object p1, p0, Ljun;->dHr:Ljava/lang/String;

    .line 119
    iget v0, p0, Ljun;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljun;->aez:I

    .line 120
    return-object p0
.end method
