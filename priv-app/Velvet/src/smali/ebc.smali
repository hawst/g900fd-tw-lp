.class public final Lebc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lelk;


# instance fields
.field private Oq:I

.field private bLZ:Ljava/lang/String;

.field private final bSN:Leax;

.field private final bTA:Ljava/util/ArrayList;

.field private final bTB:Ljava/util/ArrayList;

.field private final bTC:Ljava/util/ArrayList;

.field private final bTD:Ljava/util/ArrayList;

.field private final bTE:Ljava/util/ArrayList;

.field private final bTF:Lcom/google/android/shared/search/SuggestionLogInfo;

.field private bTG:Z

.field private bTH:I

.field private bTI:I

.field private bTJ:Z

.field private bTK:Lcom/google/android/shared/search/Suggestion;

.field bTL:Lecd;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bTx:I

.field final bTy:Lebd;

.field private final bTz:Ligi;


# direct methods
.method public constructor <init>(IILebd;Leey;Leeo;Lesm;Lela;Leax;Ligi;)V
    .locals 6

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lebc;->bTA:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lebc;->bTB:Ljava/util/ArrayList;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lebc;->bTC:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lebc;->bTD:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lebc;->bTE:Ljava/util/ArrayList;

    .line 78
    sget-object v0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZT:Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, p0, Lebc;->bTF:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 100
    iput p2, p0, Lebc;->bTx:I

    .line 101
    iput-object p3, p0, Lebc;->bTy:Lebd;

    .line 103
    iput-object p8, p0, Lebc;->bSN:Leax;

    .line 104
    iput-object p9, p0, Lebc;->bTz:Ligi;

    .line 107
    iget-object v0, p0, Lebc;->bTy:Lebd;

    move-object v1, p4

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-interface/range {v0 .. v5}, Lebd;->a(Leey;Leeo;Lesm;Lela;Leax;)V

    .line 108
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;IZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 246
    if-eqz p4, :cond_0

    const/4 v0, 0x2

    :goto_0
    add-int/2addr v0, p3

    .line 247
    iget v2, p0, Lebc;->bTx:I

    if-lt v0, v2, :cond_1

    .line 248
    iget-object v0, p0, Lebc;->bTy:Lebd;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lebd;->eD(Z)V

    .line 249
    iget-object v0, p0, Lebc;->bTy:Lebd;

    add-int/lit8 v1, p3, -0x1

    invoke-interface {v0, p1, p2, v1}, Lebd;->b(Ljava/lang/String;Ljava/util/List;I)V

    .line 254
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 246
    goto :goto_0

    .line 251
    :cond_1
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, v1}, Lebd;->eD(Z)V

    .line 252
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, p1, p2, p3}, Lebd;->b(Ljava/lang/String;Ljava/util/List;I)V

    goto :goto_1
.end method

.method private amF()Z
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebc;->bSN:Leax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebc;->bSN:Leax;

    invoke-interface {v0}, Leax;->aeE()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private eC(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 402
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, p1}, Lebd;->eC(Z)V

    .line 403
    iput v1, p0, Lebc;->bTI:I

    .line 404
    iput v1, p0, Lebc;->bTH:I

    .line 405
    iput-boolean v1, p0, Lebc;->bTJ:Z

    .line 406
    return-void
.end method

.method private static hc(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 409
    if-ne p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static hd(I)Z
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/api/SuggestionsGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 133
    iget-boolean v0, p0, Lebc;->bTG:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 135
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->getType()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 137
    iget-object v0, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 139
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 140
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    iput-object v0, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 143
    iget-object v0, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 144
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 146
    iget-object v0, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 147
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->getType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 149
    iget-object v0, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;ZLcom/google/android/shared/search/SuggestionLogInfo;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 161
    iget-boolean v0, p0, Lebc;->bTG:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 164
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v3

    .line 167
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 169
    iget-object v0, p0, Lebc;->bTD:Ljava/util/ArrayList;

    iget-object v4, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 172
    :cond_0
    iget v0, p0, Lebc;->Oq:I

    if-ne v0, v1, :cond_8

    .line 174
    iget-object v0, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lebc;->bTI:I

    .line 175
    iget-object v0, p0, Lebc;->bTy:Lebd;

    iget-object v4, p0, Lebc;->bTA:Ljava/util/ArrayList;

    iget v5, p0, Lebc;->bTI:I

    invoke-interface {v0, v3, v4, v5}, Lebd;->a(Ljava/lang/String;Ljava/util/List;I)V

    .line 177
    if-nez p2, :cond_3

    iget-object v0, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 178
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0}, Lebd;->amz()V

    .line 185
    :goto_0
    iget-object v0, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lebc;->bTH:I

    .line 186
    iget-object v0, p0, Lebc;->bTB:Ljava/util/ArrayList;

    iget v4, p0, Lebc;->bTH:I

    iget-boolean v5, p0, Lebc;->bTJ:Z

    invoke-direct {p0, v3, v0, v4, v5}, Lebc;->a(Ljava/lang/String;Ljava/util/List;IZ)V

    .line 189
    if-nez p2, :cond_5

    iget-object v0, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    if-nez v0, :cond_5

    .line 190
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0}, Lebd;->amA()V

    .line 195
    :goto_1
    iget-object v0, p0, Lebc;->bTz:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledu;

    .line 196
    if-eqz v0, :cond_1

    .line 199
    new-instance v4, Lijk;

    invoke-direct {v4}, Lijk;-><init>()V

    iget-object v5, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    move-result-object v4

    iget-object v5, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    move-result-object v4

    iget-object v5, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    move-result-object v4

    iget-object v4, v4, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v4}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v4

    .line 204
    invoke-virtual {v0, v4, p3, p2}, Ledu;->a(Ljava/util/List;Lcom/google/android/shared/search/SuggestionLogInfo;Z)Ledu;

    .line 206
    :cond_1
    iget v0, p0, Lebc;->bTI:I

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    iget v4, p0, Lebc;->bTH:I

    if-eqz v4, :cond_7

    :goto_3
    or-int/2addr v0, v1

    iget-boolean v1, p0, Lebc;->bTJ:Z

    or-int/2addr v0, v1

    .line 208
    if-eqz v0, :cond_2

    iget-object v0, p0, Lebc;->bTL:Lecd;

    if-eqz v0, :cond_2

    .line 209
    iget-object v0, p0, Lebc;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->amP()V

    .line 222
    :cond_2
    :goto_4
    iput-object v3, p0, Lebc;->bLZ:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0}, Lebd;->amB()V

    .line 224
    iput-boolean v2, p0, Lebc;->bTG:Z

    .line 225
    return-void

    .line 180
    :cond_3
    iget-object v0, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lebc;->bTJ:Z

    .line 181
    iget-object v0, p0, Lebc;->bTy:Lebd;

    iget-object v4, p0, Lebc;->bTC:Ljava/util/ArrayList;

    iget-object v5, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lebd;->c(Ljava/lang/String;Ljava/util/List;I)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 180
    goto :goto_5

    .line 192
    :cond_5
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-direct {p0}, Lebc;->amF()Z

    move-result v4

    invoke-interface {v0, v4}, Lebd;->eF(Z)V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 206
    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3

    .line 211
    :cond_8
    iget v0, p0, Lebc;->Oq:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 213
    invoke-direct {p0, v2}, Lebc;->eC(Z)V

    .line 216
    iget-object v0, p0, Lebc;->bTE:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 217
    iget-object v0, p0, Lebc;->bTy:Lebd;

    iget-object v1, p0, Lebc;->bTE:Ljava/util/ArrayList;

    iget-object v4, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-interface {v0, v3, v1, v4}, Lebd;->d(Ljava/lang/String;Ljava/util/List;I)V

    goto :goto_4
.end method

.method public final a(Lelj;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, p1}, Lebd;->a(Lelj;)V

    .line 238
    return-void
.end method

.method public final amE()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 118
    iget-boolean v0, p0, Lebc;->bTG:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 121
    iput-boolean v1, p0, Lebc;->bTG:Z

    .line 122
    iget-object v0, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 123
    iget-object v0, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    iget-object v0, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    .line 126
    iget-object v0, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 127
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hb(I)I
    .locals 2

    .prologue
    .line 229
    iget v0, p0, Lebc;->Oq:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 230
    const/4 v0, -0x1

    .line 232
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, p1}, Lebd;->hb(I)I

    move-result v0

    goto :goto_0
.end method

.method public final w(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 264
    iget-boolean v0, p0, Lebc;->bTG:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 267
    const-string v0, "gel_suggestions_controller:mode"

    iget v1, p0, Lebc;->Oq:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 268
    const-string v0, "gel_suggestions_controller:cached_zero_prefix_suggestions"

    iget-object v1, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 273
    iget-object v0, p0, Lebc;->bLZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "gel_suggestions_controller:query_string"

    iget-object v1, p0, Lebc;->bLZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v0, "gel_suggestions_controller:web_suggestions_shown"

    iget v1, p0, Lebc;->bTI:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 277
    const-string v0, "gel_suggestions_controller:summons_shown"

    iget v1, p0, Lebc;->bTH:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 278
    const-string v0, "gel_suggestions_controller:horizontal_summons_shown"

    iget-boolean v1, p0, Lebc;->bTJ:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 279
    const-string v0, "gel_suggestions_controller:now_promo_suggestions"

    iget-object v1, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280
    const-string v0, "gel_suggestions_controller:web_suggestions"

    iget-object v1, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 281
    const-string v0, "gel_suggestions_controller:summons"

    iget-object v1, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 282
    const-string v0, "gel_suggestions_controller:horizontal_summons"

    iget-object v1, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 283
    const-string v0, "gel_suggestions_controller:action_discovery_suggestions"

    iget-object v1, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 286
    :cond_0
    return-void

    .line 264
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 289
    iget-boolean v0, p0, Lebc;->bTG:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 290
    iget-object v0, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 291
    iget-object v0, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 292
    iget-object v0, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 293
    iget-object v0, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 298
    const-string v0, "gel_suggestions_controller:cached_zero_prefix_suggestions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    const-string v0, "gel_suggestions_controller:cached_zero_prefix_suggestions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 300
    iget-object v3, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 302
    :cond_0
    const-string v0, "gel_suggestions_controller:mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const-string v0, "gel_suggestions_controller:mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebc;->Oq:I

    .line 305
    :cond_1
    const-string v0, "gel_suggestions_controller:query_string"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 308
    const-string v0, "gel_suggestions_controller:query_string"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebc;->bLZ:Ljava/lang/String;

    .line 309
    const-string v0, "gel_suggestions_controller:web_suggestions_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebc;->bTI:I

    .line 310
    const-string v0, "gel_suggestions_controller:summons_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebc;->bTH:I

    .line 311
    const-string v0, "gel_suggestions_controller:horizontal_summons_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lebc;->bTJ:Z

    .line 312
    const-string v0, "gel_suggestions_controller:now_promo_suggestions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    iput-object v0, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    .line 314
    const-string v0, "gel_suggestions_controller:web_suggestions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 315
    iget-object v3, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 317
    const-string v0, "gel_suggestions_controller:summons"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 318
    iget-object v3, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 320
    const-string v0, "gel_suggestions_controller:horizontal_summons"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 321
    iget-object v3, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 323
    const-string v0, "gel_suggestions_controller:action_discovery_suggestions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 324
    iget-object v3, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 327
    iget v0, p0, Lebc;->Oq:I

    invoke-static {v0}, Lebc;->hc(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 328
    iget-object v0, p0, Lebc;->bTy:Lebd;

    iget-object v3, p0, Lebc;->bLZ:Ljava/lang/String;

    iget-object v4, p0, Lebc;->bTA:Ljava/util/ArrayList;

    iget-object v5, p0, Lebc;->bTA:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget v6, p0, Lebc;->bTI:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lebd;->a(Ljava/lang/String;Ljava/util/List;I)V

    .line 331
    iget-object v0, p0, Lebc;->bTy:Lebd;

    iget-object v3, p0, Lebc;->bLZ:Ljava/lang/String;

    iget-object v4, p0, Lebc;->bTC:Ljava/util/ArrayList;

    iget-object v5, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lebd;->c(Ljava/lang/String;Ljava/util/List;I)V

    .line 334
    iget-object v0, p0, Lebc;->bLZ:Ljava/lang/String;

    iget-object v3, p0, Lebc;->bTB:Ljava/util/ArrayList;

    iget-object v4, p0, Lebc;->bTB:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lebc;->bTH:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, p0, Lebc;->bTC:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    move v2, v1

    :cond_2
    invoke-direct {p0, v0, v3, v4, v2}, Lebc;->a(Ljava/lang/String;Ljava/util/List;IZ)V

    .line 338
    invoke-direct {p0}, Lebc;->amF()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, v1}, Lebd;->eF(Z)V

    .line 346
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v0, v2

    .line 289
    goto/16 :goto_0

    .line 341
    :cond_5
    iget v0, p0, Lebc;->Oq:I

    invoke-static {v0}, Lebc;->hd(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 342
    iget-object v0, p0, Lebc;->bTy:Lebd;

    iget-object v1, p0, Lebc;->bLZ:Ljava/lang/String;

    iget-object v2, p0, Lebc;->bTE:Ljava/util/ArrayList;

    iget-object v3, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lebd;->d(Ljava/lang/String;Ljava/util/List;I)V

    goto :goto_1
.end method

.method public final z(IZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 357
    iget-boolean v0, p0, Lebc;->bTG:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 361
    iget-object v3, p0, Lebc;->bTy:Lebd;

    if-nez p2, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Lebd;->eE(Z)V

    .line 363
    invoke-static {p1}, Lebc;->hc(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 364
    iget-object v0, p0, Lebc;->bTz:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledu;

    .line 366
    iget-object v3, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 368
    iget-object v3, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lebc;->bTI:I

    .line 369
    iget-object v3, p0, Lebc;->bTy:Lebd;

    const-string v4, ""

    iget-object v5, p0, Lebc;->bTD:Ljava/util/ArrayList;

    iget-object v6, p0, Lebc;->bTD:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-interface {v3, v4, v5, v6}, Lebd;->a(Ljava/lang/String;Ljava/util/List;I)V

    .line 373
    if-eqz v0, :cond_0

    .line 374
    iget-object v3, p0, Lebc;->bTD:Ljava/util/ArrayList;

    iget-object v4, p0, Lebc;->bTF:Lcom/google/android/shared/search/SuggestionLogInfo;

    invoke-virtual {v0, v3, v4, v1}, Ledu;->a(Ljava/util/List;Lcom/google/android/shared/search/SuggestionLogInfo;Z)Ledu;

    .line 377
    :cond_0
    iget-object v0, p0, Lebc;->bTL:Lecd;

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lebc;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->amP()V

    .line 384
    :cond_1
    invoke-direct {p0}, Lebc;->amF()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 385
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, v1}, Lebd;->eF(Z)V

    .line 398
    :goto_2
    iput p1, p0, Lebc;->Oq:I

    .line 399
    return-void

    :cond_2
    move v0, v2

    .line 357
    goto :goto_0

    :cond_3
    move v0, v2

    .line 361
    goto :goto_1

    .line 387
    :cond_4
    iget-object v0, p0, Lebc;->bTy:Lebd;

    invoke-interface {v0, v2}, Lebd;->eF(Z)V

    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lebc;->bTK:Lcom/google/android/shared/search/Suggestion;

    goto :goto_2

    .line 390
    :cond_5
    invoke-static {p1}, Lebc;->hd(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 391
    invoke-direct {p0, v2}, Lebc;->eC(Z)V

    .line 392
    iget-object v0, p0, Lebc;->bTy:Lebd;

    const-string v1, ""

    iget-object v2, p0, Lebc;->bTE:Ljava/util/ArrayList;

    iget-object v3, p0, Lebc;->bTE:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lebd;->d(Ljava/lang/String;Ljava/util/List;I)V

    goto :goto_2

    .line 395
    :cond_6
    invoke-direct {p0, v1}, Lebc;->eC(Z)V

    goto :goto_2
.end method
