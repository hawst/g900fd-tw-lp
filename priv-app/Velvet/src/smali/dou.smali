.class public final Ldou;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field bEs:Ldow;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bEt:Ljava/lang/String;

.field bEu:Licx;

.field final mCoreServices:Lcfo;

.field final mFactory:Lgpu;

.field final mPumpkinState:Ldcg;

.field mPumpkinTagger:Lgkp;

.field final mVoiceSearchServices:Lhhq;


# direct methods
.method public constructor <init>(Lcfo;Lhhq;Ldcg;Lgpu;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Ldou;->mCoreServices:Lcfo;

    .line 55
    iput-object p2, p0, Ldou;->mVoiceSearchServices:Lhhq;

    .line 56
    iput-object p3, p0, Ldou;->mPumpkinState:Ldcg;

    .line 57
    iput-object p4, p0, Ldou;->mFactory:Lgpu;

    .line 58
    return-void
.end method


# virtual methods
.method public final aee()V
    .locals 5

    .prologue
    .line 75
    invoke-virtual {p0}, Ldou;->aem()V

    .line 80
    iget-object v0, p0, Ldou;->mPumpkinState:Ldcg;

    invoke-virtual {v0}, Ldcg;->WV()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    iget-object v1, p0, Ldou;->mPumpkinTagger:Lgkp;

    new-instance v2, Ldoz;

    invoke-direct {v2, p0, v0}, Ldoz;-><init>(Ldou;Lcom/google/android/shared/search/Query;)V

    new-instance v3, Ldox;

    invoke-direct {v3, p0, v0}, Ldox;-><init>(Ldou;Lcom/google/android/shared/search/Query;)V

    new-instance v4, Ldov;

    invoke-direct {v4, p0, v0}, Ldov;-><init>(Ldou;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {v1, v0, v2, v3, v4}, Lgkp;->a(Lcom/google/android/shared/search/Query;Lefk;Lefk;Lefk;)V

    .line 87
    :cond_0
    return-void
.end method

.method public final aej()V
    .locals 0

    .prologue
    .line 65
    invoke-virtual {p0}, Ldou;->ael()V

    .line 66
    return-void
.end method

.method aek()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Ldou;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ael()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-virtual {p0}, Ldou;->aek()Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v1, p0, Ldou;->mVoiceSearchServices:Lhhq;

    iget-object v2, p0, Ldou;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lhhq;->a(Lchk;Ljava/lang/String;)Z

    move-result v1

    .line 94
    if-eqz v1, :cond_2

    .line 95
    iget-object v2, p0, Ldou;->bEt:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 98
    iput-object v3, p0, Ldou;->bEs:Ldow;

    .line 99
    iget-object v1, p0, Ldou;->mPumpkinState:Ldcg;

    invoke-virtual {v1}, Ldcg;->WT()V

    .line 100
    iget-object v1, p0, Ldou;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1, v0}, Lhhq;->nX(Ljava/lang/String;)Lgkp;

    move-result-object v1

    iput-object v1, p0, Ldou;->mPumpkinTagger:Lgkp;

    .line 101
    iput-object v0, p0, Ldou;->bEt:Ljava/lang/String;

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v2, p0, Ldou;->mPumpkinTagger:Lgkp;

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 105
    iput-object v3, p0, Ldou;->bEs:Ldow;

    .line 106
    iget-object v1, p0, Ldou;->mPumpkinState:Ldcg;

    invoke-virtual {v1}, Ldcg;->WT()V

    .line 107
    iget-object v1, p0, Ldou;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1, v0}, Lhhq;->nX(Ljava/lang/String;)Lgkp;

    move-result-object v0

    iput-object v0, p0, Ldou;->mPumpkinTagger:Lgkp;

    goto :goto_0

    .line 109
    :cond_2
    iget-object v0, p0, Ldou;->mPumpkinTagger:Lgkp;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Ldou;->mPumpkinState:Ldcg;

    invoke-virtual {v0}, Ldcg;->WT()V

    .line 113
    iget-object v0, p0, Ldou;->mPumpkinState:Ldcg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldcg;->dg(Z)V

    .line 114
    iput-object v3, p0, Ldou;->bEs:Ldow;

    .line 115
    iput-object v3, p0, Ldou;->mPumpkinTagger:Lgkp;

    goto :goto_0
.end method

.method public final aem()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Ldou;->mPumpkinTagger:Lgkp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldou;->mPumpkinState:Ldcg;

    invoke-virtual {v0}, Ldcg;->WU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Ldou;->bEs:Ldow;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Ldow;

    invoke-direct {v0, p0}, Ldow;-><init>(Ldou;)V

    iput-object v0, p0, Ldou;->bEs:Ldow;

    .line 126
    iget-object v0, p0, Ldou;->mPumpkinTagger:Lgkp;

    iget-object v1, p0, Ldou;->bEs:Ldow;

    invoke-virtual {v0, v1}, Lgkp;->d(Lefk;)V

    .line 129
    :cond_0
    return-void
.end method

.method public final ed()V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0}, Ldou;->ael()V

    .line 62
    return-void
.end method
