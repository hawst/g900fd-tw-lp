.class public final Ljpa;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field private dZe:Ljava/lang/String;

.field private dZf:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4530
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4531
    const/4 v0, 0x0

    iput v0, p0, Ljpa;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljpa;->dZe:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljpa;->afb:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljpa;->dZf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljpa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpa;->eCz:I

    .line 4532
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4445
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpa;->dZe:Ljava/lang/String;

    iget v0, p0, Ljpa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpa;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpa;->afb:Ljava/lang/String;

    iget v0, p0, Ljpa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpa;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpa;->dZf:Ljava/lang/String;

    iget v0, p0, Ljpa;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpa;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4547
    iget v0, p0, Ljpa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4548
    const/4 v0, 0x1

    iget-object v1, p0, Ljpa;->dZe:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4550
    :cond_0
    iget v0, p0, Ljpa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4551
    const/4 v0, 0x2

    iget-object v1, p0, Ljpa;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4553
    :cond_1
    iget v0, p0, Ljpa;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 4554
    const/4 v0, 0x3

    iget-object v1, p0, Ljpa;->dZf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4556
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4557
    return-void
.end method

.method public final bfm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4467
    iget-object v0, p0, Ljpa;->dZe:Ljava/lang/String;

    return-object v0
.end method

.method public final bfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4511
    iget-object v0, p0, Ljpa;->dZf:Ljava/lang/String;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4489
    iget-object v0, p0, Ljpa;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4561
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4562
    iget v1, p0, Ljpa;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4563
    const/4 v1, 0x1

    iget-object v2, p0, Ljpa;->dZe:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4566
    :cond_0
    iget v1, p0, Ljpa;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4567
    const/4 v1, 0x2

    iget-object v2, p0, Ljpa;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4570
    :cond_1
    iget v1, p0, Ljpa;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 4571
    const/4 v1, 0x3

    iget-object v2, p0, Ljpa;->dZf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4574
    :cond_2
    return v0
.end method

.method public final xR(Ljava/lang/String;)Ljpa;
    .locals 1

    .prologue
    .line 4470
    if-nez p1, :cond_0

    .line 4471
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4473
    :cond_0
    iput-object p1, p0, Ljpa;->dZe:Ljava/lang/String;

    .line 4474
    iget v0, p0, Ljpa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpa;->aez:I

    .line 4475
    return-object p0
.end method

.method public final xS(Ljava/lang/String;)Ljpa;
    .locals 1

    .prologue
    .line 4492
    if-nez p1, :cond_0

    .line 4493
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4495
    :cond_0
    iput-object p1, p0, Ljpa;->afb:Ljava/lang/String;

    .line 4496
    iget v0, p0, Ljpa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpa;->aez:I

    .line 4497
    return-object p0
.end method

.method public final xT(Ljava/lang/String;)Ljpa;
    .locals 1

    .prologue
    .line 4514
    if-nez p1, :cond_0

    .line 4515
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4517
    :cond_0
    iput-object p1, p0, Ljpa;->dZf:Ljava/lang/String;

    .line 4518
    iget v0, p0, Ljpa;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpa;->aez:I

    .line 4519
    return-object p0
.end method
