.class public final Lben;
.super Lbek;
.source "PG"


# instance fields
.field public axW:Ljava/lang/String;

.field private final axX:Ljava/util/List;

.field public axZ:Z

.field public aya:Z

.field private ayb:[I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Lbek;-><init>()V

    .line 134
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A valid tableName must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    invoke-virtual {p0, p1}, Lben;->eT(Ljava/lang/String;)Lbek;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lben;->axX:Ljava/util/List;

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lben;->axZ:Z

    .line 140
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lben;->n(Ljava/lang/String;Ljava/lang/String;)Lbek;

    .line 200
    iget-object v0, p0, Lben;->axX:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    return-object p0
.end method

.method public final bridge synthetic b(Ljava/lang/String;Z)Lbek;
    .locals 0

    .prologue
    .line 119
    invoke-super {p0, p1, p2}, Lbek;->b(Ljava/lang/String;Z)Lbek;

    return-object p0
.end method

.method public final bridge synthetic eU(Ljava/lang/String;)Lbek;
    .locals 0

    .prologue
    .line 119
    invoke-super {p0, p1}, Lbek;->eU(Ljava/lang/String;)Lbek;

    return-object p0
.end method

.method public final bridge synthetic eV(Ljava/lang/String;)Lbek;
    .locals 0

    .prologue
    .line 119
    invoke-super {p0, p1}, Lbek;->eV(Ljava/lang/String;)Lbek;

    return-object p0
.end method

.method public final bridge synthetic eW(Ljava/lang/String;)Lbek;
    .locals 0

    .prologue
    .line 119
    invoke-super {p0, p1}, Lbek;->eW(Ljava/lang/String;)Lbek;

    return-object p0
.end method

.method public final eX(Ljava/lang/String;)Lben;
    .locals 0

    .prologue
    .line 144
    invoke-super {p0, p1}, Lbek;->eU(Ljava/lang/String;)Lbek;

    .line 145
    return-object p0
.end method

.method public final eY(Ljava/lang/String;)Lben;
    .locals 0

    .prologue
    .line 156
    invoke-super {p0, p1}, Lbek;->eV(Ljava/lang/String;)Lbek;

    .line 157
    return-object p0
.end method

.method public final eZ(Ljava/lang/String;)Lben;
    .locals 0

    .prologue
    .line 162
    invoke-super {p0, p1}, Lbek;->eW(Ljava/lang/String;)Lbek;

    .line 163
    return-object p0
.end method

.method public final h(Ljava/lang/String;I)Lben;
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Lben;->ayb:[I

    if-nez v0, :cond_0

    .line 228
    invoke-static {}, Lbcb;->xc()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lben;->ayb:[I

    .line 230
    :cond_0
    invoke-static {p1}, Lbcb;->eM(Ljava/lang/String;)I

    move-result v0

    .line 231
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid global search section: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_1
    iget-object v1, p0, Lben;->ayb:[I

    aget v1, v1, v0

    if-eqz v1, :cond_2

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Table spec already contains mapping for global search section "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_2
    iget-object v1, p0, Lben;->ayb:[I

    aput p2, v1, v0

    .line 241
    return-object p0
.end method

.method public final xN()Lbem;
    .locals 13

    .prologue
    .line 259
    new-instance v0, Lbem;

    iget-object v1, p0, Lben;->axN:Ljava/lang/String;

    iget-object v2, p0, Lben;->axP:Ljava/lang/String;

    iget-object v3, p0, Lben;->axQ:Ljava/lang/String;

    iget-object v4, p0, Lben;->axR:Ljava/lang/String;

    iget-object v5, p0, Lben;->axW:Ljava/lang/String;

    iget-object v6, p0, Lben;->axX:Ljava/util/List;

    iget-object v7, p0, Lben;->axV:Ljava/util/Map;

    const/4 v8, 0x0

    iget-object v9, p0, Lben;->ayb:[I

    iget-boolean v10, p0, Lben;->axZ:Z

    iget-boolean v11, p0, Lben;->aya:Z

    iget-boolean v12, p0, Lben;->axU:Z

    invoke-direct/range {v0 .. v12}, Lbem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;[IZZZ)V

    return-object v0
.end method
