.class public final Lcpp;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile bfA:[Lcpp;


# instance fields
.field public bfB:Ljava/lang/String;

.field public content:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 30
    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lcpp;->content:[B

    const-string v0, ""

    iput-object v0, p0, Lcpp;->bfB:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcpp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcpp;->eCz:I

    .line 31
    return-void
.end method

.method public static Sd()[Lcpp;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcpp;->bfA:[Lcpp;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcpp;->bfA:[Lcpp;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcpp;

    sput-object v0, Lcpp;->bfA:[Lcpp;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcpp;->bfA:[Lcpp;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcpp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcpp;->content:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpp;->bfB:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lcpp;->content:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 45
    const/4 v0, 0x2

    iget-object v1, p0, Lcpp;->bfB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 47
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 51
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 52
    const/4 v1, 0x1

    iget-object v2, p0, Lcpp;->content:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    const/4 v1, 0x2

    iget-object v2, p0, Lcpp;->bfB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    return v0
.end method
