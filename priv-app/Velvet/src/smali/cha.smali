.class public Lcha;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aWA:Ljava/lang/Integer;

.field private aWB:Ljava/lang/Integer;

.field private aWC:Lchf;

.field private final aWw:Ligi;

.field private final aWx:Ligi;

.field private final aWy:Ljava/util/List;

.field private aWz:Ljava/lang/Integer;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lemp;)V
    .locals 7

    .prologue
    .line 78
    new-instance v5, Lchb;

    invoke-direct {v5, p1}, Lchb;-><init>(Landroid/content/Context;)V

    new-instance v6, Lchc;

    invoke-direct {v6, p1}, Lchc;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcha;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lemp;Ligi;Ligi;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lemp;Ligi;Ligi;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcha;->mContext:Landroid/content/Context;

    .line 104
    iput-object p2, p0, Lcha;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 105
    iput-object p3, p0, Lcha;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 106
    iput-object p4, p0, Lcha;->mClock:Lemp;

    .line 107
    iput-object p5, p0, Lcha;->aWw:Ligi;

    .line 108
    iput-object p6, p0, Lcha;->aWx:Ligi;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcha;->aWy:Ljava/util/List;

    .line 110
    return-void
.end method


# virtual methods
.method public final Fi()Z
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcha;->Fj()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized Fj()I
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcha;->aWC:Lchf;

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcha;->aWC:Lchf;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    new-instance v0, Lchf;

    invoke-direct {v0, p0}, Lchf;-><init>(Lcha;)V

    iput-object v0, p0, Lcha;->aWC:Lchf;

    iget-object v0, p0, Lcha;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcha;->aWC:Lchf;

    iget-object v2, p0, Lcha;->aWC:Lchf;

    invoke-static {v2}, Lchf;->a(Lchf;)Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcha;->mClock:Lemp;

    new-instance v1, Lche;

    invoke-direct {v1, p0}, Lche;-><init>(Lcha;)V

    invoke-interface {v0, v1}, Lemp;->a(Lemq;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcha;->aWA:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lcha;->aWw:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcha;->aWA:Ljava/lang/Integer;

    .line 134
    iget-object v0, p0, Lcha;->aWx:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcha;->aWB:Ljava/lang/Integer;

    .line 136
    const/16 v0, 0x6d

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 138
    new-instance v1, Litt;

    invoke-direct {v1}, Litt;-><init>()V

    iget-object v2, p0, Lcha;->aWA:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Litt;->mz(I)Litt;

    move-result-object v1

    iget-object v2, p0, Lcha;->aWB:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Litt;->mA(I)Litt;

    move-result-object v1

    iput-object v1, v0, Litu;->dIJ:Litt;

    .line 141
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 144
    :cond_1
    iget-object v0, p0, Lcha;->aWA:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 146
    iget-object v0, p0, Lcha;->aWz:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcha;->aWz:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v1, v0, :cond_5

    .line 152
    :cond_2
    iget-object v2, p0, Lcha;->aWy:Ljava/util/List;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153
    :try_start_1
    iget-object v0, p0, Lcha;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchg;

    .line 154
    invoke-interface {v0, v1}, Lchg;->onAvailabilityChanged(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 156
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 128
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 129
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 156
    :cond_4
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    :try_start_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcha;->aWz:Ljava/lang/Integer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 160
    :cond_5
    monitor-exit p0

    return v1
.end method

.method public final declared-synchronized Fk()I
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcha;->aWB:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcha;->aWx:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcha;->aWB:Ljava/lang/Integer;

    .line 191
    :cond_0
    iget-object v0, p0, Lcha;->aWB:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized Fl()V
    .locals 1

    .prologue
    .line 200
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcha;->aWA:Ljava/lang/Integer;

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcha;->aWB:Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    monitor-exit p0

    return-void

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lchg;)V
    .locals 3

    .prologue
    .line 210
    iget-object v1, p0, Lcha;->aWy:Ljava/util/List;

    monitor-enter v1

    .line 211
    :try_start_0
    iget-object v0, p0, Lcha;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "listener already added"

    invoke-static {v0, v2}, Lifv;->c(ZLjava/lang/Object;)V

    .line 212
    iget-object v0, p0, Lcha;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/content/pm/PackageInfo;)Z
    .locals 2

    .prologue
    .line 240
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const-string v1, "Missing signatures. Use GET_SIGNATURES for PackageInfo"

    invoke-static {v0, v1}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcha;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lbgt;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;)Z

    move-result v0

    return v0
.end method

.method public final b(Lchg;)V
    .locals 3

    .prologue
    .line 220
    iget-object v1, p0, Lcha;->aWy:Ljava/util/List;

    monitor-enter v1

    .line 221
    :try_start_0
    iget-object v0, p0, Lcha;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 222
    const-string v2, "listener not added"

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 223
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lemy;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 167
    new-instance v0, Lchd;

    const-string v2, "Get availability"

    iget-object v3, p0, Lcha;->mUiExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcha;->mBgExecutor:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v5, v1, [I

    const/4 v1, 0x2

    aput v1, v5, v7

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lchd;-><init>(Lcha;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[ILemy;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lchd;->a([Ljava/lang/Object;)Lenp;

    .line 179
    return-void
.end method

.method public final eB(I)Landroid/content/Intent;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcha;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lbgt;->dE(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
