.class public final Lccv;
.super Lccp;
.source "PG"


# instance fields
.field final synthetic aRY:Lcom/google/android/remotesearch/RemoteSearchService;

.field private final aSd:Ldmf;


# direct methods
.method public constructor <init>(Lcom/google/android/remotesearch/RemoteSearchService;Ldmf;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    invoke-direct {p0}, Lccp;-><init>()V

    .line 286
    iput-object p2, p0, Lccv;->aSd:Ldmf;

    .line 287
    return-void
.end method

.method private static l([B)Ljkt;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 343
    const/4 v0, 0x0

    .line 344
    if-eqz p0, :cond_0

    array-length v1, p0

    if-lez v1, :cond_0

    .line 346
    :try_start_0
    invoke-static {p0}, Ljkt;->ar([B)Ljkt;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 352
    :cond_0
    :goto_0
    return-object v0

    .line 348
    :catch_0
    move-exception v1

    .line 349
    const-string v2, "RemoteSearchService"

    const-string v3, "Invalid ActionV2"

    invoke-static {v2, v3, v1}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final CS()Z
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lccv;->aSd:Ldmf;

    invoke-static {}, Lccv;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 379
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->CS()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/net/Uri;Lccl;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lccv;->aSd:Ldmf;

    invoke-static {}, Lccv;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 360
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object p2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    .line 361
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-static {}, Lcom/google/android/remotesearch/RemoteSearchService;->CU()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/google/android/shared/search/Query;->b(Landroid/net/Uri;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apa()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 365
    iget-object v1, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v1, v1, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v1, v0}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    .line 366
    return-void
.end method

.method public final a(Landroid/net/Uri;Lccl;[BLjava/lang/String;)V
    .locals 4

    .prologue
    .line 292
    iget-object v0, p0, Lccv;->aSd:Ldmf;

    invoke-static {}, Lccv;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 293
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object p2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    .line 294
    invoke-static {p3}, Lccv;->l([B)Ljkt;

    move-result-object v0

    .line 295
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 296
    const-string v2, "user-agent-suffix"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-static {}, Lcom/google/android/remotesearch/RemoteSearchService;->CU()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/shared/search/Query;->G(Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/shared/search/Query;->E(Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 302
    if-eqz v0, :cond_0

    .line 303
    iget-object v2, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/remotesearch/RemoteSearchService;->a(Lcom/google/android/shared/search/Query;Ljkt;)V

    .line 309
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Nd()V

    .line 307
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v0, v1}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lccv;->aSd:Ldmf;

    invoke-static {}, Lccv;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 372
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->cancel()V

    .line 373
    return-void
.end method

.method public final cd(Z)V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lccv;->aSd:Ldmf;

    invoke-static {}, Lccv;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 390
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mSearchSettings:Lcke;

    invoke-interface {v0, p1}, Lcke;->cd(Z)V

    .line 391
    return-void
.end method

.method public final k([B)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 313
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 314
    iget-object v0, p0, Lccv;->aSd:Ldmf;

    invoke-static {}, Lccv;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 315
    invoke-static {p1}, Lccv;->l([B)Ljkt;

    move-result-object v0

    .line 317
    if-nez v0, :cond_0

    .line 318
    const-string v0, "RemoteSearchService"

    const-string v1, "Unable to parse the actionV2 to execute"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 319
    iget-object v0, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lccl;->ey(I)V

    .line 340
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v1, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    invoke-static {v1}, Lcom/google/android/remotesearch/RemoteSearchService;->a(Lcom/google/android/remotesearch/RemoteSearchService;)V

    .line 327
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-static {}, Lcom/google/android/remotesearch/RemoteSearchService;->CU()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/shared/search/Query;->G(Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 330
    iget-object v2, p0, Lccv;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v2, v2, Lcom/google/android/remotesearch/RemoteSearchService;->aRO:Lhzc;

    new-instance v3, Lccw;

    invoke-direct {v3, p0}, Lccw;-><init>(Lccv;)V

    invoke-virtual {v2, v0, v1, v3}, Lhzc;->a(Ljkt;Lcom/google/android/shared/search/Query;Lefk;)Z

    goto :goto_0
.end method
