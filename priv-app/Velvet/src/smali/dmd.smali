.class public Ldmd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzi;


# static fields
.field private static rZ:Z


# instance fields
.field private final bCc:Lern;

.field private final bCd:Ldla;

.field final bCe:Lkas;

.field private bCf:Lkas;

.field private final bCg:Lepm;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ldla;Lern;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ldme;

    const-string v1, "OkHttpEngine"

    const-string v2, "flush HTTP cache"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v0, p0, v1, v2, v3}, Ldme;-><init>(Ldmd;Ljava/lang/String;Ljava/lang/String;[I)V

    iput-object v0, p0, Ldmd;->bCg:Lepm;

    .line 91
    iput-object p1, p0, Ldmd;->bCd:Ldla;

    .line 92
    iput-object p2, p0, Ldmd;->bCc:Lern;

    .line 93
    new-instance v0, Lkas;

    invoke-direct {v0}, Lkas;-><init>()V

    iput-object v0, p0, Ldmd;->bCe:Lkas;

    .line 94
    new-instance v0, Lkas;

    invoke-direct {v0}, Lkas;-><init>()V

    iput-object v0, p0, Ldmd;->bCf:Lkas;

    .line 95
    iput-object p3, p0, Ldmd;->mContext:Landroid/content/Context;

    .line 96
    return-void

    .line 58
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final Ce()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Ldmd;->bCc:Lern;

    iget-object v1, p0, Ldmd;->bCg:Lepm;

    invoke-interface {v0, v1}, Lern;->a(Leri;)V

    .line 134
    return-void
.end method

.method public final a(Ljava/net/URL;ZZ)Lbzh;
    .locals 10

    .prologue
    .line 138
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    iget-object v2, p0, Ldmd;->bCd:Ldla;

    iget-object v3, p0, Ldmd;->mContext:Landroid/content/Context;

    iget-object v4, p0, Ldmd;->bCe:Lkas;

    iget-object v5, p0, Ldmd;->bCf:Lkas;

    const-class v6, Ldmd;

    monitor-enter v6

    :try_start_0
    sget-boolean v0, Ldmd;->rZ:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :try_start_1
    new-instance v7, Ljava/io/File;

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v8, "http"

    invoke-direct {v7, v0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Lkag;

    invoke-interface {v2}, Ldla;->Ew()J

    move-result-wide v8

    invoke-direct {v0, v7, v8, v9}, Lkag;-><init>(Ljava/io/File;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v4, v0}, Lkas;->a(Ljava/net/ResponseCache;)Lkas;

    invoke-virtual {v5, v0}, Lkas;->a(Ljava/net/ResponseCache;)Lkas;

    :cond_0
    new-instance v0, Ldms;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Ldms;-><init>(Ldla;Landroid/content/Context;Z)V

    invoke-virtual {v4, v0}, Lkas;->a(Ljavax/net/ssl/SSLSocketFactory;)Lkas;

    new-instance v0, Ldms;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Ldms;-><init>(Ldla;Landroid/content/Context;Z)V

    invoke-virtual {v5, v0}, Lkas;->a(Ljavax/net/ssl/SSLSocketFactory;)Lkas;

    const/4 v0, 0x1

    sput-boolean v0, Ldmd;->rZ:Z

    :cond_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 142
    :cond_2
    iget-object v0, p0, Ldmd;->bCd:Ldla;

    invoke-interface {v0}, Ldla;->Dg()I

    move-result v1

    .line 143
    iget-object v0, p0, Ldmd;->bCd:Ldla;

    invoke-interface {v0}, Ldla;->Dh()I

    move-result v2

    .line 146
    if-eqz p3, :cond_5

    iget-object v0, p0, Ldmd;->bCd:Ldla;

    invoke-interface {v0}, Ldla;->Ey()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "https"

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 148
    iget-object v0, p0, Ldmd;->bCe:Lkas;

    .line 154
    :goto_1
    new-instance v3, Lbzk;

    invoke-virtual {v0, p1}, Lkas;->b(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-direct {v3, v0}, Lbzk;-><init>(Ljava/net/HttpURLConnection;)V

    .line 155
    if-eqz v1, :cond_3

    .line 156
    invoke-virtual {v3, v1}, Lbzk;->setConnectTimeout(I)V

    .line 158
    :cond_3
    if-eqz v2, :cond_4

    .line 159
    invoke-virtual {v3, v2}, Lbzk;->setReadTimeout(I)V

    .line 161
    :cond_4
    return-object v3

    .line 139
    :catch_0
    move-exception v0

    :try_start_3
    const-string v7, "OkHttpEngine"

    const-string v8, "Failed to install HTTP cache"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v0, v8, v9}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 151
    :cond_5
    iget-object v0, p0, Ldmd;->bCf:Lkas;

    goto :goto_1
.end method
