.class public final Lay;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cF:Landroid/os/Bundle;

.field cI:Ljava/lang/CharSequence;

.field cJ:Ljava/lang/CharSequence;

.field public cK:Landroid/app/PendingIntent;

.field cL:Ljava/lang/CharSequence;

.field public cM:I

.field cN:Z

.field cO:Lbk;

.field public cP:Ljava/util/ArrayList;

.field cQ:Z

.field public cR:Ljava/lang/String;

.field public cS:I

.field public cT:I

.field cU:Landroid/app/Notification;

.field public cV:Ljava/util/ArrayList;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 845
    const/4 v0, 0x1

    iput-boolean v0, p0, Lay;->cN:Z

    .line 855
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lay;->cP:Ljava/util/ArrayList;

    .line 856
    iput-boolean v4, p0, Lay;->cQ:Z

    .line 859
    iput v4, p0, Lay;->cS:I

    .line 860
    iput v4, p0, Lay;->cT:I

    .line 863
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lay;->cU:Landroid/app/Notification;

    .line 878
    iput-object p1, p0, Lay;->mContext:Landroid/content/Context;

    .line 881
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 882
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 883
    iput v4, p0, Lay;->cM:I

    .line 884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lay;->cV:Ljava/util/ArrayList;

    .line 885
    return-void
.end method

.method private b(IZ)V
    .locals 3

    .prologue
    .line 1233
    if-eqz p2, :cond_0

    .line 1234
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1238
    :goto_0
    return-void

    .line 1236
    :cond_0
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method protected static f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1507
    if-nez p0, :cond_1

    .line 1511
    :cond_0
    :goto_0
    return-object p0

    .line 1508
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1509
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Lay;
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 893
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Lay;
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1038
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lay;
    .locals 2

    .prologue
    .line 1102
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1103
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 1104
    return-object p0
.end method

.method public final a(Laz;)Lay;
    .locals 0

    .prologue
    .line 1486
    invoke-interface {p1, p0}, Laz;->a(Lay;)Lay;

    .line 1487
    return-object p0
.end method

.method public final a(Lbk;)Lay;
    .locals 2

    .prologue
    .line 1435
    iget-object v0, p0, Lay;->cO:Lbk;

    if-eq v0, p1, :cond_0

    .line 1436
    iput-object p1, p0, Lay;->cO:Lbk;

    .line 1437
    iget-object v0, p0, Lay;->cO:Lbk;

    if-eqz v0, :cond_0

    .line 1438
    iget-object v0, p0, Lay;->cO:Lbk;

    iget-object v1, v0, Lbk;->cX:Lay;

    if-eq v1, p0, :cond_0

    iput-object p0, v0, Lbk;->cX:Lay;

    iget-object v1, v0, Lbk;->cX:Lay;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbk;->cX:Lay;

    invoke-virtual {v1, v0}, Lay;->a(Lbk;)Lay;

    .line 1441
    :cond_0
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Lay;
    .locals 1

    .prologue
    .line 953
    invoke-static {p1}, Lay;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lay;->cI:Ljava/lang/CharSequence;

    .line 954
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Lay;
    .locals 1

    .prologue
    .line 961
    invoke-static {p1}, Lay;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lay;->cJ:Ljava/lang/CharSequence;

    .line 962
    return-object p0
.end method

.method public final c(Z)Lay;
    .locals 2

    .prologue
    .line 1176
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lay;->b(IZ)V

    .line 1177
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)Lay;
    .locals 1

    .prologue
    .line 993
    invoke-static {p1}, Lay;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lay;->cL:Ljava/lang/CharSequence;

    .line 994
    return-object p0
.end method

.method public final d(Z)Lay;
    .locals 2

    .prologue
    .line 1187
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lay;->b(IZ)V

    .line 1188
    return-object p0
.end method

.method public final e(Ljava/lang/CharSequence;)Lay;
    .locals 2

    .prologue
    .line 1070
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    invoke-static {p1}, Lay;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1071
    return-object p0
.end method

.method public final g(I)Lay;
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 930
    return-object p0
.end method

.method public final h(I)Lay;
    .locals 2

    .prologue
    .line 1225
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 1226
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 1227
    iget-object v0, p0, Lay;->cU:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1229
    :cond_0
    return-object p0
.end method
