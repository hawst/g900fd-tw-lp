.class public Ldmo;
.super Ljava/util/Observable;
.source "PG"


# static fields
.field private static final NULL:Ljava/lang/Object;


# instance fields
.field private final bCr:Ljava/util/Queue;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldmo;->NULL:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Ldmo;->bCr:Ljava/util/Queue;

    .line 31
    iput-object p1, p0, Ldmo;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 32
    return-void
.end method

.method static synthetic a(Ldmo;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Ldmo;->bCr:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic a(Ldmo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0, p1}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic mt()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Ldmo;->NULL:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public notifyObservers()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldmo;->notifyObservers(Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public declared-synchronized notifyObservers(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ldmo;->hasChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Ldmo;->bCr:Ljava/util/Queue;

    if-nez p1, :cond_0

    sget-object p1, Ldmo;->NULL:Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Ldmo;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Ldmp;

    const-string v2, "notifyObservers"

    invoke-direct {v1, p0, v2}, Ldmp;-><init>(Ldmo;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_1
    monitor-exit p0

    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
