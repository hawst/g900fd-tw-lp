.class final Ltl;
.super Landroid/os/Handler;
.source "PG"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# instance fields
.field private synthetic Cu:Ltj;


# direct methods
.method private constructor <init>(Ltj;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Ltl;->Cu:Ltj;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltj;B)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Ltl;-><init>(Ltj;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Ltl;->Cu:Ltj;

    iget-object v1, v0, Ltj;->Cr:Ljava/util/LinkedList;

    monitor-enter v1

    .line 45
    :try_start_0
    iget-object v0, p0, Ltl;->Cu:Ltj;

    iget-object v0, v0, Ltj;->Cr:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 46
    monitor-exit v1

    .line 54
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Ltl;->Cu:Ltj;

    iget-object v0, v0, Ltj;->Cr:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 49
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Runnable;

    .line 50
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 51
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 52
    iget-object v0, p0, Ltl;->Cu:Ltj;

    iget-object v1, v0, Ltj;->Cr:Ljava/util/LinkedList;

    monitor-enter v1

    .line 53
    :try_start_1
    iget-object v0, p0, Ltl;->Cu:Ltj;

    invoke-virtual {v0}, Ltj;->fx()V

    .line 54
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 50
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final queueIdle()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ltl;->handleMessage(Landroid/os/Message;)V

    .line 59
    const/4 v0, 0x0

    return v0
.end method
