.class public final Ljlk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile erI:[Ljlk;


# instance fields
.field private aez:I

.field private afW:Ljava/lang/String;

.field private ajB:Ljava/lang/String;

.field public eoQ:[I

.field private erJ:I

.field private erK:I

.field private erL:[Ljava/lang/String;

.field private erM:[Ljlp;

.field public erN:[I

.field private erO:Ljlo;

.field public erP:Ljkh;

.field private erQ:I

.field private erR:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 10461
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10462
    iput v1, p0, Ljlk;->aez:I

    iput v1, p0, Ljlk;->erJ:I

    iput v1, p0, Ljlk;->erK:I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljlk;->erL:[Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljlk;->eoQ:[I

    invoke-static {}, Ljlp;->bpp()[Ljlp;

    move-result-object v0

    iput-object v0, p0, Ljlk;->erM:[Ljlp;

    const-string v0, ""

    iput-object v0, p0, Ljlk;->ajB:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljlk;->erN:[I

    iput-object v2, p0, Ljlk;->erO:Ljlo;

    const-string v0, ""

    iput-object v0, p0, Ljlk;->afW:Ljava/lang/String;

    iput-object v2, p0, Ljlk;->erP:Ljkh;

    iput v1, p0, Ljlk;->erQ:I

    iput v1, p0, Ljlk;->erR:I

    iput-object v2, p0, Ljlk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlk;->eCz:I

    .line 10463
    return-void
.end method

.method public static boS()[Ljlk;
    .locals 2

    .prologue
    .line 10310
    sget-object v0, Ljlk;->erI:[Ljlk;

    if-nez v0, :cond_1

    .line 10311
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 10313
    :try_start_0
    sget-object v0, Ljlk;->erI:[Ljlk;

    if-nez v0, :cond_0

    .line 10314
    const/4 v0, 0x0

    new-array v0, v0, [Ljlk;

    sput-object v0, Ljlk;->erI:[Ljlk;

    .line 10316
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10318
    :cond_1
    sget-object v0, Ljlk;->erI:[Ljlk;

    return-object v0

    .line 10316
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 10291
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlk;->erJ:I

    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlk;->erK:I

    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlk;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlk;->erL:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljlk;->erL:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljlk;->erL:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljlk;->erL:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_5

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_5
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljlk;->eoQ:[I

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    if-nez v0, :cond_7

    array-length v3, v5

    if-ne v2, v3, :cond_7

    iput-object v5, p0, Ljlk;->eoQ:[I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Ljlk;->eoQ:[I

    array-length v0, v0

    goto :goto_5

    :cond_7
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_8

    iget-object v4, p0, Ljlk;->eoQ:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljlk;->eoQ:[I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_6

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    if-eqz v0, :cond_d

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljlk;->eoQ:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_a

    iget-object v0, p0, Ljlk;->eoQ:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_8

    :pswitch_2
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_b
    iget-object v2, p0, Ljlk;->eoQ:[I

    array-length v2, v2

    goto :goto_7

    :cond_c
    iput-object v4, p0, Ljlk;->eoQ:[I

    :cond_d
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_9
    if-ge v3, v4, :cond_f

    if-eqz v3, :cond_e

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_e
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_3

    move v0, v2

    :goto_a
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_9

    :pswitch_3
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_a

    :cond_f
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljlk;->erN:[I

    if-nez v0, :cond_10

    move v0, v1

    :goto_b
    if-nez v0, :cond_11

    array-length v3, v5

    if-ne v2, v3, :cond_11

    iput-object v5, p0, Ljlk;->erN:[I

    goto/16 :goto_0

    :cond_10
    iget-object v0, p0, Ljlk;->erN:[I

    array-length v0, v0

    goto :goto_b

    :cond_11
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_12

    iget-object v4, p0, Ljlk;->erN:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljlk;->erN:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_c
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_13

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_4

    goto :goto_c

    :pswitch_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_13
    if-eqz v0, :cond_17

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljlk;->erN:[I

    if-nez v2, :cond_15

    move v2, v1

    :goto_d
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_14

    iget-object v0, p0, Ljlk;->erN:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14
    :goto_e
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_16

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_5

    goto :goto_e

    :pswitch_5
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_e

    :cond_15
    iget-object v2, p0, Ljlk;->erN:[I

    array-length v2, v2

    goto :goto_d

    :cond_16
    iput-object v4, p0, Ljlk;->erN:[I

    :cond_17
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlk;->afW:Ljava/lang/String;

    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljlk;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljlk;->erP:Ljkh;

    if-nez v0, :cond_18

    new-instance v0, Ljkh;

    invoke-direct {v0}, Ljkh;-><init>()V

    iput-object v0, p0, Ljlk;->erP:Ljkh;

    :cond_18
    iget-object v0, p0, Ljlk;->erP:Ljkh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlk;->erQ:I

    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljlk;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlk;->erR:I

    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljlk;->aez:I

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ljlk;->erO:Ljlo;

    if-nez v0, :cond_19

    new-instance v0, Ljlo;

    invoke-direct {v0}, Ljlo;-><init>()V

    iput-object v0, p0, Ljlk;->erO:Ljlo;

    :cond_19
    iget-object v0, p0, Ljlk;->erO:Ljlo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x62

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlk;->erM:[Ljlp;

    if-nez v0, :cond_1b

    move v0, v1

    :goto_f
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlp;

    if-eqz v0, :cond_1a

    iget-object v3, p0, Ljlk;->erM:[Ljlp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1a
    :goto_10
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_1c

    new-instance v3, Ljlp;

    invoke-direct {v3}, Ljlp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_1b
    iget-object v0, p0, Ljlk;->erM:[Ljlp;

    array-length v0, v0

    goto :goto_f

    :cond_1c
    new-instance v3, Ljlp;

    invoke-direct {v3}, Ljlp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljlk;->erM:[Ljlp;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlk;->ajB:Ljava/lang/String;

    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlk;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x50 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x6a -> :sswitch_e
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10487
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10488
    const/4 v0, 0x1

    iget v2, p0, Ljlk;->erJ:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 10490
    :cond_0
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 10491
    const/4 v0, 0x2

    iget v2, p0, Ljlk;->erK:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 10493
    :cond_1
    iget-object v0, p0, Ljlk;->erL:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljlk;->erL:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 10494
    :goto_0
    iget-object v2, p0, Ljlk;->erL:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 10495
    iget-object v2, p0, Ljlk;->erL:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 10496
    if-eqz v2, :cond_2

    .line 10497
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 10494
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10501
    :cond_3
    iget-object v0, p0, Ljlk;->eoQ:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljlk;->eoQ:[I

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 10502
    :goto_1
    iget-object v2, p0, Ljlk;->eoQ:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 10503
    const/4 v2, 0x4

    iget-object v3, p0, Ljlk;->eoQ:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 10502
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10506
    :cond_4
    iget-object v0, p0, Ljlk;->erN:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljlk;->erN:[I

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 10507
    :goto_2
    iget-object v2, p0, Ljlk;->erN:[I

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 10508
    const/4 v2, 0x6

    iget-object v3, p0, Ljlk;->erN:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 10507
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 10511
    :cond_5
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 10512
    const/4 v0, 0x7

    iget-object v2, p0, Ljlk;->afW:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 10514
    :cond_6
    iget-object v0, p0, Ljlk;->erP:Ljkh;

    if-eqz v0, :cond_7

    .line 10515
    const/16 v0, 0x8

    iget-object v2, p0, Ljlk;->erP:Ljkh;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 10517
    :cond_7
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    .line 10518
    const/16 v0, 0x9

    iget v2, p0, Ljlk;->erQ:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 10520
    :cond_8
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_9

    .line 10521
    const/16 v0, 0xa

    iget v2, p0, Ljlk;->erR:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 10523
    :cond_9
    iget-object v0, p0, Ljlk;->erO:Ljlo;

    if-eqz v0, :cond_a

    .line 10524
    const/16 v0, 0xb

    iget-object v2, p0, Ljlk;->erO:Ljlo;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 10526
    :cond_a
    iget-object v0, p0, Ljlk;->erM:[Ljlp;

    if-eqz v0, :cond_c

    iget-object v0, p0, Ljlk;->erM:[Ljlp;

    array-length v0, v0

    if-lez v0, :cond_c

    .line 10527
    :goto_3
    iget-object v0, p0, Ljlk;->erM:[Ljlp;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    .line 10528
    iget-object v0, p0, Ljlk;->erM:[Ljlp;

    aget-object v0, v0, v1

    .line 10529
    if-eqz v0, :cond_b

    .line 10530
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 10527
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 10534
    :cond_c
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_d

    .line 10535
    const/16 v0, 0xd

    iget-object v1, p0, Ljlk;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 10537
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10538
    return-void
.end method

.method public final bhT()Z
    .locals 1

    .prologue
    .line 10384
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boT()I
    .locals 1

    .prologue
    .line 10326
    iget v0, p0, Ljlk;->erJ:I

    return v0
.end method

.method public final boU()Z
    .locals 1

    .prologue
    .line 10334
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boV()I
    .locals 1

    .prologue
    .line 10345
    iget v0, p0, Ljlk;->erK:I

    return v0
.end method

.method public final boW()Z
    .locals 1

    .prologue
    .line 10353
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boX()I
    .locals 1

    .prologue
    .line 10426
    iget v0, p0, Ljlk;->erQ:I

    return v0
.end method

.method public final boY()Z
    .locals 1

    .prologue
    .line 10434
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boZ()I
    .locals 1

    .prologue
    .line 10445
    iget v0, p0, Ljlk;->erR:I

    return v0
.end method

.method public final bpa()Z
    .locals 1

    .prologue
    .line 10453
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10373
    iget-object v0, p0, Ljlk;->ajB:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 10542
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10543
    iget v1, p0, Ljlk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10544
    const/4 v1, 0x1

    iget v3, p0, Ljlk;->erJ:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10547
    :cond_0
    iget v1, p0, Ljlk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 10548
    const/4 v1, 0x2

    iget v3, p0, Ljlk;->erK:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10551
    :cond_1
    iget-object v1, p0, Ljlk;->erL:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljlk;->erL:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 10554
    :goto_0
    iget-object v5, p0, Ljlk;->erL:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 10555
    iget-object v5, p0, Ljlk;->erL:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 10556
    if-eqz v5, :cond_2

    .line 10557
    add-int/lit8 v4, v4, 0x1

    .line 10558
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 10554
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10562
    :cond_3
    add-int/2addr v0, v3

    .line 10563
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 10565
    :cond_4
    iget-object v1, p0, Ljlk;->eoQ:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljlk;->eoQ:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    .line 10567
    :goto_1
    iget-object v4, p0, Ljlk;->eoQ:[I

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 10568
    iget-object v4, p0, Ljlk;->eoQ:[I

    aget v4, v4, v1

    .line 10569
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 10567
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 10572
    :cond_5
    add-int/2addr v0, v3

    .line 10573
    iget-object v1, p0, Ljlk;->eoQ:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10575
    :cond_6
    iget-object v1, p0, Ljlk;->erN:[I

    if-eqz v1, :cond_8

    iget-object v1, p0, Ljlk;->erN:[I

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    .line 10577
    :goto_2
    iget-object v4, p0, Ljlk;->erN:[I

    array-length v4, v4

    if-ge v1, v4, :cond_7

    .line 10578
    iget-object v4, p0, Ljlk;->erN:[I

    aget v4, v4, v1

    .line 10579
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 10577
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10582
    :cond_7
    add-int/2addr v0, v3

    .line 10583
    iget-object v1, p0, Ljlk;->erN:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10585
    :cond_8
    iget v1, p0, Ljlk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_9

    .line 10586
    const/4 v1, 0x7

    iget-object v3, p0, Ljlk;->afW:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10589
    :cond_9
    iget-object v1, p0, Ljlk;->erP:Ljkh;

    if-eqz v1, :cond_a

    .line 10590
    const/16 v1, 0x8

    iget-object v3, p0, Ljlk;->erP:Ljkh;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10593
    :cond_a
    iget v1, p0, Ljlk;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_b

    .line 10594
    const/16 v1, 0x9

    iget v3, p0, Ljlk;->erQ:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10597
    :cond_b
    iget v1, p0, Ljlk;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_c

    .line 10598
    const/16 v1, 0xa

    iget v3, p0, Ljlk;->erR:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10601
    :cond_c
    iget-object v1, p0, Ljlk;->erO:Ljlo;

    if-eqz v1, :cond_d

    .line 10602
    const/16 v1, 0xb

    iget-object v3, p0, Ljlk;->erO:Ljlo;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10605
    :cond_d
    iget-object v1, p0, Ljlk;->erM:[Ljlp;

    if-eqz v1, :cond_f

    iget-object v1, p0, Ljlk;->erM:[Ljlp;

    array-length v1, v1

    if-lez v1, :cond_f

    .line 10606
    :goto_3
    iget-object v1, p0, Ljlk;->erM:[Ljlp;

    array-length v1, v1

    if-ge v2, v1, :cond_f

    .line 10607
    iget-object v1, p0, Ljlk;->erM:[Ljlp;

    aget-object v1, v1, v2

    .line 10608
    if-eqz v1, :cond_e

    .line 10609
    const/16 v3, 0xc

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10606
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 10614
    :cond_f
    iget v1, p0, Ljlk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_10

    .line 10615
    const/16 v1, 0xd

    iget-object v2, p0, Ljlk;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10618
    :cond_10
    return v0
.end method

.method public final oj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10401
    iget-object v0, p0, Ljlk;->afW:Ljava/lang/String;

    return-object v0
.end method

.method public final ok()Z
    .locals 1

    .prologue
    .line 10412
    iget v0, p0, Ljlk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qC(I)Ljlk;
    .locals 1

    .prologue
    .line 10329
    iput p1, p0, Ljlk;->erJ:I

    .line 10330
    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlk;->aez:I

    .line 10331
    return-object p0
.end method

.method public final qD(I)Ljlk;
    .locals 1

    .prologue
    .line 10348
    iput p1, p0, Ljlk;->erK:I

    .line 10349
    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlk;->aez:I

    .line 10350
    return-object p0
.end method

.method public final qE(I)Ljlk;
    .locals 1

    .prologue
    .line 10429
    const/16 v0, 0x708

    iput v0, p0, Ljlk;->erQ:I

    .line 10430
    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljlk;->aez:I

    .line 10431
    return-object p0
.end method

.method public final qF(I)Ljlk;
    .locals 1

    .prologue
    .line 10448
    iput p1, p0, Ljlk;->erR:I

    .line 10449
    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljlk;->aez:I

    .line 10450
    return-object p0
.end method

.method public final wG(Ljava/lang/String;)Ljlk;
    .locals 1

    .prologue
    .line 10376
    if-nez p1, :cond_0

    .line 10377
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10379
    :cond_0
    iput-object p1, p0, Ljlk;->ajB:Ljava/lang/String;

    .line 10380
    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlk;->aez:I

    .line 10381
    return-object p0
.end method

.method public final wH(Ljava/lang/String;)Ljlk;
    .locals 1

    .prologue
    .line 10404
    if-nez p1, :cond_0

    .line 10405
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10407
    :cond_0
    iput-object p1, p0, Ljlk;->afW:Ljava/lang/String;

    .line 10408
    iget v0, p0, Ljlk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljlk;->aez:I

    .line 10409
    return-object p0
.end method
