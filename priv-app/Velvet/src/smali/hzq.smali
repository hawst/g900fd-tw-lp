.class public final Lhzq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private dvK:Ljava/lang/String;

.field private dvL:Ljzt;

.field private final mSearchConfig:Lcjs;

.field private final mSearchUrlHelper:Lcpn;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lhym;Lcjs;Lcpn;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lhzq;->mSettings:Lhym;

    .line 35
    iput-object p2, p0, Lhzq;->mSearchConfig:Lcjs;

    .line 36
    iput-object p3, p0, Lhzq;->mSearchUrlHelper:Lcpn;

    .line 37
    return-void
.end method

.method private static a(Ljava/lang/String;Lhym;)Ljzg;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-virtual {p1}, Lhym;->aER()Ljze;

    move-result-object v1

    iget-object v1, v1, Ljze;->eNp:Ljzf;

    if-nez v1, :cond_0

    .line 144
    const-string v1, "VS.ServerInfoSupplier"

    const-string v2, "Debug info section not found"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_0
    return-object v0

    .line 148
    :cond_0
    invoke-virtual {p1}, Lhym;->aER()Ljze;

    move-result-object v1

    iget-object v1, v1, Ljze;->eNp:Ljzf;

    iget-object v3, v1, Ljzf;->eNv:[Ljzg;

    .line 150
    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v1, v3, v2

    .line 151
    invoke-virtual {v1}, Ljzg;->rn()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljzg;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v0, v1

    .line 152
    goto :goto_0

    .line 150
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 156
    :cond_2
    const-string v1, "VS.ServerInfoSupplier"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid or missing override: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Ljzm;Ljava/lang/String;)Ljzm;
    .locals 5

    .prologue
    .line 127
    new-instance v0, Ljzm;

    invoke-direct {v0}, Ljzm;-><init>()V

    invoke-static {p0, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljzm;

    .line 132
    invoke-virtual {p0}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 135
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Ljzm;->Ag(Ljava/lang/String;)Ljzm;

    .line 138
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lhzq;->get()Ljzt;

    move-result-object v0

    return-object v0
.end method

.method public final get()Ljzt;
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Lhzq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNp:Ljzf;

    if-eqz v0, :cond_3

    .line 42
    iget-object v1, p0, Lhzq;->mSettings:Lhym;

    iget-object v2, p0, Lhzq;->mSearchConfig:Lcjs;

    invoke-virtual {v1}, Lhym;->aUo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Dev SSL/HTTPS"

    invoke-static {v0, v1}, Lhzq;->a(Ljava/lang/String;Lhym;)Ljzg;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v0, "VS.ServerInfoSupplier"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Using manual S3 server override: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljzt;

    invoke-direct {v0}, Ljzt;-><init>()V

    iget-object v1, v4, Ljzg;->eNj:Ljzt;

    iget-object v1, v1, Ljzt;->eOr:Ljzm;

    invoke-static {v1, v3}, Lhzq;->a(Ljzm;Ljava/lang/String;)Ljzm;

    move-result-object v1

    iput-object v1, v0, Ljzt;->eOr:Ljzm;

    iget-object v1, v4, Ljzg;->eNj:Ljzt;

    iget-object v1, v1, Ljzt;->eOs:Ljzm;

    invoke-static {v1, v3}, Lhzq;->a(Ljzm;Ljava/lang/String;)Ljzm;

    move-result-object v1

    iput-object v1, v0, Ljzt;->eOs:Ljzm;

    .line 43
    :goto_0
    if-eqz v0, :cond_3

    .line 61
    :goto_1
    return-object v0

    .line 42
    :cond_0
    invoke-virtual {v1}, Lhym;->Mc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcjs;->Mc()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0, v1}, Lhzq;->a(Ljava/lang/String;Lhym;)Ljzg;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "VS.ServerInfoSupplier"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Using s3 override: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Ljzg;->eNj:Ljzt;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :cond_3
    iget-object v0, p0, Lhzq;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0}, Lcpn;->Rq()Ljava/lang/String;

    move-result-object v1

    .line 50
    monitor-enter p0

    .line 53
    :try_start_0
    iget-object v0, p0, Lhzq;->dvK:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    iget-object v0, p0, Lhzq;->dvL:Ljzt;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 58
    :cond_4
    :try_start_1
    iput-object v1, p0, Lhzq;->dvK:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lhzq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNj:Ljzt;

    const-string v2, "sandbox.google"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_5

    iget-object v2, v0, Ljzt;->eOs:Ljzm;

    invoke-virtual {v2}, Ljzm;->bxc()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    :goto_2
    iput-object v0, p0, Lhzq;->dvL:Ljzt;

    .line 61
    iget-object v0, p0, Lhzq;->dvL:Ljzt;

    monitor-exit p0

    goto :goto_1

    .line 59
    :cond_6
    new-instance v2, Ljzt;

    invoke-direct {v2}, Ljzt;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljzt;

    iget-object v2, v0, Ljzt;->eOr:Ljzm;

    invoke-virtual {v2}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, v0, Ljzt;->eOs:Ljzm;

    invoke-virtual {v3}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, v0, Ljzt;->eOr:Ljzm;

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljzm;->Ag(Ljava/lang/String;)Ljzm;

    iget-object v2, v0, Ljzt;->eOs:Ljzm;

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljzm;->Ag(Ljava/lang/String;)Ljzm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
