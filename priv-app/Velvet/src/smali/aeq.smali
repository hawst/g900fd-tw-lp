.class final Laeq;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 1138
    const v0, 0x7f0401c2

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1139
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Laeq;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 1140
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1143
    invoke-virtual {p0, p1}, Laeq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeu;

    iget-object v0, v0, Laeu;->UB:Landroid/graphics/drawable/Drawable;

    .line 1144
    if-nez v0, :cond_0

    .line 1145
    const-string v1, "Launcher.WallpaperPickerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error decoding thumbnail for wallpaper #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    :cond_0
    iget-object v1, p0, Laeq;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-static {v1, p2, p3, v0}, Ladz;->a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
