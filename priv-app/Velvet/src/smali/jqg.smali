.class public final Ljqg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eyD:[Ljqg;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private eoL:I

.field private eyE:I

.field private eyF:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 684
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 685
    iput v1, p0, Ljqg;->aez:I

    iput v1, p0, Ljqg;->eoL:I

    const-string v0, ""

    iput-object v0, p0, Ljqg;->akf:Ljava/lang/String;

    iput v1, p0, Ljqg;->eyE:I

    iput v1, p0, Ljqg;->eyF:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqg;->eCz:I

    .line 686
    return-void
.end method

.method public static bsr()[Ljqg;
    .locals 2

    .prologue
    .line 592
    sget-object v0, Ljqg;->eyD:[Ljqg;

    if-nez v0, :cond_1

    .line 593
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 595
    :try_start_0
    sget-object v0, Ljqg;->eyD:[Ljqg;

    if-nez v0, :cond_0

    .line 596
    const/4 v0, 0x0

    new-array v0, v0, [Ljqg;

    sput-object v0, Ljqg;->eyD:[Ljqg;

    .line 598
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    :cond_1
    sget-object v0, Ljqg;->eyD:[Ljqg;

    return-object v0

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 586
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqg;->eoL:I

    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqg;->akf:Ljava/lang/String;

    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqg;->eyE:I

    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqg;->eyF:I

    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqg;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 702
    iget v0, p0, Ljqg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 703
    const/4 v0, 0x1

    iget v1, p0, Ljqg;->eoL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 705
    :cond_0
    iget v0, p0, Ljqg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 706
    const/4 v0, 0x2

    iget-object v1, p0, Ljqg;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 708
    :cond_1
    iget v0, p0, Ljqg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 709
    const/4 v0, 0x3

    iget v1, p0, Ljqg;->eyE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 711
    :cond_2
    iget v0, p0, Ljqg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 712
    const/4 v0, 0x5

    iget v1, p0, Ljqg;->eyF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 714
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 715
    return-void
.end method

.method public final ajF()I
    .locals 1

    .prologue
    .line 649
    iget v0, p0, Ljqg;->eyE:I

    return v0
.end method

.method public final bss()I
    .locals 1

    .prologue
    .line 668
    iget v0, p0, Ljqg;->eyF:I

    return v0
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 608
    iget v0, p0, Ljqg;->eoL:I

    return v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Ljqg;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 719
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 720
    iget v1, p0, Ljqg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 721
    const/4 v1, 0x1

    iget v2, p0, Ljqg;->eoL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 724
    :cond_0
    iget v1, p0, Ljqg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 725
    const/4 v1, 0x2

    iget-object v2, p0, Ljqg;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 728
    :cond_1
    iget v1, p0, Ljqg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 729
    const/4 v1, 0x3

    iget v2, p0, Ljqg;->eyE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 732
    :cond_2
    iget v1, p0, Ljqg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 733
    const/4 v1, 0x5

    iget v2, p0, Ljqg;->eyF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 736
    :cond_3
    return v0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 638
    iget v0, p0, Ljqg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rq(I)Ljqg;
    .locals 1

    .prologue
    .line 611
    iput p1, p0, Ljqg;->eoL:I

    .line 612
    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqg;->aez:I

    .line 613
    return-object p0
.end method

.method public final rr(I)Ljqg;
    .locals 1

    .prologue
    .line 652
    iput p1, p0, Ljqg;->eyE:I

    .line 653
    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqg;->aez:I

    .line 654
    return-object p0
.end method

.method public final rs(I)Ljqg;
    .locals 1

    .prologue
    .line 671
    iput p1, p0, Ljqg;->eyF:I

    .line 672
    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqg;->aez:I

    .line 673
    return-object p0
.end method

.method public final yh(Ljava/lang/String;)Ljqg;
    .locals 1

    .prologue
    .line 630
    if-nez p1, :cond_0

    .line 631
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 633
    :cond_0
    iput-object p1, p0, Ljqg;->akf:Ljava/lang/String;

    .line 634
    iget v0, p0, Ljqg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqg;->aez:I

    .line 635
    return-object p0
.end method
