.class public final Lanb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afV:I

.field private afW:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12564
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12565
    iput v0, p0, Lanb;->aez:I

    iput v0, p0, Lanb;->afV:I

    const-string v0, ""

    iput-object v0, p0, Lanb;->afW:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lanb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lanb;->eCz:I

    .line 12566
    return-void
.end method


# virtual methods
.method public final Y(Ljava/lang/String;)Lanb;
    .locals 1

    .prologue
    .line 12548
    if-nez p1, :cond_0

    .line 12549
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12551
    :cond_0
    iput-object p1, p0, Lanb;->afW:Ljava/lang/String;

    .line 12552
    iget v0, p0, Lanb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanb;->aez:I

    .line 12553
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 12504
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lanb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lanb;->afV:I

    iget v0, p0, Lanb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanb;->afW:Ljava/lang/String;

    iget v0, p0, Lanb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12580
    iget v0, p0, Lanb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 12581
    const/4 v0, 0x1

    iget v1, p0, Lanb;->afV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 12583
    :cond_0
    iget v0, p0, Lanb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 12584
    const/4 v0, 0x2

    iget-object v1, p0, Lanb;->afW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12586
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12587
    return-void
.end method

.method public final ch(I)Lanb;
    .locals 1

    .prologue
    .line 12529
    iput p1, p0, Lanb;->afV:I

    .line 12530
    iget v0, p0, Lanb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanb;->aez:I

    .line 12531
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12591
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12592
    iget v1, p0, Lanb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 12593
    const/4 v1, 0x1

    iget v2, p0, Lanb;->afV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12596
    :cond_0
    iget v1, p0, Lanb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 12597
    const/4 v1, 0x2

    iget-object v2, p0, Lanb;->afW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12600
    :cond_1
    return v0
.end method

.method public final oh()I
    .locals 1

    .prologue
    .line 12526
    iget v0, p0, Lanb;->afV:I

    return v0
.end method

.method public final oi()Z
    .locals 1

    .prologue
    .line 12534
    iget v0, p0, Lanb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12545
    iget-object v0, p0, Lanb;->afW:Ljava/lang/String;

    return-object v0
.end method

.method public final ok()Z
    .locals 1

    .prologue
    .line 12556
    iget v0, p0, Lanb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
