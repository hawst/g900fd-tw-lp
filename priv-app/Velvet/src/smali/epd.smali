.class public final Lepd;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static varargs a(Landroid/content/pm/PackageManager;[Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 7

    .prologue
    const/high16 v6, 0x10000

    const/4 v2, 0x0

    .line 34
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    aget-object v0, p1, v2

    if-nez v0, :cond_1

    .line 35
    :cond_0
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avb()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 38
    :cond_1
    array-length v3, p1

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, p1, v1

    .line 44
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 46
    invoke-virtual {p0, v4, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 54
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 57
    if-eqz v5, :cond_7

    .line 60
    invoke-static {v5}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 64
    invoke-static {p0, v4, v0, v3}, Lcom/google/android/shared/util/App;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Ljava/util/List;Ljava/util/List;)V

    .line 68
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 70
    :goto_3
    new-instance v1, Lcom/google/android/shared/util/MatchingAppInfo;

    invoke-direct {v1, v3, v0}, Lcom/google/android/shared/util/MatchingAppInfo;-><init>(Ljava/util/List;Lcom/google/android/shared/util/App;)V

    move-object v0, v1

    goto :goto_0

    .line 51
    :cond_2
    invoke-virtual {p0, v4, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 68
    :cond_3
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v4, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    :goto_4
    if-nez v0, :cond_6

    const-string v0, "MatchingAppInfoFactory"

    const-string v1, "resolveForPreferredApp(): System should resolve intent to some app!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    invoke-virtual {p0, v4, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    goto :goto_4

    :cond_6
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/shared/util/App;->c(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/shared/util/App;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_3

    .line 38
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 74
    :cond_8
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avb()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    goto :goto_0
.end method
