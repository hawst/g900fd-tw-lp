.class public final Leve;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic cjL:Lcom/google/android/sidekick/main/RemindersListActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/main/RemindersListActivity;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 234
    iget-object v0, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    iget-object v0, v0, Lcom/google/android/sidekick/main/RemindersListActivity;->mUserInteractionLogger:Lcpx;

    const-string v1, "BUTTON_PRESS"

    const-string v2, "ADD_REMINDER"

    invoke-virtual {v0, v1, v2}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    iget-object v0, v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    iget-object v0, v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 239
    iget-object v0, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    .line 244
    :cond_0
    iget-object v0, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    const v3, 0x7f0a03f8

    invoke-virtual {v2, v3}, Lcom/google/android/sidekick/main/RemindersListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    const-string v2, "velvet"

    const-string v3, "android-search-app"

    invoke-static {v2, v3}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v2

    invoke-virtual {v2}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-static {v0, v1}, Lhgn;->b(Landroid/content/Context;Lcom/google/android/shared/search/Query;)Landroid/content/Intent;

    move-result-object v0

    .line 251
    iget-object v1, p0, Leve;->cjL:Lcom/google/android/sidekick/main/RemindersListActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->startActivity(Landroid/content/Intent;)V

    .line 252
    return-void
.end method
