.class public final Leud;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private final mCardsPrefs:Lcxs;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mLoginHelper:Lcrh;


# direct methods
.method public constructor <init>(Lcxs;Lcrh;Leue;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Leud;->mCardsPrefs:Lcxs;

    .line 28
    iput-object p2, p0, Leud;->mLoginHelper:Lcrh;

    .line 29
    iput-object p3, p0, Leud;->mGmsLocationReportingHelper:Leue;

    .line 30
    iget-object v0, p0, Leud;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TI()Lcyg;

    move-result-object v0

    invoke-interface {v0, p0}, Lcyg;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 31
    iget-object v0, p0, Leud;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TI()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "location_reporting_configuration"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 33
    return-void
.end method


# virtual methods
.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    const-string v0, "location_reporting_configuration"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Leud;->mCardsPrefs:Lcxs;

    iget-object v1, p0, Leud;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    iget-object v1, v0, Liyl;->dQH:Ljbs;

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Leud;->mGmsLocationReportingHelper:Leue;

    iget-object v0, v0, Liyl;->dQH:Ljbs;

    invoke-virtual {v1, v0}, Leue;->a(Ljbs;)Lcgs;

    .line 50
    iget-object v0, p0, Leud;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TI()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "location_reporting_configuration"

    invoke-interface {v0, v1, v2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 54
    :cond_0
    return-void
.end method
