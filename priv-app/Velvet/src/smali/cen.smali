.class public Lcen;
.super Lcep;
.source "PG"


# instance fields
.field private final aTA:Ljava/util/concurrent/Future;

.field private final aTB:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final aTC:Ljwe;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final aTD:Ljwe;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field protected final aTE:Lces;

.field private final aTr:Ljava/lang/String;

.field private final aTz:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljwe;Ljwe;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/util/concurrent/Future;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Future;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljwe;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljwe;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 53
    invoke-direct {p0}, Lcep;-><init>()V

    .line 54
    iput-object p1, p0, Lcen;->aTz:Ljava/util/concurrent/Future;

    .line 55
    iput-object p2, p0, Lcen;->aTA:Ljava/util/concurrent/Future;

    .line 56
    iput-object p3, p0, Lcen;->aTB:Ljava/util/concurrent/Future;

    .line 57
    iput-object p4, p0, Lcen;->aTC:Ljwe;

    .line 58
    iput-object p5, p0, Lcen;->aTD:Ljwe;

    .line 59
    iput-object p6, p0, Lcen;->mRequestId:Ljava/lang/String;

    .line 60
    iput-object p7, p0, Lcen;->aTr:Ljava/lang/String;

    .line 61
    new-instance v0, Lces;

    const-wide/16 v2, 0x1388

    invoke-direct {v0, v2, v3}, Lces;-><init>(J)V

    iput-object v0, p0, Lcen;->aTE:Lces;

    .line 62
    return-void
.end method


# virtual methods
.method public Dn()Ljwv;
    .locals 5

    .prologue
    .line 66
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v0

    iget-object v1, p0, Lcen;->aTr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljwv;->zH(Ljava/lang/String;)Ljwv;

    move-result-object v0

    .line 67
    const/4 v1, 0x0

    .line 69
    :try_start_0
    iget-object v2, p0, Lcen;->aTz:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_0

    .line 70
    const v1, 0x20003

    .line 71
    sget-object v2, Ljwl;->eJf:Ljsm;

    iget-object v3, p0, Lcen;->aTE:Lces;

    iget-object v4, p0, Lcen;->aTz:Ljava/util/concurrent/Future;

    invoke-virtual {v3, v4}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 74
    :cond_0
    iget-object v2, p0, Lcen;->aTC:Ljwe;

    if-eqz v2, :cond_1

    .line 75
    sget-object v2, Ljwe;->eIT:Ljsm;

    iget-object v3, p0, Lcen;->aTC:Ljwe;

    invoke-virtual {v0, v2, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 77
    :cond_1
    iget-object v2, p0, Lcen;->aTD:Ljwe;

    if-eqz v2, :cond_2

    .line 78
    sget-object v2, Ljwe;->eIU:Ljsm;

    iget-object v3, p0, Lcen;->aTD:Ljwe;

    invoke-virtual {v0, v2, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 80
    :cond_2
    iget-object v2, p0, Lcen;->aTB:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_3

    .line 81
    const v1, 0x20004

    .line 82
    sget-object v2, Ljwy;->eJP:Ljsm;

    iget-object v3, p0, Lcen;->aTE:Lces;

    iget-object v4, p0, Lcen;->aTB:Ljava/util/concurrent/Future;

    invoke-virtual {v3, v4}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 85
    :cond_3
    sget-object v2, Ljwx;->eJO:Ljsm;

    new-instance v3, Ljwx;

    invoke-direct {v3}, Ljwx;-><init>()V

    iget-object v4, p0, Lcen;->mRequestId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljwx;->zI(Ljava/lang/String;)Ljwx;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 87
    const v1, 0x20005

    .line 88
    sget-object v2, Ljwu;->eJE:Ljsm;

    iget-object v3, p0, Lcen;->aTE:Lces;

    iget-object v4, p0, Lcen;->aTA:Ljava/util/concurrent/Future;

    invoke-virtual {v3, v4}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v2, Leie;

    invoke-direct {v2, v0, v1}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v2
.end method

.method public final yo()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method
