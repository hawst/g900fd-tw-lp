.class public final Ldxr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 243
    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    const-class v0, Ljqg;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljqg;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;-><init>(Ljqg;)V

    return-object v1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 243
    new-array v0, p1, [Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    return-object v0
.end method
