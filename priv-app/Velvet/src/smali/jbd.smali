.class public final Ljbd;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dXN:Ljcn;

.field private dXO:J

.field private dXP:Ljbe;

.field private dXQ:Ljava/lang/String;

.field private dXR:[Ljbf;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64673
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 64674
    const/4 v0, 0x0

    iput v0, p0, Ljbd;->aez:I

    iput-object v2, p0, Ljbd;->dXN:Ljcn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbd;->dXO:J

    iput-object v2, p0, Ljbd;->dXP:Ljbe;

    const-string v0, ""

    iput-object v0, p0, Ljbd;->dXQ:Ljava/lang/String;

    invoke-static {}, Ljbf;->beM()[Ljbf;

    move-result-object v0

    iput-object v0, p0, Ljbd;->dXR:[Ljbf;

    iput-object v2, p0, Ljbd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbd;->eCz:I

    .line 64675
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63884
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljbd;->dXN:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbd;->dXN:Ljcn;

    :cond_1
    iget-object v0, p0, Ljbd;->dXN:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbd;->dXO:J

    iget v0, p0, Ljbd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbd;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbd;->dXR:[Ljbf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbf;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljbd;->dXR:[Ljbf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljbf;

    invoke-direct {v3}, Ljbf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljbd;->dXR:[Ljbf;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljbf;

    invoke-direct {v3}, Ljbf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbd;->dXR:[Ljbf;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljbd;->dXP:Ljbe;

    if-nez v0, :cond_5

    new-instance v0, Ljbe;

    invoke-direct {v0}, Ljbe;-><init>()V

    iput-object v0, p0, Ljbd;->dXP:Ljbe;

    :cond_5
    iget-object v0, p0, Ljbd;->dXP:Ljbe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbd;->dXQ:Ljava/lang/String;

    iget v0, p0, Ljbd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbd;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 64692
    iget-object v0, p0, Ljbd;->dXN:Ljcn;

    if-eqz v0, :cond_0

    .line 64693
    const/4 v0, 0x1

    iget-object v1, p0, Ljbd;->dXN:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 64695
    :cond_0
    iget v0, p0, Ljbd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 64696
    const/4 v0, 0x2

    iget-wide v2, p0, Ljbd;->dXO:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 64698
    :cond_1
    iget-object v0, p0, Ljbd;->dXR:[Ljbf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljbd;->dXR:[Ljbf;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 64699
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljbd;->dXR:[Ljbf;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 64700
    iget-object v1, p0, Ljbd;->dXR:[Ljbf;

    aget-object v1, v1, v0

    .line 64701
    if-eqz v1, :cond_2

    .line 64702
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 64699
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64706
    :cond_3
    iget-object v0, p0, Ljbd;->dXP:Ljbe;

    if-eqz v0, :cond_4

    .line 64707
    const/4 v0, 0x4

    iget-object v1, p0, Ljbd;->dXP:Ljbe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 64709
    :cond_4
    iget v0, p0, Ljbd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 64710
    const/4 v0, 0x5

    iget-object v1, p0, Ljbd;->dXQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 64712
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 64713
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 64717
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 64718
    iget-object v1, p0, Ljbd;->dXN:Ljcn;

    if-eqz v1, :cond_0

    .line 64719
    const/4 v1, 0x1

    iget-object v2, p0, Ljbd;->dXN:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64722
    :cond_0
    iget v1, p0, Ljbd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 64723
    const/4 v1, 0x2

    iget-wide v2, p0, Ljbd;->dXO:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 64726
    :cond_1
    iget-object v1, p0, Ljbd;->dXR:[Ljbf;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljbd;->dXR:[Ljbf;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 64727
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljbd;->dXR:[Ljbf;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 64728
    iget-object v2, p0, Ljbd;->dXR:[Ljbf;

    aget-object v2, v2, v0

    .line 64729
    if-eqz v2, :cond_2

    .line 64730
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 64727
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 64735
    :cond_4
    iget-object v1, p0, Ljbd;->dXP:Ljbe;

    if-eqz v1, :cond_5

    .line 64736
    const/4 v1, 0x4

    iget-object v2, p0, Ljbd;->dXP:Ljbe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64739
    :cond_5
    iget v1, p0, Ljbd;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 64740
    const/4 v1, 0x5

    iget-object v2, p0, Ljbd;->dXQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64743
    :cond_6
    return v0
.end method
