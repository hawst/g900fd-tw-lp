.class public final Lfdg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cpg:Ligi;

.field private final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private final mWidgetManager:Lfjo;


# direct methods
.method public constructor <init>(Ligi;Lcjs;Lfjo;Lemp;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lfdg;->cpg:Ligi;

    .line 32
    iput-object p2, p0, Lfdg;->mConfig:Lcjs;

    .line 33
    iput-object p3, p0, Lfdg;->mWidgetManager:Lfjo;

    .line 34
    iput-object p4, p0, Lfdg;->mClock:Lemp;

    .line 35
    return-void
.end method


# virtual methods
.method public final ayp()V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lfdg;->cpg:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_predictive_interaction"

    iget-object v2, p0, Lfdg;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 48
    return-void
.end method

.method public final ayq()Z
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 56
    iget-object v0, p0, Lfdg;->mWidgetManager:Lfjo;

    invoke-virtual {v0}, Lfjo;->aAu()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lfdg;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LN()I

    move-result v3

    if-gtz v3, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lfdg;->cpg:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    const-string v4, "last_predictive_interaction"

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lfdg;->ayp()V

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfdg;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    int-to-long v8, v3

    const-wide/32 v10, 0xea60

    mul-long/2addr v8, v10

    sub-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method
