.class public Lhgz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldvn;


# instance fields
.field private final aoE:Landroid/content/Context;

.field private final dgQ:Ligi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Ligi;)V
    .locals 2
    .param p2    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f090162

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lhgz;->aoE:Landroid/content/Context;

    .line 56
    iput-object p3, p0, Lhgz;->dgQ:Ligi;

    .line 58
    return-void
.end method


# virtual methods
.method public a(Lhjo;)Lhjs;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p1}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjs;

    invoke-virtual {v0, p1}, Lhjs;->b(Lhjo;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lhnu;

    iget-object v1, p0, Lhgz;->aoE:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhnu;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhgz;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    invoke-interface {v0, p1}, Lhju;->a(Lcom/google/android/search/shared/actions/VoiceAction;)Lhjs;

    move-result-object v0

    return-object v0
.end method
