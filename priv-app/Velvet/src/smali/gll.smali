.class public final Lgll;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final RO:Ljava/lang/String;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lgll;->mResources:Landroid/content/res/Resources;

    .line 29
    iput-object p2, p0, Lgll;->RO:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public final G(Ljava/lang/String;I)Ljava/lang/StringBuilder;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lgll;->mResources:Landroid/content/res/Resources;

    const-string v2, "raw"

    iget-object v3, p0, Lgll;->RO:Ljava/lang/String;

    invoke-virtual {v0, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 34
    if-eqz v3, :cond_0

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    const/high16 v2, 0x80000

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 39
    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lgll;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :try_start_1
    invoke-static {v2, v0}, Lisp;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 42
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 51
    :goto_0
    return-object v0

    .line 42
    :catchall_0
    move-exception v0

    :goto_1
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :cond_0
    move-object v0, v1

    .line 51
    goto :goto_0

    .line 42
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method
