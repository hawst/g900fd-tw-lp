.class public abstract Lnq;
.super Landroid/view/View;
.source "PG"


# static fields
.field private static ne:I

.field private static nf:I

.field private static ng:I

.field protected static nh:I

.field private static ni:I

.field private static nj:I

.field private static nk:I

.field protected static nl:I


# instance fields
.field private mF:I

.field private mX:Lng;

.field private final ms:Ljava/util/Calendar;

.field protected nA:Z

.field protected nB:I

.field protected nC:I

.field protected nD:I

.field protected nE:I

.field private nF:Ljava/util/Calendar;

.field private final nG:Lnr;

.field private nH:I

.field private nI:Lns;

.field private nJ:Z

.field protected nK:I

.field protected nL:I

.field protected nM:I

.field private nN:I

.field private nO:I

.field protected nm:I

.field private nn:Ljava/lang/String;

.field private no:Ljava/lang/String;

.field protected np:Landroid/graphics/Paint;

.field private nq:Landroid/graphics/Paint;

.field private nr:Landroid/graphics/Paint;

.field protected ns:Landroid/graphics/Paint;

.field private nt:Landroid/graphics/Paint;

.field private final nu:Ljava/util/Formatter;

.field private final nv:Ljava/lang/StringBuilder;

.field protected nw:I

.field protected nx:I

.field protected ny:I

.field protected nz:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/16 v0, 0x20

    sput v0, Lnq;->ne:I

    .line 103
    const/16 v0, 0xa

    sput v0, Lnq;->nf:I

    .line 114
    const/4 v0, 0x1

    sput v0, Lnq;->ng:I

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lnq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 192
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 195
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 127
    iput v5, p0, Lnq;->nm:I

    .line 142
    sget v0, Lnq;->ne:I

    iput v0, p0, Lnq;->nz:I

    .line 156
    iput-boolean v5, p0, Lnq;->nA:Z

    .line 158
    iput v1, p0, Lnq;->nB:I

    .line 160
    iput v1, p0, Lnq;->nC:I

    .line 162
    iput v4, p0, Lnq;->mF:I

    .line 164
    const/4 v0, 0x7

    iput v0, p0, Lnq;->nD:I

    .line 166
    iget v0, p0, Lnq;->nD:I

    iput v0, p0, Lnq;->nE:I

    .line 168
    const/4 v0, 0x6

    iput v0, p0, Lnq;->nH:I

    .line 329
    iput v5, p0, Lnq;->nO:I

    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 198
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lnq;->nF:Ljava/util/Calendar;

    .line 199
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lnq;->ms:Ljava/util/Calendar;

    .line 201
    const v1, 0x7f0a006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnq;->nn:Ljava/lang/String;

    .line 202
    const v1, 0x7f0a0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnq;->no:Ljava/lang/String;

    .line 204
    const v1, 0x7f0b003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lnq;->nK:I

    .line 205
    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lnq;->nL:I

    .line 206
    const v1, 0x7f0b003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lnq;->nM:I

    .line 207
    const v1, 0x7f0b0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 208
    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lnq;->nN:I

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v1, p0, Lnq;->nv:Ljava/lang/StringBuilder;

    .line 211
    new-instance v1, Ljava/util/Formatter;

    iget-object v2, p0, Lnq;->nv:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v1, p0, Lnq;->nu:Ljava/util/Formatter;

    .line 213
    const v1, 0x7f0d0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lnq;->nh:I

    .line 214
    const v1, 0x7f0d0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lnq;->ni:I

    .line 215
    const v1, 0x7f0d0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lnq;->nj:I

    .line 216
    const v1, 0x7f0d0040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lnq;->nk:I

    .line 217
    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lnq;->nl:I

    .line 220
    const v1, 0x7f0d003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sget v1, Lnq;->nk:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lnq;->nz:I

    .line 224
    new-instance v0, Lnr;

    invoke-direct {v0, p0, p0}, Lnr;-><init>(Lnq;Landroid/view/View;)V

    iput-object v0, p0, Lnq;->nG:Lnr;

    .line 225
    iget-object v0, p0, Lnq;->nG:Lnr;

    invoke-static {p0, v0}, Lge;->a(Landroid/view/View;Lep;)V

    .line 226
    invoke-static {p0, v4}, Lge;->c(Landroid/view/View;I)V

    .line 227
    iput-boolean v4, p0, Lnq;->nJ:Z

    .line 230
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    sget v1, Lnq;->ni:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    iget-object v1, p0, Lnq;->no:Ljava/lang/String;

    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    iget v1, p0, Lnq;->nK:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lnq;->nq:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnq;->nr:Landroid/graphics/Paint;

    iget-object v0, p0, Lnq;->nr:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lnq;->nr:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lnq;->nr:Landroid/graphics/Paint;

    iget v1, p0, Lnq;->nN:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lnq;->nr:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lnq;->nr:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    iget-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    iget v1, p0, Lnq;->nL:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lnq;->ns:Landroid/graphics/Paint;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    sget v1, Lnq;->nj:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    iget v1, p0, Lnq;->nK:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    iget-object v1, p0, Lnq;->nn:Ljava/lang/String;

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lnq;->nt:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnq;->np:Landroid/graphics/Paint;

    iget-object v0, p0, Lnq;->np:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lnq;->np:Landroid/graphics/Paint;

    sget v1, Lnq;->nh:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lnq;->np:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lnq;->np:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lnq;->np:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 231
    return-void
.end method

.method static synthetic a(Lnq;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lnq;->ag(I)V

    return-void
.end method

.method private ag(I)V
    .locals 4

    .prologue
    .line 572
    iget v0, p0, Lnq;->nx:I

    iget v1, p0, Lnq;->nw:I

    invoke-virtual {p0, v0, v1, p1}, Lnq;->e(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    :goto_0
    return-void

    .line 577
    :cond_0
    iget-object v0, p0, Lnq;->nI:Lns;

    if-eqz v0, :cond_1

    .line 578
    iget-object v0, p0, Lnq;->nI:Lns;

    new-instance v1, Lnp;

    iget v2, p0, Lnq;->nx:I

    iget v3, p0, Lnq;->nw:I

    invoke-direct {v1, v2, v3, p1}, Lnp;-><init>(III)V

    invoke-interface {v0, v1}, Lns;->b(Lnp;)V

    .line 582
    :cond_1
    iget-object v0, p0, Lnq;->nG:Lnr;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lnr;->g(II)Z

    goto :goto_0
.end method

.method protected static cu()I
    .locals 1

    .prologue
    .line 440
    sget v0, Lnq;->nk:I

    return v0
.end method


# virtual methods
.method public abstract a(Landroid/graphics/Canvas;IIIII)V
.end method

.method public final a(Ljava/util/HashMap;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 342
    const-string v0, "month"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "year"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "You must specify month and year for this view"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_0
    invoke-virtual {p0, p1}, Lnq;->setTag(Ljava/lang/Object;)V

    .line 347
    const-string v0, "height"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    const-string v0, "height"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lnq;->nz:I

    .line 349
    iget v0, p0, Lnq;->nz:I

    sget v3, Lnq;->nf:I

    if-ge v0, v3, :cond_1

    .line 350
    sget v0, Lnq;->nf:I

    iput v0, p0, Lnq;->nz:I

    .line 353
    :cond_1
    const-string v0, "selected_day"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    const-string v0, "selected_day"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lnq;->nB:I

    .line 358
    :cond_2
    const-string v0, "month"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lnq;->nw:I

    .line 359
    const-string v0, "year"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lnq;->nx:I

    .line 362
    new-instance v4, Landroid/text/format/Time;

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 363
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 364
    iput-boolean v1, p0, Lnq;->nA:Z

    .line 365
    const/4 v0, -0x1

    iput v0, p0, Lnq;->nC:I

    .line 367
    iget-object v0, p0, Lnq;->ms:Ljava/util/Calendar;

    const/4 v3, 0x2

    iget v5, p0, Lnq;->nw:I

    invoke-virtual {v0, v3, v5}, Ljava/util/Calendar;->set(II)V

    .line 368
    iget-object v0, p0, Lnq;->ms:Ljava/util/Calendar;

    iget v3, p0, Lnq;->nx:I

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 369
    iget-object v0, p0, Lnq;->ms:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 370
    iget-object v0, p0, Lnq;->ms:Ljava/util/Calendar;

    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lnq;->nO:I

    .line 372
    const-string v0, "week_start"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 373
    const-string v0, "week_start"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lnq;->mF:I

    .line 378
    :goto_0
    iget v0, p0, Lnq;->nw:I

    iget v3, p0, Lnq;->nx:I

    invoke-static {v0, v3}, Lnf;->m(II)I

    move-result v0

    iput v0, p0, Lnq;->nE:I

    move v0, v1

    .line 379
    :goto_1
    iget v3, p0, Lnq;->nE:I

    if-ge v0, v3, :cond_6

    .line 380
    add-int/lit8 v5, v0, 0x1

    .line 381
    iget v3, p0, Lnq;->nx:I

    iget v6, v4, Landroid/text/format/Time;->year:I

    if-ne v3, v6, :cond_5

    iget v3, p0, Lnq;->nw:I

    iget v6, v4, Landroid/text/format/Time;->month:I

    if-ne v3, v6, :cond_5

    iget v3, v4, Landroid/text/format/Time;->monthDay:I

    if-ne v5, v3, :cond_5

    move v3, v2

    :goto_2
    if-eqz v3, :cond_3

    .line 382
    iput-boolean v2, p0, Lnq;->nA:Z

    .line 383
    iput v5, p0, Lnq;->nC:I

    .line 379
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 375
    :cond_4
    iget-object v0, p0, Lnq;->ms:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    iput v0, p0, Lnq;->mF:I

    goto :goto_0

    :cond_5
    move v3, v1

    .line 381
    goto :goto_2

    .line 386
    :cond_6
    invoke-virtual {p0}, Lnq;->cv()I

    move-result v0

    iget v3, p0, Lnq;->nE:I

    add-int/2addr v3, v0

    iget v4, p0, Lnq;->nD:I

    div-int/2addr v3, v4

    iget v4, p0, Lnq;->nE:I

    add-int/2addr v0, v4

    iget v4, p0, Lnq;->nD:I

    rem-int/2addr v0, v4

    if-lez v0, :cond_7

    move v1, v2

    :cond_7
    add-int v0, v3, v1

    iput v0, p0, Lnq;->nH:I

    .line 389
    iget-object v0, p0, Lnq;->nG:Lnr;

    invoke-virtual {v0}, Lnr;->bi()V

    .line 390
    return-void
.end method

.method public final a(Lng;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lnq;->mX:Lng;

    .line 235
    return-void
.end method

.method public final a(Lns;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lnq;->nI:Lns;

    .line 252
    return-void
.end method

.method public final ct()V
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x6

    iput v0, p0, Lnq;->nH:I

    .line 398
    invoke-virtual {p0}, Lnq;->requestLayout()V

    .line 399
    return-void
.end method

.method protected final cv()I
    .locals 2

    .prologue
    .line 521
    iget v0, p0, Lnq;->nO:I

    iget v1, p0, Lnq;->mF:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lnq;->nO:I

    iget v1, p0, Lnq;->nD:I

    add-int/2addr v0, v1

    :goto_0
    iget v1, p0, Lnq;->mF:I

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lnq;->nO:I

    goto :goto_0
.end method

.method public final cw()Lnp;
    .locals 4

    .prologue
    .line 661
    iget-object v0, p0, Lnq;->nG:Lnr;

    invoke-virtual {v0}, Lnr;->bj()I

    move-result v1

    .line 662
    if-ltz v1, :cond_0

    .line 663
    new-instance v0, Lnp;

    iget v2, p0, Lnq;->nx:I

    iget v3, p0, Lnq;->nw:I

    invoke-direct {v0, v2, v3, v1}, Lnp;-><init>(III)V

    .line 665
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cx()V
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lnq;->nG:Lnr;

    invoke-virtual {v0}, Lnr;->cy()V

    .line 674
    return-void
.end method

.method public final d(Lnp;)Z
    .locals 2

    .prologue
    .line 684
    iget v0, p1, Lnp;->year:I

    iget v1, p0, Lnq;->nx:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lnp;->month:I

    iget v1, p0, Lnq;->nw:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lnp;->nd:I

    iget v1, p0, Lnq;->nE:I

    if-le v0, v1, :cond_1

    .line 685
    :cond_0
    const/4 v0, 0x0

    .line 688
    :goto_0
    return v0

    .line 687
    :cond_1
    iget-object v0, p0, Lnq;->nG:Lnr;

    iget v1, p1, Lnp;->nd:I

    invoke-virtual {v0, v1}, Lnr;->ah(I)V

    .line 688
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lnq;->nG:Lnr;

    invoke-virtual {v0, p1}, Lnr;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    const/4 v0, 0x1

    .line 260
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final e(III)Z
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    .line 591
    iget-object v2, p0, Lnq;->mX:Lng;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lnq;->mX:Lng;

    invoke-interface {v2}, Lng;->cm()Ljava/util/Calendar;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge p1, v3, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    .line 597
    :cond_0
    :goto_1
    return v0

    .line 591
    :cond_1
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-gt p1, v3, :cond_3

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge p2, v3, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-gt p2, v3, :cond_3

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ge p3, v2, :cond_3

    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_0

    .line 593
    :cond_4
    iget-object v2, p0, Lnq;->mX:Lng;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lnq;->mX:Lng;

    invoke-interface {v2}, Lng;->cn()Ljava/util/Calendar;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-le p1, v3, :cond_5

    move v2, v0

    :goto_2
    if-nez v2, :cond_0

    move v0, v1

    .line 597
    goto :goto_1

    .line 593
    :cond_5
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-lt p1, v3, :cond_7

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-le p2, v3, :cond_6

    move v2, v0

    goto :goto_2

    :cond_6
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-lt p2, v3, :cond_7

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-le p3, v2, :cond_7

    move v2, v0

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_2
.end method

.method public final g(FF)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 534
    cmpg-float v0, p1, v3

    if-ltz v0, :cond_0

    iget v0, p0, Lnq;->ny:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    :cond_0
    move v0, v1

    .line 535
    :goto_0
    if-lez v0, :cond_1

    iget v2, p0, Lnq;->nE:I

    if-le v0, v2, :cond_2

    :cond_1
    move v0, v1

    .line 538
    :cond_2
    return v0

    .line 534
    :cond_3
    sget v0, Lnq;->nk:I

    int-to-float v0, v0

    sub-float v0, p2, v0

    float-to-int v0, v0

    iget v2, p0, Lnq;->nz:I

    div-int/2addr v0, v2

    sub-float v2, p1, v3

    iget v3, p0, Lnq;->nD:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lnq;->ny:I

    add-int/lit8 v3, v3, 0x0

    add-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {p0}, Lnq;->cv()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lnq;->nD:I

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v12, 0x7

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 324
    iget v0, p0, Lnq;->ny:I

    add-int/lit8 v0, v0, 0x0

    div-int/lit8 v10, v0, 0x2

    sget v0, Lnq;->nk:I

    sget v1, Lnq;->nj:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sget v1, Lnq;->ni:I

    div-int/lit8 v1, v1, 0x3

    add-int v11, v0, v1

    iget-object v0, p0, Lnq;->nv:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lnq;->ms:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0}, Lnq;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lnq;->nu:Ljava/util/Formatter;

    const/16 v6, 0x34

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v7

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v10

    int-to-float v2, v11

    iget-object v3, p0, Lnq;->nq:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 325
    sget v0, Lnq;->nk:I

    sget v1, Lnq;->nj:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    iget v0, p0, Lnq;->ny:I

    add-int/lit8 v0, v0, 0x0

    iget v2, p0, Lnq;->nD:I

    mul-int/lit8 v2, v2, 0x2

    div-int v2, v0, v2

    move v0, v8

    :goto_0
    iget v3, p0, Lnq;->nD:I

    if-ge v0, v3, :cond_0

    iget v3, p0, Lnq;->mF:I

    add-int/2addr v3, v0

    iget v4, p0, Lnq;->nD:I

    rem-int/2addr v3, v4

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v4, v2

    add-int/lit8 v4, v4, 0x0

    iget-object v5, p0, Lnq;->nF:Ljava/util/Calendar;

    invoke-virtual {v5, v12, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v3, p0, Lnq;->nF:Ljava/util/Calendar;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v12, v9, v5}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    int-to-float v4, v4

    int-to-float v5, v1

    iget-object v6, p0, Lnq;->nt:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    :cond_0
    iget v0, p0, Lnq;->nz:I

    sget v1, Lnq;->nh:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sget v1, Lnq;->ng:I

    sub-int/2addr v0, v1

    sget v1, Lnq;->nk:I

    add-int v6, v0, v1

    iget v0, p0, Lnq;->ny:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    iget v1, p0, Lnq;->nD:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    div-float v10, v0, v1

    invoke-virtual {p0}, Lnq;->cv()I

    move-result v0

    move v4, v9

    move v7, v0

    :goto_1
    iget v0, p0, Lnq;->nE:I

    if-gt v4, v0, :cond_2

    mul-int/lit8 v0, v7, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    mul-float/2addr v0, v10

    const/4 v1, 0x0

    add-float/2addr v0, v1

    float-to-int v5, v0

    iget v0, p0, Lnq;->nz:I

    sget v0, Lnq;->nh:I

    sget v0, Lnq;->ng:I

    iget v0, p0, Lnq;->nz:I

    iget v2, p0, Lnq;->nx:I

    iget v3, p0, Lnq;->nw:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lnq;->a(Landroid/graphics/Canvas;IIIII)V

    add-int/lit8 v0, v7, 0x1

    iget v1, p0, Lnq;->nD:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lnq;->nz:I

    add-int/2addr v6, v0

    move v0, v8

    :cond_1
    add-int/lit8 v4, v4, 0x1

    move v7, v0

    goto :goto_1

    .line 327
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 416
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lnq;->nz:I

    iget v2, p0, Lnq;->nH:I

    mul-int/2addr v1, v2

    sget v2, Lnq;->nk:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lnq;->setMeasuredDimension(II)V

    .line 418
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 422
    iput p1, p0, Lnq;->ny:I

    .line 425
    iget-object v0, p0, Lnq;->nG:Lnr;

    invoke-virtual {v0}, Lnr;->bi()V

    .line 426
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 273
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 267
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lnq;->g(FF)I

    move-result v0

    .line 268
    if-ltz v0, :cond_0

    .line 269
    invoke-direct {p0, v0}, Lnq;->ag(I)V

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lnq;->nJ:Z

    if-nez v0, :cond_0

    .line 246
    invoke-super {p0, p1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 248
    :cond_0
    return-void
.end method
