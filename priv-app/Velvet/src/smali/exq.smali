.class public final Lexq;
.super Lewj;
.source "PG"


# instance fields
.field ckE:Liwk;

.field private clt:Liwk;

.field private clu:Ljak;

.field private clv:Ljava/lang/String;

.field clw:Landroid/widget/EditText;

.field mEntry:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lewj;-><init>()V

    .line 354
    return-void
.end method

.method public static F(Landroid/content/Intent;)Lexq;
    .locals 5

    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 95
    const-string v0, "entry"

    const-class v1, Lizj;

    invoke-static {v2, v0, v1}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 96
    const-string v1, "action"

    const-class v3, Liwk;

    invoke-static {v2, v1, v3}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Liwk;

    .line 98
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 99
    :cond_0
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    .line 102
    :cond_1
    const-string v3, "delete_action"

    const-class v4, Liwk;

    invoke-static {v2, v3, v4}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v2

    check-cast v2, Liwk;

    .line 103
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "entry_key"

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v0, "rename_action_key"

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    if-eqz v2, :cond_2

    const-string v0, "delete_action_key"

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_2
    new-instance v0, Lexq;

    invoke-direct {v0}, Lexq;-><init>()V

    invoke-virtual {v0, v3}, Lexq;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method final a(Ljcu;)V
    .locals 5

    .prologue
    .line 206
    invoke-virtual {p0}, Lexq;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 208
    if-nez v0, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v1, "editPlaceWorkerFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 213
    iget-object v1, p0, Lexq;->mEntry:Lizj;

    iget-object v2, p0, Lexq;->clt:Liwk;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "entry_key"

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v1, "action_key"

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v1, "renamed_place_key"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Leya;

    invoke-direct {v1}, Leya;-><init>()V

    invoke-virtual {v1, v3}, Leya;->setArguments(Landroid/os/Bundle;)V

    .line 215
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "editPlaceWorkerFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "editPlaceWorkerFragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 108
    invoke-virtual {p0}, Lexq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 109
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v1

    iput-object v1, p0, Lexq;->mEntry:Lizj;

    .line 110
    const-string v1, "rename_action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->R([B)Liwk;

    move-result-object v1

    iput-object v1, p0, Lexq;->clt:Liwk;

    .line 111
    const-string v1, "delete_action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const-string v1, "delete_action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->R([B)Liwk;

    move-result-object v0

    iput-object v0, p0, Lexq;->ckE:Liwk;

    .line 117
    :cond_0
    iget-object v0, p0, Lexq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lexq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lexq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iput-object v0, p0, Lexq;->clu:Ljak;

    iget-object v0, p0, Lexq;->clu:Ljak;

    iget-object v0, v0, Ljak;->clz:Ljcu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lexq;->clu:Ljak;

    iget-object v0, v0, Ljak;->clz:Ljcu;

    invoke-virtual {v0}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexq;->clv:Ljava/lang/String;

    .line 119
    :goto_0
    iget-object v0, p0, Lexq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    if-nez v0, :cond_5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    .line 120
    :goto_1
    new-instance v1, Lexx;

    invoke-virtual {p0}, Lexq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, p0, v0, v2}, Lexx;-><init>(Lexq;Landroid/content/Context;Ljava/util/List;)V

    .line 122
    invoke-virtual {p0}, Lexq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f040155

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 125
    const v0, 0x7f1103ab

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 126
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    const v1, 0x7f110076

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lexq;->clw:Landroid/widget/EditText;

    .line 129
    if-eqz p1, :cond_8

    const-string v1, "place_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 130
    iget-object v1, p0, Lexq;->clw:Landroid/widget/EditText;

    const-string v4, "place_name"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_2
    iget-object v1, p0, Lexq;->clw:Landroid/widget/EditText;

    const/16 v4, 0x2000

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 136
    new-instance v1, Lexl;

    invoke-virtual {p0}, Lexq;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0}, Lexq;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const v6, 0x7f0a033b

    invoke-direct {v1, v4, v5, v6}, Lexl;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;I)V

    .line 138
    invoke-virtual {v1, v3}, Lexl;->setView(Landroid/view/View;)V

    .line 139
    const/high16 v3, 0x1040000

    new-instance v4, Lexr;

    invoke-direct {v4, p0, v1}, Lexr;-><init>(Lexq;Lexl;)V

    invoke-virtual {v1, v3, v4}, Lexl;->c(ILandroid/view/View$OnClickListener;)V

    .line 146
    const v3, 0x104000a

    new-instance v4, Lexs;

    invoke-direct {v4, p0, v1}, Lexs;-><init>(Lexq;Lexl;)V

    invoke-virtual {v1, v3, v4}, Lexl;->a(ILandroid/view/View$OnClickListener;)V

    .line 157
    iget-object v3, p0, Lexq;->ckE:Liwk;

    if-eqz v3, :cond_1

    .line 158
    const v3, 0x7f0a033d

    new-instance v4, Lext;

    invoke-direct {v4, p0, v1}, Lext;-><init>(Lexq;Lexl;)V

    invoke-virtual {v1, v3, v4}, Lexl;->b(ILandroid/view/View$OnClickListener;)V

    .line 172
    :cond_1
    new-instance v3, Lexu;

    invoke-direct {v3, p0, v2, v1}, Lexu;-><init>(Lexq;Ljava/util/List;Lexl;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    new-instance v0, Lexv;

    invoke-direct {v0, p0}, Lexv;-><init>(Lexq;)V

    invoke-virtual {v1, v0}, Lexl;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 193
    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Lexl;->ir(I)V

    .line 195
    return-object v1

    .line 117
    :cond_2
    iget-object v0, p0, Lexq;->clu:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lexq;->clu:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexq;->clv:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    const-string v0, ""

    iput-object v0, p0, Lexq;->clv:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljak;

    invoke-direct {v0}, Ljak;-><init>()V

    iput-object v0, p0, Lexq;->clu:Ljak;

    const-string v0, ""

    iput-object v0, p0, Lexq;->clv:Ljava/lang/String;

    goto/16 :goto_0

    .line 119
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lexq;->clu:Ljak;

    iget-object v1, v1, Ljak;->dWH:[Ljcu;

    array-length v1, v1

    if-lez v1, :cond_7

    iget-object v1, p0, Lexq;->clu:Ljak;

    iget-object v2, v1, Ljak;->dWH:[Ljcu;

    array-length v4, v2

    move v1, v3

    :goto_3
    if-ge v1, v4, :cond_7

    aget-object v5, v2, v1

    invoke-virtual {v5}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lexq;->clv:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v6, Lexw;

    invoke-direct {v6, v5}, Lexw;-><init>(Ljcu;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    move-object v2, v0

    goto/16 :goto_1

    .line 132
    :cond_8
    iget-object v1, p0, Lexq;->clw:Landroid/widget/EditText;

    invoke-virtual {p0}, Lexq;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lexq;->clu:Ljak;

    invoke-static {v4, v5}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 200
    const-string v0, "place_name"

    iget-object v1, p0, Lexq;->clw:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-super {p0, p1}, Lewj;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 202
    return-void
.end method
