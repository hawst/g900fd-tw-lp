.class public final Lgah;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mActivityHelper:Lfzw;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfzw;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lgah;->mContext:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lgah;->mActivityHelper:Lfzw;

    .line 31
    return-void
.end method

.method private a(Ljava/lang/String;Ljbp;[Ljbp;Lgba;)Landroid/net/Uri;
    .locals 5
    .param p3    # [Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lgba;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 133
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    :try_start_0
    const-string v0, "google.navigation:title="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-static {p1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {p2}, Ljbp;->nH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljbp;->nI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "&ll="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lgah;->c(Ljbp;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    const-string v0, "&mode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Lgba;->aEe()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&entry=r"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    if-eqz p3, :cond_2

    .line 154
    array-length v2, p3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p3, v0

    .line 155
    const-string v4, "&altvia="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Lgah;->c(Ljbp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 146
    :cond_1
    sget-object v0, Lgba;->cEz:Lgba;

    invoke-virtual {v0}, Lgba;->aEe()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {p2}, Ljbp;->bfh()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161
    const-string v0, "&token="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljbp;->bfg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    return-object v0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    const-string v1, "DirectionsLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Encoding Error while attempting to encode location label: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 167
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Liyg;)Lgba;
    .locals 1
    .param p1    # Liyg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 64
    invoke-static {p1}, Lgae;->a(Liyg;)Ljava/lang/Integer;

    move-result-object v0

    .line 65
    if-nez v0, :cond_0

    .line 66
    invoke-static {p1}, Lgae;->b(Liyg;)I

    move-result v0

    .line 67
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->jc(I)Ljava/lang/Integer;

    move-result-object v0

    .line 69
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lgba;->jQ(I)Lgba;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lgba;)Z
    .locals 1
    .param p0    # Lgba;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77
    sget-object v0, Lgba;->cEB:Lgba;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljbp;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljbp;->mR()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljbp;->mS()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljbp;[Ljbp;Lgba;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # [Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-virtual/range {p0 .. p5}, Lgah;->b(Ljbp;[Ljbp;Lgba;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 49
    if-nez v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v1, p0, Lgah;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lgah;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0653

    invoke-virtual {v1, v2, v0, v3}, Lfzw;->a(Landroid/content/Context;Landroid/content/Intent;I)Z

    goto :goto_0
.end method

.method public final b(Ljbp;[Ljbp;Lgba;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p2    # [Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lgba;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p1}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {p1}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 100
    :cond_0
    invoke-static {p3}, Lgah;->a(Lgba;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0, p1, p2, p3}, Lgah;->a(Ljava/lang/String;Ljbp;[Ljbp;Lgba;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_3

    const-string v0, "DirectionsLauncher"

    const-string v1, "uri was null when try to launch navigation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x1

    invoke-static {p1, p3, p4, v0, p5}, Lgaz;->a(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lgah;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lgah;->mActivityHelper:Lfzw;

    iget-boolean v2, v2, Lfzw;->cEb:Z

    invoke-static {v1, v0, v2}, Lgaz;->a(Landroid/content/Context;Landroid/content/Intent;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method
