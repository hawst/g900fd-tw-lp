.class final Liky;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private dDE:Ljava/util/Iterator;

.field private dDF:Ljava/util/Iterator;

.field private synthetic dDu:Ljava/lang/Iterable;


# direct methods
.method constructor <init>(Ljava/lang/Iterable;)V
    .locals 1

    .prologue
    .line 402
    iput-object p1, p0, Liky;->dDu:Ljava/lang/Iterable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    invoke-static {}, Likr;->aXh()Lirv;

    move-result-object v0

    iput-object v0, p0, Liky;->dDE:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Liky;->dDE:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 409
    iget-object v0, p0, Liky;->dDu:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Liky;->dDE:Ljava/util/Iterator;

    .line 411
    :cond_0
    iget-object v0, p0, Liky;->dDE:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p0}, Liky;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 418
    :cond_0
    iget-object v0, p0, Liky;->dDE:Ljava/util/Iterator;

    iput-object v0, p0, Liky;->dDF:Ljava/util/Iterator;

    .line 419
    iget-object v0, p0, Liky;->dDE:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Liky;->dDF:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "no calls to next() since last call to remove()"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 425
    iget-object v0, p0, Liky;->dDF:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Liky;->dDF:Ljava/util/Iterator;

    .line 427
    return-void

    .line 423
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
