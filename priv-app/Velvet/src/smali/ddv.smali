.class public final Lddv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final buq:Ljava/util/List;

.field private final mClock:Lemp;

.field private final mGsaConfig:Lchk;


# direct methods
.method public constructor <init>(Lchk;Lemp;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lddv;->buq:Ljava/util/List;

    .line 32
    iput-object p1, p0, Lddv;->mGsaConfig:Lchk;

    .line 33
    iput-object p2, p0, Lddv;->mClock:Lemp;

    .line 34
    return-void
.end method

.method private static g(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final e(Lcom/google/android/shared/search/Suggestion;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lddv;->buq:Ljava/util/List;

    iget-object v1, p0, Lddv;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public final f(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lddv;->buq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 53
    :goto_0
    return v0

    .line 47
    :cond_0
    iget-object v0, p0, Lddv;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lddv;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->IM()I

    move-result v0

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iget-object v0, p0, Lddv;->buq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v6, v2

    if-gtz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 48
    :cond_2
    iget-object v0, p0, Lddv;->buq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 49
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    invoke-static {p1}, Lddv;->g(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v0}, Lddv;->g(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    :goto_2
    if-eqz v0, :cond_3

    .line 50
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v0}, Lcom/google/android/shared/search/Suggestion;->s(Lcom/google/android/shared/search/Suggestion;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/shared/search/Suggestion;->s(Lcom/google/android/shared/search/Suggestion;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v1

    .line 53
    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lddv;->buq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method
