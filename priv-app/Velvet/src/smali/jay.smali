.class public final Ljay;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dXw:[Ljay;


# instance fields
.field private dQj:Ljhe;

.field public dXx:Ljhg;

.field public dXy:[Ljhg;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50014
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50015
    iput-object v1, p0, Ljay;->dXx:Ljhg;

    invoke-static {}, Ljhg;->blD()[Ljhg;

    move-result-object v0

    iput-object v0, p0, Ljay;->dXy:[Ljhg;

    iput-object v1, p0, Ljay;->dQj:Ljhe;

    iput-object v1, p0, Ljay;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljay;->eCz:I

    .line 50016
    return-void
.end method

.method public static beF()[Ljay;
    .locals 2

    .prologue
    .line 49994
    sget-object v0, Ljay;->dXw:[Ljay;

    if-nez v0, :cond_1

    .line 49995
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 49997
    :try_start_0
    sget-object v0, Ljay;->dXw:[Ljay;

    if-nez v0, :cond_0

    .line 49998
    const/4 v0, 0x0

    new-array v0, v0, [Ljay;

    sput-object v0, Ljay;->dXw:[Ljay;

    .line 50000
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50002
    :cond_1
    sget-object v0, Ljay;->dXw:[Ljay;

    return-object v0

    .line 50000
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49988
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljay;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljay;->dXx:Ljhg;

    if-nez v0, :cond_1

    new-instance v0, Ljhg;

    invoke-direct {v0}, Ljhg;-><init>()V

    iput-object v0, p0, Ljay;->dXx:Ljhg;

    :cond_1
    iget-object v0, p0, Ljay;->dXx:Ljhg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljay;->dXy:[Ljhg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhg;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljay;->dXy:[Ljhg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljay;->dXy:[Ljhg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljay;->dXy:[Ljhg;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljay;->dQj:Ljhe;

    if-nez v0, :cond_5

    new-instance v0, Ljhe;

    invoke-direct {v0}, Ljhe;-><init>()V

    iput-object v0, p0, Ljay;->dQj:Ljhe;

    :cond_5
    iget-object v0, p0, Ljay;->dQj:Ljhe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 50030
    iget-object v0, p0, Ljay;->dXx:Ljhg;

    if-eqz v0, :cond_0

    .line 50031
    const/4 v0, 0x1

    iget-object v1, p0, Ljay;->dXx:Ljhg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 50033
    :cond_0
    iget-object v0, p0, Ljay;->dXy:[Ljhg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljay;->dXy:[Ljhg;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 50034
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljay;->dXy:[Ljhg;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 50035
    iget-object v1, p0, Ljay;->dXy:[Ljhg;

    aget-object v1, v1, v0

    .line 50036
    if-eqz v1, :cond_1

    .line 50037
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 50034
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50041
    :cond_2
    iget-object v0, p0, Ljay;->dQj:Ljhe;

    if-eqz v0, :cond_3

    .line 50042
    const/4 v0, 0x3

    iget-object v1, p0, Ljay;->dQj:Ljhe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 50044
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 50045
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 50049
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 50050
    iget-object v1, p0, Ljay;->dXx:Ljhg;

    if-eqz v1, :cond_0

    .line 50051
    const/4 v1, 0x1

    iget-object v2, p0, Ljay;->dXx:Ljhg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50054
    :cond_0
    iget-object v1, p0, Ljay;->dXy:[Ljhg;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljay;->dXy:[Ljhg;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 50055
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljay;->dXy:[Ljhg;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 50056
    iget-object v2, p0, Ljay;->dXy:[Ljhg;

    aget-object v2, v2, v0

    .line 50057
    if-eqz v2, :cond_1

    .line 50058
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 50055
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 50063
    :cond_3
    iget-object v1, p0, Ljay;->dQj:Ljhe;

    if-eqz v1, :cond_4

    .line 50064
    const/4 v1, 0x3

    iget-object v2, p0, Ljay;->dQj:Ljhe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50067
    :cond_4
    return v0
.end method
