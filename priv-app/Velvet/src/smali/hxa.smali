.class public final Lhxa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field cwm:I

.field private final dtJ:Lhxb;

.field private final dtK:Lhxc;

.field private final dtL:Ljava/util/List;

.field private dtM:Ljvi;

.field private final dtN:Ljava/util/List;

.field dtO:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field dtP:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mClock:Lemp;

.field mRequestId:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lemp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Lchk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhxa;->dtN:Ljava/util/List;

    .line 186
    iput-object p1, p0, Lhxa;->mClock:Lemp;

    .line 187
    new-instance v1, Lhxb;

    invoke-virtual/range {p9 .. p9}, Lchk;->Ft()[I

    move-result-object v9

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v9}, Lhxb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;[I)V

    iput-object v1, p0, Lhxa;->dtJ:Lhxb;

    .line 190
    move-object/from16 v0, p10

    iput-object v0, p0, Lhxa;->dtO:Ljava/lang/String;

    .line 191
    move-object/from16 v0, p11

    iput-object v0, p0, Lhxa;->dtP:Ljava/lang/String;

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhxa;->dtL:Ljava/util/List;

    .line 194
    invoke-direct {p0}, Lhxa;->aTn()V

    .line 196
    new-instance v1, Lhxg;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lhxg;-><init>(Lhxa;Lhxc;)V

    .line 203
    new-instance v2, Lhxd;

    invoke-direct {v2, p0, v1}, Lhxd;-><init>(Lhxa;Lhxc;)V

    .line 204
    new-instance v1, Lhxh;

    invoke-direct {v1, p0, v2}, Lhxh;-><init>(Lhxa;Lhxc;)V

    .line 205
    new-instance v2, Lhxe;

    invoke-direct {v2, p0, v1}, Lhxe;-><init>(Lhxa;Lhxc;)V

    .line 207
    iput-object v2, p0, Lhxa;->dtK:Lhxc;

    .line 208
    return-void
.end method

.method private aTn()V
    .locals 5

    .prologue
    .line 215
    iget-object v0, p0, Lhxa;->dtN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 216
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Android"

    sget-object v2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    new-instance v3, Ljvi;

    invoke-direct {v3}, Ljvi;-><init>()V

    iget-object v4, p0, Lhxa;->dtJ:Lhxb;

    iget-object v4, v4, Lhxb;->dtQ:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljvi;->zl(Ljava/lang/String;)Ljvi;

    iget-object v4, p0, Lhxa;->dtJ:Lhxb;

    iget-object v4, v4, Lhxb;->dtR:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljvi;->zm(Ljava/lang/String;)Ljvi;

    invoke-virtual {v3, v0}, Ljvi;->zi(Ljava/lang/String;)Ljvi;

    iget-object v0, p0, Lhxa;->dtJ:Lhxb;

    iget-object v0, v0, Lhxb;->dtS:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljvi;->ze(Ljava/lang/String;)Ljvi;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljvi;->zn(Ljava/lang/String;)Ljvi;

    invoke-virtual {v3, v1}, Ljvi;->zg(Ljava/lang/String;)Ljvi;

    invoke-virtual {v3, v2}, Ljvi;->zh(Ljava/lang/String;)Ljvi;

    iget-object v0, p0, Lhxa;->dtJ:Lhxb;

    iget-object v0, v0, Lhxb;->RO:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljvi;->zf(Ljava/lang/String;)Ljvi;

    iget-object v0, p0, Lhxa;->dtJ:Lhxb;

    iget v0, v0, Lhxb;->dtT:I

    invoke-virtual {v3, v0}, Ljvi;->sy(I)Ljvi;

    iget-object v0, p0, Lhxa;->dtJ:Lhxb;

    iget v0, v0, Lhxb;->dtU:I

    invoke-virtual {v3, v0}, Ljvi;->sz(I)Ljvi;

    iget-object v0, p0, Lhxa;->dtJ:Lhxb;

    iget-object v0, v0, Lhxb;->dtV:Ljava/util/List;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Ljvi;->eHq:[Ljava/lang/String;

    iget-object v0, p0, Lhxa;->dtJ:Lhxb;

    iget-object v0, v0, Lhxb;->dtW:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, v3, Ljvi;->eHr:[I

    iput-object v3, p0, Lhxa;->dtM:Ljvi;

    .line 217
    return-void
.end method


# virtual methods
.method public final a(Legp;)Ljava/util/List;
    .locals 8

    .prologue
    .line 234
    new-instance v2, Lhxf;

    invoke-direct {v2, p0}, Lhxf;-><init>(Lhxa;)V

    .line 235
    iget-object v0, p0, Lhxa;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lhxa;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v0, v4

    .line 237
    invoke-interface {p1}, Legp;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 239
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Legp;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 241
    invoke-interface {p1, v1}, Legp;->hv(I)I

    move-result v0

    invoke-interface {p1, v1}, Legp;->hw(I)J

    move-result-wide v6

    add-long/2addr v6, v4

    invoke-interface {p1, v1}, Legp;->hx(I)Legh;

    move-result-object v3

    iput v0, v2, Lhxf;->type:I

    iput-wide v6, v2, Lhxf;->timestamp:J

    iput-object v3, v2, Lhxf;->data:Ljava/lang/Object;

    .line 243
    iget-object v0, v2, Lhxf;->data:Ljava/lang/Object;

    instance-of v0, v0, Lega;

    if-eqz v0, :cond_2

    .line 244
    iget-object v0, v2, Lhxf;->data:Ljava/lang/Object;

    check-cast v0, Lega;

    iget-object v0, v0, Lega;->bYe:Litu;

    .line 245
    iget-wide v6, v2, Lhxf;->timestamp:J

    invoke-virtual {v0, v6, v7}, Litu;->bV(J)Litu;

    .line 246
    invoke-virtual {v0}, Litu;->aYY()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 247
    invoke-virtual {v0}, Litu;->aYX()J

    move-result-wide v6

    .line 248
    add-long/2addr v6, v4

    invoke-virtual {v0, v6, v7}, Litu;->bX(J)Litu;

    .line 256
    :cond_0
    :goto_1
    iget-object v3, p0, Lhxa;->dtK:Lhxc;

    invoke-virtual {v3, v2, v0}, Lhxc;->a(Lhxf;Litu;)Z

    move-result v0

    .line 257
    if-nez v0, :cond_1

    .line 258
    const-string v0, "ClientLogsBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Failed to process event: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 251
    :cond_2
    new-instance v0, Litu;

    invoke-direct {v0}, Litu;-><init>()V

    goto :goto_1

    .line 261
    :cond_3
    invoke-virtual {p0}, Lhxa;->aTo()V

    .line 262
    iget-object v0, p0, Lhxa;->dtO:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhxa;->ot(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lhxa;->dtP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhxa;->ou(Ljava/lang/String;)V

    .line 265
    :cond_4
    iget-object v0, p0, Lhxa;->dtL:Ljava/util/List;

    return-object v0
.end method

.method final aTo()V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lhxa;->dtN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v1, p0, Lhxa;->dtM:Ljvi;

    iget-object v0, p0, Lhxa;->dtN:Ljava/util/List;

    const-class v2, Litu;

    invoke-static {v0, v2}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Litu;

    iput-object v0, v1, Ljvi;->eHp:[Litu;

    .line 286
    iget-object v0, p0, Lhxa;->dtL:Ljava/util/List;

    new-instance v1, Lhxm;

    iget-object v2, p0, Lhxa;->dtM:Ljvi;

    invoke-direct {v1, v2}, Lhxm;-><init>(Ljvi;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_0
    invoke-direct {p0}, Lhxa;->aTn()V

    .line 289
    return-void
.end method

.method final aZ(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 342
    invoke-virtual {p0}, Lhxa;->aTo()V

    .line 343
    if-eqz p1, :cond_0

    .line 344
    iget-object v0, p0, Lhxa;->dtM:Ljvi;

    invoke-virtual {v0, p1}, Ljvi;->zj(Ljava/lang/String;)Ljvi;

    .line 346
    :cond_0
    if-eqz p2, :cond_1

    .line 347
    iget-object v0, p0, Lhxa;->dtM:Ljvi;

    invoke-virtual {v0, p2}, Ljvi;->zk(Ljava/lang/String;)Ljvi;

    .line 349
    :cond_1
    return-void
.end method

.method final b(Litu;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lhxa;->dtN:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    return-void
.end method

.method final ot(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298
    iget-object v0, p0, Lhxa;->dtL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxm;

    .line 299
    iget-boolean v2, v0, Lhxm;->duh:Z

    if-nez v2, :cond_0

    .line 300
    iput-object p1, v0, Lhxm;->bbH:Ljava/lang/String;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lhxm;->duh:Z

    goto :goto_0

    .line 303
    :cond_1
    return-void
.end method

.method final ou(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311
    iget-object v0, p0, Lhxa;->dtL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxm;

    .line 312
    iget-boolean v2, v0, Lhxm;->dui:Z

    if-nez v2, :cond_0

    .line 313
    iput-object p1, v0, Lhxm;->dug:Ljava/lang/String;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lhxm;->dui:Z

    goto :goto_0

    .line 316
    :cond_1
    return-void
.end method
