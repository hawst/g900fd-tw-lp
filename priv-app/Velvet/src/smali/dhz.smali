.class public final Ldhz;
.super Ldei;
.source "PG"


# instance fields
.field private final aVQ:Ldgc;

.field private final byr:Ljava/util/Map;

.field private final byv:Lbci;

.field private byw:Ljava/lang/Integer;

.field private final byx:Ljava/util/List;


# direct methods
.method private constructor <init>(Ldgc;Ljava/util/Collection;Ljava/lang/String;Lcom/google/android/shared/search/Query;Lbci;)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0, p3, p4}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 50
    iput-object p1, p0, Ldhz;->aVQ:Ldgc;

    .line 51
    iput-object p5, p0, Ldhz;->byv:Lbci;

    .line 52
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ldhz;->byr:Ljava/util/Map;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldhz;->byx:Ljava/util/List;

    .line 55
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 56
    iget-object v2, p0, Ldhz;->byr:Ljava/util/Map;

    invoke-virtual {v0}, Ldht;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method static a(Ldgc;Ldht;Lcom/google/android/shared/search/Query;Lbci;)Ldhz;
    .locals 6

    .prologue
    .line 76
    new-instance v0, Ldhz;

    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p1}, Ldht;->getName()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ldhz;-><init>(Ldgc;Ljava/util/Collection;Ljava/lang/String;Lcom/google/android/shared/search/Query;Lbci;)V

    return-object v0
.end method

.method static a(Ldgc;Ljava/util/Collection;Lcom/google/android/shared/search/Query;Lbci;)Ldhz;
    .locals 6

    .prologue
    .line 63
    new-instance v0, Ldhz;

    const-string v3, "mixed-icing-results"

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ldhz;-><init>(Ldgc;Ljava/util/Collection;Ljava/lang/String;Lcom/google/android/shared/search/Query;Lbci;)V

    return-object v0
.end method

.method private b(Lbch;)Ldht;
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Ldhz;->aVQ:Ldgc;

    invoke-virtual {v0, p1}, Ldgc;->a(Lbch;)Ljava/lang/String;

    move-result-object v1

    .line 137
    iget-object v0, p0, Ldhz;->byr:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find source for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", list source name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldei;->bvl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", existng sources = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",\n"

    iget-object v3, p0, Ldhz;->byr:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-object v0
.end method

.method private fS(I)V
    .locals 8

    .prologue
    .line 105
    iget-object v0, p0, Ldhz;->byx:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v2, v0

    .line 106
    :goto_0
    if-gt v2, p1, :cond_9

    .line 107
    iget-object v3, p0, Ldhz;->byx:Ljava/util/List;

    iget-object v0, p0, Ldhz;->byv:Lbci;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldhz;->byv:Lbci;

    invoke-virtual {v0}, Lbci;->hasNext()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Ldhz;->byv:Lbci;

    invoke-virtual {v0}, Lbci;->xj()Lbch;

    move-result-object v4

    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    invoke-direct {p0, v4}, Ldhz;->b(Lbch;)Ldht;

    move-result-object v1

    invoke-virtual {v0, v1}, Lded;->h(Ldgb;)Lded;

    move-result-object v5

    const-string v0, "text1"

    invoke-virtual {v4, v0}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lded;->bux:Ljava/lang/CharSequence;

    const-string v0, "text2"

    invoke-virtual {v4, v0}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lded;->buy:Ljava/lang/CharSequence;

    const-string v0, "intent_action"

    invoke-virtual {v4, v0}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, v4}, Ldhz;->b(Lbch;)Ldht;

    move-result-object v0

    invoke-virtual {v0}, Ldht;->abJ()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, v5, Lded;->buE:Ljava/lang/String;

    const-string v0, "intent_data"

    invoke-virtual {v4, v0}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0, v4}, Ldhz;->b(Lbch;)Ldht;

    move-result-object v0

    invoke-virtual {v0}, Ldht;->abK()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "intent_data_id"

    invoke-virtual {v4, v1}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    iput-object v0, v5, Lded;->buF:Ljava/lang/String;

    const-string v0, "intent_extra_data"

    invoke-virtual {v4, v0}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lded;->buG:Ljava/lang/String;

    const-string v0, "intent_activity"

    invoke-virtual {v4, v0}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-direct {p0, v4}, Ldhz;->b(Lbch;)Ldht;

    move-result-object v0

    invoke-virtual {v0}, Ldht;->aca()Ljava/lang/String;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    :cond_4
    const/4 v0, 0x0

    :goto_1
    iput-object v0, v5, Lded;->buI:Landroid/content/ComponentName;

    invoke-virtual {v4}, Lbch;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "icon"

    invoke-virtual {v4, v1}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lesp;->aG(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lded;->buA:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, v5, Lded;->buO:Z

    sget-object v0, Lcgg;->aVi:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcgg;->aVj:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_5
    invoke-direct {p0, v4}, Ldhz;->b(Lbch;)Ldht;

    move-result-object v0

    invoke-virtual {v4}, Lbch;->xg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldht;->jE(Ljava/lang/String;)Z

    move-result v0

    :goto_2
    iput-boolean v0, v5, Lded;->buY:Z

    invoke-virtual {v4}, Lbch;->xg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lded;->bvb:Ljava/lang/String;

    invoke-virtual {v4}, Lbch;->getUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lded;->bvc:Ljava/lang/String;

    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-virtual {v4}, Lbch;->xi()D

    move-result-wide v6

    mul-double/2addr v0, v6

    double-to-long v0, v0

    iput-wide v0, v5, Lded;->bjh:J

    invoke-virtual {v5}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 107
    :cond_6
    invoke-direct {p0, v4}, Ldhz;->b(Lbch;)Ldht;

    move-result-object v1

    invoke-virtual {v1}, Ldht;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_7
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v6, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 110
    :cond_9
    return-void
.end method


# virtual methods
.method public final abe()Ljava/util/List;
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Ldhz;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Ldhz;->fS(I)V

    .line 199
    iget-object v0, p0, Ldhz;->byx:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final fJ(I)Lcom/google/android/shared/search/Suggestion;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Ldhz;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 100
    invoke-direct {p0, p1}, Ldhz;->fS(I)V

    .line 101
    iget-object v0, p0, Ldhz;->byx:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    return-object v0

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldhz;->byw:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Ldhz;->byv:Lbci;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhz;->byw:Ljava/lang/Integer;

    .line 91
    :cond_0
    iget-object v0, p0, Ldhz;->byw:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 89
    :cond_1
    iget-object v0, p0, Ldhz;->byv:Lbci;

    invoke-virtual {v0}, Lbci;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 193
    new-instance v0, Ldia;

    invoke-direct {v0, p0}, Ldia;-><init>(Ldhz;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IcingSuggestionList["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldei;->bvl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldhz;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
