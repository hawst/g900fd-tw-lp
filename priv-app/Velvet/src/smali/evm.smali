.class public final Levm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cjP:Lfdo;

.field public cjQ:[B

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mLocationOracle:Lfdr;

.field private final mWidgetManager:Lfjo;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemp;Lfdr;Lfjo;Leue;Landroid/net/wifi/WifiManager;Lfdo;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, Lifq;->aVX()Lifq;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Levm;->cjQ:[B

    .line 71
    iput-object p1, p0, Levm;->mContext:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Levm;->mClock:Lemp;

    .line 73
    iput-object p3, p0, Levm;->mLocationOracle:Lfdr;

    .line 74
    iput-object p4, p0, Levm;->mWidgetManager:Lfjo;

    .line 75
    iput-object p5, p0, Levm;->mGmsLocationReportingHelper:Leue;

    .line 76
    iput-object p6, p0, Levm;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 77
    iput-object p7, p0, Levm;->cjP:Lfdo;

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Ljej;Z)Ljej;
    .locals 12
    .param p1    # Ljej;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 92
    invoke-static {}, Lenu;->auQ()V

    .line 94
    if-nez p1, :cond_0

    new-instance p1, Ljej;

    invoke-direct {p1}, Ljej;-><init>()V

    .line 96
    :cond_0
    iget-object v0, p0, Levm;->mGmsLocationReportingHelper:Leue;

    invoke-virtual {v0}, Leue;->avX()Lcgs;

    move-result-object v4

    .line 100
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljej;->ue(Ljava/lang/String;)Ljej;

    .line 103
    iget-object v0, p0, Levm;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    .line 104
    div-long v8, v6, v10

    invoke-virtual {p1, v8, v9}, Ljej;->da(J)Ljej;

    .line 105
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-virtual {p1, v0}, Ljej;->pi(I)Ljej;

    .line 107
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Ljej;->uf(Ljava/lang/String;)Ljej;

    .line 111
    :cond_1
    iget-object v0, p0, Levm;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Ljej;->pk(I)Ljej;

    .line 116
    if-eqz p2, :cond_2

    .line 117
    invoke-virtual {p0}, Levm;->awd()Ljava/util/List;

    move-result-object v0

    .line 118
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljhm;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljhm;

    iput-object v0, p1, Ljej;->eew:[Ljhm;

    .line 121
    iget-object v0, p0, Levm;->cjP:Lfdo;

    invoke-virtual {v0}, Lfdo;->ayx()Ljava/util/List;

    move-result-object v0

    .line 122
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Liyy;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liyy;

    iput-object v0, p1, Ljej;->amc:[Liyy;

    .line 127
    :cond_2
    iget-object v0, p0, Levm;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    const/4 v5, 0x3

    if-eq v0, v5, :cond_3

    if-ne v0, v1, :cond_9

    :cond_3
    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Ljej;->hP(Z)Ljej;

    .line 128
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_4

    .line 130
    :try_start_0
    iget-object v0, p0, Levm;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljej;->hQ(Z)Ljej;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :cond_4
    :goto_2
    invoke-virtual {p1, v3}, Ljej;->hR(Z)Ljej;

    .line 144
    iget-object v0, p0, Levm;->mWidgetManager:Lfjo;

    invoke-virtual {v0}, Lfjo;->aAu()I

    move-result v0

    invoke-virtual {p1, v0}, Ljej;->pj(I)Ljej;

    .line 147
    iget-object v0, p0, Levm;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 149
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 150
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 155
    iget-object v0, p0, Levm;->mContext:Landroid/content/Context;

    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v7}, Leot;->e(Landroid/content/Context;I)I

    move-result v0

    .line 156
    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v7, v0

    .line 160
    const v7, 0x7f0c005d

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 161
    if-le v7, v2, :cond_5

    .line 162
    const v2, 0x7f0d00bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 163
    add-int/lit8 v2, v7, -0x1

    mul-int/2addr v1, v2

    .line 164
    sub-int/2addr v0, v1

    div-int/2addr v0, v7

    .line 167
    :cond_5
    new-instance v1, Ljbn;

    invoke-direct {v1}, Ljbn;-><init>()V

    invoke-virtual {v1, v5}, Ljbn;->oo(I)Ljbn;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljbn;->op(I)Ljbn;

    move-result-object v1

    iget v2, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v1, v2}, Ljbn;->U(F)Ljbn;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljbn;->or(I)Ljbn;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljbn;->oq(I)Ljbn;

    move-result-object v0

    iput-object v0, p1, Ljej;->eeB:Ljbn;

    .line 175
    iget-object v0, p0, Levm;->cjQ:[B

    if-eqz v0, :cond_6

    .line 176
    iget-object v0, p0, Levm;->cjQ:[B

    invoke-virtual {p1, v0}, Ljej;->ak([B)Ljej;

    .line 179
    :cond_6
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v10, v11, v0}, Lcgs;->c(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/ReportingState;

    .line 181
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->Aw()Z

    move-result v1

    if-nez v1, :cond_7

    .line 182
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->At()I

    move-result v0

    invoke-static {v0}, Lbsd;->ee(I)Z

    move-result v0

    .line 183
    invoke-virtual {p1, v0}, Ljej;->hS(Z)Ljej;

    .line 186
    :cond_7
    return-object p1

    :cond_8
    move v0, v2

    .line 111
    goto/16 :goto_0

    :cond_9
    move v0, v3

    .line 127
    goto/16 :goto_1

    .line 131
    :catch_0
    move-exception v0

    .line 133
    const-string v1, "SensorSignalsOracle"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2
.end method

.method public final awd()Ljava/util/List;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Levm;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->ayz()Ljava/util/List;

    move-result-object v0

    .line 193
    invoke-static {v0}, Lgay;->ac(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
