.class public Lep;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final ft:Les;

.field private static final fu:Ljava/lang/Object;


# instance fields
.field private fv:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 295
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 296
    new-instance v0, Let;

    invoke-direct {v0}, Let;-><init>()V

    sput-object v0, Lep;->ft:Les;

    .line 302
    :goto_0
    sget-object v0, Lep;->ft:Les;

    invoke-interface {v0}, Les;->aC()Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lep;->fu:Ljava/lang/Object;

    .line 303
    return-void

    .line 297
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 298
    new-instance v0, Leq;

    invoke-direct {v0}, Leq;-><init>()V

    sput-object v0, Lep;->ft:Les;

    goto :goto_0

    .line 300
    :cond_1
    new-instance v0, Lev;

    invoke-direct {v0}, Lev;-><init>()V

    sput-object v0, Lep;->ft:Les;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    sget-object v0, Lep;->ft:Les;

    invoke-interface {v0, p0}, Les;->a(Lep;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lep;->fv:Ljava/lang/Object;

    .line 312
    return-void
.end method

.method public static onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 399
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, p1}, Les;->c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 400
    return-void
.end method

.method public static sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 336
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, p1}, Les;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 337
    return-void
.end method

.method public static sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 358
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, p1}, Les;->d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 359
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lhz;)V
    .locals 2

    .prologue
    .line 438
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Les;->a(Ljava/lang/Object;Landroid/view/View;Lhz;)V

    .line 439
    return-void
.end method

.method final aB()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lep;->fv:Ljava/lang/Object;

    return-object v0
.end method

.method public d(Landroid/view/View;)Lih;
    .locals 2

    .prologue
    .line 480
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Les;->b(Ljava/lang/Object;Landroid/view/View;)Lih;

    move-result-object v0

    return-object v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 379
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Les;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 419
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Les;->b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 420
    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 462
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Les;->a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 500
    sget-object v0, Lep;->ft:Les;

    sget-object v1, Lep;->fu:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Les;->a(Ljava/lang/Object;Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
