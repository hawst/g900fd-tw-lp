.class public final Ltt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private CZ:Landroid/graphics/PointF;

.field private Da:Landroid/graphics/Rect;

.field private Db:J

.field private Dc:Z

.field private final Dd:Landroid/animation/TimeInterpolator;

.field private mR:F

.field private xu:Lcom/android/launcher3/DragLayer;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/DragLayer;Landroid/graphics/PointF;Landroid/graphics/Rect;JF)V
    .locals 2

    .prologue
    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Ltt;->Dd:Landroid/animation/TimeInterpolator;

    .line 448
    iput-object p1, p0, Ltt;->xu:Lcom/android/launcher3/DragLayer;

    .line 449
    iput-object p2, p0, Ltt;->CZ:Landroid/graphics/PointF;

    .line 450
    iput-object p3, p0, Ltt;->Da:Landroid/graphics/Rect;

    .line 451
    iput-wide p4, p0, Ltt;->Db:J

    .line 452
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Lcom/android/launcher3/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, p6

    sub-float/2addr v0, v1

    iput v0, p0, Ltt;->mR:F

    .line 453
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 12

    .prologue
    const/high16 v11, 0x447a0000    # 1000.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 457
    iget-object v0, p0, Ltt;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fN()Landroid/view/View;

    move-result-object v0

    check-cast v0, Luj;

    .line 458
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 459
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    .line 461
    iget-boolean v4, p0, Ltt;->Dc:Z

    if-nez v4, :cond_0

    .line 462
    const/4 v4, 0x1

    iput-boolean v4, p0, Ltt;->Dc:Z

    .line 463
    invoke-virtual {v0}, Luj;->getScaleX()F

    move-result v4

    .line 464
    sub-float v5, v4, v10

    invoke-virtual {v0}, Luj;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v7

    .line 465
    sub-float/2addr v4, v10

    invoke-virtual {v0}, Luj;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v4, v6

    div-float/2addr v4, v7

    .line 467
    iget-object v6, p0, Ltt;->Da:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    iput v5, v6, Landroid/graphics/Rect;->left:I

    .line 468
    iget-object v5, p0, Ltt;->Da:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    add-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, v5, Landroid/graphics/Rect;->top:I

    .line 471
    :cond_0
    iget-object v4, p0, Ltt;->Da:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget-object v6, p0, Ltt;->CZ:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-wide v8, p0, Ltt;->Db:J

    sub-long v8, v2, v8

    long-to-float v7, v8

    mul-float/2addr v6, v7

    div-float/2addr v6, v11

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 472
    iget-object v4, p0, Ltt;->Da:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget-object v6, p0, Ltt;->CZ:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-wide v8, p0, Ltt;->Db:J

    sub-long v8, v2, v8

    long-to-float v7, v8

    mul-float/2addr v6, v7

    div-float/2addr v6, v11

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 474
    iget-object v4, p0, Ltt;->Da:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Luj;->setTranslationX(F)V

    .line 475
    iget-object v4, p0, Ltt;->Da:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Luj;->setTranslationY(F)V

    .line 476
    iget-object v4, p0, Ltt;->Dd:Landroid/animation/TimeInterpolator;

    invoke-interface {v4, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    sub-float v1, v10, v1

    invoke-virtual {v0, v1}, Luj;->setAlpha(F)V

    .line 478
    iget-object v0, p0, Ltt;->CZ:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v4, p0, Ltt;->mR:F

    mul-float/2addr v1, v4

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 479
    iget-object v0, p0, Ltt;->CZ:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    iget v4, p0, Ltt;->mR:F

    mul-float/2addr v1, v4

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 480
    iput-wide v2, p0, Ltt;->Db:J

    .line 481
    return-void
.end method
