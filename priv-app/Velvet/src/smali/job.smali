.class public final Ljob;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private evP:I

.field private evQ:I

.field private ewb:I

.field private ewc:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 656
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 657
    iput v0, p0, Ljob;->aez:I

    iput v0, p0, Ljob;->evP:I

    iput v0, p0, Ljob;->evQ:I

    iput v0, p0, Ljob;->ewb:I

    iput v0, p0, Ljob;->ewc:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljob;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljob;->eCz:I

    .line 658
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljob;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljob;->evP:I

    iget v0, p0, Ljob;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljob;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljob;->evQ:I

    iget v0, p0, Ljob;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljob;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljob;->ewb:I

    iget v0, p0, Ljob;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljob;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljob;->ewc:I

    iget v0, p0, Ljob;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljob;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 674
    iget v0, p0, Ljob;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 675
    const/4 v0, 0x1

    iget v1, p0, Ljob;->evP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 677
    :cond_0
    iget v0, p0, Ljob;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 678
    const/4 v0, 0x2

    iget v1, p0, Ljob;->evQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 680
    :cond_1
    iget v0, p0, Ljob;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 681
    const/4 v0, 0x3

    iget v1, p0, Ljob;->ewb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 683
    :cond_2
    iget v0, p0, Ljob;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 684
    const/4 v0, 0x4

    iget v1, p0, Ljob;->ewc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 686
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 687
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 691
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 692
    iget v1, p0, Ljob;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 693
    const/4 v1, 0x1

    iget v2, p0, Ljob;->evP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    :cond_0
    iget v1, p0, Ljob;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 697
    const/4 v1, 0x2

    iget v2, p0, Ljob;->evQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 700
    :cond_1
    iget v1, p0, Ljob;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 701
    const/4 v1, 0x3

    iget v2, p0, Ljob;->ewb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    :cond_2
    iget v1, p0, Ljob;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 705
    const/4 v1, 0x4

    iget v2, p0, Ljob;->ewc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 708
    :cond_3
    return v0
.end method
