.class public Lcmt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field final bcA:Ljava/util/Queue;

.field private final bcB:Ljava/util/Queue;

.field private bcC:J

.field private bcD:Lcmq;

.field private bcE:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final dK:Ljava/lang/Object;

.field private final mConfig:Lcjs;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcjs;Lcpn;)V
    .locals 2

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcmt;->dK:Ljava/lang/Object;

    .line 92
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcmt;->bcC:J

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcmt;->bcD:Lcmq;

    .line 113
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcmt;->bcE:Lcom/google/android/shared/search/Query;

    .line 123
    iput-object p1, p0, Lcmt;->mConfig:Lcjs;

    .line 124
    iput-object p2, p0, Lcmt;->mUrlHelper:Lcpn;

    .line 125
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcmt;->bcA:Ljava/util/Queue;

    .line 126
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    .line 127
    return-void
.end method

.method private b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 517
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcmt;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p1, p2}, Lcpn;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/google/android/shared/search/Query;)Z
    .locals 6

    .prologue
    .line 531
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 535
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    iget-object v0, p0, Lcmt;->bcE:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 536
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method final QD()V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcmt;->bcA:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 201
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    invoke-virtual {v0}, Lcmq;->Qn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 208
    :cond_1
    iget-object v0, p0, Lcmt;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LR()I

    move-result v1

    .line 209
    :cond_2
    :goto_1
    iget-object v0, p0, Lcmt;->bcA:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-le v0, v1, :cond_3

    .line 210
    iget-object v0, p0, Lcmt;->bcA:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 212
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcmq;->isComplete()Z

    move-result v2

    if-nez v2, :cond_2

    .line 214
    invoke-virtual {v0}, Lcmq;->Qw()V

    goto :goto_1

    .line 217
    :cond_3
    return-void
.end method

.method public final Z(J)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 358
    .line 359
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 360
    :try_start_0
    iget-object v2, p0, Lcmt;->bcD:Lcmq;

    if-eqz v2, :cond_0

    .line 361
    iget-object v0, p0, Lcmt;->bcD:Lcmq;

    .line 362
    const/4 v2, 0x0

    iput-object v2, p0, Lcmt;->bcD:Lcmq;

    .line 363
    iput-wide p1, p0, Lcmt;->bcC:J

    .line 365
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    if-eqz v0, :cond_1

    .line 367
    invoke-virtual {p0, v0}, Lcmt;->a(Lcmq;)V

    .line 368
    invoke-virtual {v0}, Lcmq;->Qq()V

    .line 370
    :cond_1
    return-void

    .line 365
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcmq;J)J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 321
    iget-object v4, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v4

    .line 322
    :try_start_0
    iget-object v0, p0, Lcmt;->bcD:Lcmq;

    if-eqz v0, :cond_1

    .line 325
    const-wide/16 v0, -0x1

    .line 347
    :cond_0
    :goto_0
    iput-object p1, p0, Lcmt;->bcD:Lcmq;

    .line 348
    monitor-exit v4

    .line 349
    return-wide v0

    .line 326
    :cond_1
    iget-wide v0, p0, Lcmt;->bcC:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 329
    cmp-long v0, p2, v2

    if-ltz v0, :cond_2

    .line 330
    iget-wide v0, p0, Lcmt;->bcC:J

    sub-long v0, p2, v0

    .line 332
    iget-object v5, p0, Lcmt;->mConfig:Lcjs;

    invoke-virtual {v5}, Lcjs;->LS()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    int-to-long v6, v5

    sub-long v0, v6, v0

    .line 336
    :goto_1
    cmp-long v5, v0, v2

    if-gtz v5, :cond_0

    move-wide v0, v2

    .line 343
    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 334
    goto :goto_1

    :cond_3
    move-wide v0, v2

    .line 345
    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;JZ)Lcmq;
    .locals 10
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 229
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    if-eqz p4, :cond_0

    .line 237
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 243
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcmt;->bcD:Lcmq;

    .line 244
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :cond_0
    const/4 v0, 0x0

    .line 251
    iget-object v1, p0, Lcmt;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LP()I

    move-result v3

    .line 252
    iget-object v1, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v0

    .line 253
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 256
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 257
    invoke-virtual {v0}, Lcmq;->Ql()Lcnb;

    move-result-object v2

    iget-wide v6, v2, Lcnb;->bcL:J

    .line 258
    const/4 v2, 0x0

    .line 261
    invoke-virtual {v0}, Lcmq;->isFailed()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 264
    const/4 v2, 0x1

    .line 295
    :cond_2
    :goto_1
    if-eqz v2, :cond_1

    .line 297
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 298
    invoke-virtual {v0}, Lcmq;->Qn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 299
    invoke-virtual {v0}, Lcmq;->Qw()V

    goto :goto_0

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 265
    :cond_3
    invoke-virtual {v0}, Lcmq;->isComplete()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcmq;->Qr()Z

    move-result v5

    if-nez v5, :cond_4

    .line 269
    const/4 v2, 0x1

    goto :goto_1

    .line 270
    :cond_4
    sub-long v6, p2, v6

    int-to-long v8, v3

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    .line 273
    const/4 v2, 0x1

    goto :goto_1

    .line 274
    :cond_5
    invoke-virtual {v0}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lcmt;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 276
    if-eqz v1, :cond_6

    .line 280
    invoke-virtual {v1}, Lcmq;->Qw()V

    .line 281
    const-string v1, "SearchResultCache"

    const-string v5, "Found another page matching the query. Using the latest one."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v1, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_6
    move-object v1, v0

    .line 285
    goto :goto_1

    .line 286
    :cond_7
    if-eqz p4, :cond_2

    invoke-virtual {v0}, Lcmq;->isComplete()Z

    move-result v5

    if-nez v5, :cond_2

    .line 290
    const/4 v2, 0x1

    goto :goto_1

    .line 304
    :cond_8
    return-object v1
.end method

.method public final a(Lcmq;)V
    .locals 3

    .prologue
    .line 155
    :goto_0
    iget-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    iget-object v1, p0, Lcmt;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LQ()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 156
    iget-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_0

    .line 158
    :cond_0
    iget-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-virtual {p1}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 162
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-eq v0, v1, :cond_2

    .line 163
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 164
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqx()Z

    move-result v2

    if-nez v2, :cond_1

    .line 167
    invoke-virtual {p0, v0}, Lcmt;->e(Lcom/google/android/shared/search/Query;)V

    .line 169
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :goto_1
    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 174
    :cond_2
    new-instance v0, Lcmu;

    invoke-direct {v0, p0, p1}, Lcmu;-><init>(Lcmt;Lcmq;)V

    invoke-virtual {p1, v0}, Lcmq;->addObserver(Ljava/util/Observer;)V

    goto :goto_1
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 418
    const-string v0, "SearchResultCache state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 419
    const-string v0, "max cache entries"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcmt;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LQ()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 420
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 421
    :try_start_0
    const-string v0, "mLastFetchTime"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcmt;->bcC:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 422
    iget-object v0, p0, Lcmt;->bcD:Lcmq;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 423
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v0

    .line 426
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mPrefetchSrpDownloads, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcmt;->bcA:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items, these should also be in mCache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Lcmt;->bcA:Ljava/util/Queue;

    invoke-virtual {v0, v1}, Letj;->f(Ljava/lang/Iterable;)V

    .line 429
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCache, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 430
    iget-object v1, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-virtual {v0, v1}, Letj;->f(Ljava/lang/Iterable;)V

    .line 431
    return-void

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 486
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v2

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v3

    if-eq v2, v3, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v0

    .line 490
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v2

    invoke-virtual {v2}, Lehm;->asN()Z

    move-result v2

    if-nez v2, :cond_2

    .line 491
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lehm;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 493
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    move v0, v1

    .line 495
    goto :goto_0

    .line 496
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 501
    invoke-direct {p0, p1, p2}, Lcmt;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    goto :goto_0

    .line 507
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqc()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqx()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    move v2, v1

    .line 510
    :goto_1
    if-eqz v2, :cond_0

    invoke-direct {p0, p2}, Lcmt;->f(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, p2}, Lcmt;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v0

    .line 507
    goto :goto_1
.end method

.method public final clear()V
    .locals 3

    .prologue
    .line 376
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 377
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcmt;->bcD:Lcmq;

    .line 378
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    iget-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 382
    invoke-virtual {v0}, Lcmq;->isComplete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 383
    invoke-virtual {v0}, Lcmq;->Qw()V

    goto :goto_0

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 386
    :cond_1
    iget-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 390
    iget-object v0, p0, Lcmt;->bcA:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 391
    return-void
.end method

.method public final e(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 456
    iget-object v1, p0, Lcmt;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 459
    :try_start_0
    invoke-direct {p0, p1}, Lcmt;->f(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmt;->mUrlHelper:Lcpn;

    iget-object v2, p0, Lcmt;->bcE:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2, p1}, Lcpn;->c(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 461
    iput-object p1, p0, Lcmt;->bcE:Lcom/google/android/shared/search/Query;

    .line 463
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final eA()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 136
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 138
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 140
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 395
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 396
    const-string v0, "{"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    const/4 v0, 0x0

    .line 398
    iget-object v1, p0, Lcmt;->bcB:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 399
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 400
    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_0

    .line 401
    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 404
    invoke-virtual {v0}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 405
    if-eqz v0, :cond_1

    .line 406
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_0

    .line 408
    :cond_1
    const-string v0, "non-srp"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 410
    goto :goto_0

    .line 411
    :cond_2
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
