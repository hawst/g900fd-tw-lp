.class public final Lhcb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mGmsLocationReportingHelper:Leue;

.field private final mNowOptInSettings:Lcin;


# direct methods
.method public constructor <init>(Leue;Lcin;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lhcb;->mGmsLocationReportingHelper:Leue;

    .line 68
    iput-object p2, p0, Lhcb;->mNowOptInSettings:Lcin;

    .line 69
    return-void
.end method


# virtual methods
.method public final b([Landroid/accounts/Account;)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 76
    invoke-static {}, Lenu;->auQ()V

    .line 78
    array-length v0, p1

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 83
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v6

    .line 85
    array-length v1, p1

    move v0, v3

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v4, p1, v0

    .line 86
    iget-object v7, p0, Lhcb;->mGmsLocationReportingHelper:Leue;

    invoke-virtual {v7, v4}, Leue;->C(Landroid/accounts/Account;)Lcgs;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_0
    array-length v7, p1

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_6

    aget-object v8, p1, v4

    .line 91
    const/4 v0, 0x0

    move-object v1, v0

    move v0, v3

    .line 95
    :goto_2
    const/4 v9, 0x3

    if-ge v0, v9, :cond_1

    if-nez v1, :cond_1

    .line 97
    iget-object v1, p0, Lhcb;->mNowOptInSettings:Lcin;

    invoke-interface {v1, v8}, Lcin;->l(Landroid/accounts/Account;)Lizy;

    move-result-object v1

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 100
    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, v1, Lizy;->dUJ:Liyl;

    if-nez v0, :cond_3

    .line 101
    :cond_2
    const-string v0, "AccountConfigurationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "config was null for: "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 106
    :cond_3
    iget-object v0, v1, Lizy;->dUJ:Liyl;

    iget-object v0, v0, Liyl;->dQB:Ljel;

    if-nez v0, :cond_4

    .line 107
    const-string v0, "AccountConfigurationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "sidekick config was null for: "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const v0, 0xab3ebe

    invoke-static {v0}, Lhwt;->lx(I)V

    goto :goto_3

    .line 112
    :cond_4
    iget-object v9, v1, Lizy;->dUJ:Liyl;

    .line 115
    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgs;

    invoke-virtual {v0}, Lcgs;->Fg()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/ReportingState;

    .line 117
    iget-object v1, p0, Lhcb;->mNowOptInSettings:Lcin;

    invoke-interface {v1, v9, v8, v0}, Lcin;->b(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)I

    move-result v1

    if-ne v1, v2, :cond_5

    move v1, v2

    .line 120
    :goto_4
    new-instance v10, Lhcc;

    invoke-direct {v10, v8, v1}, Lhcc;-><init>(Landroid/accounts/Account;Z)V

    .line 121
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v1, p0, Lhcb;->mNowOptInSettings:Lcin;

    invoke-interface {v1, v9, v8, v0}, Lcin;->a(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)V

    goto :goto_3

    :cond_5
    move v1, v3

    .line 117
    goto :goto_4

    .line 127
    :cond_6
    return-object v5
.end method
