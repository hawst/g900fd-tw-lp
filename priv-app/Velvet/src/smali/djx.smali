.class public final Ldjx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bAz:I

.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mClock:Lemp;

.field private final mPreferenceController:Lchr;

.field private final mRandom:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/app/AlarmManager;Lemp;Lchr;Ljava/util/Random;I)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Ldjx;->mAlarmManager:Landroid/app/AlarmManager;

    .line 59
    iput-object p2, p0, Ldjx;->mClock:Lemp;

    .line 60
    iput-object p3, p0, Ldjx;->mPreferenceController:Lchr;

    .line 61
    iput-object p4, p0, Ldjx;->mRandom:Ljava/util/Random;

    .line 62
    iput p5, p0, Ldjx;->bAz:I

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lemp;Lchr;Ljava/util/Random;)V
    .locals 6

    .prologue
    .line 51
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Ldjx;-><init>(Landroid/app/AlarmManager;Lemp;Lchr;Ljava/util/Random;I)V

    .line 53
    return-void
.end method

.method public static jL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AlarmStartTimeMillis_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Ldjx;->mPreferenceController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 254
    return-void
.end method


# virtual methods
.method public final a(IJLandroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Ldjx;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 75
    return-void
.end method

.method public final a(JLandroid/app/PendingIntent;Ljava/lang/String;Z)V
    .locals 15

    .prologue
    .line 135
    iget-object v2, p0, Ldjx;->mPreferenceController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    .line 137
    invoke-static/range {p4 .. p4}, Ldjx;->jL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 139
    const-wide/16 v4, 0x0

    .line 140
    iget-object v3, p0, Ldjx;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->elapsedRealtime()J

    move-result-wide v8

    .line 149
    if-nez p5, :cond_3

    const-wide/16 v10, 0x0

    invoke-interface {v2, v6, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-lez v3, :cond_3

    .line 150
    const-wide/16 v10, 0x0

    invoke-interface {v2, v6, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 157
    cmp-long v7, v2, v8

    if-gez v7, :cond_2

    .line 159
    sub-long v4, v8, v2

    div-long v4, v4, p1

    .line 160
    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    mul-long v4, v4, p1

    add-long/2addr v2, v4

    .line 162
    invoke-direct {p0, v6, v2, v3}, Ldjx;->m(Ljava/lang/String;J)V

    .line 178
    :cond_0
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 181
    add-long v2, v8, p1

    iget-object v4, p0, Ldjx;->mRandom:Ljava/util/Random;

    const-wide/16 v8, 0xf

    div-long v8, p1, v8

    long-to-int v5, v8

    const/16 v7, 0x7530

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 183
    invoke-direct {p0, v6, v2, v3}, Ldjx;->m(Ljava/lang/String;J)V

    .line 193
    :cond_1
    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-virtual {p0, v4, v2, v3, v0}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    .line 194
    return-void

    .line 169
    :cond_2
    add-long v10, v8, p1

    cmp-long v7, v2, v10

    if-ltz v7, :cond_0

    :cond_3
    move-wide v2, v4

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 224
    invoke-static {p2}, Ldjx;->jL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    iget-object v1, p0, Ldjx;->mPreferenceController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    invoke-interface {v1, v0}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 229
    invoke-virtual {p0, p1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 230
    return-void
.end method

.method public final cancel(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ldjx;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 113
    return-void
.end method

.method public final jK(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 202
    invoke-static {p1}, Ldjx;->jL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-object v1, p0, Ldjx;->mPreferenceController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1, v0, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 204
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 208
    :goto_0
    return-object v0

    .line 207
    :cond_0
    iget-object v2, p0, Ldjx;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    .line 208
    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final setExact(IJLandroid/app/PendingIntent;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 85
    iget v0, p0, Ldjx;->bAz:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Ldjx;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Ldjx;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public final setInexactRepeating(IJJLandroid/app/PendingIntent;)V
    .locals 8

    .prologue
    .line 105
    iget-object v0, p0, Ldjx;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v1, 0x0

    const-wide/32 v4, 0x36ee80

    move-wide v2, p2

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 106
    return-void
.end method
