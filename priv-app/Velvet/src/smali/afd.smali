.class final Lafd;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic Ld:Ljava/lang/String;

.field private synthetic Vj:Lafi;


# direct methods
.method constructor <init>(Lafi;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lafd;->Vj:Lafi;

    iput-object p2, p0, Lafd;->Ld:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs ko()Ljava/lang/Void;
    .locals 7

    .prologue
    .line 393
    iget-object v0, p0, Lafd;->Vj:Lafi;

    invoke-virtual {v0}, Lafi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 395
    :try_start_0
    const-string v1, "shortcut_and_widget_previews"

    const-string v2, "name LIKE ? OR name LIKE ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Widget:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lafd;->Ld:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Shortcut:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lafd;->Ld:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :goto_0
    invoke-static {}, Lafb;->kH()Ljava/util/HashSet;

    move-result-object v1

    monitor-enter v1

    .line 409
    :try_start_1
    invoke-static {}, Lafb;->kH()Ljava/util/HashSet;

    move-result-object v0

    iget-object v2, p0, Lafd;->Ld:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 410
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 411
    const/4 v0, 0x0

    return-object v0

    .line 404
    :catch_0
    move-exception v0

    .line 405
    invoke-static {}, Lafb;->kG()V

    .line 406
    throw v0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 407
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lafd;->ko()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
