.class public final Lgbw;
.super Lfra;
.source "PG"


# instance fields
.field private synthetic cCj:I

.field private synthetic cFq:Ljava/util/List;

.field private synthetic cFr:Lgbu;

.field private synthetic ctI:Lfmt;


# direct methods
.method public constructor <init>(Lgbu;Lfmt;Lizj;IILjava/util/List;Lfmt;)V
    .locals 1

    .prologue
    .line 146
    iput-object p1, p0, Lgbw;->cFr:Lgbu;

    iput p5, p0, Lgbw;->cCj:I

    iput-object p6, p0, Lgbw;->cFq:Ljava/util/List;

    iput-object p7, p0, Lgbw;->ctI:Lfmt;

    const/16 v0, 0x3a

    invoke-direct {p0, p2, p3, v0}, Lfra;-><init>(Lfmt;Lizj;I)V

    return-void
.end method


# virtual methods
.method protected final bj(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 149
    iget v0, p0, Lgbw;->cCj:I

    iget-object v1, p0, Lgbw;->cFq:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 150
    new-instance v1, Lgam;

    iget-object v2, p0, Lgbw;->mEntry:Lizj;

    iget-object v0, p0, Lgbw;->cFq:Ljava/util/List;

    iget v3, p0, Lgbw;->cCj:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwk;

    iget-object v3, p0, Lgbw;->cFr:Lgbu;

    iget-object v3, v3, Lgbu;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v1}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lgbw;->ctI:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfml;->V(Ljava/util/List;)V

    .line 155
    :cond_0
    check-cast p1, Lcom/google/android/sidekick/shared/ui/TransitTripRow;

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->aCX()Liyg;

    move-result-object v5

    .line 156
    iget-object v0, p0, Lgbw;->cFr:Lgbu;

    iget-object v0, v0, Lgbu;->mDirectionsLauncher:Lgah;

    iget-object v1, p0, Lgbw;->cFr:Lgbu;

    iget-object v1, v1, Lgbu;->mFrequentPlaceEntry:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    const/4 v2, 0x0

    sget-object v3, Lgba;->cEB:Lgba;

    invoke-static {v5}, Lgaz;->c(Liyg;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Liyg;->bbL()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lgah;->a(Ljbp;[Ljbp;Lgba;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    return-void
.end method
