.class public final Ljin;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private enw:Ljava/lang/String;

.field private enx:Ljava/lang/String;

.field private eny:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23414
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 23415
    const/4 v0, 0x0

    iput v0, p0, Ljin;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljin;->enw:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljin;->enx:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljin;->eny:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljin;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljin;->eCz:I

    .line 23416
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 23348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljin;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljin;->enw:Ljava/lang/String;

    iget v0, p0, Ljin;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljin;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljin;->enx:Ljava/lang/String;

    iget v0, p0, Ljin;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljin;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljin;->eny:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljin;->eny:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljin;->eny:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljin;->eny:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 23431
    iget v0, p0, Ljin;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 23432
    const/4 v0, 0x1

    iget-object v1, p0, Ljin;->enw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 23434
    :cond_0
    iget v0, p0, Ljin;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 23435
    const/4 v0, 0x2

    iget-object v1, p0, Ljin;->enx:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 23437
    :cond_1
    iget-object v0, p0, Ljin;->eny:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljin;->eny:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 23438
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljin;->eny:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 23439
    iget-object v1, p0, Ljin;->eny:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 23440
    if-eqz v1, :cond_2

    .line 23441
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 23438
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23445
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 23446
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 23450
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 23451
    iget v2, p0, Ljin;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 23452
    const/4 v2, 0x1

    iget-object v3, p0, Ljin;->enw:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 23455
    :cond_0
    iget v2, p0, Ljin;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 23456
    const/4 v2, 0x2

    iget-object v3, p0, Ljin;->enx:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 23459
    :cond_1
    iget-object v2, p0, Ljin;->eny:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljin;->eny:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 23462
    :goto_0
    iget-object v4, p0, Ljin;->eny:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 23463
    iget-object v4, p0, Ljin;->eny:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 23464
    if-eqz v4, :cond_2

    .line 23465
    add-int/lit8 v3, v3, 0x1

    .line 23466
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 23462
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 23470
    :cond_3
    add-int/2addr v0, v2

    .line 23471
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 23473
    :cond_4
    return v0
.end method
