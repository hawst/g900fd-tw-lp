.class public final Lhwh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public bbe:Ljava/lang/String;

.field public cPu:Z

.field private cQb:Ljava/lang/String;

.field public cQc:Ljava/util/List;

.field private diT:Lglu;

.field public diX:Z

.field public dtf:Landroid/net/Uri;

.field public final mAudioStore:Lgfb;

.field private mMainThreadExecutor:Ljava/util/concurrent/Executor;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mRecognizer:Lgdb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mVoiceSearchServices:Lhhq;


# direct methods
.method public constructor <init>(Lhhq;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwh;->diX:Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lhwh;->dtf:Landroid/net/Uri;

    .line 58
    iput-object p1, p0, Lhwh;->mVoiceSearchServices:Lhhq;

    .line 59
    new-instance v0, Lgfq;

    invoke-direct {v0}, Lgfq;-><init>()V

    iput-object v0, p0, Lhwh;->mAudioStore:Lgfb;

    .line 60
    return-void
.end method

.method private a(Lgfc;Ljava/lang/String;)Lgnj;
    .locals 4
    .param p1    # Lgfc;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 152
    new-instance v1, Lgmy;

    invoke-direct {v1}, Lgmy;-><init>()V

    .line 153
    if-eqz p1, :cond_2

    .line 154
    invoke-virtual {p1}, Lgfc;->asV()[B

    move-result-object v2

    iput-object v2, v1, Lgmy;->mAudio:[B

    .line 158
    :cond_0
    :goto_0
    new-instance v2, Lgnk;

    invoke-direct {v2}, Lgnk;-><init>()V

    iget-object v3, p0, Lhwh;->cQb:Ljava/lang/String;

    iput-object v3, v2, Lgnk;->cQb:Ljava/lang/String;

    iget-object v3, p0, Lhwh;->cQc:Ljava/util/List;

    iput-object v3, v2, Lgnk;->cQc:Ljava/util/List;

    iget-boolean v3, p0, Lhwh;->cPu:Z

    iput-boolean v3, v2, Lgnk;->cPu:Z

    sget-object v3, Lgjo;->cMK:Lgjo;

    iput-object v3, v2, Lgnk;->cQe:Lgjo;

    invoke-virtual {v2, v0}, Lgnk;->kt(I)Lgnk;

    move-result-object v2

    new-instance v3, Ljtp;

    invoke-direct {v3}, Ljtp;-><init>()V

    iput-object v3, v2, Lgnk;->cPw:Ljtp;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, v2, Lgnk;->cQa:Z

    invoke-virtual {v1}, Lgmy;->aHB()Lgmx;

    move-result-object v0

    iput-object v0, v2, Lgnk;->cPY:Lgmx;

    iput-object p2, v2, Lgnk;->cQd:Ljava/lang/String;

    invoke-virtual {v2}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    return-object v0

    .line 155
    :cond_2
    iget-object v2, p0, Lhwh;->dtf:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 156
    iget-object v2, p0, Lhwh;->dtf:Landroid/net/Uri;

    iput-object v2, v1, Lgmy;->cPM:Landroid/net/Uri;

    goto :goto_0
.end method

.method private auJ()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lhwh;->mRecognizer:Lgdb;

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lhwh;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    iput-object v0, p0, Lhwh;->mRecognizer:Lgdb;

    .line 146
    iget-object v0, p0, Lhwh;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lhwh;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    .line 148
    :cond_0
    return-void
.end method

.method private d(Lglx;)V
    .locals 2

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhwh;->ct(Z)V

    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwh;->diX:Z

    .line 176
    if-eqz p1, :cond_0

    .line 177
    new-instance v0, Lglv;

    invoke-direct {v0}, Lglv;-><init>()V

    .line 178
    new-instance v1, Lhwi;

    invoke-direct {v1, p0}, Lhwi;-><init>(Lhwh;)V

    invoke-virtual {v0, v1}, Lglv;->b(Lglx;)V

    .line 179
    invoke-virtual {v0, p1}, Lglv;->b(Lglx;)V

    .line 184
    :goto_0
    new-instance v1, Lglu;

    invoke-direct {v1, v0}, Lglu;-><init>(Lglx;)V

    iput-object v1, p0, Lhwh;->diT:Lglu;

    .line 185
    return-void

    .line 182
    :cond_0
    new-instance v0, Lhwi;

    invoke-direct {v0, p0}, Lhwi;-><init>(Lhwh;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lglx;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lglx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79
    iget-object v0, p0, Lhwh;->cQb:Ljava/lang/String;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-direct {p0}, Lhwh;->auJ()V

    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lhwh;->a(Lgfc;Ljava/lang/String;)Lgnj;

    move-result-object v0

    .line 83
    invoke-direct {p0, p1}, Lhwh;->d(Lglx;)V

    .line 84
    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhwh;->bbe:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lhwh;->mRecognizer:Lgdb;

    iget-object v2, p0, Lhwh;->diT:Lglu;

    iget-object v3, p0, Lhwh;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lhwh;->mAudioStore:Lgfb;

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 89
    return-void
.end method

.method public final aFy()Lgfc;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lhwh;->mAudioStore:Lgfb;

    invoke-interface {v0}, Lgfb;->aFy()Lgfc;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lglx;Ljava/lang/String;)Z
    .locals 5
    .param p1    # Lglx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114
    iget-object v0, p0, Lhwh;->cQb:Ljava/lang/String;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-virtual {p0}, Lhwh;->aFy()Lgfc;

    move-result-object v0

    .line 116
    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p0, p1, p2}, Lhwh;->a(Lglx;Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x0

    .line 127
    :goto_0
    return v0

    .line 120
    :cond_0
    invoke-direct {p0}, Lhwh;->auJ()V

    .line 122
    invoke-direct {p0, v0, p2}, Lhwh;->a(Lgfc;Ljava/lang/String;)Lgnj;

    move-result-object v0

    .line 123
    invoke-direct {p0, p1}, Lhwh;->d(Lglx;)V

    .line 124
    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhwh;->bbe:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lhwh;->mRecognizer:Lgdb;

    iget-object v2, p0, Lhwh;->diT:Lglu;

    iget-object v3, p0, Lhwh;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 127
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ct(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 190
    iget-boolean v0, p0, Lhwh;->diX:Z

    if-eqz v0, :cond_1

    .line 191
    const-string v0, "no_match"

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 193
    const/16 v0, 0x12

    invoke-static {v0}, Lege;->ht(I)V

    .line 199
    if-eqz p1, :cond_0

    .line 201
    iget-object v0, p0, Lhwh;->diT:Lglu;

    invoke-virtual {v0}, Lglu;->aEM()V

    .line 206
    :cond_0
    iget-object v0, p0, Lhwh;->mRecognizer:Lgdb;

    iget-object v1, p0, Lhwh;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 207
    iput-object v2, p0, Lhwh;->bbe:Ljava/lang/String;

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwh;->diX:Z

    .line 212
    :cond_1
    iget-object v0, p0, Lhwh;->diT:Lglu;

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lhwh;->diT:Lglu;

    invoke-virtual {v0}, Lglu;->invalidate()V

    .line 214
    iput-object v2, p0, Lhwh;->diT:Lglu;

    .line 216
    :cond_2
    return-void
.end method

.method public final oq(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lhwh;->cQb:Ljava/lang/String;

    .line 64
    return-void
.end method
