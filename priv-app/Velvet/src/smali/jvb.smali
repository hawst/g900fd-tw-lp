.class public final Ljvb;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFU:[Ljvb;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dMI:Ljava/lang/String;

.field private eBD:Ljava/lang/String;

.field private eFV:Ljvd;

.field private eFW:Ljvc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3741
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3742
    const/4 v0, 0x0

    iput v0, p0, Ljvb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljvb;->dMI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljvb;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljvb;->eBD:Ljava/lang/String;

    iput-object v1, p0, Ljvb;->eFV:Ljvd;

    iput-object v1, p0, Ljvb;->eFW:Ljvc;

    iput-object v1, p0, Ljvb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvb;->eCz:I

    .line 3743
    return-void
.end method

.method public static buT()[Ljvb;
    .locals 2

    .prologue
    .line 3656
    sget-object v0, Ljvb;->eFU:[Ljvb;

    if-nez v0, :cond_1

    .line 3657
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3659
    :try_start_0
    sget-object v0, Ljvb;->eFU:[Ljvb;

    if-nez v0, :cond_0

    .line 3660
    const/4 v0, 0x0

    new-array v0, v0, [Ljvb;

    sput-object v0, Ljvb;->eFU:[Ljvb;

    .line 3662
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3664
    :cond_1
    sget-object v0, Ljvb;->eFU:[Ljvb;

    return-object v0

    .line 3662
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvb;->dMI:Ljava/lang/String;

    iget v0, p0, Ljvb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvb;->agq:Ljava/lang/String;

    iget v0, p0, Ljvb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvb;->eBD:Ljava/lang/String;

    iget v0, p0, Ljvb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvb;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljvb;->eFV:Ljvd;

    if-nez v0, :cond_1

    new-instance v0, Ljvd;

    invoke-direct {v0}, Ljvd;-><init>()V

    iput-object v0, p0, Ljvb;->eFV:Ljvd;

    :cond_1
    iget-object v0, p0, Ljvb;->eFV:Ljvd;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljvb;->eFW:Ljvc;

    if-nez v0, :cond_2

    new-instance v0, Ljvc;

    invoke-direct {v0}, Ljvc;-><init>()V

    iput-object v0, p0, Ljvb;->eFW:Ljvc;

    :cond_2
    iget-object v0, p0, Ljvb;->eFW:Ljvc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3760
    iget v0, p0, Ljvb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3761
    const/4 v0, 0x1

    iget-object v1, p0, Ljvb;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3763
    :cond_0
    iget v0, p0, Ljvb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3764
    const/4 v0, 0x2

    iget-object v1, p0, Ljvb;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3766
    :cond_1
    iget v0, p0, Ljvb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3767
    const/4 v0, 0x3

    iget-object v1, p0, Ljvb;->eBD:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3769
    :cond_2
    iget-object v0, p0, Ljvb;->eFV:Ljvd;

    if-eqz v0, :cond_3

    .line 3770
    const/4 v0, 0x4

    iget-object v1, p0, Ljvb;->eFV:Ljvd;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3772
    :cond_3
    iget-object v0, p0, Ljvb;->eFW:Ljvc;

    if-eqz v0, :cond_4

    .line 3773
    const/4 v0, 0x5

    iget-object v1, p0, Ljvb;->eFW:Ljvc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3775
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3776
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3780
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3781
    iget v1, p0, Ljvb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3782
    const/4 v1, 0x1

    iget-object v2, p0, Ljvb;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3785
    :cond_0
    iget v1, p0, Ljvb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3786
    const/4 v1, 0x2

    iget-object v2, p0, Ljvb;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3789
    :cond_1
    iget v1, p0, Ljvb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3790
    const/4 v1, 0x3

    iget-object v2, p0, Ljvb;->eBD:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3793
    :cond_2
    iget-object v1, p0, Ljvb;->eFV:Ljvd;

    if-eqz v1, :cond_3

    .line 3794
    const/4 v1, 0x4

    iget-object v2, p0, Ljvb;->eFV:Ljvd;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3797
    :cond_3
    iget-object v1, p0, Ljvb;->eFW:Ljvc;

    if-eqz v1, :cond_4

    .line 3798
    const/4 v1, 0x5

    iget-object v2, p0, Ljvb;->eFW:Ljvc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3801
    :cond_4
    return v0
.end method
