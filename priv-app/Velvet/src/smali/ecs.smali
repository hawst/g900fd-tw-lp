.class final Lecs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private final aPk:Lcom/google/android/search/shared/service/ClientConfig;

.field private final bUZ:Lect;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private synthetic bVc:Lecq;


# direct methods
.method public constructor <init>(Lecq;Lect;Lcom/google/android/search/shared/service/ClientConfig;)V
    .locals 0
    .param p2    # Lect;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 555
    iput-object p1, p0, Lecs;->bVc:Lecq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556
    iput-object p2, p0, Lecs;->bUZ:Lect;

    .line 557
    iput-object p3, p0, Lecs;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    .line 558
    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    .line 564
    iget-object v0, p0, Lecs;->bVc:Lecq;

    iget-object v1, p0, Lecs;->bVc:Lecq;

    invoke-virtual {v1, p2}, Lecq;->H(Landroid/os/IBinder;)Lecj;

    move-result-object v1

    iput-object v1, v0, Lecq;->bVa:Lecj;

    .line 567
    :try_start_0
    iget-object v0, p0, Lecs;->bVc:Lecq;

    iget-object v1, p0, Lecs;->bVc:Lecq;

    iget-object v1, v1, Lecq;->bVa:Lecj;

    iget-object v2, p0, Lecs;->bVc:Lecq;

    iget-wide v2, v2, Lecq;->bpc:J

    iget-object v4, p0, Lecs;->bUZ:Lect;

    iget-object v5, p0, Lecs;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-interface {v1, v2, v3, v4, v5}, Lecj;->a(JLecm;Lcom/google/android/search/shared/service/ClientConfig;)Lecg;

    move-result-object v1

    iput-object v1, v0, Lecq;->bVb:Lecg;

    .line 568
    iget-object v0, p0, Lecs;->bVc:Lecq;

    iget-object v0, v0, Lecq;->bVb:Lecg;

    if-nez v0, :cond_1

    .line 569
    const-string v0, "SearchServiceClient"

    const-string v1, "Attach client call failed!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget-object v0, p0, Lecs;->bVc:Lecq;

    invoke-virtual {v0}, Lecq;->anA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lecs;->bVc:Lecq;

    invoke-virtual {v0}, Lecq;->onConnected()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 577
    :catch_0
    move-exception v0

    .line 578
    const-string v1, "SearchServiceClient"

    const-string v2, "setSearchServiceUiCallback() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lecs;->bVc:Lecq;

    const/4 v1, 0x0

    iput-object v1, v0, Lecq;->bVb:Lecg;

    .line 586
    iget-object v0, p0, Lecs;->bVc:Lecq;

    invoke-virtual {v0}, Lecq;->onDisconnected()V

    .line 587
    return-void
.end method
