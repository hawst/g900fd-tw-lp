.class public final Ledj;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/search/shared/ui/WebImageView;DIIIII)V
    .locals 3

    .prologue
    .line 249
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 250
    if-eqz v0, :cond_0

    .line 252
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 253
    int-to-double v0, v2

    mul-double/2addr v0, p1

    double-to-int v1, v0

    .line 254
    invoke-static {p5}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    invoke-static {p5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p7, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 257
    :goto_0
    if-ge v0, v1, :cond_1

    .line 259
    sub-int/2addr v1, v0

    mul-int/2addr v1, p3

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setScrollY(I)V

    .line 262
    :goto_1
    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/search/shared/ui/WebImageView;->aF(II)V

    .line 264
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, p7

    goto :goto_0
.end method

.method public static a(DI)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 229
    const-wide/16 v2, 0x0

    cmpl-double v1, p0, v2

    if-eqz v1, :cond_0

    .line 230
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 231
    const-string v1, "WebImageView"

    const-string v2, "fixedAspectRatio set, but neither width nor height is restricted."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_0
    :goto_0
    return v0

    .line 234
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
