.class public final Lgbg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bCP:[B


# instance fields
.field private volatile cEJ:Ljava/lang/Integer;

.field private volatile chr:[B

.field private final cxW:Ljsr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lgbg;->bCP:[B

    return-void
.end method

.method private constructor <init>(Ljsr;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lgbg;->cxW:Ljsr;

    .line 26
    return-void
.end method

.method public static j(Ljsr;)Lgbg;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lgbg;

    invoke-direct {v0, p0}, Lgbg;-><init>(Ljsr;)V

    return-object v0
.end method


# virtual methods
.method public final aEf()Ljsr;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lgbg;->cxW:Ljsr;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    if-ne p1, p0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v2, p1, Lgbg;

    if-nez v2, :cond_2

    move v0, v1

    .line 55
    goto :goto_0

    .line 58
    :cond_2
    check-cast p1, Lgbg;

    .line 59
    iget-object v2, p0, Lgbg;->cxW:Ljsr;

    iget-object v3, p1, Lgbg;->cxW:Ljsr;

    if-eq v2, v3, :cond_0

    .line 65
    iget-object v0, p0, Lgbg;->cxW:Ljsr;

    if-nez v0, :cond_3

    move v0, v1

    .line 66
    goto :goto_0

    .line 70
    :cond_3
    iget-object v0, p0, Lgbg;->cxW:Ljsr;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v2, p1, Lgbg;->cxW:Ljsr;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 71
    goto :goto_0

    .line 74
    :cond_4
    invoke-virtual {p0}, Lgbg;->getBytes()[B

    move-result-object v0

    invoke-virtual {p1}, Lgbg;->getBytes()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public final getBytes()[B
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lgbg;->chr:[B

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lgbg;->cxW:Ljsr;

    if-nez v0, :cond_1

    sget-object v0, Lgbg;->bCP:[B

    :goto_0
    iput-object v0, p0, Lgbg;->chr:[B

    .line 41
    :cond_0
    iget-object v0, p0, Lgbg;->chr:[B

    return-object v0

    .line 39
    :cond_1
    iget-object v0, p0, Lgbg;->cxW:Ljsr;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lgbg;->cEJ:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lgbg;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgbg;->cEJ:Ljava/lang/Integer;

    .line 82
    :cond_0
    iget-object v0, p0, Lgbg;->cEJ:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
