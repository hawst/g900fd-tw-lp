.class public final Ljhv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dPR:Ljbp;

.field public emo:Ljbc;

.field private emp:J

.field private emq:Ljava/lang/String;

.field private emr:Ljava/lang/String;

.field private ems:Ljava/lang/String;

.field public emt:[Ljhw;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41828
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 41829
    const/4 v0, 0x0

    iput v0, p0, Ljhv;->aez:I

    iput-object v2, p0, Ljhv;->emo:Ljbc;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhv;->emp:J

    const-string v0, ""

    iput-object v0, p0, Ljhv;->emq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljhv;->emr:Ljava/lang/String;

    iput-object v2, p0, Ljhv;->dPR:Ljbp;

    const-string v0, ""

    iput-object v0, p0, Ljhv;->ems:Ljava/lang/String;

    invoke-static {}, Ljhw;->bmk()[Ljhw;

    move-result-object v0

    iput-object v0, p0, Ljhv;->emt:[Ljhw;

    iput-object v2, p0, Ljhv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhv;->eCz:I

    .line 41830
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 41034
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljhv;->emo:Ljbc;

    if-nez v0, :cond_1

    new-instance v0, Ljbc;

    invoke-direct {v0}, Ljbc;-><init>()V

    iput-object v0, p0, Ljhv;->emo:Ljbc;

    :cond_1
    iget-object v0, p0, Ljhv;->emo:Ljbc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljhv;->emp:J

    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhv;->emq:Ljava/lang/String;

    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhv;->ems:Ljava/lang/String;

    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhv;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhv;->emt:[Ljhw;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhw;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljhv;->emt:[Ljhw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljhw;

    invoke-direct {v3}, Ljhw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljhv;->emt:[Ljhw;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljhw;

    invoke-direct {v3}, Ljhw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhv;->emt:[Ljhw;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhv;->emr:Ljava/lang/String;

    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhv;->aez:I

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljhv;->dPR:Ljbp;

    if-nez v0, :cond_5

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljhv;->dPR:Ljbp;

    :cond_5
    iget-object v0, p0, Ljhv;->dPR:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 41849
    iget-object v0, p0, Ljhv;->emo:Ljbc;

    if-eqz v0, :cond_0

    .line 41850
    const/4 v0, 0x1

    iget-object v1, p0, Ljhv;->emo:Ljbc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 41852
    :cond_0
    iget v0, p0, Ljhv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 41853
    const/4 v0, 0x2

    iget-wide v2, p0, Ljhv;->emp:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 41855
    :cond_1
    iget v0, p0, Ljhv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 41856
    const/4 v0, 0x3

    iget-object v1, p0, Ljhv;->emq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41858
    :cond_2
    iget v0, p0, Ljhv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 41859
    const/4 v0, 0x4

    iget-object v1, p0, Ljhv;->ems:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41861
    :cond_3
    iget-object v0, p0, Ljhv;->emt:[Ljhw;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljhv;->emt:[Ljhw;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 41862
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljhv;->emt:[Ljhw;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 41863
    iget-object v1, p0, Ljhv;->emt:[Ljhw;

    aget-object v1, v1, v0

    .line 41864
    if-eqz v1, :cond_4

    .line 41865
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 41862
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41869
    :cond_5
    iget v0, p0, Ljhv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 41870
    const/4 v0, 0x6

    iget-object v1, p0, Ljhv;->emr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41872
    :cond_6
    iget-object v0, p0, Ljhv;->dPR:Ljbp;

    if-eqz v0, :cond_7

    .line 41873
    const/4 v0, 0x7

    iget-object v1, p0, Ljhv;->dPR:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 41875
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 41876
    return-void
.end method

.method public final bmg()J
    .locals 2

    .prologue
    .line 41740
    iget-wide v0, p0, Ljhv;->emp:J

    return-wide v0
.end method

.method public final bmh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41759
    iget-object v0, p0, Ljhv;->emq:Ljava/lang/String;

    return-object v0
.end method

.method public final bmi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41781
    iget-object v0, p0, Ljhv;->emr:Ljava/lang/String;

    return-object v0
.end method

.method public final bmj()Z
    .locals 1

    .prologue
    .line 41792
    iget v0, p0, Ljhv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dm(J)Ljhv;
    .locals 2

    .prologue
    .line 41743
    const-wide/32 v0, 0x5276e380

    iput-wide v0, p0, Ljhv;->emp:J

    .line 41744
    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhv;->aez:I

    .line 41745
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 41880
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 41881
    iget-object v1, p0, Ljhv;->emo:Ljbc;

    if-eqz v1, :cond_0

    .line 41882
    const/4 v1, 0x1

    iget-object v2, p0, Ljhv;->emo:Ljbc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41885
    :cond_0
    iget v1, p0, Ljhv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 41886
    const/4 v1, 0x2

    iget-wide v2, p0, Ljhv;->emp:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 41889
    :cond_1
    iget v1, p0, Ljhv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 41890
    const/4 v1, 0x3

    iget-object v2, p0, Ljhv;->emq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41893
    :cond_2
    iget v1, p0, Ljhv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 41894
    const/4 v1, 0x4

    iget-object v2, p0, Ljhv;->ems:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41897
    :cond_3
    iget-object v1, p0, Ljhv;->emt:[Ljhw;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljhv;->emt:[Ljhw;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 41898
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljhv;->emt:[Ljhw;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 41899
    iget-object v2, p0, Ljhv;->emt:[Ljhw;

    aget-object v2, v2, v0

    .line 41900
    if-eqz v2, :cond_4

    .line 41901
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 41898
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 41906
    :cond_6
    iget v1, p0, Ljhv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 41907
    const/4 v1, 0x6

    iget-object v2, p0, Ljhv;->emr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41910
    :cond_7
    iget-object v1, p0, Ljhv;->dPR:Ljbp;

    if-eqz v1, :cond_8

    .line 41911
    const/4 v1, 0x7

    iget-object v2, p0, Ljhv;->dPR:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41914
    :cond_8
    return v0
.end method

.method public final vh(Ljava/lang/String;)Ljhv;
    .locals 1

    .prologue
    .line 41762
    if-nez p1, :cond_0

    .line 41763
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41765
    :cond_0
    iput-object p1, p0, Ljhv;->emq:Ljava/lang/String;

    .line 41766
    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhv;->aez:I

    .line 41767
    return-object p0
.end method

.method public final vi(Ljava/lang/String;)Ljhv;
    .locals 1

    .prologue
    .line 41784
    if-nez p1, :cond_0

    .line 41785
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41787
    :cond_0
    iput-object p1, p0, Ljhv;->emr:Ljava/lang/String;

    .line 41788
    iget v0, p0, Ljhv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhv;->aez:I

    .line 41789
    return-object p0
.end method
