.class final Ldqe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private synthetic bFj:Ldpz;


# direct methods
.method constructor <init>(Ldpz;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Ldqe;->bFj:Ldpz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 381
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Ldqe;->bFj:Ldpz;

    invoke-static {v1}, Ldpz;->c(Ldpz;)Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getWidth()I

    move-result v1

    iget-object v2, p0, Ldqe;->bFj:Ldpz;

    invoke-static {v2}, Ldpz;->d(Ldpz;)Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 383
    iget-object v1, p0, Ldqe;->bFj:Ldpz;

    invoke-static {v1}, Ldpz;->f(Ldpz;)Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Ldqe;->bFj:Ldpz;

    invoke-static {v2}, Ldpz;->e(Ldpz;)Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 384
    iget-object v1, p0, Ldqe;->bFj:Ldpz;

    iget-object v1, v1, Ldpz;->bFf:Ldqk;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v0}, Ldqk;->gg(I)V

    .line 385
    iget-object v0, p0, Ldqe;->bFj:Ldpz;

    iget-object v0, v0, Ldpz;->bFf:Ldqk;

    iget-object v1, p0, Ldqe;->bFj:Ldpz;

    invoke-static {v1}, Ldpz;->g(Ldpz;)Lecc;

    move-result-object v1

    invoke-virtual {v1}, Lecc;->aeD()I

    move-result v1

    invoke-virtual {v0, v1}, Ldqk;->setContentWidth(I)V

    .line 386
    return-void
.end method
