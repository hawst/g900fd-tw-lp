.class public final Ljwj;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJb:Ljsm;


# instance fields
.field public eJc:[Ljyk;

.field private eJd:[Ljyj;

.field public eJe:[Ljyi;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 104
    const/16 v0, 0xb

    const-class v1, Ljwj;

    const v2, 0x1b9e8252

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwj;->eJb:Ljsm;

    .line 110
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 125
    invoke-static {}, Ljyk;->bwh()[Ljyk;

    move-result-object v0

    iput-object v0, p0, Ljwj;->eJc:[Ljyk;

    invoke-static {}, Ljyj;->bwg()[Ljyj;

    move-result-object v0

    iput-object v0, p0, Ljwj;->eJd:[Ljyj;

    invoke-static {}, Ljyi;->bwf()[Ljyi;

    move-result-object v0

    iput-object v0, p0, Ljwj;->eJe:[Ljyi;

    const/4 v0, 0x0

    iput-object v0, p0, Ljwj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwj;->eCz:I

    .line 126
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 97
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwj;->eJc:[Ljyk;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljyk;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljwj;->eJc:[Ljyk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljyk;

    invoke-direct {v3}, Ljyk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljwj;->eJc:[Ljyk;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljyk;

    invoke-direct {v3}, Ljyk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwj;->eJc:[Ljyk;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwj;->eJd:[Ljyj;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljyj;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljwj;->eJd:[Ljyj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljyj;

    invoke-direct {v3}, Ljyj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljwj;->eJd:[Ljyj;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljyj;

    invoke-direct {v3}, Ljyj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwj;->eJd:[Ljyj;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwj;->eJe:[Ljyi;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljyi;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljwj;->eJe:[Ljyi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Ljyi;

    invoke-direct {v3}, Ljyi;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljwj;->eJe:[Ljyi;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Ljyi;

    invoke-direct {v3}, Ljyi;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwj;->eJe:[Ljyi;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Ljwj;->eJc:[Ljyk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljwj;->eJc:[Ljyk;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 141
    :goto_0
    iget-object v2, p0, Ljwj;->eJc:[Ljyk;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 142
    iget-object v2, p0, Ljwj;->eJc:[Ljyk;

    aget-object v2, v2, v0

    .line 143
    if-eqz v2, :cond_0

    .line 144
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 141
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_1
    iget-object v0, p0, Ljwj;->eJd:[Ljyj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljwj;->eJd:[Ljyj;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 149
    :goto_1
    iget-object v2, p0, Ljwj;->eJd:[Ljyj;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 150
    iget-object v2, p0, Ljwj;->eJd:[Ljyj;

    aget-object v2, v2, v0

    .line 151
    if-eqz v2, :cond_2

    .line 152
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 149
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 156
    :cond_3
    iget-object v0, p0, Ljwj;->eJe:[Ljyi;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljwj;->eJe:[Ljyi;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 157
    :goto_2
    iget-object v0, p0, Ljwj;->eJe:[Ljyi;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 158
    iget-object v0, p0, Ljwj;->eJe:[Ljyi;

    aget-object v0, v0, v1

    .line 159
    if-eqz v0, :cond_4

    .line 160
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 157
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 164
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 165
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 170
    iget-object v2, p0, Ljwj;->eJc:[Ljyk;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljwj;->eJc:[Ljyk;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 171
    :goto_0
    iget-object v3, p0, Ljwj;->eJc:[Ljyk;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 172
    iget-object v3, p0, Ljwj;->eJc:[Ljyk;

    aget-object v3, v3, v0

    .line 173
    if-eqz v3, :cond_0

    .line 174
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 171
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 179
    :cond_2
    iget-object v2, p0, Ljwj;->eJd:[Ljyj;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljwj;->eJd:[Ljyj;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 180
    :goto_1
    iget-object v3, p0, Ljwj;->eJd:[Ljyj;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 181
    iget-object v3, p0, Ljwj;->eJd:[Ljyj;

    aget-object v3, v3, v0

    .line 182
    if-eqz v3, :cond_3

    .line 183
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 180
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 188
    :cond_5
    iget-object v2, p0, Ljwj;->eJe:[Ljyi;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljwj;->eJe:[Ljyi;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 189
    :goto_2
    iget-object v2, p0, Ljwj;->eJe:[Ljyi;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 190
    iget-object v2, p0, Ljwj;->eJe:[Ljyi;

    aget-object v2, v2, v1

    .line 191
    if-eqz v2, :cond_6

    .line 192
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 189
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 197
    :cond_7
    return v0
.end method
