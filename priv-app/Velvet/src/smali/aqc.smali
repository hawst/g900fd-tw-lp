.class final Laqc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private final aml:I

.field private amm:Z

.field private synthetic amn:Laqb;

.field private k:I


# direct methods
.method constructor <init>(Laqb;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 117
    iput-object p1, p0, Laqc;->amn:Laqb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput v0, p0, Laqc;->k:I

    .line 114
    iput-boolean v0, p0, Laqc;->amm:Z

    .line 118
    iget v0, p1, Laqb;->amk:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Laqb;->amk:I

    .line 119
    iget-object v0, p1, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Laqc;->aml:I

    .line 120
    return-void
.end method

.method private tM()V
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Laqc;->amm:Z

    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Laqc;->amm:Z

    .line 157
    iget-object v0, p0, Laqc;->amn:Laqb;

    invoke-virtual {v0}, Laqb;->tL()V

    .line 159
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Laqc;->k:I

    .line 125
    :goto_0
    iget v1, p0, Laqc;->aml:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Laqc;->amn:Laqb;

    invoke-virtual {v1, v0}, Laqb;->cX(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    iget v1, p0, Laqc;->aml:I

    if-ge v0, v1, :cond_1

    .line 129
    const/4 v0, 0x1

    .line 133
    :goto_1
    return v0

    .line 132
    :cond_1
    invoke-direct {p0}, Laqc;->tM()V

    .line 133
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 139
    :goto_0
    iget v0, p0, Laqc;->k:I

    iget v1, p0, Laqc;->aml:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Laqc;->amn:Laqb;

    iget v1, p0, Laqc;->k:I

    invoke-virtual {v0, v1}, Laqb;->cX(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 140
    iget v0, p0, Laqc;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laqc;->k:I

    goto :goto_0

    .line 141
    :cond_0
    iget v0, p0, Laqc;->k:I

    iget v1, p0, Laqc;->aml:I

    if-ge v0, v1, :cond_1

    .line 142
    iget-object v0, p0, Laqc;->amn:Laqb;

    iget v1, p0, Laqc;->k:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Laqc;->k:I

    invoke-virtual {v0, v1}, Laqb;->cX(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 145
    :cond_1
    invoke-direct {p0}, Laqc;->tM()V

    .line 146
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
