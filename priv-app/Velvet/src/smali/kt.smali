.class public abstract Lkt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final kg:Lkw;

.field final kh:Lkv;

.field ki:Lku;

.field kj:Lks;

.field kk:Z

.field kl:Lky;

.field km:Z

.field final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lkw;)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lkv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkv;-><init>(Lkt;B)V

    iput-object v0, p0, Lkt;->kh:Lkv;

    .line 83
    if-nez p1, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, Lkt;->mContext:Landroid/content/Context;

    .line 88
    if-nez p2, :cond_1

    .line 89
    new-instance v0, Lkw;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Lkw;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, Lkt;->kg:Lkw;

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_1
    iput-object p2, p0, Lkt;->kg:Lkw;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lks;)V
    .locals 2

    .prologue
    .line 148
    invoke-static {}, Lld;->bN()V

    .line 150
    iget-object v0, p0, Lkt;->kj:Lks;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lkt;->kj:Lks;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkt;->kj:Lks;

    invoke-virtual {v0, p1}, Lks;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iput-object p1, p0, Lkt;->kj:Lks;

    .line 156
    iget-boolean v0, p0, Lkt;->kk:Z

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkt;->kk:Z

    .line 158
    iget-object v0, p0, Lkt;->kh:Lkv;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lkv;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final a(Lku;)V
    .locals 0

    .prologue
    .line 122
    invoke-static {}, Lld;->bN()V

    .line 123
    iput-object p1, p0, Lkt;->ki:Lku;

    .line 124
    return-void
.end method

.method public final a(Lky;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 222
    invoke-static {}, Lld;->bN()V

    .line 224
    iget-object v0, p0, Lkt;->kl:Lky;

    if-eq v0, p1, :cond_0

    .line 225
    iput-object p1, p0, Lkt;->kl:Lky;

    .line 226
    iget-boolean v0, p0, Lkt;->km:Z

    if-nez v0, :cond_0

    .line 227
    iput-boolean v1, p0, Lkt;->km:Z

    .line 228
    iget-object v0, p0, Lkt;->kh:Lkv;

    invoke-virtual {v0, v1}, Lkv;->sendEmptyMessage(I)Z

    .line 231
    :cond_0
    return-void
.end method

.method public b(Lks;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public g(Ljava/lang/String;)Lkx;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method
