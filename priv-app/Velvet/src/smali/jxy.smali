.class public final Ljxy;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eLu:I

.field private eLv:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 541
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 542
    iput v0, p0, Ljxy;->aez:I

    iput v0, p0, Ljxy;->eLu:I

    iput v0, p0, Ljxy;->eLv:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljxy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxy;->eCz:I

    .line 543
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 484
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxy;->eLu:I

    iget v0, p0, Ljxy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxy;->eLv:I

    iget v0, p0, Ljxy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxy;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 557
    iget v0, p0, Ljxy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 558
    const/4 v0, 0x1

    iget v1, p0, Ljxy;->eLu:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 560
    :cond_0
    iget v0, p0, Ljxy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 561
    const/4 v0, 0x2

    iget v1, p0, Ljxy;->eLv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 563
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 564
    return-void
.end method

.method public final bvQ()Z
    .locals 1

    .prologue
    .line 533
    iget v0, p0, Ljxy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 568
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 569
    iget v1, p0, Ljxy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 570
    const/4 v1, 0x1

    iget v2, p0, Ljxy;->eLu:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 573
    :cond_0
    iget v1, p0, Ljxy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 574
    const/4 v1, 0x2

    iget v2, p0, Ljxy;->eLv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 577
    :cond_1
    return v0
.end method

.method public final sU(I)Ljxy;
    .locals 1

    .prologue
    .line 509
    iput p1, p0, Ljxy;->eLu:I

    .line 510
    iget v0, p0, Ljxy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxy;->aez:I

    .line 511
    return-object p0
.end method

.method public final sV(I)Ljxy;
    .locals 1

    .prologue
    .line 528
    iput p1, p0, Ljxy;->eLv:I

    .line 529
    iget v0, p0, Ljxy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxy;->aez:I

    .line 530
    return-object p0
.end method
