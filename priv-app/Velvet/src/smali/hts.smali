.class public final Lhts;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field cHC:Z

.field cJz:Ljava/util/List;

.field private cKQ:Ljava/util/List;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final dqJ:Lhth;

.field dqS:Lhtv;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private dqT:Z

.field dqb:Lhtj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mRecognizerController:Lhkd;

.field private final mResources:Landroid/content/res/Resources;

.field final mSoundManager:Lhik;

.field private final mViewDisplayer:Lhul;


# direct methods
.method public constructor <init>(Lhkd;Landroid/content/res/Resources;Lhul;Lhik;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhts;->cJz:Ljava/util/List;

    .line 65
    iput-object p2, p0, Lhts;->mResources:Landroid/content/res/Resources;

    .line 66
    iput-object p1, p0, Lhts;->mRecognizerController:Lhkd;

    .line 67
    new-instance v0, Lhth;

    new-instance v1, Lhtu;

    invoke-direct {v1, p0}, Lhtu;-><init>(Lhts;)V

    invoke-direct {v0, v1}, Lhth;-><init>(Lhti;)V

    iput-object v0, p0, Lhts;->dqJ:Lhth;

    .line 69
    iput-object p3, p0, Lhts;->mViewDisplayer:Lhul;

    .line 70
    iput-object p4, p0, Lhts;->mSoundManager:Lhik;

    .line 71
    return-void
.end method

.method private aw(Ljava/util/List;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 136
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 137
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 139
    iget-object v4, p0, Lhts;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a05ee

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lhts;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a05f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lhtj;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lhts;->dqb:Lhtj;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 75
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtj;

    iput-object v0, p0, Lhts;->dqb:Lhtj;

    .line 76
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aRZ()V
    .locals 3

    .prologue
    const v2, 0x7f0a0930

    .line 213
    const/16 v0, 0x2d

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 216
    iget-object v0, p0, Lhts;->dqb:Lhtj;

    invoke-virtual {v0, v2, v2}, Lhtj;->bh(II)V

    .line 217
    return-void
.end method

.method public final av(Ljava/util/List;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v2, 0x0

    .line 79
    invoke-static {}, Lenu;->auR()V

    .line 82
    iput-object p1, p0, Lhts;->cKQ:Ljava/util/List;

    .line 85
    sget-object v0, Ldzb;->bRq:Ldzb;

    iget-object v1, p0, Lhts;->cKQ:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/search/shared/contact/Person;->a(Ldzb;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v9, :cond_0

    invoke-interface {v0, v2, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lhts;->cJz:Ljava/util/List;

    .line 87
    iput-boolean v2, p0, Lhts;->cHC:Z

    .line 89
    iget-object v0, p0, Lhts;->cKQ:Ljava/util/List;

    invoke-static {v0}, Lico;->aC(Ljava/util/List;)Z

    move-result v0

    iput-boolean v0, p0, Lhts;->dqT:Z

    .line 91
    invoke-virtual {p0}, Lhts;->azY()V

    .line 93
    iget-boolean v0, p0, Lhts;->dqT:Z

    if-eqz v0, :cond_2

    iget-object v3, p0, Lhts;->cJz:Ljava/util/List;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    iget-object v5, p0, Lhts;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f0a05ef

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    add-int/lit8 v8, v1, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhts;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a05f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_1
    iget-object v1, p0, Lhts;->mRecognizerController:Lhkd;

    new-instance v2, Lhtt;

    invoke-direct {v2, p0}, Lhtt;-><init>(Lhts;)V

    invoke-virtual {v1, v2, v9, v0}, Lhkd;->a(Lglx;ILjava/lang/String;)V

    .line 104
    return-void

    .line 93
    :cond_2
    iget-object v0, p0, Lhts;->cJz:Ljava/util/List;

    invoke-direct {p0, v0}, Lhts;->aw(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final azY()V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lhts;->mViewDisplayer:Lhul;

    invoke-virtual {v0}, Lhul;->aSc()Lhtv;

    move-result-object v0

    iput-object v0, p0, Lhts;->dqS:Lhtv;

    .line 116
    iget-object v0, p0, Lhts;->dqS:Lhtv;

    iget-object v1, p0, Lhts;->cJz:Ljava/util/List;

    iget-boolean v2, p0, Lhts;->dqT:Z

    invoke-interface {v0, v1, v2}, Lhtv;->g(Ljava/util/List;Z)V

    .line 117
    iget-object v0, p0, Lhts;->dqS:Lhtv;

    invoke-interface {v0, p0}, Lhtv;->a(Lhts;)V

    .line 118
    iget-object v0, p0, Lhts;->dqS:Lhtv;

    iget-object v1, p0, Lhts;->dqb:Lhtj;

    iget-object v1, v1, Lhtj;->dqF:Lhuk;

    invoke-virtual {v1}, Lhuk;->aSb()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhtv;->oa(Ljava/lang/String;)V

    .line 120
    iget-boolean v0, p0, Lhts;->dqT:Z

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lhts;->cJz:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getName()Ljava/lang/String;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    .line 125
    iget-object v1, p0, Lhts;->dqS:Lhtv;

    invoke-interface {v1, v0}, Lhtv;->setTitle(Ljava/lang/String;)V

    .line 129
    :cond_0
    iget-boolean v0, p0, Lhts;->cHC:Z

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lhts;->dqS:Lhtv;

    invoke-interface {v0}, Lhtv;->aQp()V

    .line 132
    :cond_1
    return-void
.end method
