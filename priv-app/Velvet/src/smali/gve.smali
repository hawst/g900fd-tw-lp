.class Lgve;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic cXr:Lguz;

.field private final cXu:Z

.field private final cXv:Z

.field private final cXw:Ljli;


# direct methods
.method constructor <init>(Lguz;Ljli;ZZ)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lgve;->cXr:Lguz;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 356
    iput-boolean p3, p0, Lgve;->cXu:Z

    .line 357
    iput-boolean p4, p0, Lgve;->cXv:Z

    .line 358
    iput-object p2, p0, Lgve;->cXw:Ljli;

    .line 359
    return-void
.end method


# virtual methods
.method protected ar(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 381
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgve;->cXr:Lguz;

    invoke-virtual {v0}, Lguz;->aKP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 386
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/HelpAction;

    .line 387
    iget-object v3, p0, Lgve;->cXr:Lguz;

    invoke-virtual {v3, v0}, Lguz;->b(Lcom/google/android/search/shared/actions/HelpAction;)Lhld;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 389
    :cond_0
    iget-object v0, p0, Lgve;->cXr:Lguz;

    new-instance v2, Lgvd;

    iget-object v3, p0, Lgve;->cXr:Lguz;

    invoke-direct {v2, v3, v1}, Lgvd;-><init>(Lguz;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Lguz;->a(Lesj;)V

    .line 391
    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 348
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v1, p0, Lgve;->cXu:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgve;->cXw:Ljli;

    invoke-static {v1, v0}, Ldkw;->a(Ljli;Ljava/util/List;)V

    :cond_0
    iget-boolean v1, p0, Lgve;->cXv:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgve;->cXw:Ljli;

    iget-object v2, p0, Lgve;->cXr:Lguz;

    invoke-virtual {v2}, Lguz;->aKM()Libv;

    move-result-object v2

    iget-object v3, p0, Lgve;->cXr:Lguz;

    iget-object v3, v3, Lguz;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DX()Lenm;

    move-result-object v3

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v4

    invoke-static {v1, v2, v3, v4, v0}, Ldkw;->a(Ljli;Libv;Lenm;ILjava/util/List;)V

    :cond_1
    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 348
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lgve;->ar(Ljava/util/List;)V

    return-void
.end method
