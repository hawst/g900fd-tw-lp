.class public final Lajq;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field private GS:Landroid/view/LayoutInflater;

.field private synthetic abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

.field private abe:I

.field private abf:I

.field private abg:Ljava/util/ArrayList;

.field private abh:Ljava/lang/String;

.field private abi:Z


# direct methods
.method public constructor <init>(Lcom/android/recurrencepicker/RecurrencePickerDialog;Landroid/content/Context;Ljava/util/ArrayList;II)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1238
    iput-object p1, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    .line 1239
    invoke-direct {p0, p2, p4, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1227
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lajq;->GS:Landroid/view/LayoutInflater;

    .line 1241
    iput p4, p0, Lajq;->abe:I

    .line 1242
    iput p5, p0, Lajq;->abf:I

    .line 1243
    iput-object p3, p0, Lajq;->abg:Ljava/util/ArrayList;

    .line 1244
    invoke-virtual {p1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajq;->abh:Ljava/lang/String;

    .line 1249
    iget-object v0, p0, Lajq;->abh:Ljava/lang/String;

    const-string v1, "%s"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1250
    if-gtz v0, :cond_2

    .line 1253
    iput-boolean v2, p0, Lajq;->abi:Z

    .line 1265
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lajq;->abi:Z

    if-eqz v0, :cond_1

    .line 1268
    invoke-static {p1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->i(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/Spinner;

    move-result-object v0

    new-instance v1, Landroid/widget/TableLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v3, -0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/TableLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1271
    :cond_1
    return-void

    .line 1255
    :cond_2
    invoke-virtual {p1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f100000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 1257
    const-string v1, "%d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1258
    if-gtz v0, :cond_0

    .line 1261
    iput-boolean v2, p0, Lajq;->abi:Z

    goto :goto_0
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1343
    if-nez p2, :cond_0

    .line 1344
    iget-object v0, p0, Lajq;->GS:Landroid/view/LayoutInflater;

    iget v1, p0, Lajq;->abe:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1349
    :cond_0
    const v0, 0x7f1103a4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1350
    iget-object v1, p0, Lajq;->abg:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1352
    return-object p2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 1277
    if-nez p2, :cond_0

    .line 1278
    iget-object v0, p0, Lajq;->GS:Landroid/view/LayoutInflater;

    iget v1, p0, Lajq;->abf:I

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1283
    :cond_0
    const v0, 0x7f1103a4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1285
    packed-switch p1, :pswitch_data_0

    .line 1332
    const/4 p2, 0x0

    .line 1336
    :cond_1
    :goto_0
    return-object p2

    .line 1287
    :pswitch_0
    iget-object v1, p0, Lajq;->abg:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1290
    :pswitch_1
    iget-object v1, p0, Lajq;->abh:Ljava/lang/String;

    const-string v2, "%s"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1292
    if-eq v1, v4, :cond_1

    .line 1293
    iget-boolean v2, p0, Lajq;->abi:Z

    if-nez v2, :cond_2

    if-nez v1, :cond_3

    .line 1296
    :cond_2
    iget-object v1, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->j(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1298
    :cond_3
    iget-object v2, p0, Lajq;->abh:Ljava/lang/String;

    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1303
    :pswitch_2
    iget-object v1, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->k(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f100000

    iget-object v3, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v3}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->b(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    move-result-object v3

    iget v3, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    .line 1305
    const-string v1, "%d"

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1307
    if-eq v1, v4, :cond_1

    .line 1308
    iget-boolean v3, p0, Lajq;->abi:Z

    if-nez v3, :cond_4

    if-nez v1, :cond_5

    .line 1311
    :cond_4
    iget-object v1, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->l(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1313
    iget-object v0, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->m(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1315
    iget-object v0, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Lcom/android/recurrencepicker/RecurrencePickerDialog;Z)Z

    goto :goto_0

    .line 1317
    :cond_5
    add-int/lit8 v3, v1, 0x2

    .line 1318
    iget-object v4, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v4}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->m(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1321
    iget-object v3, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v3}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->b(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    move-result-object v3

    iget v3, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 1322
    iget-object v3, p0, Lajq;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v3}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->m(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1324
    :cond_6
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_7

    .line 1325
    add-int/lit8 v1, v1, -0x1

    .line 1327
    :cond_7
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1285
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
