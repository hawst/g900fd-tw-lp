.class public final Leux;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cjx:Lfcl;

.field private final mCardPrefs:Lcxs;

.field private final mEntryProvider:Lfaq;

.field private final mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

.field private final mLoginHelper:Lcrh;

.field private final mNetworkClient:Lfcx;

.field private final mNowOptInSettings:Lcin;

.field private final mSearchSettings:Lcke;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;

.field private final mVelvetServices:Lgql;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcrh;Lfcx;Lcxs;Lcin;Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;Lgql;Lfaq;Lcke;Lfcl;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Leux;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 54
    iput-object p2, p0, Leux;->mLoginHelper:Lcrh;

    .line 55
    iput-object p3, p0, Leux;->mNetworkClient:Lfcx;

    .line 56
    iput-object p4, p0, Leux;->mCardPrefs:Lcxs;

    .line 57
    iput-object p5, p0, Leux;->mNowOptInSettings:Lcin;

    .line 58
    iput-object p6, p0, Leux;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    .line 59
    iput-object p7, p0, Leux;->mVelvetServices:Lgql;

    .line 60
    iput-object p8, p0, Leux;->mEntryProvider:Lfaq;

    .line 61
    iput-object p9, p0, Leux;->mSearchSettings:Lcke;

    .line 62
    iput-object p10, p0, Leux;->cjx:Lfcl;

    .line 63
    return-void
.end method

.method private awb()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 123
    const/4 v2, 0x2

    :try_start_0
    invoke-static {v2}, Lfjw;->iQ(I)Ljed;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljed;->hK(Z)Ljed;

    move-result-object v2

    .line 126
    new-instance v3, Liyl;

    invoke-direct {v3}, Liyl;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Liyl;->hp(Z)Liyl;

    move-result-object v3

    iput-object v3, v2, Ljed;->edq:Liyl;

    .line 131
    iget-object v3, p0, Leux;->mNetworkClient:Lfcx;

    invoke-interface {v3, v2}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v2

    if-nez v2, :cond_0

    .line 132
    const-string v1, "NowOptInHelper"

    const-string v2, "Network error while attempting to opt-in"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    return v0

    .line 135
    :catch_0
    move-exception v1

    .line 136
    const-string v2, "NowOptInHelper"

    const-string v3, "Exception while attempting to authenticate"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    move v0, v1

    .line 140
    goto :goto_0
.end method


# virtual methods
.method public final D(Landroid/accounts/Account;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-static {}, Lenu;->auQ()V

    .line 75
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Leux;->mLoginHelper:Lcrh;

    .line 80
    new-instance v3, Ljava/util/concurrent/FutureTask;

    new-instance v4, Leuy;

    invoke-direct {v4, p0, v0, p1}, Leuy;-><init>(Leux;Lcrh;Landroid/accounts/Account;)V

    invoke-direct {v3, v4}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 92
    iget-object v0, p0, Leux;->mUiExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 94
    invoke-static {v3}, Livg;->c(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v1

    .line 99
    :cond_1
    iget-object v0, p0, Leux;->mCardPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TK()V

    .line 102
    invoke-direct {p0}, Leux;->awb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Leux;->mNowOptInSettings:Lcin;

    iget-object v3, p0, Leux;->mLoginHelper:Lcrh;

    invoke-virtual {v3}, Lcrh;->KE()I

    move-result v3

    invoke-interface {v0, p1, v3}, Lcin;->a(Landroid/accounts/Account;I)V

    iget-object v0, p0, Leux;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KB()V

    iget-object v0, p0, Leux;->mCardPrefs:Lcxs;

    invoke-virtual {v0, p1}, Lcxs;->w(Landroid/accounts/Account;)V

    iget-object v0, p0, Leux;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ov()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Leux;->mCardPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v3

    iget-object v0, p0, Leux;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ov()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcxi;->fq(I)V

    :cond_2
    iget-object v0, p0, Leux;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayI()V

    iget-object v0, p0, Leux;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJO()V

    iget-object v0, p0, Leux;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJP()V

    iget-object v0, p0, Leux;->cjx:Lfcl;

    invoke-virtual {v0, p1}, Lfcl;->F(Landroid/accounts/Account;)V

    iget-object v0, p0, Leux;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->invalidate()V

    move v1, v2

    .line 104
    goto :goto_0

    :cond_3
    move v0, v2

    .line 103
    goto :goto_1
.end method
