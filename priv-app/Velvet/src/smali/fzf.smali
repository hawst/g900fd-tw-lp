.class final Lfzf;
.super Lfra;
.source "PG"


# instance fields
.field private synthetic cCj:I

.field private synthetic cDD:Landroid/view/ViewGroup;

.field private synthetic cDE:Lapf;

.field private synthetic cDF:Lfze;


# direct methods
.method constructor <init>(Lfze;Lfmt;Lizj;IILandroid/view/ViewGroup;Lapf;)V
    .locals 1

    .prologue
    .line 162
    iput-object p1, p0, Lfzf;->cDF:Lfze;

    iput p5, p0, Lfzf;->cCj:I

    iput-object p6, p0, Lfzf;->cDD:Landroid/view/ViewGroup;

    iput-object p7, p0, Lfzf;->cDE:Lapf;

    const/16 v0, 0x3a

    invoke-direct {p0, p2, p3, v0}, Lfra;-><init>(Lfmt;Lizj;I)V

    return-void
.end method


# virtual methods
.method protected final bj(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lfzf;->cDF:Lfze;

    iget-object v0, v0, Lfze;->cDC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lfzf;->cCj:I

    iget-object v2, p0, Lfzf;->cDF:Lfze;

    iget-object v2, v2, Lfze;->cDC:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    move v0, v1

    .line 166
    :goto_0
    iget-object v2, p0, Lfzf;->cDD:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 167
    iget-object v2, p0, Lfzf;->cDD:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_0
    iget-object v0, p0, Lfzf;->cDF:Lfze;

    iget-object v0, v0, Lfze;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 171
    new-instance v1, Lgam;

    iget-object v2, p0, Lfzf;->mEntry:Lizj;

    iget-object v0, p0, Lfzf;->cDF:Lfze;

    iget-object v0, v0, Lfze;->cDC:Ljava/util/List;

    iget v3, p0, Lfzf;->cCj:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwk;

    iget-object v3, p0, Lfzf;->cDF:Lfze;

    iget-object v3, v3, Lfze;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v1}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lfzf;->cDF:Lfze;

    iget-object v1, v1, Lfze;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfml;->V(Ljava/util/List;)V

    .line 175
    iget-object v0, p0, Lfzf;->cDF:Lfze;

    iget-object v0, v0, Lfze;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfzf;->mEntry:Lizj;

    invoke-interface {v0, v1, v6}, Lfmt;->c(Lizj;Z)V

    .line 179
    :goto_1
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lfzf;->cDF:Lfze;

    iget-object v0, v0, Lfze;->mActivityHelper:Lfzw;

    iget-object v1, p0, Lfzf;->cDF:Lfze;

    iget-object v1, v1, Lfze;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfzf;->cDE:Lapf;

    invoke-virtual {v2}, Lapf;->sg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Lfzw;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    goto :goto_1
.end method
