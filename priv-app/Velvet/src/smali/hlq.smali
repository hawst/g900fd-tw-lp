.class public final Lhlq;
.super Lhlo;
.source "PG"


# instance fields
.field private final dku:Libs;

.field private final dkw:Lglm;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Leoj;Lglm;Libs;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lhlo;-><init>(Leoj;)V

    .line 37
    iput-object p2, p0, Lhlq;->dkw:Lglm;

    .line 38
    iput-object p3, p0, Lhlq;->dku:Libs;

    .line 39
    iput-object p4, p0, Lhlq;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 40
    return-void
.end method

.method private a(Lcom/google/android/search/shared/actions/EmailAction;Z)[Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 62
    new-instance v2, Libu;

    invoke-direct {v2}, Libu;-><init>()V

    .line 63
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 66
    :goto_0
    if-eqz v0, :cond_0

    .line 67
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alQ()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    iput-object v3, v2, Libu;->dxs:[Ljava/lang/String;

    .line 69
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Libu;->dxt:Ljava/lang/CharSequence;

    .line 70
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getBody()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Libu;->dxu:Ljava/lang/CharSequence;

    .line 72
    new-instance v0, Lefl;

    invoke-direct {v0}, Lefl;-><init>()V

    .line 73
    iget-object v3, p0, Lhlq;->dkw:Lglm;

    invoke-interface {v3, v0}, Lglm;->a(Lefk;)V

    .line 77
    :try_start_0
    invoke-virtual {v0}, Lefl;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 83
    :goto_1
    iget-object v1, p0, Lhlq;->dku:Libs;

    invoke-virtual {v1, v2, p2, v0, v4}, Libs;->a(Libu;ZLjava/lang/String;Z)[Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 64
    goto :goto_0

    .line 79
    :catch_0
    move-exception v0

    const-string v0, "SelfNoteActionExecutor"

    const-string v3, "Unable to get account"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 82
    goto :goto_1

    .line 81
    :catch_1
    move-exception v0

    const-string v0, "SelfNoteActionExecutor"

    const-string v3, "Unable to get account"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-static {}, Libs;->aUJ()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gm"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {}, Libs;->aUJ()Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 26
    check-cast p1, Lcom/google/android/search/shared/actions/EmailAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v2, [Landroid/content/Intent;

    iget-object v2, p0, Lhlq;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v2, [Landroid/content/Intent;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.category.APP_EMAIL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    check-cast p1, Lcom/google/android/search/shared/actions/EmailAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-static {v0}, Lico;->l(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v1, v2

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v2}, Lhlq;->a(Lcom/google/android/search/shared/actions/EmailAction;Z)[Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/search/shared/actions/EmailAction;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lhlq;->a(Lcom/google/android/search/shared/actions/EmailAction;Z)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
