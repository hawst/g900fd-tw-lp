.class final Ldiu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field OF:Z

.field final buk:Leqo;

.field final byY:Ljava/util/List;

.field final byZ:Ldig;

.field private final bza:I

.field private final bzb:I

.field final bzc:Z

.field bzd:I

.field bze:I

.field private bzf:I

.field private final bzg:Ljava/lang/Runnable;

.field final bzh:Ljava/lang/Runnable;

.field final bzi:Ljava/lang/Runnable;

.field final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lepo;Leqo;IIZ)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lepo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Leqo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ldiv;

    const-string v3, "Next batch"

    invoke-direct {v0, p0, v3}, Ldiv;-><init>(Ldiu;Ljava/lang/String;)V

    iput-object v0, p0, Ldiu;->bzg:Ljava/lang/Runnable;

    .line 62
    new-instance v0, Ldiw;

    const-string v3, "Finish first task"

    invoke-direct {v0, p0, v3}, Ldiw;-><init>(Ldiu;Ljava/lang/String;)V

    iput-object v0, p0, Ldiu;->bzh:Ljava/lang/Runnable;

    .line 69
    new-instance v0, Ldix;

    const-string v3, "Finish task and start next"

    invoke-direct {v0, p0, v3}, Ldix;-><init>(Ldiu;Ljava/lang/String;)V

    iput-object v0, p0, Ldiu;->bzi:Ljava/lang/Runnable;

    .line 41
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    if-lez p4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 45
    if-ltz p5, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 46
    iput-object p1, p0, Ldiu;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Ldig;

    invoke-direct {v0, p2}, Ldig;-><init>(Lepo;)V

    iput-object v0, p0, Ldiu;->byZ:Ldig;

    .line 48
    iput-object p3, p0, Ldiu;->buk:Leqo;

    .line 49
    iput p4, p0, Ldiu;->bza:I

    .line 50
    iput p5, p0, Ldiu;->bzb:I

    .line 51
    iput-boolean p6, p0, Ldiu;->bzc:Z

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldiu;->byY:Ljava/util/List;

    .line 53
    return-void

    :cond_0
    move v0, v2

    .line 44
    goto :goto_0

    :cond_1
    move v1, v2

    .line 45
    goto :goto_1
.end method


# virtual methods
.method final fT(I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-virtual {p0}, Ldiu;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget v1, p0, Ldiu;->bza:I

    iget v2, p0, Ldiu;->bze:I

    sub-int/2addr v1, v2

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 113
    if-lez v1, :cond_0

    .line 114
    iget-object v2, p0, Ldiu;->buk:Leqo;

    iget-object v3, p0, Ldiu;->bzg:Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 115
    iget-object v2, p0, Ldiu;->byZ:Ldig;

    iget-object v3, v2, Ldig;->byI:Ljava/util/ArrayList;

    new-array v4, v0, [Lepn;

    invoke-static {v3, v1, v4}, Ldig;->a(Ljava/util/List;I[Lepn;)[Lepn;

    move-result-object v1

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    iget-object v5, v2, Ldig;->mExecutor:Lepo;

    invoke-interface {v5, v4}, Lepo;->a(Lepn;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    array-length v0, v1

    .line 117
    iget v1, p0, Ldiu;->bze:I

    add-int/2addr v1, v0

    iput v1, p0, Ldiu;->bze:I

    .line 118
    iget v1, p0, Ldiu;->bzf:I

    add-int/2addr v0, v1

    iput v0, p0, Ldiu;->bzf:I

    .line 119
    iget v0, p0, Ldiu;->bzf:I

    iget v1, p0, Ldiu;->bzd:I

    if-ge v0, v1, :cond_0

    .line 120
    iget-object v0, p0, Ldiu;->buk:Leqo;

    iget-object v1, p0, Ldiu;->bzg:Ljava/lang/Runnable;

    iget v2, p0, Ldiu;->bzb:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final isFinished()Z
    .locals 2

    .prologue
    .line 126
    iget-boolean v0, p0, Ldiu;->OF:Z

    if-nez v0, :cond_0

    iget v0, p0, Ldiu;->bzf:I

    iget v1, p0, Ldiu;->bze:I

    sub-int/2addr v0, v1

    iget v1, p0, Ldiu;->bzd:I

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Ldiu;->bza:I

    invoke-virtual {p0, v0}, Ldiu;->fT(I)V

    .line 108
    return-void
.end method
