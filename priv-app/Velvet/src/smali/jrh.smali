.class public final Ljrh;
.super Ljsl;
.source "PG"


# static fields
.field public static final eAt:Ljsm;


# instance fields
.field private aez:I

.field private dHr:Ljava/lang/String;

.field private eAu:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1239
    const/16 v0, 0xb

    const-class v1, Ljrh;

    const/16 v2, 0x1f42

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljrh;->eAt:Ljsm;

    .line 1250
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1298
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1299
    iput v1, p0, Ljrh;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljrh;->dHr:Ljava/lang/String;

    iput v1, p0, Ljrh;->eAu:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljrh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrh;->eCz:I

    .line 1300
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1232
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrh;->dHr:Ljava/lang/String;

    iget v0, p0, Ljrh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljrh;->eAu:I

    iget v0, p0, Ljrh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrh;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1314
    iget v0, p0, Ljrh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1315
    const/4 v0, 0x2

    iget-object v1, p0, Ljrh;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1317
    :cond_0
    iget v0, p0, Ljrh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1318
    const/4 v0, 0x3

    iget v1, p0, Ljrh;->eAu:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1320
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1321
    return-void
.end method

.method public final btj()I
    .locals 1

    .prologue
    .line 1282
    iget v0, p0, Ljrh;->eAu:I

    return v0
.end method

.method public final btk()Z
    .locals 1

    .prologue
    .line 1290
    iget v0, p0, Ljrh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1260
    iget-object v0, p0, Ljrh;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1325
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1326
    iget v1, p0, Ljrh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1327
    const/4 v1, 0x2

    iget-object v2, p0, Ljrh;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1330
    :cond_0
    iget v1, p0, Ljrh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1331
    const/4 v1, 0x3

    iget v2, p0, Ljrh;->eAu:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1334
    :cond_1
    return v0
.end method

.method public final rO(I)Ljrh;
    .locals 1

    .prologue
    .line 1285
    iput p1, p0, Ljrh;->eAu:I

    .line 1286
    iget v0, p0, Ljrh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrh;->aez:I

    .line 1287
    return-object p0
.end method

.method public final yw(Ljava/lang/String;)Ljrh;
    .locals 1

    .prologue
    .line 1263
    if-nez p1, :cond_0

    .line 1264
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1266
    :cond_0
    iput-object p1, p0, Ljrh;->dHr:Ljava/lang/String;

    .line 1267
    iget v0, p0, Ljrh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrh;->aez:I

    .line 1268
    return-object p0
.end method
