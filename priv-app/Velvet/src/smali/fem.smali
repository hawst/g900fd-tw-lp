.class public final Lfem;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field private synthetic cpN:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

.field private synthetic cpO:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;Landroid/os/PowerManager$WakeLock;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lfem;->cpN:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    iput-object p2, p0, Lfem;->cpO:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Integer;)Z
    .locals 2
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 222
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lfem;->cpN:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayK()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :cond_0
    iget-object v0, p0, Lfem;->cpO:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 231
    const/4 v0, 0x1

    return v0

    .line 229
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfem;->cpO:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method


# virtual methods
.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 218
    check-cast p1, Ljava/lang/Integer;

    invoke-direct {p0, p1}, Lfem;->a(Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method
