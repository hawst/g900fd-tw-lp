.class final Ldqa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldyg;


# instance fields
.field private bFh:Ldyh;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bFi:Z

.field private synthetic bFj:Ldpz;


# direct methods
.method constructor <init>(Ldpz;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Ldqa;->bFj:Ldpz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Query;Ldyh;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 233
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqq()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 234
    invoke-static {}, Lenu;->auR()V

    .line 235
    iget-object v0, p0, Ldqa;->bFj:Ldpz;

    iget-boolean v0, v0, Lebm;->bUu:Z

    if-eqz v0, :cond_0

    .line 236
    const-string v0, "GelSearchOverlay"

    const-string v1, "Ignoring external search request. Search Started."

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 246
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-virtual {p0}, Ldqa;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldqa;->bFh:Ldyh;

    if-eq v0, p2, :cond_1

    .line 239
    const/4 v0, 0x0

    invoke-interface {p2, v2, v0}, Ldyh;->a(ILequ;)V

    .line 241
    :cond_1
    iput-object p2, p0, Ldqa;->bFh:Ldyh;

    .line 242
    iget-object v0, p0, Ldqa;->bFj:Ldpz;

    invoke-static {v0, p1}, Ldpz;->a(Ldpz;Lcom/google/android/shared/search/Query;)V

    .line 243
    iget-object v0, p0, Ldqa;->bFj:Ldpz;

    invoke-static {v0}, Ldpz;->a(Ldpz;)V

    .line 244
    iget-object v0, p0, Ldqa;->bFj:Ldpz;

    invoke-virtual {v0, p1}, Ldpz;->aO(Lcom/google/android/shared/search/Query;)V

    .line 245
    iget-object v0, p0, Ldqa;->bFh:Ldyh;

    const/4 v1, 0x1

    iget-object v2, p0, Ldqa;->bFj:Ldpz;

    invoke-static {v2}, Ldpz;->b(Ldpz;)Lequ;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ldyh;->a(ILequ;)V

    goto :goto_0
.end method

.method final dL(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 258
    invoke-static {}, Lenu;->auR()V

    .line 259
    invoke-virtual {p0}, Ldqa;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v1, p0, Ldqa;->bFh:Ldyh;

    .line 261
    iput-object v2, p0, Ldqa;->bFh:Ldyh;

    .line 262
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-interface {v1, v0, v2}, Ldyh;->a(ILequ;)V

    .line 264
    :cond_0
    iput-boolean p1, p0, Ldqa;->bFi:Z

    .line 265
    return-void

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final isActive()Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Ldqa;->bFh:Ldyh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
