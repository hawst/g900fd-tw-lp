.class public final Ljlr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dXL:J

.field private dXM:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1499
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1500
    const/4 v0, 0x0

    iput v0, p0, Ljlr;->aez:I

    iput-wide v2, p0, Ljlr;->dXL:J

    iput-wide v2, p0, Ljlr;->dXM:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljlr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlr;->eCz:I

    .line 1501
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1442
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljlr;->dXL:J

    iget v0, p0, Ljlr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljlr;->dXM:J

    iget v0, p0, Ljlr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlr;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1515
    iget v0, p0, Ljlr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1516
    const/4 v0, 0x1

    iget-wide v2, p0, Ljlr;->dXL:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 1518
    :cond_0
    iget v0, p0, Ljlr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1519
    const/4 v0, 0x2

    iget-wide v2, p0, Ljlr;->dXM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 1521
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1522
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1526
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1527
    iget v1, p0, Ljlr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1528
    const/4 v1, 0x1

    iget-wide v2, p0, Ljlr;->dXL:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1531
    :cond_0
    iget v1, p0, Ljlr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1532
    const/4 v1, 0x2

    iget-wide v2, p0, Ljlr;->dXM:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1535
    :cond_1
    return v0
.end method
