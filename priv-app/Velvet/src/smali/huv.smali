.class public final Lhuv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static DEBUG:Z


# instance fields
.field private drA:S

.field final drw:B

.field private final drx:B

.field dry:S

.field drz:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lhuv;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(BB)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-byte p1, p0, Lhuv;->drw:B

    .line 76
    iput-byte p2, p0, Lhuv;->drx:B

    .line 77
    return-void
.end method


# virtual methods
.method public final d(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)S
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 89
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lhuv;->drz:S

    .line 90
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lhuv;->drA:S

    .line 94
    iget-byte v0, p0, Lhuv;->drx:B

    iget-short v2, p0, Lhuv;->drz:S

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x64

    int-to-short v0, v0

    iput-short v0, p0, Lhuv;->dry:S

    .line 95
    new-instance v4, Lhux;

    iget-short v0, p0, Lhuv;->drA:S

    add-int/lit8 v0, v0, 0x1

    iget-short v2, p0, Lhuv;->drz:S

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v4, p0, v0, v2}, Lhux;-><init>(Lhuv;II)V

    move v0, v1

    .line 97
    :goto_0
    iget-short v2, p0, Lhuv;->drz:S

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_7

    move v2, v1

    move v3, v1

    .line 111
    :goto_1
    iget-short v5, p0, Lhuv;->drA:S

    add-int/lit8 v5, v5, 0x1

    if-ge v2, v5, :cond_3

    .line 112
    add-int/lit8 v5, v2, -0x1

    iget-short v6, p0, Lhuv;->drz:S

    iget-short v7, p0, Lhuv;->dry:S

    sub-int/2addr v6, v7

    add-int/2addr v6, v0

    if-le v5, v6, :cond_1

    .line 122
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    aget-object v5, v5, v2

    iput-boolean v1, v5, Lhuw;->drD:Z

    .line 145
    :goto_2
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    aget-object v5, v5, v2

    iget-boolean v5, v5, Lhuw;->drD:Z

    if-nez v5, :cond_0

    .line 146
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    aget-object v5, v5, v2

    add-int/lit8 v6, v2, -0x1

    iget-object v7, v5, Lhuw;->drE:Lhuv;

    iget-short v7, v7, Lhuv;->drz:S

    iget-object v8, v5, Lhuw;->drE:Lhuv;

    iget-short v8, v8, Lhuv;->dry:S

    sub-int/2addr v7, v8

    add-int/2addr v7, v0

    iget-short v8, v5, Lhuw;->drC:S

    add-int/2addr v7, v8

    if-le v6, v7, :cond_0

    iput-boolean v1, v5, Lhuw;->drD:Z

    .line 150
    :cond_0
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    aget-object v5, v5, v2

    iget-boolean v5, v5, Lhuw;->drD:Z

    and-int/2addr v3, v5

    .line 111
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 124
    :cond_1
    add-int/lit8 v5, v2, -0x1

    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    add-int/lit8 v6, v0, -0x1

    invoke-interface {p2, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_2

    .line 125
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    aget-object v5, v5, v2

    iget-object v6, v4, Lhux;->drF:[Lhuw;

    add-int/lit8 v7, v2, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lhuw;->a(Lhuw;)V

    .line 128
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    aget-object v5, v5, v2

    iput-byte v9, v5, Lhuw;->drB:B

    goto :goto_2

    .line 132
    :cond_2
    iget-object v5, v4, Lhux;->drG:[Lhuw;

    add-int/lit8 v6, v2, -0x1

    aget-object v5, v5, v6

    .line 133
    iget-object v6, v4, Lhux;->drG:[Lhuw;

    aget-object v6, v6, v2

    invoke-virtual {v6, v5}, Lhuw;->a(Lhuw;)V

    .line 135
    iget-object v5, v4, Lhux;->drF:[Lhuw;

    aget-object v5, v5, v2

    .line 136
    iget-object v6, v4, Lhux;->drG:[Lhuw;

    aget-object v6, v6, v2

    invoke-virtual {v6, v5}, Lhuw;->b(Lhuw;)V

    .line 138
    iget-object v5, v4, Lhux;->drF:[Lhuw;

    add-int/lit8 v6, v2, -0x1

    aget-object v5, v5, v6

    .line 139
    iget-object v6, v4, Lhux;->drG:[Lhuw;

    aget-object v6, v6, v2

    invoke-virtual {v6, v5}, Lhuw;->b(Lhuw;)V

    goto :goto_2

    .line 153
    :cond_3
    if-eqz v3, :cond_4

    .line 160
    iget-short v0, p0, Lhuv;->dry:S

    .line 171
    :goto_3
    return v0

    .line 163
    :cond_4
    iget-object v2, v4, Lhux;->drG:[Lhuw;

    iget-object v3, v4, Lhux;->drF:[Lhuw;

    iput-object v3, v4, Lhux;->drG:[Lhuw;

    iput-object v2, v4, Lhux;->drF:[Lhuw;

    iget-object v2, v4, Lhux;->drF:[Lhuw;

    aget-object v2, v2, v9

    iget-boolean v2, v2, Lhuw;->drD:Z

    if-nez v2, :cond_5

    iget-object v2, v4, Lhux;->drF:[Lhuw;

    aget-object v2, v2, v9

    invoke-virtual {v2}, Lhuw;->aSt()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    iget-object v2, v4, Lhux;->drG:[Lhuw;

    aget-object v2, v2, v9

    iput-boolean v1, v2, Lhuw;->drD:Z

    .line 109
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 163
    :cond_6
    iget-object v2, v4, Lhux;->drG:[Lhuw;

    iget-object v3, v4, Lhux;->drF:[Lhuw;

    aget-object v3, v3, v9

    aput-object v3, v2, v9

    iget-object v2, v4, Lhux;->drG:[Lhuw;

    aget-object v2, v2, v9

    iget-short v3, v2, Lhuw;->drC:S

    add-int/lit8 v3, v3, 0x1

    int-to-short v3, v3

    iput-short v3, v2, Lhuw;->drC:S

    goto :goto_4

    .line 166
    :cond_7
    iget-object v0, v4, Lhux;->drF:[Lhuw;

    iget-short v1, p0, Lhuv;->drA:S

    aget-object v0, v0, v1

    .line 168
    iget-boolean v0, v0, Lhuw;->drD:Z

    if-eqz v0, :cond_8

    .line 169
    iget-short v0, p0, Lhuv;->dry:S

    goto :goto_3

    .line 171
    :cond_8
    iget-object v0, v4, Lhux;->drF:[Lhuw;

    iget-short v1, p0, Lhuv;->drA:S

    aget-object v0, v0, v1

    iget-short v0, v0, Lhuw;->drC:S

    goto :goto_3
.end method
