.class public final Lbpd;
.super Lbir;


# instance fields
.field private final aAk:Ljava/lang/Object;

.field private final aGZ:Lbpb;

.field private final aHa:Lboy;

.field private aHb:Z

.field private final auP:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbpb;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p2, v0}, Lbir;-><init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbpd;->auP:Ljava/lang/String;

    invoke-static {p2}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpb;

    iput-object v0, p0, Lbpd;->aGZ:Lbpb;

    iget-object v0, p0, Lbpd;->aGZ:Lbpb;

    iput-object p0, v0, Lbpb;->aGS:Lbpd;

    new-instance v0, Lboy;

    invoke-direct {v0}, Lboy;-><init>()V

    iput-object v0, p0, Lbpd;->aHa:Lboy;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbpd;->aAk:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbpd;->aHb:Z

    return-void
.end method

.method private b(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lbpd;->aHa:Lboy;

    iget-object v1, v0, Lboy;->aGM:Ljava/util/ArrayList;

    new-instance v2, Lboz;

    invoke-direct {v2, p1, p2, v3}, Lboz;-><init>(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, v0, Lboy;->aGM:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lboy;->aGN:I

    if-le v1, v2, :cond_0

    iget-object v1, v0, Lboy;->aGM:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private zZ()V
    .locals 6

    iget-boolean v0, p0, Lbpd;->aHb:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbip;->bF(Z)V

    iget-object v0, p0, Lbpd;->aHa:Lboy;

    iget-object v0, v0, Lboy;->aGM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lbpd;->aHa:Lboy;

    iget-object v1, v1, Lboy;->aGM:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboz;

    iget-object v1, v0, Lboz;->aGO:Lcom/google/android/gms/internal/qt;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lboz;->aGP:Lcom/google/android/gms/internal/qp;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PlayLoggerImpl"

    const-string v1, "Couldn\'t send cached log events to AndroidLog service.  Retaining in memory cache."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lbpd;->yT()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lbov;

    iget-object v5, p0, Lbpd;->auP:Ljava/lang/String;

    invoke-interface {v1, v5, v2, v3}, Lbov;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qt;Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_3
    iget-object v1, v0, Lboz;->aGO:Lcom/google/android/gms/internal/qt;

    iget-object v0, v0, Lboz;->aGP:Lcom/google/android/gms/internal/qp;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lbpd;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbov;

    iget-object v1, p0, Lbpd;->auP:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lbov;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qt;Ljava/util/List;)V

    :cond_5
    iget-object v0, p0, Lbpd;->aHa:Lboy;

    iget-object v0, v0, Lboy;->aGM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method


# virtual methods
.method protected final a(Lbjm;Lbiv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5d3f18

    iget-object v2, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjm;->f(Lbjj;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V
    .locals 3

    iget-object v1, p0, Lbpd;->aAk:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbpd;->aHb:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lbpd;->b(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lbpd;->zZ()V

    invoke-virtual {p0}, Lbpd;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbov;

    iget-object v2, p0, Lbpd;->auP:Ljava/lang/String;

    invoke-interface {v0, v2, p1, p2}, Lbov;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lbpd;->b(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lbpd;->b(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method final bL(Z)V
    .locals 2

    iget-object v1, p0, Lbpd;->aAk:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbpd;->aHb:Z

    iput-boolean p1, p0, Lbpd;->aHb:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbpd;->aHb:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbpd;->zZ()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lbow;->r(Landroid/os/IBinder;)Lbov;

    move-result-object v0

    return-object v0
.end method

.method public final start()V
    .locals 3

    iget-object v1, p0, Lbpd;->aAk:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lbpd;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbpd;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbpd;->aGZ:Lbpb;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lbpb;->aGT:Z

    invoke-virtual {p0}, Lbpd;->connect()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final stop()V
    .locals 3

    iget-object v1, p0, Lbpd;->aAk:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbpd;->aGZ:Lbpb;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lbpb;->aGT:Z

    invoke-virtual {p0}, Lbpd;->disconnect()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.service.START"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.internal.IPlayLogService"

    return-object v0
.end method
