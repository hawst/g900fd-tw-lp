.class public final Lgag;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/gms/location/DetectedActivity;)Liyz;
    .locals 2

    .prologue
    const/4 v0, 0x4

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 40
    :goto_0
    :pswitch_0
    new-instance v1, Liyz;

    invoke-direct {v1}, Liyz;-><init>()V

    invoke-virtual {v1, v0}, Liyz;->nQ(I)Liyz;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/location/DetectedActivity;->Ad()I

    move-result v1

    invoke-virtual {v0, v1}, Liyz;->nR(I)Liyz;

    move-result-object v0

    return-object v0

    .line 38
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static c(Lcom/google/android/gms/location/ActivityRecognitionResult;)Liyy;
    .locals 6

    .prologue
    .line 24
    new-instance v2, Liyy;

    invoke-direct {v2}, Liyy;-><init>()V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->getTime()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Liyy;->cq(J)Liyy;

    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->Ac()Ljava/util/List;

    move-result-object v3

    .line 28
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Liyz;

    iput-object v0, v2, Liyy;->dRr:[Liyz;

    .line 29
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 30
    iget-object v4, v2, Liyy;->dRr:[Liyz;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    invoke-static {v0}, Lgag;->a(Lcom/google/android/gms/location/DetectedActivity;)Liyz;

    move-result-object v0

    aput-object v0, v4, v1

    .line 29
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 33
    :cond_0
    return-object v2
.end method
