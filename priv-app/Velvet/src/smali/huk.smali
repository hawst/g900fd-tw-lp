.class public final Lhuk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private drg:Ljava/lang/String;

.field public final mGreco3DataManager:Lgix;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lgix;Lhym;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lhuk;->mGreco3DataManager:Lgix;

    .line 21
    iput-object p2, p0, Lhuk;->mSettings:Lhym;

    .line 22
    return-void
.end method


# virtual methods
.method public final aHG()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lhuk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 39
    iget-object v0, p0, Lhuk;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lhuk;->mGreco3DataManager:Lgix;

    invoke-virtual {v1, v0}, Lgix;->mN(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 41
    const-string v0, "en-US"

    .line 44
    :cond_0
    return-object v0
.end method

.method public final declared-synchronized aSb()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhuk;->drg:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lhuk;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-virtual {p0}, Lhuk;->aHG()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhuk;->drg:Ljava/lang/String;

    .line 29
    :cond_0
    iget-object v0, p0, Lhuk;->drg:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
