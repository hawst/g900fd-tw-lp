.class public final Ldya;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bOm:I

.field public bOn:I

.field private bQi:Ljava/lang/String;

.field private bQj:Ljava/lang/String;

.field private bQk:Z

.field public bQl:Z

.field private bQm:Z

.field private bQn:Z

.field private bQo:Z

.field private bQp:J

.field public bQq:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object v0, p0, Ldya;->bQi:Ljava/lang/String;

    .line 247
    iput-object v0, p0, Ldya;->bQj:Ljava/lang/String;

    .line 248
    iput-boolean v2, p0, Ldya;->bQk:Z

    .line 249
    iput-boolean v2, p0, Ldya;->bQl:Z

    .line 250
    iput-boolean v2, p0, Ldya;->bQm:Z

    .line 251
    iput-boolean v2, p0, Ldya;->bQq:Z

    .line 252
    iput-boolean v2, p0, Ldya;->bQn:Z

    .line 253
    iput-boolean v2, p0, Ldya;->bQo:Z

    .line 254
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldya;->bQp:J

    .line 255
    iput v2, p0, Ldya;->bOm:I

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Ldya;->bOn:I

    return-void
.end method


# virtual methods
.method public final aB(J)Ldya;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 355
    iget-boolean v0, p0, Ldya;->bQo:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 356
    iput-boolean v1, p0, Ldya;->bQn:Z

    .line 357
    iput-wide p1, p0, Ldya;->bQp:J

    .line 358
    return-object p0

    .line 355
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alp()Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 14

    .prologue
    .line 259
    new-instance v1, Lcom/google/android/search/shared/actions/utils/CardDecision;

    iget-object v2, p0, Ldya;->bQi:Ljava/lang/String;

    iget-object v3, p0, Ldya;->bQj:Ljava/lang/String;

    iget-boolean v4, p0, Ldya;->bQk:Z

    iget-boolean v5, p0, Ldya;->bQl:Z

    iget-boolean v6, p0, Ldya;->bQm:Z

    iget-boolean v7, p0, Ldya;->bQq:Z

    iget-boolean v8, p0, Ldya;->bQn:Z

    iget-boolean v9, p0, Ldya;->bQo:Z

    iget-wide v10, p0, Ldya;->bQp:J

    iget v12, p0, Ldya;->bOm:I

    iget v13, p0, Ldya;->bOn:I

    invoke-direct/range {v1 .. v13}, Lcom/google/android/search/shared/actions/utils/CardDecision;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZZZJII)V

    return-object v1
.end method

.method public final alq()Ldya;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 349
    iget-boolean v0, p0, Ldya;->bQo:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 350
    iput-boolean v1, p0, Ldya;->bQm:Z

    .line 351
    return-object p0

    .line 349
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alr()Ldya;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 362
    iget-boolean v0, p0, Ldya;->bQn:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 363
    iget-boolean v0, p0, Ldya;->bQm:Z

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 364
    iput-boolean v1, p0, Ldya;->bQo:Z

    .line 365
    return-object p0

    :cond_1
    move v0, v2

    .line 362
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)Ldya;
    .locals 1

    .prologue
    .line 286
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldya;->bQi:Ljava/lang/String;

    .line 287
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldya;->bQj:Ljava/lang/String;

    .line 288
    iput p3, p0, Ldya;->bOm:I

    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldya;->bQk:Z

    .line 290
    return-object p0
.end method

.method public final t(Ljava/lang/String;I)Ldya;
    .locals 1

    .prologue
    .line 269
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldya;->bQi:Ljava/lang/String;

    .line 270
    iput p2, p0, Ldya;->bOm:I

    .line 271
    return-object p0
.end method

.method public final u(Ljava/lang/String;I)Ldya;
    .locals 1

    .prologue
    .line 275
    iput-object p1, p0, Ldya;->bQj:Ljava/lang/String;

    .line 276
    iput p2, p0, Ldya;->bOm:I

    .line 277
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldya;->bQk:Z

    .line 278
    return-object p0
.end method
