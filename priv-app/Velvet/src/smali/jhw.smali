.class public final Ljhw;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile emu:[Ljhw;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private akQ:Ljava/lang/String;

.field private dPS:I

.field private ekT:I

.field private emv:I

.field public emw:[Ljhx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41552
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 41553
    iput v1, p0, Ljhw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljhw;->agq:Ljava/lang/String;

    iput v1, p0, Ljhw;->ekT:I

    iput v1, p0, Ljhw;->emv:I

    iput v1, p0, Ljhw;->dPS:I

    invoke-static {}, Ljhx;->bmn()[Ljhx;

    move-result-object v0

    iput-object v0, p0, Ljhw;->emw:[Ljhx;

    const-string v0, ""

    iput-object v0, p0, Ljhw;->akQ:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljhw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhw;->eCz:I

    .line 41554
    return-void
.end method

.method public static bmk()[Ljhw;
    .locals 2

    .prologue
    .line 41435
    sget-object v0, Ljhw;->emu:[Ljhw;

    if-nez v0, :cond_1

    .line 41436
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 41438
    :try_start_0
    sget-object v0, Ljhw;->emu:[Ljhw;

    if-nez v0, :cond_0

    .line 41439
    const/4 v0, 0x0

    new-array v0, v0, [Ljhw;

    sput-object v0, Ljhw;->emu:[Ljhw;

    .line 41441
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41443
    :cond_1
    sget-object v0, Ljhw;->emu:[Ljhw;

    return-object v0

    .line 41441
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 41037
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhw;->agq:Ljava/lang/String;

    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhw;->ekT:I

    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhw;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhw;->emv:I

    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhw;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljhw;->dPS:I

    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhw;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhw;->emw:[Ljhx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhx;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljhw;->emw:[Ljhx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljhx;

    invoke-direct {v3}, Ljhx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljhw;->emw:[Ljhx;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljhx;

    invoke-direct {v3}, Ljhx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhw;->emw:[Ljhx;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhw;->akQ:Ljava/lang/String;

    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljhw;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 41572
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 41573
    const/4 v0, 0x1

    iget-object v1, p0, Ljhw;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41575
    :cond_0
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 41576
    const/4 v0, 0x2

    iget v1, p0, Ljhw;->ekT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 41578
    :cond_1
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 41579
    const/4 v0, 0x3

    iget v1, p0, Ljhw;->emv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 41581
    :cond_2
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 41582
    const/4 v0, 0x4

    iget v1, p0, Ljhw;->dPS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 41584
    :cond_3
    iget-object v0, p0, Ljhw;->emw:[Ljhx;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljhw;->emw:[Ljhx;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 41585
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljhw;->emw:[Ljhx;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 41586
    iget-object v1, p0, Ljhw;->emw:[Ljhx;

    aget-object v1, v1, v0

    .line 41587
    if-eqz v1, :cond_4

    .line 41588
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 41585
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41592
    :cond_5
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 41593
    const/4 v0, 0x6

    iget-object v1, p0, Ljhw;->akQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41595
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 41596
    return-void
.end method

.method public final bbN()I
    .locals 1

    .prologue
    .line 41511
    iget v0, p0, Ljhw;->dPS:I

    return v0
.end method

.method public final bml()Z
    .locals 1

    .prologue
    .line 41481
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmm()I
    .locals 1

    .prologue
    .line 41492
    iget v0, p0, Ljhw;->emv:I

    return v0
.end method

.method public final getColor()I
    .locals 1

    .prologue
    .line 41473
    iget v0, p0, Ljhw;->ekT:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41451
    iget-object v0, p0, Ljhw;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 41600
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 41601
    iget v1, p0, Ljhw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 41602
    const/4 v1, 0x1

    iget-object v2, p0, Ljhw;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41605
    :cond_0
    iget v1, p0, Ljhw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 41606
    const/4 v1, 0x2

    iget v2, p0, Ljhw;->ekT:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41609
    :cond_1
    iget v1, p0, Ljhw;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 41610
    const/4 v1, 0x3

    iget v2, p0, Ljhw;->emv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41613
    :cond_2
    iget v1, p0, Ljhw;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 41614
    const/4 v1, 0x4

    iget v2, p0, Ljhw;->dPS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41617
    :cond_3
    iget-object v1, p0, Ljhw;->emw:[Ljhx;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljhw;->emw:[Ljhx;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 41618
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljhw;->emw:[Ljhx;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 41619
    iget-object v2, p0, Ljhw;->emw:[Ljhx;

    aget-object v2, v2, v0

    .line 41620
    if-eqz v2, :cond_4

    .line 41621
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 41618
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 41626
    :cond_6
    iget v1, p0, Ljhw;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 41627
    const/4 v1, 0x6

    iget-object v2, p0, Ljhw;->akQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41630
    :cond_7
    return v0
.end method

.method public final pX(I)Ljhw;
    .locals 1

    .prologue
    .line 41476
    const/16 v0, 0x4d2

    iput v0, p0, Ljhw;->ekT:I

    .line 41477
    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhw;->aez:I

    .line 41478
    return-object p0
.end method

.method public final pY(I)Ljhw;
    .locals 1

    .prologue
    .line 41495
    const/16 v0, 0x10e1

    iput v0, p0, Ljhw;->emv:I

    .line 41496
    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhw;->aez:I

    .line 41497
    return-object p0
.end method

.method public final pZ(I)Ljhw;
    .locals 1

    .prologue
    .line 41514
    const/4 v0, 0x3

    iput v0, p0, Ljhw;->dPS:I

    .line 41515
    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhw;->aez:I

    .line 41516
    return-object p0
.end method

.method public final sj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41533
    iget-object v0, p0, Ljhw;->akQ:Ljava/lang/String;

    return-object v0
.end method

.method public final sk()Z
    .locals 1

    .prologue
    .line 41544
    iget v0, p0, Ljhw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final vj(Ljava/lang/String;)Ljhw;
    .locals 1

    .prologue
    .line 41454
    if-nez p1, :cond_0

    .line 41455
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41457
    :cond_0
    iput-object p1, p0, Ljhw;->agq:Ljava/lang/String;

    .line 41458
    iget v0, p0, Ljhw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhw;->aez:I

    .line 41459
    return-object p0
.end method
