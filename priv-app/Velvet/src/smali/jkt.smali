.class public final Ljkt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eqk:[Ljkt;


# instance fields
.field private aez:I

.field private eql:Z

.field private eqm:Z

.field public eqn:Ljku;

.field public eqo:Ljlm;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 172
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 173
    iput v0, p0, Ljkt;->aez:I

    iput-boolean v0, p0, Ljkt;->eql:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljkt;->eqm:Z

    iput-object v1, p0, Ljkt;->eqn:Ljku;

    iput-object v1, p0, Ljkt;->eqo:Ljlm;

    iput-object v1, p0, Ljkt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkt;->eCz:I

    .line 174
    return-void
.end method

.method public static ar([B)Ljkt;
    .locals 1

    .prologue
    .line 272
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljkt;

    return-object v0
.end method

.method public static bot()[Ljkt;
    .locals 2

    .prologue
    .line 115
    sget-object v0, Ljkt;->eqk:[Ljkt;

    if-nez v0, :cond_1

    .line 116
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    :try_start_0
    sget-object v0, Ljkt;->eqk:[Ljkt;

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    new-array v0, v0, [Ljkt;

    sput-object v0, Ljkt;->eqk:[Ljkt;

    .line 121
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :cond_1
    sget-object v0, Ljkt;->eqk:[Ljkt;

    return-object v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljkt;->eqn:Ljku;

    if-nez v0, :cond_1

    new-instance v0, Ljku;

    invoke-direct {v0}, Ljku;-><init>()V

    iput-object v0, p0, Ljkt;->eqn:Ljku;

    :cond_1
    iget-object v0, p0, Ljkt;->eqn:Ljku;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljkt;->eql:Z

    iget v0, p0, Ljkt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkt;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljkt;->eqo:Ljlm;

    if-nez v0, :cond_2

    new-instance v0, Ljlm;

    invoke-direct {v0}, Ljlm;-><init>()V

    iput-object v0, p0, Ljkt;->eqo:Ljlm;

    :cond_2
    iget-object v0, p0, Ljkt;->eqo:Ljlm;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljkt;->eqm:Z

    iget v0, p0, Ljkt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkt;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x50 -> :sswitch_2
        0x5a -> :sswitch_3
        0x60 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Ljkt;->eqn:Ljku;

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x4

    iget-object v1, p0, Ljkt;->eqn:Ljku;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 193
    :cond_0
    iget v0, p0, Ljkt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 194
    const/16 v0, 0xa

    iget-boolean v1, p0, Ljkt;->eql:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 196
    :cond_1
    iget-object v0, p0, Ljkt;->eqo:Ljlm;

    if-eqz v0, :cond_2

    .line 197
    const/16 v0, 0xb

    iget-object v1, p0, Ljkt;->eqo:Ljlm;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 199
    :cond_2
    iget v0, p0, Ljkt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 200
    const/16 v0, 0xc

    iget-boolean v1, p0, Ljkt;->eqm:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 202
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 203
    return-void
.end method

.method public final bou()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Ljkt;->eql:Z

    return v0
.end method

.method public final bov()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Ljkt;->eqm:Z

    return v0
.end method

.method public final is(Z)Ljkt;
    .locals 1

    .prologue
    .line 134
    iput-boolean p1, p0, Ljkt;->eql:Z

    .line 135
    iget v0, p0, Ljkt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkt;->aez:I

    .line 136
    return-object p0
.end method

.method public final it(Z)Ljkt;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljkt;->eqm:Z

    .line 154
    iget v0, p0, Ljkt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkt;->aez:I

    .line 155
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 207
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 208
    iget-object v1, p0, Ljkt;->eqn:Ljku;

    if-eqz v1, :cond_0

    .line 209
    const/4 v1, 0x4

    iget-object v2, p0, Ljkt;->eqn:Ljku;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_0
    iget v1, p0, Ljkt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 213
    const/16 v1, 0xa

    iget-boolean v2, p0, Ljkt;->eql:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 216
    :cond_1
    iget-object v1, p0, Ljkt;->eqo:Ljlm;

    if-eqz v1, :cond_2

    .line 217
    const/16 v1, 0xb

    iget-object v2, p0, Ljkt;->eqo:Ljlm;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_2
    iget v1, p0, Ljkt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 221
    const/16 v1, 0xc

    iget-boolean v2, p0, Ljkt;->eqm:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 224
    :cond_3
    return v0
.end method
