.class public Ladz;
.super Lcom/android/launcher3/WallpaperCropActivity;
.source "PG"


# instance fields
.field private Hz:Landroid/view/ActionMode$Callback;

.field private Qx:Landroid/view/View$OnLongClickListener;

.field private Ub:Landroid/view/View;

.field private Uc:Z

.field private Ud:Landroid/view/View$OnClickListener;

.field private Ue:Landroid/widget/LinearLayout;

.field private Uf:Landroid/view/View;

.field private Ug:Landroid/view/ActionMode;

.field private Uh:Ljava/util/ArrayList;

.field private Ui:Ladc;

.field private Uj:Landroid/app/WallpaperInfo;

.field private Uk:I

.field private Ul:Landroid/app/WallpaperInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/android/launcher3/WallpaperCropActivity;-><init>()V

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ladz;->Uh:Ljava/util/ArrayList;

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Ladz;->Uk:I

    .line 1134
    return-void
.end method

.method static synthetic a(Ladz;I)I
    .locals 1

    .prologue
    .line 81
    const/4 v0, -0x1

    iput v0, p0, Ladz;->Uk:I

    return v0
.end method

.method private static a(Landroid/graphics/Point;Landroid/content/Context;Landroid/net/Uri;[BLandroid/content/res/Resources;IIZ)Landroid/graphics/Bitmap;
    .locals 19

    .prologue
    .line 791
    move-object/from16 v0, p0

    iget v6, v0, Landroid/graphics/Point;->x:I

    .line 792
    move-object/from16 v0, p0

    iget v7, v0, Landroid/graphics/Point;->y:I

    .line 795
    if-eqz p2, :cond_1

    .line 796
    new-instance v1, Ladx;

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v5, p6

    invoke-direct/range {v1 .. v10}, Ladx;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 805
    :goto_0
    invoke-virtual {v1}, Ladx;->kq()Landroid/graphics/Point;

    move-result-object v2

    .line 806
    if-eqz v2, :cond_0

    iget v3, v2, Landroid/graphics/Point;->x:I

    if-eqz v3, :cond_0

    iget v3, v2, Landroid/graphics/Point;->y:I

    if-nez v3, :cond_3

    .line 807
    :cond_0
    const/4 v1, 0x0

    .line 824
    :goto_1
    return-object v1

    .line 798
    :cond_1
    if-eqz p3, :cond_2

    .line 799
    new-instance v2, Ladx;

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v3, p3

    move/from16 v5, p6

    invoke-direct/range {v2 .. v10}, Ladx;-><init>([BLandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    move-object v1, v2

    goto :goto_0

    .line 802
    :cond_2
    new-instance v8, Ladx;

    const/4 v12, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p4

    move/from16 v11, p5

    move/from16 v13, p6

    move v14, v6

    move v15, v7

    invoke-direct/range {v8 .. v18}, Ladx;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    move-object v1, v8

    goto :goto_0

    .line 810
    :cond_3
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 811
    move/from16 v0, p6

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 812
    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v8, v2, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    aput v8, v4, v5

    const/4 v5, 0x1

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    aput v2, v4, v5

    .line 813
    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 814
    const/4 v2, 0x0

    const/4 v3, 0x0

    aget v3, v4, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    aput v3, v4, v2

    .line 815
    const/4 v2, 0x1

    const/4 v3, 0x1

    aget v3, v4, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    aput v3, v4, v2

    .line 817
    const/4 v2, 0x0

    aget v2, v4, v2

    float-to-int v2, v2

    const/4 v3, 0x1

    aget v3, v4, v3

    float-to-int v3, v3

    move/from16 v0, p7

    invoke-static {v2, v3, v6, v7, v0}, Lcom/android/launcher3/WallpaperCropActivity;->a(IIIIZ)Landroid/graphics/RectF;

    move-result-object v2

    .line 819
    invoke-virtual {v1, v2}, Ladx;->b(Landroid/graphics/RectF;)V

    .line 821
    invoke-virtual {v1}, Ladx;->ks()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 822
    invoke-virtual {v1}, Ladx;->kr()Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 824
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method static synthetic a(Ladz;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Ladz;->Ug:Landroid/view/ActionMode;

    return-object p1
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1155
    if-nez p1, :cond_1

    .line 1156
    const v0, 0x7f0401c2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1161
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Ladz;->a(Landroid/widget/FrameLayout;)V

    .line 1163
    const v0, 0x7f1104a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1165
    if-eqz p3, :cond_0

    .line 1166
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1167
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 1170
    :cond_0
    return-object v1

    :cond_1
    move-object v1, p1

    .line 1158
    goto :goto_0
.end method

.method static synthetic a(Ladz;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 81
    iget-object v0, p0, Ladz;->Ub:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladz;->Ub:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Ladz;->Ub:Landroid/view/View;

    :cond_0
    iput-object p1, p0, Ladz;->Ub:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Ladz;->Uk:I

    const v0, 0x7f0a0077

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Ladz;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Ladz;Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;Z)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ladz;->a(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;Z)V

    return-void
.end method

.method static synthetic a(Ladz;Z)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Ladz;->ar(Z)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;Z)V
    .locals 3

    .prologue
    .line 729
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 730
    const/4 v0, 0x0

    invoke-virtual {p2, v2, v0, p1}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 731
    invoke-virtual {p1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 732
    invoke-virtual {p2, v2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laeu;

    .line 733
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 734
    invoke-virtual {v1, v0}, Laeu;->setView(Landroid/view/View;)V

    .line 735
    if-eqz p3, :cond_0

    .line 736
    invoke-direct {p0, v0}, Ladz;->ae(Landroid/view/View;)V

    .line 738
    :cond_0
    iget-object v1, p0, Ladz;->Ud:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 729
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 740
    :cond_1
    return-void
.end method

.method static a(Landroid/widget/FrameLayout;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 913
    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 914
    new-instance v0, Laev;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, Laev;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 915
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 1089
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 1090
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1091
    const-string v4, "drawable"

    invoke-virtual {p1, v3, v4, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 1092
    if-eqz v4, :cond_1

    .line 1093
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_small"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "drawable"

    invoke-virtual {p1, v3, v5, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 1095
    if-eqz v3, :cond_0

    .line 1096
    new-instance v5, Laep;

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v5, p1, v4, v3}, Laep;-><init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;)V

    .line 1098
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1090
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1102
    :cond_1
    const-string v4, "Launcher.WallpaperPickerActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Couldn\'t find wallpaper "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1105
    :cond_2
    return-void
.end method

.method private a(Ljava/io/File;Landroid/graphics/Bitmap;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 987
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 988
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Ladz;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 990
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5f

    invoke-virtual {p2, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 991
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 992
    const/4 v0, 0x1

    .line 997
    :goto_0
    return v0

    .line 993
    :catch_0
    move-exception v1

    .line 994
    const-string v2, "Launcher.WallpaperPickerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while writing bitmap to file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private ae(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Ladz;->Qx:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 919
    return-void
.end method

.method private ar(Z)V
    .locals 3

    .prologue
    const/high16 v1, 0x100000

    .line 344
    if-eqz p1, :cond_1

    move v0, v1

    .line 345
    :goto_0
    invoke-virtual {p0}, Ladz;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v2, v1

    .line 347
    if-eq v0, v2, :cond_0

    .line 348
    invoke-virtual {p0}, Ladz;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setFlags(II)V

    .line 351
    :cond_0
    return-void

    .line 344
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/graphics/Point;Landroid/content/Context;Landroid/net/Uri;[BLandroid/content/res/Resources;IIZ)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    .line 81
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, p6

    move v7, p7

    invoke-static/range {v0 .. v7}, Ladz;->a(Landroid/graphics/Point;Landroid/content/Context;Landroid/net/Uri;[BLandroid/content/res/Resources;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/res/Resources;)Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 784
    new-instance v0, Landroid/graphics/Point;

    const v1, 0x7f0d005c

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0d005d

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method static synthetic b(Ladz;Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Ladz;->Ub:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/net/Uri;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 829
    iget-object v0, p0, Ladz;->Uh:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 831
    invoke-virtual {p0}, Ladz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401c2

    iget-object v2, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 833
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 834
    invoke-static {v6}, Ladz;->a(Landroid/widget/FrameLayout;)V

    .line 835
    iget-object v0, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 838
    const v0, 0x7f1104a3

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 839
    invoke-virtual {p0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Ladz;->b(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v4

    .line 841
    new-instance v0, Laec;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Laec;-><init>(Ladz;Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Point;Landroid/widget/ImageView;Landroid/widget/FrameLayout;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Laec;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 873
    new-instance v0, Laer;

    invoke-direct {v0, p1}, Laer;-><init>(Landroid/net/Uri;)V

    .line 874
    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 875
    invoke-virtual {v0, v6}, Laer;->setView(Landroid/view/View;)V

    .line 876
    invoke-direct {p0, v6}, Ladz;->ae(Landroid/view/View;)V

    .line 877
    invoke-direct {p0}, Ladz;->kv()V

    .line 878
    iget-object v0, p0, Ladz;->Ud:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 879
    if-nez p2, :cond_0

    .line 880
    iget-object v0, p0, Ladz;->Ud:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v6}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 882
    :cond_0
    return-void
.end method

.method static synthetic b(Ladz;Z)Z
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Ladz;->Uc:Z

    return p1
.end method

.method static synthetic c(Landroid/content/res/Resources;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 81
    invoke-static {p0}, Ladz;->b(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ladz;)Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Uf:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Ladz;)Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Ladz;->Uc:Z

    return v0
.end method

.method static synthetic e(Ladz;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Ug:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic f(Ladz;)Landroid/view/View$OnLongClickListener;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Qx:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method static synthetic g(Ladz;)Landroid/view/ActionMode$Callback;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Hz:Landroid/view/ActionMode$Callback;

    return-object v0
.end method

.method static synthetic h(Ladz;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic i(Ladz;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ladz;->kt()V

    return-void
.end method

.method static synthetic j(Ladz;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ladz;->kv()V

    return-void
.end method

.method private j(Landroid/graphics/Bitmap;)Z
    .locals 5

    .prologue
    .line 1007
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ladz;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "default_thumb.jpg"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1008
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ladz;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "default_thumb2.jpg"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1010
    const/16 v0, 0x10

    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_0

    .line 1011
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Ladz;->getFilesDir()Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_default_thumb2.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1010
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1013
    :cond_0
    invoke-direct {p0}, Ladz;->kx()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Ladz;->a(Ljava/io/File;Landroid/graphics/Bitmap;)Z

    move-result v0

    return v0
.end method

.method static synthetic k(Ladz;)I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Ladz;->Uk:I

    return v0
.end method

.method private kt()V
    .locals 3

    .prologue
    .line 670
    const v0, 0x7f11049e

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 673
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 674
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 675
    new-instance v2, Laeb;

    invoke-direct {v2, p0, v0}, Laeb;-><init>(Ladz;Landroid/widget/HorizontalScrollView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 684
    :cond_0
    return-void
.end method

.method private ku()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 687
    invoke-virtual {p0}, Ladz;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v6

    const-string v4, "datetaken"

    aput-object v4, v2, v7

    const-string v5, "datetaken DESC LIMIT 1"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Landroid/provider/MediaStore$Images$Media;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 694
    if-eqz v0, :cond_1

    .line 695
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 696
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 697
    invoke-virtual {p0}, Ladz;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    int-to-long v4, v1

    invoke-static {v2, v4, v5, v7, v3}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 700
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 702
    :cond_1
    return-object v3
.end method

.method private kv()V
    .locals 17

    .prologue
    .line 743
    const v1, 0x7f11049f

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 744
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    .line 745
    invoke-virtual/range {p0 .. p0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 748
    const/4 v7, 0x0

    .line 749
    const/4 v2, 0x0

    move v9, v2

    :goto_0
    const/4 v2, 0x2

    if-ge v9, v2, :cond_5

    .line 750
    const/4 v6, 0x0

    .line 751
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v10, :cond_4

    .line 752
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 757
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Laeu;

    if-eqz v3, :cond_0

    .line 760
    add-int/lit8 v2, v4, 0x1

    move v3, v2

    move-object v5, v1

    move v2, v4

    :goto_2
    move v8, v2

    .line 767
    :goto_3
    if-ge v8, v3, :cond_3

    .line 768
    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laeu;

    .line 769
    invoke-virtual {v2}, Laeu;->kC()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 770
    if-nez v9, :cond_1

    .line 771
    add-int/lit8 v2, v7, 0x1

    move/from16 v16, v6

    move v6, v2

    move/from16 v2, v16

    .line 767
    :goto_4
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move v7, v6

    move v6, v2

    goto :goto_3

    .line 762
    :cond_0
    check-cast v2, Landroid/widget/LinearLayout;

    .line 763
    const/4 v5, 0x0

    .line 764
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move/from16 v16, v5

    move-object v5, v2

    move/from16 v2, v16

    goto :goto_2

    .line 773
    :cond_1
    const v12, 0x7f0a0076

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 775
    invoke-virtual {v2, v12}, Laeu;->i(Ljava/lang/CharSequence;)V

    :cond_2
    move v2, v6

    move v6, v7

    goto :goto_4

    .line 751
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 749
    :cond_4
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_0

    .line 781
    :cond_5
    return-void
.end method

.method private kw()Ljava/util/ArrayList;
    .locals 15

    .prologue
    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v11, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 922
    invoke-virtual {p0}, Ladz;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 923
    new-instance v14, Ljava/util/ArrayList;

    const/16 v1, 0x18

    invoke-direct {v14, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 925
    invoke-static {v0}, Lacv;->a(Landroid/content/pm/PackageManager;)Lacv;

    move-result-object v4

    .line 926
    if-eqz v4, :cond_3

    .line 927
    invoke-virtual {v4}, Lacv;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 928
    const-string v1, "partner_wallpapers"

    const-string v3, "array"

    invoke-virtual {v4}, Lacv;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v3, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 930
    if-eqz v1, :cond_0

    .line 931
    invoke-virtual {v4}, Lacv;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v14, v0, v3, v1}, Ladz;->a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/lang/String;I)V

    .line 935
    :cond_0
    invoke-virtual {v4}, Lacv;->jT()Ljava/io/File;

    move-result-object v5

    .line 936
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 937
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    array-length v8, v6

    move v3, v7

    :goto_0
    if-ge v3, v8, :cond_3

    aget-object v9, v6, v3

    .line 938
    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 939
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 942
    const/16 v0, 0x2e

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    .line 943
    const-string v0, ""

    .line 944
    const/4 v13, -0x1

    if-lt v10, v13, :cond_1

    .line 945
    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 946
    invoke-virtual {v1, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 949
    :cond_1
    const-string v10, "_small"

    invoke-virtual {v1, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 951
    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v13, "_small"

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 955
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 956
    if-eqz v0, :cond_2

    .line 957
    new-instance v1, Laen;

    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v10, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v1, v9, v10}, Laen;-><init>(Ljava/io/File;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 963
    :cond_3
    invoke-virtual {p0}, Ladz;->ky()Landroid/util/Pair;

    move-result-object v1

    .line 964
    if-eqz v1, :cond_4

    .line 966
    :try_start_0
    invoke-virtual {p0}, Ladz;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v3

    .line 967
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v14, v3, v5, v0}, Ladz;->a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 972
    :cond_4
    :goto_1
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lacv;->jS()Z

    move-result v0

    if-nez v0, :cond_7

    .line 974
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_9

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    const-string v0, "default_wallpaper"

    const-string v1, "drawable"

    const-string v3, "android"

    invoke-virtual {v4, v0, v1, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0}, Ladz;->kx()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_2
    if-eqz v11, :cond_6

    new-instance v2, Laep;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v2, v4, v5, v1}, Laep;-><init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;)V

    .line 978
    :cond_6
    :goto_3
    if-eqz v2, :cond_7

    .line 979
    invoke-virtual {v14, v7, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 982
    :cond_7
    return-object v14

    .line 974
    :cond_8
    invoke-virtual {p0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Ladz;->b(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v1, v5}, Lcom/android/launcher3/WallpaperCropActivity;->b(Landroid/content/res/Resources;I)I

    move-result v6

    move-object v1, p0

    move-object v3, v2

    invoke-static/range {v0 .. v7}, Ladz;->a(Landroid/graphics/Point;Landroid/content/Context;Landroid/net/Uri;[BLandroid/content/res/Resources;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-direct {p0, v0}, Ladz;->j(Landroid/graphics/Bitmap;)Z

    move-result v11

    goto :goto_2

    :cond_9
    invoke-direct {p0}, Ladz;->kx()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_4
    if-eqz v11, :cond_6

    new-instance v2, Laem;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v2, v1}, Laem;-><init>(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Ladz;->b(Landroid/content/res/Resources;)Landroid/graphics/Point;

    move-result-object v1

    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v8

    iget v9, v1, Landroid/graphics/Point;->x:I

    iget v10, v1, Landroid/graphics/Point;->y:I

    move v13, v12

    invoke-virtual/range {v8 .. v13}, Landroid/app/WallpaperManager;->getBuiltInDrawable(IIZFF)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_c

    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v5, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v7, v7, v5, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v4, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :goto_5
    if-eqz v0, :cond_b

    invoke-direct {p0, v0}, Ladz;->j(Landroid/graphics/Bitmap;)Z

    move-result v11

    goto :goto_4

    :catch_0
    move-exception v0

    goto/16 :goto_1

    :cond_b
    move v11, v7

    goto :goto_4

    :cond_c
    move-object v0, v2

    goto :goto_5

    :cond_d
    move v11, v7

    goto/16 :goto_2
.end method

.method private kx()Ljava/io/File;
    .locals 4

    .prologue
    .line 1001
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ladz;->getFilesDir()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_default_thumb2.jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic l(Ladz;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Ud:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic m(Ladz;)Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ladz;->Ub:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Laif;ZZLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 359
    new-instance v0, Laed;

    invoke-direct {v0, p0, p4}, Laed;-><init>(Ladz;Ljava/lang/Runnable;)V

    .line 368
    invoke-super {p0, p1, p2, p3, v0}, Lcom/android/launcher3/WallpaperCropActivity;->a(Laif;ZZLjava/lang/Runnable;)V

    .line 372
    return-void
.end method

.method public final a(Landroid/app/WallpaperInfo;)V
    .locals 1

    .prologue
    .line 1116
    iput-object p1, p0, Ladz;->Ul:Landroid/app/WallpaperInfo;

    .line 1117
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    iput-object v0, p0, Ladz;->Uj:Landroid/app/WallpaperInfo;

    .line 1118
    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 1176
    invoke-virtual {p0, p1, p2}, Ladz;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1177
    return-void
.end method

.method protected final aq(Z)V
    .locals 4

    .prologue
    .line 324
    if-nez p1, :cond_0

    .line 325
    iget-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CropView;->setVisibility(I)V

    .line 331
    :goto_0
    iget-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    new-instance v1, Laea;

    invoke-direct {v1, p0, p1}, Laea;-><init>(Ladz;Z)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher3/CropView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 341
    return-void

    .line 327
    :cond_0
    invoke-direct {p0, p1}, Ladz;->ar(Z)V

    goto :goto_0
.end method

.method protected final ed()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    const v0, 0x7f0401c0

    invoke-virtual {p0, v0}, Ladz;->setContentView(I)V

    .line 378
    const v0, 0x7f11049b

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CropView;

    iput-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    .line 379
    iget-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CropView;->setVisibility(I)V

    .line 381
    const v0, 0x7f11049d

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ladz;->Uf:Landroid/view/View;

    .line 382
    iget-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    new-instance v1, Laee;

    invoke-direct {v1, p0}, Laee;-><init>(Ladz;)V

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CropView;->a(Lti;)V

    .line 425
    new-instance v0, Laeg;

    invoke-direct {v0, p0}, Laeg;-><init>(Ladz;)V

    iput-object v0, p0, Ladz;->Ud:Landroid/view/View$OnClickListener;

    .line 442
    new-instance v0, Laeh;

    invoke-direct {v0, p0}, Laeh;-><init>(Ladz;)V

    iput-object v0, p0, Ladz;->Qx:Landroid/view/View$OnLongClickListener;

    .line 463
    invoke-direct {p0}, Ladz;->kw()Ljava/util/ArrayList;

    move-result-object v1

    .line 464
    const v0, 0x7f1104a0

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    .line 465
    new-instance v0, Laeq;

    invoke-direct {v0, p0, v1}, Laeq;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    .line 466
    iget-object v1, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0, v3}, Ladz;->a(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;Z)V

    .line 469
    new-instance v0, Ladc;

    invoke-direct {v0, p0}, Ladc;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Ladz;->Ui:Ladc;

    .line 470
    iget-object v0, p0, Ladz;->Ui:Ladc;

    invoke-virtual {v0}, Ladc;->jZ()V

    .line 471
    iget-object v0, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    iget-object v1, p0, Ladz;->Ui:Ladc;

    invoke-direct {p0, v0, v1, v4}, Ladz;->a(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;Z)V

    .line 474
    const v0, 0x7f1104a1

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 476
    new-instance v1, Labt;

    invoke-direct {v1, p0}, Labt;-><init>(Landroid/content/Context;)V

    .line 477
    new-instance v2, Laei;

    invoke-direct {v2, p0, v0, v1}, Laei;-><init>(Ladz;Landroid/widget/LinearLayout;Labt;)V

    invoke-virtual {v1, v2}, Labt;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 487
    const v0, 0x7f1104a2

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 489
    new-instance v1, Ladm;

    invoke-direct {v1, p0}, Ladm;-><init>(Landroid/content/Context;)V

    .line 491
    invoke-direct {p0, v0, v1, v3}, Ladz;->a(Landroid/view/ViewGroup;Landroid/widget/BaseAdapter;Z)V

    .line 494
    const v0, 0x7f11049f

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 495
    invoke-virtual {p0}, Ladz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0401c1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 497
    invoke-static {v1}, Ladz;->a(Landroid/widget/FrameLayout;)V

    .line 498
    invoke-virtual {v0, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 501
    invoke-direct {p0}, Ladz;->ku()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 502
    if-eqz v0, :cond_0

    .line 503
    const v0, 0x7f1104a3

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 505
    invoke-direct {p0}, Ladz;->ku()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 506
    invoke-virtual {p0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 507
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 511
    :cond_0
    new-instance v0, Laeo;

    invoke-direct {v0}, Laeo;-><init>()V

    .line 512
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 513
    invoke-virtual {v0, v1}, Laeo;->setView(Landroid/view/View;)V

    .line 514
    iget-object v0, p0, Ladz;->Ud:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 518
    iget-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    new-instance v1, Laej;

    invoke-direct {v1, p0}, Laej;-><init>(Ladz;)V

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CropView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 533
    invoke-direct {p0}, Ladz;->kv()V

    .line 536
    invoke-direct {p0}, Ladz;->kt()V

    .line 539
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 540
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 541
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v4, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 542
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 543
    iget-object v1, p0, Ladz;->Ue:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 547
    invoke-virtual {p0}, Ladz;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 548
    const v1, 0x7f04000c

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 549
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Laek;

    invoke-direct {v1, p0}, Laek;-><init>(Ladz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 563
    const v0, 0x7f110072

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ladz;->Tz:Landroid/view/View;

    .line 566
    new-instance v0, Lael;

    invoke-direct {v0, p0}, Lael;-><init>(Ladz;)V

    iput-object v0, p0, Ladz;->Hz:Landroid/view/ActionMode$Callback;

    .line 653
    return-void
.end method

.method public final kA()Ladc;
    .locals 1

    .prologue
    .line 1112
    iget-object v0, p0, Ladz;->Ui:Ladc;

    return-object v0
.end method

.method public ky()Landroid/util/Pair;
    .locals 3

    .prologue
    const v1, 0x7f0f0009

    .line 1078
    invoke-virtual {p0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 1080
    :try_start_0
    invoke-virtual {p0}, Ladz;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1081
    new-instance v0, Landroid/util/Pair;

    const v2, 0x7f0f0009

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1083
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kz()Lcom/android/launcher3/CropView;
    .locals 1

    .prologue
    .line 1108
    iget-object v0, p0, Ladz;->Ty:Lcom/android/launcher3/CropView;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 885
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    if-ne p2, v4, :cond_1

    .line 886
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 887
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 888
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ladz;->b(Landroid/net/Uri;Z)V

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 890
    :cond_1
    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    .line 891
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    .line 894
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 895
    iget-object v1, p0, Ladz;->Uj:Landroid/app/WallpaperInfo;

    .line 896
    iget-object v2, p0, Ladz;->Ul:Landroid/app/WallpaperInfo;

    .line 897
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    .line 899
    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    :cond_2
    invoke-virtual {p0, v4}, Ladz;->setResult(I)V

    .line 907
    invoke-virtual {p0}, Ladz;->finish()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 720
    const-string v0, "TEMP_WALLPAPER_TILES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 721
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 722
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Ladz;->b(Landroid/net/Uri;Z)V

    goto :goto_0

    .line 724
    :cond_0
    const-string v0, "SELECTED_INDEX"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ladz;->Uk:I

    .line 725
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 715
    const-string v0, "TEMP_WALLPAPER_TILES"

    iget-object v1, p0, Ladz;->Uh:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 716
    const-string v0, "SELECTED_INDEX"

    iget v1, p0, Ladz;->Uk:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 717
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 706
    invoke-super {p0}, Lcom/android/launcher3/WallpaperCropActivity;->onStop()V

    .line 707
    const v0, 0x7f11049d

    invoke-virtual {p0, v0}, Ladz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ladz;->Uf:Landroid/view/View;

    .line 708
    iget-object v0, p0, Ladz;->Uf:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 709
    iget-object v0, p0, Ladz;->Uf:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 710
    iget-object v0, p0, Ladz;->Uf:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 712
    :cond_0
    return-void
.end method

.method public final v(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 314
    iget-object v0, p0, Ladz;->Uf:Landroid/view/View;

    float-to-int v1, p1

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 315
    return-void
.end method
