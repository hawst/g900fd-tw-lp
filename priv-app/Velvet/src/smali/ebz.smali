.class public Lebz;
.super Lecp;
.source "PG"


# instance fields
.field private synthetic bUB:Lebm;


# direct methods
.method public constructor <init>(Lebm;)V
    .locals 0

    .prologue
    .line 1027
    iput-object p1, p0, Lebz;->bUB:Lebm;

    invoke-direct {p0}, Lecp;-><init>()V

    return-void
.end method


# virtual methods
.method public final UK()V
    .locals 1

    .prologue
    .line 1162
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1167
    :goto_0
    return-void

    .line 1166
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    invoke-virtual {v0}, Lebq;->UK()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 1

    .prologue
    .line 1211
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1215
    :goto_0
    return-void

    .line 1214
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    invoke-virtual {v0, p1}, Lebq;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 4

    .prologue
    .line 1225
    const-string v0, "SearchOverlay"

    const-string v1, "This shouldn\'t be called as GEL doesn\'t support showing voice actions."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1226
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljava/util/List;ZLcom/google/android/shared/search/SuggestionLogInfo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1032
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1077
    :goto_0
    return-void

    .line 1039
    :cond_0
    if-nez p2, :cond_1

    .line 1040
    const-string v0, "SearchOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showSuggestions(query="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",, isFinal="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    :cond_1
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUe:Lebc;

    invoke-virtual {v0}, Lebc;->amE()V

    .line 1046
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/api/SuggestionsGroup;

    .line 1048
    if-nez v0, :cond_3

    .line 1049
    const-string v2, "SearchOverlay"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "showSuggestions(query="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", suggestions="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isFinal="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/SuggestionsGroup;->getType()I

    move-result v2

    if-nez v2, :cond_4

    .line 1055
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/SuggestionsGroup;->abe()Ljava/util/List;

    move-result-object v0

    .line 1057
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1058
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1064
    if-eqz v0, :cond_2

    instance-of v2, v0, Landroid/text/Spanned;

    if-eqz v2, :cond_2

    .line 1065
    iget-object v2, p0, Lebz;->bUB:Lebm;

    iget-object v2, v2, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    check-cast v0, Landroid/text/Spanned;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/search/searchplate/SearchPlate;->a(Landroid/text/Spanned;Z)V

    goto :goto_1

    .line 1069
    :cond_4
    iget-object v2, p0, Lebz;->bUB:Lebm;

    iget-object v2, v2, Lebm;->bUe:Lebc;

    invoke-virtual {v2, v0}, Lebc;->a(Lcom/google/android/search/shared/api/SuggestionsGroup;)V

    goto :goto_1

    .line 1073
    :cond_5
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUe:Lebc;

    invoke-virtual {v0, p1, p3, p4}, Lebc;->a(Lcom/google/android/shared/search/Query;ZLcom/google/android/shared/search/SuggestionLogInfo;)V

    .line 1074
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, p0, Lebz;->bUB:Lebm;

    iget-object v1, v1, Lebm;->bUe:Lebc;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lebc;->hb(I)I

    move-result v1

    iget-object v2, p0, Lebz;->bUB:Lebm;

    iget-object v2, v2, Lebm;->bUe:Lebc;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lebc;->hb(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->au(II)V

    goto/16 :goto_0
.end method

.method public final a([Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1102
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1131
    :cond_0
    :goto_0
    return-void

    .line 1107
    :cond_1
    array-length v2, p1

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, p1, v0

    .line 1108
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 1109
    const-string v4, "scrim_transition_to_solid"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1110
    iget-object v4, p0, Lebz;->bUB:Lebm;

    iget-object v4, v4, Lebm;->bUj:Lebe;

    invoke-virtual {v4}, Lebe;->amH()V

    .line 1111
    const-string v4, "scrim_transition_to_solid"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1107
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1115
    :cond_3
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SearchPlate;->afg()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUj:Lebe;

    invoke-virtual {v0}, Lebe;->amI()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1117
    :cond_4
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUr:[Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 1122
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iput-object p1, v0, Lebm;->bUr:[Landroid/content/Intent;

    goto :goto_0

    .line 1125
    :cond_5
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->mIntentStarter:Leqp;

    invoke-interface {v0, p1}, Leqp;->b([Landroid/content/Intent;)Z

    move-result v0

    .line 1126
    if-eqz v0, :cond_0

    .line 1127
    iget-object v0, p0, Lebz;->bUB:Lebm;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lebm;->bUv:Z

    .line 1128
    iget-object v0, p0, Lebz;->bUB:Lebm;

    invoke-virtual {v0}, Lebm;->amL()V

    goto :goto_0
.end method

.method public final d(IIZ)V
    .locals 1

    .prologue
    .line 1135
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1140
    :goto_0
    return-void

    .line 1139
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    invoke-virtual {v0, p1, p2, p3}, Lebm;->d(IIZ)V

    goto :goto_0
.end method

.method public final e(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1144
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1149
    :goto_0
    return-void

    .line 1148
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lebq;->a(ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method public final en(I)V
    .locals 1

    .prologue
    .line 1171
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1180
    :cond_0
    :goto_0
    return-void

    .line 1175
    :cond_1
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    invoke-virtual {v0, p1}, Lebq;->en(I)V

    .line 1176
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTL:Lecd;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1178
    :cond_2
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->amQ()V

    goto :goto_0
.end method

.method public final eo(I)V
    .locals 1

    .prologue
    .line 1184
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1189
    :goto_0
    return-void

    .line 1188
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->mSpeechLevelSource:Lequ;

    invoke-virtual {v0, p1}, Lequ;->gp(I)V

    goto :goto_0
.end method

.method public final fv(I)V
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1158
    :goto_0
    return-void

    .line 1157
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    invoke-virtual {v0, p1}, Lebq;->fv(I)V

    goto :goto_0
.end method

.method public final gb(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1207
    :goto_0
    return-void

    .line 1206
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    invoke-virtual {v0, p1}, Lebq;->n(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final j([B)V
    .locals 4

    .prologue
    .line 1230
    const-string v0, "SearchOverlay"

    const-string v1, "This shouldn\'t be called as GEL doesn\'t support clockwork result."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1231
    return-void
.end method

.method public final w(Lcom/google/android/shared/search/Query;)V
    .locals 4

    .prologue
    .line 1219
    const-string v0, "SearchOverlay"

    const-string v1, "This shouldn\'t be called as GEL doesn\'t support handling plain queries."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1220
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1198
    :goto_0
    return-void

    .line 1197
    :cond_0
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    invoke-virtual {v0, p1, p2}, Lebq;->x(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final z(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 1082
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1097
    :cond_0
    :goto_0
    return-void

    .line 1089
    :cond_1
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1091
    :cond_2
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-boolean v0, v0, Lebm;->bUq:Z

    if-eqz v0, :cond_3

    .line 1092
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object p1

    .line 1094
    :cond_3
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iput-object p1, v0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1095
    iget-object v0, p0, Lebz;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {p1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    goto :goto_0
.end method
