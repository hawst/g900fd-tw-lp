.class public final Lelv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static bKT:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lelv;->bKT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;
    .locals 2

    .prologue
    .line 116
    invoke-static {p0}, Lelv;->az(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lelw;

    invoke-direct {v1, p0, p1, p2}, Lelw;-><init>(Landroid/widget/TextView;Ljava/lang/CharSequence;F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)Landroid/view/ViewPropertyAnimator;
    .locals 1

    .prologue
    .line 102
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, p1, v0}, Lelv;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/animation/LayoutTransition;Landroid/view/View;)Livq;
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159
    new-instance v0, Lelx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lelx;-><init>(Landroid/animation/LayoutTransition;Landroid/view/View;)V

    return-object v0
.end method

.method public static varargs a(JF[Landroid/view/ViewPropertyAnimator;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 132
    .line 133
    array-length v2, p3

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p3, v0

    .line 134
    int-to-long v4, v1

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->getDuration()J

    move-result-wide v6

    long-to-float v3, v6

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v3, v5

    float-to-long v6, v3

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 136
    add-int/lit8 v1, v1, 0x1

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 146
    const v0, 0x7f0c006a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 147
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    int-to-long v2, v0

    invoke-virtual {v1, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 148
    return-void
.end method

.method public static aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 65
    :cond_0
    invoke-static {p0}, Lelv;->az(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method static az(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 4

    .prologue
    .line 47
    invoke-static {}, Lenu;->auR()V

    .line 48
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lelv;->bKT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static ba(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lelv;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 57
    invoke-static {p0}, Lelv;->az(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lely;

    invoke-direct {v1, p0, p1}, Lely;-><init>(Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
