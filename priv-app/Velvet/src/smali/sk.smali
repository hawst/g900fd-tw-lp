.class final Lsk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lsp;


# instance fields
.field private synthetic zl:Lsi;


# direct methods
.method private constructor <init>(Lsi;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lsk;->zl:Lsi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lsi;B)V
    .locals 0

    .prologue
    .line 344
    invoke-direct {p0, p1}, Lsk;-><init>(Lsi;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;)J
    .locals 11

    .prologue
    const-wide/16 v2, -0x1

    const/4 v7, 0x0

    .line 349
    const-string v0, "packageName"

    invoke-static {p1, v0}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    const-string v0, "className"

    invoke-static {p1, v0}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 351
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    :cond_0
    :goto_0
    return-wide v2

    .line 356
    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v1, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :try_start_0
    iget-object v5, p0, Lsk;->zl:Lsi;

    invoke-static {v5}, Lsi;->a(Lsi;)Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :goto_1
    iget-object v1, p0, Lsk;->zl:Lsi;

    invoke-static {v1}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v4, "spanX"

    const-string v5, "spanX"

    invoke-static {p1, v5}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v1, p0, Lsk;->zl:Lsi;

    invoke-static {v1}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v4, "spanY"

    const-string v5, "spanY"

    invoke-static {p1, v5}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 376
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v4

    .line 378
    :cond_2
    :goto_2
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v6

    if-le v6, v4, :cond_6

    .line 380
    :cond_3
    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 381
    const-string v5, "extra"

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 385
    const-string v5, "key"

    invoke-static {p1, v5}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 386
    const-string v6, "value"

    invoke-static {p1, v6}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 387
    if-eqz v5, :cond_4

    if-eqz v6, :cond_4

    .line 388
    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 360
    :catch_0
    move-exception v0

    iget-object v0, p0, Lsk;->zl:Lsi;

    invoke-static {v0}, Lsi;->a(Lsi;)Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v7

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 362
    new-instance v0, Landroid/content/ComponentName;

    aget-object v1, v1, v7

    invoke-direct {v0, v1, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :try_start_1
    iget-object v1, p0, Lsk;->zl:Lsi;

    invoke-static {v1}, Lsi;->a(Lsi;)Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 366
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can\'t find widget provider: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 390
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Widget extras must have a key and value"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Widgets can contain only extras"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_6
    iget-object v4, p0, Lsk;->zl:Lsi;

    invoke-static {v4}, Lsi;->c(Lsi;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    .line 400
    :try_start_2
    iget-object v5, p0, Lsk;->zl:Lsi;

    invoke-static {v5}, Lsi;->d(Lsi;)Landroid/appwidget/AppWidgetHost;

    move-result-object v5

    invoke-virtual {v5}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    move-result v5

    .line 402
    invoke-virtual {v4, v5, v0}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 403
    const-string v1, "AutoInstalls"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to bind app widget id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 425
    :catch_2
    move-exception v0

    move-object v10, v0

    move-wide v0, v2

    move-object v2, v10

    .line 426
    const-string v3, "AutoInstalls"

    const-string v4, "Problem allocating appWidgetId"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    move-wide v2, v0

    .line 428
    goto/16 :goto_0

    .line 407
    :cond_7
    :try_start_3
    iget-object v4, p0, Lsk;->zl:Lsi;

    invoke-static {v4}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v6, "itemType"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 408
    iget-object v4, p0, Lsk;->zl:Lsi;

    invoke-static {v4}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v6, "appWidgetId"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 409
    iget-object v4, p0, Lsk;->zl:Lsi;

    invoke-static {v4}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v6, "appWidgetProvider"

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iget-object v4, p0, Lsk;->zl:Lsi;

    invoke-static {v4}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v6, "_id"

    iget-object v7, p0, Lsk;->zl:Lsi;

    invoke-static {v7}, Lsi;->e(Lsi;)Lsn;

    move-result-object v7

    invoke-interface {v7}, Lsn;->eE()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 411
    iget-object v4, p0, Lsk;->zl:Lsi;

    invoke-static {v4}, Lsi;->e(Lsi;)Lsn;

    move-result-object v4

    iget-object v6, p0, Lsk;->zl:Lsi;

    invoke-static {v6}, Lsi;->f(Lsi;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    iget-object v7, p0, Lsk;->zl:Lsi;

    invoke-static {v7}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lsn;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 412
    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-gez v4, :cond_8

    .line 413
    iget-object v0, p0, Lsk;->zl:Lsi;

    invoke-static {v0}, Lsi;->d(Lsi;)Landroid/appwidget/AppWidgetHost;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    goto/16 :goto_0

    .line 418
    :cond_8
    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 419
    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.android.launcher.action.APPWIDGET_DEFAULT_WORKSPACE_CONFIGURE"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 420
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 421
    invoke-virtual {v4, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 422
    const-string v0, "appWidgetId"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 423
    iget-object v0, p0, Lsk;->zl:Lsi;

    invoke-static {v0}, Lsi;->c(Lsi;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_9
    move-wide v0, v2

    .line 427
    goto/16 :goto_3
.end method
