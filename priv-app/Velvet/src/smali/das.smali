.class public Ldas;
.super Lecq;
.source "PG"


# instance fields
.field private final bpZ:Ldcs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V
    .locals 1
    .param p5    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct/range {p0 .. p5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    .line 41
    new-instance v0, Ldcs;

    invoke-direct {v0}, Ldcs;-><init>()V

    iput-object v0, p0, Ldas;->bpZ:Ldcs;

    .line 42
    return-void
.end method

.method private Vf()Ldah;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Ldas;->isConnected()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 46
    iget-object v0, p0, Ldas;->bVb:Lecg;

    check-cast v0, Ldah;

    return-object v0
.end method

.method private jq(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    invoke-virtual {p0}, Ldas;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_0
    const-string v2, "LocalSearchServiceClient"

    const-string v3, "Unconnected to search service: %s"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const/4 v4, 0x5

    invoke-static {v4, v2, v3, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 82
    goto :goto_0
.end method


# virtual methods
.method public final Nf()Ldda;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0}, Lcjt;->Nf()Ldda;

    move-result-object v0

    return-object v0
.end method

.method public final Ng()V
    .locals 1

    .prologue
    .line 129
    const-string v0, "ignoring clear srp cache"

    invoke-direct {p0, v0}, Ldas;->jq(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0}, Lcjt;->Ng()V

    .line 132
    :cond_0
    return-void
.end method

.method public final Nl()V
    .locals 1

    .prologue
    .line 141
    const-string v0, "can\'t open commit in chrome"

    invoke-direct {p0, v0}, Ldas;->jq(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0}, Lcjt;->Nl()V

    .line 144
    :cond_0
    return-void
.end method

.method public final UG()Ldep;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UG()Ldep;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;Z)V
    .locals 4

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lecq;->a(Landroid/os/Bundle;Z)V

    .line 72
    if-nez p2, :cond_0

    invoke-virtual {p0}, Ldas;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Ldas;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iget-wide v2, p0, Lecq;->bpc:J

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/search/core/state/QueryState;->a(JLandroid/os/Bundle;)V

    .line 75
    :cond_0
    return-void
.end method

.method public final a(Lddc;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Ldas;->bpZ:Ldcs;

    invoke-virtual {v0, p1}, Ldcs;->a(Lddc;)V

    .line 148
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 135
    const-string v0, "can\'t dump last results"

    invoke-direct {p0, v0}, Ldas;->jq(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcjt;->a(Ljava/io/PrintWriter;)V

    .line 138
    :cond_0
    return-void
.end method

.method protected a(ZZZ)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public final b(Landroid/graphics/Point;)V
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Ldas;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcjt;->b(Landroid/graphics/Point;)V

    .line 116
    invoke-virtual {p0}, Ldas;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->dj(Z)V

    .line 118
    :cond_0
    return-void
.end method

.method public final b(Lddc;)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Ldas;->bpZ:Ldcs;

    invoke-virtual {v0, p1}, Ldcs;->b(Lddc;)V

    .line 152
    return-void
.end method

.method public final b(Letj;)V
    .locals 1

    .prologue
    .line 171
    const-string v0, "ignoring dump request"

    invoke-direct {p0, v0}, Ldas;->jq(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 174
    :cond_0
    return-void
.end method

.method public final cN()Z
    .locals 1

    .prologue
    .line 121
    const-string v0, "not consuming back button"

    invoke-direct {p0, v0}, Ldas;->jq(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0}, Lcjt;->cN()Z

    move-result v0

    .line 124
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cl(Z)Landroid/view/View;
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Ldas;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->UH()Lcjt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcjt;->cl(Z)Landroid/view/View;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    .line 158
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 159
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final isActive()Z
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Ldas;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0}, Ldah;->isActive()Z

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onConnected()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lecq;->onConnected()V

    .line 103
    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldah;->a(Ldas;)V

    .line 104
    iget-object v0, p0, Ldas;->bpZ:Ldcs;

    invoke-direct {p0}, Ldas;->Vf()Ldah;

    move-result-object v1

    invoke-virtual {v1}, Ldah;->UH()Lcjt;

    move-result-object v1

    invoke-virtual {v1}, Lcjt;->Nf()Ldda;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcs;->b(Ldda;)V

    .line 105
    return-void
.end method

.method protected final onDisconnected()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Ldas;->bpZ:Ldcs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldcs;->b(Ldda;)V

    .line 110
    invoke-super {p0}, Lecq;->onDisconnected()V

    .line 111
    return-void
.end method
