.class public final Lhqs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aoG:Lhla;

.field private final mHttpActionExecutor:Lhlr;


# direct methods
.method public constructor <init>(Lhlr;Lhla;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lhqs;->mHttpActionExecutor:Lhlr;

    .line 21
    iput-object p2, p0, Lhqs;->aoG:Lhla;

    .line 22
    return-void
.end method

.method private static a(Ljkw;)Ljkt;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 76
    sget-object v1, Ljkw;->eqw:Ljsm;

    invoke-virtual {v0, v1, p0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 77
    return-object v0
.end method

.method private static b(Ljkw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Ljkw;->boC()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    .line 85
    :cond_0
    invoke-virtual {p0}, Ljkw;->aiw()Ljava/lang/String;

    move-result-object v0

    .line 87
    invoke-virtual {p0}, Ljkw;->boD()Ljkw;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/SetReminderAction;Lefk;)V
    .locals 3

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiC()Ljkw;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lhqs;->mHttpActionExecutor:Lhlr;

    invoke-static {v0}, Lhqs;->a(Ljkw;)Ljkt;

    move-result-object v2

    invoke-static {v0}, Lhqs;->b(Ljkw;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2}, Lhlr;->a(Ljkt;Ljava/lang/String;Lefk;)V

    .line 72
    return-void
.end method

.method public final c(Lcom/google/android/search/shared/actions/SetReminderAction;)Z
    .locals 3

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiC()Ljkw;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lhqs;->mHttpActionExecutor:Lhlr;

    invoke-static {v0}, Lhqs;->a(Ljkw;)Ljkt;

    move-result-object v2

    invoke-static {v0}, Lhqs;->b(Ljkw;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lhlr;->a(Ljkt;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
