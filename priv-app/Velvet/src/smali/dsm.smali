.class public final Ldsm;
.super Landroid/animation/ValueAnimator;
.source "PG"


# instance fields
.field private final bIH:Z

.field private bII:Landroid/view/View;

.field private bIJ:I

.field private bIK:F

.field private bIL:Ljava/util/Map;


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 44
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldsm;->bIL:Ljava/util/Map;

    .line 45
    iput-boolean p1, p0, Ldsm;->bIH:Z

    .line 46
    new-instance v0, Ldsn;

    invoke-direct {v0, p0}, Ldsn;-><init>(Ldsm;)V

    invoke-virtual {p0, v0}, Ldsm;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 52
    new-instance v0, Ldso;

    invoke-direct {v0, p0}, Ldso;-><init>(Ldsm;)V

    invoke-virtual {p0, v0}, Ldsm;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 58
    iget-boolean v0, p0, Ldsm;->bIH:Z

    if-eqz v0, :cond_0

    .line 59
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Ldsm;->setFloatValues([F)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Ldsm;->setFloatValues([F)V

    goto :goto_0

    .line 59
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 61
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method final afD()V
    .locals 4

    .prologue
    .line 114
    iget v0, p0, Ldsm;->bIJ:I

    if-nez v0, :cond_1

    .line 115
    invoke-virtual {p0}, Ldsm;->cancel()V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p0}, Ldsm;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 121
    iget v1, p0, Ldsm;->bIJ:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 122
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 123
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 124
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v0, v2

    const/high16 v3, 0x42340000    # 45.0f

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setRotation(F)V

    .line 132
    :goto_1
    iget v1, p0, Ldsm;->bIK:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 133
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v0

    sub-double v0, v2, v0

    iget v2, p0, Ldsm;->bIK:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 134
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 125
    :cond_2
    iget v1, p0, Ldsm;->bIJ:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_3

    .line 126
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 127
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 128
    :cond_3
    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method

.method final afE()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 139
    iget v0, p0, Ldsm;->bIJ:I

    if-nez v0, :cond_0

    .line 160
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-boolean v0, p0, Ldsm;->bIH:Z

    if-eqz v0, :cond_1

    .line 144
    iget v0, p0, Ldsm;->bIJ:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 145
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 146
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 147
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setRotation(F)V

    .line 156
    :cond_1
    :goto_1
    iget v0, p0, Ldsm;->bIK:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 159
    :cond_2
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 148
    :cond_3
    iget v0, p0, Ldsm;->bIJ:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_4

    .line 149
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 150
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 151
    :cond_4
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method

.method public final n(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Ldsm;->bIL:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Ldsm;->bII:Landroid/view/View;

    .line 73
    iget-object v0, p0, Ldsm;->bIL:Ljava/util/Map;

    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    iget-object v0, p0, Ldsm;->bIL:Ljava/util/Map;

    iget-object v1, p0, Ldsm;->bII:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Ldsm;->bIJ:I

    .line 80
    :goto_0
    iget v0, p0, Ldsm;->bIJ:I

    packed-switch v0, :pswitch_data_0

    .line 90
    iput v2, p0, Ldsm;->bIK:F

    .line 93
    :goto_1
    iget v0, p0, Ldsm;->bIJ:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldsm;->bIJ:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 94
    :cond_0
    iget v1, p0, Ldsm;->bIK:F

    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-static {v0}, Ldtp;->ay(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/high16 v0, -0x40800000    # -1.0f

    :goto_2
    mul-float/2addr v0, v1

    iput v0, p0, Ldsm;->bIK:F

    .line 97
    :cond_1
    iget v0, p0, Ldsm;->bIJ:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 98
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 99
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 111
    :cond_2
    :goto_3
    return-void

    .line 77
    :cond_3
    const/4 v0, 0x1

    iput v0, p0, Ldsm;->bIJ:I

    goto :goto_0

    .line 83
    :pswitch_0
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Ldsm;->bIK:F

    goto :goto_1

    .line 87
    :pswitch_1
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Ldsm;->bIK:F

    goto :goto_1

    .line 94
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    .line 100
    :cond_5
    iget v0, p0, Ldsm;->bIJ:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_6

    .line 101
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 102
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 103
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_3

    .line 104
    :cond_6
    iget v0, p0, Ldsm;->bIJ:I

    if-eqz v0, :cond_2

    .line 105
    iget-boolean v0, p0, Ldsm;->bIH:Z

    if-eqz v0, :cond_7

    .line 106
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 108
    :cond_7
    iget-object v0, p0, Ldsm;->bII:Landroid/view/View;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_3

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
