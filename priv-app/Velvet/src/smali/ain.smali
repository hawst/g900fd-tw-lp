.class final Lain;
.super Lrk;
.source "PG"


# instance fields
.field public ZA:I

.field public ZB:I

.field public ZC:I

.field public ZD:Lain;

.field public ZE:Landroid/graphics/Bitmap;

.field public volatile ZF:I

.field private synthetic ZG:Laim;


# direct methods
.method public constructor <init>(Laim;III)V
    .locals 1

    .prologue
    .line 671
    iput-object p1, p0, Lain;->ZG:Laim;

    invoke-direct {p0}, Lrk;-><init>()V

    .line 669
    const/4 v0, 0x1

    iput v0, p0, Lain;->ZF:I

    .line 672
    iput p2, p0, Lain;->ZA:I

    .line 673
    iput p3, p0, Lain;->ZB:I

    .line 674
    iput p4, p0, Lain;->ZC:I

    .line 675
    return-void
.end method


# virtual methods
.method protected final d(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 679
    invoke-static {}, Laim;->lT()Lna;

    move-result-object v0

    invoke-interface {v0, p1}, Lna;->Y(Ljava/lang/Object;)Z

    .line 680
    return-void
.end method

.method public final dH()I
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lain;->ZG:Laim;

    invoke-static {v0}, Laim;->a(Laim;)I

    move-result v0

    return v0
.end method

.method public final dI()I
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lain;->ZG:Laim;

    invoke-static {v0}, Laim;->a(Laim;)I

    move-result v0

    return v0
.end method

.method protected final dM()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 699
    iget v0, p0, Lain;->ZF:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lqm;->assertTrue(Z)V

    .line 703
    iget-object v0, p0, Lain;->ZG:Laim;

    iget v0, v0, Laim;->Zn:I

    iget v2, p0, Lain;->ZA:I

    sub-int/2addr v0, v2

    iget v2, p0, Lain;->ZC:I

    shr-int/2addr v0, v2

    .line 704
    iget-object v2, p0, Lain;->ZG:Laim;

    iget v2, v2, Laim;->Zo:I

    iget v3, p0, Lain;->ZB:I

    sub-int/2addr v2, v3

    iget v3, p0, Lain;->ZC:I

    shr-int/2addr v2, v3

    .line 705
    iget-object v3, p0, Lain;->ZG:Laim;

    invoke-static {v3}, Laim;->a(Laim;)I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v3, p0, Lain;->ZG:Laim;

    invoke-static {v3}, Laim;->a(Laim;)I

    move-result v3

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lain;->setSize(II)V

    .line 707
    iget-object v0, p0, Lain;->ZE:Landroid/graphics/Bitmap;

    .line 708
    const/4 v2, 0x0

    iput-object v2, p0, Lain;->ZE:Landroid/graphics/Bitmap;

    .line 709
    iput v1, p0, Lain;->ZF:I

    .line 710
    return-object v0

    .line 699
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(III)V
    .locals 0

    .prologue
    .line 728
    iput p1, p0, Lain;->ZA:I

    .line 729
    iput p2, p0, Lain;->ZB:I

    .line 730
    iput p3, p0, Lain;->ZC:I

    .line 731
    invoke-virtual {p0}, Lain;->dR()V

    .line 732
    return-void
.end method

.method final lU()Z
    .locals 5

    .prologue
    .line 686
    :try_start_0
    invoke-static {}, Laim;->lT()Lna;

    move-result-object v0

    invoke-interface {v0}, Lna;->ch()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 687
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lain;->ZG:Laim;

    invoke-static {v2}, Laim;->a(Laim;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 688
    const/4 v0, 0x0

    .line 690
    :cond_0
    iget-object v1, p0, Lain;->ZG:Laim;

    invoke-static {v1}, Laim;->b(Laim;)Laiq;

    move-result-object v1

    iget v2, p0, Lain;->ZC:I

    iget v3, p0, Lain;->ZA:I

    iget v4, p0, Lain;->ZB:I

    invoke-interface {v1, v2, v3, v4, v0}, Laiq;->a(IIILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lain;->ZE:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    :goto_0
    iget-object v0, p0, Lain;->ZE:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 691
    :catch_0
    move-exception v0

    .line 692
    const-string v1, "TiledImageRenderer"

    const-string v2, "fail to decode tile"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 694
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final lV()Lain;
    .locals 4

    .prologue
    .line 735
    iget v0, p0, Lain;->ZC:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lain;->ZG:Laim;

    iget v1, v1, Laim;->Za:I

    if-ne v0, v1, :cond_0

    .line 736
    const/4 v0, 0x0

    .line 741
    :goto_0
    return-object v0

    .line 738
    :cond_0
    iget-object v0, p0, Lain;->ZG:Laim;

    invoke-static {v0}, Laim;->a(Laim;)I

    move-result v0

    iget v1, p0, Lain;->ZC:I

    add-int/lit8 v1, v1, 0x1

    shl-int/2addr v0, v1

    .line 739
    iget v1, p0, Lain;->ZA:I

    div-int/2addr v1, v0

    mul-int/2addr v1, v0

    .line 740
    iget v2, p0, Lain;->ZB:I

    div-int/2addr v2, v0

    mul-int/2addr v0, v2

    .line 741
    iget-object v2, p0, Lain;->ZG:Laim;

    iget v3, p0, Lain;->ZC:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v1, v0, v3}, Laim;->a(Laim;III)Lain;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 746
    const-string v0, "tile(%s, %s, %s / %s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lain;->ZA:I

    iget-object v4, p0, Lain;->ZG:Laim;

    invoke-static {v4}, Laim;->a(Laim;)I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lain;->ZB:I

    iget-object v4, p0, Lain;->ZG:Laim;

    invoke-static {v4}, Laim;->a(Laim;)I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lain;->ZG:Laim;

    invoke-static {v3}, Laim;->c(Laim;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lain;->ZG:Laim;

    iget v3, v3, Laim;->Za:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
