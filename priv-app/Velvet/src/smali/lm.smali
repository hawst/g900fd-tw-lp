.class public final Llm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cF:Landroid/os/Bundle;

.field hO:Z

.field private final kL:Lll;

.field final kM:Ljava/lang/String;

.field public final kN:Ljava/lang/String;

.field kO:Ljava/lang/String;

.field private kP:Z

.field kQ:I

.field kR:I

.field kS:I

.field kT:I

.field kU:I

.field private kV:I

.field kW:Lkq;

.field private final kf:Ljava/util/ArrayList;

.field mName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lll;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    .line 710
    const/4 v0, -0x1

    iput v0, p0, Llm;->kV:I

    .line 764
    iput-object p1, p0, Llm;->kL:Lll;

    .line 765
    iput-object p2, p0, Llm;->kM:Ljava/lang/String;

    .line 766
    iput-object p3, p0, Llm;->kN:Ljava/lang/String;

    .line 767
    return-void
.end method


# virtual methods
.method final b(Lkq;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1178
    const/4 v0, 0x0

    .line 1179
    iget-object v2, p0, Llm;->kW:Lkq;

    if-eq v2, p1, :cond_a

    .line 1180
    iput-object p1, p0, Llm;->kW:Lkq;

    .line 1181
    if-eqz p1, :cond_a

    .line 1182
    iget-object v2, p0, Llm;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Lkq;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lld;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1183
    invoke-virtual {p1}, Lkq;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llm;->mName:Ljava/lang/String;

    move v0, v1

    .line 1186
    :cond_0
    iget-object v2, p0, Llm;->kO:Ljava/lang/String;

    invoke-virtual {p1}, Lkq;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lld;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1187
    invoke-virtual {p1}, Lkq;->getDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llm;->kO:Ljava/lang/String;

    move v0, v1

    .line 1190
    :cond_1
    iget-boolean v2, p0, Llm;->hO:Z

    invoke-virtual {p1}, Lkq;->isEnabled()Z

    move-result v3

    if-eq v2, v3, :cond_c

    .line 1191
    invoke-virtual {p1}, Lkq;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Llm;->hO:Z

    .line 1194
    :goto_0
    iget-boolean v0, p0, Llm;->kP:Z

    invoke-virtual {p1}, Lkq;->isConnecting()Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 1195
    invoke-virtual {p1}, Lkq;->isConnecting()Z

    move-result v0

    iput-boolean v0, p0, Llm;->kP:Z

    .line 1196
    or-int/lit8 v1, v1, 0x1

    .line 1198
    :cond_2
    iget-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lkq;->bw()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1199
    iget-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lkq;->bw()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1201
    or-int/lit8 v1, v1, 0x1

    .line 1203
    :cond_3
    iget v0, p0, Llm;->kQ:I

    invoke-virtual {p1}, Lkq;->getPlaybackType()I

    move-result v2

    if-eq v0, v2, :cond_4

    .line 1204
    invoke-virtual {p1}, Lkq;->getPlaybackType()I

    move-result v0

    iput v0, p0, Llm;->kQ:I

    .line 1205
    or-int/lit8 v1, v1, 0x1

    .line 1207
    :cond_4
    iget v0, p0, Llm;->kR:I

    invoke-virtual {p1}, Lkq;->getPlaybackStream()I

    move-result v2

    if-eq v0, v2, :cond_5

    .line 1208
    invoke-virtual {p1}, Lkq;->getPlaybackStream()I

    move-result v0

    iput v0, p0, Llm;->kR:I

    .line 1209
    or-int/lit8 v1, v1, 0x1

    .line 1211
    :cond_5
    iget v0, p0, Llm;->kS:I

    invoke-virtual {p1}, Lkq;->getVolumeHandling()I

    move-result v2

    if-eq v0, v2, :cond_6

    .line 1212
    invoke-virtual {p1}, Lkq;->getVolumeHandling()I

    move-result v0

    iput v0, p0, Llm;->kS:I

    .line 1213
    or-int/lit8 v1, v1, 0x3

    .line 1215
    :cond_6
    iget v0, p0, Llm;->kT:I

    invoke-virtual {p1}, Lkq;->getVolume()I

    move-result v2

    if-eq v0, v2, :cond_7

    .line 1216
    invoke-virtual {p1}, Lkq;->getVolume()I

    move-result v0

    iput v0, p0, Llm;->kT:I

    .line 1217
    or-int/lit8 v1, v1, 0x3

    .line 1219
    :cond_7
    iget v0, p0, Llm;->kU:I

    invoke-virtual {p1}, Lkq;->getVolumeMax()I

    move-result v2

    if-eq v0, v2, :cond_8

    .line 1220
    invoke-virtual {p1}, Lkq;->getVolumeMax()I

    move-result v0

    iput v0, p0, Llm;->kU:I

    .line 1221
    or-int/lit8 v1, v1, 0x3

    .line 1223
    :cond_8
    iget v0, p0, Llm;->kV:I

    invoke-virtual {p1}, Lkq;->by()I

    move-result v2

    if-eq v0, v2, :cond_9

    .line 1224
    invoke-virtual {p1}, Lkq;->by()I

    move-result v0

    iput v0, p0, Llm;->kV:I

    .line 1225
    or-int/lit8 v1, v1, 0x5

    .line 1228
    :cond_9
    iget-object v0, p0, Llm;->cF:Landroid/os/Bundle;

    invoke-virtual {p1}, Lkq;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lld;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1229
    invoke-virtual {p1}, Lkq;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Llm;->cF:Landroid/os/Bundle;

    .line 1230
    or-int/lit8 v0, v1, 0x1

    .line 1234
    :cond_a
    :goto_1
    return v0

    :cond_b
    move v0, v1

    goto :goto_1

    :cond_c
    move v1, v0

    goto/16 :goto_0
.end method

.method final bS()Lkt;
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p0, Llm;->kL:Lll;

    invoke-static {}, Lld;->bN()V

    iget-object v0, v0, Lll;->kK:Lkt;

    return-object v0
.end method

.method public final e(Llb;)Z
    .locals 2

    .prologue
    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :cond_0
    invoke-static {}, Lld;->bN()V

    .line 891
    iget-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Llb;->c(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final l(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 913
    if-nez p1, :cond_0

    .line 914
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_0
    invoke-static {}, Lld;->bN()V

    .line 918
    iget-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 919
    :goto_0
    if-ge v2, v3, :cond_2

    .line 920
    iget-object v0, p0, Llm;->kf:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    const/4 v0, 0x1

    .line 924
    :goto_1
    return v0

    .line 919
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 924
    goto :goto_1
.end method

.method public final select()V
    .locals 1

    .prologue
    .line 1155
    invoke-static {}, Lld;->bN()V

    .line 1156
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0, p0}, Llh;->a(Llm;)V

    .line 1157
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Llm;->kN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llm;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llm;->kO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Llm;->hO:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Llm;->kP:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llm;->kQ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llm;->kR:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llm;->kS:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llm;->kT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llm;->kU:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Llm;->kV:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llm;->cF:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llm;->kL:Lll;

    invoke-virtual {v1}, Lll;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
