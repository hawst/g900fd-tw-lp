.class Limn;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# static fields
.field private static dEL:Lioj; = null

.field static final dEM:Ljava/util/Queue;

.field private static final logger:Ljava/util/logging/Logger;

.field private static final serialVersionUID:J = 0x5L


# instance fields
.field private dCT:Liml;

.field private dCd:Ljava/util/Set;

.field private dCj:Ljava/util/Set;

.field private transient dEF:I

.field private transient dEG:I

.field final transient dEH:[Lins;

.field final dEI:Ljava/util/Queue;

.field final transient dEJ:Lims;

.field final dEK:Lign;

.field private dEN:Ljava/util/Collection;

.field private dEp:I

.field private dEq:I

.field private dEr:Linz;

.field final dEs:Linz;

.field final dEt:J

.field final dEu:J

.field final dEw:Lifc;

.field final dEx:Lifc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136
    const-class v0, Limn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Limn;->logger:Ljava/util/logging/Logger;

    .line 630
    new-instance v0, Limo;

    invoke-direct {v0}, Limo;-><init>()V

    sput-object v0, Limn;->dEL:Lioj;

    .line 920
    new-instance v0, Limp;

    invoke-direct {v0}, Limp;-><init>()V

    sput-object v0, Limn;->dEM:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Limd;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 196
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 197
    invoke-virtual {p1}, Limd;->aXq()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Limn;->dEp:I

    .line 199
    invoke-virtual {p1}, Limd;->aXr()Linz;

    move-result-object v0

    iput-object v0, p0, Limn;->dEr:Linz;

    .line 200
    invoke-virtual {p1}, Limd;->aXs()Linz;

    move-result-object v0

    iput-object v0, p0, Limn;->dEs:Linz;

    .line 202
    iget-object v0, p1, Limd;->dEw:Lifc;

    invoke-virtual {p1}, Limd;->aXr()Linz;

    move-result-object v1

    invoke-virtual {v1}, Linz;->aXY()Lifc;

    move-result-object v1

    invoke-static {v0, v1}, Lifo;->n(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifc;

    iput-object v0, p0, Limn;->dEw:Lifc;

    .line 203
    iget-object v0, p1, Limd;->dEx:Lifc;

    invoke-virtual {p1}, Limd;->aXs()Linz;

    move-result-object v1

    invoke-virtual {v1}, Linz;->aXY()Lifc;

    move-result-object v1

    invoke-static {v0, v1}, Lifo;->n(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifc;

    iput-object v0, p0, Limn;->dEx:Lifc;

    .line 205
    iget v0, p1, Limd;->dEq:I

    iput v0, p0, Limn;->dEq:I

    .line 206
    iget-wide v0, p1, Limd;->dEu:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    move-wide v0, v2

    :goto_0
    iput-wide v0, p0, Limn;->dEu:J

    .line 207
    iget-wide v0, p1, Limd;->dEt:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_3

    :goto_1
    iput-wide v2, p0, Limn;->dEt:J

    .line 209
    iget-object v0, p0, Limn;->dEr:Linz;

    invoke-virtual {p0}, Limn;->aXv()Z

    move-result v1

    invoke-virtual {p0}, Limn;->aXu()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lims;->a(Linz;ZZ)Lims;

    move-result-object v0

    iput-object v0, p0, Limn;->dEJ:Lims;

    .line 210
    const/4 v0, 0x0

    invoke-static {}, Lign;->aWd()Lign;

    move-result-object v1

    invoke-static {v0, v1}, Lifo;->n(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lign;

    iput-object v0, p0, Limn;->dEK:Lign;

    .line 212
    invoke-virtual {p1}, Limd;->aWR()Liml;

    move-result-object v0

    iput-object v0, p0, Limn;->dCT:Liml;

    .line 213
    iget-object v0, p0, Limn;->dCT:Liml;

    sget-object v1, Liix;->dCU:Liix;

    if-ne v0, v1, :cond_4

    sget-object v0, Limn;->dEM:Ljava/util/Queue;

    :goto_2
    iput-object v0, p0, Limn;->dEI:Ljava/util/Queue;

    .line 217
    invoke-virtual {p1}, Limd;->aXp()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 218
    invoke-virtual {p0}, Limn;->aXu()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    iget v1, p0, Limn;->dEq:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    move v1, v4

    move v2, v5

    .line 228
    :goto_3
    iget v3, p0, Limn;->dEp:I

    if-ge v1, v3, :cond_5

    invoke-virtual {p0}, Limn;->aXu()Z

    move-result v3

    if-eqz v3, :cond_1

    mul-int/lit8 v3, v1, 0x2

    iget v6, p0, Limn;->dEq:I

    if-gt v3, v6, :cond_5

    .line 229
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 230
    shl-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 206
    :cond_2
    iget-wide v0, p1, Limd;->dEu:J

    goto :goto_0

    .line 207
    :cond_3
    iget-wide v2, p1, Limd;->dEt:J

    goto :goto_1

    .line 213
    :cond_4
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_2

    .line 232
    :cond_5
    rsub-int/lit8 v2, v2, 0x20

    iput v2, p0, Limn;->dEG:I

    .line 233
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Limn;->dEF:I

    .line 235
    new-array v2, v1, [Lins;

    iput-object v2, p0, Limn;->dEH:[Lins;

    .line 237
    div-int v2, v0, v1

    .line 238
    mul-int v3, v2, v1

    if-ge v3, v0, :cond_a

    .line 239
    add-int/lit8 v0, v2, 0x1

    .line 243
    :goto_4
    if-ge v4, v0, :cond_6

    .line 244
    shl-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 247
    :cond_6
    invoke-virtual {p0}, Limn;->aXu()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 249
    iget v0, p0, Limn;->dEq:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 250
    iget v2, p0, Limn;->dEq:I

    rem-int v1, v2, v1

    .line 251
    :goto_5
    iget-object v2, p0, Limn;->dEH:[Lins;

    array-length v2, v2

    if-ge v5, v2, :cond_9

    .line 252
    if-ne v5, v1, :cond_7

    .line 253
    add-int/lit8 v0, v0, -0x1

    .line 255
    :cond_7
    iget-object v2, p0, Limn;->dEH:[Lins;

    invoke-direct {p0, v4, v0}, Limn;->bo(II)Lins;

    move-result-object v3

    aput-object v3, v2, v5

    .line 251
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 259
    :cond_8
    :goto_6
    iget-object v0, p0, Limn;->dEH:[Lins;

    array-length v0, v0

    if-ge v5, v0, :cond_9

    .line 260
    iget-object v0, p0, Limn;->dEH:[Lins;

    const/4 v1, -0x1

    invoke-direct {p0, v4, v1}, Limn;->bo(II)Lins;

    move-result-object v1

    aput-object v1, v0, v5

    .line 259
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 264
    :cond_9
    return-void

    :cond_a
    move v0, v2

    goto :goto_4
.end method

.method static a(Linr;Linr;)V
    .locals 0

    .prologue
    .line 1979
    invoke-interface {p0, p1}, Linr;->f(Linr;)V

    .line 1980
    invoke-interface {p1, p0}, Linr;->g(Linr;)V

    .line 1981
    return-void
.end method

.method static a(Linr;J)Z
    .locals 5

    .prologue
    .line 1974
    invoke-interface {p0}, Linr;->zL()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static aXA()Linr;
    .locals 1

    .prologue
    .line 917
    sget-object v0, Linq;->dFs:Linq;

    return-object v0
.end method

.method static aXB()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 952
    sget-object v0, Limn;->dEM:Ljava/util/Queue;

    return-object v0
.end method

.method static aXz()Lioj;
    .locals 1

    .prologue
    .line 666
    sget-object v0, Limn;->dEL:Lioj;

    return-object v0
.end method

.method private aZ(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 1901
    iget-object v0, p0, Limn;->dEw:Lifc;

    invoke-virtual {v0, p1}, Lifc;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 1902
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static b(Linr;Linr;)V
    .locals 0

    .prologue
    .line 2011
    invoke-interface {p0, p1}, Linr;->h(Linr;)V

    .line 2012
    invoke-interface {p1, p0}, Linr;->i(Linr;)V

    .line 2013
    return-void
.end method

.method private bo(II)Lins;
    .locals 1

    .prologue
    .line 1937
    new-instance v0, Lins;

    invoke-direct {v0, p0, p1, p2}, Lins;-><init>(Limn;II)V

    return-object v0
.end method

.method static d(Linr;)V
    .locals 1

    .prologue
    .line 1985
    sget-object v0, Linq;->dFs:Linq;

    .line 1986
    invoke-interface {p0, v0}, Linr;->f(Linr;)V

    .line 1987
    invoke-interface {p0, v0}, Linr;->g(Linr;)V

    .line 1988
    return-void
.end method

.method static e(Linr;)V
    .locals 1

    .prologue
    .line 2017
    sget-object v0, Linq;->dFs:Linq;

    .line 2018
    invoke-interface {p0, v0}, Linr;->h(Linr;)V

    .line 2019
    invoke-interface {p0, v0}, Linr;->i(Linr;)V

    .line 2020
    return-void
.end method

.method private mi(I)Lins;
    .locals 3

    .prologue
    .line 1933
    iget-object v0, p0, Limn;->dEH:[Lins;

    iget v1, p0, Limn;->dEG:I

    ushr-int v1, p1, v1

    iget v2, p0, Limn;->dEF:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method final a(Linr;)V
    .locals 2

    .prologue
    .line 1912
    invoke-interface {p1}, Linr;->aXH()I

    move-result v0

    .line 1913
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lins;->a(Linr;I)Z

    .line 1914
    return-void
.end method

.method final a(Lioj;)V
    .locals 3

    .prologue
    .line 1906
    invoke-interface {p1}, Lioj;->aXD()Linr;

    move-result-object v0

    .line 1907
    invoke-interface {v0}, Linr;->aXH()I

    move-result v1

    .line 1908
    invoke-direct {p0, v1}, Limn;->mi(I)Lins;

    move-result-object v2

    invoke-interface {v0}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lins;->a(Ljava/lang/Object;ILioj;)Z

    .line 1909
    return-void
.end method

.method final aXC()V
    .locals 4

    .prologue
    .line 1999
    :goto_0
    iget-object v0, p0, Limn;->dEI:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limm;

    if-eqz v0, :cond_0

    .line 2001
    :try_start_0
    iget-object v0, p0, Limn;->dCT:Liml;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2002
    :catch_0
    move-exception v0

    .line 2003
    sget-object v1, Limn;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2006
    :cond_0
    return-void
.end method

.method final aXu()Z
    .locals 2

    .prologue
    .line 267
    iget v0, p0, Limn;->dEq:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aXv()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 271
    iget-wide v2, p0, Limn;->dEt:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0}, Limn;->aXw()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method final aXw()Z
    .locals 4

    .prologue
    .line 279
    iget-wide v0, p0, Limn;->dEu:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aXx()Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Limn;->dEr:Linz;

    sget-object v1, Linz;->dFI:Linz;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aXy()Z
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Limn;->dEs:Linz;

    sget-object v1, Linz;->dFI:Linz;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Linr;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1946
    invoke-interface {p1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1957
    :cond_0
    :goto_0
    return-object v0

    .line 1949
    :cond_1
    invoke-interface {p1}, Linr;->aXF()Lioj;

    move-result-object v1

    invoke-interface {v1}, Lioj;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1950
    if-eqz v1, :cond_0

    .line 1954
    invoke-virtual {p0}, Limn;->aXv()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p1}, Limn;->c(Linr;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move-object v0, v1

    .line 1957
    goto :goto_0
.end method

.method final c(Linr;)Z
    .locals 2

    .prologue
    .line 1966
    iget-object v0, p0, Limn;->dEK:Lign;

    invoke-virtual {v0}, Lign;->aWc()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Limn;->a(Linr;J)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 4

    .prologue
    .line 3615
    iget-object v1, p0, Limn;->dEH:[Lins;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3616
    invoke-virtual {v3}, Lins;->clear()V

    .line 3615
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3618
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3507
    if-nez p1, :cond_0

    .line 3508
    const/4 v0, 0x0

    .line 3511
    :goto_0
    return v0

    .line 3510
    :cond_0
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3511
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lins;->m(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3516
    if-nez p1, :cond_0

    .line 3517
    const/4 v0, 0x0

    .line 3550
    :goto_0
    return v0

    .line 3525
    :cond_0
    iget-object v7, p0, Limn;->dEH:[Lins;

    .line 3526
    const-wide/16 v4, -0x1

    .line 3527
    const/4 v0, 0x0

    move v6, v0

    move-wide v8, v4

    :goto_1
    const/4 v0, 0x3

    if-ge v6, v0, :cond_5

    .line 3528
    const-wide/16 v2, 0x0

    .line 3529
    array-length v10, v7

    const/4 v0, 0x0

    move-wide v4, v2

    move v2, v0

    :goto_2
    if-ge v2, v10, :cond_4

    aget-object v3, v7, v2

    .line 3532
    iget v0, v3, Lins;->count:I

    .line 3534
    iget-object v11, v3, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3535
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3536
    invoke-virtual {v11, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    :goto_4
    if-eqz v0, :cond_2

    .line 3537
    invoke-virtual {v3, v0}, Lins;->b(Linr;)Ljava/lang/Object;

    move-result-object v12

    .line 3538
    if-eqz v12, :cond_1

    iget-object v13, p0, Limn;->dEx:Lifc;

    invoke-virtual {v13, p1, v12}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 3539
    const/4 v0, 0x1

    goto :goto_0

    .line 3536
    :cond_1
    invoke-interface {v0}, Linr;->aXG()Linr;

    move-result-object v0

    goto :goto_4

    .line 3535
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 3543
    :cond_3
    iget v0, v3, Lins;->modCount:I

    int-to-long v0, v0

    add-long/2addr v4, v0

    .line 3529
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3545
    :cond_4
    cmp-long v0, v4, v8

    if-eqz v0, :cond_5

    .line 3527
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 3550
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 3640
    iget-object v0, p0, Limn;->dCj:Ljava/util/Set;

    .line 3641
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ling;

    invoke-direct {v0, p0}, Ling;-><init>(Limn;)V

    iput-object v0, p0, Limn;->dCj:Ljava/util/Set;

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3475
    if-nez p1, :cond_0

    .line 3476
    const/4 v0, 0x0

    .line 3479
    :goto_0
    return-object v0

    .line 3478
    :cond_0
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3479
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lins;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3440
    .line 3441
    iget-object v6, p0, Limn;->dEH:[Lins;

    move v0, v1

    move-wide v2, v4

    .line 3442
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3443
    aget-object v7, v6, v0

    iget v7, v7, Lins;->count:I

    if-eqz v7, :cond_1

    .line 3460
    :cond_0
    :goto_1
    return v1

    .line 3446
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lins;->modCount:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3442
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3449
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3450
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3451
    aget-object v7, v6, v0

    iget v7, v7, Lins;->count:I

    if-nez v7, :cond_0

    .line 3454
    aget-object v7, v6, v0

    iget v7, v7, Lins;->modCount:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3450
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3456
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3460
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 3624
    iget-object v0, p0, Limn;->dCd:Ljava/util/Set;

    .line 3625
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Linp;

    invoke-direct {v0, p0}, Linp;-><init>(Limn;)V

    iput-object v0, p0, Limn;->dCd:Ljava/util/Set;

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3555
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3556
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3557
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3558
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lins;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 3571
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3572
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Limn;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3574
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3563
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3564
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3565
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3566
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lins;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3578
    if-nez p1, :cond_0

    .line 3579
    const/4 v0, 0x0

    .line 3582
    :goto_0
    return-object v0

    .line 3581
    :cond_0
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3582
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lins;->n(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3587
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3588
    :cond_0
    const/4 v0, 0x0

    .line 3591
    :goto_0
    return v0

    .line 3590
    :cond_1
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3591
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lins;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3607
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3608
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3609
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3610
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lins;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3596
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3597
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3598
    if-nez p2, :cond_0

    .line 3599
    const/4 v0, 0x0

    .line 3602
    :goto_0
    return v0

    .line 3601
    :cond_0
    invoke-direct {p0, p1}, Limn;->aZ(Ljava/lang/Object;)I

    move-result v0

    .line 3602
    invoke-direct {p0, v0}, Limn;->mi(I)Lins;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lins;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    .prologue
    .line 3465
    iget-object v1, p0, Limn;->dEH:[Lins;

    .line 3466
    const-wide/16 v2, 0x0

    .line 3467
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 3468
    aget-object v4, v1, v0

    iget v4, v4, Lins;->count:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 3467
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3470
    :cond_0
    invoke-static {v2, v3}, Lius;->bZ(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 3632
    iget-object v0, p0, Limn;->dEN:Ljava/util/Collection;

    .line 3633
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Liok;

    invoke-direct {v0, p0}, Liok;-><init>(Limn;)V

    iput-object v0, p0, Limn;->dEN:Ljava/util/Collection;

    goto :goto_0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 3938
    new-instance v1, Lint;

    iget-object v2, p0, Limn;->dEr:Linz;

    iget-object v3, p0, Limn;->dEs:Linz;

    iget-object v4, p0, Limn;->dEw:Lifc;

    iget-object v5, p0, Limn;->dEx:Lifc;

    iget-wide v6, p0, Limn;->dEt:J

    iget-wide v8, p0, Limn;->dEu:J

    iget v10, p0, Limn;->dEq:I

    iget v11, p0, Limn;->dEp:I

    iget-object v12, p0, Limn;->dCT:Liml;

    move-object v13, p0

    invoke-direct/range {v1 .. v13}, Lint;-><init>(Linz;Linz;Lifc;Lifc;JJIILiml;Ljava/util/concurrent/ConcurrentMap;)V

    return-object v1
.end method
