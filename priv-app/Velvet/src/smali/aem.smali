.class public final Laem;
.super Laeu;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Laeu;-><init>()V

    .line 272
    iput-object p1, p0, Laem;->UB:Landroid/graphics/drawable/Drawable;

    .line 273
    return-void
.end method


# virtual methods
.method public final a(Ladz;)V
    .locals 8

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 276
    invoke-virtual {p1}, Ladz;->kz()Lcom/android/launcher3/CropView;

    move-result-object v6

    .line 278
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v1

    invoke-virtual {v6}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/app/WallpaperManager;->getBuiltInDrawable(IIZFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 281
    if-nez v0, :cond_0

    .line 282
    const-string v0, "Launcher.WallpaperPickerActivity"

    const-string v1, "Null default wallpaper encountered."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-virtual {v6, v7, v7}, Lcom/android/launcher3/CropView;->a(Laiq;Ljava/lang/Runnable;)V

    .line 292
    :goto_0
    return-void

    .line 287
    :cond_0
    new-instance v1, Lun;

    const/16 v2, 0x400

    invoke-direct {v1, p1, v0, v2}, Lun;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6, v1, v7}, Lcom/android/launcher3/CropView;->a(Laiq;Ljava/lang/Runnable;)V

    .line 289
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v6, v0}, Lcom/android/launcher3/CropView;->k(F)V

    .line 290
    invoke-virtual {v6, v3}, Lcom/android/launcher3/CropView;->K(Z)V

    .line 291
    invoke-virtual {p1, v3}, Ladz;->aq(Z)V

    goto :goto_0
.end method

.method public final isSelectable()Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x1

    return v0
.end method

.method public final kC()Z
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    return v0
.end method

.method public final n(Ladz;)V
    .locals 2

    .prologue
    .line 296
    :try_start_0
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/WallpaperManager;->clear()V

    .line 297
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Ladz;->setResult(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :goto_0
    invoke-virtual {p1}, Ladz;->finish()V

    .line 302
    return-void

    .line 298
    :catch_0
    move-exception v0

    .line 299
    const-string v1, "Setting wallpaper to default threw exception"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
