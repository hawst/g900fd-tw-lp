.class public final Lfvo;
.super Lfuz;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgan;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 43
    iput-object p4, p0, Lfvo;->mFifeImageUrlUtil:Lgan;

    .line 44
    return-void
.end method

.method private static a(Ljam;)[Ljbb;
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Ljam;->dWT:[Ljcn;

    array-length v0, v0

    new-array v1, v0, [Ljbb;

    .line 163
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljam;->dWT:[Ljcn;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 164
    iget-object v2, p0, Ljam;->dWT:[Ljcn;

    aget-object v2, v2, v0

    .line 165
    new-instance v3, Ljbb;

    invoke-direct {v3}, Ljbb;-><init>()V

    .line 166
    iput-object v2, v3, Ljbb;->dXI:Ljcn;

    .line 167
    iput-object v2, v3, Ljbb;->dXJ:Ljcn;

    .line 168
    iput-object v2, v3, Ljbb;->dXK:Ljcn;

    .line 169
    iget-object v2, p0, Ljam;->aeB:Ljbp;

    iput-object v2, v3, Ljbb;->aeB:Ljbp;

    .line 170
    aput-object v3, v1, v0

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 48
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    .line 50
    new-instance v2, Lang;

    invoke-direct {v2}, Lang;-><init>()V

    .line 51
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 54
    iget-object v4, v1, Lizj;->dUd:Ljam;

    iget-object v0, v4, Ljam;->aeB:Ljbp;

    if-eqz v0, :cond_5

    iget-object v0, v4, Ljam;->dWS:Ljcj;

    if-eqz v0, :cond_5

    iget-object v0, v4, Ljam;->dWS:Ljcj;

    invoke-virtual {v0}, Ljcj;->oK()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v4, Ljam;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0a04ca

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, v4, Ljam;->dWS:Ljcj;

    invoke-virtual {v6}, Ljcj;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, v4, Ljam;->aeB:Ljbp;

    invoke-virtual {v6}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p1, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v5, Lftn;

    invoke-direct {v5, v0}, Lftn;-><init>(Ljava/lang/String;)V

    iget-object v0, v4, Ljam;->dWS:Ljcj;

    if-eqz v0, :cond_0

    iget-object v0, v4, Ljam;->dWS:Ljcj;

    invoke-virtual {v0}, Ljcj;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a04cc

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, v4, Ljam;->dWS:Ljcj;

    invoke-virtual {v7}, Ljcj;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p1, v0, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lftn;->ceJ:Ljava/lang/String;

    :cond_0
    iget-object v0, v4, Ljam;->dWS:Ljcj;

    iget-object v0, v0, Ljcj;->dXN:Ljcn;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0d0167

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    new-instance v6, Ljcn;

    invoke-direct {v6}, Ljcn;-><init>()V

    iget-object v4, v4, Ljam;->dWS:Ljcj;

    iget-object v4, v4, Ljcj;->dXN:Ljcn;

    invoke-virtual {v4}, Ljcn;->bgJ()I

    move-result v7

    if-ne v7, v10, :cond_6

    iget-object v7, p0, Lfvo;->mFifeImageUrlUtil:Lgan;

    invoke-virtual {v4}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v0, v0, v4}, Lgan;->b(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-virtual {v6, v0}, Ljcn;->tk(Ljava/lang/String;)Ljcn;

    move-result-object v0

    iput-object v0, v5, Lftn;->cBM:Ljcn;

    :cond_2
    invoke-virtual {v5}, Lftn;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, v1, Lizj;->dUd:Ljam;

    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Lanh;->cm(I)Lanh;

    iput-object v1, v4, Lanh;->ahu:Lizj;

    new-instance v5, Laoh;

    invoke-direct {v5}, Laoh;-><init>()V

    iput-object v5, v4, Lanh;->agQ:Laoh;

    iget-object v5, v4, Lanh;->agQ:Laoh;

    invoke-static {v0}, Lfvo;->a(Ljam;)[Ljbb;

    move-result-object v0

    iput-object v0, v5, Laoh;->ajt:[Ljbb;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v0, v1, Lizj;->dUd:Ljam;

    iget-object v0, v0, Ljam;->dWS:Ljcj;

    if-eqz v0, :cond_4

    .line 62
    iget-object v0, v1, Lizj;->dUd:Ljam;

    iget-object v0, v0, Ljam;->dWS:Ljcj;

    invoke-virtual {v0}, Ljcj;->bgB()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    iget-object v0, v1, Lizj;->dUd:Ljam;

    iget-object v0, v0, Ljam;->dWS:Ljcj;

    invoke-virtual {v0}, Ljcj;->bgA()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lgaq;->M(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    new-instance v4, Laoj;

    invoke-direct {v4}, Laoj;-><init>()V

    const v5, 0x7f0a04cf

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, v1, Lizj;->dUd:Ljam;

    iget-object v7, v7, Ljam;->dWS:Ljcj;

    invoke-virtual {v7}, Ljcj;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v5, Lfsa;

    const/16 v6, 0xca

    invoke-direct {v5, v6}, Lfsa;-><init>(I)V

    const v6, 0x7f0201c1

    invoke-virtual {v5, v6}, Lfsa;->jB(I)Lfsa;

    move-result-object v5

    invoke-virtual {v0, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v8}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v0

    new-instance v5, Lanh;

    invoke-direct {v5}, Lanh;-><init>()V

    invoke-virtual {v5, v11}, Lanh;->cm(I)Lanh;

    iput-object v1, v5, Lanh;->ahu:Lizj;

    iput-object v4, v5, Lanh;->agz:Laoj;

    iput-object v0, v5, Lanh;->ahs:Lani;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_3
    iget-object v0, v1, Lizj;->dUd:Ljam;

    iget-object v0, v0, Ljam;->dWS:Ljcj;

    invoke-virtual {v0}, Ljcj;->bac()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.SENDTO"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mailto:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lizj;->dUd:Ljam;

    iget-object v5, v5, Ljam;->dWS:Ljcj;

    invoke-virtual {v5}, Ljcj;->bab()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    new-instance v4, Laoj;

    invoke-direct {v4}, Laoj;-><init>()V

    const v5, 0x7f0a04cd

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, v1, Lizj;->dUd:Ljam;

    iget-object v7, v7, Ljam;->dWS:Ljcj;

    invoke-virtual {v7}, Ljcj;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v5, Lfsa;

    const/16 v6, 0xcb

    invoke-direct {v5, v6}, Lfsa;-><init>(I)V

    const v6, 0x7f02012d

    invoke-virtual {v5, v6}, Lfsa;->jB(I)Lfsa;

    move-result-object v5

    invoke-virtual {v0, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v8}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v0

    new-instance v5, Lanh;

    invoke-direct {v5}, Lanh;-><init>()V

    invoke-virtual {v5, v11}, Lanh;->cm(I)Lanh;

    iput-object v1, v5, Lanh;->ahu:Lizj;

    iput-object v4, v5, Lanh;->agz:Laoj;

    iput-object v0, v5, Lanh;->ahs:Lani;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v2, Lang;->ags:[Lanh;

    .line 74
    invoke-virtual {v2, v8}, Lang;->aS(Z)Lang;

    .line 75
    return-object v2

    .line 54
    :cond_5
    const v0, 0x7f0a04cb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v4}, Ljcn;->bgJ()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v4}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v4, "//"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "https:"

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v4}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method
