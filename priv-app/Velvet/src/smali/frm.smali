.class final Lfrm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic cAY:Landroid/widget/EditText;

.field private synthetic cAZ:Z

.field private synthetic cBa:Lfrk;


# direct methods
.method constructor <init>(Lfrk;Landroid/widget/EditText;Z)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lfrm;->cBa:Lfrk;

    iput-object p2, p0, Lfrm;->cAY:Landroid/widget/EditText;

    iput-boolean p3, p0, Lfrm;->cAZ:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 117
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    iget-object v0, p0, Lfrm;->cAY:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-boolean v1, p0, Lfrm;->cAZ:Z

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lfrm;->cBa:Lfrk;

    iget-object v2, p0, Lfrm;->cAY:Landroid/widget/EditText;

    invoke-virtual {v1, v0, v2}, Lfrk;->b(Ljava/lang/String;Landroid/widget/EditText;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 124
    :cond_1
    iget-object v1, p0, Lfrm;->cBa:Lfrk;

    iget-object v2, p0, Lfrm;->cAY:Landroid/widget/EditText;

    invoke-virtual {v1, v0, v2}, Lfrk;->a(Ljava/lang/String;Landroid/widget/EditText;)V
    :try_end_0
    .catch Lfrn; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method
