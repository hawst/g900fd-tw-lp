.class public final Lhix;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aOw:Ljava/util/concurrent/Executor;

.field private dij:Lhiu;

.field private dil:Lhip;

.field private dim:Z

.field private final mAudioRouter:Lhhu;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lhhu;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lhix;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 30
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lhix;->aOw:Ljava/util/concurrent/Executor;

    .line 31
    iput-object p1, p0, Lhix;->mAudioRouter:Lhhu;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhix;->dim:Z

    .line 33
    return-void
.end method


# virtual methods
.method public final X([B)V
    .locals 4
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 43
    iget-object v0, p0, Lhix;->dil:Lhip;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 44
    new-instance v0, Lhip;

    iget-object v1, p0, Lhix;->mAudioRouter:Lhhu;

    iget-object v2, p0, Lhix;->mUiExecutor:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lhix;->aOw:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1, p1, v2, v3}, Lhip;-><init>(Lhhu;[BLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lhix;->dil:Lhip;

    .line 46
    iget-boolean v0, p0, Lhix;->dim:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lhix;->dil:Lhip;

    iget-object v1, p0, Lhix;->dij:Lhiu;

    invoke-virtual {v0, v1}, Lhip;->a(Lhiu;)V

    .line 49
    :cond_0
    return-void

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhiy;)V
    .locals 2

    .prologue
    .line 52
    iput-object p1, p0, Lhix;->dij:Lhiu;

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhix;->dim:Z

    .line 54
    iget-object v0, p0, Lhix;->dil:Lhip;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lhix;->dil:Lhip;

    iget-object v1, p0, Lhix;->dij:Lhiu;

    invoke-virtual {v0, v1}, Lhip;->a(Lhiu;)V

    .line 57
    :cond_0
    return-void
.end method

.method public final aPL()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lhix;->dil:Lhip;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lhix;->dil:Lhip;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lhip;->bf:Z

    iget-object v1, v0, Lhip;->dii:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    iget-object v0, v0, Lhip;->dih:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lhix;->dil:Lhip;

    .line 64
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhix;->dim:Z

    .line 65
    return-void
.end method
