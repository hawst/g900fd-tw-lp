.class public final Lanv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public aiG:Ljax;

.field private aiH:I

.field private aiI:I

.field private aiJ:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 14777
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14778
    iput v0, p0, Lanv;->aez:I

    iput-object v1, p0, Lanv;->aiG:Ljax;

    iput v0, p0, Lanv;->aiH:I

    iput v0, p0, Lanv;->aiI:I

    iput-boolean v0, p0, Lanv;->aiJ:Z

    iput-object v1, p0, Lanv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lanv;->eCz:I

    .line 14779
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 14698
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lanv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lanv;->aiG:Ljax;

    if-nez v0, :cond_1

    new-instance v0, Ljax;

    invoke-direct {v0}, Ljax;-><init>()V

    iput-object v0, p0, Lanv;->aiG:Ljax;

    :cond_1
    iget-object v0, p0, Lanv;->aiG:Ljax;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lanv;->aiH:I

    iget v0, p0, Lanv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lanv;->aiI:I

    iget v0, p0, Lanv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lanv;->aiJ:Z

    iget v0, p0, Lanv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lanv;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 14795
    iget-object v0, p0, Lanv;->aiG:Ljax;

    if-eqz v0, :cond_0

    .line 14796
    const/4 v0, 0x1

    iget-object v1, p0, Lanv;->aiG:Ljax;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 14798
    :cond_0
    iget v0, p0, Lanv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 14799
    const/4 v0, 0x2

    iget v1, p0, Lanv;->aiH:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 14801
    :cond_1
    iget v0, p0, Lanv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 14802
    const/4 v0, 0x3

    iget v1, p0, Lanv;->aiI:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 14804
    :cond_2
    iget v0, p0, Lanv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 14805
    const/4 v0, 0x4

    iget-boolean v1, p0, Lanv;->aiJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14807
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14808
    return-void
.end method

.method public final cx(I)Lanv;
    .locals 1

    .prologue
    .line 14726
    iput p1, p0, Lanv;->aiH:I

    .line 14727
    iget v0, p0, Lanv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanv;->aez:I

    .line 14728
    return-object p0
.end method

.method public final cy(I)Lanv;
    .locals 1

    .prologue
    .line 14745
    iput p1, p0, Lanv;->aiI:I

    .line 14746
    iget v0, p0, Lanv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanv;->aez:I

    .line 14747
    return-object p0
.end method

.method public final getColumnWidth()I
    .locals 1

    .prologue
    .line 14723
    iget v0, p0, Lanv;->aiH:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 14812
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14813
    iget-object v1, p0, Lanv;->aiG:Ljax;

    if-eqz v1, :cond_0

    .line 14814
    const/4 v1, 0x1

    iget-object v2, p0, Lanv;->aiG:Ljax;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14817
    :cond_0
    iget v1, p0, Lanv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 14818
    const/4 v1, 0x2

    iget v2, p0, Lanv;->aiH:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14821
    :cond_1
    iget v1, p0, Lanv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 14822
    const/4 v1, 0x3

    iget v2, p0, Lanv;->aiI:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14825
    :cond_2
    iget v1, p0, Lanv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 14826
    const/4 v1, 0x4

    iget-boolean v2, p0, Lanv;->aiJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14829
    :cond_3
    return v0
.end method

.method public final pV()I
    .locals 1

    .prologue
    .line 14742
    iget v0, p0, Lanv;->aiI:I

    return v0
.end method

.method public final pW()Z
    .locals 1

    .prologue
    .line 14761
    iget-boolean v0, p0, Lanv;->aiJ:Z

    return v0
.end method
