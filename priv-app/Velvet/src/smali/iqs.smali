.class public final Liqs;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/Set;Ljava/util/Set;)Liqx;
    .locals 2

    .prologue
    .line 584
    const-string v0, "set1"

    invoke-static {p0, v0}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    const-string v0, "set2"

    invoke-static {p1, v0}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    invoke-static {p1, p0}, Liqs;->b(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v0

    .line 589
    new-instance v1, Liqt;

    invoke-direct {v1, p0, v0, p1}, Liqt;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-object v1
.end method

.method public static b(Ljava/util/Set;Ljava/util/Set;)Liqx;
    .locals 2

    .prologue
    .line 680
    const-string v0, "set1"

    invoke-static {p0, v0}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    const-string v0, "set2"

    invoke-static {p1, v0}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 683
    invoke-static {p1}, Lifx;->C(Ljava/util/Collection;)Lifw;

    move-result-object v0

    invoke-static {v0}, Lifx;->c(Lifw;)Lifw;

    move-result-object v0

    .line 684
    new-instance v1, Liqu;

    invoke-direct {v1, p0, v0, p1}, Liqu;-><init>(Ljava/util/Set;Lifw;Ljava/util/Set;)V

    return-object v1
.end method

.method static b(Ljava/util/Set;Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1263
    if-ne p0, p1, :cond_1

    .line 1277
    :cond_0
    :goto_0
    return v0

    .line 1266
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-eqz v2, :cond_3

    .line 1267
    check-cast p1, Ljava/util/Set;

    .line 1270
    :try_start_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-interface {p0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1272
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 1274
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1277
    goto :goto_0
.end method

.method public static c(Ljava/util/Comparator;)Ljava/util/TreeSet;
    .locals 2

    .prologue
    .line 344
    new-instance v1, Ljava/util/TreeSet;

    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    return-object v1
.end method

.method static f(Ljava/util/Set;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1252
    .line 1253
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1254
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    .line 1255
    goto :goto_0

    :cond_0
    move v2, v1

    .line 1254
    goto :goto_1

    .line 1256
    :cond_1
    return v0
.end method

.method private static h(Ljava/util/Iterator;)Ljava/util/HashSet;
    .locals 2

    .prologue
    .line 230
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 231
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    :cond_0
    return-object v0
.end method

.method public static varargs m([Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 175
    array-length v0, p0

    invoke-static {v0}, Liqs;->mm(I)Ljava/util/HashSet;

    move-result-object v0

    .line 176
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 177
    return-object v0
.end method

.method public static mm(I)Ljava/util/HashSet;
    .locals 2

    .prologue
    .line 194
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, Lior;->ml(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    return-object v0
.end method

.method public static z(Ljava/lang/Iterable;)Ljava/util/HashSet;
    .locals 2

    .prologue
    .line 211
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, Liia;->o(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Liqs;->h(Ljava/util/Iterator;)Ljava/util/HashSet;

    move-result-object v0

    goto :goto_0
.end method
