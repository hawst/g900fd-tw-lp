.class public final Lfnk;
.super Lgbo;
.source "PG"


# instance fields
.field private final mRemoteClient:Lfml;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lerp;Ljava/util/concurrent/Executor;Lfml;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lgbo;-><init>(Landroid/content/res/Resources;Lerp;Ljava/util/concurrent/Executor;)V

    .line 24
    iput-object p4, p0, Lfnk;->mRemoteClient:Lfml;

    .line 25
    return-void
.end method


# virtual methods
.method protected final a(Lgbn;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # Lgbn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 29
    if-nez p1, :cond_0

    .line 30
    iget-object v0, p0, Lfnk;->mRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->ayr()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lfnk;->mRemoteClient:Lfml;

    invoke-static {}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCb()Lfox;

    move-result-object v2

    iget-object v0, p1, Lgbn;->cFd:Lgbg;

    invoke-virtual {v0}, Lgbg;->aEf()Ljsr;

    move-result-object v0

    check-cast v0, Ljal;

    iput-object v0, v2, Lfox;->mFrequentPlaceEntry:Ljal;

    iget-object v0, p1, Lgbn;->cFc:Lgbg;

    invoke-virtual {v0}, Lgbg;->aEf()Ljsr;

    move-result-object v0

    check-cast v0, Ljbp;

    iput-object v0, v2, Lfox;->mSource:Ljbp;

    iget-boolean v0, p1, Lgbn;->cxZ:Z

    iput-boolean v0, v2, Lfox;->cxZ:Z

    iget-boolean v0, p1, Lgbn;->cFe:Z

    iput-boolean v0, v2, Lfox;->cya:Z

    iget-boolean v0, p1, Lgbn;->cyb:Z

    iput-boolean v0, v2, Lfox;->cyb:Z

    iget-boolean v0, p1, Lgbn;->cyc:Z

    iput-boolean v0, v2, Lfox;->cyc:Z

    iget-object v0, p1, Lgbn;->cyd:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lgbn;->cyd:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lfox;->ja(I)Lfox;

    :cond_1
    iget-object v0, p1, Lgbn;->cye:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lgbn;->cye:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lfox;->iZ(I)Lfox;

    :cond_2
    invoke-virtual {v2}, Lfox;->aCh()Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfml;->b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected final isConnected()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lfnk;->mRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    return v0
.end method
