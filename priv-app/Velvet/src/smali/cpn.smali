.class public Lcpn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final beZ:Lijp;

.field private static final bfa:Lijp;

.field public static bfb:Ljava/lang/String;

.field public static bfc:Ljava/lang/String;

.field private static bfd:Ljava/lang/String;

.field private static bfe:Ljava/lang/String;

.field private static bff:Ljava/lang/String;

.field public static bfg:Ljava/lang/String;

.field public static bfh:Ljava/lang/String;

.field public static bfi:Ljava/lang/String;

.field private static bfj:I


# instance fields
.field private final bfk:Lcxs;

.field private final bfl:Ligi;

.field final bfm:Ljava/text/SimpleDateFormat;

.field private final bfn:Ljava/lang/String;

.field private final bfo:Ligi;

.field volatile bfp:Landroid/graphics/Point;

.field private volatile bfq:Lgnu;

.field private final mClock:Lemp;

.field private mConfig:Lcjs;

.field private final mCookies:Lgpf;

.field private final mCorpora:Lcfu;

.field private final mDebugFeatures:Lckw;

.field final mDiscourseContextHelper:Lcla;

.field private final mGsaConfig:Lchk;

.field private final mLoginHelper:Lcrh;

.field private final mPartner:Lcoe;

.field private final mResources:Landroid/content/res/Resources;

.field private final mRlzHelper:Lcoy;

.field private final mScreenStateHelper:Ldmh;

.field private final mSearchBoxLogging:Lcpd;

.field final mSearchSettings:Lcke;

.field private final mStaticContentCache:Lcpq;

.field private final mVelvetServices:Lgql;

.field private final mVoiceSettings:Lhym;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 254
    const-string v0, "auth"

    const-string v1, "uberauth"

    invoke-static {v0, v1}, Lijp;->v(Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcpn;->beZ:Lijp;

    .line 256
    const-string v0, "X-Additional-Discourse-Context"

    const-string v1, "X-Geo"

    invoke-static {v0, v1}, Lijp;->v(Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcpn;->bfa:Lijp;

    .line 269
    const-string v0, "active"

    sput-object v0, Lcpn;->bfb:Ljava/lang/String;

    .line 270
    const-string v0, "images"

    sput-object v0, Lcpn;->bfc:Ljava/lang/String;

    .line 271
    const-string v0, "default"

    sput-object v0, Lcpn;->bfd:Ljava/lang/String;

    .line 275
    const-string v0, "a11y"

    sput-object v0, Lcpn;->bfe:Ljava/lang/String;

    .line 276
    const-string v0, "eyesfree"

    sput-object v0, Lcpn;->bff:Ljava/lang/String;

    .line 279
    const-string v0, "1"

    sput-object v0, Lcpn;->bfg:Ljava/lang/String;

    .line 280
    const-string v0, "0"

    sput-object v0, Lcpn;->bfh:Ljava/lang/String;

    .line 284
    const-string v0, "android-lockscreen"

    sput-object v0, Lcpn;->bfi:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcke;Lcjs;Lchk;Lcxs;Lemp;Lcfu;Lgql;Lcpd;Lcoy;Ligi;Lcoe;Lgpf;Lhym;Lcla;Lcrh;Ljava/lang/String;Ligi;Lcpq;Landroid/content/res/Resources;Lckw;)V
    .locals 4

    .prologue
    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338
    iput-object p1, p0, Lcpn;->mSearchSettings:Lcke;

    .line 339
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcjs;

    iput-object v1, p0, Lcpn;->mConfig:Lcjs;

    .line 340
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lchk;

    iput-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    .line 341
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcxs;

    iput-object v1, p0, Lcpn;->bfk:Lcxs;

    .line 342
    iput-object p7, p0, Lcpn;->mVelvetServices:Lgql;

    .line 343
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lemp;

    iput-object v1, p0, Lcpn;->mClock:Lemp;

    .line 344
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcfu;

    iput-object v1, p0, Lcpn;->mCorpora:Lcfu;

    .line 345
    invoke-static {p9}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcoy;

    iput-object v1, p0, Lcpn;->mRlzHelper:Lcoy;

    .line 346
    invoke-static {p8}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcpd;

    iput-object v1, p0, Lcpn;->mSearchBoxLogging:Lcpd;

    .line 347
    iput-object p10, p0, Lcpn;->bfl:Ligi;

    .line 348
    iput-object p11, p0, Lcpn;->mPartner:Lcoe;

    .line 349
    move-object/from16 v0, p12

    iput-object v0, p0, Lcpn;->mCookies:Lgpf;

    .line 350
    move-object/from16 v0, p13

    iput-object v0, p0, Lcpn;->mVoiceSettings:Lhym;

    .line 351
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE, dd MMM yyyy HH:mm:ss\' GMT\'"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, Lcpn;->bfm:Ljava/text/SimpleDateFormat;

    .line 352
    iget-object v1, p0, Lcpn;->bfm:Ljava/text/SimpleDateFormat;

    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 353
    move-object/from16 v0, p14

    iput-object v0, p0, Lcpn;->mDiscourseContextHelper:Lcla;

    .line 354
    move-object/from16 v0, p15

    iput-object v0, p0, Lcpn;->mLoginHelper:Lcrh;

    .line 355
    move-object/from16 v0, p16

    iput-object v0, p0, Lcpn;->bfn:Ljava/lang/String;

    .line 356
    move-object/from16 v0, p17

    iput-object v0, p0, Lcpn;->bfo:Ligi;

    .line 357
    iget-object v1, p0, Lcpn;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJU()Lgpu;

    move-result-object v1

    invoke-virtual {v1}, Lgpu;->aJM()Ldmh;

    move-result-object v1

    iput-object v1, p0, Lcpn;->mScreenStateHelper:Ldmh;

    .line 358
    move-object/from16 v0, p18

    iput-object v0, p0, Lcpn;->mStaticContentCache:Lcpq;

    .line 359
    move-object/from16 v0, p19

    iput-object v0, p0, Lcpn;->mResources:Landroid/content/res/Resources;

    .line 360
    move-object/from16 v0, p20

    iput-object v0, p0, Lcpn;->mDebugFeatures:Lckw;

    .line 361
    return-void
.end method

.method public static Q(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcpn;->bfa:Lijp;

    invoke-virtual {v0, p0}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    const-string p1, "REDACTED"

    .line 516
    :cond_0
    return-object p1
.end method

.method private RA()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 969
    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Ka()Z

    move-result v1

    if-nez v1, :cond_1

    .line 975
    :cond_0
    :goto_0
    return v0

    .line 972
    :cond_1
    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Kb()J

    move-result-wide v2

    .line 973
    iget-object v1, p0, Lcpn;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 974
    iget-object v1, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->OH()J

    move-result-wide v6

    .line 975
    sub-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Rp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Ma()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NU()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcpn;->bfg:Ljava/lang/String;

    .line 544
    :goto_0
    return-object v0

    .line 541
    :cond_0
    sget-object v0, Lcpn;->bfh:Ljava/lang/String;

    goto :goto_0

    .line 544
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Rs()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NI()Ljava/lang/String;

    move-result-object v0

    .line 626
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 627
    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Using manual override for search domain: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    invoke-static {v0}, Lcpn;->hC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 637
    :goto_0
    return-object v0

    .line 632
    :cond_0
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lc()Ljava/lang/String;

    move-result-object v0

    .line 633
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 634
    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Using overriden search domain: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    invoke-static {v0}, Lcpn;->hC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 637
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static Rv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 689
    const-string v0, "www.google.com"

    return-object v0
.end method

.method private static T(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2385
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2386
    const/4 v0, 0x0

    .line 2388
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lchk;ZZ)I
    .locals 2

    .prologue
    .line 2604
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lchk;->Kd()Z

    move-result v0

    .line 2606
    :goto_0
    if-eqz v0, :cond_3

    .line 2607
    sget v0, Lcpn;->bfj:I

    if-nez v0, :cond_0

    .line 2608
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lcpn;->bfj:I

    .line 2611
    :cond_0
    sget v0, Lcpn;->bfj:I

    .line 2616
    :goto_1
    if-eqz p3, :cond_1

    .line 2617
    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 2619
    :cond_1
    return v0

    .line 2604
    :cond_2
    invoke-virtual {p1}, Lchk;->Ke()Z

    move-result v0

    goto :goto_0

    .line 2613
    :cond_3
    if-eqz p2, :cond_4

    const v0, 0x7f0d00aa

    :goto_2
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1

    :cond_4
    const v0, 0x7f0d00ab

    goto :goto_2
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 1719
    invoke-static {p1, p2}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1720
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1722
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1727
    :goto_0
    return v0

    .line 1724
    :catch_0
    move-exception v1

    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid integer value \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" in search URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1727
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;
    .locals 2
    .param p2    # Ljava/util/Set;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2504
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1, p2, p3}, Lcpn;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;
    .locals 5
    .param p3    # Ljava/util/Set;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2521
    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2522
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {p2}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 2526
    invoke-virtual {p2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2527
    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2528
    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2529
    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 2534
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    move-object v2, v0

    .line 2536
    :cond_2
    invoke-virtual {v2, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2538
    if-eqz p4, :cond_3

    .line 2539
    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2540
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 2543
    :cond_3
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcpn;)Lchk;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    return-object v0
.end method

.method public static a(Lcom/google/android/search/shared/api/UriRequest;Ljava/lang/String;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 5

    .prologue
    .line 2577
    invoke-virtual {p0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 2578
    invoke-virtual {p0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2579
    invoke-virtual {p0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2580
    const-string v4, "q"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    .line 2581
    invoke-virtual {v1, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 2584
    :cond_1
    const-string v0, "q"

    invoke-virtual {v1, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2585
    new-instance v0, Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    return-object v0
.end method

.method private a(Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZ)Lcpo;
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1136
    invoke-virtual {p0, p1}, Lcpn;->p(Lcom/google/android/shared/search/Query;)Lckh;

    move-result-object v0

    .line 1137
    invoke-virtual {v0}, Lckh;->OK()Ljava/lang/String;

    move-result-object v1

    .line 1138
    invoke-virtual {p0, p1}, Lcpn;->q(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    move v6, p4

    .line 1139
    invoke-direct/range {v0 .. v6}, Lcpn;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZZ)Lcpo;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Lcpo;
    .locals 9
    .param p2    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1163
    .line 1167
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v0

    .line 1169
    invoke-virtual {p0, p1, v0}, Lcpn;->R(Ljava/lang/String;Ljava/lang/String;)Lcpo;

    move-result-object v1

    .line 1171
    if-eqz p2, :cond_0

    move-object v0, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    .line 1172
    invoke-virtual/range {v0 .. v8}, Lcpn;->a(Lcpo;Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 1175
    :cond_0
    invoke-direct {p0, v1, p2}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;)V

    .line 1176
    return-object v1
.end method

.method private a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZZ)Lcpo;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1146
    const/4 v3, 0x0

    .line 1147
    if-eqz p2, :cond_0

    .line 1148
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 1149
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v3

    .line 1151
    :cond_0
    invoke-virtual {p0}, Lcpn;->Ry()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v8}, Lcpn;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Lcpo;

    move-result-object v0

    .line 1153
    if-eqz p6, :cond_1

    .line 1154
    invoke-virtual {v0}, Lcpo;->RM()Lcpo;

    .line 1155
    iget-object v1, p0, Lcpn;->bfq:Lgnu;

    invoke-virtual {v0, v1}, Lcpo;->e(Lgnu;)V

    .line 1157
    :cond_1
    return-object v0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1704
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    .line 1705
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1706
    if-nez v1, :cond_1

    .line 1714
    :cond_0
    :goto_0
    return-object v0

    .line 1709
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1710
    invoke-virtual {v1, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1711
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 1712
    goto :goto_0
.end method

.method public static a(Landroid/net/Uri$Builder;)V
    .locals 3

    .prologue
    .line 791
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 792
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v0

    .line 793
    if-eqz v0, :cond_0

    .line 794
    const-string v1, "%20"

    const-string v2, "+"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 796
    :cond_0
    return-void
.end method

.method private a(Lcpo;)V
    .locals 5

    .prologue
    .line 1051
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->JT()I

    move-result v0

    .line 1052
    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->JU()Ljava/lang/String;

    move-result-object v1

    .line 1053
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    .line 1054
    invoke-virtual {p1}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v3

    .line 1055
    invoke-virtual {v3}, Lcom/google/android/search/shared/api/UriRequest;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    sub-int/2addr v0, v3

    sub-int v2, v0, v2

    .line 1056
    if-lez v2, :cond_0

    .line 1057
    const-string v0, ""

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v2, :cond_1

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    :cond_0
    return-void

    .line 1057
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_1
    if-ge v0, v2, :cond_2

    const/16 v4, 0x30

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcpo;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 1230
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->HH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1234
    invoke-virtual {p1}, Lcpo;->RQ()Lcpo;

    .line 1236
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1239
    invoke-virtual {p1}, Lcpo;->RR()Lcpo;

    .line 1241
    :cond_1
    return-void
.end method

.method private a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1078
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1079
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1080
    invoke-virtual {p1}, Lcpo;->RZ()Lcpo;

    .line 1084
    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Lcpo;->hZ(Ljava/lang/String;)Lcpo;

    .line 1087
    :cond_1
    if-eqz p4, :cond_2

    .line 1088
    invoke-virtual {p1, p4}, Lcpo;->ij(Ljava/lang/String;)Lcpo;

    .line 1091
    :cond_2
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lr()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1092
    invoke-virtual {p1}, Lcpo;->RY()Lcpo;

    .line 1095
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->getSelectionStart()I

    move-result v0

    .line 1096
    if-ltz v0, :cond_4

    .line 1097
    invoke-virtual {p1, v0}, Lcpo;->fl(I)Lcpo;

    .line 1101
    :cond_4
    iget-object v0, p0, Lcpn;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0}, Lcpd;->Ri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcpo;->ik(Ljava/lang/String;)Lcpo;

    .line 1102
    iget-object v0, p0, Lcpn;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0}, Lcpd;->Rk()I

    move-result v0

    invoke-virtual {p1, v0}, Lcpo;->fm(I)Lcpo;

    .line 1104
    invoke-virtual {p0}, Lcpn;->Ro()Ljava/lang/String;

    move-result-object v0

    .line 1105
    if-eqz v0, :cond_5

    .line 1106
    invoke-virtual {p1, v0}, Lcpo;->il(Ljava/lang/String;)Lcpo;

    .line 1112
    :cond_5
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->II()[Ljava/lang/String;

    move-result-object v0

    .line 1113
    if-eqz v0, :cond_6

    .line 1114
    invoke-virtual {p1, v0}, Lcpo;->i([Ljava/lang/String;)Lcpo;

    .line 1116
    :cond_6
    return-void
.end method

.method private a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1399
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->ara()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->arb()J

    move-result-wide v0

    .line 1401
    :goto_0
    iget-object v5, p0, Lcpn;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    .line 1402
    invoke-virtual {v5}, Lcfo;->DX()Lenm;

    move-result-object v5

    .line 1403
    invoke-interface {v5}, Lenm;->auH()Z

    move-result v5

    .line 1404
    invoke-virtual {p1, p3}, Lcpo;->ig(Ljava/lang/String;)Lcpo;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Lcpo;->ac(J)Lcpo;

    move-result-object v0

    iget-object v1, p0, Lcpn;->mRlzHelper:Lcoy;

    invoke-virtual {v1}, Lcoy;->Rf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpo;->ib(Ljava/lang/String;)Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RU()Lcpo;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcpo;->cE(Z)Lcpo;

    move-result-object v0

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpo;->hO(Ljava/lang/String;)Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RW()Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RO()Lcpo;

    move-result-object v0

    iget-object v1, p0, Lcpn;->bfn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcpo;->ii(Ljava/lang/String;)Lcpo;

    .line 1415
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqm()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_0
    move v0, v3

    :goto_1
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v1

    if-eqz v1, :cond_c

    if-eqz v0, :cond_b

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqe()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "vs-asst-actions"

    .line 1416
    :goto_2
    if-eqz v0, :cond_1

    .line 1417
    invoke-virtual {p1, v0}, Lcpo;->hT(Ljava/lang/String;)Lcpo;

    .line 1421
    :cond_1
    if-eqz p5, :cond_2

    .line 1422
    iget-object v0, p0, Lcpn;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0, p2, p1}, Lcpd;->a(Lcom/google/android/shared/search/Query;Lcpo;)V

    .line 1425
    :cond_2
    iget-object v0, p0, Lcpn;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DP()Lcob;

    move-result-object v0

    .line 1426
    invoke-interface {v0}, Lcob;->QL()Z

    move-result v1

    .line 1427
    if-eqz v1, :cond_10

    iget-object v0, p0, Lcpn;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->El()Lfdr;

    move-result-object v0

    invoke-interface {v0}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v0

    .line 1430
    :goto_3
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqW()Landroid/location/Location;

    move-result-object v4

    invoke-virtual {p1, v1, v0, v4}, Lcpo;->a(ZLandroid/location/Location;Landroid/location/Location;)Lcpo;

    .line 1440
    invoke-direct {p0}, Lcpn;->Rp()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1441
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Kj()Z

    move-result v0

    if-nez v0, :cond_13

    .line 1442
    invoke-direct {p0}, Lcpn;->Rp()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcpn;->bfg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1446
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcpn;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->adq()I

    move-result v0

    sget v4, Ldmh;->bCo:I

    if-ne v0, v4, :cond_11

    move v0, v3

    .line 1449
    :goto_4
    if-eqz v1, :cond_12

    if-nez v0, :cond_12

    .line 1450
    sget-object v0, Lcpn;->bfg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcpo;->hX(Ljava/lang/String;)Lcpo;

    .line 1462
    :cond_3
    :goto_5
    iget-object v0, p0, Lcpn;->bfk:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    invoke-virtual {v0}, Lcxi;->TA()I

    move-result v0

    .line 1463
    if-eq v0, v7, :cond_15

    .line 1464
    if-ne v0, v3, :cond_14

    :goto_6
    invoke-virtual {p1, v3}, Lcpo;->cD(Z)Lcpo;

    .line 1475
    :cond_4
    :goto_7
    invoke-virtual {p1}, Lcpo;->RI()Lcpo;

    .line 1476
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1477
    invoke-virtual {p1, p4}, Lcpo;->if(Ljava/lang/String;)Lcpo;

    .line 1480
    :cond_5
    invoke-virtual {p1}, Lcpo;->RJ()Lcpo;

    .line 1482
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Kf()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1483
    iget-object v0, p0, Lcpn;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DY()Lgno;

    move-result-object v0

    invoke-virtual {v0}, Lgno;->aAn()I

    move-result v0

    .line 1484
    if-eq v0, v7, :cond_6

    .line 1485
    invoke-virtual {p1, v0}, Lcpo;->fn(I)Lcpo;

    .line 1489
    :cond_6
    invoke-direct {p0, p1}, Lcpn;->d(Lcpo;)V

    .line 1498
    invoke-virtual {p1}, Lcpo;->RX()Lcpo;

    .line 1499
    return-void

    .line 1399
    :cond_7
    iget-object v0, p0, Lcpn;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 1415
    goto/16 :goto_1

    :cond_9
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqd()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "vs-hw-asst"

    goto/16 :goto_2

    :cond_a
    const-string v0, "vs-asst"

    goto/16 :goto_2

    :cond_b
    const-string v0, "asst"

    goto/16 :goto_2

    :cond_c
    if-eqz v0, :cond_f

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqd()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "vs-hw"

    goto/16 :goto_2

    :cond_d
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "vs-projected"

    goto/16 :goto_2

    :cond_e
    const-string v0, "vs"

    goto/16 :goto_2

    :cond_f
    move-object v0, v4

    goto/16 :goto_2

    :cond_10
    move-object v0, v4

    .line 1427
    goto/16 :goto_3

    :cond_11
    move v0, v2

    .line 1446
    goto/16 :goto_4

    .line 1452
    :cond_12
    sget-object v0, Lcpn;->bfh:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcpo;->hX(Ljava/lang/String;)Lcpo;

    goto/16 :goto_5

    .line 1455
    :cond_13
    sget-object v0, Lcpn;->bfi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcpo;->ie(Ljava/lang/String;)Lcpo;

    .line 1456
    invoke-direct {p0}, Lcpn;->Rp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcpo;->hX(Ljava/lang/String;)Lcpo;

    goto/16 :goto_5

    :cond_14
    move v3, v2

    .line 1464
    goto/16 :goto_6

    .line 1468
    :cond_15
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ov()I

    move-result v0

    .line 1469
    if-eq v0, v7, :cond_4

    .line 1470
    if-ne v0, v3, :cond_16

    :goto_8
    invoke-virtual {p1, v3}, Lcpo;->cD(Z)Lcpo;

    goto/16 :goto_7

    :cond_16
    move v3, v2

    goto :goto_8
.end method

.method private static a(Ljjq;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 2287
    if-eqz p0, :cond_0

    .line 2303
    const-string v0, "X-Additional-Discourse-Context"

    invoke-static {p0}, Leqh;->c(Ljsr;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2307
    :cond_0
    return-void
.end method

.method private a(Landroid/net/Uri;ZZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1785
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 1786
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 1788
    if-eqz p3, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1803
    :cond_0
    :goto_0
    return v0

    .line 1792
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1793
    const/4 v0, 0x1

    goto :goto_0

    .line 1796
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1803
    :cond_3
    invoke-virtual {p0, v1}, Lcpn;->hH(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcpn;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcpn;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method private b(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5
    .param p1    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2477
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ":"

    const-string v1, "%3A"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2478
    invoke-virtual {v2}, Landroid/net/Uri;->isRelative()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2480
    if-eqz p1, :cond_1

    .line 2481
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 2482
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 2487
    :goto_0
    invoke-static {v0, v1, v2, v4, v4}, Lcpn;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v0

    .line 2490
    :goto_1
    return-object v0

    .line 2484
    :cond_1
    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v0

    .line 2485
    invoke-direct {p0, v3, v3}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 2490
    goto :goto_1
.end method

.method public static b(Landroid/location/Location;)Landroid/util/Pair;
    .locals 3

    .prologue
    .line 1216
    new-instance v0, Landroid/util/Pair;

    const-string v1, "X-Geo"

    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcqg;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static b(Lcom/google/android/search/shared/api/UriRequest;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 3

    .prologue
    .line 2589
    invoke-virtual {p0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2590
    const-string v1, "reload"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2591
    new-instance v1, Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    return-object v1
.end method

.method private b(Lcpo;)V
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->JO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063
    invoke-virtual {p1}, Lcpo;->Sa()Lcpo;

    .line 1067
    :goto_0
    return-void

    .line 1065
    :cond_0
    invoke-virtual {p1}, Lcpo;->RL()Lcpo;

    goto :goto_0
.end method

.method private c(Lcom/google/android/shared/search/Query;Ljava/lang/String;)Lcpo;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    .line 1180
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    invoke-virtual {v0}, Lehm;->asL()Ljava/lang/String;

    move-result-object v0

    .line 1181
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1182
    if-nez v2, :cond_0

    .line 1183
    const-string v1, "Search.SearchUrlHelper"

    const-string v2, "Invalid WebApp request url: %s"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1184
    const/4 v1, 0x0

    .line 1191
    :goto_0
    return-object v1

    .line 1186
    :cond_0
    new-instance v1, Lcpo;

    invoke-direct {v1, p0, v2}, Lcpo;-><init>(Lcpn;Landroid/net/Uri;)V

    .line 1187
    if-eqz p1, :cond_1

    .line 1188
    invoke-virtual {p0}, Lcpn;->Ry()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1190
    :cond_1
    invoke-direct {p0, v1, p1}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method private c(Lcpo;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1248
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NJ()Ljava/lang/String;

    move-result-object v0

    .line 1249
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1251
    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Using local host parameter from preferences: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    :goto_0
    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    .line 1259
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ".sandbox.google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1262
    invoke-virtual {p1, v0}, Lcpo;->hP(Ljava/lang/String;)Lcpo;

    .line 1264
    :cond_0
    return-void

    .line 1253
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcpo;
    .locals 3

    .prologue
    .line 706
    if-nez p1, :cond_0

    .line 707
    const/4 v0, 0x0

    .line 714
    :goto_0
    return-object v0

    .line 710
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 711
    new-instance v0, Lcpo;

    invoke-direct {v0, p0, v1}, Lcpo;-><init>(Lcpn;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d(Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2125
    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Lx()Ljava/lang/String;

    move-result-object v1

    .line 2128
    if-eqz p1, :cond_1

    invoke-direct {p0, p1, v0, p2}, Lcpn;->a(Landroid/net/Uri;ZZ)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2130
    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Lw()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2131
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2132
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2137
    :goto_1
    return-object v0

    .line 2130
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2137
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static d(Ljava/util/Locale;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 410
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 411
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v1

    .line 413
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 414
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    :cond_0
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 417
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 418
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcpo;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1267
    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Mn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1269
    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Mn()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    :cond_0
    const-string v1, "X-Client-Data"

    iget-object v2, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Mm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v2, Ljxq;

    invoke-direct {v2}, Ljxq;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v6, v4, v1

    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v7, "Search.SearchUrlHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Invalid integer value \""

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\" in experiments IDs."

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Ft()[I

    move-result-object v1

    array-length v4, v1

    :goto_2
    if-ge v0, v4, :cond_2

    aget v5, v1, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Fr()J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-eqz v4, :cond_4

    invoke-virtual {v2, v0, v1}, Ljxq;->dN(J)Ljxq;

    :goto_3
    invoke-static {v3}, Lius;->I(Ljava/util/Collection;)[I

    move-result-object v0

    iput-object v0, v2, Ljxq;->eKW:[I

    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Leqh;->c(Ljsr;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    :cond_3
    :goto_4
    return-void

    .line 1272
    :cond_4
    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "No config timestamp found."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mm()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public static fh(I)I
    .locals 1

    .prologue
    .line 743
    const/16 v0, 0x14

    if-ge p0, v0, :cond_0

    .line 744
    const/4 v0, 0x0

    .line 748
    :goto_0
    return v0

    .line 745
    :cond_0
    const/16 v0, 0x64

    if-ge p0, v0, :cond_1

    .line 746
    const/4 v0, 0x1

    goto :goto_0

    .line 748
    :cond_1
    div-int/lit8 v0, p0, 0x64

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static fi(I)I
    .locals 2

    .prologue
    const/16 v0, 0x14

    .line 754
    if-ge p0, v0, :cond_1

    .line 755
    const/4 v0, 0x0

    .line 759
    :cond_0
    :goto_0
    return v0

    .line 756
    :cond_1
    const/16 v1, 0x64

    if-lt p0, v1, :cond_0

    .line 759
    div-int/lit8 v0, p0, 0x64

    mul-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method

.method public static hA(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 487
    if-nez p0, :cond_1

    .line 488
    const/4 p0, 0x0

    .line 500
    :cond_0
    :goto_0
    return-object p0

    .line 490
    :cond_1
    const-string v0, "iw"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 492
    const-string p0, "he"

    goto :goto_0

    .line 493
    :cond_2
    const-string v0, "in"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 495
    const-string p0, "id"

    goto :goto_0

    .line 496
    :cond_3
    const-string v0, "ji"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    const-string p0, "yi"

    goto :goto_0
.end method

.method public static hB(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    if-nez p0, :cond_0

    .line 506
    const-string v0, ""

    .line 508
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static hC(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 666
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 668
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "www"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 675
    :cond_0
    return-object p0

    .line 666
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static hD(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 679
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 681
    const-string v0, "www."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 685
    :cond_0
    return-object p0

    .line 679
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hI(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1980
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private i(ZZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 577
    if-nez p1, :cond_1

    .line 578
    invoke-direct {p0}, Lcpn;->Rs()Ljava/lang/String;

    move-result-object v0

    .line 579
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 609
    :goto_0
    return-object v0

    .line 587
    :cond_0
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lb()Ljava/lang/String;

    move-result-object v0

    .line 588
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 589
    invoke-static {v0}, Lcpn;->hC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 595
    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ny()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 596
    const-string v0, "www.google.com"

    goto :goto_0

    .line 601
    :cond_2
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Nz()Ljava/lang/String;

    move-result-object v0

    .line 602
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 607
    const-string v0, "www.google.com"

    goto :goto_0

    .line 609
    :cond_3
    invoke-static {v0}, Lcpn;->hC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 522
    if-nez p0, :cond_0

    .line 523
    const-string v0, ""

    .line 530
    :goto_0
    return-object v0

    .line 525
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcpn;->beZ:Lijp;

    invoke-static {v0, v1}, Ljava/util/Collections;->disjoint(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 527
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "REDACTED"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 528
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 530
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static n(Landroid/net/Uri;)Ljava/util/Map;
    .locals 6

    .prologue
    .line 1743
    invoke-static {}, Lior;->aYa()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 1744
    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    .line 1745
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1746
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1747
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1752
    if-eqz v3, :cond_1

    .line 1753
    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1755
    :cond_1
    const-string v0, "Search.SearchUrlHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error parsing URL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1759
    :cond_2
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    .line 1760
    if-eqz v0, :cond_4

    .line 1761
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 1762
    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    .line 1763
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1764
    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1765
    if-eqz v4, :cond_3

    .line 1766
    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1768
    :cond_3
    const-string v0, "Search.SearchUrlHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error parsing URL [fragment]: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1772
    :cond_4
    return-object v1
.end method

.method private s(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2237
    const-string v1, "https"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v0, v0}, Lcpn;->a(Landroid/net/Uri;ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final R(Ljava/lang/String;Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 719
    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcpn;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcpo;

    move-result-object v0

    return-object v0
.end method

.method public final RB()Landroid/util/Pair;
    .locals 4

    .prologue
    .line 1221
    new-instance v0, Landroid/util/Pair;

    const-string v1, "Cookie"

    iget-object v2, p0, Lcpn;->mCookies:Lgpf;

    invoke-virtual {p0}, Lcpn;->RC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgpf;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final RC()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1576
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v1}, Lcfu;->EA()Lckh;

    move-result-object v1

    invoke-virtual {v1}, Lckh;->OK()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final RD()Lcom/google/android/search/shared/api/UriRequest;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2165
    const-string v0, "https"

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2167
    if-eqz v0, :cond_0

    .line 2168
    invoke-direct {p0, v5, v5}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v0

    .line 2173
    :goto_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->IV()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "https"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2175
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 2176
    invoke-virtual {p0, v0, v1}, Lcpn;->a(Landroid/net/Uri;Ljava/util/Map;)V

    .line 2177
    new-instance v2, Lcom/google/android/search/shared/api/UriRequest;

    invoke-direct {v2, v0, v1}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    return-object v2

    .line 2170
    :cond_0
    const-string v0, "www.google.com"

    goto :goto_0
.end method

.method public final RE()Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2207
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->HW()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final RF()Landroid/net/Uri;
    .locals 5

    .prologue
    .line 2216
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Jn()Ljava/lang/String;

    move-result-object v1

    .line 2218
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2220
    iget-object v0, p0, Lcpn;->bfl:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Lcpn;->d(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2221
    iget-object v2, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v2, v0}, Lchk;->gp(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2222
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2229
    :goto_0
    return-object v0

    .line 2224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2229
    :cond_1
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Jm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final RG()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 2548
    const/4 v0, 0x0

    .line 2560
    iget-object v2, p0, Lcpn;->mDebugFeatures:Lckw;

    invoke-virtual {v2}, Lckw;->Pd()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcpn;->mDebugFeatures:Lckw;

    invoke-virtual {v2}, Lckw;->Pa()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 2563
    :cond_0
    if-lez v0, :cond_1

    .line 2564
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 2566
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Rm()V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcpn;->mDiscourseContextHelper:Lcla;

    iget-object v1, v0, Lcla;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Kn()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcla;->mRecentContextApiClient:Lclh;

    invoke-virtual {v1}, Lclh;->connect()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcla;->baH:Z

    .line 365
    :cond_0
    return-void
.end method

.method final Rn()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 457
    iget-object v0, p0, Lcpn;->bfl:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iget-object v1, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->NC()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcpn;->d(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v3, v4

    if-ne v3, v7, :cond_0

    new-instance v3, Ljava/util/Locale;

    aget-object v4, v4, v6

    invoke-direct {v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    :goto_0
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    array-length v3, v4

    if-ne v3, v8, :cond_3

    aget-object v3, v4, v7

    const-string v5, "Hant"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    aget-object v3, v4, v7

    const-string v5, "Hans"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    new-instance v3, Ljava/util/Locale;

    aget-object v5, v4, v6

    const-string v6, ""

    aget-object v4, v4, v7

    invoke-direct {v3, v5, v6, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/util/Locale;

    aget-object v5, v4, v6

    aget-object v4, v4, v7

    invoke-direct {v3, v5, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    array-length v3, v4

    const/4 v5, 0x3

    if-ne v3, v5, :cond_4

    new-instance v3, Ljava/util/Locale;

    aget-object v5, v4, v6

    aget-object v6, v4, v8

    aget-object v4, v4, v7

    invoke-direct {v3, v5, v6, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v3, "Search.SearchUrlHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unsupported locale format: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x5

    invoke-static {v6, v3, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public final Ro()Ljava/lang/String;
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NB()Ljava/lang/String;

    move-result-object v0

    .line 465
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 468
    :cond_0
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "gb"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "uk"

    goto :goto_0
.end method

.method public final Rq()Ljava/lang/String;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 549
    invoke-direct {p0, v0, v0}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Rr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 561
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Rt()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NH()Ljava/lang/String;

    move-result-object v0

    .line 642
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 645
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NA()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final Ru()Z
    .locals 2

    .prologue
    .line 657
    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v0

    .line 658
    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->NH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Rw()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 826
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LW()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "https"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Rx()Lcpo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 834
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Hk()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v1}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpn;->R(Ljava/lang/String;Ljava/lang/String;)Lcpo;

    move-result-object v0

    return-object v0
.end method

.method final Ry()Ljava/lang/String;
    .locals 2

    .prologue
    .line 842
    iget-object v0, p0, Lcpn;->mPartner:Lcoe;

    invoke-virtual {v0}, Lcoe;->QV()Ljava/lang/String;

    move-result-object v0

    .line 844
    if-eqz v0, :cond_0

    .line 847
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ms-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcpn;->mPartner:Lcoe;

    invoke-virtual {v1}, Lcoe;->QW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final Rz()Z
    .locals 3

    .prologue
    .line 856
    const-string v0, "www.google.com"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final S(Ljava/lang/String;Ljava/lang/String;)Lcpo;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1606
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1607
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1608
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Hh()Ljava/lang/String;

    move-result-object v0

    .line 1611
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/charset/Charset;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/charset/Charset;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p2, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1619
    :goto_0
    new-instance v1, Lcpo;

    invoke-direct {v1, p0, v0}, Lcpo;-><init>(Lcpn;Ljava/lang/String;)V

    .line 1621
    return-object v1

    .line 1615
    :catch_0
    move-exception v1

    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to URL encode query and url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v2, v9, [Ljava/lang/Object;

    invoke-direct {p0, v6, v6}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, "@|&"

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "@|&"

    const-string v4, ""

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(JJIZZZ)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 800
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LV()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "https"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 802
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 804
    const-string v2, "client"

    if-eqz p8, :cond_2

    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LY()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 807
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 808
    const-string v0, "min"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 810
    :cond_0
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 811
    const-string v0, "max"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 813
    :cond_1
    const-string v0, "num"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 816
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 804
    :cond_2
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LX()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljyr;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 5
    .param p1    # Ljyr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1632
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%1$s://%2$s/velog/action"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1634
    new-instance v1, Lcpo;

    invoke-direct {v1, p0, v0}, Lcpo;-><init>(Lcpn;Ljava/lang/String;)V

    .line 1635
    const-string v0, "pb"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    invoke-virtual {v1}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1822
    .line 1823
    invoke-direct {p0, p2, v5, v5}, Lcpn;->a(Landroid/net/Uri;ZZ)Z

    move-result v2

    .line 1825
    if-eqz v2, :cond_6

    .line 1826
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcjs;->gI(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1829
    invoke-virtual {p2}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1830
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LE()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1833
    :goto_0
    if-eqz v0, :cond_2

    .line 1834
    iget-object v2, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v2, v0}, Lcfu;->gh(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1861
    :goto_1
    if-eqz v3, :cond_9

    .line 1862
    const-string v0, "output"

    invoke-static {p2, v0}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1863
    const-string v2, "rss"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "atom"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_0
    move-object v0, v1

    .line 1876
    :cond_1
    :goto_2
    return-object v0

    .line 1836
    :cond_2
    const-string v3, "web"

    goto :goto_1

    .line 1839
    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcpn;->d(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    .line 1840
    :goto_3
    if-eqz v0, :cond_4

    .line 1842
    invoke-virtual {p0, p1, v0}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1843
    if-nez v0, :cond_1

    :cond_4
    move-object v3, v1

    .line 1847
    goto :goto_1

    .line 1839
    :cond_5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 1848
    :cond_6
    iget-object v0, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EB()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1850
    iget-object v0, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EC()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckh;

    .line 1854
    invoke-virtual {v0, p2, v2}, Lckh;->c(Landroid/net/Uri;Z)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1856
    iget-object v3, v0, Lcfy;->aqV:Ljava/lang/String;

    goto :goto_1

    .line 1867
    :cond_8
    const-string v0, "q"

    invoke-static {p2, v0}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1868
    if-eqz v2, :cond_9

    .line 1869
    const-string v0, "start"

    invoke-direct {p0, p2, v0, v5}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v4

    .line 1870
    const-string v0, "stick"

    invoke-static {p2, v0}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1871
    invoke-static {p2}, Lcpn;->n(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v6

    .line 1872
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/shared/search/Query;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_2

    :cond_9
    move-object v0, v1

    .line 1876
    goto :goto_2

    :cond_a
    move-object v3, v1

    goto :goto_1

    :cond_b
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1888
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1889
    if-eqz v0, :cond_0

    .line 1890
    invoke-virtual {v0, p3, p4, p5}, Lcom/google/android/shared/search/Query;->n(Ljava/lang/String;J)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1892
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JLehm;)Lcom/google/android/shared/search/Query;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1915
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1917
    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LE()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1922
    if-eqz v1, :cond_0

    .line 1923
    iget-object v2, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v2, v1}, Lcfu;->gh(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1928
    :goto_0
    const-string v1, "q"

    invoke-static {v0, v1}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1929
    if-eqz v2, :cond_1

    .line 1930
    const-string v1, "start"

    invoke-direct {p0, v0, v1, v4}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v4

    .line 1931
    const-string v1, "stick"

    invoke-static {v0, v1}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1932
    invoke-static {v0}, Lcpn;->n(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v8

    .line 1933
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    move-object v1, p4

    move-wide v6, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/shared/search/Query;->a(Lehm;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;JLjava/util/Map;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1938
    :goto_1
    return-object v0

    .line 1925
    :cond_0
    const-string v3, "web"

    goto :goto_0

    .line 1936
    :cond_1
    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "Can\'t find the query string from WebApp\'s directUrl: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1938
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 2250
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcpn;->s(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2251
    iget-object v0, p0, Lcpn;->mLoginHelper:Lcrh;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p3, v2, v3}, Lcrh;->k(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 2252
    if-eqz v0, :cond_0

    .line 2253
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcpn;->s(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Authorization"

    invoke-interface {p4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256
    :cond_0
    return-void
.end method

.method final a(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 2260
    iget-object v0, p0, Lcpn;->mCookies:Lgpf;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgpf;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2261
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2262
    const-string v1, "Cookie"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2266
    :goto_0
    return-void

    .line 2264
    :cond_0
    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "Auth token not ready, no auth header set."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcpo;Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1288
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->HY()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqS()Lijm;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "1"

    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqS()Lijm;

    move-result-object v1

    const-string v4, "noj"

    invoke-virtual {v1, v4}, Lijm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 1292
    :goto_0
    invoke-virtual {p1, v0}, Lcpo;->cC(Z)Lcpo;

    .line 1294
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1295
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcpo;->hU(Ljava/lang/String;)Lcpo;

    .line 1298
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1299
    invoke-virtual {p1, p2}, Lcpo;->hZ(Ljava/lang/String;)Lcpo;

    .line 1306
    :cond_1
    invoke-direct {p0, p1}, Lcpn;->c(Lcpo;)V

    .line 1309
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1310
    invoke-virtual {p1}, Lcpo;->RS()Lcpo;

    .line 1314
    :cond_2
    if-eqz p8, :cond_6

    .line 1315
    invoke-virtual {p0, p3}, Lcpn;->p(Lcom/google/android/shared/search/Query;)Lckh;

    move-result-object v0

    .line 1317
    if-eqz v0, :cond_5

    iget-object v1, v0, Lcfy;->aUN:Ljava/util/Map;

    if-eqz v1, :cond_5

    .line 1318
    iget-object v0, v0, Lcfy;->aUN:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 1319
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1320
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move v0, v3

    .line 1288
    goto :goto_0

    .line 1328
    :cond_5
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqc()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1329
    const/16 v0, 0x1f9f

    invoke-virtual {p1, v0}, Lcpo;->fk(I)Lcpo;

    .line 1334
    :cond_6
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqm()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1335
    :cond_7
    iget-object v0, p0, Lcpn;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 1336
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1337
    invoke-virtual {p1, v0}, Lcpo;->ic(Ljava/lang/String;)Lcpo;

    .line 1342
    :cond_8
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqR()I

    move-result v0

    if-lez v0, :cond_9

    .line 1343
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqR()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcpo;->ia(Ljava/lang/String;)Lcpo;

    .line 1346
    :cond_9
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqX()Ljava/lang/String;

    move-result-object v0

    .line 1347
    if-eqz v0, :cond_a

    .line 1348
    invoke-virtual {p1, v0}, Lcpo;->hV(Ljava/lang/String;)Lcpo;

    .line 1354
    :cond_a
    if-eqz p6, :cond_11

    .line 1355
    invoke-virtual {p1}, Lcpo;->RT()V

    .line 1361
    :goto_2
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcgg;->aVy:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    move v3, v2

    .line 1362
    :cond_c
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    .line 1363
    if-eqz v3, :cond_12

    if-eqz v0, :cond_12

    .line 1364
    sget-object v0, Lcpn;->bff:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcpo;->hR(Ljava/lang/String;)Lcpo;

    .line 1371
    :cond_d
    :goto_3
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqS()Lijm;

    move-result-object v0

    .line 1372
    if-eqz v0, :cond_e

    .line 1373
    invoke-virtual {p1, v0}, Lcpo;->g(Ljava/util/Map;)Lcpo;

    .line 1376
    :cond_e
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqr()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1377
    const-string v0, "ddle"

    invoke-virtual {p1, v0}, Lcpo;->im(Ljava/lang/String;)Lcpo;

    .line 1382
    :cond_f
    invoke-virtual {p3}, Lcom/google/android/shared/search/Query;->aqe()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1383
    const-string v0, "e20"

    invoke-virtual {p1, v0}, Lcpo;->hY(Ljava/lang/String;)Lcpo;

    :cond_10
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p7

    .line 1386
    invoke-direct/range {v0 .. v5}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1387
    return-void

    .line 1357
    :cond_11
    invoke-virtual {p1}, Lcpo;->RP()Lcpo;

    goto :goto_2

    .line 1365
    :cond_12
    iget-object v1, p0, Lcpn;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aKb()Lelo;

    move-result-object v1

    invoke-virtual {v1}, Lelo;->aum()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1366
    sget-object v0, Lcpn;->bfe:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcpo;->hR(Ljava/lang/String;)Lcpo;

    goto :goto_3

    .line 1367
    :cond_13
    if-eqz v0, :cond_d

    .line 1368
    sget-object v0, Lcpn;->bfd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcpo;->hR(Ljava/lang/String;)Lcpo;

    goto :goto_3
.end method

.method final a(Lgnu;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 2271
    iget-object v0, p0, Lcpn;->mDiscourseContextHelper:Lcla;

    invoke-virtual {v0, p1}, Lcla;->b(Lgnu;)Ljjq;

    move-result-object v0

    invoke-static {v0, p2}, Lcpn;->a(Ljjq;Ljava/util/Map;)V

    .line 2274
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Query;I)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 769
    invoke-static {p2}, Lcpn;->fh(I)I

    move-result v0

    .line 770
    invoke-static {p2}, Lcpn;->fi(I)I

    move-result v1

    .line 771
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v3, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->Hi()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-direct {p0, v6, v6}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 773
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 774
    iget-object v3, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->LE()Ljava/lang/String;

    move-result-object v3

    const-string v4, "isch"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "biw"

    const-string v5, "100"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "bih"

    const-string v5, "200"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "ifm"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "ijn"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "start"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "dbla"

    const-string v3, "1"

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 783
    invoke-static {v2}, Lcpn;->a(Landroid/net/Uri$Builder;)V

    .line 786
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;Landroid/net/Uri;)Landroid/util/Pair;
    .locals 5
    .param p2    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2027
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    .line 2028
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcpn;->d(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v1

    .line 2029
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2034
    invoke-direct {p0, p1, v1}, Lcpn;->b(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2036
    new-instance v2, Lcpo;

    invoke-direct {v2, p0, p1}, Lcpo;-><init>(Lcpn;Landroid/net/Uri;)V

    .line 2037
    invoke-virtual {v2}, Lcpo;->RU()Lcpo;

    .line 2039
    invoke-virtual {p0, p2, v1}, Lcpn;->c(Landroid/net/Uri;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 2040
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2041
    const-string v4, "Referer"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2043
    :cond_0
    new-instance v3, Lcom/google/android/search/shared/api/UriRequest;

    invoke-direct {v3, v1, v0}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    .line 2044
    invoke-static {v3, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 2046
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Lcom/google/android/search/shared/api/UriRequest;

    invoke-direct {v1, p1, v0}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    const/4 v0, 0x0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/shared/search/Query;Ljava/lang/String;Z)Lcom/google/android/search/shared/api/UriRequest;
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 917
    invoke-virtual {p0}, Lcpn;->RC()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 918
    new-instance v1, Lcpo;

    invoke-direct {v1, p0, v9}, Lcpo;-><init>(Lcpn;Landroid/net/Uri;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcpn;->Ry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1}, Lcpn;->q(Lcom/google/android/shared/search/Query;)Z

    move-result v6

    move-object v0, p0

    move-object v3, p1

    move-object v5, p2

    move v8, v7

    invoke-virtual/range {v0 .. v8}, Lcpn;->a(Lcpo;Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    :cond_0
    invoke-direct {p0, v1, p1}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;)V

    if-nez p3, :cond_1

    invoke-virtual {v1}, Lcpo;->RK()Lcpo;

    .line 920
    :cond_1
    invoke-virtual {v9}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcpo;->hS(Ljava/lang/String;)Lcpo;

    .line 922
    iget-object v0, p0, Lcpn;->bfo:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 923
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 924
    if-eqz v2, :cond_2

    .line 925
    const-string v3, "user-agent-suffix"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 926
    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 927
    const-string v3, "%s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    aput-object v2, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 930
    :cond_2
    invoke-virtual {v1, v0}, Lcpo;->ih(Ljava/lang/String;)Lcpo;

    .line 931
    invoke-virtual {v1}, Lcpo;->RM()Lcpo;

    .line 932
    iget-object v0, p0, Lcpn;->bfq:Lgnu;

    invoke-virtual {v1, v0}, Lcpo;->e(Lgnu;)V

    .line 933
    invoke-virtual {v1}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/shared/search/Query;)Lcpo;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 725
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 726
    if-nez v1, :cond_1

    .line 737
    :cond_0
    :goto_0
    return-object v0

    .line 729
    :cond_1
    const-string v2, "not supported"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 730
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 733
    const-string v0, "not supported"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 734
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 735
    invoke-direct {p0, p1, v0, v2}, Lcpn;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcpo;

    move-result-object v0

    goto :goto_0

    .line 737
    :cond_2
    invoke-virtual {p0, p1, v2}, Lcpn;->R(Ljava/lang/String;Ljava/lang/String;)Lcpo;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcpn;->bfp:Landroid/graphics/Point;

    .line 378
    return-void
.end method

.method final b(Lgnu;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 2279
    iget-object v0, p0, Lcpn;->mDiscourseContextHelper:Lcla;

    invoke-virtual {v0, p1}, Lcla;->c(Lgnu;)Ljjq;

    move-result-object v0

    invoke-static {v0, p2}, Lcpn;->a(Ljjq;Ljava/util/Map;)V

    .line 2283
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 2405
    invoke-static {p1, p2}, Lcom/google/android/shared/search/Query;->i(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcpn;->d(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/net/Uri;Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p2    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2055
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcpn;->Rs()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2060
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2063
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2064
    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2068
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 2070
    :cond_0
    invoke-static {p1}, Lesp;->aD(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2072
    :goto_1
    return-object v0

    .line 2066
    :cond_1
    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 2072
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public final c(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 2422
    invoke-static {p1, p2}, Lcom/google/android/shared/search/Query;->j(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcpn;->d(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cB(Z)Lcpo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1668
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 1669
    if-eqz p1, :cond_1

    .line 1670
    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1671
    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1672
    invoke-direct {p0, v3, v3}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1681
    :goto_0
    new-instance v1, Lcpo;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcpo;-><init>(Lcpn;Landroid/net/Uri;)V

    return-object v1

    .line 1674
    :cond_0
    const-string v1, "www.google.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 1677
    :cond_1
    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1678
    invoke-direct {p0, v3, v3}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public final d(Lcom/google/android/shared/search/Query;Ljava/lang/String;)Lcpo;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1585
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1587
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LT()Ljava/lang/String;

    move-result-object v0

    .line 1588
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1590
    new-instance v1, Lcpo;

    invoke-direct {v1, p0, v0}, Lcpo;-><init>(Lcpn;Ljava/lang/String;)V

    .line 1594
    iget-object v0, p0, Lcpn;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0, p1, v1}, Lcpd;->a(Lcom/google/android/shared/search/Query;Lcpo;)V

    .line 1597
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcpo;->hZ(Ljava/lang/String;)Lcpo;

    .line 1599
    invoke-virtual {v1, p2}, Lcpo;->hQ(Ljava/lang/String;)Lcpo;

    .line 1602
    return-object v1
.end method

.method public final d(Lgnu;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcpn;->bfq:Lgnu;

    .line 382
    return-void
.end method

.method public final d(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2347
    invoke-direct {p0, p2, v0, v0}, Lcpn;->a(Landroid/net/Uri;ZZ)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2348
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2349
    iget-object v1, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->LK()[Ljava/lang/String;

    move-result-object v3

    .line 2350
    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 2351
    invoke-virtual {v2, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2353
    const/4 v0, 0x1

    .line 2377
    :cond_3
    :goto_1
    return v0

    .line 2350
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2360
    :cond_5
    invoke-static {p1, p2}, Ldmq;->e(Landroid/net/Uri;Landroid/net/Uri;)Ldmq;

    move-result-object v1

    .line 2361
    sget-object v2, Ldmq;->bCt:Ldmq;

    if-eq v1, v2, :cond_3

    .line 2365
    invoke-virtual {v1}, Ldmq;->adw()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ldmq;->adv()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ldmq;->adx()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2369
    invoke-virtual {v1}, Ldmq;->ady()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1}, Ldmq;->adz()Ljava/util/Set;

    move-result-object v1

    invoke-static {v2, v1}, Liqs;->a(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v1

    .line 2370
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2374
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LJ()Ljava/util/Set;

    move-result-object v0

    .line 2375
    invoke-static {v1, v0}, Liqs;->b(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v0

    invoke-virtual {v0}, Liqx;->isEmpty()Z

    move-result v0

    goto :goto_1
.end method

.method public final d(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 5

    .prologue
    .line 2432
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqS()Lijm;

    move-result-object v2

    .line 2433
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqS()Lijm;

    move-result-object v3

    .line 2434
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LI()Ljava/util/Set;

    move-result-object v0

    .line 2435
    if-eq v2, v3, :cond_1

    .line 2436
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2437
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2439
    const/4 v0, 0x0

    .line 2443
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final f(Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 2311
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->JY()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2322
    :cond_0
    :goto_0
    return-void

    .line 2315
    :cond_1
    iget-object v0, p0, Lcpn;->mStaticContentCache:Lcpq;

    invoke-virtual {v0}, Lcpq;->PI()Ljava/lang/String;

    move-result-object v0

    .line 2316
    iget-object v1, p0, Lcpn;->mStaticContentCache:Lcpq;

    invoke-virtual {v1}, Lcpq;->PE()Leeb;

    move-result-object v1

    .line 2317
    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2318
    const-string v2, "X-Gsa-Preloaded-Page-Context"

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319
    const-string v0, "X-Gsa-Preloaded-Page-Content-Type"

    invoke-static {v1}, Leeb;->c(Leeb;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final hE(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 699
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-direct {p0, v3, v3}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 701
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final hF(Ljava/lang/String;)Lcpo;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 1120
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcpn;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZZ)Lcpo;

    move-result-object v0

    return-object v0
.end method

.method public final hG(Ljava/lang/String;)Lcpo;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1646
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%1$s://%2$s/velog/action"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-direct {p0, v4, v4}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1648
    new-instance v1, Lcpo;

    invoke-direct {v1, p0, v0}, Lcpo;-><init>(Lcpn;Ljava/lang/String;)V

    .line 1651
    invoke-virtual {v1, p1}, Lcpo;->hQ(Ljava/lang/String;)Lcpo;

    .line 1654
    return-object v1
.end method

.method public final hH(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1688
    if-nez p1, :cond_1

    .line 1690
    :cond_0
    :goto_0
    return v0

    .line 1689
    :cond_1
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 1690
    const-string v3, "www.google.com"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v0, v1}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hJ(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2081
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcpn;->c(Landroid/net/Uri;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hK(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2090
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcpn;->Rt()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v2}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hL(Ljava/lang/String;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 5

    .prologue
    .line 2181
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcpn;->cB(Z)Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 2182
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2183
    invoke-virtual {p0}, Lcpn;->Rn()Ljava/lang/String;

    move-result-object v2

    .line 2184
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2185
    const-string v2, "hl"

    invoke-virtual {p0}, Lcpn;->Rn()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2187
    :cond_0
    iget-object v2, p0, Lcpn;->mVoiceSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    .line 2188
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2189
    const-string v3, "spknlang"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2191
    :cond_1
    invoke-virtual {p0}, Lcpn;->RG()Ljava/lang/String;

    move-result-object v2

    .line 2192
    if-eqz v2, :cond_2

    .line 2193
    const-string v3, "agsad"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2195
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "/ajax/searchapp"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4, v1}, Lcpn;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v1

    .line 2198
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2199
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2200
    if-eqz p1, :cond_3

    .line 2201
    const-string v0, "ETag"

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2203
    :cond_3
    new-instance v0, Lcom/google/android/search/shared/api/UriRequest;

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    return-object v0
.end method

.method public final hM(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2466
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcpn;->b(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final j(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 870
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    invoke-virtual {v0}, Lehm;->asL()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 873
    :goto_0
    if-eqz v0, :cond_1

    .line 874
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcpn;->c(Lcom/google/android/shared/search/Query;Ljava/lang/String;)Lcpo;

    move-result-object v0

    .line 879
    :goto_1
    invoke-virtual {v0}, Lcpo;->RK()Lcpo;

    .line 880
    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 870
    goto :goto_0

    .line 876
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, v1, v1}, Lcpn;->a(Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZ)Lcpo;

    move-result-object v0

    goto :goto_1
.end method

.method public final k(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 884
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    invoke-virtual {v0}, Lehm;->asL()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 886
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcpn;->c(Lcom/google/android/shared/search/Query;Ljava/lang/String;)Lcpo;

    move-result-object v0

    .line 890
    :goto_1
    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0

    .line 884
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 886
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, v1, v1}, Lcpn;->a(Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZ)Lcpo;

    move-result-object v0

    goto :goto_1
.end method

.method public final l(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 900
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, v1, v1}, Lcpn;->a(Lcom/google/android/shared/search/Query;Ljava/lang/String;ZZ)Lcpo;

    move-result-object v1

    .line 902
    invoke-virtual {p0}, Lcpn;->RC()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 903
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcpo;->hS(Ljava/lang/String;)Lcpo;

    .line 904
    iget-object v0, p0, Lcpn;->bfo:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcpo;->ih(Ljava/lang/String;)Lcpo;

    .line 905
    invoke-virtual {v1}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0
.end method

.method public final m(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 948
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 949
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Jb()Ljava/lang/String;

    move-result-object v1

    .line 950
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v3

    .line 951
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->IT()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p1

    move v7, v6

    move v8, v6

    .line 952
    invoke-direct/range {v0 .. v8}, Lcpn;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Lcpo;

    move-result-object v0

    .line 955
    invoke-direct {p0, v0, p1, v3, v5}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    invoke-direct {p0, v0}, Lcpn;->b(Lcpo;)V

    .line 957
    invoke-direct {p0}, Lcpn;->RA()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 958
    invoke-virtual {v0}, Lcpo;->Sb()Lcpo;

    .line 961
    :cond_1
    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->JS()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 962
    invoke-direct {p0, v0}, Lcpn;->a(Lcpo;)V

    .line 965
    :cond_2
    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v6

    .line 948
    goto :goto_0
.end method

.method public final n(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 985
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 986
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    .line 987
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 989
    iget-object v2, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->Jb()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v1}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcpn;->R(Ljava/lang/String;Ljava/lang/String;)Lcpo;

    move-result-object v1

    .line 994
    iget-object v2, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->IU()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcpo;->ig(Ljava/lang/String;)Lcpo;

    move-result-object v2

    iget-object v3, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->LU()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcpo;->hO(Ljava/lang/String;)Lcpo;

    move-result-object v2

    invoke-virtual {v2}, Lcpo;->RW()Lcpo;

    move-result-object v2

    invoke-virtual {v2}, Lcpo;->RO()Lcpo;

    move-result-object v2

    iget-object v3, p0, Lcpn;->bfn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcpo;->ii(Ljava/lang/String;)Lcpo;

    .line 1000
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1001
    invoke-virtual {v1, v0}, Lcpo;->hZ(Ljava/lang/String;)Lcpo;

    .line 1004
    :cond_1
    invoke-direct {p0, v1}, Lcpn;->c(Lcpo;)V

    .line 1005
    invoke-direct {p0, v1}, Lcpn;->d(Lcpo;)V

    .line 1008
    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, v0, v2}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    invoke-virtual {v1}, Lcpo;->RK()Lcpo;

    .line 1011
    invoke-virtual {v1}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    .line 985
    goto :goto_0

    :cond_3
    move v2, v1

    .line 987
    goto :goto_1
.end method

.method public final o(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1020
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v6

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1021
    invoke-virtual {p0, p1}, Lcpn;->p(Lcom/google/android/shared/search/Query;)Lckh;

    move-result-object v0

    invoke-virtual {v0}, Lckh;->OL()Ljava/lang/String;

    move-result-object v1

    .line 1022
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v3

    .line 1023
    invoke-virtual {p0}, Lcpn;->Ry()Ljava/lang/String;

    move-result-object v4

    .line 1024
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->IT()Ljava/lang/String;

    move-result-object v9

    .line 1026
    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcpn;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Lcpo;

    move-result-object v0

    .line 1029
    const-string v1, "i"

    invoke-virtual {v0, v1}, Lcpo;->hY(Ljava/lang/String;)Lcpo;

    .line 1030
    invoke-virtual {v0}, Lcpo;->RN()Lcpo;

    .line 1031
    iget-object v1, p0, Lcpn;->bfq:Lgnu;

    invoke-virtual {v0, v1}, Lcpo;->e(Lgnu;)V

    .line 1032
    invoke-direct {p0, v0, p1, v3, v9}, Lcpn;->a(Lcpo;Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    invoke-direct {p0, v0}, Lcpn;->b(Lcpo;)V

    .line 1034
    invoke-direct {p0}, Lcpn;->RA()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1035
    invoke-virtual {v0}, Lcpo;->Sb()Lcpo;

    .line 1038
    :cond_1
    iget-object v1, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->JS()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1039
    invoke-direct {p0, v0}, Lcpn;->a(Lcpo;)V

    .line 1042
    :cond_2
    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v7

    .line 1020
    goto :goto_0
.end method

.method public final o(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1776
    invoke-direct {p0, p1, v0, v0}, Lcpn;->a(Landroid/net/Uri;ZZ)Z

    move-result v0

    return v0
.end method

.method public final onStopped()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcpn;->mDiscourseContextHelper:Lcla;

    iget-object v0, v0, Lcla;->mRecentContextApiClient:Lclh;

    invoke-virtual {v0}, Lclh;->disconnect()V

    .line 369
    return-void
.end method

.method public final p(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1963
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcpn;->hI(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final p(Lcom/google/android/shared/search/Query;)Lckh;
    .locals 2

    .prologue
    .line 1125
    iget-object v0, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EB()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1126
    iget-object v0, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcfu;->gi(Ljava/lang/String;)Lcfy;

    move-result-object v0

    .line 1127
    instance-of v1, v0, Lckh;

    if-eqz v1, :cond_0

    .line 1128
    check-cast v0, Lckh;

    .line 1131
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcpn;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EA()Lckh;

    move-result-object v0

    goto :goto_0
.end method

.method public final q(Landroid/net/Uri;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1989
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 1990
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 1995
    const-string v4, "https"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v4}, Lcke;->NH()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2005
    :cond_0
    :goto_0
    return v0

    .line 2000
    :cond_1
    const-string v2, "www.google.com"

    invoke-static {v2}, Lcpn;->hD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcpn;->T(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v0, v1}, Lcpn;->i(ZZ)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcpn;->hD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcpn;->T(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 2005
    goto :goto_0
.end method

.method public final q(Lcom/google/android/shared/search/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2397
    iget-object v0, p0, Lcpn;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->HX()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r(Landroid/net/Uri;)Lcpo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2141
    if-eqz p1, :cond_0

    invoke-direct {p0, p1, v0, v0}, Lcpn;->a(Landroid/net/Uri;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2142
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Ly()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2143
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcjs;->gL(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2144
    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "Not handling JS-redirected ad click link"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2159
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2146
    :cond_1
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lz()Ljava/util/List;

    move-result-object v1

    .line 2147
    invoke-static {p1}, Lesp;->aD(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2148
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 2149
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2150
    goto :goto_1

    .line 2155
    :cond_2
    new-instance v0, Lcpo;

    invoke-direct {v0, p0, v2}, Lcpo;-><init>(Lcpn;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final t(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 2598
    iget-object v0, p0, Lcpn;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LE()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
