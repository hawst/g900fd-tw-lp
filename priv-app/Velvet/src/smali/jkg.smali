.class public final Ljkg;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAv:I

.field private dAw:I

.field private dAx:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 261
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 262
    iput v0, p0, Ljkg;->aez:I

    iput v0, p0, Ljkg;->dAv:I

    iput v0, p0, Ljkg;->dAw:I

    iput v0, p0, Ljkg;->dAx:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljkg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkg;->eCz:I

    .line 263
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkg;->dAv:I

    iget v0, p0, Ljkg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkg;->dAw:I

    iget v0, p0, Ljkg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkg;->dAx:I

    iget v0, p0, Ljkg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 278
    iget v0, p0, Ljkg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 279
    const/4 v0, 0x1

    iget v1, p0, Ljkg;->dAv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 281
    :cond_0
    iget v0, p0, Ljkg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 282
    const/4 v0, 0x2

    iget v1, p0, Ljkg;->dAw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 284
    :cond_1
    iget v0, p0, Ljkg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 285
    const/4 v0, 0x3

    iget v1, p0, Ljkg;->dAx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 287
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 288
    return-void
.end method

.method public final boi()Z
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Ljkg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boj()Z
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Ljkg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bok()Z
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Ljkg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDay()I
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Ljkg;->dAx:I

    return v0
.end method

.method public final getMonth()I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Ljkg;->dAw:I

    return v0
.end method

.method public final getYear()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Ljkg;->dAv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 292
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 293
    iget v1, p0, Ljkg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 294
    const/4 v1, 0x1

    iget v2, p0, Ljkg;->dAv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_0
    iget v1, p0, Ljkg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 298
    const/4 v1, 0x2

    iget v2, p0, Ljkg;->dAw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    :cond_1
    iget v1, p0, Ljkg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 302
    const/4 v1, 0x3

    iget v2, p0, Ljkg;->dAx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    :cond_2
    return v0
.end method

.method public final qq(I)Ljkg;
    .locals 1

    .prologue
    .line 210
    iput p1, p0, Ljkg;->dAv:I

    .line 211
    iget v0, p0, Ljkg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkg;->aez:I

    .line 212
    return-object p0
.end method

.method public final qr(I)Ljkg;
    .locals 1

    .prologue
    .line 229
    iput p1, p0, Ljkg;->dAw:I

    .line 230
    iget v0, p0, Ljkg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkg;->aez:I

    .line 231
    return-object p0
.end method

.method public final qs(I)Ljkg;
    .locals 1

    .prologue
    .line 248
    iput p1, p0, Ljkg;->dAx:I

    .line 249
    iget v0, p0, Ljkg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkg;->aez:I

    .line 250
    return-object p0
.end method
