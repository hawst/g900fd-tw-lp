.class public final Ljmb;
.super Ljsl;
.source "PG"


# static fields
.field public static final eti:Ljsm;


# instance fields
.field private aez:I

.field private dzc:I

.field private etj:[I

.field private etk:Z

.field public etl:[Ljava/lang/String;

.field private etm:I

.field private etn:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15864
    const/16 v0, 0xb

    const-class v1, Ljmb;

    const v2, 0x1dbef07a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmb;->eti:Ljsm;

    .line 15880
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15969
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15970
    iput v1, p0, Ljmb;->aez:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljmb;->etj:[I

    iput-boolean v1, p0, Ljmb;->etk:Z

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    iput v1, p0, Ljmb;->dzc:I

    iput v1, p0, Ljmb;->etm:I

    iput v1, p0, Ljmb;->etn:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljmb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmb;->eCz:I

    .line 15971
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 15857
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljmb;->etj:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljmb;->etj:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljmb;->etj:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljmb;->etj:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljmb;->etj:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Ljmb;->etj:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljmb;->etj:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljmb;->etj:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljmb;->etj:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmb;->etk:Z

    iget v0, p0, Ljmb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmb;->aez:I

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljmb;->etl:[Ljava/lang/String;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_7

    :cond_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    iput-object v1, p0, Ljmb;->etl:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmb;->dzc:I

    iget v0, p0, Ljmb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmb;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Ljmb;->etm:I

    iget v0, p0, Ljmb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmb;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmb;->etn:I

    iget v0, p0, Ljmb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljmb;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15989
    iget-object v0, p0, Ljmb;->etj:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljmb;->etj:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 15990
    :goto_0
    iget-object v2, p0, Ljmb;->etj:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 15991
    const/4 v2, 0x1

    iget-object v3, p0, Ljmb;->etj:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 15990
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15994
    :cond_0
    iget v0, p0, Ljmb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 15995
    const/4 v0, 0x2

    iget-boolean v2, p0, Ljmb;->etk:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 15997
    :cond_1
    iget-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 15998
    :goto_1
    iget-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 15999
    iget-object v0, p0, Ljmb;->etl:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 16000
    if-eqz v0, :cond_2

    .line 16001
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 15998
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 16005
    :cond_3
    iget v0, p0, Ljmb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 16006
    const/4 v0, 0x4

    iget v1, p0, Ljmb;->dzc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 16008
    :cond_4
    iget v0, p0, Ljmb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 16009
    const/4 v0, 0x5

    iget v1, p0, Ljmb;->etm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 16011
    :cond_5
    iget v0, p0, Ljmb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 16012
    const/4 v0, 0x6

    iget v1, p0, Ljmb;->etn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 16014
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 16015
    return-void
.end method

.method public final bpA()I
    .locals 1

    .prologue
    .line 15953
    iget v0, p0, Ljmb;->etn:I

    return v0
.end method

.method public final bpB()Z
    .locals 1

    .prologue
    .line 15961
    iget v0, p0, Ljmb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpx()Z
    .locals 1

    .prologue
    .line 15893
    iget-boolean v0, p0, Ljmb;->etk:Z

    return v0
.end method

.method public final bpy()I
    .locals 1

    .prologue
    .line 15934
    iget v0, p0, Ljmb;->etm:I

    return v0
.end method

.method public final bpz()Z
    .locals 1

    .prologue
    .line 15942
    iget v0, p0, Ljmb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 16019
    invoke-super {p0}, Ljsl;->lF()I

    move-result v3

    .line 16020
    iget-object v0, p0, Ljmb;->etj:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljmb;->etj:[I

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    move v2, v1

    .line 16022
    :goto_0
    iget-object v4, p0, Ljmb;->etj:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 16023
    iget-object v4, p0, Ljmb;->etj:[I

    aget v4, v4, v0

    .line 16024
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 16022
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 16027
    :cond_0
    add-int v0, v3, v2

    .line 16028
    iget-object v2, p0, Ljmb;->etj:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 16030
    :goto_1
    iget v2, p0, Ljmb;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 16031
    const/4 v2, 0x2

    iget-boolean v3, p0, Ljmb;->etk:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 16034
    :cond_1
    iget-object v2, p0, Ljmb;->etl:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljmb;->etl:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 16037
    :goto_2
    iget-object v4, p0, Ljmb;->etl:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 16038
    iget-object v4, p0, Ljmb;->etl:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 16039
    if-eqz v4, :cond_2

    .line 16040
    add-int/lit8 v3, v3, 0x1

    .line 16041
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16037
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 16045
    :cond_3
    add-int/2addr v0, v2

    .line 16046
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 16048
    :cond_4
    iget v1, p0, Ljmb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 16049
    const/4 v1, 0x4

    iget v2, p0, Ljmb;->dzc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16052
    :cond_5
    iget v1, p0, Ljmb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 16053
    const/4 v1, 0x5

    iget v2, p0, Ljmb;->etm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16056
    :cond_6
    iget v1, p0, Ljmb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 16057
    const/4 v1, 0x6

    iget v2, p0, Ljmb;->etn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16060
    :cond_7
    return v0

    :cond_8
    move v0, v3

    goto :goto_1
.end method

.method public final qL(I)Ljmb;
    .locals 1

    .prologue
    .line 15956
    iput p1, p0, Ljmb;->etn:I

    .line 15957
    iget v0, p0, Ljmb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljmb;->aez:I

    .line 15958
    return-object p0
.end method
