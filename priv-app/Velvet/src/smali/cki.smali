.class public Lcki;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aZF:Ljava/lang/String;

.field private final aZG:J

.field private final aZH:[B

.field private final aZI:Ljava/lang/String;

.field private final aZJ:Ljava/lang/String;

.field private final mWebPage:Ldyo;


# direct methods
.method public constructor <init>(Lckp;)V
    .locals 6

    .prologue
    .line 47
    invoke-virtual {p1}, Lckp;->OT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lckp;->getCreationTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lckp;->ON()[B

    move-result-object v4

    invoke-virtual {p1}, Lckp;->getContentType()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcki;-><init>(Ljava/lang/String;J[BLjava/lang/String;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J[BLjava/lang/String;)V
    .locals 6

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcki;->aZF:Ljava/lang/String;

    .line 38
    iput-wide p2, p0, Lcki;->aZG:J

    .line 39
    iput-object p4, p0, Lcki;->aZH:[B

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcki;->aZI:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lcki;->aZJ:Ljava/lang/String;

    .line 43
    iget-object v1, p0, Lcki;->aZH:[B

    iget-object v2, p0, Lcki;->aZF:Ljava/lang/String;

    iget-object v0, p0, Lcki;->aZJ:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Leeb;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-direct {v0, v3, v4}, Leeb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/android/search/shared/api/UriRequest;

    const/4 v4, 0x0

    invoke-direct {v2, v1, v4}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    new-instance v1, Ldyo;

    invoke-direct {v1, v2, v0, v3}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    iput-object v1, p0, Lcki;->mWebPage:Ldyo;

    .line 44
    return-void

    .line 43
    :cond_0
    invoke-static {v0}, Leeb;->kM(Ljava/lang/String;)Leeb;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final OM()Lckp;
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lckp;

    invoke-direct {v0}, Lckp;-><init>()V

    .line 67
    iget-object v1, p0, Lcki;->aZF:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lckp;->hi(Ljava/lang/String;)Lckp;

    .line 68
    iget-object v1, p0, Lcki;->aZH:[B

    invoke-virtual {v0, v1}, Lckp;->u([B)Lckp;

    .line 69
    iget-wide v2, p0, Lcki;->aZG:J

    invoke-virtual {v0, v2, v3}, Lckp;->Y(J)Lckp;

    .line 70
    iget-object v1, p0, Lcki;->aZJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lckp;->hj(Ljava/lang/String;)Lckp;

    .line 71
    return-object v0
.end method

.method public final ON()[B
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcki;->aZH:[B

    return-object v0
.end method

.method public final OO()Ldyo;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcki;->mWebPage:Ldyo;

    return-object v0
.end method

.method public final OP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcki;->aZI:Ljava/lang/String;

    return-object v0
.end method

.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcki;->aZJ:Ljava/lang/String;

    return-object v0
.end method

.method public final getCreationTime()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcki;->aZG:J

    return-wide v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcki;->aZF:Ljava/lang/String;

    return-object v0
.end method
