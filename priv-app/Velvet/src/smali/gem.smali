.class public final Lgem;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private cHA:Ljava/util/List;

.field private cHB:Z

.field private cHC:Z

.field private cHD:Lger;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cHy:Lgfr;

.field private final cHz:Lhhc;

.field private final mAudioRouter:Lhhu;

.field private mAudioSource:Lgex;

.field final mContext:Landroid/content/Context;

.field private final mSettings:Lgdo;

.field private final mSpeechLevelSource:Lequ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgdo;Lequ;Lgfr;Lhhu;Lhhc;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lgem;->cHD:Lger;

    .line 82
    iput-object p1, p0, Lgem;->mContext:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lgem;->mSettings:Lgdo;

    .line 84
    iput-object p4, p0, Lgem;->cHy:Lgfr;

    .line 85
    iput-object p3, p0, Lgem;->mSpeechLevelSource:Lequ;

    .line 86
    iput-object p5, p0, Lgem;->mAudioRouter:Lhhu;

    .line 87
    iput-object p6, p0, Lgem;->cHz:Lhhc;

    .line 88
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lgmx;)Lger;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 141
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgem;->cHD:Lger;

    if-eqz v0, :cond_1

    iget-object v5, p0, Lgem;->cHD:Lger;

    .line 144
    :goto_0
    new-instance v0, Lgex;

    invoke-virtual {p1}, Lgmx;->getSamplingRate()I

    move-result v1

    invoke-virtual {p1}, Lgmx;->getSamplingRate()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    div-int/lit16 v2, v2, 0x3e8

    mul-int/lit8 v2, v2, 0x14

    const/16 v3, 0x1f4

    const/16 v4, 0x3e8

    invoke-virtual {p1}, Lgmx;->aHu()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v6, p0, Lgem;->mSpeechLevelSource:Lequ;

    :cond_0
    invoke-direct/range {v0 .. v6}, Lgex;-><init>(IIIILger;Lequ;)V

    iput-object v0, p0, Lgem;->mAudioSource:Lgex;

    .line 145
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 141
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lgmx;->aHz()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lgmx;->aHz()Landroid/net/Uri;

    move-result-object v0

    new-instance v5, Lgen;

    invoke-direct {v5, p0, v0}, Lgen;-><init>(Lgem;Landroid/net/Uri;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-virtual {p1}, Lgmx;->aHv()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgem;->mContext:Landroid/content/Context;

    invoke-static {v0}, Leoh;->as(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v5, v2

    :goto_1
    new-instance v0, Lgfo;

    invoke-virtual {p1}, Lgmx;->getSamplingRate()I

    move-result v1

    invoke-virtual {p1}, Lgmx;->aHs()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lgem;->cHA:Ljava/util/List;

    if-nez v4, :cond_3

    iget-object v4, p0, Lgem;->mSettings:Lgdo;

    invoke-interface {v4}, Lgdo;->aER()Ljze;

    move-result-object v4

    iget-object v4, v4, Ljze;->eNr:Ljzv;

    invoke-static {v4}, Lgfd;->a(Ljzv;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lgem;->cHA:Ljava/util/List;

    :cond_3
    iget-object v4, p0, Lgem;->cHA:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_5

    :goto_2
    invoke-virtual {p1}, Lgmx;->aHt()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lgem;->cHy:Lgfr;

    :goto_3
    iget-object v4, p0, Lgem;->mAudioRouter:Lhhu;

    invoke-direct/range {v0 .. v5}, Lgfo;-><init>(IZLgfr;Lhhu;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v5, v0

    goto :goto_0

    :cond_4
    move v5, v3

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    move-object v3, v6

    goto :goto_3
.end method

.method public final declared-synchronized a(Lger;)V
    .locals 1
    .param p1    # Lger;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lgem;->cHD:Lger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZLglw;)V
    .locals 2
    .param p2    # Lglw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgem;->cHC:Z

    if-nez v0, :cond_1

    .line 164
    iget-object v0, p0, Lgem;->mSpeechLevelSource:Lequ;

    invoke-virtual {v0}, Lequ;->reset()V

    .line 166
    iput-boolean p1, p0, Lgem;->cHB:Z

    .line 167
    iget-object v0, p0, Lgem;->mAudioRouter:Lhhu;

    invoke-interface {v0, p1}, Lhhu;->gr(Z)V

    .line 169
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;

    invoke-virtual {v0, p2}, Lgex;->a(Lglw;)V

    .line 173
    :cond_0
    const/16 v0, 0x4c

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lgem;->mAudioRouter:Lhhu;

    invoke-interface {v1}, Lhhu;->aPr()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mD(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lgem;->cHz:Lhhc;

    invoke-virtual {v1}, Lhhc;->getNetworkType()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mG(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgem;->cHC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    :cond_1
    monitor-exit p0

    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized shutdown()V
    .locals 1

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;

    invoke-virtual {v0}, Lgex;->shutdown()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lgem;->mAudioSource:Lgex;

    .line 210
    :cond_0
    invoke-virtual {p0}, Lgem;->stopListening()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    monitor-exit p0

    return-void

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized stopListening()V
    .locals 2

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgem;->cHC:Z

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lgem;->mAudioSource:Lgex;

    invoke-virtual {v0}, Lgex;->stopListening()V

    .line 195
    :cond_0
    iget-object v0, p0, Lgem;->mAudioRouter:Lhhu;

    iget-boolean v1, p0, Lgem;->cHB:Z

    invoke-interface {v0, v1}, Lhhu;->gs(Z)V

    .line 197
    iget-object v0, p0, Lgem;->mSpeechLevelSource:Lequ;

    invoke-virtual {v0}, Lequ;->reset()V

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgem;->cHC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :cond_1
    monitor-exit p0

    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
