.class public final Lgmr;
.super Lgmq;
.source "PG"


# instance fields
.field private final mNetworkInfo:Lgno;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgno;)V
    .locals 1
    .param p1    # Lgno;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 16
    const-string v0, "MobileUserInfoBuilderTask"

    invoke-direct {p0, v0}, Lgmq;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lgmr;->mNetworkInfo:Lgno;

    .line 18
    return-void
.end method


# virtual methods
.method protected final synthetic aHr()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Ljwl;

    invoke-direct {v0}, Ljwl;-><init>()V

    iget-object v1, p0, Lgmr;->mNetworkInfo:Lgno;

    invoke-virtual {v1}, Lgno;->aAl()[I

    move-result-object v1

    sget-object v2, Lgno;->cQq:[I

    if-eq v1, v2, :cond_0

    aget v2, v1, v3

    invoke-virtual {v0, v2}, Ljwl;->sG(I)Ljwl;

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljwl;->sH(I)Ljwl;

    :cond_0
    iget-object v1, p0, Lgmr;->mNetworkInfo:Lgno;

    invoke-virtual {v1}, Lgno;->aAm()[I

    move-result-object v1

    sget-object v2, Lgno;->cQq:[I

    if-eq v1, v2, :cond_1

    aget v2, v1, v3

    invoke-virtual {v0, v2}, Ljwl;->sI(I)Ljwl;

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljwl;->sJ(I)Ljwl;

    :cond_1
    iget-object v1, p0, Lgmr;->mNetworkInfo:Lgno;

    invoke-virtual {v1}, Lgno;->aAn()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    invoke-virtual {v0, v1}, Ljwl;->sK(I)Ljwl;

    :cond_2
    return-object v0
.end method
