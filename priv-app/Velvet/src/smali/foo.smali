.class public Lfoo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfon;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private cot:Lfmq;

.field private final mRemoteClient:Lfml;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lfoo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfoo;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lfml;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lfoo;->mRemoteClient:Lfml;

    .line 23
    return-void
.end method


# virtual methods
.method public final aBL()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    iget-object v1, p0, Lfoo;->mRemoteClient:Lfml;

    invoke-virtual {v1}, Lfml;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    :goto_0
    return v0

    .line 32
    :cond_0
    :try_start_0
    iget-object v1, p0, Lfoo;->mRemoteClient:Lfml;

    invoke-virtual {v1}, Lfml;->aBa()Lfor;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/os/RemoteException;

    const-string v2, "Not connected"

    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :catch_0
    move-exception v1

    goto :goto_0

    .line 32
    :cond_1
    invoke-interface {v1}, Lfor;->Kz()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0
.end method

.method public final aBM()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lfoo;->mRemoteClient:Lfml;

    invoke-virtual {v1}, Lfml;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    :goto_0
    return v0

    .line 46
    :cond_0
    iget-object v1, p0, Lfoo;->mRemoteClient:Lfml;

    invoke-virtual {v1}, Lfml;->azN()Landroid/os/Bundle;

    move-result-object v1

    .line 47
    const-string v2, "CONFIGURATION_REMINDERS_ENABLED"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public final aBN()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lfoo;->cot:Lfmq;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lfoo;->mRemoteClient:Lfml;

    sget-object v1, Lfoo;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Lfoo;->cot:Lfmq;

    .line 61
    iget-object v0, p0, Lfoo;->cot:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 63
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfoo;->cot:Lfmq;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lfoo;->cot:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lfoo;->cot:Lfmq;

    .line 71
    :cond_0
    return-void
.end method
