.class public final Ljfb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efQ:I

.field private egA:Z

.field private egB:Z

.field private egv:Z

.field private egw:Z

.field private egx:Z

.field private egy:Z

.field private egz:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 10858
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10859
    iput v1, p0, Ljfb;->aez:I

    iput-object v2, p0, Ljfb;->efN:Ljez;

    iput v1, p0, Ljfb;->efQ:I

    iput-boolean v0, p0, Ljfb;->egv:Z

    iput-boolean v0, p0, Ljfb;->egw:Z

    iput-boolean v0, p0, Ljfb;->egx:Z

    iput-boolean v0, p0, Ljfb;->egy:Z

    iput-boolean v0, p0, Ljfb;->egz:Z

    iput-boolean v0, p0, Ljfb;->egA:Z

    iput-boolean v0, p0, Ljfb;->egB:Z

    iput-object v2, p0, Ljfb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfb;->eCz:I

    .line 10860
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 10684
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljfb;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfb;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljfb;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egw:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egx:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egy:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egz:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egv:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfb;->efQ:I

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egA:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljfb;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfb;->egB:Z

    iget v0, p0, Ljfb;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljfb;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 10881
    iget-object v0, p0, Ljfb;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 10882
    const/4 v0, 0x1

    iget-object v1, p0, Ljfb;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 10884
    :cond_0
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 10885
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfb;->egw:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10887
    :cond_1
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 10888
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljfb;->egx:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10890
    :cond_2
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 10891
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfb;->egy:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10893
    :cond_3
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 10894
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljfb;->egz:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10896
    :cond_4
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 10897
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljfb;->egv:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10899
    :cond_5
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    .line 10900
    const/4 v0, 0x7

    iget v1, p0, Ljfb;->efQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10902
    :cond_6
    iget v0, p0, Ljfb;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 10903
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljfb;->egA:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10905
    :cond_7
    iget v0, p0, Ljfb;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_8

    .line 10906
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljfb;->egB:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10908
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10909
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 10913
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10914
    iget-object v1, p0, Ljfb;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 10915
    const/4 v1, 0x1

    iget-object v2, p0, Ljfb;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10918
    :cond_0
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 10919
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfb;->egw:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10922
    :cond_1
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 10923
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljfb;->egx:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10926
    :cond_2
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 10927
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfb;->egy:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10930
    :cond_3
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 10931
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljfb;->egz:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10934
    :cond_4
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 10935
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljfb;->egv:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10938
    :cond_5
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    .line 10939
    const/4 v1, 0x7

    iget v2, p0, Ljfb;->efQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10942
    :cond_6
    iget v1, p0, Ljfb;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 10943
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljfb;->egA:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10946
    :cond_7
    iget v1, p0, Ljfb;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 10947
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljfb;->egB:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10950
    :cond_8
    return v0
.end method
