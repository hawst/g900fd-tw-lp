.class public final Lfbg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lfbg;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 30
    return-void
.end method

.method private c(Lizq;)V
    .locals 8

    .prologue
    .line 42
    iget-object v0, p1, Lizq;->dUW:[Lizq;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 43
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 44
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 45
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizq;

    .line 46
    iget-object v1, v0, Lizq;->dUX:[Lizj;

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 47
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 48
    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 49
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizj;

    .line 50
    invoke-virtual {v1}, Lizj;->getType()I

    move-result v6

    const/16 v7, 0x43

    if-ne v6, v7, :cond_1

    .line 51
    iget-object v6, v1, Lizj;->dTo:Ljdi;

    if-eqz v6, :cond_2

    iget-object v6, v1, Lizj;->dTo:Ljdi;

    iget-object v6, v6, Ljdi;->eca:Ljdj;

    if-nez v6, :cond_3

    .line 56
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 62
    :cond_3
    iget-object v1, v1, Lizj;->dTo:Ljdi;

    iget-object v1, v1, Ljdi;->eca:Ljdj;

    iget-object v6, p0, Lfbg;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v6}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBQ()Ljava/util/Collection;

    move-result-object v6

    invoke-static {v1, v6}, Lfqe;->a(Ljdj;Ljava/util/Collection;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v1

    .line 65
    if-nez v1, :cond_4

    .line 66
    const-string v1, "EntryTreeIcebreakerPruner"

    const-string v6, "Pruning missing icebreaker"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 67
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 68
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 69
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 73
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    iget-object v5, v0, Lizq;->dUX:[Lizj;

    array-length v5, v5

    if-eq v1, v5, :cond_6

    .line 74
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lizj;

    invoke-interface {v4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lizj;

    iput-object v1, v0, Lizq;->dUX:[Lizj;

    .line 78
    :cond_6
    invoke-direct {p0, v0}, Lfbg;->c(Lizq;)V

    .line 81
    iget-object v1, v0, Lizq;->dUX:[Lizj;

    array-length v1, v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lizq;->dUW:[Lizq;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 82
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 85
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, Lizq;->dUW:[Lizq;

    array-length v1, v1

    if-eq v0, v1, :cond_8

    .line 86
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizq;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizq;

    iput-object v0, p1, Lizq;->dUW:[Lizq;

    .line 88
    :cond_8
    return-void
.end method


# virtual methods
.method public final f(Lizo;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    invoke-direct {p0, v0}, Lfbg;->c(Lizq;)V

    .line 39
    :cond_0
    return-void
.end method
