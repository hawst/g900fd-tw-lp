.class abstract Lcr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

.field private static final dQ:Ljava/util/concurrent/ThreadFactory;

.field private static final dR:Ljava/util/concurrent/BlockingQueue;

.field private static final dS:Lcx;

.field private static volatile dT:Ljava/util/concurrent/Executor;


# instance fields
.field private final dU:Lcz;

.field private final dV:Ljava/util/concurrent/FutureTask;

.field private volatile dW:Lcy;

.field private final dX:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 54
    new-instance v0, Lcs;

    invoke-direct {v0}, Lcs;-><init>()V

    sput-object v0, Lcr;->dQ:Ljava/util/concurrent/ThreadFactory;

    .line 62
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Lcr;->dR:Ljava/util/concurrent/BlockingQueue;

    .line 68
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x5

    const/16 v3, 0x80

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Lcr;->dR:Ljava/util/concurrent/BlockingQueue;

    sget-object v8, Lcr;->dQ:Ljava/util/concurrent/ThreadFactory;

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, Lcr;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    .line 75
    new-instance v0, Lcx;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcx;-><init>(B)V

    sput-object v0, Lcr;->dS:Lcx;

    .line 77
    sget-object v0, Lcr;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    sput-object v0, Lcr;->dT:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    sget-object v0, Lcy;->ed:Lcy;

    iput-object v0, p0, Lcr;->dW:Lcy;

    .line 83
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcr;->dX:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 118
    new-instance v0, Lct;

    invoke-direct {v0, p0}, Lct;-><init>(Lcr;)V

    iput-object v0, p0, Lcr;->dU:Lcz;

    .line 127
    new-instance v0, Lcu;

    iget-object v1, p0, Lcr;->dU:Lcz;

    invoke-direct {v0, p0, v1}, Lcu;-><init>(Lcr;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lcr;->dV:Ljava/util/concurrent/FutureTask;

    .line 147
    return-void
.end method

.method static synthetic a(Lcr;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcr;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcr;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcr;->dX:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method protected static varargs aj()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 157
    sget-object v0, Lcr;->dS:Lcx;

    new-instance v1, Lcw;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v1, p0, v2}, Lcw;-><init>(Lcr;[Ljava/lang/Object;)V

    invoke-virtual {v0, v4, v1}, Lcx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 160
    return-object p1
.end method

.method static synthetic b(Lcr;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcr;->dX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcr;->b(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method static synthetic c(Lcr;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcr;->dV:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcr;->onCancelled()V

    :goto_0
    sget-object v0, Lcy;->ef:Lcy;

    iput-object v0, p0, Lcr;->dW:Lcy;

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcr;->onPostExecute(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final varargs a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lcr;
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcr;->dW:Lcy;

    sget-object v1, Lcy;->ed:Lcy;

    if-eq v0, v1, :cond_0

    .line 407
    sget-object v0, Lcv;->ea:[I

    iget-object v1, p0, Lcr;->dW:Lcy;

    invoke-virtual {v1}, Lcy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 418
    :cond_0
    sget-object v0, Lcy;->ee:Lcy;

    iput-object v0, p0, Lcr;->dW:Lcy;

    .line 420
    iget-object v0, p0, Lcr;->dU:Lcz;

    const/4 v1, 0x0

    iput-object v1, v0, Lcz;->eh:[Ljava/lang/Object;

    .line 423
    iget-object v0, p0, Lcr;->dV:Ljava/util/concurrent/FutureTask;

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 425
    return-object p0

    .line 409
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 412
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final cancel(Z)Z
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcr;->dV:Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method protected varargs abstract doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 260
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method
