.class public final Lizq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dUV:[Lizq;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public dUW:[Lizq;

.field public dUX:[Lizj;

.field private dUY:[Liwk;

.field public dUZ:Lizj;

.field private dVa:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 34276
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 34277
    iput v1, p0, Lizq;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lizq;->afh:Ljava/lang/String;

    invoke-static {}, Lizq;->bdp()[Lizq;

    move-result-object v0

    iput-object v0, p0, Lizq;->dUW:[Lizq;

    invoke-static {}, Lizj;->bcP()[Lizj;

    move-result-object v0

    iput-object v0, p0, Lizq;->dUX:[Lizj;

    invoke-static {}, Liwk;->aZn()[Liwk;

    move-result-object v0

    iput-object v0, p0, Lizq;->dUY:[Liwk;

    iput-object v2, p0, Lizq;->dUZ:Lizj;

    iput-boolean v1, p0, Lizq;->dVa:Z

    iput-object v2, p0, Lizq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizq;->eCz:I

    .line 34278
    return-void
.end method

.method private static bdp()[Lizq;
    .locals 2

    .prologue
    .line 34210
    sget-object v0, Lizq;->dUV:[Lizq;

    if-nez v0, :cond_1

    .line 34211
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 34213
    :try_start_0
    sget-object v0, Lizq;->dUV:[Lizq;

    if-nez v0, :cond_0

    .line 34214
    const/4 v0, 0x0

    new-array v0, v0, [Lizq;

    sput-object v0, Lizq;->dUV:[Lizq;

    .line 34216
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34218
    :cond_1
    sget-object v0, Lizq;->dUV:[Lizq;

    return-object v0

    .line 34216
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34204
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizq;->afh:Ljava/lang/String;

    iget v0, p0, Lizq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizq;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizq;->dUW:[Lizq;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lizq;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lizq;->dUW:[Lizq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lizq;

    invoke-direct {v3}, Lizq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lizq;->dUW:[Lizq;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lizq;

    invoke-direct {v3}, Lizq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lizq;->dUW:[Lizq;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizq;->dUX:[Lizj;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lizj;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lizq;->dUX:[Lizj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lizq;->dUX:[Lizj;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lizq;->dUX:[Lizj;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizq;->dUY:[Liwk;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Liwk;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lizq;->dUY:[Liwk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lizq;->dUY:[Liwk;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lizq;->dUY:[Liwk;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lizq;->dUZ:Lizj;

    if-nez v0, :cond_a

    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    iput-object v0, p0, Lizq;->dUZ:Lizj;

    :cond_a
    iget-object v0, p0, Lizq;->dUZ:Lizj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lizq;->dVa:Z

    iget v0, p0, Lizq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizq;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34296
    iget v0, p0, Lizq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 34297
    const/4 v0, 0x1

    iget-object v2, p0, Lizq;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 34299
    :cond_0
    iget-object v0, p0, Lizq;->dUW:[Lizq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lizq;->dUW:[Lizq;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 34300
    :goto_0
    iget-object v2, p0, Lizq;->dUW:[Lizq;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 34301
    iget-object v2, p0, Lizq;->dUW:[Lizq;

    aget-object v2, v2, v0

    .line 34302
    if-eqz v2, :cond_1

    .line 34303
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 34300
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34307
    :cond_2
    iget-object v0, p0, Lizq;->dUX:[Lizj;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lizq;->dUX:[Lizj;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 34308
    :goto_1
    iget-object v2, p0, Lizq;->dUX:[Lizj;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 34309
    iget-object v2, p0, Lizq;->dUX:[Lizj;

    aget-object v2, v2, v0

    .line 34310
    if-eqz v2, :cond_3

    .line 34311
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 34308
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34315
    :cond_4
    iget-object v0, p0, Lizq;->dUY:[Liwk;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lizq;->dUY:[Liwk;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 34316
    :goto_2
    iget-object v0, p0, Lizq;->dUY:[Liwk;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 34317
    iget-object v0, p0, Lizq;->dUY:[Liwk;

    aget-object v0, v0, v1

    .line 34318
    if-eqz v0, :cond_5

    .line 34319
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 34316
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 34323
    :cond_6
    iget-object v0, p0, Lizq;->dUZ:Lizj;

    if-eqz v0, :cond_7

    .line 34324
    const/4 v0, 0x5

    iget-object v1, p0, Lizq;->dUZ:Lizj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 34326
    :cond_7
    iget v0, p0, Lizq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 34327
    const/4 v0, 0x6

    iget-boolean v1, p0, Lizq;->dVa:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 34329
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 34330
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 34334
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 34335
    iget v2, p0, Lizq;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 34336
    const/4 v2, 0x1

    iget-object v3, p0, Lizq;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34339
    :cond_0
    iget-object v2, p0, Lizq;->dUW:[Lizq;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lizq;->dUW:[Lizq;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 34340
    :goto_0
    iget-object v3, p0, Lizq;->dUW:[Lizq;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 34341
    iget-object v3, p0, Lizq;->dUW:[Lizq;

    aget-object v3, v3, v0

    .line 34342
    if-eqz v3, :cond_1

    .line 34343
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 34340
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 34348
    :cond_3
    iget-object v2, p0, Lizq;->dUX:[Lizj;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lizq;->dUX:[Lizj;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 34349
    :goto_1
    iget-object v3, p0, Lizq;->dUX:[Lizj;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 34350
    iget-object v3, p0, Lizq;->dUX:[Lizj;

    aget-object v3, v3, v0

    .line 34351
    if-eqz v3, :cond_4

    .line 34352
    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 34349
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 34357
    :cond_6
    iget-object v2, p0, Lizq;->dUY:[Liwk;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lizq;->dUY:[Liwk;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 34358
    :goto_2
    iget-object v2, p0, Lizq;->dUY:[Liwk;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 34359
    iget-object v2, p0, Lizq;->dUY:[Liwk;

    aget-object v2, v2, v1

    .line 34360
    if-eqz v2, :cond_7

    .line 34361
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34358
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 34366
    :cond_8
    iget-object v1, p0, Lizq;->dUZ:Lizj;

    if-eqz v1, :cond_9

    .line 34367
    const/4 v1, 0x5

    iget-object v2, p0, Lizq;->dUZ:Lizj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34370
    :cond_9
    iget v1, p0, Lizq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_a

    .line 34371
    const/4 v1, 0x6

    iget-boolean v2, p0, Lizq;->dVa:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 34374
    :cond_a
    return v0
.end method

.method public final rL(Ljava/lang/String;)Lizq;
    .locals 1

    .prologue
    .line 34229
    if-nez p1, :cond_0

    .line 34230
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34232
    :cond_0
    iput-object p1, p0, Lizq;->afh:Ljava/lang/String;

    .line 34233
    iget v0, p0, Lizq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizq;->aez:I

    .line 34234
    return-object p0
.end method
