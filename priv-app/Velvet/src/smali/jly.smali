.class public final Ljly;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private alA:Ljava/lang/String;

.field private dzG:Ljava/lang/String;

.field private dzJ:Ljava/lang/String;

.field private dzK:Z

.field private eta:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14187
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14188
    iput v1, p0, Ljly;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljly;->dzG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljly;->dzJ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljly;->eta:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljly;->alA:Ljava/lang/String;

    iput-boolean v1, p0, Ljly;->dzK:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljly;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljly;->eCz:I

    .line 14189
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 14061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljly;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljly;->dzG:Ljava/lang/String;

    iget v0, p0, Ljly;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljly;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljly;->dzJ:Ljava/lang/String;

    iget v0, p0, Ljly;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljly;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljly;->eta:Ljava/lang/String;

    iget v0, p0, Ljly;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljly;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljly;->alA:Ljava/lang/String;

    iget v0, p0, Ljly;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljly;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljly;->dzK:Z

    iget v0, p0, Ljly;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljly;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 14206
    iget v0, p0, Ljly;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14207
    const/4 v0, 0x1

    iget-object v1, p0, Ljly;->dzG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14209
    :cond_0
    iget v0, p0, Ljly;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14210
    const/4 v0, 0x2

    iget-object v1, p0, Ljly;->dzJ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14212
    :cond_1
    iget v0, p0, Ljly;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14213
    const/4 v0, 0x3

    iget-object v1, p0, Ljly;->eta:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14215
    :cond_2
    iget v0, p0, Ljly;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14216
    const/4 v0, 0x4

    iget-object v1, p0, Ljly;->alA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14218
    :cond_3
    iget v0, p0, Ljly;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 14219
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljly;->dzK:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14221
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14222
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 14226
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14227
    iget v1, p0, Ljly;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14228
    const/4 v1, 0x1

    iget-object v2, p0, Ljly;->dzG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14231
    :cond_0
    iget v1, p0, Ljly;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14232
    const/4 v1, 0x2

    iget-object v2, p0, Ljly;->dzJ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14235
    :cond_1
    iget v1, p0, Ljly;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14236
    const/4 v1, 0x3

    iget-object v2, p0, Ljly;->eta:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14239
    :cond_2
    iget v1, p0, Ljly;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14240
    const/4 v1, 0x4

    iget-object v2, p0, Ljly;->alA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14243
    :cond_3
    iget v1, p0, Ljly;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 14244
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljly;->dzK:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14247
    :cond_4
    return v0
.end method
