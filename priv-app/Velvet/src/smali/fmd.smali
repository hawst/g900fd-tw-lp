.class public final Lfmd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public Kl:Z

.field private Kt:Z

.field private final asz:Lfmq;

.field private final coZ:Lfjs;

.field private final cvF:Lfmg;

.field private cvG:Z

.field cvH:Z

.field public final cvI:Lflw;

.field public final cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

.field public final cvK:Lfnx;

.field cvL:Z

.field final cvM:Ljava/lang/String;

.field private cvN:Landroid/content/Intent;

.field final mActivity:Landroid/app/Activity;

.field private final mAppContext:Landroid/content/Context;

.field final mNowRemoteClient:Lfml;

.field final mRefreshManager:Lfmv;

.field private final mUndoDismissManager:Lfnn;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfnx;Lfml;Lflw;Lfmv;Lfnn;Lfjs;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Lfmg;

    invoke-direct {v0, p0}, Lfmg;-><init>(Lfmd;)V

    iput-object v0, p0, Lfmd;->cvF:Lfmg;

    .line 98
    iput-boolean v1, p0, Lfmd;->cvG:Z

    .line 103
    iput-boolean v1, p0, Lfmd;->cvH:Z

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfmd;->Kl:Z

    .line 108
    iput-boolean v1, p0, Lfmd;->Kt:Z

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lfmd;->cvN:Landroid/content/Intent;

    .line 149
    iput-object p1, p0, Lfmd;->mAppContext:Landroid/content/Context;

    .line 150
    iput-object p2, p0, Lfmd;->mActivity:Landroid/app/Activity;

    .line 151
    iput-object p3, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    .line 152
    iput-object p4, p0, Lfmd;->cvK:Lfnx;

    .line 153
    iput-object p5, p0, Lfmd;->mNowRemoteClient:Lfml;

    .line 154
    iput-object p6, p0, Lfmd;->cvI:Lflw;

    .line 155
    iput-object p7, p0, Lfmd;->mRefreshManager:Lfmv;

    .line 156
    iput-object p8, p0, Lfmd;->mUndoDismissManager:Lfnn;

    .line 157
    iput-object p9, p0, Lfmd;->coZ:Lfjs;

    .line 158
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    const-string v1, "NowOverlay"

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Lfmd;->asz:Lfmq;

    .line 159
    iput-object p10, p0, Lfmd;->cvM:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    new-instance v1, Lfme;

    iget-object v2, p0, Lfmd;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, p0, v2, p3}, Lfme;-><init>(Lfmd;Landroid/app/Activity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lfnx;->a(Lfoh;)V

    .line 186
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;Lfml;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfnx;Landroid/view/View;Lflw;Lerk;Leoj;Ligi;Lfzw;ZZLigi;Leko;Ljava/lang/String;)Lfmd;
    .locals 25
    .param p13    # Leko;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 558
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 559
    new-instance v5, Lere;

    invoke-direct {v5, v4}, Lere;-><init>(Landroid/content/Context;)V

    .line 561
    new-instance v3, Lfns;

    new-instance v6, Lfmi;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lfmi;-><init>(Lfml;)V

    move-object/from16 v7, p6

    move/from16 v8, p11

    invoke-direct/range {v3 .. v8}, Lfns;-><init>(Landroid/content/Context;Lemp;Lfjv;Lern;Z)V

    .line 568
    new-instance v8, Lgah;

    move-object/from16 v0, p9

    invoke-direct {v8, v4, v0}, Lgah;-><init>(Landroid/content/Context;Lfzw;)V

    .line 569
    new-instance v15, Lfyk;

    new-instance v6, Lgbr;

    invoke-direct {v6, v5}, Lgbr;-><init>(Lemp;)V

    invoke-direct {v15, v5, v6}, Lfyk;-><init>(Lemp;Lgbr;)V

    .line 570
    new-instance v6, Lfkr;

    const-string v7, "wifi"

    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object v7, v5

    move-object/from16 v10, p9

    move-object/from16 v11, p6

    move/from16 v14, p10

    invoke-direct/range {v6 .. v15}, Lfkr;-><init>(Lemp;Lgah;Landroid/net/wifi/WifiManager;Lfzw;Lerk;Landroid/content/pm/PackageManager;Landroid/content/ContentResolver;ZLfyk;)V

    .line 582
    new-instance v7, Lfmv;

    new-instance v14, Lgan;

    invoke-direct {v14}, Lgan;-><init>()V

    move-object/from16 v0, p1

    iget-object v15, v0, Lfml;->cvY:Lfnh;

    move-object v8, v4

    move-object/from16 v9, p6

    move-object v10, v6

    move-object/from16 v11, p9

    move-object/from16 v12, p1

    move-object v13, v3

    move-object/from16 v16, v5

    invoke-direct/range {v7 .. v16}, Lfmv;-><init>(Landroid/content/Context;Lerk;Lfjs;Lfzw;Lfml;Lfns;Lgan;Lesm;Lemp;)V

    .line 592
    move-object/from16 v0, p5

    invoke-virtual {v7, v0}, Lfmv;->a(Lfnc;)V

    .line 594
    new-instance v13, Lfnn;

    move-object/from16 v0, p6

    invoke-direct {v13, v4, v0}, Lfnn;-><init>(Landroid/content/Context;Lerp;)V

    .line 597
    new-instance v8, Lflj;

    new-instance v12, Lflo;

    move-object/from16 v0, p8

    invoke-direct {v12, v0}, Lflo;-><init>(Ligi;)V

    const/16 v17, 0x0

    move-object v9, v4

    move-object/from16 v10, p7

    move-object/from16 v11, p1

    move-object/from16 v14, p6

    move-object v15, v5

    move-object/from16 v16, p12

    invoke-direct/range {v8 .. v17}, Lflj;-><init>(Landroid/content/Context;Leoj;Lfml;Lfnm;Lfnn;Lern;Lemp;Ligi;Z)V

    .line 605
    new-instance v17, Lfmf;

    move-object/from16 v0, v17

    move-object/from16 v1, p5

    move-object/from16 v2, p3

    invoke-direct {v0, v7, v1, v2}, Lfmf;-><init>(Lfmv;Lflw;Lfnx;)V

    .line 622
    new-instance v14, Lflr;

    invoke-virtual/range {p5 .. p5}, Lflw;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v18

    invoke-virtual/range {p5 .. p5}, Lflw;->wj()Lekf;

    move-result-object v19

    new-instance v21, Lgbr;

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Lgbr;-><init>(Lemp;)V

    const/16 v22, 0x0

    move-object/from16 v15, p0

    move-object/from16 v16, p6

    move-object/from16 v20, v8

    invoke-direct/range {v14 .. v22}, Lflr;-><init>(Landroid/app/Activity;Lerp;Lflv;Lcom/google/android/shared/ui/SuggestionGridLayout;Lekf;Lfmt;Lgbr;Lflb;)V

    .line 631
    iput-object v14, v8, Lflj;->cuQ:Lflr;

    .line 633
    if-eqz p13, :cond_0

    .line 643
    invoke-virtual/range {p5 .. p5}, Lflw;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v3, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Leko;)V

    :cond_0
    move-object/from16 v15, p5

    move-object/from16 v16, p0

    move-object/from16 v17, p7

    move-object/from16 v18, v7

    move-object/from16 v19, v14

    move-object/from16 v20, p1

    move-object/from16 v21, v13

    move-object/from16 v22, v8

    .line 645
    invoke-virtual/range {v15 .. v22}, Lflw;->a(Landroid/app/Activity;Leoj;Lfmv;Lflr;Lfml;Lfnn;Lfkv;)V

    .line 648
    new-instance v14, Lfmd;

    move-object v15, v4

    move-object/from16 v16, p0

    move-object/from16 v17, p2

    move-object/from16 v18, p3

    move-object/from16 v19, p1

    move-object/from16 v20, p5

    move-object/from16 v21, v7

    move-object/from16 v22, v13

    move-object/from16 v23, v6

    move-object/from16 v24, p14

    invoke-direct/range {v14 .. v24}, Lfmd;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfnx;Lfml;Lflw;Lfmv;Lfnn;Lfjs;Ljava/lang/String;)V

    return-object v14
.end method


# virtual methods
.method public final D(F)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0, p1}, Lflw;->D(F)V

    .line 203
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->D(F)V

    .line 206
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lfok;)V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0, p1, p2}, Lfnx;->a(Ljava/lang/CharSequence;Lfok;)V

    .line 475
    return-void
.end method

.method public final a(ZLandroid/content/Intent;)V
    .locals 5
    .param p2    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 257
    iput-boolean v2, p0, Lfmd;->Kl:Z

    .line 258
    iget-object v0, p0, Lfmd;->cvN:Landroid/content/Intent;

    if-eq p2, v0, :cond_0

    iput-object p2, p0, Lfmd;->cvN:Landroid/content/Intent;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v1, v0, v2}, Lfmv;->c(Landroid/os/Bundle;Z)V

    .line 263
    :cond_0
    iget-boolean v0, p0, Lfmd;->cvG:Z

    if-nez v0, :cond_1

    .line 264
    iget-object v0, p0, Lfmd;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lfmd;->cvF:Lfmg;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.now.ENTRIES_UPDATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 266
    iput-boolean v4, p0, Lfmd;->cvG:Z

    .line 270
    :cond_1
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBc()V

    .line 274
    iget-object v0, p0, Lfmd;->asz:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 275
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBg()V

    .line 279
    if-eqz p1, :cond_3

    .line 280
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    iput-boolean v4, v0, Lfmv;->cwF:Z

    invoke-virtual {v0}, Lfmv;->aBj()V

    .line 285
    :goto_0
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onResume()V

    .line 288
    :cond_2
    return-void

    .line 282
    :cond_3
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBj()V

    goto :goto_0
.end method

.method public final bo(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 875
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0, p1}, Lflw;->bo(Landroid/view/View;)V

    .line 876
    return-void
.end method

.method public final dJ(Z)V
    .locals 2

    .prologue
    .line 380
    invoke-virtual {p0, p1}, Lfmd;->fr(Z)V

    .line 384
    invoke-virtual {p0}, Lfmd;->onPause()V

    .line 387
    iget-boolean v0, p0, Lfmd;->cvG:Z

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lfmd;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lfmd;->cvF:Lfmg;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 389
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfmd;->cvG:Z

    .line 393
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfmd;->Kl:Z

    .line 395
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onDestroy()V

    .line 398
    :cond_1
    return-void
.end method

.method public final fq(Z)V
    .locals 1

    .prologue
    .line 210
    if-eqz p1, :cond_0

    iget-object v0, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cN()Z

    .line 213
    :cond_0
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0, p1}, Lflw;->fq(Z)V

    .line 214
    return-void
.end method

.method public final fr(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 347
    iget-boolean v0, p0, Lfmd;->Kt:Z

    if-nez v0, :cond_1

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iput-boolean v1, p0, Lfmd;->Kt:Z

    .line 355
    iget-object v0, p0, Lfmd;->mUndoDismissManager:Lfnn;

    invoke-virtual {v0}, Lfnn;->aBm()V

    .line 359
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0, p1}, Lflw;->fr(Z)V

    .line 362
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBc()V

    .line 365
    iget-boolean v0, p0, Lfmd;->cvH:Z

    if-eqz v0, :cond_2

    .line 366
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBj()V

    .line 367
    iput-boolean v1, p0, Lfmd;->cvH:Z

    .line 370
    :cond_2
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStop()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfmd;->Kl:Z

    .line 294
    iget-object v0, p0, Lfmd;->mUndoDismissManager:Lfnn;

    invoke-virtual {v0}, Lfnn;->aBm()V

    .line 297
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBi()V

    .line 298
    iget-object v0, p0, Lfmd;->asz:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 299
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStop()V

    .line 302
    :cond_0
    return-void
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 416
    if-eqz p1, :cond_2

    const-string v0, "now_overlay:views_hidden_for_search"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "now_overlay:views_hidden_for_search"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lfmd;->fq(Z)V

    :cond_0
    const-string v0, "now_overlay:card_view_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfmd;->cvI:Lflw;

    const-string v1, "now_overlay:card_view_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflw;->g(Landroid/os/Parcelable;)V

    :cond_1
    const-string v0, "now_overlay:drawer_view_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    const-string v1, "now_overlay:drawer_view_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->g(Landroid/os/Parcelable;)V

    .line 418
    :cond_2
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_3

    .line 419
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onPostCreate(Landroid/os/Bundle;)V

    .line 422
    :cond_3
    return-void
.end method

.method public final w(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 402
    const-string v0, "now_overlay:views_hidden_for_search"

    iget-object v1, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v1}, Lflw;->aAR()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 403
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->atL()Landroid/os/Parcelable;

    move-result-object v0

    .line 404
    const-string v1, "now_overlay:card_view_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 405
    iget-object v0, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v0, :cond_0

    .line 406
    const-string v0, "now_overlay:drawer_view_state"

    iget-object v1, p0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atL()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 410
    :cond_0
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 413
    :cond_1
    return-void
.end method

.method public final x(ZZ)V
    .locals 5

    .prologue
    .line 305
    iget-boolean v0, p0, Lfmd;->Kt:Z

    if-eqz v0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfmd;->Kt:Z

    .line 313
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    new-instance v0, Lfmk;

    iget-object v1, p0, Lfmd;->mNowRemoteClient:Lfml;

    iget-object v2, p0, Lfmd;->coZ:Lfjs;

    iget-object v3, p0, Lfmd;->cvN:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, p2, v3}, Lfmk;-><init>(Lfml;Lfjs;ZLandroid/content/Intent;)V

    invoke-virtual {v0}, Lfmk;->run()V

    .line 325
    :goto_1
    if-eqz p1, :cond_3

    .line 328
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->aAT()V

    .line 329
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->reset()V

    .line 330
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->aAS()V

    .line 331
    iget-object v0, p0, Lfmd;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBj()V

    .line 337
    :goto_2
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBd()V

    .line 340
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->aAS()V

    .line 342
    iget-object v0, p0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->aBJ()V

    goto :goto_0

    .line 317
    :cond_2
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    new-instance v1, Lfmk;

    iget-object v2, p0, Lfmd;->mNowRemoteClient:Lfml;

    iget-object v3, p0, Lfmd;->coZ:Lfjs;

    iget-object v4, p0, Lfmd;->cvN:Landroid/content/Intent;

    invoke-direct {v1, v2, v3, p2, v4}, Lfmk;-><init>(Lfml;Lfjs;ZLandroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lfml;->d(Lesk;)V

    .line 320
    iget-object v0, p0, Lfmd;->mNowRemoteClient:Lfml;

    new-instance v1, Lfmj;

    iget-object v2, p0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-direct {v1, p0, v2}, Lfmj;-><init>(Lfmd;Lfml;)V

    invoke-virtual {v0, v1}, Lfml;->d(Lesk;)V

    .line 322
    iget-object v0, p0, Lfmd;->asz:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    goto :goto_1

    .line 333
    :cond_3
    iget-object v0, p0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->aAS()V

    goto :goto_2
.end method
