.class final Ljso;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field ciy:Ljava/lang/Object;

.field eCw:Ljsm;

.field eCx:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljso;->eCx:Ljava/util/List;

    .line 55
    return-void
.end method

.method constructor <init>(Ljsm;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Ljso;->eCw:Ljsm;

    .line 50
    iput-object p2, p0, Ljso;->ciy:Ljava/lang/Object;

    .line 51
    return-void
.end method

.method private toByteArray()[B
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p0}, Ljso;->lF()I

    move-result v0

    new-array v0, v0, [B

    .line 168
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Ljsj;->m([BII)Ljsj;

    move-result-object v1

    .line 169
    invoke-virtual {p0, v1}, Ljso;->a(Ljsj;)V

    .line 170
    return-object v0
.end method


# virtual methods
.method final a(Ljsj;)V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Ljso;->eCw:Ljsm;

    iget-object v1, p0, Ljso;->ciy:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Ljsm;->a(Ljava/lang/Object;Ljsj;)V

    .line 101
    :cond_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Ljso;->eCx:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljst;

    .line 98
    iget v2, v0, Ljst;->tag:I

    invoke-virtual {p1, v2}, Ljsj;->sd(I)V

    iget-object v0, v0, Ljst;->aHs:[B

    invoke-virtual {p1, v0}, Ljsj;->av([B)V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105
    if-ne p1, p0, :cond_1

    .line 106
    const/4 v0, 0x1

    .line 146
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    instance-of v1, p1, Ljso;

    if-eqz v1, :cond_0

    .line 112
    check-cast p1, Ljso;

    .line 113
    iget-object v1, p0, Ljso;->ciy:Ljava/lang/Object;

    if-eqz v1, :cond_9

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    if-eqz v1, :cond_9

    .line 117
    iget-object v1, p0, Ljso;->eCw:Ljsm;

    iget-object v2, p1, Ljso;->eCw:Ljsm;

    if-ne v1, v2, :cond_0

    .line 120
    iget-object v0, p0, Ljso;->eCw:Ljsm;

    iget-object v0, v0, Ljsm;->eCr:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_2

    .line 122
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 124
    :cond_2
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0

    .line 126
    :cond_3
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    instance-of v0, v0, [I

    if-eqz v0, :cond_4

    .line 127
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [I

    check-cast v0, [I

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [I

    check-cast v1, [I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0

    .line 128
    :cond_4
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_5

    .line 129
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [J

    check-cast v0, [J

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [J

    check-cast v1, [J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    goto :goto_0

    .line 130
    :cond_5
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    instance-of v0, v0, [F

    if-eqz v0, :cond_6

    .line 131
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [F

    check-cast v0, [F

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [F

    check-cast v1, [F

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    goto/16 :goto_0

    .line 132
    :cond_6
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    instance-of v0, v0, [D

    if-eqz v0, :cond_7

    .line 133
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [D

    check-cast v0, [D

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [D

    check-cast v1, [D

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v0

    goto/16 :goto_0

    .line 134
    :cond_7
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    instance-of v0, v0, [Z

    if-eqz v0, :cond_8

    .line 135
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [Z

    check-cast v0, [Z

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [Z

    check-cast v1, [Z

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Z[Z)Z

    move-result v0

    goto/16 :goto_0

    .line 137
    :cond_8
    iget-object v0, p0, Ljso;->ciy:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget-object v1, p1, Ljso;->ciy:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 140
    :cond_9
    iget-object v0, p0, Ljso;->eCx:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p1, Ljso;->eCx:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 142
    iget-object v0, p0, Ljso;->eCx:Ljava/util/List;

    iget-object v1, p1, Ljso;->eCx:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 146
    :cond_a
    :try_start_0
    invoke-direct {p0}, Ljso;->toByteArray()[B

    move-result-object v0

    invoke-direct {p1}, Ljso;->toByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto/16 :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 149
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 155
    :try_start_0
    invoke-direct {p0}, Ljso;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 163
    return v0

    .line 159
    :catch_0
    move-exception v0

    .line 161
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method final lF()I
    .locals 4

    .prologue
    .line 82
    const/4 v0, 0x0

    .line 83
    iget-object v1, p0, Ljso;->ciy:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 84
    iget-object v0, p0, Ljso;->eCw:Ljsm;

    iget-object v1, p0, Ljso;->ciy:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljsm;->bD(Ljava/lang/Object;)I

    move-result v1

    .line 90
    :cond_0
    return v1

    .line 86
    :cond_1
    iget-object v1, p0, Ljso;->eCx:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljst;

    .line 87
    iget v3, v0, Ljst;->tag:I

    invoke-static {v3}, Ljsj;->se(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v0, v0, Ljst;->aHs:[B

    array-length v0, v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 88
    goto :goto_0
.end method
