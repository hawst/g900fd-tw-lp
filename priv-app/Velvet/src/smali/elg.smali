.class public Lelg;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field protected ceA:I

.field protected cez:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 25
    invoke-virtual {p0, p3}, Lelg;->addAll([Ljava/lang/Object;)V

    .line 26
    return-void
.end method


# virtual methods
.method public final F(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lelg;->cez:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iput-object p1, p0, Lelg;->cez:Ljava/lang/CharSequence;

    .line 43
    invoke-virtual {p0}, Lelg;->notifyDataSetChanged()V

    .line 45
    :cond_0
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 31
    iget v0, p0, Lelg;->ceA:I

    if-ne p1, v0, :cond_0

    move-object v0, v1

    .line 32
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lelg;->cez:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    :cond_0
    return-object v1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lelg;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lelg;->ceA:I

    .line 50
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 51
    return-void
.end method
