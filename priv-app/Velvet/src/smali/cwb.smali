.class final Lcwb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field private synthetic blC:Lcvy;

.field private synthetic blD:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcvy;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcwb;->blC:Lcvy;

    iput-object p2, p0, Lcwb;->blD:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 238
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcwb;->blC:Lcvy;

    iput-object p1, v0, Lcvy;->blz:Ljava/lang/Boolean;

    iget-object v0, p0, Lcwb;->blD:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcwb;->blC:Lcvy;

    iget-object v1, v1, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v1}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcwb;->blC:Lcvy;

    iget-object v0, v0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v6}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcwb;->blC:Lcvy;

    const v1, 0x7f0a059f

    invoke-virtual {v0, v1}, Lcvy;->cZ(I)V

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v0, p0, Lcwb;->blC:Lcvy;

    iget-object v0, v0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v5}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcwb;->blC:Lcvy;

    iget-object v0, v0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcwb;->blC:Lcvy;

    iget-object v0, v0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwb;->blC:Lcvy;

    invoke-virtual {v1}, Lcvy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05a0

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcwb;->blD:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
