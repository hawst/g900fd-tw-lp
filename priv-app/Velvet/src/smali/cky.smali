.class public Lcky;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private final baq:Ljava/util/Map;

.field private final bar:Ljava/util/Deque;

.field private final bas:Ljava/util/Stack;

.field private final bat:Ljava/util/Stack;

.field public bau:Lckz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bav:Ljava/lang/Boolean;

.field public baw:Ljava/util/List;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public bax:Ljava/util/List;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private bay:Lclc;

.field private baz:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcky;->baq:Ljava/util/Map;

    .line 63
    new-instance v0, Ljava/util/ArrayDeque;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcky;->bar:Ljava/util/Deque;

    .line 69
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    .line 70
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    .line 140
    iput-object v2, p0, Lcky;->bav:Ljava/lang/Boolean;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcky;->baw:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcky;->bax:Ljava/util/List;

    .line 147
    iput-object v2, p0, Lcky;->bay:Lclc;

    return-void
.end method


# virtual methods
.method public final Nd()V
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lcky;->bau:Lckz;

    .line 389
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 391
    return-void
.end method

.method public final Pg()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 260
    iget-object v1, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Ph()Z
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Pi()Z
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Pj()Liwg;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x0

    .line 351
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->baA:Ljkt;

    if-nez v0, :cond_1

    .line 353
    :cond_0
    const/4 v0, 0x0

    .line 378
    :goto_0
    return-object v0

    .line 358
    :cond_1
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->baA:Ljkt;

    sget-object v1, Ljmb;->eti:Ljsm;

    invoke-virtual {v0, v1}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->aln()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 362
    new-instance v0, Liwg;

    invoke-direct {v0}, Liwg;-><init>()V

    .line 363
    new-array v1, v6, [Liwi;

    iput-object v1, v0, Liwg;->dLO:[Liwi;

    move v1, v2

    .line 364
    :goto_1
    if-ge v1, v6, :cond_2

    .line 365
    iget-object v3, v0, Liwg;->dLO:[Liwi;

    new-instance v4, Liwi;

    invoke-direct {v4}, Liwi;-><init>()V

    aput-object v4, v3, v1

    .line 366
    iget-object v3, v0, Liwg;->dLO:[Liwi;

    aget-object v3, v3, v1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v3, v4, v5}, Liwi;->m(D)Liwi;

    .line 364
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 368
    :cond_2
    iget-object v1, v0, Liwg;->dLO:[Liwi;

    aget-object v1, v1, v2

    const-string v2, "MessageSearchAction-Prompted-Continue"

    invoke-virtual {v1, v2}, Liwi;->pW(Ljava/lang/String;)Liwi;

    .line 370
    iget-object v1, v0, Liwg;->dLO:[Liwi;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const-string v2, "MessageSearchAction-Prompted-Reply"

    invoke-virtual {v1, v2}, Liwi;->pW(Ljava/lang/String;)Liwi;

    .line 372
    iget-object v1, v0, Liwg;->dLO:[Liwi;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const-string v2, "MessageSearchAction-Prompted-Repeat"

    invoke-virtual {v1, v2}, Liwi;->pW(Ljava/lang/String;)Liwi;

    .line 374
    iget-object v1, v0, Liwg;->dLO:[Liwi;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const-string v2, "MessageSearchAction-Prompted-Terminate"

    invoke-virtual {v1, v2}, Liwi;->pW(Ljava/lang/String;)Liwi;

    goto :goto_0

    .line 378
    :cond_3
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->baB:Liwg;

    goto :goto_0
.end method

.method public final Pk()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 382
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->baE:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Pl()Ljkt;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-nez v0, :cond_0

    .line 395
    const/4 v0, 0x0

    .line 397
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->baA:Ljkt;

    goto :goto_0
.end method

.method public Pm()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-nez v0, :cond_0

    .line 409
    const/4 v0, 0x0

    .line 411
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_0
.end method

.method public Pn()Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 420
    invoke-virtual {p0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 421
    instance-of v1, v0, Lcom/google/android/search/shared/actions/CommunicationAction;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/search/shared/actions/CommunicationAction;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 432
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    instance-of v0, v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 446
    :goto_0
    return-object v0

    .line 440
    :cond_1
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajh()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 441
    instance-of v3, v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    if-eqz v3, :cond_2

    .line 442
    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 446
    goto :goto_0
.end method

.method public final Pp()Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-nez v0, :cond_0

    .line 451
    const/4 v0, 0x0

    .line 453
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final Pq()I
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-nez v0, :cond_0

    .line 481
    const/4 v0, 0x0

    .line 483
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget v0, v0, Lckz;->baC:I

    goto :goto_0
.end method

.method public final Pr()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 487
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    iget-object v1, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    :cond_0
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    const/4 v0, 0x0

    .line 497
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_0
.end method

.method public final Ps()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    const/4 v0, 0x0

    .line 504
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_0
.end method

.method public final declared-synchronized Pt()Lclc;
    .locals 4

    .prologue
    .line 530
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcky;->bay:Lclc;

    if-nez v0, :cond_1

    .line 531
    invoke-static {}, Lijp;->aXc()Lijr;

    move-result-object v1

    .line 532
    iget-object v0, p0, Lcky;->baq:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclf;

    .line 533
    invoke-virtual {v0}, Lclf;->Px()Lcle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lijr;->bv(Ljava/lang/Object;)Lijr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 536
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcky;->bar:Ljava/util/Deque;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    .line 538
    new-instance v2, Lclc;

    invoke-virtual {v1}, Lijr;->aXd()Lijp;

    move-result-object v1

    iget-object v3, p0, Lcky;->baz:Ljava/lang/String;

    invoke-direct {v2, v1, v0, v3}, Lclc;-><init>(Lijp;Lijj;Ljava/lang/String;)V

    iput-object v2, p0, Lcky;->bay:Lclc;

    .line 542
    :cond_1
    iget-object v0, p0, Lcky;->bay:Lclc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final a(Letj;)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    .line 558
    const-string v0, "DiscourseContext"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 559
    const-string v0, "Entities"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcky;->baq:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 560
    iget-object v0, p0, Lcky;->baq:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclf;

    .line 561
    const-string v2, "Entity"

    invoke-virtual {p1, v2}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    invoke-virtual {v0}, Lclf;->Px()Lcle;

    move-result-object v0

    invoke-interface {v0}, Lcle;->Pv()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 563
    :cond_0
    const-string v0, "Displays"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcky;->bar:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 564
    iget-object v0, p0, Lcky;->bar:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjz;

    .line 565
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    const-string v3, "Display"

    invoke-virtual {v2, v3}, Letj;->lt(Ljava/lang/String;)V

    const-string v3, "URI"

    invoke-virtual {v2, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    invoke-virtual {v0}, Ljjz;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    const-string v3, "Timestamp"

    invoke-virtual {v2, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {v0}, Ljjz;->getTimestamp()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Letn;->a(Ljava/util/Date;)V

    iget-object v3, v0, Ljjz;->epG:Ljjo;

    if-eqz v3, :cond_1

    const-string v3, "AppPkg"

    invoke-virtual {v2, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-object v4, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v4}, Ljjo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    const-string v3, "AppURI"

    invoke-virtual {v2, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-object v4, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v4}, Ljjo;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    const-string v3, "Query"

    invoke-virtual {v2, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v0}, Ljjo;->bnX()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_1

    .line 567
    :cond_2
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Lcld;)V
    .locals 1

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcky;->av(Ljava/lang/Object;)Lclf;

    move-result-object v0

    .line 191
    invoke-virtual {v0, p2}, Lclf;->b(Lcld;)V

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcky;->bay:Lclc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljkb;J)V
    .locals 4

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljjz;

    invoke-direct {v0}, Ljjz;-><init>()V

    .line 200
    iput-object p1, v0, Ljjz;->epH:Ljkb;

    .line 201
    invoke-virtual {v0, p2, p3}, Ljjz;->dy(J)Ljjz;

    .line 202
    :goto_0
    iget-object v1, p0, Lcky;->bar:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 203
    iget-object v1, p0, Lcky;->bar:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 205
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcky;->bar:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcky;->bay:Lclc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    monitor-exit p0

    return-void
.end method

.method public final a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;Liwg;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 9

    .prologue
    .line 216
    instance-of v0, p2, Lcom/google/android/search/shared/actions/errors/SearchError;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/android/search/shared/actions/ButtonAction;

    if-eqz v0, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    move v8, v0

    .line 223
    :goto_1
    if-eqz p6, :cond_5

    .line 224
    const/4 v5, 0x0

    .line 234
    :goto_2
    new-instance v0, Lckz;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lckz;-><init>(Ljkt;Liwg;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcky;->bau:Lckz;

    .line 238
    invoke-virtual {p0, p2}, Lcky;->g(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 243
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p2, :cond_0

    .line 244
    :cond_2
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_7

    .line 245
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 255
    :cond_3
    :goto_3
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 220
    :cond_4
    const/4 v0, 0x0

    move v8, v0

    goto :goto_1

    .line 225
    :cond_5
    if-eqz p1, :cond_6

    sget-object v0, Ljlf;->erl:Ljsm;

    invoke-virtual {p1, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 228
    sget-object v0, Ljlf;->erl:Ljsm;

    invoke-virtual {p1, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlf;

    invoke-virtual {v0}, Ljlf;->Pq()I

    move-result v5

    goto :goto_2

    .line 231
    :cond_6
    invoke-virtual {p0, p2}, Lcky;->h(Lcom/google/android/search/shared/actions/VoiceAction;)I

    move-result v5

    goto :goto_2

    .line 247
    :cond_7
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 251
    if-eqz v8, :cond_3

    .line 252
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    goto :goto_3
.end method

.method public final av(Ljava/lang/Object;)Lclf;
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcky;->baq:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclf;

    .line 512
    if-eqz v0, :cond_0

    .line 517
    :goto_0
    return-object v0

    .line 515
    :cond_0
    new-instance v0, Lclf;

    invoke-direct {v0, p1}, Lclf;-><init>(Ljava/lang/Object;)V

    .line 516
    iget-object v1, p0, Lcky;->baq:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final f(Lcom/google/android/search/shared/actions/VoiceAction;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276
    if-nez p1, :cond_1

    .line 323
    :cond_0
    :goto_0
    return v0

    .line 279
    :cond_1
    iget-object v2, p0, Lcky;->bau:Lckz;

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcky;->Pi()Z

    move-result v2

    if-nez v2, :cond_3

    .line 280
    iget-object v2, p0, Lcky;->bau:Lckz;

    iget-object v2, v2, Lckz;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcky;->bau:Lckz;

    iget-object v2, v2, Lckz;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alk()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 285
    goto :goto_0

    .line 292
    :cond_3
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agy()Ldxl;

    move-result-object v2

    .line 293
    if-nez v2, :cond_4

    move v0, v1

    .line 296
    goto :goto_0

    .line 303
    :cond_4
    invoke-virtual {v2}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v2

    .line 306
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alA()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 312
    :goto_1
    iget-object v1, p0, Lcky;->bau:Lckz;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcky;->bau:Lckz;

    iget-object v1, v1, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v1}, Lcom/google/android/search/shared/actions/VoiceAction;->agy()Ldxl;

    move-result-object v1

    .line 316
    :goto_2
    if-eqz v1, :cond_0

    .line 317
    invoke-virtual {v1}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 306
    goto :goto_1

    .line 312
    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final g(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 327
    invoke-virtual {p0}, Lcky;->Pl()Ljkt;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_1

    .line 329
    iget-object v1, v0, Ljkt;->eqo:Ljlm;

    if-nez v1, :cond_0

    .line 330
    new-instance v1, Ljlm;

    invoke-direct {v1}, Ljlm;-><init>()V

    iput-object v1, v0, Ljkt;->eqo:Ljlm;

    .line 332
    :cond_0
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 333
    iget-object v0, v0, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0, v2}, Ljlm;->iA(Z)Ljlm;

    .line 337
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 345
    :cond_1
    :goto_0
    return-void

    .line 338
    :cond_2
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agr()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 339
    :cond_3
    iget-object v0, v0, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0, v2}, Ljlm;->ix(Z)Ljlm;

    .line 341
    iget-object v0, p0, Lcky;->bat:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 342
    iget-object v0, p0, Lcky;->bas:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    goto :goto_0
.end method

.method public final h(Lcom/google/android/search/shared/actions/VoiceAction;)I
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    if-ne p1, v0, :cond_0

    .line 466
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget v0, v0, Lckz;->baC:I

    .line 472
    :goto_0
    return v0

    .line 467
    :cond_0
    invoke-virtual {p0, p1}, Lcky;->f(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 468
    iget-object v0, p0, Lcky;->bau:Lckz;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcky;->Pi()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcky;->bau:Lckz;

    iget v0, v0, Lckz;->baC:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 472
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized hl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 523
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcky;->baz:Ljava/lang/String;

    invoke-static {v0, p1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    iput-object p1, p0, Lcky;->baz:Ljava/lang/String;

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcky;->bay:Lclc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    :cond_0
    monitor-exit p0

    return-void

    .line 523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
