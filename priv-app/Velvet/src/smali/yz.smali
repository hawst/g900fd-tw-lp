.class public final Lyz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/app/backup/BackupHelper;


# static fields
.field private static Mu:Landroid/app/backup/BackupManager;

.field private static final Mv:Landroid/graphics/Bitmap$CompressFormat;

.field private static final Mw:[Ljava/lang/String;

.field private static final Mx:[Ljava/lang/String;

.field private static xn:Lwi;


# instance fields
.field private final MA:Ljava/util/ArrayList;

.field private final My:Z

.field private Mz:Ljava/util/HashMap;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    sput-object v0, Lyz;->Mv:Landroid/graphics/Bitmap$CompressFormat;

    .line 95
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "modified"

    aput-object v1, v0, v4

    const-string v1, "intent"

    aput-object v1, v0, v5

    const-string v1, "appWidgetProvider"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "appWidgetId"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cellX"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "cellY"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "container"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "iconPackage"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "iconResource"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "iconType"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "itemType"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "screen"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "spanX"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "spanY"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "profileId"

    aput-object v2, v0, v1

    sput-object v0, Lyz;->Mw:[Ljava/lang/String;

    .line 135
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "modified"

    aput-object v1, v0, v4

    const-string v1, "screenRank"

    aput-object v1, v0, v5

    sput-object v0, Lyz;->Mx:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lyz;->mContext:Landroid/content/Context;

    .line 155
    iput-boolean p2, p0, Lyz;->My:Z

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lyz;->MA:Ljava/util/ArrayList;

    .line 157
    return-void
.end method

.method private static a(Ljava/util/Set;Landroid/app/backup/BackupDataOutput;)I
    .locals 5

    .prologue
    .line 1080
    const/4 v0, 0x0

    .line 1081
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1082
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dropping deleted item "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1083
    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    .line 1084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1085
    goto :goto_0

    .line 1086
    :cond_0
    return v1
.end method

.method private static a(Landroid/os/ParcelFileDescriptor;)Lahc;
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 981
    new-instance v0, Lahc;

    invoke-direct {v0}, Lahc;-><init>()V

    .line 982
    if-nez p0, :cond_0

    .line 1044
    :goto_0
    return-object v0

    .line 985
    :cond_0
    new-instance v10, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v10, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 987
    :try_start_0
    invoke-virtual {v10}, Ljava/io/FileInputStream;->available()I

    move-result v4

    .line 989
    const v1, 0xf4240

    if-ge v4, v1, :cond_3

    .line 990
    new-array v7, v4, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v9, v8

    move v2, v5

    move v1, v5

    move v6, v4

    move-object v4, v7

    .line 994
    :goto_1
    if-lez v6, :cond_2

    .line 1000
    const/4 v7, 0x1

    :try_start_1
    invoke-virtual {v10, v4, v1, v7}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .line 1001
    if-lez v7, :cond_1

    .line 1002
    sub-int/2addr v6, v7

    .line 1003
    add-int/2addr v1, v7

    move-object v7, v4

    move v4, v6

    move v6, v1

    .line 1019
    :goto_2
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v7, v1, v6}, Lyz;->b([BII)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1023
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "read "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes of journal"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljsq; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v2, v3

    move v1, v6

    move-object v4, v7

    move v6, v5

    .line 1028
    goto :goto_1

    .line 1008
    :cond_1
    :try_start_4
    const-string v6, "LauncherBackupHelper"

    const-string v7, "unexpected end of file while reading journal."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v6, v1

    move-object v7, v4

    move v4, v5

    .line 1015
    goto :goto_2

    .line 1013
    :catch_0
    move-exception v4

    move v6, v1

    move-object v7, v8

    move v4, v5

    .line 1014
    goto :goto_2

    .line 1024
    :catch_1
    move-exception v1

    .line 1027
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Lahc;->lG()Lahc;

    move-object v9, v1

    move v1, v6

    move v6, v4

    move-object v4, v7

    .line 1028
    goto :goto_1

    .line 1031
    :cond_2
    if-nez v2, :cond_3

    .line 1032
    const-string v1, "LauncherBackupHelper"

    const-string v2, "could not find a valid journal"

    invoke-static {v1, v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1039
    :cond_3
    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 1040
    :catch_2
    move-exception v1

    .line 1041
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to close the journal"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1035
    :catch_3
    move-exception v1

    .line 1036
    :try_start_7
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to close the journal"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1039
    :try_start_8
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_0

    .line 1040
    :catch_4
    move-exception v1

    .line 1041
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to close the journal"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 1038
    :catchall_0
    move-exception v0

    .line 1039
    :try_start_9
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 1042
    :goto_4
    throw v0

    .line 1040
    :catch_5
    move-exception v1

    .line 1041
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to close the journal"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 1024
    :catch_6
    move-exception v1

    move v2, v3

    move v4, v5

    goto :goto_3
.end method

.method private static a(Lahd;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 728
    invoke-static {p0}, Lahd;->m(Ljsr;)[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILahc;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 1068
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1069
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p2, Lahc;->Yc:[Lahd;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1070
    iget-object v2, p2, Lahc;->Yc:[Lahd;

    aget-object v2, v2, v0

    .line 1071
    iget v3, v2, Lahd;->type:I

    if-ne v3, p1, :cond_0

    .line 1072
    invoke-static {v2}, Lyz;->a(Lahd;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1069
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1075
    :cond_1
    return-object v1
.end method

.method private a(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 297
    invoke-direct {p0, v0, p1}, Lyz;->a(ILahc;)Ljava/util/Set;

    move-result-object v6

    .line 301
    iget-object v0, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 303
    sget-object v1, Labn;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lyz;->Mw:[Ljava/lang/String;

    invoke-direct {p0}, Lyz;->ip()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 305
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 307
    const/4 v2, -0x1

    :try_start_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 308
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 309
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 310
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 311
    const/4 v7, 0x1

    invoke-direct {p0, v7, v2, v3}, Lyz;->c(IJ)Lahd;

    move-result-object v7

    .line 312
    invoke-virtual {p4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    invoke-static {v7}, Lyz;->a(Lahd;)Ljava/lang/String;

    move-result-object v8

    .line 314
    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 315
    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-wide v8, p1, Lahc;->XZ:J

    cmp-long v8, v4, v8

    if-ltz v8, :cond_1

    .line 316
    :cond_0
    invoke-direct {p0, v1}, Lyz;->e(Landroid/database/Cursor;)[B

    move-result-object v2

    .line 317
    invoke-direct {p0, v7, v2, p3, p2}, Lyz;->a(Lahd;[BLahc;Landroid/app/backup/BackupDataOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 323
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 319
    :cond_1
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "favorite "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was too old: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 323
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 328
    invoke-interface {v6, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 329
    iget v0, p3, Lahc;->Yb:I

    invoke-static {v6, p2}, Lyz;->a(Ljava/util/Set;Landroid/app/backup/BackupDataOutput;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p3, Lahc;->Yb:I

    .line 330
    return-void
.end method

.method private a(Lahd;[BLahc;Landroid/app/backup/BackupDataOutput;)V
    .locals 6

    .prologue
    .line 1049
    invoke-static {p1}, Lyz;->a(Lahd;)Ljava/lang/String;

    move-result-object v1

    .line 1050
    array-length v0, p2

    invoke-virtual {p4, v1, v0}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    .line 1051
    array-length v0, p2

    invoke-virtual {p4, p2, v0}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I

    .line 1052
    iget v0, p3, Lahc;->Yb:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p3, Lahc;->Yb:I

    .line 1053
    iget-wide v2, p3, Lahc;->Ya:J

    array-length v0, p2

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p3, Lahc;->Ya:J

    .line 1054
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "saving "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p1, Lahd;->type:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "anonymous"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p1, Lahd;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v2, p1, Lahd;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1065
    return-void

    .line 1054
    :pswitch_0
    const-string v0, "favorite"

    goto :goto_0

    :pswitch_1
    const-string v0, "screen"

    goto :goto_0

    :pswitch_2
    const-string v0, "icon"

    goto :goto_0

    :pswitch_3
    const-string v0, "widget"

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lahd;->name:Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/os/ParcelFileDescriptor;Lahc;)V
    .locals 3

    .prologue
    .line 1099
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 1102
    invoke-static {p2}, Lyz;->a(Ljsr;)[B

    move-result-object v1

    .line 1103
    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 1104
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 1105
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "wrote "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes of journal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1109
    :goto_0
    return-void

    .line 1106
    :catch_0
    move-exception v0

    .line 1107
    const-string v1, "LauncherBackupHelper"

    const-string v2, "failed to write backup journal"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static a(Ljsr;)[B
    .locals 4

    .prologue
    .line 1113
    new-instance v0, Laha;

    invoke-direct {v0}, Laha;-><init>()V

    .line 1114
    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    iput-object v1, v0, Laha;->XL:[B

    .line 1115
    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    .line 1116
    iget-object v2, v0, Laha;->XL:[B

    invoke-virtual {v1, v2}, Ljava/util/zip/CRC32;->update([B)V

    .line 1117
    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    iput-wide v2, v0, Laha;->XM:J

    .line 1118
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    return-object v0
.end method

.method private static b(Lahd;)J
    .locals 6

    .prologue
    const-wide/32 v4, 0xffff

    .line 773
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 774
    iget v1, p0, Lahd;->type:I

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update(I)V

    .line 775
    iget-wide v2, p0, Lahd;->id:J

    and-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update(I)V

    .line 776
    iget-wide v2, p0, Lahd;->id:J

    const/16 v1, 0x20

    shr-long/2addr v2, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update(I)V

    .line 777
    iget-object v1, p0, Lahd;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 778
    iget-object v1, p0, Lahd;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update([B)V

    .line 780
    :cond_0
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;
    .locals 4

    .prologue
    .line 1135
    iget-object v0, p0, Lyz;->Mz:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1136
    iget-object v0, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v0

    .line 1138
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lyz;->Mz:Ljava/util/HashMap;

    .line 1139
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 1140
    iget-object v2, p0, Lyz;->Mz:Ljava/util/HashMap;

    iget-object v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1143
    :cond_0
    iget-object v0, p0, Lyz;->Mz:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    return-object v0
.end method

.method private b(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    .line 375
    invoke-direct {p0, v0, p1}, Lyz;->a(ILahc;)Ljava/util/Set;

    move-result-object v6

    .line 379
    iget-object v0, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 380
    sget-object v1, Labo;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lyz;->Mx:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 382
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 384
    const/4 v2, -0x1

    :try_start_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 386
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 387
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 388
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 389
    const/4 v7, 0x2

    invoke-direct {p0, v7, v2, v3}, Lyz;->c(IJ)Lahd;

    move-result-object v7

    .line 390
    invoke-virtual {p4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    invoke-static {v7}, Lyz;->a(Lahd;)Ljava/lang/String;

    move-result-object v8

    .line 392
    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393
    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-wide v8, p1, Lahc;->XZ:J

    cmp-long v8, v4, v8

    if-ltz v8, :cond_1

    .line 394
    :cond_0
    new-instance v2, Lahf;

    invoke-direct {v2}, Lahf;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lahf;->id:J

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lahf;->Yg:I

    invoke-static {v2}, Lyz;->a(Ljsr;)[B

    move-result-object v2

    .line 395
    invoke-direct {p0, v7, v2, p3, p2}, Lyz;->a(Lahd;[BLahc;Landroid/app/backup/BackupDataOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 401
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 397
    :cond_1
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "screen "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was too old: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 401
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 406
    invoke-interface {v6, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 407
    iget v0, p3, Lahc;->Yb:I

    invoke-static {v6, p2}, Lyz;->a(Ljava/util/Set;Landroid/app/backup/BackupDataOutput;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p3, Lahc;->Yb:I

    .line 408
    return-void
.end method

.method private static b([BII)[B
    .locals 6

    .prologue
    .line 1124
    new-instance v0, Laha;

    invoke-direct {v0}, Laha;-><init>()V

    .line 1125
    invoke-static {v0, p0, p1, p2}, Ljsr;->b(Ljsr;[BII)Ljsr;

    .line 1126
    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    .line 1127
    iget-object v2, v0, Laha;->XL:[B

    invoke-virtual {v1, v2}, Ljava/util/zip/CRC32;->update([B)V

    .line 1128
    iget-wide v2, v0, Laha;->XM:J

    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1129
    new-instance v0, Ljsq;

    const-string v1, "checksum does not match"

    invoke-direct {v0, v1}, Ljsq;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1131
    :cond_0
    iget-object v0, v0, Laha;->XL:[B

    return-object v0
.end method

.method private c(IJ)Lahd;
    .locals 4

    .prologue
    .line 706
    new-instance v0, Lahd;

    invoke-direct {v0}, Lahd;-><init>()V

    .line 707
    iput p1, v0, Lahd;->type:I

    .line 708
    iput-wide p2, v0, Lahd;->id:J

    .line 709
    invoke-static {v0}, Lyz;->b(Lahd;)J

    move-result-wide v2

    iput-wide v2, v0, Lahd;->XM:J

    .line 710
    return-object v0
.end method

.method private c(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V
    .locals 18

    .prologue
    .line 453
    invoke-static {}, Lyz;->io()Z

    move-result v4

    if-nez v4, :cond_0

    .line 454
    invoke-direct/range {p0 .. p0}, Lyz;->dataChanged()V

    .line 529
    :goto_0
    return-void

    .line 458
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 459
    move-object/from16 v0, p0

    iget-object v5, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v10, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 460
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v11

    .line 463
    const/4 v5, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1}, Lyz;->a(ILahc;)Ljava/util/Set;

    move-result-object v12

    .line 467
    move-object/from16 v0, p3

    iget v13, v0, Lahc;->Yb:I

    .line 470
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "(itemType=0 OR itemType=1) AND "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lyz;->ip()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 473
    sget-object v5, Labn;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lyz;->Mw:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 475
    new-instance v7, Ljava/util/HashSet;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v7, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 477
    const/4 v4, -0x1

    :try_start_0
    invoke-interface {v6, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 478
    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 479
    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 480
    const/4 v4, 0x2

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 482
    const/4 v5, 0x0

    :try_start_1
    invoke-static {v4, v5}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v14

    .line 483
    invoke-virtual {v14}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v15

    .line 484
    const/4 v5, 0x0

    .line 485
    const/4 v4, 0x0

    .line 486
    if-eqz v15, :cond_2

    .line 487
    const/4 v4, 0x3

    invoke-virtual {v15}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lyz;->d(ILjava/lang/String;)Lahd;

    move-result-object v5

    .line 488
    invoke-static {v5}, Lyz;->a(Lahd;)Ljava/lang/String;

    move-result-object v4

    .line 489
    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 493
    :goto_2
    invoke-interface {v12, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 494
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "already saved icon "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 515
    :catch_0
    move-exception v4

    :try_start_2
    const-string v4, "LauncherBackupHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v14, "invalid URI on application favorite: "

    invoke-direct {v5, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 522
    :catchall_0
    move-exception v4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v4

    .line 491
    :cond_2
    :try_start_3
    const-string v15, "LauncherBackupHelper"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "empty intent on application favorite: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 517
    :catch_1
    move-exception v4

    :try_start_4
    const-string v4, "LauncherBackupHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v14, "unable to save application icon for favorite: "

    invoke-direct {v5, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 498
    :cond_3
    if-eqz v4, :cond_1

    .line 500
    :try_start_5
    move-object/from16 v0, p3

    iget v15, v0, Lahc;->Yb:I

    sub-int/2addr v15, v13

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ge v15, v0, :cond_5

    .line 501
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "saving icon "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    sget-object v4, Lyz;->xn:Lwi;

    invoke-virtual {v4, v14, v11}, Lwi;->a(Landroid/content/Intent;Lahz;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 503
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    if-eqz v4, :cond_1

    sget-object v14, Lyz;->xn:Lwi;

    invoke-virtual {v14, v4, v11}, Lwi;->a(Landroid/graphics/Bitmap;Lahz;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 505
    new-instance v14, Lahe;

    invoke-direct {v14}, Lahe;-><init>()V

    iput v10, v14, Lahe;->Ye:I

    new-instance v15, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v15}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v16, Lyz;->Mv:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v17, 0x4b

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v4, v0, v1, v15}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iput-object v4, v14, Lahe;->Yf:[B

    :cond_4
    invoke-static {v14}, Lyz;->a(Ljsr;)[B

    move-result-object v4

    .line 506
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct {v0, v5, v4, v1, v2}, Lyz;->a(Lahd;[BLahc;Landroid/app/backup/BackupDataOutput;)V

    goto/16 :goto_1

    .line 509
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v14, "deferring icon backup "

    invoke-direct {v5, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    invoke-direct/range {p0 .. p0}, Lyz;->dataChanged()V
    :try_end_5
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 522
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 527
    invoke-interface {v12, v7}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 528
    move-object/from16 v0, p3

    iget v4, v0, Lahc;->Yb:I

    move-object/from16 v0, p2

    invoke-static {v12, v0}, Lyz;->a(Ljava/util/Set;Landroid/app/backup/BackupDataOutput;)I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p3

    iput v4, v0, Lahc;->Yb:I

    goto/16 :goto_0
.end method

.method private d(ILjava/lang/String;)Lahd;
    .locals 4

    .prologue
    .line 719
    new-instance v0, Lahd;

    invoke-direct {v0}, Lahd;-><init>()V

    .line 720
    iput p1, v0, Lahd;->type:I

    .line 721
    iput-object p2, v0, Lahd;->name:Ljava/lang/String;

    .line 722
    invoke-static {v0}, Lyz;->b(Lahd;)J

    move-result-wide v2

    iput-wide v2, v0, Lahd;->XM:J

    .line 723
    return-object v0
.end method

.method private d(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V
    .locals 22

    .prologue
    .line 589
    sget-object v5, Lyu;->Mj:Lyu;

    .line 590
    if-eqz v5, :cond_0

    invoke-static {}, Lyz;->io()Z

    move-result v4

    if-nez v4, :cond_1

    .line 591
    :cond_0
    const-string v4, "LauncherBackupHelper"

    const-string v5, "Failed to get icon cache during restore"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    :goto_0
    return-void

    .line 594
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 595
    new-instance v10, Lafb;

    move-object/from16 v0, p0

    iget-object v6, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-direct {v10, v6}, Lafb;-><init>(Landroid/content/Context;)V

    .line 596
    new-instance v11, Laco;

    move-object/from16 v0, p0

    iget-object v6, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-direct {v11, v6}, Laco;-><init>(Landroid/content/Context;)V

    .line 597
    move-object/from16 v0, p0

    iget-object v6, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v12, v6, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 598
    iget-object v5, v5, Lyu;->Mk:Lur;

    invoke-virtual {v5}, Lur;->gl()Ltu;

    move-result-object v13

    .line 602
    const/4 v5, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1}, Lyz;->a(ILahc;)Ljava/util/Set;

    move-result-object v14

    .line 605
    move-object/from16 v0, p3

    iget v15, v0, Lahc;->Yb:I

    .line 607
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "itemType=4 AND "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lyz;->ip()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 609
    sget-object v5, Labn;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lyz;->Mw:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 611
    new-instance v7, Ljava/util/HashSet;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v7, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 613
    const/4 v4, -0x1

    :try_start_0
    invoke-interface {v6, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 614
    :cond_2
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 615
    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 616
    const/4 v4, 0x3

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 617
    const/16 v4, 0xe

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 618
    const/16 v4, 0xf

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 619
    invoke-static/range {v16 .. v16}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v19

    .line 620
    const/4 v5, 0x0

    .line 621
    const/4 v4, 0x0

    .line 622
    if-eqz v19, :cond_3

    .line 623
    const/4 v4, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v4, v1}, Lyz;->d(ILjava/lang/String;)Lahd;

    move-result-object v5

    .line 624
    invoke-static {v5}, Lyz;->a(Lahd;)Ljava/lang/String;

    move-result-object v4

    .line 625
    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 629
    :goto_2
    invoke-interface {v14, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 630
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "already saved widget "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 652
    :catchall_0
    move-exception v4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v4

    .line 627
    :cond_3
    :try_start_1
    const-string v16, "LauncherBackupHelper"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "empty intent on appwidget: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v16

    invoke-static {v0, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 634
    :cond_4
    if-eqz v4, :cond_2

    .line 636
    move-object/from16 v0, p3

    iget v8, v0, Lahc;->Yb:I

    sub-int/2addr v8, v15

    const/4 v9, 0x5

    if-ge v8, v9, :cond_8

    .line 637
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "saving widget "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    iget v4, v13, Ltu;->DL:I

    mul-int v4, v4, v17

    iget v8, v13, Ltu;->DM:I

    mul-int v8, v8, v18

    invoke-virtual {v10, v4, v8, v11}, Lafb;->a(IILaco;)V

    .line 640
    sget-object v8, Lyz;->xn:Lwi;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lyz;->b(Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v9

    new-instance v16, Lahg;

    invoke-direct/range {v16 .. v16}, Lahg;-><init>()V

    invoke-virtual/range {v19 .. v19}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    iput-object v4, v0, Lahg;->Yh:Ljava/lang/String;

    iget-object v4, v9, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v4, v0, Lahg;->label:Ljava/lang/String;

    iget-object v4, v9, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    :goto_3
    move-object/from16 v0, v16

    iput-boolean v4, v0, Lahg;->Yi:Z

    iget v4, v9, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    if-eqz v4, :cond_5

    new-instance v4, Lahe;

    invoke-direct {v4}, Lahe;-><init>()V

    move-object/from16 v0, v16

    iput-object v4, v0, Lahg;->Yj:Lahe;

    invoke-virtual/range {v19 .. v19}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget v0, v9, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v4, v0}, Lwi;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lyz;->mContext:Landroid/content/Context;

    invoke-static {v4, v8}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v17, Lyz;->Mv:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v18, 0x4b

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, v16

    iget-object v4, v0, Lahg;->Yj:Lahe;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    iput-object v8, v4, Lahe;->Yf:[B

    move-object/from16 v0, v16

    iget-object v4, v0, Lahg;->Yj:Lahe;

    iput v12, v4, Lahe;->Ye:I

    :cond_5
    iget v4, v9, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    if-eqz v4, :cond_6

    new-instance v4, Lahe;

    invoke-direct {v4}, Lahe;-><init>()V

    move-object/from16 v0, v16

    iput-object v4, v0, Lahg;->Yk:Lahe;

    const/4 v4, 0x0

    invoke-virtual {v10, v9, v4}, Lafb;->a(Landroid/appwidget/AppWidgetProviderInfo;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v9, Lyz;->Mv:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v17, 0x4b

    move/from16 v0, v17

    invoke-virtual {v4, v9, v0, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v0, v16

    iget-object v4, v0, Lahg;->Yk:Lahe;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    iput-object v8, v4, Lahe;->Yf:[B

    move-object/from16 v0, v16

    iget-object v4, v0, Lahg;->Yk:Lahe;

    iput v12, v4, Lahe;->Ye:I

    :cond_6
    invoke-static/range {v16 .. v16}, Lyz;->a(Ljsr;)[B

    move-result-object v4

    .line 641
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct {v0, v5, v4, v1, v2}, Lyz;->a(Lahd;[BLahc;Landroid/app/backup/BackupDataOutput;)V

    goto/16 :goto_1

    .line 640
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 645
    :cond_8
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "deferring widget backup "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 647
    invoke-direct/range {p0 .. p0}, Lyz;->dataChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 652
    :cond_9
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 657
    invoke-interface {v14, v7}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 658
    move-object/from16 v0, p3

    iget v4, v0, Lahc;->Yb:I

    move-object/from16 v0, p2

    invoke-static {v14, v0}, Lyz;->a(Ljava/util/Set;Landroid/app/backup/BackupDataOutput;)I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p3

    iput v4, v0, Lahc;->Yb:I

    goto/16 :goto_0
.end method

.method private dataChanged()V
    .locals 2

    .prologue
    .line 160
    sget-object v0, Lyz;->Mu:Landroid/app/backup/BackupManager;

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Landroid/app/backup/BackupManager;

    iget-object v1, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lyz;->Mu:Landroid/app/backup/BackupManager;

    .line 163
    :cond_0
    sget-object v0, Lyz;->Mu:Landroid/app/backup/BackupManager;

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->dataChanged()V

    .line 164
    return-void
.end method

.method private e(Landroid/database/Cursor;)[B
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 785
    new-instance v1, Lahb;

    invoke-direct {v1}, Lahb;-><init>()V

    .line 786
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lahb;->id:J

    .line 787
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->XP:I

    .line 788
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->XO:I

    .line 789
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->Bb:I

    .line 790
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->Bc:I

    .line 791
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->AY:I

    .line 792
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->AZ:I

    .line 793
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->XU:I

    .line 794
    iget v0, v1, Lahb;->XU:I

    if-nez v0, :cond_1

    .line 795
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 796
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 797
    iput-object v0, v1, Lahb;->XV:Ljava/lang/String;

    .line 799
    :cond_0
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 800
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 801
    iput-object v0, v1, Lahb;->XW:Ljava/lang/String;

    .line 804
    :cond_1
    iget v0, v1, Lahb;->XU:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 805
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 806
    if-eqz v0, :cond_2

    array-length v2, v0

    if-lez v2, :cond_2

    .line 807
    iput-object v0, v1, Lahb;->XX:[B

    .line 810
    :cond_2
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 811
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 812
    iput-object v0, v1, Lahb;->XN:Ljava/lang/String;

    .line 814
    :cond_3
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 815
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 817
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 818
    const-string v2, "profile"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 819
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lahb;->XS:Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 824
    :cond_4
    :goto_0
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->Jz:I

    .line 825
    iget v0, v1, Lahb;->Jz:I

    if-ne v0, v4, :cond_5

    .line 826
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lahb;->LT:I

    .line 827
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 828
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 829
    iput-object v0, v1, Lahb;->XR:Ljava/lang/String;

    .line 833
    :cond_5
    invoke-static {v1}, Lyz;->a(Ljsr;)[B

    move-result-object v0

    return-object v0

    .line 820
    :catch_0
    move-exception v0

    .line 821
    const-string v2, "LauncherBackupHelper"

    const-string v3, "Invalid intent"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static io()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1148
    sget-object v2, Lyz;->xn:Lwi;

    if-eqz v2, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return v0

    .line 1152
    :cond_1
    sget-object v2, Lyu;->Mj:Lyu;

    .line 1153
    if-nez v2, :cond_2

    .line 1154
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 1155
    invoke-virtual {v0}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    .line 1156
    const-string v2, "LauncherBackupHelper"

    const-string v3, "Failed to get app state during backup/restore"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 1157
    goto :goto_0

    .line 1159
    :cond_2
    iget-object v2, v2, Lyu;->xn:Lwi;

    .line 1160
    sput-object v2, Lyz;->xn:Lwi;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private ip()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "profileId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-static {v1}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v1

    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v2

    invoke-virtual {v1, v2}, Laia;->c(Lahz;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private t(Ljava/lang/String;)Lahd;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 734
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {v0}, Lahd;->c([B)Lahd;

    move-result-object v0

    .line 735
    iget-wide v2, v0, Lahd;->XM:J

    invoke-static {v0}, Lyz;->b(Lahd;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 736
    new-instance v0, Lza;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid key read from stream"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lza;-><init>(Lyz;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 740
    :catch_0
    move-exception v0

    .line 741
    new-instance v1, Lza;

    invoke-direct {v1, p0, v0, v6}, Lza;-><init>(Lyz;Ljava/lang/Throwable;B)V

    throw v1

    .line 742
    :catch_1
    move-exception v0

    .line 743
    new-instance v1, Lza;

    invoke-direct {v1, p0, v0, v6}, Lza;-><init>(Lyz;Ljava/lang/Throwable;B)V

    throw v1

    .line 739
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final performBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 181
    invoke-static {p1}, Lyz;->a(Landroid/os/ParcelFileDescriptor;)Lahc;

    move-result-object v7

    .line 184
    new-instance v6, Lahc;

    invoke-direct {v6}, Lahc;-><init>()V

    .line 186
    iget-wide v0, v7, Lahc;->XZ:J

    .line 187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v6, Lahc;->XZ:J

    .line 188
    iput v8, v6, Lahc;->Yb:I

    .line 189
    const-wide/16 v4, 0x0

    iput-wide v4, v6, Lahc;->Ya:J

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "lastBackupTime = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 193
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 194
    iget-object v0, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Labn;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lyz;->Mw:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    if-eqz v0, :cond_2

    .line 196
    :try_start_0
    invoke-direct {p0, v7, p2, v6, v9}, Lyz;->a(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V

    .line 197
    invoke-direct {p0, v7, p2, v6, v9}, Lyz;->b(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V

    .line 198
    invoke-direct {p0, v7, p2, v6, v9}, Lyz;->c(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V

    .line 199
    invoke-direct {p0, v7, p2, v6, v9}, Lyz;->d(Lahc;Landroid/app/backup/BackupDataOutput;Lahc;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lahd;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lahd;

    iput-object v0, v6, Lahc;->Yc:[Lahd;

    move-object v0, v6

    .line 208
    :goto_2
    invoke-direct {p0, p3, v0}, Lyz;->a(Landroid/os/ParcelFileDescriptor;Lahc;)V

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onBackup: wrote "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v0, Lahc;->Ya:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "b in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Lahc;->Yb:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rows."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    return-void

    .line 194
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-static {}, Lyz;->io()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lyz;->dataChanged()V

    move v0, v8

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    const-string v1, "LauncherBackupHelper"

    const-string v2, "launcher backup has failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    move-object v0, v7

    .line 205
    goto :goto_2
.end method

.method public final restoreEntity(Landroid/app/backup/BackupDataInputStream;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 221
    const/16 v0, 0x200

    new-array v0, v0, [B

    .line 223
    invoke-virtual {p1}, Landroid/app/backup/BackupDataInputStream;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 224
    invoke-virtual {p1}, Landroid/app/backup/BackupDataInputStream;->size()I

    move-result v2

    .line 225
    array-length v3, v0

    if-ge v3, v2, :cond_0

    .line 226
    new-array v0, v2, [B

    .line 228
    :cond_0
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v0, v3, v2}, Landroid/app/backup/BackupDataInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :goto_0
    :try_start_1
    invoke-direct {p0, v1}, Lyz;->t(Ljava/lang/String;)Lahd;

    move-result-object v3

    .line 238
    iget-object v4, p0, Lyz;->MA:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    iget v4, v3, Lahd;->type:I

    packed-switch v4, :pswitch_data_0

    .line 257
    const-string v0, "LauncherBackupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "unknown restore entity type: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v3, Lahd;->type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lza; {:try_start_1 .. :try_end_1} :catch_2

    .line 264
    :cond_1
    :goto_1
    return-void

    .line 233
    :catch_0
    move-exception v3

    .line 234
    const-string v4, "LauncherBackupHelper"

    const-string v5, "failed to read entity from restore data"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 241
    :pswitch_0
    :try_start_2
    iget-object v4, p0, Lyz;->MA:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unpacking favorite "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v3, Lahd;->id:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lyz;->My:Z
    :try_end_2
    .catch Lza; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v3, :cond_1

    :try_start_3
    iget-object v3, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Lahb;

    invoke-direct {v4}, Lahb;-><init>()V

    const/4 v5, 0x0

    invoke-static {v0, v5, v2}, Lyz;->b([BII)[B

    move-result-object v0

    invoke-static {v4, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unpacked favorite "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v4, Lahb;->Jz:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v4, Lahb;->XN:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-wide v6, v4, Lahb;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "_id"

    iget-wide v6, v4, Lahb;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "screen"

    iget v5, v4, Lahb;->XP:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "container"

    iget v5, v4, Lahb;->XO:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cellX"

    iget v5, v4, Lahb;->Bb:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cellY"

    iget v5, v4, Lahb;->Bc:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "spanX"

    iget v5, v4, Lahb;->AY:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "spanY"

    iget v5, v4, Lahb;->AZ:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "iconType"

    iget v5, v4, Lahb;->XU:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget v2, v4, Lahb;->XU:I

    if-nez v2, :cond_2

    const-string v2, "iconPackage"

    iget-object v5, v4, Lahb;->XV:Ljava/lang/String;

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "iconResource"

    iget-object v5, v4, Lahb;->XW:Ljava/lang/String;

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget v2, v4, Lahb;->XU:I

    if-ne v2, v8, :cond_3

    const-string v2, "icon"

    iget-object v5, v4, Lahb;->XX:[B

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_3
    iget-object v2, v4, Lahb;->XN:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "title"

    iget-object v5, v4, Lahb;->XN:Ljava/lang/String;

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    iget-object v2, v4, Lahb;->XS:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "intent"

    iget-object v5, v4, Lahb;->XS:Ljava/lang/String;

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v2, "itemType"

    iget v5, v4, Lahb;->Jz:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v2

    iget-object v5, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-static {v5}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v5

    invoke-virtual {v5, v2}, Laia;->c(Lahz;)J

    move-result-wide v6

    const-string v2, "profileId"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget v2, v4, Lahb;->Jz:I

    const/4 v5, 0x4

    if-ne v2, v5, :cond_8

    iget-object v2, v4, Lahb;->XR:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "appWidgetProvider"

    iget-object v5, v4, Lahb;->XR:Ljava/lang/String;

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v2, "appWidgetId"

    iget v4, v4, Lahb;->LT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "restored"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_4
    sget-object v2, Labn;->OT:Landroid/net/Uri;

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_3
    .catch Ljsq; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lza; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    :catch_1
    move-exception v0

    :try_start_4
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to decode favorite"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Lza; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 261
    :catch_2
    move-exception v0

    const-string v0, "LauncherBackupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ignoring unparsable backup key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 241
    :cond_6
    :try_start_5
    iget-object v0, v4, Lahb;->XN:Ljava/lang/String;

    goto/16 :goto_2

    :cond_7
    const-string v2, "title"

    const-string v5, ""

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_8
    const-string v2, "restored"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_5
    .catch Ljsq; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lza; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_4

    .line 245
    :pswitch_1
    :try_start_6
    iget-object v4, p0, Lyz;->MA:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unpacking screen "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v3, Lahd;->id:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lyz;->My:Z
    :try_end_6
    .catch Lza; {:try_start_6 .. :try_end_6} :catch_2

    if-eqz v3, :cond_1

    :try_start_7
    iget-object v3, p0, Lyz;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Lahf;

    invoke-direct {v4}, Lahf;-><init>()V

    const/4 v5, 0x0

    invoke-static {v0, v5, v2}, Lyz;->b([BII)[B

    move-result-object v0

    invoke-static {v4, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unpacked screen "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v4, Lahf;->id:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v4, Lahf;->Yg:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "_id"

    iget-wide v6, v4, Lahf;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "screenRank"

    iget v4, v4, Lahf;->Yg:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Labo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_7
    .catch Ljsq; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lza; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_1

    :catch_3
    move-exception v0

    :try_start_8
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to decode screen"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 249
    :pswitch_2
    iget-object v4, p0, Lyz;->MA:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unpacking icon "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v3, Lahd;->id:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_8
    .catch Lza; {:try_start_8 .. :try_end_8} :catch_2

    :try_start_9
    new-instance v4, Lahe;

    invoke-direct {v4}, Lahe;-><init>()V

    const/4 v5, 0x0

    invoke-static {v0, v5, v2}, Lyz;->b([BII)[B

    move-result-object v0

    invoke-static {v4, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unpacked icon "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v4, Lahe;->Ye:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v4, Lahe;->Yf:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v0, v4, Lahe;->Yf:[B

    const/4 v2, 0x0

    iget-object v5, v4, Lahe;->Yf:[B

    array-length v5, v5

    invoke-static {v0, v2, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_9

    const-string v2, "LauncherBackupHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to unpack icon for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lahd;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-boolean v2, p0, Lyz;->My:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "saving restored icon as: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v3, Lahd;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lyz;->mContext:Landroid/content/Context;

    iget-object v5, v3, Lahd;->name:Ljava/lang/String;

    invoke-static {v5}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    iget v4, v4, Lahe;->Ye:I

    invoke-static {v2, v5, v0}, Lwi;->a(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Bitmap;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Lza; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_1

    :catch_4
    move-exception v0

    :try_start_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "failed to save restored icon for: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lahd;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 253
    :pswitch_3
    iget-object v4, p0, Lyz;->MA:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unpacking widget "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v3, Lahd;->id:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_a
    .catch Lza; {:try_start_a .. :try_end_a} :catch_2

    :try_start_b
    new-instance v4, Lahg;

    invoke-direct {v4}, Lahg;-><init>()V

    const/4 v5, 0x0

    invoke-static {v0, v5, v2}, Lyz;->b([BII)[B

    move-result-object v0

    invoke-static {v4, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unpacked widget "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v4, Lahg;->Yh:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v4, Lahg;->Yj:Lahe;

    iget-object v0, v0, Lahe;->Yf:[B

    if-eqz v0, :cond_a

    iget-object v0, v4, Lahg;->Yj:Lahe;

    iget-object v0, v0, Lahe;->Yf:[B

    const/4 v2, 0x0

    iget-object v5, v4, Lahg;->Yj:Lahe;

    iget-object v5, v5, Lahe;->Yf:[B

    array-length v5, v5

    invoke-static {v0, v2, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_b

    const-string v0, "LauncherBackupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "failed to unpack widget icon for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lahd;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    :goto_5
    iget-boolean v0, p0, Lyz;->My:Z

    if-nez v0, :cond_1

    goto/16 :goto_1

    :cond_b
    iget-object v2, p0, Lyz;->mContext:Landroid/content/Context;

    iget-object v3, v4, Lahg;->Yh:Ljava/lang/String;

    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    iget-object v4, v4, Lahg;->Yj:Lahe;

    iget v4, v4, Lahe;->Ye:I

    invoke-static {v2, v3, v0}, Lwi;->a(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Bitmap;)V
    :try_end_b
    .catch Ljsq; {:try_start_b .. :try_end_b} :catch_5
    .catch Lza; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_5

    :catch_5
    move-exception v0

    :try_start_c
    const-string v2, "LauncherBackupHelper"

    const-string v3, "failed to decode widget"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catch Lza; {:try_start_c .. :try_end_c} :catch_2

    goto/16 :goto_1

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V
    .locals 4

    .prologue
    .line 275
    new-instance v1, Lahc;

    invoke-direct {v1}, Lahc;-><init>()V

    .line 276
    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lahc;->XZ:J

    .line 277
    iget-object v0, p0, Lyz;->MA:Ljava/util/ArrayList;

    iget-object v2, p0, Lyz;->MA:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lahd;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lahd;

    iput-object v0, v1, Lahc;->Yc:[Lahd;

    .line 278
    invoke-direct {p0, p1, v1}, Lyz;->a(Landroid/os/ParcelFileDescriptor;Lahc;)V

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onRestore: read "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lyz;->MA:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rows"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    iget-object v0, p0, Lyz;->MA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 281
    return-void
.end method
