.class public final Lftf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cBJ:[Liyg;

.field private final clu:Ljak;

.field private final mCardContainer:Lfmt;

.field private final mEntry:Lizj;

.field private final mFrequentPlaceEntry:Ljal;

.field private final mStringEvaluator:Lgbr;


# direct methods
.method public constructor <init>(Ljal;Lizj;Lfmt;Lgbr;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p2, p0, Lftf;->mEntry:Lizj;

    .line 41
    iput-object p1, p0, Lftf;->mFrequentPlaceEntry:Ljal;

    .line 42
    iget-object v0, p0, Lftf;->mFrequentPlaceEntry:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iput-object v0, p0, Lftf;->clu:Ljak;

    .line 43
    iget-object v0, p0, Lftf;->mFrequentPlaceEntry:Ljal;

    iget-object v0, v0, Ljal;->dMF:[Liyg;

    iput-object v0, p0, Lftf;->cBJ:[Liyg;

    .line 44
    iput-object p3, p0, Lftf;->mCardContainer:Lfmt;

    .line 45
    iput-object p4, p0, Lftf;->mStringEvaluator:Lgbr;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lemp;Lfyk;)Lanh;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lftf;->clu:Ljak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lftf;->clu:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lftf;->cBJ:[Liyg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lftf;->cBJ:[Liyg;

    array-length v0, v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lftf;->cBJ:[Liyg;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Liyg;->aEz()I

    move-result v0

    if-eq v0, v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 85
    :goto_0
    if-eqz v0, :cond_3

    .line 88
    :goto_1
    return-object v0

    .line 84
    :cond_1
    new-instance v0, Lgca;

    iget-object v2, p0, Lftf;->cBJ:[Liyg;

    aget-object v2, v2, v3

    invoke-direct {v0, v2, p2}, Lgca;-><init>(Liyg;Lemp;)V

    iget-object v2, p0, Lftf;->mCardContainer:Lfmt;

    invoke-interface {v2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgca;->p(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v2, Lfzb;

    iget-object v3, p0, Lftf;->mStringEvaluator:Lgbr;

    invoke-direct {v2, p2, v0, v3}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    iget-object v0, p0, Lftf;->mEntry:Lizj;

    iget-object v3, p0, Lftf;->mCardContainer:Lfmt;

    iget-object v4, p0, Lftf;->clu:Ljak;

    iget-object v4, v4, Ljak;->aeB:Ljbp;

    invoke-virtual {v2, p1, v0, v3, v4}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Lftf;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUv:Ljhk;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lftf;->clu:Ljak;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lftf;->clu:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lftf;->cBJ:[Liyg;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lftf;->cBJ:[Liyg;

    array-length v0, v0

    if-nez v0, :cond_5

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lftf;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v3, p0, Lftf;->mEntry:Lizj;

    iget-object v4, p0, Lftf;->cBJ:[Liyg;

    iget-object v0, p0, Lftf;->clu:Ljak;

    iget-object v5, v0, Ljak;->aeB:Ljbp;

    move-object v0, p3

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;)Lanh;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lfmt;Z)Lanh;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 129
    iget-object v1, p0, Lftf;->clu:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-object v0

    .line 131
    :cond_0
    iget-object v1, p0, Lftf;->cBJ:[Liyg;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v0, p0, Lftf;->cBJ:[Liyg;

    aget-object v0, v0, v6

    .line 132
    :cond_1
    new-instance v1, Lfvv;

    iget-object v2, p0, Lftf;->mEntry:Lizj;

    const/16 v3, 0xba

    iget-object v4, p0, Lftf;->clu:Ljak;

    iget-object v4, v4, Ljak;->aeB:Ljbp;

    invoke-direct {v1, v2, v3, v0, v4}, Lfvv;-><init>(Lizj;ILiyg;Ljbp;)V

    iput-boolean v6, v1, Lfvv;->cCH:Z

    .line 139
    if-eqz p3, :cond_2

    .line 140
    iget-object v0, p0, Lftf;->clu:Ljak;

    invoke-static {p1, v0}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 142
    iput-object v0, v1, Lfvv;->mName:Ljava/lang/String;

    .line 146
    :cond_2
    invoke-virtual {v1}, Lfvv;->aDh()Lanh;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_3

    iget-object v0, p0, Lftf;->clu:Ljak;

    iget-object v0, v0, Ljak;->clz:Ljcu;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lftf;->clu:Ljak;

    iget-object v0, v0, Ljak;->clz:Ljcu;

    iget-object v0, v0, Ljcu;->dQU:Liym;

    if-eqz v0, :cond_3

    .line 153
    iget-object v0, p0, Lftf;->clu:Ljak;

    iget-object v0, v0, Ljak;->clz:Ljcu;

    iget-object v0, v0, Ljcu;->dQU:Liym;

    .line 154
    iget-object v2, v0, Liym;->dQL:[Ljcm;

    array-length v2, v2

    if-lez v2, :cond_3

    iget-object v2, v0, Liym;->dQL:[Ljcm;

    aget-object v2, v2, v6

    invoke-virtual {v2}, Ljcm;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 155
    iget-object v2, v0, Liym;->dQL:[Ljcm;

    aget-object v2, v2, v6

    invoke-virtual {v2}, Ljcm;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-virtual {v0}, Liym;->oK()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Liym;->getName()Ljava/lang/String;

    move-result-object v0

    .line 158
    :goto_1
    new-instance v3, Lfsa;

    const/16 v4, 0x3b

    invoke-direct {v3, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f0200f7

    invoke-virtual {v3, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    const v4, 0x7f0a0466

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lfsa;->cBy:Ljava/lang/String;

    invoke-virtual {v3, v2, p2}, Lfsa;->a(Ljava/lang/String;Lfmt;)Lani;

    move-result-object v0

    iput-object v0, v1, Lanh;->aht:Lani;

    :cond_3
    move-object v0, v1

    .line 166
    goto/16 :goto_0

    .line 156
    :cond_4
    iget-object v0, p0, Lftf;->clu:Ljak;

    invoke-static {p1, v0}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final aDp()Lanh;
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lftf;->clu:Ljak;

    invoke-static {v0}, Lgbf;->a(Ljak;)Ljava/lang/String;

    move-result-object v1

    .line 95
    if-nez v1, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 104
    :goto_0
    return-object v0

    .line 98
    :cond_0
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 99
    iget-object v2, p0, Lftf;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 100
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 101
    new-instance v2, Lanz;

    invoke-direct {v2}, Lanz;-><init>()V

    iput-object v2, v0, Lanh;->agy:Lanz;

    .line 102
    iget-object v2, v0, Lanh;->agy:Lanz;

    new-instance v3, Laoi;

    invoke-direct {v3}, Laoi;-><init>()V

    iput-object v3, v2, Lanz;->aiU:Laoi;

    .line 103
    iget-object v2, v0, Lanh;->agy:Lanz;

    iget-object v2, v2, Lanz;->aiU:Laoi;

    invoke-virtual {v2, v1}, Laoi;->bQ(Ljava/lang/String;)Laoi;

    goto :goto_0
.end method

.method public final aR(Landroid/content/Context;)Lanh;
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lftf;->clu:Ljak;

    invoke-static {p1, v0}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v1

    .line 110
    if-nez v1, :cond_0

    .line 111
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    .line 113
    :cond_0
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 114
    iget-object v2, p0, Lftf;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 115
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 116
    new-instance v2, Lany;

    invoke-direct {v2}, Lany;-><init>()V

    iput-object v2, v0, Lanh;->agw:Lany;

    .line 117
    iget-object v2, v0, Lanh;->agw:Lany;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lany;->bh(Ljava/lang/String;)Lany;

    goto :goto_0
.end method
