.class public final Lfen;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private final aNB:Landroid/os/PowerManager$WakeLock;

.field private final aWa:Landroid/app/PendingIntent;

.field private final cpP:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Landroid/os/PowerManager$WakeLock;Landroid/app/PendingIntent;Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;)V
    .locals 1

    .prologue
    .line 320
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 321
    iput-object p2, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    .line 322
    iput-object p3, p0, Lfen;->aWa:Landroid/app/PendingIntent;

    .line 323
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lfen;->cpP:Ljava/lang/ref/WeakReference;

    .line 324
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 331
    :try_start_0
    iget-object v0, p0, Lfen;->aWa:Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 358
    iget-object v0, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    :try_start_1
    iget-object v0, p0, Lfen;->aWa:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 350
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_2

    .line 351
    iget-object v0, p0, Lfen;->cpP:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    .line 352
    if-eqz v0, :cond_2

    .line 353
    invoke-virtual {v0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayK()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    :cond_2
    iget-object v0, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 358
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 359
    iget-object v1, p0, Lfen;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_3
    throw v0
.end method
