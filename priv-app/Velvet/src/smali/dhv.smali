.class public final Ldhv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aVQ:Ldgc;

.field private final byq:Ljava/util/Set;

.field private final mConfig:Lcjs;

.field private final mContext:Landroid/content/Context;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcjs;Ldgc;Lcke;)V
    .locals 6

    .prologue
    .line 47
    invoke-static {p2}, Lcum;->a(Lcjs;)Ljava/util/Set;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Ldhv;-><init>(Landroid/content/Context;Lcjs;Ldgc;Ljava/util/Set;Lcke;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcjs;Ldgc;Ljava/util/Set;Lcke;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Ldhv;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Ldhv;->mConfig:Lcjs;

    .line 57
    iput-object p3, p0, Ldhv;->aVQ:Ldgc;

    .line 58
    iput-object p4, p0, Ldhv;->byq:Ljava/util/Set;

    .line 59
    iput-object p5, p0, Ldhv;->mSearchSettings:Lcke;

    .line 60
    return-void
.end method

.method private static a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;)Ljava/util/Set;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    if-nez v0, :cond_1

    .line 129
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 138
    :cond_0
    return-object v0

    .line 132
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 133
    iget-object v5, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v7, v5, v4

    .line 134
    iget-object v1, v7, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->avn:[Lcom/google/android/gms/appdatasearch/Feature;

    invoke-static {v2, v1}, Lcom/google/android/gms/appdatasearch/Feature;->a(I[Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    .line 135
    iget-object v1, v7, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->auC:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_3
    move v1, v3

    .line 134
    goto :goto_1
.end method

.method private jG(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 167
    :try_start_0
    iget-object v0, p0, Ldhv;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    .line 168
    :catch_0
    move-exception v0

    .line 169
    const-string v1, "IcingSourcesFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not get application info for package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 170
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final varargs a([Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;)Ljava/util/Collection;
    .locals 22

    .prologue
    .line 66
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v2, v0

    if-nez v2, :cond_1

    .line 67
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 124
    :goto_0
    return-object v2

    .line 70
    :cond_1
    move-object/from16 v0, p1

    array-length v2, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Ldhv;->byq:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v17

    .line 73
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    const/4 v2, 0x0

    move/from16 v18, v2

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_8

    aget-object v20, p1, v18

    .line 74
    if-nez v20, :cond_3

    .line 75
    const-string v2, "IcingSourcesFactory"

    const-string v3, "GlobalSearchSource array contained null value"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 73
    :cond_2
    :goto_2
    add-int/lit8 v2, v18, 0x1

    move/from16 v18, v2

    goto :goto_1

    .line 79
    :cond_3
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->packageName:Ljava/lang/String;

    .line 80
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 81
    const-string v2, "IcingSourcesFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "no packagename set in global search app info: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 85
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Ldhv;->mConfig:Lcjs;

    invoke-virtual {v3, v2}, Lcjs;->gO(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 86
    const-string v3, "IcingSourcesFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ignoring icing source "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x4

    invoke-static {v5, v3, v2, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 90
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldhv;->jG(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 91
    if-nez v4, :cond_6

    .line 92
    const-string v3, "IcingSourcesFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "could not find application info for package "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v3, v2, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 96
    :cond_6
    invoke-static/range {v20 .. v20}, Ldhv;->a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;)Ljava/util/Set;

    move-result-object v15

    .line 98
    move-object/from16 v0, p0

    iget-object v3, v0, Ldhv;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Ldhv;->byq:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_3
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcum;

    .line 100
    move-object/from16 v0, p0

    iget-object v3, v0, Ldhv;->aVQ:Ldgc;

    invoke-virtual {v3, v2}, Ldgc;->a(Lcum;)Ljava/lang/String;

    move-result-object v3

    .line 102
    move-object/from16 v0, p0

    iget-object v5, v0, Ldhv;->aVQ:Ldgc;

    invoke-virtual {v5, v2}, Ldgc;->b(Lcum;)Ljava/lang/String;

    move-result-object v13

    .line 104
    invoke-virtual {v2}, Lcum;->SY()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v11

    invoke-virtual {v2}, Lcum;->xF()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Ldhv;->mConfig:Lcjs;

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcjs;->gN(Ljava/lang/String;)Z

    move-result v12

    new-instance v2, Ldht;

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->packageName:Ljava/lang/String;

    iget v6, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->labelId:I

    iget v7, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avi:I

    iget v8, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->iconId:I

    iget-object v9, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avj:Ljava/lang/String;

    iget-object v10, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avk:Ljava/lang/String;

    iget-object v11, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avl:Ljava/lang/String;

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->enabled:Z

    move/from16 v16, v0

    invoke-direct/range {v2 .. v16}, Ldht;-><init>(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Ldhv;->mSearchSettings:Lcke;

    invoke-interface {v3, v2}, Lcke;->c(Ldgb;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ldht;->setEnabled(Z)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 112
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Ldhv;->aVQ:Ldgc;

    invoke-static {v2}, Ldgc;->jA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 114
    move-object/from16 v0, p0

    iget-object v5, v0, Ldhv;->aVQ:Ldgc;

    invoke-static {v2}, Ldgc;->jC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Ldhv;->mConfig:Lcjs;

    iget-object v5, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcjs;->gN(Ljava/lang/String;)Z

    move-result v12

    new-instance v2, Ldht;

    iget-object v5, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v20

    iget v6, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->labelId:I

    move-object/from16 v0, v20

    iget v7, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avi:I

    move-object/from16 v0, v20

    iget v8, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->iconId:I

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avj:Ljava/lang/String;

    move-object/from16 v0, v20

    iget-object v10, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avk:Ljava/lang/String;

    move-object/from16 v0, v20

    iget-object v11, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avl:Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->enabled:Z

    move/from16 v16, v0

    invoke-direct/range {v2 .. v16}, Ldht;-><init>(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;Z)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_8
    move-object/from16 v2, v17

    .line 124
    goto/16 :goto_0
.end method
