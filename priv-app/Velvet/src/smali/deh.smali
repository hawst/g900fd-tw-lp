.class final Ldeh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final bvk:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean p1, p0, Ldeh;->bvk:Z

    .line 55
    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/shared/search/Suggestion;

    check-cast p2, Lcom/google/android/shared/search/Suggestion;

    if-nez p1, :cond_2

    if-nez p2, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Ldeh;->bvk:Z

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/android/shared/search/Suggestion;->asd()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asd()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    if-nez v0, :cond_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lesp;->k(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arZ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Suggestion;->arZ()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lesp;->k(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method
