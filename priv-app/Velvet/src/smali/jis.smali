.class public final Ljis;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile enP:[Ljis;


# instance fields
.field private aez:I

.field private afW:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private akf:Ljava/lang/String;

.field private alN:Ljava/lang/String;

.field private enQ:I

.field private enR:I

.field private enS:I

.field private enT:I

.field private enU:I

.field private enV:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42241
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 42242
    iput v1, p0, Ljis;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljis;->akf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljis;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljis;->afW:Ljava/lang/String;

    iput v1, p0, Ljis;->enQ:I

    iput v1, p0, Ljis;->enR:I

    const/4 v0, 0x1

    iput v0, p0, Ljis;->enS:I

    iput v1, p0, Ljis;->enT:I

    const/4 v0, 0x2

    iput v0, p0, Ljis;->enU:I

    const-string v0, ""

    iput-object v0, p0, Ljis;->alN:Ljava/lang/String;

    iput v1, p0, Ljis;->enV:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljis;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljis;->eCz:I

    .line 42243
    return-void
.end method

.method public static bnr()[Ljis;
    .locals 2

    .prologue
    .line 42026
    sget-object v0, Ljis;->enP:[Ljis;

    if-nez v0, :cond_1

    .line 42027
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 42029
    :try_start_0
    sget-object v0, Ljis;->enP:[Ljis;

    if-nez v0, :cond_0

    .line 42030
    const/4 v0, 0x0

    new-array v0, v0, [Ljis;

    sput-object v0, Ljis;->enP:[Ljis;

    .line 42032
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42034
    :cond_1
    sget-object v0, Ljis;->enP:[Ljis;

    return-object v0

    .line 42032
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 42011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljis;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljis;->akf:Ljava/lang/String;

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljis;->aiK:Ljava/lang/String;

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljis;->afW:Ljava/lang/String;

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljis;->enQ:I

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljis;->enR:I

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljis;->enS:I

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljis;->enT:I

    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljis;->enU:I

    iget v0, p0, Ljis;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljis;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljis;->alN:Ljava/lang/String;

    iget v0, p0, Ljis;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljis;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljis;->enV:I

    iget v0, p0, Ljis;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljis;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 42265
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 42266
    const/4 v0, 0x1

    iget-object v1, p0, Ljis;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 42268
    :cond_0
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 42269
    const/4 v0, 0x2

    iget-object v1, p0, Ljis;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 42271
    :cond_1
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 42272
    const/4 v0, 0x3

    iget-object v1, p0, Ljis;->afW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 42274
    :cond_2
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 42275
    const/4 v0, 0x4

    iget v1, p0, Ljis;->enQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 42277
    :cond_3
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 42278
    const/4 v0, 0x5

    iget v1, p0, Ljis;->enR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 42280
    :cond_4
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 42281
    const/4 v0, 0x6

    iget v1, p0, Ljis;->enS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 42283
    :cond_5
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 42284
    const/4 v0, 0x7

    iget v1, p0, Ljis;->enT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 42286
    :cond_6
    iget v0, p0, Ljis;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 42287
    const/16 v0, 0x8

    iget v1, p0, Ljis;->enU:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 42289
    :cond_7
    iget v0, p0, Ljis;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    .line 42290
    const/16 v0, 0x9

    iget-object v1, p0, Ljis;->alN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 42292
    :cond_8
    iget v0, p0, Ljis;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_9

    .line 42293
    const/16 v0, 0xa

    iget v1, p0, Ljis;->enV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 42295
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 42296
    return-void
.end method

.method public final bns()I
    .locals 1

    .prologue
    .line 42108
    iget v0, p0, Ljis;->enQ:I

    return v0
.end method

.method public final bnt()Z
    .locals 1

    .prologue
    .line 42116
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bnu()I
    .locals 1

    .prologue
    .line 42127
    iget v0, p0, Ljis;->enR:I

    return v0
.end method

.method public final bnv()I
    .locals 1

    .prologue
    .line 42165
    iget v0, p0, Ljis;->enT:I

    return v0
.end method

.method public final bnw()Z
    .locals 1

    .prologue
    .line 42173
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bnx()I
    .locals 1

    .prologue
    .line 42184
    iget v0, p0, Ljis;->enU:I

    return v0
.end method

.method public final bny()I
    .locals 1

    .prologue
    .line 42225
    iget v0, p0, Ljis;->enV:I

    return v0
.end method

.method public final bnz()Z
    .locals 1

    .prologue
    .line 42233
    iget v0, p0, Ljis;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42064
    iget-object v0, p0, Ljis;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42042
    iget-object v0, p0, Ljis;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 42300
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 42301
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 42302
    const/4 v1, 0x1

    iget-object v2, p0, Ljis;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42305
    :cond_0
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 42306
    const/4 v1, 0x2

    iget-object v2, p0, Ljis;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42309
    :cond_1
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 42310
    const/4 v1, 0x3

    iget-object v2, p0, Ljis;->afW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42313
    :cond_2
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 42314
    const/4 v1, 0x4

    iget v2, p0, Ljis;->enQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42317
    :cond_3
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 42318
    const/4 v1, 0x5

    iget v2, p0, Ljis;->enR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42321
    :cond_4
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 42322
    const/4 v1, 0x6

    iget v2, p0, Ljis;->enS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42325
    :cond_5
    iget v1, p0, Ljis;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 42326
    const/4 v1, 0x7

    iget v2, p0, Ljis;->enT:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42329
    :cond_6
    iget v1, p0, Ljis;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 42330
    const/16 v1, 0x8

    iget v2, p0, Ljis;->enU:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42333
    :cond_7
    iget v1, p0, Ljis;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    .line 42334
    const/16 v1, 0x9

    iget-object v2, p0, Ljis;->alN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42337
    :cond_8
    iget v1, p0, Ljis;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    .line 42338
    const/16 v1, 0xa

    iget v2, p0, Ljis;->enV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42341
    :cond_9
    return v0
.end method

.method public final oj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42086
    iget-object v0, p0, Ljis;->afW:Ljava/lang/String;

    return-object v0
.end method

.method public final ok()Z
    .locals 1

    .prologue
    .line 42097
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pX()Z
    .locals 1

    .prologue
    .line 42075
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qd(I)Ljis;
    .locals 1

    .prologue
    .line 42111
    iput p1, p0, Ljis;->enQ:I

    .line 42112
    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljis;->aez:I

    .line 42113
    return-object p0
.end method

.method public final qe(I)Ljis;
    .locals 1

    .prologue
    .line 42130
    iput p1, p0, Ljis;->enR:I

    .line 42131
    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljis;->aez:I

    .line 42132
    return-object p0
.end method

.method public final qf(I)Ljis;
    .locals 1

    .prologue
    .line 42168
    const/16 v0, 0x5a

    iput v0, p0, Ljis;->enT:I

    .line 42169
    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljis;->aez:I

    .line 42170
    return-object p0
.end method

.method public final qg(I)Ljis;
    .locals 1

    .prologue
    .line 42187
    const/4 v0, 0x1

    iput v0, p0, Ljis;->enU:I

    .line 42188
    iget v0, p0, Ljis;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljis;->aez:I

    .line 42189
    return-object p0
.end method

.method public final qh(I)Ljis;
    .locals 1

    .prologue
    .line 42228
    const/16 v0, 0xa

    iput v0, p0, Ljis;->enV:I

    .line 42229
    iget v0, p0, Ljis;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljis;->aez:I

    .line 42230
    return-object p0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 42053
    iget v0, p0, Ljis;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final vZ(Ljava/lang/String;)Ljis;
    .locals 1

    .prologue
    .line 42045
    if-nez p1, :cond_0

    .line 42046
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42048
    :cond_0
    iput-object p1, p0, Ljis;->akf:Ljava/lang/String;

    .line 42049
    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljis;->aez:I

    .line 42050
    return-object p0
.end method

.method public final wa(Ljava/lang/String;)Ljis;
    .locals 1

    .prologue
    .line 42067
    if-nez p1, :cond_0

    .line 42068
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42070
    :cond_0
    iput-object p1, p0, Ljis;->aiK:Ljava/lang/String;

    .line 42071
    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljis;->aez:I

    .line 42072
    return-object p0
.end method

.method public final wb(Ljava/lang/String;)Ljis;
    .locals 1

    .prologue
    .line 42089
    if-nez p1, :cond_0

    .line 42090
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42092
    :cond_0
    iput-object p1, p0, Ljis;->afW:Ljava/lang/String;

    .line 42093
    iget v0, p0, Ljis;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljis;->aez:I

    .line 42094
    return-object p0
.end method
