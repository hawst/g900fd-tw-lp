.class public abstract Laif;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field FX:I

.field TT:I

.field YK:Laik;

.field YP:Landroid/graphics/Bitmap;

.field public YQ:Laig;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    sget-object v0, Laig;->YR:Laig;

    iput-object v0, p0, Laif;->YQ:Laig;

    .line 166
    iput p1, p0, Laif;->FX:I

    .line 167
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method public abstract a(Lqp;)Z
.end method

.method public final lO()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 169
    new-instance v0, Lqp;

    invoke-direct {v0}, Lqp;-><init>()V

    .line 170
    invoke-virtual {p0, v0}, Laif;->a(Lqp;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    sget v3, Lqp;->to:I

    invoke-virtual {v0, v3}, Lqp;->aC(I)Ljava/lang/Integer;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {v0}, Ljava/lang/Integer;->shortValue()S

    move-result v0

    invoke-static {v0}, Lqp;->b(S)I

    move-result v0

    iput v0, p0, Laif;->TT:I

    .line 176
    :cond_0
    invoke-virtual {p0}, Laif;->lP()Laik;

    move-result-object v0

    iput-object v0, p0, Laif;->YK:Laik;

    .line 177
    iget-object v0, p0, Laif;->YK:Laik;

    if-nez v0, :cond_1

    .line 178
    sget-object v0, Laig;->YT:Laig;

    iput-object v0, p0, Laif;->YQ:Laig;

    move v1, v2

    .line 195
    :goto_0
    return v1

    .line 181
    :cond_1
    iget-object v0, p0, Laif;->YK:Laik;

    invoke-interface {v0}, Laik;->getWidth()I

    move-result v0

    .line 182
    iget-object v3, p0, Laif;->YK:Laik;

    invoke-interface {v3}, Laik;->getHeight()I

    move-result v3

    .line 183
    iget v4, p0, Laif;->FX:I

    if-eqz v4, :cond_2

    .line 184
    iget v4, p0, Laif;->FX:I

    const/16 v5, 0x400

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 185
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 186
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v6, v5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 187
    iput-boolean v1, v5, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 189
    int-to-float v4, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    div-float v0, v4, v0

    .line 190
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v0, v3, v0

    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    move-result v0

    float-to-int v0, v0

    if-gt v0, v1, :cond_3

    move v0, v1

    :goto_1
    iput v0, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 191
    iput-boolean v2, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 192
    invoke-virtual {p0, v5}, Laif;->a(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Laif;->YP:Landroid/graphics/Bitmap;

    .line 194
    :cond_2
    sget-object v0, Laig;->YS:Laig;

    iput-object v0, p0, Laif;->YQ:Laig;

    goto :goto_0

    .line 190
    :cond_3
    const/16 v3, 0x8

    if-gt v0, v3, :cond_4

    invoke-static {v0}, Lqm;->az(I)I

    move-result v0

    goto :goto_1

    :cond_4
    div-int/lit8 v0, v0, 0x8

    mul-int/lit8 v0, v0, 0x8

    goto :goto_1
.end method

.method public abstract lP()Laik;
.end method
