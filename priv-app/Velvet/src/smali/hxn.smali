.class public final Lhxn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Libn;


# instance fields
.field public final mContext:Landroid/content/Context;

.field public final mSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhym;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lhxn;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lhxn;->mSettings:Lhym;

    .line 62
    return-void
.end method


# virtual methods
.method final h(ZI)V
    .locals 2

    .prologue
    .line 206
    iget-object v1, p0, Lhxn;->mSettings:Lhym;

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Lhym;->lE(I)V

    .line 209
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 210
    if-eqz p1, :cond_2

    const/16 v0, 0x30

    :goto_1
    invoke-static {v0}, Lege;->ht(I)V

    .line 214
    :cond_0
    return-void

    .line 206
    :cond_1
    const/4 v0, 0x3

    goto :goto_0

    .line 210
    :cond_2
    const/16 v0, 0x31

    goto :goto_1
.end method

.method public final ov(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lhxn;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNc:Ljzu;

    invoke-virtual {v0}, Ljzu;->bxu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    const/16 v0, 0x2e

    invoke-static {v0}, Lege;->ht(I)V

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lhxn;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNc:Ljzu;

    invoke-virtual {v0}, Ljzu;->bxt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const/16 v0, 0x2f

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_0
.end method
