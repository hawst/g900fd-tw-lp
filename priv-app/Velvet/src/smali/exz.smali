.class public final Lexz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfbn;


# instance fields
.field private final clB:Ljcu;

.field private final cla:Lgbg;


# direct methods
.method public constructor <init>(Lizj;Ljcu;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iput-object v0, p0, Lexz;->cla:Lgbg;

    .line 24
    iput-object p2, p0, Lexz;->clB:Ljcu;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lgbg;)Lizj;
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 29
    iget-object v0, p0, Lexz;->cla:Lgbg;

    invoke-virtual {v0, p1}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 30
    invoke-virtual {p1}, Lgbg;->aEf()Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 31
    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Lizj;

    .line 33
    iget-object v3, v0, Lizj;->dSc:Ljal;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lizj;->dSc:Ljal;

    iget-object v3, v3, Ljal;->dWL:Ljak;

    if-eqz v3, :cond_1

    .line 35
    iget-object v3, v0, Lizj;->dSc:Ljal;

    iget-object v3, v3, Ljal;->dWL:Ljak;

    .line 36
    iget-object v5, v3, Ljak;->clz:Ljcu;

    if-eqz v5, :cond_0

    iget-object v5, v3, Ljak;->clz:Ljcu;

    invoke-virtual {v5}, Ljcu;->baV()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 37
    iget-object v2, v3, Ljak;->clz:Ljcu;

    invoke-virtual {v2}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    .line 39
    :cond_0
    iget-object v5, p0, Lexz;->clB:Ljcu;

    iput-object v5, v3, Ljak;->clz:Ljcu;

    .line 42
    :cond_1
    iget-object v3, p0, Lexz;->clB:Ljcu;

    invoke-virtual {v3}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    .line 44
    if-eqz v2, :cond_4

    .line 45
    iget-object v7, v0, Lizj;->dTp:[Ljdj;

    array-length v8, v7

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_4

    aget-object v3, v7, v5

    .line 46
    iget-object v3, v3, Ljdj;->ame:Ljde;

    iget-object v9, v3, Ljde;->ebv:[Ljdg;

    array-length v10, v9

    move v3, v4

    :goto_1
    if-ge v3, v10, :cond_3

    aget-object v11, v9, v3

    .line 47
    invoke-virtual {v11}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 48
    invoke-virtual {v11, v6}, Ljdg;->tz(Ljava/lang/String;)Ljdg;

    .line 46
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 45
    :cond_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 55
    :cond_4
    const/16 v2, 0x10

    invoke-static {v0, v2}, Lgbm;->j(Lizj;I)V

    .line 58
    :goto_2
    return-object v1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method public final synthetic as(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lgbg;

    invoke-virtual {p0, p1}, Lexz;->a(Lgbg;)Lizj;

    move-result-object v0

    return-object v0
.end method
