.class public Ldei;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldef;


# instance fields
.field protected final bub:Ljava/util/List;

.field public final bvl:Ljava/lang/String;

.field private final bvm:Lcom/google/android/shared/search/Query;

.field protected bvn:Z

.field bvo:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Ldei;->bvl:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Ldei;->bvm:Lcom/google/android/shared/search/Query;

    .line 41
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Ldei;->bub:Ljava/util/List;

    .line 42
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;I)V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, p1, p2, v0}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Ldei;->bvl:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Ldei;->bvm:Lcom/google/android/shared/search/Query;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Ldei;->bub:Ljava/util/List;

    .line 36
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;[Lcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 51
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final QJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldei;->bvl:Ljava/lang/String;

    return-object v0
.end method

.method public final abb()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ldei;->bvm:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final abc()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Ldei;->bvn:Z

    return v0
.end method

.method public final abd()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Ldei;->bvo:Z

    return v0
.end method

.method public abe()Ljava/util/List;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Ldei;->bub:Ljava/util/List;

    return-object v0
.end method

.method public final dt(Z)V
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Ldei;->bvo:Z

    .line 99
    return-void
.end method

.method public fJ(I)Lcom/google/android/shared/search/Suggestion;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ldei;->bub:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ldei;->bub:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Ldei;->bub:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Likr;->b(Ljava/util/Iterator;)Lirv;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n{["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldei;->bvm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    iget-object v0, p0, Ldei;->bub:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 69
    const-string v3, "\n  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " isFinal:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Ldei;->bvn:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " mSourceName:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldei;->bvl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string v0, "}\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
