.class public final Lapi;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile alk:[Lapi;


# instance fields
.field private aez:I

.field private agv:I

.field private aib:Ljava/lang/String;

.field public all:[Laph;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9677
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 9678
    const/4 v0, 0x0

    iput v0, p0, Lapi;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Lapi;->agv:I

    const-string v0, ""

    iput-object v0, p0, Lapi;->aib:Ljava/lang/String;

    invoke-static {}, Laph;->sL()[Laph;

    move-result-object v0

    iput-object v0, p0, Lapi;->all:[Laph;

    const/4 v0, 0x0

    iput-object v0, p0, Lapi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapi;->eCz:I

    .line 9679
    return-void
.end method

.method public static sP()[Lapi;
    .locals 2

    .prologue
    .line 9620
    sget-object v0, Lapi;->alk:[Lapi;

    if-nez v0, :cond_1

    .line 9621
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 9623
    :try_start_0
    sget-object v0, Lapi;->alk:[Lapi;

    if-nez v0, :cond_0

    .line 9624
    const/4 v0, 0x0

    new-array v0, v0, [Lapi;

    sput-object v0, Lapi;->alk:[Lapi;

    .line 9626
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9628
    :cond_1
    sget-object v0, Lapi;->alk:[Lapi;

    return-object v0

    .line 9626
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9610
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lapi;->agv:I

    iget v0, p0, Lapi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapi;->aib:Ljava/lang/String;

    iget v0, p0, Lapi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapi;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lapi;->all:[Laph;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Laph;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lapi;->all:[Laph;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Laph;

    invoke-direct {v3}, Laph;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lapi;->all:[Laph;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Laph;

    invoke-direct {v3}, Laph;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lapi;->all:[Laph;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 9694
    iget v0, p0, Lapi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9695
    const/4 v0, 0x1

    iget v1, p0, Lapi;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 9697
    :cond_0
    iget v0, p0, Lapi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9698
    const/4 v0, 0x2

    iget-object v1, p0, Lapi;->aib:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9700
    :cond_1
    iget-object v0, p0, Lapi;->all:[Laph;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lapi;->all:[Laph;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 9701
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lapi;->all:[Laph;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 9702
    iget-object v1, p0, Lapi;->all:[Laph;

    aget-object v1, v1, v0

    .line 9703
    if-eqz v1, :cond_2

    .line 9704
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 9701
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9708
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9709
    return-void
.end method

.method public final cV(I)Lapi;
    .locals 1

    .prologue
    .line 9639
    iput p1, p0, Lapi;->agv:I

    .line 9640
    iget v0, p0, Lapi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapi;->aez:I

    .line 9641
    return-object p0
.end method

.method public final dg(Ljava/lang/String;)Lapi;
    .locals 1

    .prologue
    .line 9658
    if-nez p1, :cond_0

    .line 9659
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9661
    :cond_0
    iput-object p1, p0, Lapi;->aib:Ljava/lang/String;

    .line 9662
    iget v0, p0, Lapi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapi;->aez:I

    .line 9663
    return-object p0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 9636
    iget v0, p0, Lapi;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 9713
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9714
    iget v1, p0, Lapi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9715
    const/4 v1, 0x1

    iget v2, p0, Lapi;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9718
    :cond_0
    iget v1, p0, Lapi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9719
    const/4 v1, 0x2

    iget-object v2, p0, Lapi;->aib:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9722
    :cond_1
    iget-object v1, p0, Lapi;->all:[Laph;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lapi;->all:[Laph;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 9723
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lapi;->all:[Laph;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 9724
    iget-object v2, p0, Lapi;->all:[Laph;

    aget-object v2, v2, v0

    .line 9725
    if-eqz v2, :cond_2

    .line 9726
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 9723
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 9731
    :cond_4
    return v0
.end method

.method public final ps()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9655
    iget-object v0, p0, Lapi;->aib:Ljava/lang/String;

    return-object v0
.end method

.method public final pt()Z
    .locals 1

    .prologue
    .line 9666
    iget v0, p0, Lapi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
