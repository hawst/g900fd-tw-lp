.class public final Lhcr;
.super Lepp;
.source "PG"


# instance fields
.field private synthetic aXK:Landroid/accounts/Account;

.field private synthetic dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lhcr;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    iput-object p3, p0, Lhcr;->aXK:Landroid/accounts/Account;

    invoke-direct {p0, p2}, Lepp;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lhcr;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    invoke-static {v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->a(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    :goto_0
    return-void

    .line 166
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhcr;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    const-class v2, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 168
    iget-object v1, p0, Lhcr;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 169
    const-string v1, "optin_account"

    iget-object v2, p0, Lhcr;->aXK:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 170
    iget-object v1, p0, Lhcr;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v0, v2}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 171
    iget-object v0, p0, Lhcr;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    const v1, 0x7f050008

    const v2, 0x7f050009

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method
