.class public final Lfdb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aqp:Ljava/lang/Object;

.field public final cjP:Lfdo;

.field private cjR:Levm;

.field private ckD:Leuc;

.field private ckc:Lfam;

.field private final coU:Lfdf;

.field public final coV:Lgxc;

.field public final coW:Lgue;

.field private coX:Lfgu;

.field private coY:Leyj;

.field private coZ:Lfjs;

.field private cpa:Lfao;

.field private cpb:Lezg;

.field private cpc:Lezt;

.field private cpd:Lfdi;

.field private cpe:Livq;

.field private cpf:Livq;

.field private mActivityHelper:Lfzw;

.field private final mActivityLifecycleNotifier:Lgpd;

.field private final mAppContext:Landroid/content/Context;

.field public final mAsyncFileStorage:Lfbz;

.field private final mAsyncServices:Lema;

.field private mCalendarDataProvider:Leym;

.field private final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private mContentCacherFactory:Lfjr;

.field public final mCoreServices:Lcfo;

.field private mDirectionsLauncher:Lgah;

.field private mEntriesRefreshScheduler:Lfak;

.field private mEntryNotificationFactory:Lfjs;

.field private mEntryProvider:Lfaq;

.field private mEntryTreePruner:Lfbh;

.field private mEntryValidator:Lfbp;

.field private mExecutedUserActionStore:Lfcr;

.field private final mFileBytesReader:Lfcj;

.field private final mFileBytesWriter:Lfck;

.field private mFreshenRequestManager:Lfil;

.field private mGCMManager:Lfcl;

.field private final mGeofenceHelper:Lffl;

.field public final mGmsLocationProvider:Lguh;

.field private mInteractionManager:Lfdg;

.field public final mLocalBroadcastManager:Lcn;

.field public final mLocationDisabledCardHelper:Lfbq;

.field private final mLocationManager:Lfdq;

.field public final mLocationOracle:Lfdr;

.field private mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

.field private mNetworkClient:Lfcx;

.field public final mNotificationStore:Lffp;

.field private mNowNotificationManager:Lfga;

.field private mNowOptInHelper:Leux;

.field private mNowRemoteClient:Lfml;

.field private final mPrefController:Lchr;

.field private mReminderSmartActionUtil:Leqm;

.field private mRenderingContextAdapterFactory:Lfjs;

.field private final mSignedCipherHelper:Lfdh;

.field private final mStringEvaluator:Lgbr;

.field private mTrainingQuestionManager:Lfdn;

.field private final mTriggerConditionScheduler:Lfiv;

.field private mTvDetectorFactory:Livq;

.field public final mUndoDismissManager:Lfnn;

.field private mUserClientIdManager:Lewh;

.field public mVehicleActivityHandler:Lfer;

.field private mWidgetManager:Lfjo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchr;Lcfo;Lema;Lgpd;Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    iput-object p1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    .line 204
    iput-object p2, p0, Lfdb;->mPrefController:Lchr;

    .line 205
    iput-object p3, p0, Lfdb;->mCoreServices:Lcfo;

    .line 206
    iget-object v1, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    iput-object v1, p0, Lfdb;->mClock:Lemp;

    .line 207
    iget-object v1, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    iput-object v1, p0, Lfdb;->mConfig:Lcjs;

    .line 208
    iput-object p4, p0, Lfdb;->mAsyncServices:Lema;

    .line 209
    move-object/from16 v0, p5

    iput-object v0, p0, Lfdb;->mActivityLifecycleNotifier:Lgpd;

    .line 210
    move-object/from16 v0, p6

    iput-object v0, p0, Lfdb;->aqp:Ljava/lang/Object;

    .line 214
    new-instance v1, Lfdf;

    invoke-direct {v1, p2}, Lfdf;-><init>(Lchr;)V

    iput-object v1, p0, Lfdb;->coU:Lfdf;

    .line 218
    new-instance v2, Lfeq;

    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    invoke-virtual {p3}, Lcfo;->DP()Lcob;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lfeq;-><init>(Landroid/location/LocationManager;Lcob;)V

    iput-object v2, p0, Lfdb;->mLocationManager:Lfdq;

    .line 221
    new-instance v1, Lguh;

    iget-object v2, p0, Lfdb;->mAsyncServices:Lema;

    invoke-direct {v1, p1, v2}, Lguh;-><init>(Landroid/content/Context;Lerk;)V

    iput-object v1, p0, Lfdb;->mGmsLocationProvider:Lguh;

    .line 222
    new-instance v1, Lffl;

    iget-object v2, p0, Lfdb;->mGmsLocationProvider:Lguh;

    iget-object v3, p0, Lfdb;->mConfig:Lcjs;

    invoke-direct {v1, p1, v2, v3}, Lffl;-><init>(Landroid/content/Context;Lguh;Lcjs;)V

    iput-object v1, p0, Lfdb;->mGeofenceHelper:Lffl;

    .line 223
    new-instance v1, Levn;

    invoke-direct {v1, p2}, Levn;-><init>(Lchr;)V

    iput-object v1, p0, Lfdb;->mSignedCipherHelper:Lfdh;

    .line 225
    new-instance v1, Lfcj;

    iget-object v2, p0, Lfdb;->mSignedCipherHelper:Lfdh;

    invoke-direct {v1, p1, v2}, Lfcj;-><init>(Landroid/content/Context;Lfdh;)V

    iput-object v1, p0, Lfdb;->mFileBytesReader:Lfcj;

    .line 226
    new-instance v1, Lfck;

    iget-object v2, p0, Lfdb;->mSignedCipherHelper:Lfdh;

    invoke-direct {v1, p1, v2}, Lfck;-><init>(Landroid/content/Context;Lfdh;)V

    iput-object v1, p0, Lfdb;->mFileBytesWriter:Lfck;

    .line 227
    new-instance v1, Lfca;

    iget-object v2, p0, Lfdb;->mFileBytesReader:Lfcj;

    iget-object v3, p0, Lfdb;->mFileBytesWriter:Lfck;

    invoke-direct {v1, v2, v3}, Lfca;-><init>(Lfcj;Lfck;)V

    iput-object v1, p0, Lfdb;->mAsyncFileStorage:Lfbz;

    .line 228
    invoke-static {p1}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    iput-object v1, p0, Lfdb;->mLocalBroadcastManager:Lcn;

    .line 230
    new-instance v1, Lfdu;

    iget-object v2, p0, Lfdb;->mLocationManager:Lfdq;

    iget-object v3, p0, Lfdb;->mGmsLocationProvider:Lguh;

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    iget-object v5, p0, Lfdb;->mFileBytesReader:Lfcj;

    iget-object v6, p0, Lfdb;->mFileBytesWriter:Lfck;

    invoke-virtual {p3}, Lcfo;->DP()Lcob;

    move-result-object v7

    iget-object v9, p0, Lfdb;->mLocalBroadcastManager:Lcn;

    new-instance v10, Lfep;

    iget-object v8, p0, Lfdb;->mSignedCipherHelper:Lfdh;

    invoke-direct {v10, v8}, Lfep;-><init>(Lfdh;)V

    move-object v8, p2

    invoke-direct/range {v1 .. v10}, Lfdu;-><init>(Lfdq;Lguh;Lemp;Lfcj;Lfck;Lcob;Lchr;Lcn;Lfep;)V

    iput-object v1, p0, Lfdb;->mLocationOracle:Lfdr;

    .line 235
    new-instance v1, Lfdo;

    invoke-virtual {p3}, Lcfo;->DC()Lemp;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mAsyncFileStorage:Lfbz;

    invoke-virtual {p3}, Lcfo;->DD()Lcjs;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lfdo;-><init>(Lemp;Lfbz;Lcjs;)V

    iput-object v1, p0, Lfdb;->cjP:Lfdo;

    .line 238
    new-instance v1, Lffp;

    iget-object v2, p0, Lfdb;->mFileBytesReader:Lfcj;

    iget-object v3, p0, Lfdb;->mFileBytesWriter:Lfck;

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    iget-object v5, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v6, p0, Lfdb;->mAsyncServices:Lema;

    iget-object v7, p0, Lfdb;->mGeofenceHelper:Lffl;

    invoke-direct/range {v1 .. v7}, Lffp;-><init>(Lfcj;Lfck;Lemp;Landroid/content/Context;Lema;Lffl;)V

    iput-object v1, p0, Lfdb;->mNotificationStore:Lffp;

    .line 241
    iget-object v1, p0, Lfdb;->mLocationOracle:Lfdr;

    new-instance v2, Lffm;

    iget-object v3, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v4, p0, Lfdb;->mNotificationStore:Lffp;

    iget-object v5, p0, Lfdb;->mConfig:Lcjs;

    invoke-direct {v2, v3, v4, v5}, Lffm;-><init>(Landroid/content/Context;Lffp;Lcjs;)V

    invoke-interface {v1, v2}, Lfdr;->a(Lfds;)V

    .line 244
    new-instance v1, Lgxc;

    iget-object v2, p0, Lfdb;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mClock:Lemp;

    invoke-direct {v1, v2, v3}, Lgxc;-><init>(Landroid/content/SharedPreferences;Lemp;)V

    iput-object v1, p0, Lfdb;->coV:Lgxc;

    .line 247
    new-instance v1, Lfnn;

    invoke-direct {v1, p1, p4}, Lfnn;-><init>(Landroid/content/Context;Lerp;)V

    iput-object v1, p0, Lfdb;->mUndoDismissManager:Lfnn;

    .line 250
    new-instance v1, Lfbq;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->coU:Lfdf;

    iget-object v4, p0, Lfdb;->mLocationManager:Lfdq;

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DP()Lcob;

    move-result-object v5

    iget-object v6, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DL()Lcrh;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lfbq;-><init>(Landroid/content/Context;Ligi;Lfdq;Lcob;Lcrh;)V

    iput-object v1, p0, Lfdb;->mLocationDisabledCardHelper:Lfbq;

    .line 254
    new-instance v1, Lgue;

    iget-object v2, p0, Lfdb;->mAsyncServices:Lema;

    invoke-direct {v1, p1, v2}, Lgue;-><init>(Landroid/content/Context;Lerk;)V

    iput-object v1, p0, Lfdb;->coW:Lgue;

    .line 256
    new-instance v1, Lfiv;

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->Ea()Ldjx;

    move-result-object v2

    invoke-virtual {p3}, Lcfo;->DD()Lcjs;

    move-result-object v3

    iget-object v4, p0, Lfdb;->mGmsLocationProvider:Lguh;

    invoke-direct {v1, v2, v3, v4}, Lfiv;-><init>(Ldjx;Lcjs;Lguh;)V

    iput-object v1, p0, Lfdb;->mTriggerConditionScheduler:Lfiv;

    .line 259
    new-instance v1, Lgbr;

    iget-object v2, p0, Lfdb;->mClock:Lemp;

    invoke-direct {v1, v2}, Lgbr;-><init>(Lemp;)V

    iput-object v1, p0, Lfdb;->mStringEvaluator:Lgbr;

    .line 264
    return-void
.end method

.method private axK()Lfbp;
    .locals 5

    .prologue
    .line 375
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 376
    :try_start_0
    iget-object v0, p0, Lfdb;->mEntryValidator:Lfbp;

    if-nez v0, :cond_0

    .line 377
    new-instance v0, Lfbp;

    invoke-virtual {p0}, Lfdb;->axN()Leym;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v4, p0, Lfdb;->mLocationOracle:Lfdr;

    invoke-direct {v0, v2, v3, v4}, Lfbp;-><init>(Leym;Landroid/content/Context;Lfdr;)V

    iput-object v0, p0, Lfdb;->mEntryValidator:Lfbp;

    .line 380
    :cond_0
    iget-object v0, p0, Lfdb;->mEntryValidator:Lfbp;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 381
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private axS()Lfjs;
    .locals 7

    .prologue
    .line 504
    iget-object v6, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 505
    :try_start_0
    iget-object v0, p0, Lfdb;->mRenderingContextAdapterFactory:Lfjs;

    if-nez v0, :cond_0

    .line 506
    new-instance v0, Lezr;

    iget-object v1, p0, Lfdb;->coU:Lfdf;

    invoke-direct {p0}, Lfdb;->axK()Lfbp;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DE()Lcxs;

    move-result-object v3

    iget-object v4, p0, Lfdb;->mNotificationStore:Lffp;

    iget-object v5, p0, Lfdb;->mClock:Lemp;

    invoke-direct/range {v0 .. v5}, Lezr;-><init>(Ligi;Lfbp;Lcxs;Lffp;Lemp;)V

    iput-object v0, p0, Lfdb;->mRenderingContextAdapterFactory:Lfjs;

    .line 513
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    iget-object v0, p0, Lfdb;->mRenderingContextAdapterFactory:Lfjs;

    return-object v0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method private ayd()Lgah;
    .locals 4

    .prologue
    .line 651
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 652
    :try_start_0
    iget-object v0, p0, Lfdb;->mDirectionsLauncher:Lgah;

    if-nez v0, :cond_0

    .line 653
    new-instance v0, Lgah;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lfdb;->axW()Lfzw;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lgah;-><init>(Landroid/content/Context;Lfzw;)V

    iput-object v0, p0, Lfdb;->mDirectionsLauncher:Lgah;

    .line 658
    :cond_0
    iget-object v0, p0, Lfdb;->mDirectionsLauncher:Lgah;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 659
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private aye()Lezg;
    .locals 9

    .prologue
    .line 663
    iget-object v8, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v8

    .line 664
    :try_start_0
    iget-object v0, p0, Lfdb;->cpb:Lezg;

    if-nez v0, :cond_0

    .line 665
    new-instance v0, Lezg;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DE()Lcxs;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mCalendarDataProvider:Leym;

    iget-object v4, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DL()Lcrh;

    move-result-object v4

    new-instance v5, Lgpk;

    iget-object v6, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lgpk;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DX()Lenm;

    move-result-object v6

    iget-object v7, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->DD()Lcjs;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lezg;-><init>(Landroid/content/Context;Lcxs;Leym;Lcrh;Lgpk;Lenm;Lcjs;)V

    iput-object v0, p0, Lfdb;->cpb:Lezg;

    .line 674
    :cond_0
    iget-object v0, p0, Lfdb;->cpb:Lezg;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method private static d(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 808
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 809
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 810
    const/4 v0, 0x1

    .line 813
    :cond_0
    return v0

    .line 808
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final Sh()Lfcr;
    .locals 7

    .prologue
    .line 439
    iget-object v6, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 440
    :try_start_0
    iget-object v0, p0, Lfdb;->mExecutedUserActionStore:Lfcr;

    if-nez v0, :cond_0

    .line 441
    new-instance v0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->mFileBytesReader:Lfcj;

    iget-object v3, p0, Lfdb;->mFileBytesWriter:Lfck;

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    iget-object v5, p0, Lfdb;->mActivityLifecycleNotifier:Lgpd;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;-><init>(Landroid/content/Context;Lfcj;Lfck;Lemp;Lgpd;)V

    iput-object v0, p0, Lfdb;->mExecutedUserActionStore:Lfcr;

    .line 448
    :cond_0
    iget-object v0, p0, Lfdb;->mExecutedUserActionStore:Lfcr;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final axF()Lfam;
    .locals 5

    .prologue
    .line 267
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 268
    :try_start_0
    iget-object v0, p0, Lfdb;->ckc:Lfam;

    if-nez v0, :cond_0

    .line 269
    new-instance v0, Lfan;

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->Ea()Ldjx;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mPrefController:Lchr;

    iget-object v4, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lfan;-><init>(Ldjx;Lchr;Lchk;)V

    iput-object v0, p0, Lfdb;->ckc:Lfam;

    .line 274
    :cond_0
    iget-object v0, p0, Lfdb;->ckc:Lfam;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axG()Lfcl;
    .locals 6

    .prologue
    .line 279
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    :try_start_0
    iget-object v0, p0, Lfdb;->mGCMManager:Lfcl;

    if-nez v0, :cond_0

    .line 281
    new-instance v0, Lfcl;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-static {v3}, Lbjz;->X(Landroid/content/Context;)Lbjz;

    move-result-object v3

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    iget-object v5, p0, Lfdb;->coU:Lfdf;

    invoke-direct {v0, v2, v3, v4, v5}, Lfcl;-><init>(Landroid/content/Context;Lbjz;Lemp;Ligi;)V

    iput-object v0, p0, Lfdb;->mGCMManager:Lfcl;

    .line 286
    :cond_0
    iget-object v0, p0, Lfdb;->mGCMManager:Lfcl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axH()Lfil;
    .locals 4

    .prologue
    .line 291
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 292
    :try_start_0
    iget-object v0, p0, Lfdb;->mFreshenRequestManager:Lfil;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Lfil;

    iget-object v2, p0, Lfdb;->mFileBytesReader:Lfcj;

    iget-object v3, p0, Lfdb;->mFileBytesWriter:Lfck;

    invoke-direct {v0, v2, v3}, Lfil;-><init>(Lfcj;Lfck;)V

    iput-object v0, p0, Lfdb;->mFreshenRequestManager:Lfil;

    .line 297
    :cond_0
    iget-object v0, p0, Lfdb;->mFreshenRequestManager:Lfil;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axI()Lfcx;
    .locals 18

    .prologue
    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lfdb;->aqp:Ljava/lang/Object;

    move-object/from16 v17, v0

    monitor-enter v17

    .line 307
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lfdb;->mNetworkClient:Lfcx;

    if-nez v1, :cond_0

    .line 308
    move-object/from16 v0, p0

    iget-object v1, v0, Lfdb;->mAppContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/ConnectivityManager;

    .line 310
    new-instance v1, Lewi;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfdb;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v4

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJh()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DG()Ldkx;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->DL()Lcrh;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lfdb;->axV()Levm;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lfdb;->Sh()Lfcr;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v11}, Lcfo;->Em()Lcha;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lfdb;->aya()Lewh;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lfdb;->mPrefController:Lchr;

    move-object/from16 v0, p0

    iget-object v14, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v14}, Lcfo;->DX()Lenm;

    move-result-object v14

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v15

    invoke-virtual {v15}, Lgql;->aKb()Lelo;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lfdb;->mCoreServices:Lcfo;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcfo;->BL()Lchk;

    move-result-object v16

    invoke-direct/range {v1 .. v16}, Lewi;-><init>(Landroid/content/Context;Lcfo;Lckw;Ljava/lang/String;Ldkx;Lcrh;Levm;Landroid/net/ConnectivityManager;Lfcr;Lcha;Lewh;Lchr;Lenm;Lelo;Lchk;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lfdb;->mNetworkClient:Lfcx;

    .line 327
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lfdb;->mNetworkClient:Lfcx;

    monitor-exit v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    .line 328
    :catchall_0
    move-exception v1

    monitor-exit v17

    throw v1
.end method

.method public final axJ()Lfgu;
    .locals 6

    .prologue
    .line 365
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v0, p0, Lfdb;->coX:Lfgu;

    if-nez v0, :cond_0

    .line 367
    new-instance v0, Lfgu;

    invoke-virtual {p0}, Lfdb;->axI()Lfcx;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mLocationOracle:Lfdr;

    invoke-virtual {p0}, Lfdb;->axL()Lfbh;

    move-result-object v4

    invoke-virtual {p0}, Lfdb;->ayf()Lezt;

    move-result-object v5

    invoke-direct {v0, v2, v3, v4, v5}, Lfgu;-><init>(Lfcx;Lfdr;Lfbh;Lezt;)V

    iput-object v0, p0, Lfdb;->coX:Lfgu;

    .line 370
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    iget-object v0, p0, Lfdb;->coX:Lfgu;

    return-object v0

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axL()Lfbh;
    .locals 3

    .prologue
    .line 385
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 386
    :try_start_0
    iget-object v0, p0, Lfdb;->mEntryTreePruner:Lfbh;

    if-nez v0, :cond_0

    .line 387
    new-instance v0, Lfbh;

    invoke-direct {p0}, Lfdb;->axK()Lfbp;

    move-result-object v2

    invoke-direct {v0, v2}, Lfbh;-><init>(Lfbp;)V

    iput-object v0, p0, Lfdb;->mEntryTreePruner:Lfbh;

    .line 389
    :cond_0
    iget-object v0, p0, Lfdb;->mEntryTreePruner:Lfbh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axM()Leyj;
    .locals 5

    .prologue
    .line 394
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 395
    :try_start_0
    iget-object v0, p0, Lfdb;->coY:Leyj;

    if-nez v0, :cond_0

    .line 396
    new-instance v0, Leyk;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->mClock:Lemp;

    iget-object v4, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DE()Lcxs;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Leyk;-><init>(Landroid/content/Context;Lemp;Lcxs;)V

    iput-object v0, p0, Lfdb;->coY:Leyj;

    .line 400
    :cond_0
    iget-object v0, p0, Lfdb;->coY:Leyj;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 401
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axN()Leym;
    .locals 2

    .prologue
    .line 405
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 406
    :try_start_0
    iget-object v0, p0, Lfdb;->mCalendarDataProvider:Leym;

    if-nez v0, :cond_0

    .line 407
    invoke-virtual {p0}, Lfdb;->axM()Leyj;

    move-result-object v0

    invoke-interface {v0}, Leyj;->awy()Leym;

    move-result-object v0

    iput-object v0, p0, Lfdb;->mCalendarDataProvider:Leym;

    .line 410
    :cond_0
    iget-object v0, p0, Lfdb;->mCalendarDataProvider:Leym;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axO()Lfga;
    .locals 14

    .prologue
    .line 419
    iget-object v13, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v13

    .line 420
    :try_start_0
    iget-object v0, p0, Lfdb;->mNowNotificationManager:Lfga;

    if-nez v0, :cond_0

    .line 421
    new-instance v6, Lfdl;

    iget-object v0, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Lfdl;-><init>(Landroid/content/Context;)V

    .line 423
    new-instance v0, Lfgc;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->mClock:Lemp;

    invoke-virtual {p0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v3

    iget-object v4, p0, Lfdb;->mPrefController:Lchr;

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DR()Lcpx;

    move-result-object v5

    invoke-virtual {p0}, Lfdb;->axI()Lfcx;

    move-result-object v7

    iget-object v8, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v8}, Lcfo;->Eb()Lfda;

    move-result-object v8

    invoke-direct {p0}, Lfdb;->axS()Lfjs;

    move-result-object v9

    iget-object v10, p0, Lfdb;->mLocationOracle:Lfdr;

    invoke-direct {p0}, Lfdb;->aye()Lezg;

    move-result-object v11

    iget-object v12, p0, Lfdb;->mNotificationStore:Lffp;

    invoke-direct/range {v0 .. v12}, Lfgc;-><init>(Landroid/content/Context;Lemp;Lfaq;Lchr;Lcpx;Lfcz;Lfcx;Lfda;Lfjs;Lfdr;Lezg;Lffp;)V

    iput-object v0, p0, Lfdb;->mNowNotificationManager:Lfga;

    .line 434
    :cond_0
    iget-object v0, p0, Lfdb;->mNowNotificationManager:Lfga;

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit v13

    throw v0
.end method

.method public final axP()Lfjs;
    .locals 11

    .prologue
    .line 453
    iget-object v10, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v10

    .line 454
    :try_start_0
    iget-object v0, p0, Lfdb;->coZ:Lfjs;

    if-nez v0, :cond_0

    .line 455
    iget-object v0, p0, Lfdb;->mPrefController:Lchr;

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v1, "quantum_now_cards"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldku;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 458
    invoke-direct {p0}, Lfdb;->ayd()Lgah;

    move-result-object v2

    .line 459
    new-instance v9, Lfyk;

    iget-object v0, p0, Lfdb;->mClock:Lemp;

    iget-object v1, p0, Lfdb;->mStringEvaluator:Lgbr;

    invoke-direct {v9, v0, v1}, Lfyk;-><init>(Lemp;Lgbr;)V

    .line 461
    new-instance v0, Lfkr;

    iget-object v1, p0, Lfdb;->mClock:Lemp;

    iget-object v3, p0, Lfdb;->mAppContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    invoke-virtual {p0}, Lfdb;->axW()Lfzw;

    move-result-object v4

    iget-object v5, p0, Lfdb;->mAsyncServices:Lema;

    iget-object v6, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct/range {v0 .. v9}, Lfkr;-><init>(Lemp;Lgah;Landroid/net/wifi/WifiManager;Lfzw;Lerk;Landroid/content/pm/PackageManager;Landroid/content/ContentResolver;ZLfyk;)V

    iput-object v0, p0, Lfdb;->coZ:Lfjs;

    .line 472
    :cond_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 473
    iget-object v0, p0, Lfdb;->coZ:Lfjs;

    return-object v0

    .line 472
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0
.end method

.method public final axQ()Lfdi;
    .locals 5

    .prologue
    .line 477
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 478
    :try_start_0
    iget-object v0, p0, Lfdb;->cpd:Lfdi;

    if-nez v0, :cond_0

    .line 479
    new-instance v0, Lfdj;

    invoke-virtual {p0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lfdb;->axI()Lfcx;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lfdj;-><init>(Lfaq;Landroid/content/Context;Lfcx;)V

    iput-object v0, p0, Lfdb;->cpd:Lfdi;

    .line 484
    :cond_0
    iget-object v0, p0, Lfdb;->cpd:Lfdi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 485
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axR()Lfjs;
    .locals 8

    .prologue
    .line 489
    iget-object v7, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v7

    .line 490
    :try_start_0
    iget-object v0, p0, Lfdb;->mEntryNotificationFactory:Lfjs;

    if-nez v0, :cond_0

    .line 491
    new-instance v0, Lfez;

    iget-object v1, p0, Lfdb;->mLocationOracle:Lfdr;

    invoke-virtual {p0}, Lfdb;->axN()Leym;

    move-result-object v2

    invoke-direct {p0}, Lfdb;->ayd()Lgah;

    move-result-object v3

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    invoke-direct {p0}, Lfdb;->axK()Lfbp;

    move-result-object v5

    invoke-virtual {p0}, Lfdb;->aym()Leqm;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lfez;-><init>(Lfdr;Leym;Lgah;Lemp;Lfbp;Leqm;)V

    iput-object v0, p0, Lfdb;->mEntryNotificationFactory:Lfjs;

    .line 498
    :cond_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    iget-object v0, p0, Lfdb;->mEntryNotificationFactory:Lfjs;

    return-object v0

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final axT()Lfao;
    .locals 7

    .prologue
    .line 518
    iget-object v6, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 519
    :try_start_0
    iget-object v0, p0, Lfdb;->cpa:Lfao;

    if-nez v0, :cond_0

    .line 520
    new-instance v0, Lfap;

    invoke-virtual {p0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v1

    invoke-virtual {p0}, Lfdb;->axO()Lfga;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mLocationDisabledCardHelper:Lfbq;

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v5

    invoke-virtual {v5}, Lgql;->aKb()Lelo;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lfap;-><init>(Lfaq;Lfga;Lfbq;Lemp;Lelo;)V

    iput-object v0, p0, Lfdb;->cpa:Lfao;

    .line 527
    :cond_0
    iget-object v0, p0, Lfdb;->cpa:Lfao;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final axU()Lfjo;
    .locals 4

    .prologue
    .line 536
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 537
    :try_start_0
    iget-object v0, p0, Lfdb;->mWidgetManager:Lfjo;

    if-nez v0, :cond_0

    .line 538
    new-instance v0, Lfjo;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lfjo;-><init>(Landroid/content/Context;Leqo;)V

    iput-object v0, p0, Lfdb;->mWidgetManager:Lfjo;

    .line 542
    :cond_0
    iget-object v0, p0, Lfdb;->mWidgetManager:Lfjo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 543
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axV()Levm;
    .locals 9

    .prologue
    .line 547
    iget-object v8, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v8

    .line 548
    :try_start_0
    iget-object v0, p0, Lfdb;->cjR:Levm;

    if-nez v0, :cond_0

    .line 549
    new-instance v0, Levm;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->mClock:Lemp;

    iget-object v3, p0, Lfdb;->mLocationOracle:Lfdr;

    invoke-virtual {p0}, Lfdb;->axU()Lfjo;

    move-result-object v4

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->En()Leue;

    move-result-object v5

    iget-object v6, p0, Lfdb;->mAppContext:Landroid/content/Context;

    const-string v7, "wifi"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    iget-object v7, p0, Lfdb;->cjP:Lfdo;

    invoke-direct/range {v0 .. v7}, Levm;-><init>(Landroid/content/Context;Lemp;Lfdr;Lfjo;Leue;Landroid/net/wifi/WifiManager;Lfdo;)V

    iput-object v0, p0, Lfdb;->cjR:Levm;

    .line 557
    :cond_0
    iget-object v0, p0, Lfdb;->cjR:Levm;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 558
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public final axW()Lfzw;
    .locals 5

    .prologue
    .line 562
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 563
    :try_start_0
    iget-object v0, p0, Lfdb;->mActivityHelper:Lfzw;

    if-nez v0, :cond_0

    .line 564
    new-instance v0, Lfzw;

    iget-object v2, p0, Lfdb;->mAsyncServices:Lema;

    iget-object v3, p0, Lfdb;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->Mv()Z

    move-result v3

    iget-object v4, p0, Lfdb;->mConfig:Lcjs;

    invoke-virtual {v4}, Lcjs;->Mw()Z

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lfzw;-><init>(Lerk;ZZ)V

    iput-object v0, p0, Lfdb;->mActivityHelper:Lfzw;

    .line 568
    :cond_0
    iget-object v0, p0, Lfdb;->mActivityHelper:Lfzw;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 569
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axX()Leuc;
    .locals 5

    .prologue
    .line 573
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 574
    :try_start_0
    iget-object v0, p0, Lfdb;->ckD:Leuc;

    if-nez v0, :cond_0

    .line 575
    new-instance v0, Leuc;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->coU:Lfdf;

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    invoke-direct {v0, v2, v3, v4}, Leuc;-><init>(Landroid/content/Context;Ligi;Lemp;)V

    iput-object v0, p0, Lfdb;->ckD:Leuc;

    .line 579
    :cond_0
    iget-object v0, p0, Lfdb;->ckD:Leuc;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 580
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axY()Lfdg;
    .locals 6

    .prologue
    .line 584
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 585
    :try_start_0
    iget-object v0, p0, Lfdb;->mInteractionManager:Lfdg;

    if-nez v0, :cond_0

    .line 586
    new-instance v0, Lfdg;

    iget-object v2, p0, Lfdb;->coU:Lfdf;

    iget-object v3, p0, Lfdb;->mConfig:Lcjs;

    invoke-virtual {p0}, Lfdb;->axU()Lfjo;

    move-result-object v4

    iget-object v5, p0, Lfdb;->mClock:Lemp;

    invoke-direct {v0, v2, v3, v4, v5}, Lfdg;-><init>(Ligi;Lcjs;Lfjo;Lemp;)V

    iput-object v0, p0, Lfdb;->mInteractionManager:Lfdg;

    .line 590
    :cond_0
    iget-object v0, p0, Lfdb;->mInteractionManager:Lfdg;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 591
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axZ()Lfak;
    .locals 7

    .prologue
    .line 595
    iget-object v6, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 596
    :try_start_0
    iget-object v0, p0, Lfdb;->mEntriesRefreshScheduler:Lfak;

    if-nez v0, :cond_0

    .line 597
    new-instance v0, Lfak;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->Ea()Ldjx;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mConfig:Lcjs;

    invoke-virtual {p0}, Lfdb;->axY()Lfdg;

    move-result-object v4

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->Eb()Lfda;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lfak;-><init>(Landroid/content/Context;Ldjx;Lcjs;Lfdg;Lfda;)V

    iput-object v0, p0, Lfdb;->mEntriesRefreshScheduler:Lfak;

    .line 602
    :cond_0
    iget-object v0, p0, Lfdb;->mEntriesRefreshScheduler:Lfak;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 603
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final aya()Lewh;
    .locals 4

    .prologue
    .line 607
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 608
    :try_start_0
    iget-object v0, p0, Lfdb;->mUserClientIdManager:Lewh;

    if-nez v0, :cond_0

    .line 609
    new-instance v0, Lewh;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->coU:Lfdf;

    invoke-direct {v0, v2, v3}, Lewh;-><init>(Landroid/content/Context;Ligi;)V

    iput-object v0, p0, Lfdb;->mUserClientIdManager:Lewh;

    .line 613
    :cond_0
    iget-object v0, p0, Lfdb;->mUserClientIdManager:Lewh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 614
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayb()Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;
    .locals 10

    .prologue
    .line 618
    iget-object v9, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v9

    .line 619
    :try_start_0
    iget-object v0, p0, Lfdb;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    if-nez v0, :cond_0

    .line 620
    new-instance v0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->coU:Lfdf;

    iget-object v3, p0, Lfdb;->mClock:Lemp;

    iget-object v4, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->Ea()Ldjx;

    move-result-object v4

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DL()Lcrh;

    move-result-object v5

    iget-object v6, p0, Lfdb;->mAppContext:Landroid/content/Context;

    const-string v7, "power"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    iget-object v7, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->En()Leue;

    move-result-object v7

    iget-object v8, p0, Lfdb;->mAsyncServices:Lema;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;-><init>(Landroid/content/Context;Ligi;Lemp;Ldjx;Lcrh;Landroid/os/PowerManager;Leue;Lerp;)V

    iput-object v0, p0, Lfdb;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    .line 629
    :cond_0
    iget-object v0, p0, Lfdb;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 630
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method public final ayc()Lfdn;
    .locals 10

    .prologue
    .line 634
    invoke-virtual {p0}, Lfdb;->axI()Lfcx;

    move-result-object v3

    .line 635
    iget-object v9, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v9

    .line 636
    :try_start_0
    iget-object v0, p0, Lfdb;->mTrainingQuestionManager:Lfdn;

    if-nez v0, :cond_0

    .line 637
    new-instance v0, Lfig;

    iget-object v1, p0, Lfdb;->mFileBytesReader:Lfcj;

    iget-object v2, p0, Lfdb;->mFileBytesWriter:Lfck;

    iget-object v4, p0, Lfdb;->mClock:Lemp;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v5

    iget-object v5, v5, Lgql;->mAsyncServices:Lema;

    iget-object v6, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DO()Lgpp;

    move-result-object v6

    invoke-virtual {p0}, Lfdb;->axN()Leym;

    move-result-object v7

    iget-object v8, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v8}, Lcfo;->DF()Lcin;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lfig;-><init>(Lfcj;Lfck;Lfcx;Lemp;Lerk;Lgpp;Leym;Lcin;)V

    iput-object v0, p0, Lfdb;->mTrainingQuestionManager:Lfdn;

    .line 646
    :cond_0
    iget-object v0, p0, Lfdb;->mTrainingQuestionManager:Lfdn;

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 647
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method public final ayf()Lezt;
    .locals 5

    .prologue
    .line 679
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 680
    :try_start_0
    iget-object v0, p0, Lfdb;->cpc:Lezt;

    if-nez v0, :cond_0

    .line 681
    new-instance v0, Lezt;

    invoke-direct {p0}, Lfdb;->aye()Lezg;

    move-result-object v2

    invoke-direct {p0}, Lfdb;->axS()Lfjs;

    move-result-object v3

    invoke-virtual {p0}, Lfdb;->ayc()Lfdn;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lezt;-><init>(Lezg;Lfjs;Lfdn;)V

    iput-object v0, p0, Lfdb;->cpc:Lezt;

    .line 686
    :cond_0
    iget-object v0, p0, Lfdb;->cpc:Lezt;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 687
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayg()Lfml;
    .locals 4

    .prologue
    .line 691
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 692
    :try_start_0
    iget-object v0, p0, Lfdb;->mNowRemoteClient:Lfml;

    if-nez v0, :cond_0

    .line 693
    new-instance v0, Lfml;

    iget-object v2, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lfdb;->mAsyncServices:Lema;

    invoke-direct {v0, v2, v3}, Lfml;-><init>(Landroid/content/Context;Lerk;)V

    iput-object v0, p0, Lfdb;->mNowRemoteClient:Lfml;

    .line 696
    :cond_0
    iget-object v0, p0, Lfdb;->mNowRemoteClient:Lfml;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 697
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayh()Leux;
    .locals 12

    .prologue
    .line 701
    iget-object v11, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v11

    .line 702
    :try_start_0
    iget-object v0, p0, Lfdb;->mNowOptInHelper:Leux;

    if-nez v0, :cond_0

    .line 703
    new-instance v0, Leux;

    iget-object v1, p0, Lfdb;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-virtual {p0}, Lfdb;->axI()Lfcx;

    move-result-object v3

    iget-object v4, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DE()Lcxs;

    move-result-object v4

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DF()Lcin;

    move-result-object v5

    invoke-virtual {p0}, Lfdb;->ayb()Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    move-result-object v6

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v7

    invoke-virtual {p0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v8

    iget-object v9, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v9}, Lcfo;->BK()Lcke;

    move-result-object v9

    invoke-virtual {p0}, Lfdb;->axG()Lfcl;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Leux;-><init>(Ljava/util/concurrent/Executor;Lcrh;Lfcx;Lcxs;Lcin;Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;Lgql;Lfaq;Lcke;Lfcl;)V

    iput-object v0, p0, Lfdb;->mNowOptInHelper:Leux;

    .line 715
    :cond_0
    iget-object v0, p0, Lfdb;->mNowOptInHelper:Leux;

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 716
    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0
.end method

.method public final ayi()Livq;
    .locals 3

    .prologue
    .line 724
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 725
    :try_start_0
    iget-object v0, p0, Lfdb;->mTvDetectorFactory:Livq;

    if-nez v0, :cond_0

    .line 726
    iget-object v0, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v0

    sget-object v2, Lfiz;->ctv:Lgor;

    invoke-virtual {v0, v2}, Lgtx;->c(Lgor;)Livq;

    move-result-object v0

    new-instance v2, Lfdc;

    invoke-direct {v2, p0}, Lfdc;-><init>(Lfdb;)V

    invoke-static {v0, v2}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    iput-object v0, p0, Lfdb;->mTvDetectorFactory:Livq;

    .line 735
    :cond_0
    iget-object v0, p0, Lfdb;->mTvDetectorFactory:Livq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 736
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayj()Livq;
    .locals 3

    .prologue
    .line 740
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 741
    :try_start_0
    iget-object v0, p0, Lfdb;->cpe:Livq;

    if-nez v0, :cond_0

    .line 742
    iget-object v0, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v0

    sget-object v2, Lfiz;->ctv:Lgor;

    invoke-virtual {v0, v2}, Lgtx;->c(Lgor;)Livq;

    move-result-object v0

    new-instance v2, Lfdd;

    invoke-direct {v2, p0}, Lfdd;-><init>(Lfdb;)V

    invoke-static {v0, v2}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    iput-object v0, p0, Lfdb;->cpe:Livq;

    .line 751
    :cond_0
    iget-object v0, p0, Lfdb;->cpe:Livq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 752
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayk()Livq;
    .locals 3

    .prologue
    .line 756
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 757
    :try_start_0
    iget-object v0, p0, Lfdb;->cpf:Livq;

    if-nez v0, :cond_0

    .line 758
    iget-object v0, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v0

    sget-object v2, Lfjl;->ctv:Lgor;

    invoke-virtual {v0, v2}, Lgtx;->c(Lgor;)Livq;

    move-result-object v0

    new-instance v2, Lfde;

    invoke-direct {v2, p0}, Lfde;-><init>(Lfdb;)V

    invoke-static {v0, v2}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    iput-object v0, p0, Lfdb;->cpf:Livq;

    .line 767
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    iget-object v0, p0, Lfdb;->cpf:Livq;

    return-object v0

    .line 767
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayl()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 786
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 787
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 789
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 793
    :cond_0
    :goto_0
    return v0

    .line 792
    :cond_1
    iget-object v3, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    .line 793
    invoke-virtual {v3}, Lchk;->Hd()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lfdb;->d(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lchk;->He()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdb;->d(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aym()Leqm;
    .locals 3

    .prologue
    .line 798
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 799
    :try_start_0
    iget-object v0, p0, Lfdb;->mReminderSmartActionUtil:Leqm;

    if-nez v0, :cond_0

    .line 800
    new-instance v0, Leqm;

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DX()Lenm;

    move-result-object v2

    invoke-direct {v0, v2}, Leqm;-><init>(Lenm;)V

    iput-object v0, p0, Lfdb;->mReminderSmartActionUtil:Leqm;

    .line 803
    :cond_0
    iget-object v0, p0, Lfdb;->mReminderSmartActionUtil:Leqm;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 804
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayn()Lfer;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 837
    iget-object v0, p0, Lfdb;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mr()Z

    move-result v0

    if-nez v0, :cond_0

    .line 838
    const-string v0, "SidekickInjector"

    const-string v1, "FMC disabled in gservices config"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    const/4 v0, 0x0

    .line 850
    :goto_0
    return-object v0

    .line 842
    :cond_0
    iget-object v8, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v8

    .line 843
    :try_start_0
    iget-object v0, p0, Lfdb;->mVehicleActivityHandler:Lfer;

    if-nez v0, :cond_1

    .line 844
    new-instance v0, Lfer;

    iget-object v1, p0, Lfdb;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->Ea()Ldjx;

    move-result-object v2

    iget-object v3, p0, Lfdb;->mClock:Lemp;

    iget-object v4, p0, Lfdb;->mGmsLocationProvider:Lguh;

    iget-object v5, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->En()Leue;

    move-result-object v5

    iget-object v6, p0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DL()Lcrh;

    move-result-object v6

    iget-object v7, p0, Lfdb;->mPrefController:Lchr;

    invoke-direct/range {v0 .. v7}, Lfer;-><init>(Landroid/content/Context;Ldjx;Lemp;Lguh;Leue;Lcrh;Lchr;)V

    iput-object v0, p0, Lfdb;->mVehicleActivityHandler:Lfer;

    .line 850
    :cond_1
    iget-object v0, p0, Lfdb;->mVehicleActivityHandler:Lfer;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 851
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public final ayo()Lfjr;
    .locals 3

    .prologue
    .line 876
    iget-object v1, p0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 877
    :try_start_0
    iget-object v0, p0, Lfdb;->mContentCacherFactory:Lfjr;

    if-nez v0, :cond_0

    .line 878
    new-instance v0, Lfjr;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->anM()Lesm;

    move-result-object v2

    invoke-direct {v0, v2}, Lfjr;-><init>(Lesm;)V

    iput-object v0, p0, Lfdb;->mContentCacherFactory:Lfjr;

    .line 881
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 882
    iget-object v0, p0, Lfdb;->mContentCacherFactory:Lfjr;

    return-object v0

    .line 881
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final getEntryProvider()Lfaq;
    .locals 17

    .prologue
    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lfdb;->aqp:Ljava/lang/Object;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 345
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lfdb;->mEntryProvider:Lfaq;

    if-nez v1, :cond_0

    .line 346
    new-instance v1, Lfaq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfdb;->mClock:Lemp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfdb;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfdb;->mAsyncFileStorage:Lfbz;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfdb;->mNotificationStore:Lffp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lfdb;->mLocationOracle:Lfdr;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfdb;->mConfig:Lcjs;

    move-object/from16 v0, p0

    iget-object v8, v0, Lfdb;->mAsyncServices:Lema;

    move-object/from16 v0, p0

    iget-object v9, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v9}, Lcfo;->DE()Lcxs;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lfdb;->axR()Lfjs;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lfdb;->mTriggerConditionScheduler:Lfiv;

    invoke-virtual/range {p0 .. p0}, Lfdb;->axL()Lfbh;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lfdb;->axI()Lfcx;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lfdb;->axY()Lfdg;

    move-result-object v15

    move-object/from16 v9, p0

    invoke-direct/range {v1 .. v15}, Lfaq;-><init>(Lemp;Landroid/content/Context;Lfbz;Lffp;Lfdr;Lcjs;Lerm;Lfdb;Lcxs;Lfjs;Lfiv;Lfbh;Lfcx;Lfdg;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lfdb;->mEntryProvider:Lfaq;

    .line 357
    move-object/from16 v0, p0

    iget-object v1, v0, Lfdb;->mEntryProvider:Lfaq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfdb;->mNotificationStore:Lffp;

    invoke-virtual {v1, v2}, Lfaq;->a(Lfbb;)V

    .line 360
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lfdb;->mEntryProvider:Lfaq;

    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    .line 361
    :catchall_0
    move-exception v1

    monitor-exit v16

    throw v1
.end method
