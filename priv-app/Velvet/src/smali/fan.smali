.class public final Lfan;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfam;


# instance fields
.field private final mAlarmHelper:Ldjx;

.field private final mPrefController:Lchr;

.field private final mSearchConfig:Lchk;


# direct methods
.method public constructor <init>(Ldjx;Lchr;Lchk;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lfan;->mAlarmHelper:Ldjx;

    .line 38
    iput-object p2, p0, Lfan;->mPrefController:Lchr;

    .line 39
    iput-object p3, p0, Lfan;->mSearchConfig:Lchk;

    .line 40
    return-void
.end method

.method private static it(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 150
    const/4 v2, 0x5

    if-eq p0, v2, :cond_0

    const/4 v2, 0x7

    if-ne p0, v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/app/PendingIntent;J)Z
    .locals 11
    .param p2    # Landroid/app/PendingIntent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/32 v8, 0xea60

    const-wide/16 v0, 0x0

    .line 56
    invoke-static {p1}, Lflk;->iS(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move-wide v0, p3

    .line 57
    :goto_0
    cmp-long v2, v0, p3

    if-gtz v2, :cond_5

    .line 66
    iget-object v0, p0, Lfan;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 67
    invoke-static {p1}, Lfan;->it(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 68
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "throttle_last_partial"

    invoke-interface {v0, v1, p3, p4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 72
    :goto_1
    const/4 v0, 0x1

    .line 90
    :goto_2
    return v0

    .line 56
    :cond_0
    if-nez p1, :cond_1

    move-wide v0, p3

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lfan;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    const-string v3, "throttle_last_partial"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v3, "throttle_last_full"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v6, v4, p3

    if-lez v6, :cond_2

    move-wide v4, v0

    :cond_2
    cmp-long v6, v2, p3

    if-lez v6, :cond_7

    :goto_3
    invoke-static {p1}, Lfan;->it(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lfan;->mSearchConfig:Lchk;

    invoke-virtual {v2}, Lchk;->Hb()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v8

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long/2addr v0, v2

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lfan;->mSearchConfig:Lchk;

    invoke-virtual {v2}, Lchk;->Hc()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v8

    add-long/2addr v0, v2

    goto :goto_0

    .line 70
    :cond_4
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "throttle_last_full"

    invoke-interface {v0, v1, p3, p4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 84
    :cond_5
    if-eqz p2, :cond_6

    .line 86
    iget-object v2, p0, Lfan;->mAlarmHelper:Ldjx;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0, v1, p2}, Ldjx;->a(IJLandroid/app/PendingIntent;)V

    .line 90
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    move-wide v0, v2

    goto :goto_3
.end method

.method public final disableScheduledRefreshes(Z)V
    .locals 0

    .prologue
    .line 43
    return-void
.end method
