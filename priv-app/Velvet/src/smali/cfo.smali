.class public Lcfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private final aMD:Leqo;

.field private final aUk:Lgql;

.field private final aUl:Lcpw;

.field private final aUm:Landroid/database/DataSetObservable;

.field private final aUn:Lckg;

.field private aUo:Livq;

.field private aUp:Lhzu;

.field private aUq:Lhzn;

.field private aUr:Lhzj;

.field private aUs:Lcqq;

.field private aUt:Lghl;

.field private aUu:Lggo;

.field private aUv:Lddv;

.field private aUw:Lebk;

.field private final aqp:Ljava/lang/Object;

.field private mActionDiscoveryData:Lgpc;

.field private final mAlarmHelper:Ldjx;

.field private mAlertQueue:Ljava/util/Queue;

.field private mAppSelectionHelper:Libo;

.field private final mAssistContextHelper:Lckx;

.field private final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private final mContext:Landroid/content/Context;

.field private final mCookies:Lgpf;

.field private mCookiesLock:Ldkq;

.field private final mCorpora:Lcfu;

.field private final mDeviceCapabilityManager:Lenm;

.field private mDiscourseContext:Lcky;

.field private final mExtraDexReg:Lgtx;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mGooglePlayServicesHelper:Lcha;

.field private mGsaConfigFlags:Lchk;

.field private final mHttpHelper:Ldkx;

.field private final mLocationSettings:Lcob;

.field private final mLoginHelper:Lcrh;

.field private mMessageManager:Lbxy;

.field private mNetworkInfo:Lgno;

.field private final mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mNowOptInSettings:Lcin;

.field private final mPendingIntentFactory:Lfda;

.field private mPinholeParamsBuilder:Lhzl;

.field private final mPredictiveCardsPreferences:Lcxs;

.field private mRecentAlerts:Ljava/util/Queue;

.field private mRecentContextApiClient:Lclh;

.field private final mRlzHelper:Lcoy;

.field private mSdchManager:Lczz;

.field private final mSearchBoxLogging:Lcpd;

.field private final mSettings:Lcke;

.field private final mStaticContentCache:Lcpq;

.field private mUrlHelper:Lcpn;

.field private mUserInteractionLogger:Lcpx;

.field private mVelvetBackgroundTasks:Lgpp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgql;Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p3, p0, Lcfo;->aqp:Ljava/lang/Object;

    .line 164
    iput-object p1, p0, Lcfo;->mContext:Landroid/content/Context;

    .line 165
    iput-object p2, p0, Lcfo;->aUk:Lgql;

    .line 166
    iget-object v0, p2, Lgql;->mClock:Lemp;

    iput-object v0, p0, Lcfo;->mClock:Lemp;

    .line 167
    iget-object v0, p2, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lcfo;->aMD:Leqo;

    .line 168
    iget-object v0, p2, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 169
    new-instance v0, Lcjs;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v2}, Lgql;->aJs()Lchr;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcjs;-><init>(Landroid/content/Context;Lchr;)V

    iput-object v0, p0, Lcfo;->mConfig:Lcjs;

    .line 170
    new-instance v0, Lckf;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v2}, Lgql;->aJs()Lchr;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lckf;-><init>(Landroid/content/Context;Lchr;)V

    iput-object v0, p0, Lcfo;->mSettings:Lcke;

    .line 171
    new-instance v0, Lcpw;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcpw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcfo;->aUl:Lcpw;

    .line 172
    new-instance v0, Lckg;

    iget-object v1, p0, Lcfo;->mConfig:Lcjs;

    iget-object v2, p0, Lcfo;->mSettings:Lcke;

    invoke-direct {v0, p1, v1, v2}, Lckg;-><init>(Landroid/content/Context;Lcjs;Lcke;)V

    iput-object v0, p0, Lcfo;->aUn:Lckg;

    .line 173
    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1}, Lgtx;->b(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)Lgtx;

    move-result-object v0

    iput-object v0, p0, Lcfo;->mExtraDexReg:Lgtx;

    .line 174
    iget-object v0, p0, Lcfo;->mConfig:Lcjs;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v2

    new-instance v1, Lcfp;

    invoke-direct {v1, v0, v2}, Lcfp;-><init>(Lcjs;Lchk;)V

    new-instance v5, Lcfz;

    iget-object v0, p0, Lcfo;->aUn:Lckg;

    iget-object v2, p0, Lcfo;->aUk:Lgql;

    iget-object v2, v2, Lgql;->mAsyncServices:Lema;

    iget-object v3, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-direct {v5, v1, v0, v2, v3}, Lcfz;-><init>(Ldla;Ligi;Lern;Landroid/content/Context;)V

    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    new-instance v0, Ldkx;

    iget-object v2, p0, Lcfo;->aUl:Lcpw;

    iget-object v3, p0, Lcfo;->aUn:Lckg;

    invoke-direct/range {v0 .. v5}, Ldkx;-><init>(Ldla;Ldlf;Ligi;Landroid/net/ConnectivityManager;Lbzj;)V

    iput-object v0, p0, Lcfo;->mHttpHelper:Ldkx;

    .line 175
    new-instance v0, Lcoy;

    iget-object v1, p0, Lcfo;->aUk:Lgql;

    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iget-object v2, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcfo;->mConfig:Lcjs;

    invoke-direct {v0, v1, v2, v3}, Lcoy;-><init>(Ljava/util/concurrent/Executor;Landroid/content/Context;Lcjs;)V

    iput-object v0, p0, Lcfo;->mRlzHelper:Lcoy;

    .line 176
    new-instance v0, Lcnu;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcfo;->aMD:Leqo;

    iget-object v3, p0, Lcfo;->aUk:Lgql;

    iget-object v3, v3, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcnu;-><init>(Landroid/content/Context;Leqo;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcfo;->mLocationSettings:Lcob;

    .line 177
    new-instance v0, Lcha;

    iget-object v1, p0, Lcfo;->aUk:Lgql;

    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iget-object v2, p0, Lcfo;->aMD:Leqo;

    iget-object v3, p0, Lcfo;->mClock:Lemp;

    invoke-direct {v0, p1, v1, v2, v3}, Lcha;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lemp;)V

    iput-object v0, p0, Lcfo;->mGooglePlayServicesHelper:Lcha;

    .line 180
    new-instance v0, Lcrh;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcfo;->mSettings:Lcke;

    iget-object v3, p0, Lcfo;->aUk:Lgql;

    iget-object v3, v3, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    iget-object v4, p0, Lcfo;->aUk:Lgql;

    iget-object v4, v4, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    new-instance v5, Lcqz;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v6

    iget-object v7, p0, Lcfo;->mClock:Lemp;

    new-instance v8, Lcrc;

    new-instance v9, Lcrg;

    invoke-direct {v9}, Lcrg;-><init>()V

    new-instance v10, Lcqy;

    invoke-virtual {p0}, Lcfo;->DB()Landroid/accounts/AccountManager;

    move-result-object v11

    invoke-direct {v10, v11}, Lcqy;-><init>(Landroid/accounts/AccountManager;)V

    iget-object v11, p0, Lcfo;->mGooglePlayServicesHelper:Lcha;

    invoke-direct {v8, v9, v10, v11}, Lcrc;-><init>(Lcrf;Lcrf;Lcha;)V

    invoke-direct {v5, v6, v7, v8}, Lcqz;-><init>(Lchk;Lemp;Lcrf;)V

    invoke-virtual {p0}, Lcfo;->DB()Landroid/accounts/AccountManager;

    move-result-object v6

    new-instance v7, Lcfr;

    invoke-direct {v7, p0}, Lcfr;-><init>(Lcfo;)V

    invoke-direct/range {v0 .. v7}, Lcrh;-><init>(Landroid/content/Context;Lcke;Leqo;Ljava/util/concurrent/Executor;Lcrf;Landroid/accounts/AccountManager;Ligi;)V

    iput-object v0, p0, Lcfo;->mLoginHelper:Lcrh;

    .line 181
    new-instance v0, Lcpd;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v1

    iget-object v2, p0, Lcfo;->mHttpHelper:Ldkx;

    iget-object v3, p0, Lcfo;->aUk:Lgql;

    iget-object v3, v3, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aut()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    iget-object v4, p0, Lcfo;->mClock:Lemp;

    iget-object v5, p0, Lcfo;->mSettings:Lcke;

    iget-object v6, p0, Lcfo;->mLocationSettings:Lcob;

    iget-object v7, p0, Lcfo;->mLoginHelper:Lcrh;

    invoke-direct/range {v0 .. v7}, Lcpd;-><init>(Lchk;Ldkx;Ljava/util/concurrent/Executor;Lemp;Lcke;Lcob;Lcrh;)V

    iput-object v0, p0, Lcfo;->mSearchBoxLogging:Lcpd;

    .line 182
    iget-object v0, p0, Lcfo;->aUk:Lgql;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iget-object v1, p0, Lcfo;->mGsaConfigFlags:Lchk;

    invoke-static {p1, v0, v1, p0}, Lgpf;->a(Landroid/content/Context;Lema;Lchk;Lcfo;)Lgpf;

    move-result-object v0

    iput-object v0, p0, Lcfo;->mCookies:Lgpf;

    .line 183
    new-instance v0, Lcxs;

    invoke-virtual {p2}, Lgql;->aJs()Lchr;

    move-result-object v1

    invoke-direct {v0, v1}, Lcxs;-><init>(Lchr;)V

    iput-object v0, p0, Lcfo;->mPredictiveCardsPreferences:Lcxs;

    .line 185
    new-instance v0, Lcfu;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcfo;->mSettings:Lcke;

    iget-object v3, p0, Lcfo;->aMD:Leqo;

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcfu;-><init>(Landroid/content/Context;Lcke;Ljava/util/concurrent/Executor;I)V

    iput-object v0, p0, Lcfo;->mCorpora:Lcfu;

    .line 186
    iget-object v0, p0, Lcfo;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->ed()V

    .line 187
    new-instance v0, Ldjx;

    iget-object v1, p0, Lcfo;->mClock:Lemp;

    invoke-virtual {p2}, Lgql;->aJs()Lchr;

    move-result-object v2

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-direct {v0, p1, v1, v2, v3}, Ldjx;-><init>(Landroid/content/Context;Lemp;Lchr;Ljava/util/Random;)V

    iput-object v0, p0, Lcfo;->mAlarmHelper:Ldjx;

    .line 191
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcfo;->aUm:Landroid/database/DataSetObservable;

    .line 192
    new-instance v0, Lenn;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lenn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcfo;->mDeviceCapabilityManager:Lenm;

    .line 193
    new-instance v0, Lfdm;

    invoke-direct {v0, p1}, Lfdm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcfo;->mPendingIntentFactory:Lfda;

    .line 194
    new-instance v0, Leue;

    iget-object v1, p0, Lcfo;->aUk:Lgql;

    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    iget-object v2, p0, Lcfo;->mLoginHelper:Lcrh;

    iget-object v3, p0, Lcfo;->mClock:Lemp;

    invoke-direct {v0, p1, v1, v2, v3}, Leue;-><init>(Landroid/content/Context;Lerk;Lcrh;Lemp;)V

    iput-object v0, p0, Lcfo;->mGmsLocationReportingHelper:Leue;

    .line 196
    new-instance v0, Lcio;

    iget-object v1, p0, Lcfo;->aUk:Lgql;

    iget-object v2, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v3}, Lgql;->aJs()Lchr;

    move-result-object v3

    iget-object v4, p0, Lcfo;->mClock:Lemp;

    iget-object v5, p0, Lcfo;->aUk:Lgql;

    iget-object v5, v5, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    iget-object v6, p0, Lcfo;->mLoginHelper:Lcrh;

    iget-object v7, p0, Lcfo;->mConfig:Lcjs;

    iget-object v8, p0, Lcfo;->mPredictiveCardsPreferences:Lcxs;

    iget-object v9, p0, Lcfo;->mGmsLocationReportingHelper:Leue;

    invoke-direct/range {v0 .. v9}, Lcio;-><init>(Lgql;Landroid/content/Context;Lchr;Lemp;Ljava/util/concurrent/Executor;Lcrh;Lcjs;Lcxs;Leue;)V

    iput-object v0, p0, Lcfo;->mNowOptInSettings:Lcin;

    .line 197
    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    new-instance v2, Lckx;

    iget-object v3, p0, Lcfo;->mGooglePlayServicesHelper:Lcha;

    invoke-direct {v2, v1, v0, v3}, Lckx;-><init>(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;Lcha;)V

    iput-object v2, p0, Lcfo;->mAssistContextHelper:Lckx;

    .line 198
    new-instance v0, Leud;

    iget-object v1, p0, Lcfo;->mPredictiveCardsPreferences:Lcxs;

    iget-object v2, p0, Lcfo;->mLoginHelper:Lcrh;

    iget-object v3, p0, Lcfo;->mGmsLocationReportingHelper:Leue;

    invoke-direct {v0, v1, v2, v3}, Leud;-><init>(Lcxs;Lcrh;Leue;)V

    .line 200
    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, Lcfo;->mGsaConfigFlags:Lchk;

    invoke-static {v0, v1, v2}, Lcpq;->a(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lchk;)Lcpq;

    move-result-object v0

    iput-object v0, p0, Lcfo;->mStaticContentCache:Lcpq;

    .line 202
    return-void
.end method

.method private DJ()Lclh;
    .locals 5

    .prologue
    .line 413
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 414
    :try_start_0
    iget-object v0, p0, Lcfo;->mRecentContextApiClient:Lclh;

    if-nez v0, :cond_0

    .line 415
    new-instance v0, Lclh;

    iget-object v2, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcfo;->aUk:Lgql;

    iget-object v3, v3, Lgql;->mAsyncServices:Lema;

    const-string v4, "ctx-api"

    invoke-virtual {v3, v4}, Lema;->ld(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lclh;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v0, p0, Lcfo;->mRecentContextApiClient:Lclh;

    .line 417
    :cond_0
    iget-object v0, p0, Lcfo;->mRecentContextApiClient:Lclh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private Ec()Lhzn;
    .locals 7

    .prologue
    .line 550
    iget-object v6, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 551
    :try_start_0
    iget-object v0, p0, Lcfo;->aUq:Lhzn;

    if-nez v0, :cond_0

    .line 552
    new-instance v0, Lhzn;

    invoke-virtual {p0}, Lcfo;->DI()Lcpn;

    move-result-object v1

    iget-object v2, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v2}, Lgql;->El()Lfdr;

    move-result-object v2

    iget-object v3, p0, Lcfo;->aUk:Lgql;

    iget-object v3, v3, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    iget-object v4, p0, Lcfo;->mLocationSettings:Lcob;

    iget-object v5, p0, Lcfo;->mGsaConfigFlags:Lchk;

    invoke-direct/range {v0 .. v5}, Lhzn;-><init>(Lcpn;Lfdr;Ljava/util/concurrent/ExecutorService;Lcob;Lchk;)V

    iput-object v0, p0, Lcfo;->aUq:Lhzn;

    .line 557
    :cond_0
    iget-object v0, p0, Lcfo;->aUq:Lhzn;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 558
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method private Eh()Lghl;
    .locals 6

    .prologue
    .line 608
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 609
    :try_start_0
    iget-object v0, p0, Lcfo;->aUt:Lghl;

    if-nez v0, :cond_0

    .line 610
    new-instance v0, Lghl;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lcfo;->mSettings:Lcke;

    new-instance v4, Lghj;

    iget-object v5, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v4, v5}, Lghj;-><init>(Landroid/content/ContentResolver;)V

    iget-object v5, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v2, v3, v4, v5}, Lghl;-><init>(Lchk;Lcke;Lghj;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcfo;->aUt:Lghl;

    .line 615
    :cond_0
    iget-object v0, p0, Lcfo;->aUt:Lghl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private Ei()Lggo;
    .locals 5

    .prologue
    .line 620
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 621
    :try_start_0
    iget-object v0, p0, Lcfo;->aUu:Lggo;

    if-nez v0, :cond_0

    .line 622
    new-instance v0, Lggo;

    iget-object v2, p0, Lcfo;->mSettings:Lcke;

    new-instance v3, Lggk;

    iget-object v4, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v3, v4}, Lggk;-><init>(Landroid/content/ContentResolver;)V

    iget-object v4, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v2, v3, v4}, Lggo;-><init>(Lcke;Lggk;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcfo;->aUu:Lggo;

    .line 627
    :cond_0
    iget-object v0, p0, Lcfo;->aUu:Lggo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 628
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final BK()Lcke;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcfo;->mSettings:Lcke;

    return-object v0
.end method

.method public final declared-synchronized BL()Lchk;
    .locals 2

    .prologue
    .line 675
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcfo;->mGsaConfigFlags:Lchk;

    if-nez v0, :cond_0

    .line 676
    new-instance v0, Lchk;

    iget-object v1, p0, Lcfo;->mSettings:Lcke;

    invoke-direct {v0, v1}, Lchk;-><init>(Lcke;)V

    iput-object v0, p0, Lcfo;->mGsaConfigFlags:Lchk;

    .line 678
    :cond_0
    iget-object v0, p0, Lcfo;->mGsaConfigFlags:Lchk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final DB()Landroid/accounts/AccountManager;
    .locals 2

    .prologue
    .line 325
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 326
    :try_start_0
    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final DC()Lemp;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcfo;->mClock:Lemp;

    return-object v0
.end method

.method public final DD()Lcjs;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcfo;->mConfig:Lcjs;

    return-object v0
.end method

.method public final DE()Lcxs;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcfo;->mPredictiveCardsPreferences:Lcxs;

    return-object v0
.end method

.method public final DF()Lcin;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcfo;->mNowOptInSettings:Lcin;

    return-object v0
.end method

.method public final DG()Ldkx;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcfo;->mHttpHelper:Ldkx;

    return-object v0
.end method

.method public final DH()Lcoy;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcfo;->mRlzHelper:Lcoy;

    return-object v0
.end method

.method public final DI()Lcpn;
    .locals 24

    .prologue
    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcfo;->aqp:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    .line 405
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcfo;->mUrlHelper:Lcpn;

    if-nez v1, :cond_0

    .line 406
    new-instance v12, Lcfq;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcfq;-><init>(Lcfo;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcfo;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcfo;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/media/AudioManager;

    new-instance v6, Lidh;

    invoke-virtual/range {p0 .. p0}, Lcfo;->BL()Lchk;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcfo;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcfo;->mLoginHelper:Lcrh;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcfo;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v4

    invoke-direct {v6, v1, v2, v3, v4}, Lidh;-><init>(Lchk;Lcke;Lcrh;Ldyv;)V

    new-instance v1, Lcla;

    invoke-virtual/range {p0 .. p0}, Lcfo;->Eg()Ligi;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcfo;->mClock:Lemp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcfo;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcfo;->mAssistContextHelper:Lckx;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcfo;->mContext:Landroid/content/Context;

    invoke-direct/range {p0 .. p0}, Lcfo;->DJ()Lclh;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcfo;->mLoginHelper:Lcrh;

    invoke-virtual/range {p0 .. p0}, Lcfo;->BL()Lchk;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lcla;-><init>(Ligi;Lemp;Landroid/content/res/Resources;Lckx;Lidh;Landroid/content/Context;Lclh;Landroid/media/AudioManager;Lcrh;Lchk;)V

    new-instance v2, Lcpn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcfo;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcfo;->mConfig:Lcjs;

    invoke-virtual/range {p0 .. p0}, Lcfo;->BL()Lchk;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcfo;->mPredictiveCardsPreferences:Lcxs;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcfo;->mClock:Lemp;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcfo;->mCorpora:Lcfu;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcfo;->aUk:Lgql;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcfo;->mSearchBoxLogging:Lcpd;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcfo;->mRlzHelper:Lcoy;

    new-instance v13, Lcoe;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcfo;->mContext:Landroid/content/Context;

    invoke-direct {v13, v14}, Lcoe;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcfo;->mCookies:Lgpf;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcfo;->aUk:Lgql;

    invoke-virtual {v15}, Lgql;->aJq()Lhhq;

    move-result-object v15

    iget-object v15, v15, Lhhq;->mSettings:Lhym;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcfo;->mLoginHelper:Lcrh;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJh()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcfo;->aUn:Lckg;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcfo;->mStaticContentCache:Lcpq;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcfo;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v22

    move-object/from16 v16, v1

    invoke-direct/range {v2 .. v22}, Lcpn;-><init>(Lcke;Lcjs;Lchk;Lcxs;Lemp;Lcfu;Lgql;Lcpd;Lcoy;Ligi;Lcoe;Lgpf;Lhym;Lcla;Lcrh;Ljava/lang/String;Ligi;Lcpq;Landroid/content/res/Resources;Lckw;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcfo;->mUrlHelper:Lcpn;

    .line 408
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcfo;->mUrlHelper:Lcpn;

    monitor-exit v23
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    .line 409
    :catchall_0
    move-exception v1

    monitor-exit v23

    throw v1
.end method

.method public final DK()Ldkq;
    .locals 3

    .prologue
    .line 422
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 423
    :try_start_0
    iget-object v0, p0, Lcfo;->mCookiesLock:Ldkq;

    if-nez v0, :cond_0

    .line 424
    new-instance v0, Ldkq;

    iget-object v2, p0, Lcfo;->aMD:Leqo;

    invoke-direct {v0, v2}, Ldkq;-><init>(Leqo;)V

    iput-object v0, p0, Lcfo;->mCookiesLock:Ldkq;

    .line 426
    :cond_0
    iget-object v0, p0, Lcfo;->mCookiesLock:Ldkq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final DL()Lcrh;
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcfo;->mLoginHelper:Lcrh;

    return-object v0
.end method

.method public final DM()Lcpw;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcfo;->aUl:Lcpw;

    return-object v0
.end method

.method public final DN()Landroid/database/DataSetObservable;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcfo;->aUm:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method public DO()Lgpp;
    .locals 11

    .prologue
    .line 447
    iget-object v10, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v10

    .line 448
    :try_start_0
    iget-object v0, p0, Lcfo;->mVelvetBackgroundTasks:Lgpp;

    if-nez v0, :cond_0

    .line 449
    new-instance v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v1, p0, Lcfo;->mClock:Lemp;

    iget-object v2, p0, Lcfo;->mConfig:Lcjs;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v3

    iget-object v4, p0, Lcfo;->mSettings:Lcke;

    iget-object v5, p0, Lcfo;->aUk:Lgql;

    iget-object v5, v5, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->aut()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    iget-object v6, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v6}, Lgql;->aJU()Lgpu;

    move-result-object v6

    iget-object v7, p0, Lcfo;->mAlarmHelper:Ldjx;

    iget-object v8, p0, Lcfo;->mPendingIntentFactory:Lfda;

    iget-object v9, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;-><init>(Lemp;Lcjs;Lchk;Lcke;Ljava/util/concurrent/Executor;Lgpu;Ldjx;Lfda;Landroid/content/Context;)V

    iput-object v0, p0, Lcfo;->mVelvetBackgroundTasks:Lgpp;

    .line 452
    :cond_0
    iget-object v0, p0, Lcfo;->mVelvetBackgroundTasks:Lgpp;

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0
.end method

.method public final DP()Lcob;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcfo;->mLocationSettings:Lcob;

    return-object v0
.end method

.method public final DQ()Lcpd;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcfo;->mSearchBoxLogging:Lcpd;

    return-object v0
.end method

.method public final DR()Lcpx;
    .locals 6

    .prologue
    .line 465
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 466
    :try_start_0
    iget-object v0, p0, Lcfo;->mUserInteractionLogger:Lcpx;

    if-nez v0, :cond_0

    .line 467
    new-instance v0, Lcpx;

    iget-object v2, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcfo;->mClock:Lemp;

    iget-object v4, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v4}, Lgql;->aJs()Lchr;

    move-result-object v4

    iget-object v5, p0, Lcfo;->mConfig:Lcjs;

    invoke-direct {v0, v2, v3, v4, v5}, Lcpx;-><init>(Landroid/content/Context;Lemp;Lchr;Lcjs;)V

    iput-object v0, p0, Lcfo;->mUserInteractionLogger:Lcpx;

    .line 469
    :cond_0
    iget-object v0, p0, Lcfo;->mUserInteractionLogger:Lcpx;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 470
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final DS()Lckg;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcfo;->aUn:Lckg;

    return-object v0
.end method

.method public final DT()Lgpf;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcfo;->mCookies:Lgpf;

    return-object v0
.end method

.method public final DU()Lcfu;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcfo;->mCorpora:Lcfu;

    return-object v0
.end method

.method public final DV()Livq;
    .locals 3

    .prologue
    .line 486
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 487
    :try_start_0
    iget-object v0, p0, Lcfo;->aUo:Livq;

    if-nez v0, :cond_0

    .line 488
    iget-object v0, p0, Lcfo;->mExtraDexReg:Lgtx;

    sget-object v2, Lguc;->ctv:Lgor;

    invoke-virtual {v0, v2}, Lgtx;->c(Lgor;)Livq;

    move-result-object v0

    new-instance v2, Lcfs;

    invoke-direct {v2, p0}, Lcfs;-><init>(Lcfo;)V

    invoke-static {v0, v2}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    iput-object v0, p0, Lcfo;->aUo:Livq;

    .line 496
    :cond_0
    iget-object v0, p0, Lcfo;->aUo:Livq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final DW()Lcpq;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcfo;->mStaticContentCache:Lcpq;

    return-object v0
.end method

.method public final DX()Lenm;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcfo;->mDeviceCapabilityManager:Lenm;

    return-object v0
.end method

.method public final DY()Lgno;
    .locals 5

    .prologue
    .line 509
    iget-object v2, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v2

    .line 510
    :try_start_0
    iget-object v0, p0, Lcfo;->mNetworkInfo:Lgno;

    if-nez v0, :cond_0

    .line 511
    new-instance v3, Lgno;

    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcfo;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-direct {v3, v0, v1}, Lgno;-><init>(Landroid/telephony/TelephonyManager;Landroid/net/ConnectivityManager;)V

    iput-object v3, p0, Lcfo;->mNetworkInfo:Lgno;

    .line 518
    :cond_0
    iget-object v0, p0, Lcfo;->mNetworkInfo:Lgno;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 519
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final DZ()Lczz;
    .locals 7

    .prologue
    .line 523
    iget-object v6, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 524
    :try_start_0
    iget-object v0, p0, Lcfo;->mSdchManager:Lczz;

    if-nez v0, :cond_0

    .line 525
    new-instance v2, Lczu;

    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, Lcfo;->mClock:Lemp;

    invoke-direct {v2, v0, v1, v3}, Lczu;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lemp;)V

    .line 527
    new-instance v1, Lczx;

    iget-object v0, p0, Lcfo;->mHttpHelper:Ldkx;

    iget-object v3, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v1, v0, v3}, Lczx;-><init>(Ldkx;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 528
    new-instance v0, Lczz;

    iget-object v3, p0, Lcfo;->mClock:Lemp;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v4

    invoke-virtual {p0}, Lcfo;->DY()Lgno;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lczz;-><init>(Lczx;Lczu;Lemp;Lchk;Lgno;)V

    iput-object v0, p0, Lcfo;->mSdchManager:Lczz;

    .line 532
    :cond_0
    iget-object v0, p0, Lcfo;->mSdchManager:Lczz;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 533
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final Ea()Ldjx;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcfo;->mAlarmHelper:Ldjx;

    return-object v0
.end method

.method public final Eb()Lfda;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcfo;->mPendingIntentFactory:Lfda;

    return-object v0
.end method

.method public final Ed()Lhzl;
    .locals 6

    .prologue
    .line 562
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 563
    :try_start_0
    iget-object v0, p0, Lcfo;->mPinholeParamsBuilder:Lhzl;

    if-nez v0, :cond_0

    .line 564
    new-instance v0, Lhzl;

    invoke-virtual {p0}, Lcfo;->DI()Lcpn;

    move-result-object v2

    invoke-virtual {p0}, Lcfo;->DZ()Lczz;

    move-result-object v3

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v4

    invoke-direct {p0}, Lcfo;->Ec()Lhzn;

    move-result-object v5

    invoke-direct {v0, v2, v3, v4, v5}, Lhzl;-><init>(Lcpn;Lczz;Lchk;Lhzn;)V

    iput-object v0, p0, Lcfo;->mPinholeParamsBuilder:Lhzl;

    .line 567
    :cond_0
    iget-object v0, p0, Lcfo;->mPinholeParamsBuilder:Lhzl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 568
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ee()Lhzj;
    .locals 3

    .prologue
    .line 572
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    :try_start_0
    iget-object v0, p0, Lcfo;->aUr:Lhzj;

    if-nez v0, :cond_0

    .line 574
    new-instance v0, Lhzj;

    invoke-virtual {p0}, Lcfo;->DI()Lcpn;

    move-result-object v2

    invoke-direct {v0, v2}, Lhzj;-><init>(Lcpn;)V

    iput-object v0, p0, Lcfo;->aUr:Lhzj;

    .line 576
    :cond_0
    iget-object v0, p0, Lcfo;->aUr:Lhzj;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ef()Lgpc;
    .locals 8

    .prologue
    .line 581
    iget-object v6, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 582
    :try_start_0
    iget-object v0, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    .line 584
    invoke-virtual {v0}, Lhym;->aTF()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v5

    .line 588
    :goto_0
    iget-object v0, p0, Lcfo;->mActionDiscoveryData:Lgpc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfo;->mActionDiscoveryData:Lgpc;

    invoke-virtual {v0, v5}, Lgpc;->isSupported(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 589
    :cond_0
    new-instance v0, Lgpc;

    iget-object v1, p0, Lcfo;->mConfig:Lcjs;

    iget-object v2, p0, Lcfo;->mSettings:Lcke;

    new-instance v3, Ldmb;

    iget-object v4, p0, Lcfo;->mHttpHelper:Ldkx;

    const/16 v7, 0xc

    invoke-direct {v3, v4, v7}, Ldmb;-><init>(Ldkx;I)V

    iget-object v4, p0, Lcfo;->aUk:Lgql;

    iget-object v4, v4, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lgpc;-><init>(Lcjs;Lcke;Lesm;Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    iput-object v0, p0, Lcfo;->mActionDiscoveryData:Lgpc;

    .line 594
    :cond_1
    iget-object v0, p0, Lcfo;->mActionDiscoveryData:Lgpc;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 584
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 595
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final Eg()Ligi;
    .locals 1

    .prologue
    .line 599
    new-instance v0, Lcft;

    invoke-direct {v0, p0}, Lcft;-><init>(Lcfo;)V

    return-object v0
.end method

.method final Ej()Lcky;
    .locals 4

    .prologue
    .line 632
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 633
    :try_start_0
    iget-object v0, p0, Lcfo;->mDiscourseContext:Lcky;

    if-nez v0, :cond_1

    .line 634
    new-instance v0, Lcky;

    invoke-direct {v0}, Lcky;-><init>()V

    iput-object v0, p0, Lcfo;->mDiscourseContext:Lcky;

    .line 635
    new-instance v0, Lght;

    iget-object v2, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcfo;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v2, v3}, Lght;-><init>(Landroid/content/ContentResolver;Ljava/util/concurrent/Executor;)V

    .line 637
    iget-object v2, p0, Lcfo;->mDiscourseContext:Lcky;

    invoke-virtual {v0}, Lght;->aFM()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcky;->bav:Ljava/lang/Boolean;

    .line 639
    iget-object v0, p0, Lcfo;->mDiscourseContext:Lcky;

    invoke-direct {p0}, Lcfo;->Eh()Lghl;

    move-result-object v2

    invoke-virtual {v2}, Lghl;->aFL()Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, Lcky;->baw:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, v0, Lcky;->baw:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 640
    :cond_0
    iget-object v0, p0, Lcfo;->mDiscourseContext:Lcky;

    invoke-direct {p0}, Lcfo;->Ei()Lggo;

    move-result-object v2

    invoke-virtual {v2}, Lggo;->aFL()Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, Lcky;->bax:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, v0, Lcky;->bax:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 643
    :cond_1
    iget-object v0, p0, Lcfo;->mDiscourseContext:Lcky;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 644
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ek()Lccz;
    .locals 4

    .prologue
    .line 648
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 649
    :try_start_0
    iget-object v0, p0, Lcfo;->aUp:Lhzu;

    if-nez v0, :cond_0

    .line 650
    new-instance v0, Lhzu;

    iget-object v2, p0, Lcfo;->mHttpHelper:Ldkx;

    const/16 v3, 0xe

    invoke-direct {v0, v2, v3}, Lhzu;-><init>(Ldkx;I)V

    iput-object v0, p0, Lcfo;->aUp:Lhzu;

    .line 654
    :cond_0
    iget-object v0, p0, Lcfo;->aUp:Lhzu;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 655
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final El()Lfdr;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcfo;->aUk:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v0, v0, Lfdb;->mLocationOracle:Lfdr;

    return-object v0
.end method

.method public final Em()Lcha;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcfo;->mGooglePlayServicesHelper:Lcha;

    return-object v0
.end method

.method public final En()Leue;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcfo;->mGmsLocationReportingHelper:Leue;

    return-object v0
.end method

.method public final Eo()Lckx;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcfo;->mAssistContextHelper:Lckx;

    return-object v0
.end method

.method public final Ep()Ljava/util/Queue;
    .locals 2

    .prologue
    .line 697
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 698
    :try_start_0
    iget-object v0, p0, Lcfo;->mAlertQueue:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 699
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcfo;->mAlertQueue:Ljava/util/Queue;

    .line 701
    :cond_0
    iget-object v0, p0, Lcfo;->mAlertQueue:Ljava/util/Queue;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 702
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Eq()Lebk;
    .locals 7

    .prologue
    .line 706
    iget-object v6, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 707
    :try_start_0
    iget-object v0, p0, Lcfo;->aUw:Lebk;

    if-nez v0, :cond_0

    .line 708
    new-instance v0, Lebk;

    iget-object v1, p0, Lcfo;->aUk:Lgql;

    iget-object v1, v1, Lgql;->mAppContext:Landroid/content/Context;

    const-string v2, "android-search-assist"

    const-string v3, "assistant-query-entry"

    sget-object v4, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPt:Lcom/google/android/search/shared/service/ClientConfig;

    iget-object v5, p0, Lcfo;->aUk:Lgql;

    iget-object v5, v5, Lgql;->mAsyncServices:Lema;

    invoke-direct/range {v0 .. v5}, Lebk;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/search/shared/service/ClientConfig;Lerp;)V

    iput-object v0, p0, Lcfo;->aUw:Lebk;

    .line 710
    :cond_0
    iget-object v0, p0, Lcfo;->aUw:Lebk;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final Er()Ljava/util/Queue;
    .locals 2

    .prologue
    .line 724
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 725
    :try_start_0
    iget-object v0, p0, Lcfo;->mRecentAlerts:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 726
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcfo;->mRecentAlerts:Ljava/util/Queue;

    .line 728
    :cond_0
    iget-object v0, p0, Lcfo;->mRecentAlerts:Ljava/util/Queue;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 729
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Es()Libo;
    .locals 2

    .prologue
    .line 733
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 734
    :try_start_0
    iget-object v0, p0, Lcfo;->mAppSelectionHelper:Libo;

    if-nez v0, :cond_0

    .line 735
    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Libo;->bU(Landroid/content/Context;)Libo;

    move-result-object v0

    iput-object v0, p0, Lcfo;->mAppSelectionHelper:Libo;

    .line 737
    :cond_0
    iget-object v0, p0, Lcfo;->mAppSelectionHelper:Libo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 738
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Et()Lcqq;
    .locals 2

    .prologue
    .line 742
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 743
    :try_start_0
    iget-object v0, p0, Lcfo;->aUs:Lcqq;

    if-nez v0, :cond_0

    .line 744
    new-instance v0, Lcqq;

    invoke-direct {v0, p0}, Lcqq;-><init>(Lcfo;)V

    iput-object v0, p0, Lcfo;->aUs:Lcqq;

    .line 746
    :cond_0
    iget-object v0, p0, Lcfo;->aUs:Lcqq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 747
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Eu()Lddv;
    .locals 4

    .prologue
    .line 751
    iget-object v1, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 752
    :try_start_0
    iget-object v0, p0, Lcfo;->aUv:Lddv;

    if-nez v0, :cond_0

    .line 753
    new-instance v0, Lddv;

    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lcfo;->mClock:Lemp;

    invoke-direct {v0, v2, v3}, Lddv;-><init>(Lchk;Lemp;)V

    iput-object v0, p0, Lcfo;->aUv:Lddv;

    .line 756
    :cond_0
    iget-object v0, p0, Lcfo;->aUv:Lddv;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 757
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ev()Lgtx;
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcfo;->mExtraDexReg:Lgtx;

    return-object v0
.end method

.method public final a(Ldda;)Lbxy;
    .locals 12

    .prologue
    .line 682
    iget-object v11, p0, Lcfo;->aqp:Ljava/lang/Object;

    monitor-enter v11

    .line 683
    :try_start_0
    iget-object v0, p0, Lcfo;->mMessageManager:Lbxy;

    if-nez v0, :cond_0

    .line 684
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v7

    .line 685
    iget-object v0, p0, Lcfo;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    .line 687
    new-instance v0, Lbxy;

    invoke-static {}, Lema;->aur()Lema;

    move-result-object v1

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-static {}, Lema;->aur()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    iget-object v3, p0, Lcfo;->mContext:Landroid/content/Context;

    new-instance v5, Lbwo;

    iget-object v6, v7, Lhhq;->mSettings:Lhym;

    iget-object v8, p0, Lcfo;->mClock:Lemp;

    invoke-direct {v5, v6, v8}, Lbwo;-><init>(Lhym;Lemp;)V

    iget-object v8, p0, Lcfo;->mClock:Lemp;

    invoke-virtual {p0}, Lcfo;->Eg()Ligi;

    move-result-object v9

    new-instance v10, Lcom/google/android/handsfree/MessageBuffer;

    invoke-direct {v10}, Lcom/google/android/handsfree/MessageBuffer;-><init>()V

    move-object v6, p1

    invoke-direct/range {v0 .. v10}, Lbxy;-><init>(Leqo;Ljava/util/concurrent/Executor;Landroid/content/Context;Landroid/media/AudioManager;Lbwo;Ldda;Lhhq;Lemp;Ligi;Lcom/google/android/handsfree/MessageBuffer;)V

    iput-object v0, p0, Lcfo;->mMessageManager:Lbxy;

    .line 692
    :cond_0
    iget-object v0, p0, Lcfo;->mMessageManager:Lbxy;

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 693
    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 773
    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 774
    iget-object v0, p0, Lcfo;->mExtraDexReg:Lgtx;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 775
    iget-object v0, p0, Lcfo;->mStaticContentCache:Lcpq;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 776
    invoke-virtual {p0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leti;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 777
    invoke-direct {p0}, Lcfo;->DJ()Lclh;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 778
    return-void
.end method
