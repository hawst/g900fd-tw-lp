.class public final Lhxr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dkw:Lglm;

.field private final mNetworkInformation:Lgno;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lhym;Lglm;Lgno;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lhxr;->mSettings:Lhym;

    .line 25
    iput-object p2, p0, Lhxr;->dkw:Lglm;

    .line 26
    iput-object p3, p0, Lhxr;->mNetworkInformation:Lgno;

    .line 27
    return-void
.end method


# virtual methods
.method public final aTt()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 30
    iget-object v2, p0, Lhxr;->dkw:Lglm;

    invoke-interface {v2}, Lglm;->St()Z

    move-result v2

    if-nez v2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    iget-object v2, p0, Lhxr;->mNetworkInformation:Lgno;

    invoke-virtual {v2}, Lgno;->aAj()I

    move-result v2

    .line 38
    iget-object v3, p0, Lhxr;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aER()Ljze;

    move-result-object v3

    iget-object v3, v3, Ljze;->eNc:Ljzu;

    iget-object v3, v3, Ljzu;->eOt:[I

    .line 41
    invoke-static {v3, v2}, Lius;->d([II)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 52
    :cond_2
    iget-object v2, p0, Lhxr;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aUt()I

    move-result v2

    if-nez v2, :cond_0

    .line 53
    iget-object v2, p0, Lhxr;->mSettings:Lhym;

    if-eqz v0, :cond_3

    const/4 v1, 0x5

    :cond_3
    invoke-virtual {v2, v1}, Lhym;->lE(I)V

    goto :goto_0
.end method
