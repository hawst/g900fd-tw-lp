.class public final Leyu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cma:Leyv;


# instance fields
.field private final cmb:Leyg;

.field private final mAlarmHelper:Ldjx;

.field private final mAppContext:Landroid/content/Context;

.field private final mCalendarDataProvider:Leym;

.field private final mClock:Lemp;

.field private final mEntryNotificationFactory:Lfjs;

.field private final mLocationOracle:Lfdr;

.field private final mNetworkClient:Lfcx;

.field private final mNowNotificationManager:Lfga;

.field private final mPredictiveCardsPreferences:Lcxs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Leyv;

    invoke-direct {v0}, Leyv;-><init>()V

    sput-object v0, Leyu;->cma:Leyv;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Leym;Lemp;Lfdr;Lfcx;Lfga;Lfjs;Ldjx;Lcxs;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Leyu;->mAppContext:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Leyu;->mCalendarDataProvider:Leym;

    .line 101
    iput-object p3, p0, Leyu;->mClock:Lemp;

    .line 102
    new-instance v0, Leyg;

    iget-object v1, p0, Leyu;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Leyg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leyu;->cmb:Leyg;

    .line 103
    iput-object p4, p0, Leyu;->mLocationOracle:Lfdr;

    .line 104
    iput-object p5, p0, Leyu;->mNetworkClient:Lfcx;

    .line 105
    iput-object p6, p0, Leyu;->mNowNotificationManager:Lfga;

    .line 106
    iput-object p7, p0, Leyu;->mEntryNotificationFactory:Lfjs;

    .line 107
    iput-object p8, p0, Leyu;->mAlarmHelper:Ldjx;

    .line 108
    iput-object p9, p0, Leyu;->mPredictiveCardsPreferences:Lcxs;

    .line 109
    return-void
.end method

.method private awG()V
    .locals 5

    .prologue
    .line 509
    iget-object v0, p0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v0}, Leym;->awA()Ljava/lang/Long;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_0
    iget-object v2, p0, Leyu;->mAppContext:Landroid/content/Context;

    sget-object v3, Lfzy;->cEg:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4, v0, v1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->a(Landroid/content/Context;Ljava/lang/String;IJ)V

    .line 514
    return-void

    .line 511
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private f(IJ)V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Leyu;->mAppContext:Landroid/content/Context;

    sget-object v1, Lfzy;->cEf:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->a(Landroid/content/Context;Ljava/lang/String;IJ)V

    .line 396
    return-void
.end method


# virtual methods
.method public final G(Landroid/content/Intent;)V
    .locals 18

    .prologue
    .line 121
    invoke-static {}, Lenu;->auQ()V

    .line 127
    sget-object v2, Lfzy;->cEg:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 128
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v2}, Lcxs;->TH()Lcxi;

    move-result-object v2

    const/4 v3, 0x4

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v9}, Leym;->awB()Ljava/lang/Iterable;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v9, v2

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lamk;

    const/4 v12, -0x1

    if-eq v10, v12, :cond_3

    iget-object v12, v2, Lamk;->aeQ:Lamo;

    invoke-virtual {v12}, Lamo;->nm()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, Leyu;->mClock:Lemp;

    invoke-interface {v14}, Lemp;->currentTimeMillis()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    cmp-long v12, v12, v14

    if-ltz v12, :cond_3

    iget-object v3, v2, Lamk;->aeR:Lamp;

    iget-object v3, v3, Lamp;->afw:Lizj;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lamk;->aeR:Lamp;

    iget-object v7, v3, Lamp;->afw:Lizj;

    :goto_1
    iget-object v3, v7, Lizj;->dUr:Ljcg;

    if-nez v3, :cond_0

    new-instance v3, Ljcg;

    invoke-direct {v3}, Ljcg;-><init>()V

    const/4 v12, 0x2

    invoke-virtual {v3, v12}, Ljcg;->oB(I)Ljcg;

    move-result-object v3

    iput-object v3, v7, Lizj;->dUr:Ljcg;

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Leyu;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v3, v7}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfey;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v8, v0, Leyu;->mNowNotificationManager:Lfga;

    move-object/from16 v0, p0

    iget-object v12, v0, Leyu;->mAppContext:Landroid/content/Context;

    iget-object v13, v2, Lamk;->aeR:Lamp;

    invoke-virtual {v13}, Lamp;->nu()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->k(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v12

    const/4 v13, 0x1

    invoke-interface {v8, v3, v12, v13}, Lfga;->a(Lfey;Landroid/app/PendingIntent;Z)Landroid/app/Notification;

    move-result-object v8

    if-nez v8, :cond_2

    const-string v2, "CalendarIntentHandler"

    const-string v12, "createNotification surprisingly returned null"

    invoke-static {v2, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    const/16 v7, 0xe

    invoke-virtual {v3, v7}, Lizj;->nV(I)Lizj;

    move-result-object v7

    new-instance v3, Lixr;

    invoke-direct {v3}, Lixr;-><init>()V

    iget-object v12, v2, Lamk;->aeR:Lamp;

    invoke-virtual {v12}, Lamp;->nu()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lixr;->qO(Ljava/lang/String;)Lixr;

    move-result-object v3

    iput-object v3, v7, Lizj;->dSf:Lixr;

    goto :goto_1

    :cond_2
    invoke-interface {v3}, Lfey;->ayZ()Lfgb;

    move-result-object v6

    iget-object v4, v2, Lamk;->aeQ:Lamo;

    invoke-virtual {v4}, Lamo;->nm()J

    move-result-wide v4

    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, Leyu;->mCalendarDataProvider:Leym;

    iget-object v2, v2, Lamk;->aeQ:Lamo;

    invoke-virtual {v2}, Lamo;->nh()J

    move-result-wide v12

    invoke-interface {v9, v12, v13}, Leym;->bg(J)Z

    const/4 v2, 0x1

    move v9, v2

    goto/16 :goto_0

    :cond_4
    if-eqz v8, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mNowNotificationManager:Lfga;

    invoke-interface {v2, v6}, Lfga;->a(Lfgb;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mNowNotificationManager:Lfga;

    invoke-interface {v2, v8, v3}, Lfga;->a(Landroid/app/Notification;Lfey;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mNowNotificationManager:Lfga;

    invoke-interface {v2, v7}, Lfga;->p(Lizj;)V

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->mAppContext:Landroid/content/Context;

    sget-object v5, Lfzy;->cEh:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v4, v5, v6, v2, v3}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->a(Landroid/content/Context;Ljava/lang/String;IJ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mNowNotificationManager:Lfga;

    invoke-interface {v2}, Lfga;->azA()V

    :cond_5
    if-eqz v9, :cond_6

    invoke-direct/range {p0 .. p0}, Leyu;->awG()V

    .line 223
    :cond_6
    :goto_2
    return-void

    .line 130
    :cond_7
    sget-object v2, Lfzy;->cEi:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 131
    invoke-static/range {p1 .. p1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->H(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 132
    if-eqz v2, :cond_6

    .line 133
    move-object/from16 v0, p0

    iget-object v3, v0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v3, v2}, Leym;->lE(Ljava/lang/String;)Lamk;

    move-result-object v2

    .line 134
    if-eqz v2, :cond_6

    iget-object v3, v2, Lamk;->aeQ:Lamo;

    if-eqz v3, :cond_6

    iget-object v3, v2, Lamk;->aeQ:Lamo;

    invoke-virtual {v3}, Lamo;->ni()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 135
    move-object/from16 v0, p0

    iget-object v3, v0, Leyu;->mCalendarDataProvider:Leym;

    iget-object v2, v2, Lamk;->aeQ:Lamo;

    invoke-virtual {v2}, Lamo;->nh()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Leym;->bi(J)Z

    goto :goto_2

    .line 140
    :cond_8
    sget-object v2, Lfzy;->cEj:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 141
    new-instance v12, Landroid/content/Intent;

    sget-object v2, Lfzy;->cEk:Ljava/lang/String;

    invoke-direct {v12, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 142
    const-string v2, "title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->cmb:Leyg;

    const-wide/16 v3, -0x1

    const-string v6, "dtstart"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v8, "dtend"

    const-wide/16 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v10, "eventLocation"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "eventTimezone"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v2 .. v11}, Leyg;->a(JLjava/lang/String;JJLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 148
    const-string v2, "android.intent.extra.TITLE"

    invoke-virtual {v12, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 181
    :cond_9
    sget-object v2, Lfzy;->cEh:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mNowNotificationManager:Lfga;

    sget-object v3, Lfgb;->cqy:Lfgb;

    invoke-interface {v2, v3}, Lfga;->a(Lfgb;)V

    goto/16 :goto_2

    .line 189
    :cond_a
    sget-object v2, Lfzy;->cEd:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mAppContext:Landroid/content/Context;

    sget-object v3, Lfzy;->cEe:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->j(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v2, 0x1

    move v3, v2

    .line 193
    :goto_3
    if-eqz v3, :cond_b

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v2}, Leym;->awC()V

    .line 199
    invoke-direct/range {p0 .. p0}, Leyu;->awG()V

    .line 205
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->cmb:Leyg;

    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    const v6, 0x5265c00

    invoke-virtual {v2, v4, v5, v6}, Leyg;->h(JI)Ljava/util/Collection;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->cmb:Leyg;

    invoke-virtual {v4}, Leyg;->aww()Ljava/util/Collection;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Leyu;->mCalendarDataProvider:Leym;

    move-object/from16 v0, p0

    iget-object v6, v0, Leyu;->cmb:Leyg;

    invoke-virtual {v6}, Leyg;->awx()Z

    move-result v6

    invoke-interface {v5, v2, v4, v6}, Leym;->a(Ljava/util/Collection;Ljava/util/Collection;Z)Z

    move-result v2

    .line 209
    if-nez v2, :cond_c

    sget-object v2, Lfzy;->cEf:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 211
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v2}, Leym;->awz()Ljava/lang/Iterable;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v4}, Leym;->awx()Z

    move-result v4

    invoke-static {v2}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_10

    const/4 v2, 0x0

    :goto_4
    if-nez v2, :cond_12

    .line 214
    :cond_d
    :goto_5
    if-nez v3, :cond_e

    sget-object v2, Lfzy;->cEe:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 218
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Leyu;->mAlarmHelper:Ldjx;

    const-wide/32 v4, 0xdbba0

    move-object/from16 v0, p0

    iget-object v2, v0, Leyu;->mAppContext:Landroid/content/Context;

    sget-object v6, Lfzy;->cEe:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    const-string v7, "Calendar_alarm_UPDATE_CALENDAR_ACTION"

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Ldjx;->a(JLandroid/app/PendingIntent;Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 189
    :cond_f
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_3

    .line 211
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Leyu;->mLocationOracle:Lfdr;

    invoke-interface {v5}, Lfdr;->tv()Z

    move-result v5

    if-nez v5, :cond_11

    const-string v2, "CalendarIntentHandler"

    const-string v4, "All locations are stale; not sending request to server"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_4

    :cond_11
    new-instance v5, Liyb;

    invoke-direct {v5}, Liyb;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljim;

    invoke-interface {v2, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljim;

    iput-object v2, v5, Liyb;->dPk:[Ljim;

    invoke-virtual {v5, v4}, Liyb;->hl(Z)Liyb;

    new-instance v4, Lizm;

    invoke-direct {v4}, Lizm;-><init>()V

    iput-object v5, v4, Lizm;->dUG:Liyb;

    new-instance v2, Ljbj;

    invoke-direct {v2}, Ljbj;-><init>()V

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Ljbj;->om(I)Ljbj;

    move-result-object v2

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    const/16 v7, 0xe

    aput v7, v5, v6

    iput-object v5, v2, Ljbj;->dYx:[I

    const/4 v5, 0x1

    new-array v5, v5, [Ljbj;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    iput-object v5, v4, Lizm;->dUH:[Ljbj;

    const/16 v2, 0xf

    invoke-static {v2}, Lfjw;->iQ(I)Ljed;

    move-result-object v2

    iput-object v4, v2, Ljed;->edr:Lizm;

    goto/16 :goto_4

    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->mNetworkClient:Lfcx;

    invoke-interface {v4, v2}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v2

    if-eqz v2, :cond_13

    iget-object v4, v2, Ljeh;->aeC:Lizn;

    if-nez v4, :cond_14

    :cond_13
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->elapsedRealtime()J

    move-result-wide v4

    sget-object v6, Leyu;->cma:Leyv;

    iget v7, v6, Leyv;->cme:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v6, Leyv;->cme:I

    iget-object v8, v6, Leyv;->cmc:[I

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    iget-object v8, v6, Leyv;->cmc:[I

    aget v7, v8, v7

    int-to-long v8, v7

    const-wide/32 v10, 0xea60

    mul-long/2addr v8, v10

    iget v6, v6, Leyv;->cmd:I

    int-to-long v6, v6

    const-wide/16 v10, 0x3e8

    mul-long/2addr v6, v10

    add-long/2addr v6, v8

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Leyu;->f(IJ)V

    const-string v2, "CalendarIntentHandler"

    const-string v4, "Failed to get response for notification query from server"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_14
    sget-object v4, Leyu;->cma:Leyv;

    const/4 v5, -0x1

    iput v5, v4, Leyv;->cme:I

    iget-object v2, v2, Ljeh;->aeC:Lizn;

    iget-object v4, v2, Lizn;->dUI:[Lizo;

    array-length v4, v4

    if-eqz v4, :cond_d

    iget-object v4, v2, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lizo;->dUQ:Lizq;

    if-eqz v4, :cond_d

    iget-object v4, v2, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lizo;->dUQ:Lizq;

    iget-object v4, v4, Lizq;->dUX:[Lizj;

    array-length v4, v4

    if-eqz v4, :cond_d

    iget-object v2, v2, Lizn;->dUI:[Lizo;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lizo;->bdm()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-virtual {v2}, Lizo;->bdl()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Leyu;->mClock:Lemp;

    invoke-interface {v6}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x493e0

    add-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4, v5}, Leyu;->f(IJ)V

    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Leyu;->mCalendarDataProvider:Leym;

    invoke-interface {v4, v2}, Leym;->a(Lizo;)Z

    invoke-direct/range {p0 .. p0}, Leyu;->awG()V

    goto/16 :goto_5
.end method
