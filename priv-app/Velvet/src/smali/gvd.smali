.class final Lgvd;
.super Lesj;
.source "PG"


# instance fields
.field private synthetic cXr:Lguz;

.field private cXs:Ljava/util/Iterator;

.field private cXt:Ljava/util/List;


# direct methods
.method public constructor <init>(Lguz;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 294
    iput-object p1, p0, Lgvd;->cXr:Lguz;

    .line 295
    const-string v0, "AddCardsTransaction"

    invoke-direct {p0, v0}, Lesj;-><init>(Ljava/lang/String;)V

    .line 296
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 297
    invoke-static {p2}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lgvd;->cXs:Ljava/util/Iterator;

    .line 298
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgvd;->cXt:Ljava/util/List;

    .line 299
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lejm;)V
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lgvd;->cXr:Lguz;

    invoke-virtual {v0}, Lguz;->aKP()Z

    move-result v0

    if-nez v0, :cond_1

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    invoke-interface {p1}, Lejm;->atB()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v1

    .line 320
    iget-object v0, p0, Lgvd;->cXt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 321
    iget-object v3, p0, Lgvd;->cXr:Lguz;

    iget v3, v3, Lguz;->cXo:I

    invoke-virtual {v1, v0, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->u(Landroid/view/View;I)V

    goto :goto_1

    .line 323
    :cond_2
    iget-object v0, p0, Lgvd;->cXr:Lguz;

    iget-object v0, v0, Lguz;->cXj:Ljava/util/List;

    iget-object v1, p0, Lgvd;->cXt:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 326
    iget-object v1, p0, Lgvd;->cXr:Lguz;

    iget-object v0, p0, Lgvd;->cXt:Ljava/util/List;

    iget-object v2, p0, Lgvd;->cXt:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0, p1}, Lguz;->a(Landroid/view/View;Lejm;)V

    .line 329
    iget-object v0, p0, Lgvd;->cXr:Lguz;

    iget-object v0, v0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0}, Ldbj;->Wn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lgvd;->cXr:Lguz;

    iget-object v0, v0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0}, Ldbj;->Wo()Z

    move-result v0

    if-nez v0, :cond_3

    .line 331
    const/16 v0, 0xd0

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_0

    .line 334
    :cond_3
    iget-object v0, p0, Lgvd;->cXr:Lguz;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lguz;->cXp:Z

    goto :goto_0
.end method

.method public final avD()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 303
    iget-object v0, p0, Lgvd;->cXr:Lguz;

    invoke-virtual {v0}, Lguz;->aKP()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 310
    :goto_0
    return v0

    .line 307
    :cond_0
    :goto_1
    iget-object v0, p0, Lgvd;->cXs:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    iget-object v2, p0, Lgvd;->cXt:Ljava/util/List;

    iget-object v3, p0, Lgvd;->cXr:Lguz;

    iget-object v0, p0, Lgvd;->cXs:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhld;

    invoke-virtual {v3, v0}, Lguz;->a(Lhld;)Landroid/view/View;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 310
    :cond_1
    iget-object v0, p0, Lgvd;->cXs:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
