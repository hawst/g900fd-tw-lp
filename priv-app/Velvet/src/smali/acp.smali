.class public final Lacp;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "PG"


# instance fields
.field private Bq:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private Br:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field x:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field y:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 423
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 424
    iput v1, p0, Lacp;->Bq:I

    .line 425
    iput v1, p0, Lacp;->Br:I

    .line 426
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 429
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 430
    iput v0, p0, Lacp;->Bq:I

    .line 431
    iput v0, p0, Lacp;->Br:I

    .line 432
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 435
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 436
    iput v0, p0, Lacp;->Bq:I

    .line 437
    iput v0, p0, Lacp;->Br:I

    .line 438
    return-void
.end method


# virtual methods
.method public final a(IIIIII)V
    .locals 3

    .prologue
    .line 460
    iget v0, p0, Lacp;->Bq:I

    .line 461
    iget v1, p0, Lacp;->Br:I

    .line 462
    mul-int v2, v0, p1

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, p3

    add-int/2addr v0, v2

    iget v2, p0, Lacp;->leftMargin:I

    sub-int/2addr v0, v2

    iget v2, p0, Lacp;->rightMargin:I

    sub-int/2addr v0, v2

    iput v0, p0, Lacp;->width:I

    .line 467
    mul-int v0, v1, p2

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, p4

    add-int/2addr v0, v1

    iget v1, p0, Lacp;->topMargin:I

    sub-int/2addr v0, v1

    iget v1, p0, Lacp;->bottomMargin:I

    sub-int/2addr v0, v1

    iput v0, p0, Lacp;->height:I

    .line 470
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-boolean v0, v0, Lyu;->Md:Z

    if-eqz v0, :cond_0

    .line 471
    add-int v0, p1, p3

    mul-int/lit8 v0, v0, 0x0

    add-int/2addr v0, p5

    iget v1, p0, Lacp;->leftMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lacp;->x:I

    .line 472
    add-int v0, p2, p4

    mul-int/lit8 v0, v0, 0x0

    add-int/2addr v0, p6

    iget v1, p0, Lacp;->topMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lacp;->y:I

    .line 477
    :goto_0
    return-void

    .line 474
    :cond_0
    add-int v0, p1, p3

    mul-int/lit8 v0, v0, 0x0

    iget v1, p0, Lacp;->leftMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lacp;->x:I

    .line 475
    add-int v0, p2, p4

    mul-int/lit8 v0, v0, 0x0

    iget v1, p0, Lacp;->topMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lacp;->y:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 488
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lacp;->Bq:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lacp;->Br:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
