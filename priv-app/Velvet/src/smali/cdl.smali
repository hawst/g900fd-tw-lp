.class public final Lcdl;
.super Ljava/lang/Thread;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final aSB:Lcdf;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final aSE:Lbzh;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private aSM:Legq;

.field private final aSS:Lcem;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private aST:Ljww;

.field private volatile lD:Z


# direct methods
.method public constructor <init>(Lcem;Lbzh;Lcdf;Ljww;)V
    .locals 1
    .param p4    # Ljww;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 380
    const-string v0, "PairHttpReaderUp"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 381
    iput-object p1, p0, Lcdl;->aSS:Lcem;

    .line 382
    iput-object p2, p0, Lcdl;->aSE:Lbzh;

    .line 383
    iput-object p3, p0, Lcdl;->aSB:Lcdf;

    .line 384
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legq;

    iput-object v0, p0, Lcdl;->aSM:Legq;

    .line 386
    iput-object p4, p0, Lcdl;->aST:Ljww;

    .line 387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdl;->lD:Z

    .line 388
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdl;->lD:Z

    .line 393
    invoke-virtual {p0}, Lcdl;->interrupt()V

    .line 394
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 399
    :try_start_0
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    iget-object v1, p0, Lcdl;->aSM:Legq;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 400
    :goto_0
    iget-boolean v0, p0, Lcdl;->lD:Z

    if-eqz v0, :cond_4

    .line 402
    iget-object v0, p0, Lcdl;->aST:Ljww;

    if-eqz v0, :cond_3

    .line 403
    iget-object v0, p0, Lcdl;->aST:Ljww;

    .line 404
    const/4 v1, 0x0

    iput-object v1, p0, Lcdl;->aST:Ljww;

    .line 413
    :goto_1
    invoke-virtual {v0}, Ljww;->getStatus()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Ljww;->getStatus()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 415
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcdl;->lD:Z

    .line 417
    :cond_1
    iget-object v1, p0, Lcdl;->aSB:Lcdf;

    invoke-interface {v1, v0}, Lcdf;->b(Ljww;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 421
    :catch_0
    move-exception v0

    .line 422
    :try_start_1
    iget-boolean v1, p0, Lcdl;->lD:Z

    if-eqz v1, :cond_2

    .line 423
    const-string v1, "PairHttpConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Upload] exception - exit"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 424
    iget-object v1, p0, Lcdl;->aSB:Lcdf;

    new-instance v2, Leie;

    const v3, 0x1000d

    invoke-direct {v2, v0, v3}, Leie;-><init>(Ljava/lang/Throwable;I)V

    invoke-interface {v1, v2}, Lcdf;->c(Leie;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    :cond_2
    iget-object v0, p0, Lcdl;->aSS:Lcem;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 429
    iget-object v0, p0, Lcdl;->aSE:Lbzh;

    invoke-interface {v0}, Lbzh;->disconnect()V

    .line 430
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v5}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 431
    :goto_2
    return-void

    .line 406
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcdl;->aSS:Lcem;

    invoke-virtual {v0}, Lcem;->Dq()Ljww;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 428
    :cond_4
    iget-object v0, p0, Lcdl;->aSS:Lcem;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 429
    iget-object v0, p0, Lcdl;->aSE:Lbzh;

    invoke-interface {v0}, Lbzh;->disconnect()V

    .line 430
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v5}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_2

    .line 428
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcdl;->aSS:Lcem;

    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 429
    iget-object v1, p0, Lcdl;->aSE:Lbzh;

    invoke-interface {v1}, Lbzh;->disconnect()V

    .line 430
    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v5}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method
