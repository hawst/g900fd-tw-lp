.class public final Ljfz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eji:[Ljfz;


# instance fields
.field private aez:I

.field private ajk:Ljava/lang/String;

.field private ejj:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15713
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15714
    iput v1, p0, Ljfz;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljfz;->ajk:Ljava/lang/String;

    iput-boolean v1, p0, Ljfz;->ejj:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfz;->eCz:I

    .line 15715
    return-void
.end method

.method public static bjN()[Ljfz;
    .locals 2

    .prologue
    .line 15659
    sget-object v0, Ljfz;->eji:[Ljfz;

    if-nez v0, :cond_1

    .line 15660
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 15662
    :try_start_0
    sget-object v0, Ljfz;->eji:[Ljfz;

    if-nez v0, :cond_0

    .line 15663
    const/4 v0, 0x0

    new-array v0, v0, [Ljfz;

    sput-object v0, Ljfz;->eji:[Ljfz;

    .line 15665
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15667
    :cond_1
    sget-object v0, Ljfz;->eji:[Ljfz;

    return-object v0

    .line 15665
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 15653
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfz;->ajk:Ljava/lang/String;

    iget v0, p0, Ljfz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfz;->ejj:Z

    iget v0, p0, Ljfz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 15729
    iget v0, p0, Ljfz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15730
    const/4 v0, 0x1

    iget-object v1, p0, Ljfz;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15732
    :cond_0
    iget v0, p0, Ljfz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15733
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfz;->ejj:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15735
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15736
    return-void
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15675
    iget-object v0, p0, Ljfz;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 15740
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 15741
    iget v1, p0, Ljfz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15742
    const/4 v1, 0x1

    iget-object v2, p0, Ljfz;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15745
    :cond_0
    iget v1, p0, Ljfz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15746
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfz;->ejj:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 15749
    :cond_1
    return v0
.end method

.method public final uu(Ljava/lang/String;)Ljfz;
    .locals 1

    .prologue
    .line 15678
    if-nez p1, :cond_0

    .line 15679
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15681
    :cond_0
    iput-object p1, p0, Ljfz;->ajk:Ljava/lang/String;

    .line 15682
    iget v0, p0, Ljfz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfz;->aez:I

    .line 15683
    return-object p0
.end method
