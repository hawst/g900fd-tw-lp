.class final Lmb;
.super Lkt;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final DEBUG:Z


# instance fields
.field private cq:Z

.field private final lg:Lmg;

.field private final lh:Ljava/util/ArrayList;

.field private li:Z

.field private lj:Lmc;

.field private lk:Z

.field private final mComponentName:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "MediaRouteProviderProxy"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lmb;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lkw;

    invoke-direct {v0, p2}, Lkw;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, Lkt;-><init>(Landroid/content/Context;Lkw;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmb;->lh:Ljava/util/ArrayList;

    .line 61
    iput-object p2, p0, Lmb;->mComponentName:Landroid/content/ComponentName;

    .line 62
    new-instance v0, Lmg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmg;-><init>(Lmb;B)V

    iput-object v0, p0, Lmb;->lg:Lmg;

    .line 63
    return-void
.end method

.method static synthetic a(Lmb;)Lmg;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lmb;->lg:Lmg;

    return-object v0
.end method

.method static synthetic a(Lmb;Lmc;)V
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lmb;->lj:Lmc;

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmb;->lk:Z

    iget-object v0, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmf;

    iget-object v3, p0, Lmb;->lj:Lmc;

    invoke-virtual {v0, v3}, Lmf;->b(Lmc;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lkt;->kj:Lks;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmb;->lj:Lmc;

    invoke-virtual {v1, v0}, Lmc;->a(Lks;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lmb;Lmc;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lmb;->lj:Lmc;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Service connection error - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-direct {p0}, Lmb;->bX()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lmb;Lmc;Lky;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lmb;->lj:Lmc;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Descriptor changed, descriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0, p2}, Lmb;->a(Lky;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lmb;Lmf;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lmf;->cb()V

    invoke-direct {p0}, Lmb;->bU()V

    return-void
.end method

.method static synthetic b(Lmb;Lmc;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lmb;->lj:Lmc;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Service connection died"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-direct {p0}, Lmb;->disconnect()V

    :cond_1
    return-void
.end method

.method private bU()V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Lmb;->bV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-direct {p0}, Lmb;->bW()V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-direct {p0}, Lmb;->bX()V

    goto :goto_0
.end method

.method private bV()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 138
    iget-boolean v1, p0, Lmb;->cq:Z

    if-eqz v1, :cond_2

    .line 140
    iget-object v1, p0, Lkt;->kj:Lks;

    if-eqz v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    .line 146
    :cond_1
    iget-object v1, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bW()V
    .locals 3

    .prologue
    .line 154
    iget-boolean v0, p0, Lmb;->li:Z

    if-nez v0, :cond_1

    .line 155
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Binding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lmb;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 162
    :try_start_0
    iget-object v1, p0, Lkt;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lmb;->li:Z

    .line 163
    iget-boolean v0, p0, Lmb;->li:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Bind failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_1
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v0

    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Bind failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private bX()V
    .locals 2

    .prologue
    .line 175
    iget-boolean v0, p0, Lmb;->li:Z

    if-eqz v0, :cond_1

    .line 176
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Unbinding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmb;->li:Z

    .line 181
    invoke-direct {p0}, Lmb;->disconnect()V

    .line 182
    iget-object v0, p0, Lkt;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 184
    :cond_1
    return-void
.end method

.method static synthetic bY()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lmb;->DEBUG:Z

    return v0
.end method

.method private disconnect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 260
    iget-object v1, p0, Lmb;->lj:Lmc;

    if-eqz v1, :cond_1

    .line 261
    invoke-virtual {p0, v3}, Lmb;->a(Lky;)V

    .line 262
    iput-boolean v0, p0, Lmb;->lk:Z

    .line 263
    iget-object v1, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmf;

    invoke-virtual {v0}, Lmf;->cb()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 264
    :cond_0
    iget-object v0, p0, Lmb;->lj:Lmc;

    invoke-virtual {v0}, Lmc;->ca()V

    .line 265
    iput-object v3, p0, Lmb;->lj:Lmc;

    .line 267
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lmb;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmb;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lks;)V
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lmb;->lk:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lmb;->lj:Lmc;

    invoke-virtual {v0, p1}, Lmc;->a(Lks;)V

    .line 92
    :cond_0
    invoke-direct {p0}, Lmb;->bU()V

    .line 93
    return-void
.end method

.method public final bT()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lmb;->lj:Lmc;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmb;->bV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lmb;->bX()V

    .line 125
    invoke-direct {p0}, Lmb;->bW()V

    .line 127
    :cond_0
    return-void
.end method

.method public final g(Ljava/lang/String;)Lkx;
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lkt;->kl:Lky;

    .line 68
    if-eqz v0, :cond_2

    .line 69
    invoke-virtual {v0}, Lky;->getRoutes()Ljava/util/List;

    move-result-object v2

    .line 70
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 71
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 72
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkq;

    .line 73
    invoke-virtual {v0}, Lkq;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    new-instance v0, Lmf;

    invoke-direct {v0, p0, p1}, Lmf;-><init>(Lmb;Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lmb;->lh:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-boolean v1, p0, Lmb;->lk:Z

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lmb;->lj:Lmc;

    invoke-virtual {v0, v1}, Lmf;->b(Lmc;)V

    .line 79
    :cond_0
    invoke-direct {p0}, Lmb;->bU()V

    .line 84
    :goto_1
    return-object v0

    .line 71
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 84
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 188
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Connected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_0
    iget-boolean v0, p0, Lmb;->li:Z

    if-eqz v0, :cond_1

    .line 193
    invoke-direct {p0}, Lmb;->disconnect()V

    .line 195
    if-eqz p2, :cond_2

    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 196
    :goto_0
    invoke-static {v0}, Lla;->a(Landroid/os/Messenger;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 197
    new-instance v1, Lmc;

    invoke-direct {v1, p0, v0}, Lmc;-><init>(Lmb;Landroid/os/Messenger;)V

    .line 198
    invoke-virtual {v1}, Lmc;->bZ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 199
    iput-object v1, p0, Lmb;->lj:Lmc;

    .line 209
    :cond_1
    :goto_1
    return-void

    .line 195
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_3
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Registration failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 206
    :cond_4
    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Service returned invalid messenger binder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 213
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Service disconnected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_0
    invoke-direct {p0}, Lmb;->disconnect()V

    .line 217
    return-void
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lmb;->cq:Z

    if-nez v0, :cond_1

    .line 102
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Starting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmb;->cq:Z

    .line 107
    invoke-direct {p0}, Lmb;->bU()V

    .line 109
    :cond_1
    return-void
.end method

.method public final stop()V
    .locals 2

    .prologue
    .line 112
    iget-boolean v0, p0, Lmb;->cq:Z

    if-eqz v0, :cond_1

    .line 113
    sget-boolean v0, Lmb;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Stopping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmb;->cq:Z

    .line 118
    invoke-direct {p0}, Lmb;->bU()V

    .line 120
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmb;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
