.class public final Lfye;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field cDj:Ljava/util/LinkedList;

.field private cDk:I

.field cDl:Lfyg;

.field private final cDm:I

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfye;->cDj:Ljava/util/LinkedList;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lfye;->cDk:I

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lfye;->cDl:Lfyg;

    .line 28
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfye;->cDj:Ljava/util/LinkedList;

    .line 29
    const/4 v0, 0x4

    iput v0, p0, Lfye;->cDm:I

    .line 30
    iput-object p1, p0, Lfye;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;I)Lfye;
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lfye;->cDk:I

    add-int/2addr v0, p3

    iget v1, p0, Lfye;->cDm:I

    if-le v0, v1, :cond_0

    .line 49
    invoke-virtual {p0}, Lfye;->aDQ()V

    .line 51
    :cond_0
    iget-object v0, p0, Lfye;->cDl:Lfyg;

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Lfyg;

    iget-object v1, p0, Lfye;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lfyg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfye;->cDl:Lfyg;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lfye;->cDk:I

    .line 55
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 56
    iget-object v0, p0, Lfye;->cDl:Lfyg;

    invoke-virtual {v0, p1, p2, p3}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    .line 57
    iget v0, p0, Lfye;->cDk:I

    add-int/2addr v0, p3

    iput v0, p0, Lfye;->cDk:I

    .line 59
    :cond_2
    return-object p0
.end method


# virtual methods
.method public final a(ILjava/lang/String;I)Lfye;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfye;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lfye;->d(Ljava/lang/String;Ljava/lang/String;I)Lfye;

    move-result-object v0

    return-object v0
.end method

.method aDQ()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lfye;->cDj:Ljava/util/LinkedList;

    iget-object v1, p0, Lfye;->cDl:Lfyg;

    invoke-virtual {v1}, Lfyg;->aDR()Laow;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lfye;->cDl:Lfyg;

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lfye;->cDk:I

    .line 66
    return-void
.end method

.method public final l(ILjava/lang/String;)Lfye;
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lfye;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lfye;->d(Ljava/lang/String;Ljava/lang/String;I)Lfye;

    move-result-object v0

    return-object v0
.end method
