.class public final Ljxe;
.super Ljsl;
.source "PG"


# static fields
.field public static final eKo:Ljsm;


# instance fields
.field private aez:I

.field public eKp:Ljps;

.field private eKq:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const v2, 0xcaf0c22

    const/16 v1, 0xb

    .line 701
    const-class v0, Ljxe;

    invoke-static {v1, v0, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxe;->eKo:Ljsm;

    .line 711
    const-class v0, Ljxe;

    invoke-static {v1, v0, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 717
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 749
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 750
    const/4 v0, 0x0

    iput v0, p0, Ljxe;->aez:I

    iput-object v1, p0, Ljxe;->eKp:Ljps;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljxe;->eKq:[B

    iput-object v1, p0, Ljxe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxe;->eCz:I

    .line 751
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 694
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljxe;->eKp:Ljps;

    if-nez v0, :cond_1

    new-instance v0, Ljps;

    invoke-direct {v0}, Ljps;-><init>()V

    iput-object v0, p0, Ljxe;->eKp:Ljps;

    :cond_1
    iget-object v0, p0, Ljxe;->eKp:Ljps;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljxe;->eKq:[B

    iget v0, p0, Ljxe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxe;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 765
    iget-object v0, p0, Ljxe;->eKp:Ljps;

    if-eqz v0, :cond_0

    .line 766
    const/4 v0, 0x1

    iget-object v1, p0, Ljxe;->eKp:Ljps;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 768
    :cond_0
    iget v0, p0, Ljxe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 769
    const/4 v0, 0x2

    iget-object v1, p0, Ljxe;->eKq:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 771
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 772
    return-void
.end method

.method public final bvG()Z
    .locals 1

    .prologue
    .line 741
    iget v0, p0, Ljxe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 776
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 777
    iget-object v1, p0, Ljxe;->eKp:Ljps;

    if-eqz v1, :cond_0

    .line 778
    const/4 v1, 0x1

    iget-object v2, p0, Ljxe;->eKp:Ljps;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_0
    iget v1, p0, Ljxe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 782
    const/4 v1, 0x2

    iget-object v2, p0, Ljxe;->eKq:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_1
    return v0
.end method
