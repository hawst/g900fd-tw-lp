.class public final Lyu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ltw;


# static fields
.field public static Mh:Ljava/lang/ref/WeakReference;

.field static Mi:Landroid/content/Context;

.field public static Mj:Lyu;


# instance fields
.field public Kr:Lzi;

.field private final Mb:Lsr;

.field public Mc:Lafi;

.field public Md:Z

.field Me:F

.field Mf:I

.field public Mg:Z

.field public Mk:Lur;

.field private final Ml:Landroid/database/ContentObserver;

.field public xn:Lwi;

.field public final xo:Lrq;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/16 v0, 0x12c

    iput v0, p0, Lyu;->Mf:I

    .line 146
    new-instance v0, Lyv;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lyv;-><init>(Lyu;Landroid/os/Handler;)V

    iput-object v0, p0, Lyu;->Ml:Landroid/database/ContentObserver;

    .line 83
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "LauncherAppState inited before app context set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    const-string v1, "L"

    invoke-static {v0, v1}, Lcom/android/launcher3/MemoryTracker;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 94
    :cond_1
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lyu;->Md:Z

    .line 95
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lyu;->Me:F

    .line 97
    invoke-virtual {p0}, Lyu;->ik()V

    .line 98
    new-instance v0, Lwi;

    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    invoke-direct {v0, v1}, Lwi;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lyu;->xn:Lwi;

    .line 100
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    const v1, 0x7f0a007c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lrq;->loadByName(Ljava/lang/String;)Lrq;

    move-result-object v0

    iput-object v0, p0, Lyu;->xo:Lrq;

    .line 101
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    const v1, 0x7f0a007d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsr;->loadByName(Ljava/lang/String;)Lsr;

    move-result-object v0

    iput-object v0, p0, Lyu;->Mb:Lsr;

    .line 102
    new-instance v0, Lzi;

    iget-object v1, p0, Lyu;->xn:Lwi;

    iget-object v2, p0, Lyu;->xo:Lrq;

    invoke-direct {v0, p0, v1, v2}, Lzi;-><init>(Lyu;Lwi;Lrq;)V

    iput-object v0, p0, Lyu;->Kr:Lzi;

    .line 103
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    invoke-static {v0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lyu;->Kr:Lzi;

    invoke-virtual {v0, v1}, Lahn;->a(Laho;)V

    .line 107
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 108
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 109
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 110
    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    iget-object v2, p0, Lyu;->Kr:Lzi;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 111
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 112
    const-string v1, "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    iget-object v2, p0, Lyu;->Kr:Lzi;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 114
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 115
    const-string v1, "android.search.action.SEARCHABLES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 116
    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    iget-object v2, p0, Lyu;->Kr:Lzi;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 120
    sget-object v1, Labn;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lyu;->Ml:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 122
    return-void
.end method

.method public static ij()Lyu;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lyu;->Mj:Lyu;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lyu;

    invoke-direct {v0}, Lyu;-><init>()V

    sput-object v0, Lyu;->Mj:Lyu;

    .line 64
    :cond_0
    sget-object v0, Lyu;->Mj:Lyu;

    return-object v0
.end method

.method public static il()Lcom/android/launcher3/LauncherProvider;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lyu;->Mh:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/LauncherProvider;

    return-object v0
.end method

.method public static im()Z
    .locals 1

    .prologue
    .line 252
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v0, v0, Lyu;->Mb:Lsr;

    invoke-virtual {v0}, Lsr;->isDogfoodBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "launcher_noallapps"

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->r(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDogfoodBuild()Z
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v0, v0, Lyu;->Mb:Lsr;

    invoke-virtual {v0}, Lsr;->isDogfoodBuild()Z

    move-result v0

    return v0
.end method

.method public static n(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 76
    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setApplicationContext called twice! old="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lyu;->Mi:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " new="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lyu;->Mi:Landroid/content/Context;

    .line 80
    return-void
.end method


# virtual methods
.method public final a(Ltu;)V
    .locals 1

    .prologue
    .line 247
    iget v0, p1, Ltu;->DI:I

    invoke-static {v0}, Ladp;->bK(I)V

    .line 248
    return-void
.end method

.method public final ik()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lyu;->Mc:Lafi;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lyu;->Mc:Lafi;

    invoke-virtual {v0}, Lafi;->close()V

    .line 128
    :cond_0
    new-instance v0, Lafi;

    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    invoke-direct {v0, v1}, Lafi;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lyu;->Mc:Lafi;

    .line 129
    return-void
.end method

.method public final p(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lyu;->Kr:Lzi;

    invoke-virtual {v0, p1}, Lzi;->p(Ljava/util/ArrayList;)V

    .line 262
    return-void
.end method

.method public final s(Lcom/android/launcher3/Launcher;)Lzi;
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lyu;->Kr:Lzi;

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setLauncher() called before init()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    iget-object v0, p0, Lyu;->Kr:Lzi;

    invoke-virtual {v0, p1}, Lzi;->a(Laaf;)V

    .line 161
    iget-object v0, p0, Lyu;->Kr:Lzi;

    return-object v0
.end method
