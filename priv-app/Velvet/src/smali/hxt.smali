.class public final Lhxt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cPu:Z

.field public final cQb:Ljava/lang/String;

.field public final cQc:Ljava/util/List;

.field public final cQd:Ljava/lang/String;

.field public final dtf:Landroid/net/Uri;

.field final dum:Z

.field public final dun:Z


# direct methods
.method public constructor <init>(Landroid/content/Intent;Lhym;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhxt;->cQd:Ljava/lang/String;

    .line 57
    const-string v0, "android.speech.extra.LANGUAGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 58
    :goto_0
    const-string v2, "android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 60
    :goto_1
    if-eqz v0, :cond_2

    .line 61
    iput-object v0, p0, Lhxt;->cQb:Ljava/lang/String;

    .line 68
    :goto_2
    if-nez v1, :cond_3

    .line 69
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lhxt;->cQc:Ljava/util/List;

    .line 77
    :goto_3
    const-string v0, "android.speech.extra.PARTIAL_RESULTS"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhxt;->dum:Z

    .line 78
    const-string v0, "android.speech.extra.DICTATION_MODE"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhxt;->dun:Z

    .line 79
    const-string v0, "android.speech.extra.PROFANITY_FILTER"

    invoke-virtual {p2}, Lhym;->aHP()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhxt;->cPu:Z

    .line 80
    invoke-static {p1}, Lhxt;->al(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lhxt;->dtf:Landroid/net/Uri;

    .line 81
    return-void

    .line 57
    :cond_0
    invoke-virtual {p2}, Lhym;->aER()Ljze;

    move-result-object v2

    invoke-static {v2, v0}, Lgnq;->h(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p2}, Lhym;->aER()Ljze;

    move-result-object v1

    invoke-static {v1, v2}, Lgnq;->c(Ljze;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 63
    :cond_2
    invoke-virtual {p2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lhxt;->cQb:Ljava/lang/String;

    .line 64
    const-string v2, "GoogleRecognitionParams"

    const-string v3, "Using default language (%s) as no primary language was specified."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lhxt;->cQb:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 70
    :cond_3
    if-nez v0, :cond_4

    .line 71
    const-string v0, "GoogleRecognitionParams"

    const-string v1, "Ignoring additional languages as no primary language was specified."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 72
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lhxt;->cQc:Ljava/util/List;

    goto :goto_3

    .line 74
    :cond_4
    iput-object v1, p0, Lhxt;->cQc:Ljava/util/List;

    goto :goto_3
.end method

.method private static al(Landroid/content/Intent;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    sget-object v0, Lcgg;->aVh:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    :try_start_0
    const-string v0, "com.google.android.voicesearch.extra.AUDIO_SOURCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    return-object v0

    .line 125
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 128
    goto :goto_0
.end method
