.class public final Lhyc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cph:Landroid/util/LruCache;

.field private duz:I

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lhym;Lchk;)V
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    .line 127
    iput-object p1, p0, Lhyc;->mSettings:Lhym;

    .line 128
    invoke-virtual {p2}, Lchk;->JL()I

    move-result v0

    iput v0, p0, Lhyc;->duz:I

    .line 129
    return-void
.end method

.method public static a(Ljava/lang/String;IIIII)Ljava/lang/String;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;III)V
    .locals 7

    .prologue
    .line 140
    iget-object v6, p0, Lhyc;->cph:Landroid/util/LruCache;

    new-instance v0, Lhyd;

    iget v5, p0, Lhyc;->duz:I

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lhyd;-><init>(Lhyc;IIII)V

    invoke-virtual {v6, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-void
.end method

.method public final aTw()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 208
    iget-object v0, p0, Lhyc;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUd()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 209
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 212
    array-length v10, v9

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_1

    aget-object v0, v9, v7

    .line 213
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 215
    array-length v0, v1

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    .line 216
    iget-object v11, p0, Lhyc;->cph:Landroid/util/LruCache;

    aget-object v12, v1, v8

    new-instance v0, Lhyd;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x3

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x5

    aget-object v1, v1, v6

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lhyd;-><init>(Lhyc;IIIII)V

    invoke-virtual {v11, v12, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 224
    :cond_1
    return-void
.end method

.method public final aTx()V
    .locals 9

    .prologue
    .line 242
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v7

    .line 246
    const/4 v0, 0x1

    .line 247
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v0

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 248
    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lhyd;

    .line 249
    if-nez v2, :cond_0

    .line 250
    const-string v1, ","

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_0
    iget v1, v5, Lhyd;->duA:I

    iget v2, v5, Lhyd;->AUDIO:I

    iget v3, v5, Lhyd;->duB:I

    iget v4, v5, Lhyd;->duC:I

    iget v5, v5, Lhyd;->duD:I

    invoke-static/range {v0 .. v5}, Lhyc;->a(Ljava/lang/String;IIIII)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const/4 v0, 0x0

    move v2, v0

    .line 255
    goto :goto_0

    .line 256
    :cond_1
    iget-object v0, p0, Lhyc;->mSettings:Lhym;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->oG(Ljava/lang/String;)V

    .line 257
    return-void
.end method

.method public final aTy()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 264
    invoke-virtual {p0}, Lhyc;->aTx()V

    .line 265
    return-void
.end method

.method public final oA(Ljava/lang/String;)Lhyd;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    return-object v0
.end method

.method public final ow(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/16 v6, 0x44c

    .line 148
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lhyd;

    .line 149
    if-nez v1, :cond_0

    .line 150
    iget-object v7, p0, Lhyc;->cph:Landroid/util/LruCache;

    new-instance v0, Lhyd;

    const/4 v2, 0x0

    const/16 v3, 0x64

    const/4 v4, -0x1

    iget v5, p0, Lhyc;->duz:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lhyd;-><init>(Lhyc;IIIII)V

    invoke-virtual {v7, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v7, p0, Lhyc;->cph:Landroid/util/LruCache;

    new-instance v0, Lhyd;

    iget v2, v1, Lhyd;->duA:I

    iget v3, v1, Lhyd;->AUDIO:I

    iget v4, v1, Lhyd;->duB:I

    iget v5, v1, Lhyd;->duC:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lhyd;-><init>(Lhyc;IIIII)V

    invoke-virtual {v7, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final ox(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 165
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lhyd;

    .line 167
    if-nez v1, :cond_0

    .line 168
    new-instance v0, Lhyd;

    const/4 v2, 0x0

    const/16 v3, 0x64

    const/4 v4, -0x1

    iget v5, p0, Lhyc;->duz:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lhyd;-><init>(Lhyc;IIII)V

    .line 174
    :goto_0
    iget-object v1, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    return-void

    .line 171
    :cond_0
    new-instance v0, Lhyd;

    iget v2, v1, Lhyd;->duA:I

    iget v3, v1, Lhyd;->AUDIO:I

    iget v4, v1, Lhyd;->duB:I

    iget v5, p0, Lhyc;->duz:I

    iget v6, v1, Lhyd;->duD:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lhyd;-><init>(Lhyc;IIIII)V

    goto :goto_0
.end method

.method public final oy(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    .line 181
    if-eqz v0, :cond_1

    .line 182
    iget v1, v0, Lhyd;->duA:I

    if-nez v1, :cond_0

    iget v1, v0, Lhyd;->AUDIO:I

    if-eq v1, v3, :cond_1

    .line 183
    :cond_0
    const-string v0, "DeviceCache"

    const-string v1, "bluetoothPromptShown got a configured device."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_1
    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    add-int/lit8 v5, v0, 0x1

    .line 189
    iget-object v6, p0, Lhyc;->cph:Landroid/util/LruCache;

    new-instance v0, Lhyd;

    const/4 v4, -0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lhyd;-><init>(Lhyc;IIII)V

    invoke-virtual {v6, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 188
    :cond_2
    iget v0, v0, Lhyd;->duC:I

    goto :goto_1
.end method

.method public final oz(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lhyc;->cph:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyd;

    .line 196
    if-eqz v0, :cond_0

    iget v0, v0, Lhyd;->duC:I

    iget v1, p0, Lhyc;->duz:I

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
