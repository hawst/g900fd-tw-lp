.class public Lbwp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aMA:Z

.field private final aMB:Z

.field private final aMx:Ljava/lang/String;

.field private final aMy:Ljava/lang/String;

.field private final aMz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.e100.MESSAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwp;->aMx:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.e100.LOCATION_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwp;->aMy:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.e100.LOCATION_CID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbwp;->aMz:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.e100.FORCE_READ"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbwp;->aMB:Z

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbwp;->aMA:Z

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lbwp;->aMx:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lbwp;->aMy:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lbwp;->aMz:Ljava/lang/String;

    .line 71
    iput-boolean p2, p0, Lbwp;->aMA:Z

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbwp;->aMB:Z

    .line 73
    return-void
.end method

.method public static a(Lbwp;)Z
    .locals 1

    .prologue
    .line 41
    if-eqz p0, :cond_0

    iget-object v0, p0, Lbwp;->aMx:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final AZ()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lbwp;->aMA:Z

    return v0
.end method

.method public final Ba()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lbwp;->aMx:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/shared/search/Query;->az(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final Bb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lbwp;->aMx:Ljava/lang/String;

    return-object v0
.end method

.method public final Bc()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lbwp;->aMB:Z

    return v0
.end method

.method public final a(Lcky;Lemp;)V
    .locals 6

    .prologue
    .line 87
    new-instance v0, Ljkb;

    invoke-direct {v0}, Ljkb;-><init>()V

    .line 88
    iget-object v1, p0, Lbwp;->aMy:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 89
    new-instance v1, Ljpd;

    invoke-direct {v1}, Ljpd;-><init>()V

    .line 90
    iget-object v2, p0, Lbwp;->aMy:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    .line 91
    iget-object v2, p0, Lbwp;->aMz:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    iget-object v2, p0, Lbwp;->aMz:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljpd;->xX(Ljava/lang/String;)Ljpd;

    .line 94
    :cond_0
    iput-object v1, v0, Ljkb;->epL:Ljpd;

    .line 97
    :cond_1
    invoke-interface {p2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 98
    invoke-virtual {p1, v0, v2, v3}, Lcky;->a(Ljkb;J)V

    .line 99
    return-void
.end method
