.class final Lcup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcts;


# instance fields
.field private final bjU:Lcuq;

.field private final bjV:Lcus;

.field private final bjW:Lcus;

.field private final bjX:Lcus;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcuq;

    invoke-direct {v0, p1, p2}, Lcuq;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcup;->bjU:Lcuq;

    .line 33
    new-instance v0, Lcur;

    invoke-direct {v0, p1, p2}, Lcur;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcup;->bjV:Lcus;

    .line 34
    new-instance v0, Lcut;

    invoke-direct {v0, p1, p2}, Lcut;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcup;->bjW:Lcus;

    .line 35
    new-instance v0, Lcuu;

    invoke-direct {v0, p1, p2}, Lcuu;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcup;->bjX:Lcus;

    .line 36
    return-void
.end method


# virtual methods
.method public final SS()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcup;->bjU:Lcuq;

    iget-object v0, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 64
    iget-object v0, p0, Lcup;->bjV:Lcus;

    invoke-virtual {v0}, Lcus;->close()V

    .line 65
    iget-object v0, p0, Lcup;->bjW:Lcus;

    invoke-virtual {v0}, Lcus;->close()V

    .line 66
    iget-object v0, p0, Lcup;->bjX:Lcus;

    invoke-virtual {v0}, Lcus;->close()V

    .line 67
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcup;->bjU:Lcuq;

    invoke-virtual {v0}, Lcuq;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 22
    iget-object v0, p0, Lcup;->bjU:Lcuq;

    invoke-virtual {v0}, Lcuq;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "contact_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v1, Lctp;

    iget-object v4, p0, Lcup;->bjV:Lcus;

    invoke-virtual {v4, v2, v3}, Lcus;->ag(J)Ljava/util/Map;

    move-result-object v4

    iget-object v5, p0, Lcup;->bjW:Lcus;

    invoke-virtual {v5, v2, v3}, Lcus;->ag(J)Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Lcup;->bjX:Lcus;

    invoke-virtual {v6, v2, v3}, Lcus;->ag(J)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v0, v4, v5, v2}, Lctp;-><init>(Landroid/content/ContentValues;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v1
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
