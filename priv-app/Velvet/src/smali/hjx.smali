.class public abstract Lhjx;
.super Lhjo;
.source "PG"


# direct methods
.method public constructor <init>(Ledr;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lhjo;-><init>(Ledr;)V

    .line 28
    return-void
.end method

.method private F(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 48
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {p0}, Lhjx;->uh()V

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    invoke-virtual {p0}, Lhjx;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhjx;->ac:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    goto :goto_0

    .line 52
    :cond_2
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agt()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53
    invoke-virtual {p0}, Lhjx;->uy()V

    goto :goto_0

    .line 54
    :cond_3
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lhjx;->uD()V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lelb;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lelc;

    invoke-virtual {p0, p1}, Lhjx;->a(Lelc;)V

    return-void
.end method

.method public final a(Lelc;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lhjo;->a(Lelb;)V

    .line 33
    iget-object v0, p0, Lhjx;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-direct {p0, v0}, Lhjx;->F(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 34
    return-void
.end method

.method public final aQk()V
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p0}, Lhjx;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjx;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjx;->mCardController:Ledr;

    iget-object v1, p0, Lhjx;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->B(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lhjx;->mCardController:Ledr;

    iget-object v1, p0, Lhjx;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    iget-object v2, p0, Lhjx;->diL:Lesk;

    invoke-interface {v0, v1, v2}, Ledr;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lesk;)J

    move-result-wide v2

    .line 97
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lelc;

    invoke-interface {v0, v2, v3}, Lelc;->aL(J)V

    .line 101
    :cond_0
    return-void
.end method

.method public final anO()Z
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Lhjo;->anO()Z

    move-result v1

    .line 62
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lhjx;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lelc;

    invoke-interface {v0}, Lelc;->auk()V

    .line 65
    :cond_0
    return v1
.end method

.method public final b(Ldbd;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 38
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-interface {p2}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p0}, Lhjx;->tP()V

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {p1}, Ldbd;->VG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0, p2}, Lhjx;->F(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_0
.end method

.method public uD()V
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lhjx;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lelc;

    invoke-interface {v0}, Lelc;->auk()V

    .line 81
    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lelc;

    invoke-interface {v0}, Lelc;->uD()V

    .line 83
    :cond_0
    return-void
.end method

.method public uh()V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lhjx;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lelc;

    invoke-interface {v0}, Lelc;->auk()V

    .line 74
    invoke-virtual {p0}, Lhjx;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lelc;

    invoke-interface {v0}, Lelc;->uh()V

    .line 76
    :cond_0
    return-void
.end method
