.class public final Ljab;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afX:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field private dAD:I

.field private dUn:Ljava/lang/String;

.field private dVR:Ljava/lang/String;

.field private dVS:[Ljad;

.field private dVT:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Ljab;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljab;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljab;->afX:Ljava/lang/String;

    iput v1, p0, Ljab;->dAD:I

    const-string v0, ""

    iput-object v0, p0, Ljab;->dVR:Ljava/lang/String;

    invoke-static {}, Ljad;->bdH()[Ljad;

    move-result-object v0

    iput-object v0, p0, Ljab;->dVS:[Ljad;

    const-string v0, ""

    iput-object v0, p0, Ljab;->dUn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljab;->dVT:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljab;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljab;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljab;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljab;->afh:Ljava/lang/String;

    iget v0, p0, Ljab;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljab;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljab;->afX:Ljava/lang/String;

    iget v0, p0, Ljab;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljab;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljab;->dAD:I

    iget v0, p0, Ljab;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljab;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljab;->dVR:Ljava/lang/String;

    iget v0, p0, Ljab;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljab;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljab;->dVS:[Ljad;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljad;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljab;->dVS:[Ljad;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljad;

    invoke-direct {v3}, Ljad;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljab;->dVS:[Ljad;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljad;

    invoke-direct {v3}, Ljad;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljab;->dVS:[Ljad;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljab;->dUn:Ljava/lang/String;

    iget v0, p0, Ljab;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljab;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljab;->dVT:Ljava/lang/String;

    iget v0, p0, Ljab;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljab;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget v0, p0, Ljab;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljab;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljab;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljab;->afX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljab;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljab;->dAD:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_2
    iget v0, p0, Ljab;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljab;->dVR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljab;->dVS:[Ljad;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljab;->dVS:[Ljad;

    array-length v0, v0

    if-lez v0, :cond_5

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljab;->dVS:[Ljad;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Ljab;->dVS:[Ljad;

    aget-object v1, v1, v0

    if-eqz v1, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget v0, p0, Ljab;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v1, p0, Ljab;->dUn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_6
    iget v0, p0, Ljab;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-object v1, p0, Ljab;->dVT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 5

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljab;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljab;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljab;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljab;->afX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljab;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljab;->dAD:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljab;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljab;->dVR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljab;->dVS:[Ljad;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljab;->dVS:[Ljad;

    array-length v1, v1

    if-lez v1, :cond_6

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljab;->dVS:[Ljad;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ljab;->dVS:[Ljad;

    aget-object v2, v2, v0

    if-eqz v2, :cond_4

    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    :cond_6
    iget v1, p0, Ljab;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    const/4 v1, 0x6

    iget-object v2, p0, Ljab;->dUn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Ljab;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_8

    const/4 v1, 0x7

    iget-object v2, p0, Ljab;->dVT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    return v0
.end method
