.class public final Lfyw;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljhn;)Ljava/lang/String;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 64
    invoke-virtual {p2}, Ljhn;->bmc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-static {p1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->aCF()I

    move-result v0

    .line 68
    invoke-virtual {p2}, Ljhn;->bmb()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {p0, v1, v4, v4, v0}, Lgay;->a(Landroid/content/Context;IFFI)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljhn;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Ljhn;->pX()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljhn;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Ljhn;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-virtual {p0}, Ljhn;->bma()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljhn;->blZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p0}, Ljhn;->blZ()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Liyg;)[Laoz;
    .locals 9

    .prologue
    const v1, 0x7f02016e

    .line 27
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 28
    iget-object v4, p2, Liyg;->dPO:[Ljhn;

    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v0, v4, v2

    .line 29
    new-instance v6, Laoz;

    invoke-direct {v6}, Laoz;-><init>()V

    .line 31
    invoke-static {v0}, Lfyw;->a(Ljhn;)Ljava/lang/String;

    move-result-object v7

    .line 32
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 33
    invoke-virtual {v6, v7}, Laoz;->cC(Ljava/lang/String;)Laoz;

    .line 36
    :cond_0
    invoke-virtual {v0}, Ljhn;->pc()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 37
    invoke-virtual {v0}, Ljhn;->pc()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Laoz;->cD(Ljava/lang/String;)Laoz;

    .line 40
    :cond_1
    invoke-static {p0, p1, v0}, Lfyw;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljhn;)Ljava/lang/String;

    move-result-object v7

    .line 41
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 42
    invoke-virtual {v6, v7}, Laoz;->cE(Ljava/lang/String;)Laoz;

    .line 45
    :cond_2
    invoke-virtual {v0}, Ljhn;->oP()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Ljhn;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Laoz;->cN(I)Laoz;

    .line 46
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 45
    :sswitch_0
    const v0, 0x7f0200f3

    goto :goto_1

    :sswitch_1
    const v0, 0x7f020087

    goto :goto_1

    :sswitch_2
    const v0, 0x7f02010c

    goto :goto_1

    :sswitch_3
    const v0, 0x7f02016f

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 48
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Laoz;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laoz;

    return-object v0

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch
.end method
