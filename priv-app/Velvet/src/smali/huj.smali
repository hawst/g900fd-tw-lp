.class Lhuj;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lhui;


# instance fields
.field private final UG:Landroid/widget/TextView;

.field private final aoA:Landroid/widget/TextView;

.field private final dqP:Lhuc;

.field private drf:Lhud;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-virtual {p0}, Lhuj;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 34
    const v1, 0x7f04009e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 36
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lhuj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhuj;->aoA:Landroid/widget/TextView;

    .line 37
    const v0, 0x7f1100bc

    invoke-virtual {p0, v0}, Lhuj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhuj;->UG:Landroid/widget/TextView;

    .line 38
    new-instance v0, Lhuc;

    invoke-direct {v0, v1}, Lhuc;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lhuj;->dqP:Lhuc;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lhud;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lhuj;->drf:Lhud;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 44
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhud;

    iput-object v0, p0, Lhuj;->drf:Lhud;

    .line 45
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aQo()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lhuj;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQo()V

    .line 55
    return-void
.end method

.method public final aQp()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lhuj;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQp()V

    .line 60
    return-void
.end method

.method public final aSa()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    const v0, 0x7f0a094b

    iget-object v1, p0, Lhuj;->aoA:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lhuj;->aoA:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 65
    const v0, 0x7f0a094c

    iget-object v1, p0, Lhuj;->UG:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lhuj;->UG:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 66
    return-void
.end method

.method public final oa(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lhuj;->dqP:Lhuc;

    invoke-virtual {v0, p1}, Lhuc;->oa(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 84
    const-class v0, Lhuj;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 85
    return-void
.end method
