.class public final Lbfs;
.super Ljava/lang/Object;


# instance fields
.field private IN:I

.field private final azN:Ljava/lang/String;

.field private azO:Ljava/lang/String;

.field private azP:Lbft;

.field public azQ:J

.field private azR:Ljava/util/List;

.field private azS:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lorg/json/JSONObject;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfs;->azN:Ljava/lang/String;

    const-string v0, "streamType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "NONE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iput v1, p0, Lbfs;->IN:I

    :goto_0
    const-string v0, "contentType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfs;->azO:Ljava/lang/String;

    const-string v0, "metadata"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "metadata"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "metadataType"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    new-instance v3, Lbft;

    invoke-direct {v3, v2}, Lbft;-><init>(I)V

    iput-object v3, p0, Lbfs;->azP:Lbft;

    iget-object v2, p0, Lbfs;->azP:Lbft;

    invoke-virtual {v2, v0}, Lbft;->a(Lorg/json/JSONObject;)V

    :cond_0
    const-string v0, "duration"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    iput-wide v2, p0, Lbfs;->azQ:J

    const-string v0, "tracks"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbfs;->azR:Ljava/util/List;

    const-string v0, "tracks"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    move v0, v1

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_5

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    new-instance v4, Lbfw;

    invoke-direct {v4, v3}, Lbfw;-><init>(Lorg/json/JSONObject;)V

    iget-object v3, p0, Lbfs;->azR:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-string v2, "BUFFERED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput v7, p0, Lbfs;->IN:I

    goto :goto_0

    :cond_2
    const-string v2, "LIVE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput v6, p0, Lbfs;->IN:I

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Lbfs;->IN:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lbfs;->azR:Ljava/util/List;

    :cond_5
    const-string v0, "textTrackStyle"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "textTrackStyle"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v2, Lbgi;

    invoke-direct {v2}, Lbgi;-><init>()V

    invoke-virtual {v2}, Lbgi;->clear()V

    const-string v3, "fontScale"

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, v2, Lbgi;->aAx:F

    const-string v3, "foregroundColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbgi;->fc(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lbgi;->aAy:I

    const-string v3, "backgroundColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbgi;->fc(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lbgi;->aAz:I

    const-string v3, "edgeType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "edgeType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NONE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    iput v1, v2, Lbgi;->aAA:I

    :cond_6
    :goto_2
    const-string v3, "edgeColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbgi;->fc(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lbgi;->aAB:I

    const-string v3, "windowType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "windowType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NONE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    iput v1, v2, Lbgi;->aAC:I

    :cond_7
    :goto_3
    const-string v3, "windowColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbgi;->fc(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lbgi;->aAD:I

    iget v3, v2, Lbgi;->aAC:I

    if-ne v3, v6, :cond_8

    const-string v3, "windowRoundedCornerRadius"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lbgi;->aAE:I

    :cond_8
    const-string v3, "fontFamily"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lbgi;->aAF:Ljava/lang/String;

    const-string v3, "fontGenericFamily"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "fontGenericFamily"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "SANS_SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    iput v1, v2, Lbgi;->aAG:I

    :cond_9
    :goto_4
    const-string v3, "fontStyle"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "fontStyle"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NORMAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    iput v1, v2, Lbgi;->aAH:I

    :cond_a
    :goto_5
    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, v2, Lbgi;->azS:Lorg/json/JSONObject;

    :cond_b
    const-string v0, "customData"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lbfs;->azS:Lorg/json/JSONObject;

    return-void

    :cond_c
    const-string v4, "OUTLINE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    iput v7, v2, Lbgi;->aAA:I

    goto/16 :goto_2

    :cond_d
    const-string v4, "DROP_SHADOW"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    iput v6, v2, Lbgi;->aAA:I

    goto/16 :goto_2

    :cond_e
    const-string v4, "RAISED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    iput v8, v2, Lbgi;->aAA:I

    goto/16 :goto_2

    :cond_f
    const-string v4, "DEPRESSED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iput v9, v2, Lbgi;->aAA:I

    goto/16 :goto_2

    :cond_10
    const-string v4, "NORMAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    iput v7, v2, Lbgi;->aAC:I

    goto/16 :goto_3

    :cond_11
    const-string v4, "ROUNDED_CORNERS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iput v6, v2, Lbgi;->aAC:I

    goto/16 :goto_3

    :cond_12
    const-string v4, "MONOSPACED_SANS_SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    iput v7, v2, Lbgi;->aAG:I

    goto :goto_4

    :cond_13
    const-string v4, "SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    iput v6, v2, Lbgi;->aAG:I

    goto/16 :goto_4

    :cond_14
    const-string v4, "MONOSPACED_SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    iput v8, v2, Lbgi;->aAG:I

    goto/16 :goto_4

    :cond_15
    const-string v4, "CASUAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    iput v9, v2, Lbgi;->aAG:I

    goto/16 :goto_4

    :cond_16
    const-string v4, "CURSIVE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    const/4 v3, 0x5

    iput v3, v2, Lbgi;->aAG:I

    goto/16 :goto_4

    :cond_17
    const-string v4, "SMALL_CAPITALS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v3, 0x6

    iput v3, v2, Lbgi;->aAG:I

    goto/16 :goto_4

    :cond_18
    const-string v1, "BOLD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    iput v7, v2, Lbgi;->aAH:I

    goto/16 :goto_5

    :cond_19
    const-string v1, "ITALIC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iput v6, v2, Lbgi;->aAH:I

    goto/16 :goto_5

    :cond_1a
    const-string v1, "BOLD_ITALIC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iput v8, v2, Lbgi;->aAH:I

    goto/16 :goto_5
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v0, p1, Lbfs;

    if-eqz v0, :cond_0

    check-cast p1, Lbfs;

    iget-object v0, p0, Lbfs;->azS:Lorg/json/JSONObject;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p1, Lbfs;->azS:Lorg/json/JSONObject;

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lbfs;->azS:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lbfs;->azS:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbfs;->azS:Lorg/json/JSONObject;

    iget-object v3, p1, Lbfs;->azS:Lorg/json/JSONObject;

    invoke-static {v0, v3}, Lbjx;->g(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lbfs;->azN:Ljava/lang/String;

    iget-object v3, p1, Lbfs;->azN:Ljava/lang/String;

    invoke-static {v0, v3}, Lblo;->h(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbfs;->IN:I

    iget v3, p1, Lbfs;->IN:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lbfs;->azO:Ljava/lang/String;

    iget-object v3, p1, Lbfs;->azO:Ljava/lang/String;

    invoke-static {v0, v3}, Lblo;->h(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbfs;->azP:Lbft;

    iget-object v3, p1, Lbfs;->azP:Lbft;

    invoke-static {v0, v3}, Lblo;->h(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v4, p0, Lbfs;->azQ:J

    iget-wide v6, p1, Lbfs;->azQ:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lbfs;->azN:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lbfs;->IN:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lbfs;->azO:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lbfs;->azP:Lbft;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lbfs;->azQ:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lbfs;->azS:Lorg/json/JSONObject;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
