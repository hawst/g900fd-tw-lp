.class public final Luc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic Fn:Luj;

.field private synthetic Fo:Landroid/view/animation/Interpolator;

.field private synthetic Fp:Landroid/view/animation/Interpolator;

.field private synthetic Fq:F

.field private synthetic Fr:F

.field private synthetic Fs:F

.field private synthetic Ft:F

.field private synthetic Fu:F

.field private synthetic Fv:F

.field private synthetic Fw:F

.field private synthetic Fx:Landroid/graphics/Rect;

.field private synthetic Fy:Landroid/graphics/Rect;

.field private synthetic Fz:Lcom/android/launcher3/DragLayer;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/DragLayer;Luj;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;FFFFFFFLandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 677
    iput-object p1, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    iput-object p2, p0, Luc;->Fn:Luj;

    iput-object p3, p0, Luc;->Fo:Landroid/view/animation/Interpolator;

    iput-object p4, p0, Luc;->Fp:Landroid/view/animation/Interpolator;

    iput p5, p0, Luc;->Fq:F

    iput p6, p0, Luc;->Fr:F

    iput p7, p0, Luc;->Fs:F

    iput p8, p0, Luc;->Ft:F

    iput p9, p0, Luc;->Fu:F

    iput p10, p0, Luc;->Fv:F

    iput p11, p0, Luc;->Fw:F

    iput-object p12, p0, Luc;->Fx:Landroid/graphics/Rect;

    iput-object p13, p0, Luc;->Fy:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 680
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 681
    iget-object v0, p0, Luc;->Fn:Luj;

    invoke-virtual {v0}, Luj;->getMeasuredWidth()I

    move-result v3

    .line 682
    iget-object v0, p0, Luc;->Fn:Luj;

    invoke-virtual {v0}, Luj;->getMeasuredHeight()I

    move-result v4

    .line 684
    iget-object v0, p0, Luc;->Fo:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    move v0, v1

    .line 686
    :goto_0
    iget-object v2, p0, Luc;->Fp:Landroid/view/animation/Interpolator;

    if-nez v2, :cond_1

    move v2, v1

    .line 689
    :goto_1
    iget v5, p0, Luc;->Fq:F

    iget v6, p0, Luc;->Fr:F

    mul-float/2addr v5, v6

    .line 690
    iget v6, p0, Luc;->Fs:F

    iget v7, p0, Luc;->Fr:F

    mul-float/2addr v6, v7

    .line 691
    iget v7, p0, Luc;->Ft:F

    mul-float/2addr v7, v1

    sub-float v8, v10, v1

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    .line 692
    iget v8, p0, Luc;->Fu:F

    mul-float/2addr v8, v1

    sub-float v1, v10, v1

    mul-float/2addr v1, v6

    add-float/2addr v1, v8

    .line 693
    iget v8, p0, Luc;->Fv:F

    mul-float/2addr v8, v0

    iget v9, p0, Luc;->Fw:F

    sub-float v0, v10, v0

    mul-float/2addr v0, v9

    add-float/2addr v8, v0

    .line 695
    iget-object v0, p0, Luc;->Fx:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float/2addr v5, v10

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float/2addr v3, v11

    add-float/2addr v0, v3

    .line 696
    iget-object v3, p0, Luc;->Fx:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    sub-float v5, v6, v10

    int-to-float v4, v4

    mul-float/2addr v4, v5

    div-float/2addr v4, v11

    add-float/2addr v3, v4

    .line 698
    iget-object v4, p0, Luc;->Fy:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v4, v0

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v4, v0

    .line 699
    iget-object v0, p0, Luc;->Fy:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float/2addr v0, v3

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    float-to-int v2, v0

    .line 701
    iget-object v0, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v0}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/DragLayer;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 704
    :goto_2
    iget-object v3, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v3}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v3

    invoke-virtual {v3}, Luj;->getScrollX()I

    move-result v3

    sub-int v3, v4, v3

    add-int/2addr v0, v3

    .line 705
    iget-object v3, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v3}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v3

    invoke-virtual {v3}, Luj;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    .line 707
    iget-object v3, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v3}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v3

    int-to-float v0, v0

    invoke-virtual {v3, v0}, Luj;->setTranslationX(F)V

    .line 708
    iget-object v0, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v0}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v0

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Luj;->setTranslationY(F)V

    .line 709
    iget-object v0, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v0}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v0

    invoke-virtual {v0, v7}, Luj;->setScaleX(F)V

    .line 710
    iget-object v0, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v0}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v0

    invoke-virtual {v0, v1}, Luj;->setScaleY(F)V

    .line 711
    iget-object v0, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v0}, Lcom/android/launcher3/DragLayer;->c(Lcom/android/launcher3/DragLayer;)Luj;

    move-result-object v0

    invoke-virtual {v0, v8}, Luj;->setAlpha(F)V

    .line 712
    return-void

    .line 684
    :cond_0
    iget-object v0, p0, Luc;->Fo:Landroid/view/animation/Interpolator;

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto/16 :goto_0

    .line 686
    :cond_1
    iget-object v2, p0, Luc;->Fp:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    goto/16 :goto_1

    .line 701
    :cond_2
    iget-object v0, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v0}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/DragLayer;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    iget-object v3, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v3}, Lcom/android/launcher3/DragLayer;->b(Lcom/android/launcher3/DragLayer;)I

    move-result v3

    iget-object v5, p0, Luc;->Fz:Lcom/android/launcher3/DragLayer;

    invoke-static {v5}, Lcom/android/launcher3/DragLayer;->a(Lcom/android/launcher3/DragLayer;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getScrollX()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto :goto_2
.end method
