.class public final Ljeq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efQ:I

.field private efR:I

.field private efS:Z

.field private efT:Z

.field private efU:I

.field private efV:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 10297
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10298
    const/4 v0, 0x0

    iput v0, p0, Ljeq;->aez:I

    iput-object v2, p0, Ljeq;->efN:Ljez;

    iput v1, p0, Ljeq;->efQ:I

    const/4 v0, 0x2

    iput v0, p0, Ljeq;->efR:I

    iput-boolean v1, p0, Ljeq;->efS:Z

    iput-boolean v1, p0, Ljeq;->efT:Z

    const/16 v0, 0x3c

    iput v0, p0, Ljeq;->efU:I

    const/16 v0, 0x78

    iput v0, p0, Ljeq;->efV:I

    iput-object v2, p0, Ljeq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljeq;->eCz:I

    .line 10299
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 10161
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljeq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljeq;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljeq;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljeq;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljeq;->efS:Z

    iget v0, p0, Ljeq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljeq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljeq;->efT:Z

    iget v0, p0, Ljeq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljeq;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljeq;->efQ:I

    iget v0, p0, Ljeq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljeq;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljeq;->efU:I

    iget v0, p0, Ljeq;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljeq;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljeq;->efV:I

    iget v0, p0, Ljeq;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljeq;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljeq;->efR:I

    iget v0, p0, Ljeq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljeq;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 10318
    iget-object v0, p0, Ljeq;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 10319
    const/4 v0, 0x1

    iget-object v1, p0, Ljeq;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 10321
    :cond_0
    iget v0, p0, Ljeq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 10322
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljeq;->efS:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10324
    :cond_1
    iget v0, p0, Ljeq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 10325
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljeq;->efT:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10327
    :cond_2
    iget v0, p0, Ljeq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 10328
    const/4 v0, 0x4

    iget v1, p0, Ljeq;->efQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10330
    :cond_3
    iget v0, p0, Ljeq;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 10331
    const/4 v0, 0x5

    iget v1, p0, Ljeq;->efU:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10333
    :cond_4
    iget v0, p0, Ljeq;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 10334
    const/4 v0, 0x6

    iget v1, p0, Ljeq;->efV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10336
    :cond_5
    iget v0, p0, Ljeq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 10337
    const/4 v0, 0x7

    iget v1, p0, Ljeq;->efR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10339
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10340
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 10344
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10345
    iget-object v1, p0, Ljeq;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 10346
    const/4 v1, 0x1

    iget-object v2, p0, Ljeq;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10349
    :cond_0
    iget v1, p0, Ljeq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 10350
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljeq;->efS:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10353
    :cond_1
    iget v1, p0, Ljeq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 10354
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljeq;->efT:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10357
    :cond_2
    iget v1, p0, Ljeq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 10358
    const/4 v1, 0x4

    iget v2, p0, Ljeq;->efQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10361
    :cond_3
    iget v1, p0, Ljeq;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 10362
    const/4 v1, 0x5

    iget v2, p0, Ljeq;->efU:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10365
    :cond_4
    iget v1, p0, Ljeq;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 10366
    const/4 v1, 0x6

    iget v2, p0, Ljeq;->efV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10369
    :cond_5
    iget v1, p0, Ljeq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 10370
    const/4 v1, 0x7

    iget v2, p0, Ljeq;->efR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10373
    :cond_6
    return v0
.end method
