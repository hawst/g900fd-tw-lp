.class public final Lcqh;
.super Lcnd;
.source "PG"


# instance fields
.field private final bgd:Lcqs;

.field private final bge:Lcqu;

.field private final bgf:Lcqq;


# direct methods
.method public constructor <init>(Lcqs;Lcqu;Landroid/content/Context;Lcfo;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p3, p4}, Lcnd;-><init>(Landroid/content/Context;Lcfo;)V

    .line 46
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqs;

    iput-object v0, p0, Lcqh;->bgd:Lcqs;

    .line 47
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqu;

    iput-object v0, p0, Lcqh;->bge:Lcqu;

    .line 48
    invoke-virtual {p4}, Lcfo;->Et()Lcqq;

    move-result-object v0

    iput-object v0, p0, Lcqh;->bgf:Lcqq;

    .line 49
    return-void
.end method

.method private a(Ldfw;)V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lcqh;->bgf:Lcqq;

    invoke-virtual {p1}, Ldfw;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcqq;->bgA:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcqq;->bgC:Lcqw;

    invoke-virtual {v0, p1}, Lcqw;->c(Ldfw;)V

    :goto_0
    monitor-exit v2

    return-void

    :cond_0
    iget-object v0, v0, Lcqq;->bgB:Landroid/util/LruCache;

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final QJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string v0, "complete-server"

    return-object v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Z)Ldfw;
    .locals 7

    .prologue
    .line 76
    iget-object v0, p0, Lcqh;->bgd:Lcqs;

    invoke-virtual {v0, p1, p2}, Lcqs;->b(Lcom/google/android/shared/search/Query;Z)Lcqt;

    move-result-object v2

    .line 77
    const/4 v0, 0x0

    .line 78
    if-eqz v2, :cond_0

    .line 79
    iget-object v1, v2, Lcqt;->bgE:Ljava/lang/String;

    .line 80
    iget-object v2, v2, Lcqt;->bgF:Ljava/lang/String;

    .line 87
    :try_start_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 88
    iget-object v0, p0, Lcqh;->bge:Lcqu;

    invoke-virtual {v0, p1, v2, v1}, Lcqu;->a(Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;)Lcqv;

    move-result-object v3

    .line 89
    iget-object v0, v3, Lcqv;->bgG:Ldfw;

    invoke-direct {p0, v0}, Lcqh;->a(Ldfw;)V

    .line 90
    iget-object v0, p0, Lcnc;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lr()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, v3, Lcqv;->bgH:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfw;

    .line 93
    invoke-direct {p0, v0}, Lcqh;->a(Ldfw;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    const-string v3, "CompleteServerClient"

    const-string v4, "Error parsing suggestions \'%s\'"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 102
    :cond_0
    :goto_1
    const-string v1, "complete-server"

    invoke-static {v1, p1, v0}, Ldfw;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;)Ldfw;

    move-result-object v0

    :goto_2
    return-object v0

    .line 96
    :cond_1
    :try_start_1
    new-instance v0, Ldfw;

    iget-object v3, v3, Lcqv;->bgG:Ldfw;

    invoke-direct {v0, v3}, Ldfw;-><init>(Ldfw;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 100
    goto :goto_1
.end method

.method public final b(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcqh;->bgf:Lcqq;

    iget-object v1, v0, Lcqq;->bgA:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcqq;->bgC:Lcqw;

    invoke-virtual {v2}, Lcqw;->clearCache()V

    iget-object v0, v0, Lcqq;->bgB:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    iget-object v0, p0, Lcqh;->bgd:Lcqs;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcqs;->iv(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcqh;->bgf:Lcqq;

    iget-object v1, v0, Lcqq;->bgA:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcqq;->bgC:Lcqw;

    iget-boolean v2, v0, Lcqw;->mClosed:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcqw;->mClosed:Z

    iget-object v2, v0, Lcqw;->mLoginHelper:Lcrh;

    iget-object v0, v0, Lcqw;->bgp:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v0}, Lcrh;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final synthetic h(Lcom/google/android/shared/search/Query;)Lddw;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcqh;->s(Lcom/google/android/shared/search/Query;)Ldfw;

    move-result-object v0

    return-object v0
.end method

.method public final s(Lcom/google/android/shared/search/Query;)Ldfw;
    .locals 10

    .prologue
    .line 53
    iget-object v2, p0, Lcqh;->bgf:Lcqq;

    iget-object v0, v2, Lcqq;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iget v3, v2, Lcqq;->bgz:I

    int-to-long v4, v3

    sub-long v4, v0, v4

    iget-object v3, v2, Lcqq;->bgA:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, v2, Lcqq;->bgB:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldfw;

    invoke-virtual {v1}, Ldfw;->getCreationTime()J

    move-result-wide v8

    cmp-long v1, v8, v4

    if-gez v1, :cond_0

    iget-object v1, v2, Lcqq;->bgB:Landroid/util/LruCache;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    iget-object v0, p0, Lcqh;->bgf:Lcqq;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcqq;->iu(Ljava/lang/String;)Ldfw;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_2

    .line 61
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldfw;->dt(Z)V

    .line 63
    :cond_2
    return-object v0
.end method
