.class public final Ljdt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ecO:[Ljdt;


# instance fields
.field private aez:I

.field private dHr:Ljava/lang/String;

.field private ecP:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22376
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 22377
    const/4 v0, 0x0

    iput v0, p0, Ljdt;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljdt;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljdt;->ecP:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljdt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdt;->eCz:I

    .line 22378
    return-void
.end method

.method public static bhM()[Ljdt;
    .locals 2

    .prologue
    .line 22322
    sget-object v0, Ljdt;->ecO:[Ljdt;

    if-nez v0, :cond_1

    .line 22323
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 22325
    :try_start_0
    sget-object v0, Ljdt;->ecO:[Ljdt;

    if-nez v0, :cond_0

    .line 22326
    const/4 v0, 0x0

    new-array v0, v0, [Ljdt;

    sput-object v0, Ljdt;->ecO:[Ljdt;

    .line 22328
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22330
    :cond_1
    sget-object v0, Ljdt;->ecO:[Ljdt;

    return-object v0

    .line 22328
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 22316
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdt;->dHr:Ljava/lang/String;

    iget v0, p0, Ljdt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljdt;->ecP:F

    iget v0, p0, Ljdt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdt;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 22392
    iget v0, p0, Ljdt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 22393
    const/4 v0, 0x1

    iget-object v1, p0, Ljdt;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 22395
    :cond_0
    iget v0, p0, Ljdt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 22396
    const/4 v0, 0x2

    iget v1, p0, Ljdt;->ecP:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 22398
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 22399
    return-void
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22338
    iget-object v0, p0, Ljdt;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 22403
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 22404
    iget v1, p0, Ljdt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 22405
    const/4 v1, 0x1

    iget-object v2, p0, Ljdt;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22408
    :cond_0
    iget v1, p0, Ljdt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 22409
    const/4 v1, 0x2

    iget v2, p0, Ljdt;->ecP:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 22412
    :cond_1
    return v0
.end method
