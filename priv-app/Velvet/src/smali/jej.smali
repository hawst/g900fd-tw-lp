.class public final Ljej;
.super Ljsl;
.source "PG"


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field public amc:[Liyy;

.field private dRq:J

.field private dVt:I

.field private eeA:Z

.field public eeB:Ljbn;

.field private eeC:Ljava/lang/String;

.field private eeD:I

.field private eeE:I

.field private eeF:[B

.field private eeG:Z

.field public eew:[Ljhm;

.field private eex:I

.field private eey:Z

.field private eez:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30771
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 30772
    iput v2, p0, Ljej;->aez:I

    invoke-static {}, Ljhm;->blV()[Ljhm;

    move-result-object v0

    iput-object v0, p0, Ljej;->eew:[Ljhm;

    invoke-static {}, Liyy;->bcA()[Liyy;

    move-result-object v0

    iput-object v0, p0, Ljej;->amc:[Liyy;

    const-string v0, ""

    iput-object v0, p0, Ljej;->aeE:Ljava/lang/String;

    iput v2, p0, Ljej;->eex:I

    iput-boolean v3, p0, Ljej;->eey:Z

    iput-boolean v3, p0, Ljej;->eez:Z

    iput-boolean v2, p0, Ljej;->eeA:Z

    iput-object v4, p0, Ljej;->eeB:Ljbn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljej;->dRq:J

    iput v2, p0, Ljej;->dVt:I

    const-string v0, ""

    iput-object v0, p0, Ljej;->eeC:Ljava/lang/String;

    iput v2, p0, Ljej;->eeD:I

    iput v3, p0, Ljej;->eeE:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljej;->eeF:[B

    iput-boolean v2, p0, Ljej;->eeG:Z

    iput-object v4, p0, Ljej;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljej;->eCz:I

    .line 30773
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 30502
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljej;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljej;->eew:[Ljhm;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhm;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljej;->eew:[Ljhm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljhm;

    invoke-direct {v3}, Ljhm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljej;->eew:[Ljhm;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljhm;

    invoke-direct {v3}, Ljhm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljej;->eew:[Ljhm;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljej;->aeE:Ljava/lang/String;

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljej;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljej;->eex:I

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljej;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljej;->eey:Z

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljej;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljej;->eeB:Ljbn;

    if-nez v0, :cond_4

    new-instance v0, Ljbn;

    invoke-direct {v0}, Ljbn;-><init>()V

    iput-object v0, p0, Ljej;->eeB:Ljbn;

    :cond_4
    iget-object v0, p0, Ljej;->eeB:Ljbn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljej;->dRq:J

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljej;->dVt:I

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljej;->eeA:Z

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljej;->eeD:I

    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljej;->eeE:I

    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljej;->eeC:Ljava/lang/String;

    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljej;->eeF:[B

    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljej;->eeG:Z

    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljej;->eez:Z

    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljej;->aez:I

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljej;->amc:[Liyy;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Liyy;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljej;->amc:[Liyy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Liyy;

    invoke-direct {v3}, Liyy;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljej;->amc:[Liyy;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Liyy;

    invoke-direct {v3}, Liyy;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljej;->amc:[Liyy;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 30800
    iget-object v0, p0, Ljej;->eew:[Ljhm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljej;->eew:[Ljhm;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 30801
    :goto_0
    iget-object v2, p0, Ljej;->eew:[Ljhm;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 30802
    iget-object v2, p0, Ljej;->eew:[Ljhm;

    aget-object v2, v2, v0

    .line 30803
    if-eqz v2, :cond_0

    .line 30804
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 30801
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30808
    :cond_1
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 30809
    const/4 v0, 0x2

    iget-object v2, p0, Ljej;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 30811
    :cond_2
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 30812
    const/4 v0, 0x3

    iget v2, p0, Ljej;->eex:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 30814
    :cond_3
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 30815
    const/4 v0, 0x4

    iget-boolean v2, p0, Ljej;->eey:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 30817
    :cond_4
    iget-object v0, p0, Ljej;->eeB:Ljbn;

    if-eqz v0, :cond_5

    .line 30818
    const/4 v0, 0x5

    iget-object v2, p0, Ljej;->eeB:Ljbn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 30820
    :cond_5
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 30821
    const/4 v0, 0x6

    iget-wide v2, p0, Ljej;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 30823
    :cond_6
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 30824
    const/4 v0, 0x7

    iget v2, p0, Ljej;->dVt:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 30826
    :cond_7
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    .line 30827
    const/16 v0, 0x8

    iget-boolean v2, p0, Ljej;->eeA:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 30829
    :cond_8
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_9

    .line 30830
    const/16 v0, 0x9

    iget v2, p0, Ljej;->eeD:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 30832
    :cond_9
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_a

    .line 30833
    const/16 v0, 0xa

    iget v2, p0, Ljej;->eeE:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 30835
    :cond_a
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_b

    .line 30836
    const/16 v0, 0xb

    iget-object v2, p0, Ljej;->eeC:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 30838
    :cond_b
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_c

    .line 30839
    const/16 v0, 0xc

    iget-object v2, p0, Ljej;->eeF:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    .line 30841
    :cond_c
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_d

    .line 30842
    const/16 v0, 0xd

    iget-boolean v2, p0, Ljej;->eeG:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 30844
    :cond_d
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_e

    .line 30845
    const/16 v0, 0xe

    iget-boolean v2, p0, Ljej;->eez:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 30847
    :cond_e
    iget-object v0, p0, Ljej;->amc:[Liyy;

    if-eqz v0, :cond_10

    iget-object v0, p0, Ljej;->amc:[Liyy;

    array-length v0, v0

    if-lez v0, :cond_10

    .line 30848
    :goto_1
    iget-object v0, p0, Ljej;->amc:[Liyy;

    array-length v0, v0

    if-ge v1, v0, :cond_10

    .line 30849
    iget-object v0, p0, Ljej;->amc:[Liyy;

    aget-object v0, v0, v1

    .line 30850
    if-eqz v0, :cond_f

    .line 30851
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 30848
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 30855
    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 30856
    return-void
.end method

.method public final ak([B)Ljej;
    .locals 1

    .prologue
    .line 30736
    if-nez p1, :cond_0

    .line 30737
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30739
    :cond_0
    iput-object p1, p0, Ljej;->eeF:[B

    .line 30740
    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljej;->aez:I

    .line 30741
    return-object p0
.end method

.method public final biA()Z
    .locals 1

    .prologue
    .line 30575
    iget-boolean v0, p0, Ljej;->eey:Z

    return v0
.end method

.method public final biB()Z
    .locals 1

    .prologue
    .line 30594
    iget-boolean v0, p0, Ljej;->eez:Z

    return v0
.end method

.method public final biC()Z
    .locals 1

    .prologue
    .line 30602
    iget v0, p0, Ljej;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final biD()I
    .locals 1

    .prologue
    .line 30695
    iget v0, p0, Ljej;->eeD:I

    return v0
.end method

.method public final biE()[B
    .locals 1

    .prologue
    .line 30733
    iget-object v0, p0, Ljej;->eeF:[B

    return-object v0
.end method

.method public final biF()Z
    .locals 1

    .prologue
    .line 30744
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final biG()Z
    .locals 1

    .prologue
    .line 30755
    iget-boolean v0, p0, Ljej;->eeG:Z

    return v0
.end method

.method public final biH()Z
    .locals 1

    .prologue
    .line 30763
    iget v0, p0, Ljej;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final da(J)Ljej;
    .locals 1

    .prologue
    .line 30638
    iput-wide p1, p0, Ljej;->dRq:J

    .line 30639
    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljej;->aez:I

    .line 30640
    return-object p0
.end method

.method public final hP(Z)Ljej;
    .locals 1

    .prologue
    .line 30578
    iput-boolean p1, p0, Ljej;->eey:Z

    .line 30579
    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljej;->aez:I

    .line 30580
    return-object p0
.end method

.method public final hQ(Z)Ljej;
    .locals 1

    .prologue
    .line 30597
    iput-boolean p1, p0, Ljej;->eez:Z

    .line 30598
    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljej;->aez:I

    .line 30599
    return-object p0
.end method

.method public final hR(Z)Ljej;
    .locals 1

    .prologue
    .line 30616
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljej;->eeA:Z

    .line 30617
    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljej;->aez:I

    .line 30618
    return-object p0
.end method

.method public final hS(Z)Ljej;
    .locals 1

    .prologue
    .line 30758
    iput-boolean p1, p0, Ljej;->eeG:Z

    .line 30759
    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljej;->aez:I

    .line 30760
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 30860
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 30861
    iget-object v2, p0, Ljej;->eew:[Ljhm;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljej;->eew:[Ljhm;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 30862
    :goto_0
    iget-object v3, p0, Ljej;->eew:[Ljhm;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 30863
    iget-object v3, p0, Ljej;->eew:[Ljhm;

    aget-object v3, v3, v0

    .line 30864
    if-eqz v3, :cond_0

    .line 30865
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 30862
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 30870
    :cond_2
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 30871
    const/4 v2, 0x2

    iget-object v3, p0, Ljej;->aeE:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 30874
    :cond_3
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    .line 30875
    const/4 v2, 0x3

    iget v3, p0, Ljej;->eex:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 30878
    :cond_4
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_5

    .line 30879
    const/4 v2, 0x4

    iget-boolean v3, p0, Ljej;->eey:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 30882
    :cond_5
    iget-object v2, p0, Ljej;->eeB:Ljbn;

    if-eqz v2, :cond_6

    .line 30883
    const/4 v2, 0x5

    iget-object v3, p0, Ljej;->eeB:Ljbn;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 30886
    :cond_6
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_7

    .line 30887
    const/4 v2, 0x6

    iget-wide v4, p0, Ljej;->dRq:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 30890
    :cond_7
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_8

    .line 30891
    const/4 v2, 0x7

    iget v3, p0, Ljej;->dVt:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 30894
    :cond_8
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_9

    .line 30895
    const/16 v2, 0x8

    iget-boolean v3, p0, Ljej;->eeA:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 30898
    :cond_9
    iget v2, p0, Ljej;->aez:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_a

    .line 30899
    const/16 v2, 0x9

    iget v3, p0, Ljej;->eeD:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 30902
    :cond_a
    iget v2, p0, Ljej;->aez:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_b

    .line 30903
    const/16 v2, 0xa

    iget v3, p0, Ljej;->eeE:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 30906
    :cond_b
    iget v2, p0, Ljej;->aez:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_c

    .line 30907
    const/16 v2, 0xb

    iget-object v3, p0, Ljej;->eeC:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 30910
    :cond_c
    iget v2, p0, Ljej;->aez:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_d

    .line 30911
    const/16 v2, 0xc

    iget-object v3, p0, Ljej;->eeF:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 30914
    :cond_d
    iget v2, p0, Ljej;->aez:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_e

    .line 30915
    const/16 v2, 0xd

    iget-boolean v3, p0, Ljej;->eeG:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 30918
    :cond_e
    iget v2, p0, Ljej;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_f

    .line 30919
    const/16 v2, 0xe

    iget-boolean v3, p0, Ljej;->eez:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 30922
    :cond_f
    iget-object v2, p0, Ljej;->amc:[Liyy;

    if-eqz v2, :cond_11

    iget-object v2, p0, Ljej;->amc:[Liyy;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 30923
    :goto_1
    iget-object v2, p0, Ljej;->amc:[Liyy;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 30924
    iget-object v2, p0, Ljej;->amc:[Liyy;

    aget-object v2, v2, v1

    .line 30925
    if-eqz v2, :cond_10

    .line 30926
    const/16 v3, 0xf

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 30923
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 30931
    :cond_11
    return v0
.end method

.method public final pi(I)Ljej;
    .locals 1

    .prologue
    .line 30657
    iput p1, p0, Ljej;->dVt:I

    .line 30658
    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljej;->aez:I

    .line 30659
    return-object p0
.end method

.method public final pj(I)Ljej;
    .locals 1

    .prologue
    .line 30698
    iput p1, p0, Ljej;->eeD:I

    .line 30699
    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljej;->aez:I

    .line 30700
    return-object p0
.end method

.method public final pk(I)Ljej;
    .locals 1

    .prologue
    .line 30717
    iput p1, p0, Ljej;->eeE:I

    .line 30718
    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljej;->aez:I

    .line 30719
    return-object p0
.end method

.method public final ue(Ljava/lang/String;)Ljej;
    .locals 1

    .prologue
    .line 30537
    if-nez p1, :cond_0

    .line 30538
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30540
    :cond_0
    iput-object p1, p0, Ljej;->aeE:Ljava/lang/String;

    .line 30541
    iget v0, p0, Ljej;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljej;->aez:I

    .line 30542
    return-object p0
.end method

.method public final uf(Ljava/lang/String;)Ljej;
    .locals 1

    .prologue
    .line 30676
    if-nez p1, :cond_0

    .line 30677
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30679
    :cond_0
    iput-object p1, p0, Ljej;->eeC:Ljava/lang/String;

    .line 30680
    iget v0, p0, Ljej;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljej;->aez:I

    .line 30681
    return-object p0
.end method
