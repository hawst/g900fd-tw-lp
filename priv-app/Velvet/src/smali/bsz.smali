.class public final Lbsz;
.super Ljava/lang/Object;

# interfaces
.implements Lbgo;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final aIP:Lbnv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbgp;Lbgq;I)V
    .locals 6

    const/16 v4, 0x7d

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lbsz;-><init>(Landroid/content/Context;Lbgp;Lbgq;ILjava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lbgp;Lbgq;ILjava/lang/String;)V
    .locals 6

    new-instance v0, Lbnv;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lbnv;-><init>(Landroid/content/Context;Lbgp;Lbgq;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lbsz;-><init>(Lbnv;)V

    return-void
.end method

.method private constructor <init>(Lbnv;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbsz;->aIP:Lbnv;

    return-void
.end method


# virtual methods
.method public final a(Lbtb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lbsz;->aIP:Lbnv;

    new-instance v1, Lbta;

    invoke-direct {v1, p0, p1}, Lbta;-><init>(Lbsz;Lbtb;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, p2, p3, v2}, Lbnv;->b(Lbhh;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final connect()V
    .locals 1

    iget-object v0, p0, Lbsz;->aIP:Lbnv;

    invoke-virtual {v0}, Lbnv;->connect()V

    return-void
.end method

.method public final disconnect()V
    .locals 1

    iget-object v0, p0, Lbsz;->aIP:Lbnv;

    invoke-virtual {v0}, Lbnv;->disconnect()V

    return-void
.end method
