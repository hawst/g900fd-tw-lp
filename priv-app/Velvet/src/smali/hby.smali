.class final Lhby;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private synthetic dcc:Lhbq;

.field private dce:Z

.field private dcf:Z


# direct methods
.method constructor <init>(Lhbq;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 377
    iput-object p1, p0, Lhby;->dcc:Lhbq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378
    iput-boolean v0, p0, Lhby;->dce:Z

    .line 379
    iput-boolean v0, p0, Lhby;->dcf:Z

    return-void
.end method

.method private aNs()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 417
    iget-boolean v1, p0, Lhby;->dcf:Z

    if-nez v1, :cond_0

    .line 418
    iput-boolean v0, p0, Lhby;->dcf:Z

    .line 419
    iget-object v1, p0, Lhby;->dcc:Lhbq;

    const-string v2, "Page ready to be shown"

    invoke-virtual {v1, v2}, Lhbq;->aV(Ljava/lang/Object;)V

    .line 422
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized aNr()Z
    .locals 2

    .prologue
    .line 411
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhby;->dce:Z

    .line 412
    iget-object v0, p0, Lhby;->dcc:Lhbq;

    const-string v1, "pageReady"

    invoke-virtual {v0, v1}, Lhbq;->aV(Ljava/lang/Object;)V

    .line 413
    invoke-direct {p0}, Lhby;->aNs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized delayedPageLoad()V
    .locals 2

    .prologue
    .line 400
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhby;->dce:Z

    .line 401
    iget-object v0, p0, Lhby;->dcc:Lhbq;

    const-string v1, "delayedPageLoad"

    invoke-virtual {v0, v1}, Lhbq;->aV(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    monitor-exit p0

    return-void

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized nL(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 388
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lhby;->dcc:Lhbq;

    new-instance v2, Ldls;

    const-string v3, "pageLoadFinished %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v2, v3, v4}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lhbq;->aV(Ljava/lang/Object;)V

    .line 389
    iget-boolean v1, p0, Lhby;->dce:Z

    if-nez v1, :cond_0

    .line 390
    invoke-direct {p0}, Lhby;->aNs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 392
    :cond_0
    monitor-exit p0

    return v0

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
