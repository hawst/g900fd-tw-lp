.class public final enum Lgjl;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static SIZE:I

.field public static final enum cMB:Lgjl;

.field public static final enum cMC:Lgjl;

.field private static final synthetic cMF:[Lgjl;


# instance fields
.field private final cMD:Ljava/lang/String;

.field private final cME:Z


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 10
    new-instance v0, Lgjl;

    const-string v1, "CONTACT_DIALING"

    const-string v3, "contacts"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgjl;-><init>(Ljava/lang/String;ILjava/lang/String;ZZ)V

    sput-object v0, Lgjl;->cMB:Lgjl;

    .line 11
    new-instance v5, Lgjl;

    const-string v6, "HANDS_FREE_COMMANDS"

    const-string v8, "hands_free_commands"

    move v7, v4

    move v9, v4

    move v10, v2

    invoke-direct/range {v5 .. v10}, Lgjl;-><init>(Ljava/lang/String;ILjava/lang/String;ZZ)V

    sput-object v5, Lgjl;->cMC:Lgjl;

    .line 9
    new-array v0, v11, [Lgjl;

    sget-object v1, Lgjl;->cMB:Lgjl;

    aput-object v1, v0, v2

    sget-object v1, Lgjl;->cMC:Lgjl;

    aput-object v1, v0, v4

    sput-object v0, Lgjl;->cMF:[Lgjl;

    .line 13
    sput v11, Lgjl;->SIZE:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput-object p3, p0, Lgjl;->cMD:Ljava/lang/String;

    .line 37
    iput-boolean p5, p0, Lgjl;->cME:Z

    .line 39
    return-void
.end method

.method public static i(Ljava/io/File;)Lgjl;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgjl;->mT(Ljava/lang/String;)Lgjl;

    move-result-object v0

    return-object v0
.end method

.method public static mT(Ljava/lang/String;)Lgjl;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lgjl;->cMB:Lgjl;

    iget-object v0, v0, Lgjl;->cMD:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lgjl;->cMB:Lgjl;

    .line 70
    :goto_0
    return-object v0

    .line 66
    :cond_0
    sget-object v0, Lgjl;->cMC:Lgjl;

    iget-object v0, v0, Lgjl;->cMD:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    sget-object v0, Lgjl;->cMC:Lgjl;

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lgjl;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lgjl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgjl;

    return-object v0
.end method

.method public static values()[Lgjl;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lgjl;->cMF:[Lgjl;

    invoke-virtual {v0}, [Lgjl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgjl;

    return-object v0
.end method


# virtual methods
.method public final aGv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lgjl;->cMD:Ljava/lang/String;

    return-object v0
.end method

.method public final aGw()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lgjl;->cME:Z

    return v0
.end method

.method public final mS(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "-"

    const-string v3, "_"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lgjl;->cMD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
