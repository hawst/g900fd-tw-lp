.class final Lkj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private jD:I

.field jE:I

.field private jF:I

.field private jG:I

.field private jH:I

.field private jI:I

.field private jJ:I

.field private jK:I

.field final synthetic jL:Lkh;


# direct methods
.method constructor <init>(Lkh;II)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lkj;->jL:Lkh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput p2, p0, Lkj;->jD:I

    .line 192
    iput p3, p0, Lkj;->jE:I

    .line 193
    invoke-virtual {p0}, Lkj;->br()V

    .line 194
    return-void
.end method


# virtual methods
.method final bq()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 202
    iget v1, p0, Lkj;->jE:I

    iget v2, p0, Lkj;->jD:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final br()V
    .locals 5

    .prologue
    .line 214
    const/16 v0, 0xff

    iput v0, p0, Lkj;->jJ:I

    iput v0, p0, Lkj;->jH:I

    iput v0, p0, Lkj;->jF:I

    .line 215
    const/4 v0, 0x0

    iput v0, p0, Lkj;->jK:I

    iput v0, p0, Lkj;->jI:I

    iput v0, p0, Lkj;->jG:I

    .line 217
    iget v0, p0, Lkj;->jD:I

    :goto_0
    iget v1, p0, Lkj;->jE:I

    if-gt v0, v1, :cond_6

    .line 218
    iget-object v1, p0, Lkj;->jL:Lkh;

    invoke-static {v1}, Lkh;->a(Lkh;)[I

    move-result-object v1

    aget v1, v1, v0

    .line 219
    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 220
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    .line 221
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    .line 222
    iget v4, p0, Lkj;->jG:I

    if-le v2, v4, :cond_0

    .line 223
    iput v2, p0, Lkj;->jG:I

    .line 225
    :cond_0
    iget v4, p0, Lkj;->jF:I

    if-ge v2, v4, :cond_1

    .line 226
    iput v2, p0, Lkj;->jF:I

    .line 228
    :cond_1
    iget v2, p0, Lkj;->jI:I

    if-le v3, v2, :cond_2

    .line 229
    iput v3, p0, Lkj;->jI:I

    .line 231
    :cond_2
    iget v2, p0, Lkj;->jH:I

    if-ge v3, v2, :cond_3

    .line 232
    iput v3, p0, Lkj;->jH:I

    .line 234
    :cond_3
    iget v2, p0, Lkj;->jK:I

    if-le v1, v2, :cond_4

    .line 235
    iput v1, p0, Lkj;->jK:I

    .line 237
    :cond_4
    iget v2, p0, Lkj;->jJ:I

    if-ge v1, v2, :cond_5

    .line 238
    iput v1, p0, Lkj;->jJ:I

    .line 217
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 241
    :cond_6
    return-void
.end method

.method final bs()I
    .locals 4

    .prologue
    .line 292
    iget v0, p0, Lkj;->jG:I

    iget v1, p0, Lkj;->jF:I

    sub-int/2addr v0, v1

    iget v1, p0, Lkj;->jI:I

    iget v2, p0, Lkj;->jH:I

    sub-int/2addr v1, v2

    iget v2, p0, Lkj;->jK:I

    iget v3, p0, Lkj;->jJ:I

    sub-int/2addr v2, v3

    if-lt v0, v1, :cond_1

    if-lt v0, v2, :cond_1

    const/4 v0, -0x3

    .line 297
    :goto_0
    iget-object v1, p0, Lkj;->jL:Lkh;

    iget v2, p0, Lkj;->jD:I

    iget v3, p0, Lkj;->jE:I

    invoke-static {v1, v0, v2, v3}, Lkh;->a(Lkh;III)V

    .line 300
    iget-object v1, p0, Lkj;->jL:Lkh;

    invoke-static {v1}, Lkh;->a(Lkh;)[I

    move-result-object v1

    iget v2, p0, Lkj;->jD:I

    iget v3, p0, Lkj;->jE:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->sort([III)V

    .line 303
    iget-object v1, p0, Lkj;->jL:Lkh;

    iget v2, p0, Lkj;->jD:I

    iget v3, p0, Lkj;->jE:I

    invoke-static {v1, v0, v2, v3}, Lkh;->a(Lkh;III)V

    .line 305
    packed-switch v0, :pswitch_data_0

    iget v1, p0, Lkj;->jF:I

    iget v2, p0, Lkj;->jG:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 307
    :goto_1
    iget v2, p0, Lkj;->jD:I

    :goto_2
    iget v3, p0, Lkj;->jE:I

    if-gt v2, v3, :cond_3

    .line 308
    iget-object v3, p0, Lkj;->jL:Lkh;

    invoke-static {v3}, Lkh;->a(Lkh;)[I

    move-result-object v3

    aget v3, v3, v2

    .line 310
    packed-switch v0, :pswitch_data_1

    .line 307
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 292
    :cond_1
    if-lt v1, v0, :cond_2

    if-lt v1, v2, :cond_2

    const/4 v0, -0x2

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    .line 305
    :pswitch_0
    iget v1, p0, Lkj;->jH:I

    iget v2, p0, Lkj;->jI:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    goto :goto_1

    :pswitch_1
    iget v1, p0, Lkj;->jJ:I

    iget v2, p0, Lkj;->jK:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 312
    :pswitch_2
    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    if-lt v3, v1, :cond_0

    .line 329
    :goto_3
    return v2

    .line 317
    :pswitch_3
    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    if-lt v3, v1, :cond_0

    goto :goto_3

    .line 322
    :pswitch_4
    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    if-le v3, v1, :cond_0

    goto :goto_3

    .line 329
    :cond_3
    iget v2, p0, Lkj;->jD:I

    goto :goto_3

    .line 305
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 310
    :pswitch_data_1
    .packed-switch -0x3
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method final bt()Lkp;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 336
    .line 341
    iget v0, p0, Lkj;->jD:I

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_0
    iget v5, p0, Lkj;->jE:I

    if-gt v0, v5, :cond_0

    .line 342
    iget-object v5, p0, Lkj;->jL:Lkh;

    invoke-static {v5}, Lkh;->a(Lkh;)[I

    move-result-object v5

    aget v5, v5, v0

    .line 343
    iget-object v6, p0, Lkj;->jL:Lkh;

    invoke-static {v6}, Lkh;->b(Lkh;)Landroid/util/SparseIntArray;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    .line 345
    add-int/2addr v1, v6

    .line 346
    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v7

    mul-int/2addr v7, v6

    add-int/2addr v4, v7

    .line 347
    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v7

    mul-int/2addr v7, v6

    add-int/2addr v3, v7

    .line 348
    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    mul-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_0
    int-to-float v0, v4

    int-to-float v4, v1

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 352
    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 353
    int-to-float v2, v2

    int-to-float v4, v1

    div-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 355
    new-instance v4, Lkp;

    invoke-direct {v4, v0, v3, v2, v1}, Lkp;-><init>(IIII)V

    return-object v4
.end method

.method final getVolume()I
    .locals 3

    .prologue
    .line 197
    iget v0, p0, Lkj;->jG:I

    iget v1, p0, Lkj;->jF:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lkj;->jI:I

    iget v2, p0, Lkj;->jH:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lkj;->jK:I

    iget v2, p0, Lkj;->jJ:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    return v0
.end method
