.class public final Lkcs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field aen:Ljava/lang/String;

.field bfI:Ljava/lang/String;

.field ePZ:Ljava/lang/String;

.field eQa:Ljava/lang/String;

.field public final eRZ:Lkcq;

.field eSa:Z

.field eSb:I

.field eSc:I

.field eSd:I

.field eSe:Z

.field eSf:Z

.field eSg:J

.field eSh:Ljava/lang/String;

.field eSi:Ljava/lang/String;

.field eSj:Ljava/lang/String;

.field private eSk:Ljava/lang/String;

.field eSl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/net/URI;Lkcq;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v0, p0, Lkcs;->eSb:I

    .line 32
    iput v0, p0, Lkcs;->eSc:I

    .line 33
    iput v0, p0, Lkcs;->eSd:I

    .line 51
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkcs;->eSg:J

    .line 63
    iput-object p2, p0, Lkcs;->eRZ:Lkcq;

    .line 66
    new-instance v1, Lkct;

    invoke-direct {v1, p0}, Lkct;-><init>(Lkcs;)V

    .line 82
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Lkcq;->length()I

    move-result v2

    if-ge v0, v2, :cond_d

    .line 83
    invoke-virtual {p2, v0}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v2

    .line 84
    invoke-virtual {p2, v0}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v3

    .line 85
    const-string v4, "Cache-Control"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 86
    invoke-static {v3, v1}, Lkbw;->a(Ljava/lang/String;Lkbx;)V

    .line 82
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_1
    const-string v4, "Pragma"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 88
    const-string v2, "no-cache"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    iput-boolean v5, p0, Lkcs;->eSa:Z

    goto :goto_1

    .line 91
    :cond_2
    const-string v4, "If-None-Match"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 92
    iput-object v3, p0, Lkcs;->eSl:Ljava/lang/String;

    goto :goto_1

    .line 93
    :cond_3
    const-string v4, "If-Modified-Since"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 94
    iput-object v3, p0, Lkcs;->eSk:Ljava/lang/String;

    goto :goto_1

    .line 95
    :cond_4
    const-string v4, "Authorization"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 96
    iput-boolean v5, p0, Lkcs;->eSf:Z

    goto :goto_1

    .line 97
    :cond_5
    const-string v4, "Content-Length"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 99
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lkcs;->eSg:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 101
    :catch_0
    move-exception v2

    goto :goto_1

    .line 102
    :cond_6
    const-string v4, "Transfer-Encoding"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 103
    iput-object v3, p0, Lkcs;->eSh:Ljava/lang/String;

    goto :goto_1

    .line 104
    :cond_7
    const-string v4, "User-Agent"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 105
    iput-object v3, p0, Lkcs;->aen:Ljava/lang/String;

    goto :goto_1

    .line 106
    :cond_8
    const-string v4, "Host"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 107
    iput-object v3, p0, Lkcs;->ePZ:Ljava/lang/String;

    goto :goto_1

    .line 108
    :cond_9
    const-string v4, "Connection"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 109
    iput-object v3, p0, Lkcs;->eSi:Ljava/lang/String;

    goto :goto_1

    .line 110
    :cond_a
    const-string v4, "Accept-Encoding"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 111
    iput-object v3, p0, Lkcs;->eSj:Ljava/lang/String;

    goto :goto_1

    .line 112
    :cond_b
    const-string v4, "Content-Type"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 113
    iput-object v3, p0, Lkcs;->bfI:Ljava/lang/String;

    goto/16 :goto_1

    .line 114
    :cond_c
    const-string v4, "Proxy-Authorization"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    iput-object v3, p0, Lkcs;->eQa:Ljava/lang/String;

    goto/16 :goto_1

    .line 118
    :cond_d
    return-void
.end method


# virtual methods
.method public final b(Ljava/util/Date;)V
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lkcs;->eSk:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lkcs;->eRZ:Lkcq;

    const-string v1, "If-Modified-Since"

    invoke-virtual {v0, v1}, Lkcq;->AB(Ljava/lang/String;)V

    .line 272
    :cond_0
    invoke-static {p1}, Lkca;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 273
    iget-object v1, p0, Lkcs;->eRZ:Lkcq;

    const-string v2, "If-Modified-Since"

    invoke-virtual {v1, v2, v0}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iput-object v0, p0, Lkcs;->eSk:Ljava/lang/String;

    .line 275
    return-void
.end method

.method public final byS()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lkcs;->eSk:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkcs;->eSl:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setContentLength(J)V
    .locals 5

    .prologue
    .line 209
    iget-wide v0, p0, Lkcs;->eSg:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lkcs;->eRZ:Lkcq;

    const-string v1, "Content-Length"

    invoke-virtual {v0, v1}, Lkcq;->AB(Ljava/lang/String;)V

    .line 212
    :cond_0
    iget-object v0, p0, Lkcs;->eRZ:Lkcq;

    const-string v1, "Content-Length"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iput-wide p1, p0, Lkcs;->eSg:J

    .line 214
    return-void
.end method

.method public final v(Ljava/util/Map;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 295
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 296
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 297
    const-string v2, "Cookie"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "Cookie2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 299
    iget-object v6, p0, Lkcs;->eRZ:Lkcq;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v6, v1, v0}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move v3, v4

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    if-lez v3, :cond_3

    const-string v2, "; "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 302
    :cond_5
    return-void
.end method
