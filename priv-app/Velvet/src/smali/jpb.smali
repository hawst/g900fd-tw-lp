.class public final Ljpb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field public ewY:Ljpc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4363
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4364
    const/4 v0, 0x0

    iput v0, p0, Ljpb;->aez:I

    iput-object v1, p0, Ljpb;->ewY:Ljpc;

    const-string v0, ""

    iput-object v0, p0, Ljpb;->afb:Ljava/lang/String;

    iput-object v1, p0, Ljpb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpb;->eCz:I

    .line 4365
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljpb;->ewY:Ljpc;

    if-nez v0, :cond_1

    new-instance v0, Ljpc;

    invoke-direct {v0}, Ljpc;-><init>()V

    iput-object v0, p0, Ljpb;->ewY:Ljpc;

    :cond_1
    iget-object v0, p0, Ljpb;->ewY:Ljpc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpb;->afb:Ljava/lang/String;

    iget v0, p0, Ljpb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4379
    iget-object v0, p0, Ljpb;->ewY:Ljpc;

    if-eqz v0, :cond_0

    .line 4380
    const/4 v0, 0x1

    iget-object v1, p0, Ljpb;->ewY:Ljpc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4382
    :cond_0
    iget v0, p0, Ljpb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 4383
    const/4 v0, 0x2

    iget-object v1, p0, Ljpb;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4385
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4386
    return-void
.end method

.method public final baV()Z
    .locals 1

    .prologue
    .line 4355
    iget v0, p0, Ljpb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4344
    iget-object v0, p0, Ljpb;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4390
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4391
    iget-object v1, p0, Ljpb;->ewY:Ljpc;

    if-eqz v1, :cond_0

    .line 4392
    const/4 v1, 0x1

    iget-object v2, p0, Ljpb;->ewY:Ljpc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4395
    :cond_0
    iget v1, p0, Ljpb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 4396
    const/4 v1, 0x2

    iget-object v2, p0, Ljpb;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4399
    :cond_1
    return v0
.end method

.method public final xU(Ljava/lang/String;)Ljpb;
    .locals 1

    .prologue
    .line 4347
    if-nez p1, :cond_0

    .line 4348
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4350
    :cond_0
    iput-object p1, p0, Ljpb;->afb:Ljava/lang/String;

    .line 4351
    iget v0, p0, Ljpb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpb;->aez:I

    .line 4352
    return-object p0
.end method
