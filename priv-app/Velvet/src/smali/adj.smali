.class public final Ladj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private Td:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, Ladj;->Td:F

    .line 43
    return-void
.end method


# virtual methods
.method public final bJ(I)V
    .locals 2

    .prologue
    const v0, 0x3fa66666    # 1.3f

    .line 46
    if-lez p1, :cond_0

    int-to-float v1, p1

    div-float/2addr v0, v1

    :cond_0
    iput v0, p0, Ladj;->Td:F

    .line 47
    return-void
.end method

.method public final getInterpolation(F)F
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 54
    sub-float v0, p1, v3

    .line 55
    mul-float v1, v0, v0

    iget v2, p0, Ladj;->Td:F

    add-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget v2, p0, Ladj;->Td:F

    add-float/2addr v0, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    return v0
.end method

.method public final ki()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput v0, p0, Ladj;->Td:F

    .line 51
    return-void
.end method
