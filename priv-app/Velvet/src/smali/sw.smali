.class public final Lsw;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic AQ:Lcom/android/launcher3/CellLayout;

.field private synthetic AR:Lcom/android/launcher3/CellLayout$LayoutParams;

.field private synthetic AW:Landroid/view/View;

.field private AX:Z


# direct methods
.method public constructor <init>(Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/CellLayout$LayoutParams;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1032
    iput-object p1, p0, Lsw;->AQ:Lcom/android/launcher3/CellLayout;

    iput-object p2, p0, Lsw;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    iput-object p3, p0, Lsw;->AW:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 1033
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsw;->AX:Z

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1047
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsw;->AX:Z

    .line 1048
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1038
    iget-boolean v0, p0, Lsw;->AX:Z

    if-nez v0, :cond_0

    .line 1039
    iget-object v0, p0, Lsw;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 1040
    iget-object v0, p0, Lsw;->AW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1042
    :cond_0
    iget-object v0, p0, Lsw;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->c(Lcom/android/launcher3/CellLayout;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lsw;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1043
    iget-object v0, p0, Lsw;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->c(Lcom/android/launcher3/CellLayout;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lsw;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1045
    :cond_1
    return-void
.end method
