.class public final Ljwu;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJE:Ljsm;


# instance fields
.field private aez:I

.field private dRy:Ljava/lang/String;

.field private dzh:Ljava/lang/String;

.field private eHg:Ljava/lang/String;

.field private eHh:Ljava/lang/String;

.field private eHi:Ljava/lang/String;

.field private eHj:Ljava/lang/String;

.field private eHk:Ljava/lang/String;

.field public eHq:[Ljava/lang/String;

.field private eJF:I

.field private eJG:I

.field private eJH:I

.field private eJI:[Ljava/lang/String;

.field private evR:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1227
    const/16 v0, 0xb

    const-class v1, Ljwu;

    const v2, 0x23e382

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwu;->eJE:Ljsm;

    .line 1233
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1479
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1480
    iput v1, p0, Ljwu;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljwu;->dzh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->evR:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljwu;->eHq:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->eHg:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->eHh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->dRy:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->eHk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->eHi:Ljava/lang/String;

    iput v1, p0, Ljwu;->eJF:I

    iput v1, p0, Ljwu;->eJG:I

    iput v1, p0, Ljwu;->eJH:I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwu;->eHj:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljwu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwu;->eCz:I

    .line 1481
    return-void
.end method


# virtual methods
.method public final OJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1265
    iget-object v0, p0, Ljwu;->evR:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1220
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->dzh:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->dRy:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->evR:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwu;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwu;->eHq:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljwu;->eHq:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljwu;->eHq:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljwu;->eHq:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->eHg:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwu;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->eHh:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->eHk:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->eHi:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwu;->eJF:I

    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwu;->eJG:I

    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwu;->eJH:I

    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljwu;->eJI:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljwu;->eJI:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwu;->eHj:Ljava/lang/String;

    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljwu;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x42 -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
        0x5a -> :sswitch_8
        0x60 -> :sswitch_9
        0x68 -> :sswitch_a
        0x70 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1506
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1507
    const/4 v0, 0x1

    iget-object v2, p0, Ljwu;->dzh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1509
    :cond_0
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 1510
    const/4 v0, 0x2

    iget-object v2, p0, Ljwu;->dRy:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1512
    :cond_1
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 1513
    const/4 v0, 0x4

    iget-object v2, p0, Ljwu;->evR:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1515
    :cond_2
    iget-object v0, p0, Ljwu;->eHq:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljwu;->eHq:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 1516
    :goto_0
    iget-object v2, p0, Ljwu;->eHq:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1517
    iget-object v2, p0, Ljwu;->eHq:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 1518
    if-eqz v2, :cond_3

    .line 1519
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1516
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1523
    :cond_4
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 1524
    const/16 v0, 0x8

    iget-object v2, p0, Ljwu;->eHg:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1526
    :cond_5
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 1527
    const/16 v0, 0x9

    iget-object v2, p0, Ljwu;->eHh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1529
    :cond_6
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 1530
    const/16 v0, 0xa

    iget-object v2, p0, Ljwu;->eHk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1532
    :cond_7
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 1533
    const/16 v0, 0xb

    iget-object v2, p0, Ljwu;->eHi:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1535
    :cond_8
    iget v0, p0, Ljwu;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_9

    .line 1536
    const/16 v0, 0xc

    iget v2, p0, Ljwu;->eJF:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 1538
    :cond_9
    iget v0, p0, Ljwu;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_a

    .line 1539
    const/16 v0, 0xd

    iget v2, p0, Ljwu;->eJG:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 1541
    :cond_a
    iget v0, p0, Ljwu;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_b

    .line 1542
    const/16 v0, 0xe

    iget v2, p0, Ljwu;->eJH:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 1544
    :cond_b
    iget-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_d

    .line 1545
    :goto_1
    iget-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 1546
    iget-object v0, p0, Ljwu;->eJI:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 1547
    if-eqz v0, :cond_c

    .line 1548
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 1545
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1552
    :cond_d
    iget v0, p0, Ljwu;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_e

    .line 1553
    const/16 v0, 0x10

    iget-object v1, p0, Ljwu;->eHj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1555
    :cond_e
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1556
    return-void
.end method

.method public final bvz()Z
    .locals 1

    .prologue
    .line 1276
    iget v0, p0, Ljwu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1560
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1561
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1562
    const/4 v1, 0x1

    iget-object v3, p0, Ljwu;->dzh:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1565
    :cond_0
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_1

    .line 1566
    const/4 v1, 0x2

    iget-object v3, p0, Ljwu;->dRy:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1569
    :cond_1
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 1570
    const/4 v1, 0x4

    iget-object v3, p0, Ljwu;->evR:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1573
    :cond_2
    iget-object v1, p0, Ljwu;->eHq:[Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljwu;->eHq:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v2

    move v3, v2

    move v4, v2

    .line 1576
    :goto_0
    iget-object v5, p0, Ljwu;->eHq:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_4

    .line 1577
    iget-object v5, p0, Ljwu;->eHq:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 1578
    if-eqz v5, :cond_3

    .line 1579
    add-int/lit8 v4, v4, 0x1

    .line 1580
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1576
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1584
    :cond_4
    add-int/2addr v0, v3

    .line 1585
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 1587
    :cond_5
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 1588
    const/16 v1, 0x8

    iget-object v3, p0, Ljwu;->eHg:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1591
    :cond_6
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 1592
    const/16 v1, 0x9

    iget-object v3, p0, Ljwu;->eHh:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1595
    :cond_7
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_8

    .line 1596
    const/16 v1, 0xa

    iget-object v3, p0, Ljwu;->eHk:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1599
    :cond_8
    iget v1, p0, Ljwu;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 1600
    const/16 v1, 0xb

    iget-object v3, p0, Ljwu;->eHi:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1603
    :cond_9
    iget v1, p0, Ljwu;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_a

    .line 1604
    const/16 v1, 0xc

    iget v3, p0, Ljwu;->eJF:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1607
    :cond_a
    iget v1, p0, Ljwu;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_b

    .line 1608
    const/16 v1, 0xd

    iget v3, p0, Ljwu;->eJG:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1611
    :cond_b
    iget v1, p0, Ljwu;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_c

    .line 1612
    const/16 v1, 0xe

    iget v3, p0, Ljwu;->eJH:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1615
    :cond_c
    iget-object v1, p0, Ljwu;->eJI:[Ljava/lang/String;

    if-eqz v1, :cond_f

    iget-object v1, p0, Ljwu;->eJI:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_f

    move v1, v2

    move v3, v2

    .line 1618
    :goto_1
    iget-object v4, p0, Ljwu;->eJI:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_e

    .line 1619
    iget-object v4, p0, Ljwu;->eJI:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 1620
    if-eqz v4, :cond_d

    .line 1621
    add-int/lit8 v3, v3, 0x1

    .line 1622
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1618
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1626
    :cond_e
    add-int/2addr v0, v1

    .line 1627
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1629
    :cond_f
    iget v1, p0, Ljwu;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_10

    .line 1630
    const/16 v1, 0x10

    iget-object v2, p0, Ljwu;->eHj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1633
    :cond_10
    return v0
.end method

.method public final sN(I)Ljwu;
    .locals 1

    .prologue
    .line 1403
    iput p1, p0, Ljwu;->eJF:I

    .line 1404
    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljwu;->aez:I

    .line 1405
    return-object p0
.end method

.method public final sO(I)Ljwu;
    .locals 1

    .prologue
    .line 1422
    iput p1, p0, Ljwu;->eJG:I

    .line 1423
    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljwu;->aez:I

    .line 1424
    return-object p0
.end method

.method public final sP(I)Ljwu;
    .locals 1

    .prologue
    .line 1441
    iput p1, p0, Ljwu;->eJH:I

    .line 1442
    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljwu;->aez:I

    .line 1443
    return-object p0
.end method

.method public final zA(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1268
    if-nez p1, :cond_0

    .line 1269
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1271
    :cond_0
    iput-object p1, p0, Ljwu;->evR:Ljava/lang/String;

    .line 1272
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwu;->aez:I

    .line 1273
    return-object p0
.end method

.method public final zB(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1293
    if-nez p1, :cond_0

    .line 1294
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1296
    :cond_0
    iput-object p1, p0, Ljwu;->eHg:Ljava/lang/String;

    .line 1297
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwu;->aez:I

    .line 1298
    return-object p0
.end method

.method public final zC(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1315
    if-nez p1, :cond_0

    .line 1316
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1318
    :cond_0
    iput-object p1, p0, Ljwu;->eHh:Ljava/lang/String;

    .line 1319
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwu;->aez:I

    .line 1320
    return-object p0
.end method

.method public final zD(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1337
    if-nez p1, :cond_0

    .line 1338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1340
    :cond_0
    iput-object p1, p0, Ljwu;->dRy:Ljava/lang/String;

    .line 1341
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwu;->aez:I

    .line 1342
    return-object p0
.end method

.method public final zE(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1359
    if-nez p1, :cond_0

    .line 1360
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1362
    :cond_0
    iput-object p1, p0, Ljwu;->eHk:Ljava/lang/String;

    .line 1363
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljwu;->aez:I

    .line 1364
    return-object p0
.end method

.method public final zF(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1381
    if-nez p1, :cond_0

    .line 1382
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1384
    :cond_0
    iput-object p1, p0, Ljwu;->eHi:Ljava/lang/String;

    .line 1385
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljwu;->aez:I

    .line 1386
    return-object p0
.end method

.method public final zG(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1463
    if-nez p1, :cond_0

    .line 1464
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1466
    :cond_0
    iput-object p1, p0, Ljwu;->eHj:Ljava/lang/String;

    .line 1467
    iget v0, p0, Ljwu;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljwu;->aez:I

    .line 1468
    return-object p0
.end method

.method public final zz(Ljava/lang/String;)Ljwu;
    .locals 1

    .prologue
    .line 1246
    if-nez p1, :cond_0

    .line 1247
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1249
    :cond_0
    iput-object p1, p0, Ljwu;->dzh:Ljava/lang/String;

    .line 1250
    iget v0, p0, Ljwu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwu;->aez:I

    .line 1251
    return-object p0
.end method
