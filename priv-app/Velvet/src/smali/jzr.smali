.class public final Ljzr;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eOm:[Ljzr;


# instance fields
.field private aez:I

.field private eOn:Ljava/lang/String;

.field private eOo:[Ljzw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3763
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3764
    const/4 v0, 0x0

    iput v0, p0, Ljzr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzr;->eOn:Ljava/lang/String;

    invoke-static {}, Ljzw;->bxv()[Ljzw;

    move-result-object v0

    iput-object v0, p0, Ljzr;->eOo:[Ljzw;

    const/4 v0, 0x0

    iput-object v0, p0, Ljzr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzr;->eCz:I

    .line 3765
    return-void
.end method

.method public static bxq()[Ljzr;
    .locals 2

    .prologue
    .line 3725
    sget-object v0, Ljzr;->eOm:[Ljzr;

    if-nez v0, :cond_1

    .line 3726
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3728
    :try_start_0
    sget-object v0, Ljzr;->eOm:[Ljzr;

    if-nez v0, :cond_0

    .line 3729
    const/4 v0, 0x0

    new-array v0, v0, [Ljzr;

    sput-object v0, Ljzr;->eOm:[Ljzr;

    .line 3731
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3733
    :cond_1
    sget-object v0, Ljzr;->eOm:[Ljzr;

    return-object v0

    .line 3731
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3719
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzr;->eOn:Ljava/lang/String;

    iget v0, p0, Ljzr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzr;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzr;->eOo:[Ljzw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljzw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzr;->eOo:[Ljzw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljzw;

    invoke-direct {v3}, Ljzw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzr;->eOo:[Ljzw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljzw;

    invoke-direct {v3}, Ljzw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljzr;->eOo:[Ljzw;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 3779
    iget v0, p0, Ljzr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3780
    const/4 v0, 0x1

    iget-object v1, p0, Ljzr;->eOn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3782
    :cond_0
    iget-object v0, p0, Ljzr;->eOo:[Ljzw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljzr;->eOo:[Ljzw;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3783
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljzr;->eOo:[Ljzw;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3784
    iget-object v1, p0, Ljzr;->eOo:[Ljzw;

    aget-object v1, v1, v0

    .line 3785
    if-eqz v1, :cond_1

    .line 3786
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 3783
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3790
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3791
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 3795
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3796
    iget v1, p0, Ljzr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3797
    const/4 v1, 0x1

    iget-object v2, p0, Ljzr;->eOn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3800
    :cond_0
    iget-object v1, p0, Ljzr;->eOo:[Ljzw;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljzr;->eOo:[Ljzw;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 3801
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljzr;->eOo:[Ljzw;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3802
    iget-object v2, p0, Ljzr;->eOo:[Ljzw;

    aget-object v2, v2, v0

    .line 3803
    if-eqz v2, :cond_1

    .line 3804
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3801
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 3809
    :cond_3
    return v0
.end method
