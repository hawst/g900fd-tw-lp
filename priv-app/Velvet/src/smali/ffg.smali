.class public final Lffg;
.super Lfeu;
.source "PG"


# instance fields
.field private final cqd:Ljbv;


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 22
    iget-object v0, p1, Lizj;->dTl:Ljbv;

    iput-object v0, p0, Lffg;->cqd:Ljbv;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lffg;->cqd:Ljbv;

    invoke-virtual {v0}, Ljbv;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 62
    new-instance v0, Lffz;

    const v1, 0x7f0a04a1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lffg;->cqd:Ljbv;

    invoke-virtual {v4}, Ljbv;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lffg;->cqd:Ljbv;

    invoke-virtual {v3}, Ljbv;->agN()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x2

    iget-object v4, p0, Lffg;->cqd:Ljbv;

    invoke-virtual {v4}, Ljbv;->baQ()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lffg;->cqd:Ljbv;

    iget-object v1, v1, Ljbv;->dZr:Ljbp;

    invoke-virtual {v0, v1, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 34
    const v0, 0x7f0a0363

    new-array v1, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lffg;->cqd:Ljbv;

    invoke-virtual {v3}, Ljbv;->agN()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string v0, "event"

    return-object v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 41
    const v0, 0x7f0200f4

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lfgb;->cqC:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lffg;->cqd:Ljbv;

    invoke-virtual {v0}, Ljbv;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
