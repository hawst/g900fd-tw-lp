.class final Lihn;
.super Lihj;
.source "PG"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field private synthetic dCh:Lihb;


# direct methods
.method constructor <init>(Lihb;Ljava/util/SortedMap;)V
    .locals 0

    .prologue
    .line 951
    iput-object p1, p0, Lihn;->dCh:Lihb;

    .line 952
    invoke-direct {p0, p1, p2}, Lihj;-><init>(Lihb;Ljava/util/Map;)V

    .line 953
    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lihn;->dCq:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lihn;->dCq:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 971
    new-instance v1, Lihn;

    iget-object v2, p0, Lihn;->dCh:Lihb;

    iget-object v0, p0, Lihn;->dCq:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lihn;-><init>(Lihb;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 976
    iget-object v0, p0, Lihn;->dCq:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 981
    new-instance v1, Lihn;

    iget-object v2, p0, Lihn;->dCh:Lihb;

    iget-object v0, p0, Lihn;->dCq:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lihn;-><init>(Lihb;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 986
    new-instance v1, Lihn;

    iget-object v2, p0, Lihn;->dCh:Lihb;

    iget-object v0, p0, Lihn;->dCq:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lihn;-><init>(Lihb;Ljava/util/SortedMap;)V

    return-object v1
.end method
