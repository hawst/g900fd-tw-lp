.class public abstract Lijj;
.super Lijd;
.source "PG"

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Lijd;-><init>()V

    return-void
.end method

.method public static E(Ljava/util/Collection;)Lijj;
    .locals 2

    .prologue
    .line 242
    instance-of v0, p0, Lijd;

    if-eqz v0, :cond_1

    .line 244
    check-cast p0, Lijd;

    invoke-virtual {p0}, Lijd;->aWU()Lijj;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Lijj;->aWH()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lijj;->F(Ljava/util/Collection;)Lijj;

    move-result-object v0

    .line 247
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lijj;->F(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method private static F(Ljava/util/Collection;)Lijj;
    .locals 3

    .prologue
    .line 278
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 279
    array-length v0, v1

    packed-switch v0, :pswitch_data_0

    .line 289
    invoke-static {v1}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    :goto_0
    return-object v0

    .line 281
    :pswitch_0
    sget-object v0, Liil;->dCO:Liil;

    goto :goto_0

    .line 284
    :pswitch_1
    new-instance v0, Liqy;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Liqy;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 98
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 125
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 155
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public static aWW()Lijj;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Liil;->dCO:Liil;

    return-object v0
.end method

.method public static aWX()Lijk;
    .locals 1

    .prologue
    .line 535
    new-instance v0, Lijk;

    invoke-direct {v0}, Lijk;-><init>()V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 116
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public static bs(Ljava/lang/Object;)Lijj;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Liqy;

    invoke-direct {v0, p0}, Liqy;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static d([Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 266
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 272
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    :goto_0
    return-object v0

    .line 268
    :pswitch_0
    sget-object v0, Liil;->dCO:Liil;

    goto :goto_0

    .line 270
    :pswitch_1
    new-instance v0, Liqy;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Liqy;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static varargs e([Ljava/lang/Object;)Lijj;
    .locals 4

    .prologue
    .line 295
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 296
    aget-object v1, p0, v0

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "at index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 295
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    :cond_1
    new-instance v0, Liqc;

    invoke-direct {v0, p0}, Liqc;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method public static q(Ljava/lang/Iterable;)Lijj;
    .locals 1

    .prologue
    .line 216
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-static {p0}, Liia;->o(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lijj;->F(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method public static r(Ljava/lang/Object;Ljava/lang/Object;)Lijj;
    .locals 2

    .prologue
    .line 89
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lijj;->e([Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 523
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public aWI()Lirv;
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0}, Lijj;->aWJ()Lirw;

    move-result-object v0

    return-object v0
.end method

.method public aWJ()Lirw;
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lijj;->ma(I)Lirw;

    move-result-object v0

    return-object v0
.end method

.method public final aWU()Lijj;
    .locals 0

    .prologue
    .line 389
    return-object p0
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 370
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 1

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract bm(II)Lijj;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 499
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Ljava/util/List;

    if-eqz v1, :cond_2

    check-cast p1, Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v1, v2}, Likr;->a(Ljava/util/Iterator;Ljava/util/Iterator;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 503
    const/4 v0, 0x1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v3

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lijj;->aWI()Lirv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lijj;->aWJ()Lirw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lijj;->ma(I)Lirw;

    move-result-object v0

    return-object v0
.end method

.method public abstract ma(I)Lirw;
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 380
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Lijj;->bm(II)Lijj;

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 527
    new-instance v0, Lijl;

    invoke-virtual {p0}, Lijj;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lijl;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method
