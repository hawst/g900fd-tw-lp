.class public final Lepg;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "InlinedApi"
    }
.end annotation


# instance fields
.field public final aIf:Ljava/lang/String;

.field public final bMb:Ljava/lang/String;

.field public final biV:J

.field public final chb:J

.field public final chc:Ljava/lang/String;

.field private final chd:Z

.field private final che:I


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JIZ)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p3, p0, Lepg;->chc:Ljava/lang/String;

    .line 186
    iput-wide p6, p0, Lepg;->chb:J

    .line 187
    iput-object p4, p0, Lepg;->aIf:Ljava/lang/String;

    .line 188
    iput-object p5, p0, Lepg;->bMb:Ljava/lang/String;

    .line 189
    iput p8, p0, Lepg;->che:I

    .line 190
    iput-boolean p9, p0, Lepg;->chd:Z

    .line 191
    iput-wide p1, p0, Lepg;->biV:J

    .line 192
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 237
    instance-of v1, p1, Lepg;

    if-nez v1, :cond_1

    .line 241
    :cond_0
    :goto_0
    return v0

    .line 240
    :cond_1
    check-cast p1, Lepg;

    .line 241
    iget-boolean v1, p1, Lepg;->chd:Z

    iget-boolean v2, p0, Lepg;->chd:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lepg;->aIf:Ljava/lang/String;

    iget-object v2, p0, Lepg;->aIf:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lepg;->bMb:Ljava/lang/String;

    iget-object v2, p0, Lepg;->bMb:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Lepg;->che:I

    iget v2, p0, Lepg;->che:I

    if-ne v1, v2, :cond_0

    iget-wide v2, p1, Lepg;->chb:J

    iget-wide v4, p0, Lepg;->chb:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p1, Lepg;->chc:Ljava/lang/String;

    iget-object v2, p0, Lepg;->chc:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
