.class public final Ljnp;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiA:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private alT:Ljava/lang/String;

.field public euY:Ljno;

.field public euZ:Ljno;

.field private evp:Ljava/lang/String;

.field private evq:Z

.field public evr:[Ljnq;

.field private evs:Z

.field public evt:[Ljnr;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 834
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 835
    iput v1, p0, Ljnp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnp;->evp:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnp;->aiA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnp;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnp;->alT:Ljava/lang/String;

    iput-object v2, p0, Ljnp;->euY:Ljno;

    iput-object v2, p0, Ljnp;->euZ:Ljno;

    iput-boolean v1, p0, Ljnp;->evq:Z

    invoke-static {}, Ljnq;->bqR()[Ljnq;

    move-result-object v0

    iput-object v0, p0, Ljnp;->evr:[Ljnq;

    iput-boolean v1, p0, Ljnp;->evs:Z

    invoke-static {}, Ljnr;->bqS()[Ljnr;

    move-result-object v0

    iput-object v0, p0, Ljnp;->evt:[Ljnr;

    iput-object v2, p0, Ljnp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnp;->eCz:I

    .line 836
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnp;->evp:Ljava/lang/String;

    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnp;->aiA:Ljava/lang/String;

    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnp;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnp;->aiK:Ljava/lang/String;

    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnp;->alT:Ljava/lang/String;

    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnp;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljnp;->euY:Ljno;

    if-nez v0, :cond_1

    new-instance v0, Ljno;

    invoke-direct {v0}, Ljno;-><init>()V

    iput-object v0, p0, Ljnp;->euY:Ljno;

    :cond_1
    iget-object v0, p0, Ljnp;->euY:Ljno;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljnp;->euZ:Ljno;

    if-nez v0, :cond_2

    new-instance v0, Ljno;

    invoke-direct {v0}, Ljno;-><init>()V

    iput-object v0, p0, Ljnp;->euZ:Ljno;

    :cond_2
    iget-object v0, p0, Ljnp;->euZ:Ljno;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljnp;->evr:[Ljnq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljnq;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljnp;->evr:[Ljnq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljnq;

    invoke-direct {v3}, Ljnq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljnp;->evr:[Ljnq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljnq;

    invoke-direct {v3}, Ljnq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljnp;->evr:[Ljnq;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljnp;->evs:Z

    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljnp;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljnp;->evq:Z

    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljnp;->aez:I

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljnp;->evt:[Ljnr;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljnr;

    if-eqz v0, :cond_6

    iget-object v3, p0, Ljnp;->evt:[Ljnr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Ljnr;

    invoke-direct {v3}, Ljnr;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ljnp;->evt:[Ljnr;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Ljnr;

    invoke-direct {v3}, Ljnr;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljnp;->evt:[Ljnr;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 858
    iget v0, p0, Ljnp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 859
    const/4 v0, 0x1

    iget-object v2, p0, Ljnp;->evp:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 861
    :cond_0
    iget v0, p0, Ljnp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 862
    const/4 v0, 0x2

    iget-object v2, p0, Ljnp;->aiA:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 864
    :cond_1
    iget v0, p0, Ljnp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 865
    const/4 v0, 0x3

    iget-object v2, p0, Ljnp;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 867
    :cond_2
    iget v0, p0, Ljnp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 868
    const/4 v0, 0x4

    iget-object v2, p0, Ljnp;->alT:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 870
    :cond_3
    iget-object v0, p0, Ljnp;->euY:Ljno;

    if-eqz v0, :cond_4

    .line 871
    const/4 v0, 0x5

    iget-object v2, p0, Ljnp;->euY:Ljno;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 873
    :cond_4
    iget-object v0, p0, Ljnp;->euZ:Ljno;

    if-eqz v0, :cond_5

    .line 874
    const/4 v0, 0x6

    iget-object v2, p0, Ljnp;->euZ:Ljno;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 876
    :cond_5
    iget-object v0, p0, Ljnp;->evr:[Ljnq;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljnp;->evr:[Ljnq;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 877
    :goto_0
    iget-object v2, p0, Ljnp;->evr:[Ljnq;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 878
    iget-object v2, p0, Ljnp;->evr:[Ljnq;

    aget-object v2, v2, v0

    .line 879
    if-eqz v2, :cond_6

    .line 880
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 877
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 884
    :cond_7
    iget v0, p0, Ljnp;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 885
    const/16 v0, 0x8

    iget-boolean v2, p0, Ljnp;->evs:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 887
    :cond_8
    iget v0, p0, Ljnp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 888
    const/16 v0, 0x9

    iget-boolean v2, p0, Ljnp;->evq:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 890
    :cond_9
    iget-object v0, p0, Ljnp;->evt:[Ljnr;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljnp;->evt:[Ljnr;

    array-length v0, v0

    if-lez v0, :cond_b

    .line 891
    :goto_1
    iget-object v0, p0, Ljnp;->evt:[Ljnr;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 892
    iget-object v0, p0, Ljnp;->evt:[Ljnr;

    aget-object v0, v0, v1

    .line 893
    if-eqz v0, :cond_a

    .line 894
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 891
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 898
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 899
    return-void
.end method

.method public final getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Ljnp;->alT:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 903
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 904
    iget v2, p0, Ljnp;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 905
    const/4 v2, 0x1

    iget-object v3, p0, Ljnp;->evp:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 908
    :cond_0
    iget v2, p0, Ljnp;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 909
    const/4 v2, 0x2

    iget-object v3, p0, Ljnp;->aiA:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 912
    :cond_1
    iget v2, p0, Ljnp;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 913
    const/4 v2, 0x3

    iget-object v3, p0, Ljnp;->aiK:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 916
    :cond_2
    iget v2, p0, Ljnp;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 917
    const/4 v2, 0x4

    iget-object v3, p0, Ljnp;->alT:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 920
    :cond_3
    iget-object v2, p0, Ljnp;->euY:Ljno;

    if-eqz v2, :cond_4

    .line 921
    const/4 v2, 0x5

    iget-object v3, p0, Ljnp;->euY:Ljno;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 924
    :cond_4
    iget-object v2, p0, Ljnp;->euZ:Ljno;

    if-eqz v2, :cond_5

    .line 925
    const/4 v2, 0x6

    iget-object v3, p0, Ljnp;->euZ:Ljno;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 928
    :cond_5
    iget-object v2, p0, Ljnp;->evr:[Ljnq;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljnp;->evr:[Ljnq;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 929
    :goto_0
    iget-object v3, p0, Ljnp;->evr:[Ljnq;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 930
    iget-object v3, p0, Ljnp;->evr:[Ljnq;

    aget-object v3, v3, v0

    .line 931
    if-eqz v3, :cond_6

    .line 932
    const/4 v4, 0x7

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 929
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    move v0, v2

    .line 937
    :cond_8
    iget v2, p0, Ljnp;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_9

    .line 938
    const/16 v2, 0x8

    iget-boolean v3, p0, Ljnp;->evs:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 941
    :cond_9
    iget v2, p0, Ljnp;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_a

    .line 942
    const/16 v2, 0x9

    iget-boolean v3, p0, Ljnp;->evq:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 945
    :cond_a
    iget-object v2, p0, Ljnp;->evt:[Ljnr;

    if-eqz v2, :cond_c

    iget-object v2, p0, Ljnp;->evt:[Ljnr;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 946
    :goto_1
    iget-object v2, p0, Ljnp;->evt:[Ljnr;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 947
    iget-object v2, p0, Ljnp;->evt:[Ljnr;

    aget-object v2, v2, v1

    .line 948
    if-eqz v2, :cond_b

    .line 949
    const/16 v3, 0xa

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 946
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 954
    :cond_c
    return v0
.end method

.method public final pO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Ljnp;->aiA:Ljava/lang/String;

    return-object v0
.end method

.method public final xq(Ljava/lang/String;)Ljnp;
    .locals 1

    .prologue
    .line 724
    if-nez p1, :cond_0

    .line 725
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 727
    :cond_0
    iput-object p1, p0, Ljnp;->aiA:Ljava/lang/String;

    .line 728
    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnp;->aez:I

    .line 729
    return-object p0
.end method

.method public final xr(Ljava/lang/String;)Ljnp;
    .locals 1

    .prologue
    .line 768
    if-nez p1, :cond_0

    .line 769
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 771
    :cond_0
    iput-object p1, p0, Ljnp;->alT:Ljava/lang/String;

    .line 772
    iget v0, p0, Ljnp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnp;->aez:I

    .line 773
    return-object p0
.end method
