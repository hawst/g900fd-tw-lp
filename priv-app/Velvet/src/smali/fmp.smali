.class final Lfmp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic cwj:Lfml;


# direct methods
.method constructor <init>(Lfml;)V
    .locals 0

    .prologue
    .line 1154
    iput-object p1, p0, Lfmp;->cwj:Lfml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    .line 1158
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v1, v0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1160
    :try_start_0
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    invoke-static {p2}, Lfos;->I(Landroid/os/IBinder;)Lfor;

    move-result-object v2

    iput-object v2, v0, Lfml;->cwe:Lfor;

    .line 1163
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cvY:Lfnh;

    invoke-virtual {v0}, Lfnh;->uK()V

    .line 1164
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cvZ:Lfnh;

    invoke-virtual {v0}, Lfnh;->uK()V

    .line 1165
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwa:Lgbo;

    invoke-virtual {v0}, Lgbo;->uK()V

    .line 1168
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v2, v0, Lfml;->cwd:Ljava/util/List;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1171
    :try_start_1
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwd:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1172
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwd:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    .line 1173
    iget-object v3, p0, Lfmp;->cwj:Lfml;

    iget-object v3, v3, Lfml;->cwd:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1174
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmr;

    .line 1179
    iget-object v4, p0, Lfmp;->cwj:Lfml;

    iget v5, v0, Lfmr;->cwm:I

    iget-object v0, v0, Lfmr;->ct:Landroid/os/Bundle;

    invoke-virtual {v4, v5, v0}, Lfml;->f(ILandroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1182
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1189
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1182
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1184
    :try_start_4
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwf:Lfmp;

    if-eqz v0, :cond_1

    .line 1185
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cvV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesk;

    .line 1186
    iget-object v3, p0, Lfmp;->cwj:Lfml;

    iget-object v3, v3, Lfml;->mTaskRunner:Lerk;

    invoke-interface {v3, v0}, Lerk;->a(Lesk;)V

    goto :goto_1

    .line 1189
    :cond_1
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 1194
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    iget-object v1, v0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1196
    :try_start_0
    iget-object v0, p0, Lfmp;->cwj:Lfml;

    const/4 v2, 0x0

    iput-object v2, v0, Lfml;->cwe:Lfor;

    .line 1197
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
