.class public final Lbzk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzh;


# instance fields
.field private final aPp:Ljava/net/HttpURLConnection;

.field private aPq:[B

.field private aPr:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/net/HttpURLConnection;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    .line 36
    return-void
.end method


# virtual methods
.method public final a([BIIZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getDoOutput()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 107
    iget-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 108
    iget-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, v1, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 109
    if-nez p4, :cond_0

    if-eqz p5, :cond_1

    .line 110
    :cond_0
    iget-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 112
    :cond_1
    if-eqz p5, :cond_2

    .line 113
    iget-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    .line 116
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 107
    goto :goto_0
.end method

.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public final connect()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 64
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getDoOutput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lbzk;->aPq:[B

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 69
    :try_start_0
    iget-object v0, p0, Lbzk;->aPq:[B

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 71
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v0

    .line 73
    :cond_1
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    goto :goto_0
.end method

.method public final disconnect()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 57
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 58
    return-void
.end method

.method public final es(I)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 156
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 157
    return-void
.end method

.method public final getContentLength()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v0

    return v0
.end method

.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final getResponseCode()I
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lbzk;->aPr:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Chunked upload not finished."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 99
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    return v0

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getURL()Ljava/net/URL;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public final i([B)V
    .locals 2

    .prologue
    .line 147
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lbzk;->aPq:[B

    .line 148
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 149
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    array-length v1, p1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 150
    return-void
.end method

.method public final setConnectTimeout(I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 46
    return-void
.end method

.method public final setInstanceFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 185
    return-void
.end method

.method public final setReadTimeout(I)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 41
    return-void
.end method

.method public final setRequestMethod(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public final setUseCaches(Z)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 137
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 167
    const-string v0, "JavaNetConnection(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbzk;->aPp:Ljava/net/HttpURLConnection;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
