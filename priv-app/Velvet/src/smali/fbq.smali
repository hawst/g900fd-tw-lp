.class public Lfbq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLocationManager:Lfdq;

.field private final mLocationSettings:Lcob;

.field private final mLoginHelper:Lcrh;

.field private final mMainPreferencesSupplier:Ligi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ligi;Lfdq;Lcob;Lcrh;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lfbq;->mContext:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lfbq;->mMainPreferencesSupplier:Ligi;

    .line 70
    iput-object p3, p0, Lfbq;->mLocationManager:Lfdq;

    .line 71
    iput-object p4, p0, Lfbq;->mLocationSettings:Lcob;

    .line 72
    iput-object p5, p0, Lfbq;->mLoginHelper:Lcrh;

    .line 73
    return-void
.end method

.method private getMode()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 173
    iget-object v1, p0, Lfbq;->mLocationSettings:Lcob;

    invoke-interface {v1}, Lcob;->QR()I

    move-result v1

    .line 174
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 175
    packed-switch v1, :pswitch_data_0

    .line 192
    :cond_0
    iget-object v0, p0, Lfbq;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QO()Z

    move-result v0

    if-nez v0, :cond_2

    .line 193
    const/4 v0, 0x2

    .line 196
    :goto_0
    :pswitch_0
    return v0

    .line 178
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 183
    :cond_1
    iget-object v1, p0, Lfbq;->mLocationManager:Lfdq;

    const-string v2, "network"

    invoke-interface {v1, v2}, Lfdq;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 196
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final axm()Ljas;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    invoke-direct {p0}, Lfbq;->getMode()I

    move-result v3

    .line 84
    iget-object v0, p0, Lfbq;->mMainPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    const-string v4, "location_disabled_card_mode"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "location_disabled_card_mode"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 87
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 88
    const/4 v0, 0x0

    .line 147
    :goto_1
    return-object v0

    .line 84
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "location_disabled_card_mode"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 93
    :cond_2
    if-eq v3, v6, :cond_3

    if-ne v3, v1, :cond_6

    .line 97
    :cond_3
    new-instance v4, Lixx;

    invoke-direct {v4}, Lixx;-><init>()V

    .line 98
    const-string v0, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-virtual {v4, v0}, Lixx;->rd(Ljava/lang/String;)Lixx;

    .line 99
    iget-object v0, p0, Lfbq;->mContext:Landroid/content/Context;

    const v5, 0x7f0a082b

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lixx;->rb(Ljava/lang/String;)Lixx;

    .line 101
    if-ne v3, v6, :cond_4

    .line 103
    iget-object v0, p0, Lfbq;->mContext:Landroid/content/Context;

    const v3, 0x7f0a032c

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 104
    const-string v0, "LocationDisabled"

    .line 142
    :goto_2
    new-instance v5, Ljas;

    invoke-direct {v5}, Ljas;-><init>()V

    .line 143
    invoke-virtual {v5, v3}, Ljas;->sj(Ljava/lang/String;)Ljas;

    move-result-object v3

    iget-object v6, p0, Lfbq;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0328

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljas;->si(Ljava/lang/String;)Ljas;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljas;->sh(Ljava/lang/String;)Ljas;

    .line 146
    new-array v0, v1, [Lixx;

    aput-object v4, v0, v2

    iput-object v0, v5, Ljas;->dXg:[Lixx;

    move-object v0, v5

    .line 147
    goto :goto_1

    .line 109
    :cond_4
    iget-object v3, p0, Lfbq;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lfbq;->axo()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0a032b

    :goto_3
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 112
    const-string v0, "NlpDisabled"

    goto :goto_2

    .line 109
    :cond_5
    const v0, 0x7f0a032a

    goto :goto_3

    .line 116
    :cond_6
    const/4 v0, 0x2

    if-ne v3, v0, :cond_7

    move v0, v1

    :goto_4
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 120
    invoke-virtual {p0}, Lfbq;->axo()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 125
    iget-object v0, p0, Lfbq;->mLocationSettings:Lcob;

    iget-object v3, p0, Lfbq;->mLoginHelper:Lcrh;

    invoke-virtual {v3}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcob;->hv(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lgbm;->M(Landroid/content/Intent;)Lixx;

    move-result-object v0

    .line 137
    :goto_5
    iget-object v3, p0, Lfbq;->mContext:Landroid/content/Context;

    const v4, 0x7f0a03cf

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lixx;->rb(Ljava/lang/String;)Lixx;

    .line 138
    iget-object v3, p0, Lfbq;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0329

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 139
    const-string v3, "GlsDisabled"

    move-object v8, v3

    move-object v3, v4

    move-object v4, v0

    move-object v0, v8

    goto :goto_2

    :cond_7
    move v0, v2

    .line 116
    goto :goto_4

    .line 129
    :cond_8
    iget-object v0, p0, Lfbq;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v0

    .line 130
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 131
    if-eqz v0, :cond_9

    .line 132
    const-string v4, "account"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    :cond_9
    const-string v0, "disable"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 135
    invoke-static {v3}, Lgbm;->M(Landroid/content/Intent;)Lixx;

    move-result-object v0

    goto :goto_5
.end method

.method public final axn()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 155
    iget-object v0, p0, Lfbq;->mMainPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    const-string v2, "location_disabled_card_mode"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0}, Lfbq;->getMode()I

    move-result v2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected axo()Z
    .locals 2

    .prologue
    .line 163
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
