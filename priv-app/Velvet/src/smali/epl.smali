.class public final Lepl;
.super Ljava/util/concurrent/FutureTask;
.source "PG"

# interfaces
.implements Lerj;
.implements Livq;


# instance fields
.field private final chh:Livd;

.field private final chi:Lerj;


# direct methods
.method private constructor <init>(Lerh;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 29
    new-instance v0, Livd;

    invoke-direct {v0}, Livd;-><init>()V

    iput-object v0, p0, Lepl;->chh:Livd;

    .line 50
    invoke-static {p1}, Lepl;->aN(Ljava/lang/Object;)V

    .line 51
    iput-object p1, p0, Lepl;->chi:Lerj;

    .line 52
    return-void
.end method

.method private constructor <init>(Leri;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 29
    new-instance v0, Livd;

    invoke-direct {v0}, Livd;-><init>()V

    iput-object v0, p0, Lepl;->chh:Livd;

    .line 56
    invoke-static {p1}, Lepl;->aN(Ljava/lang/Object;)V

    .line 57
    iput-object p1, p0, Lepl;->chi:Lerj;

    .line 58
    return-void
.end method

.method public static a(Lerh;)Lepl;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lepl;

    invoke-direct {v0, p0}, Lepl;-><init>(Lerh;)V

    return-object v0
.end method

.method public static a(Leri;Ljava/lang/Object;)Lepl;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    new-instance v0, Lepl;

    invoke-direct {v0, p0, p1}, Lepl;-><init>(Leri;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static final aN(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 83
    instance-of v0, p0, Lerj;

    if-nez v0, :cond_0

    .line 84
    const-string v0, "NamedListenableFuture"

    const-string v1, "Task %s should be an instance of TaggedTask"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 86
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lepl;->chh:Livd;

    invoke-virtual {v0, p1, p2}, Livd;->b(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 70
    return-void
.end method

.method protected final done()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lepl;->chh:Livd;

    invoke-virtual {v0}, Livd;->execute()V

    .line 75
    return-void
.end method

.method public final getPermissions()I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lepl;->chi:Lerj;

    invoke-interface {v0}, Lerj;->getPermissions()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lepl;->chi:Lerj;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
