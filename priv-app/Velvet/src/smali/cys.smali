.class public final Lcys;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private bnq:Landroid/preference/SwitchPreference;

.field private final mActivity:Landroid/app/Activity;

.field private final mConfig:Lcjs;

.field private final mSearchUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcjs;Lcke;Lcpn;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p2}, Lcyc;-><init>(Lcke;)V

    .line 50
    iput-object p1, p0, Lcys;->mConfig:Lcjs;

    .line 51
    iput-object p3, p0, Lcys;->mSearchUrlHelper:Lcpn;

    .line 52
    iput-object p4, p0, Lcys;->mActivity:Landroid/app/Activity;

    .line 53
    invoke-interface {p2, p0}, Lcke;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 54
    return-void
.end method

.method private cR(Z)Ljava/lang/String;
    .locals 5

    .prologue
    .line 86
    if-eqz p1, :cond_0

    const v0, 0x7f0a05c1

    .line 88
    :goto_0
    iget-object v1, p0, Lcys;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcys;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v4}, Lcpn;->Rr()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lemg;->le(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 86
    :cond_0
    const v0, 0x7f0a05c0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 65
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    .line 66
    iget-object v0, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    invoke-virtual {v1}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v1

    invoke-direct {p0, v1}, Lcys;->cR(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 70
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcys;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0}, Lcpn;->Rz()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcys;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lb()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lcyc;->onDestroy()V

    .line 95
    invoke-virtual {p0}, Lcys;->TL()Lcke;

    move-result-object v0

    invoke-interface {v0, p0}, Lcke;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 96
    return-void
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcys;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 78
    const-string v0, "search_domain_apply_time"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcys;->bnq:Landroid/preference/SwitchPreference;

    invoke-virtual {v1}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v1

    invoke-direct {p0, v1}, Lcys;->cR(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 82
    :cond_0
    return-void
.end method
