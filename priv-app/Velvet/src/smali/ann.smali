.class public final Lann;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public aie:Lank;

.field private aif:F

.field private aig:Ljava/lang/String;

.field private aih:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8078
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8079
    const/4 v0, 0x0

    iput v0, p0, Lann;->aez:I

    iput-object v1, p0, Lann;->aie:Lank;

    const/4 v0, 0x0

    iput v0, p0, Lann;->aif:F

    const-string v0, ""

    iput-object v0, p0, Lann;->aig:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lann;->aih:Ljava/lang/String;

    iput-object v1, p0, Lann;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lann;->eCz:I

    .line 8080
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 7993
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lann;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lann;->aie:Lank;

    if-nez v0, :cond_1

    new-instance v0, Lank;

    invoke-direct {v0}, Lank;-><init>()V

    iput-object v0, p0, Lann;->aie:Lank;

    :cond_1
    iget-object v0, p0, Lann;->aie:Lank;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lann;->aif:F

    iget v0, p0, Lann;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lann;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lann;->aig:Ljava/lang/String;

    iget v0, p0, Lann;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lann;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lann;->aih:Ljava/lang/String;

    iget v0, p0, Lann;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lann;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 8096
    iget-object v0, p0, Lann;->aie:Lank;

    if-eqz v0, :cond_0

    .line 8097
    const/4 v0, 0x1

    iget-object v1, p0, Lann;->aie:Lank;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8099
    :cond_0
    iget v0, p0, Lann;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 8100
    const/4 v0, 0x2

    iget v1, p0, Lann;->aif:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 8102
    :cond_1
    iget v0, p0, Lann;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 8103
    const/4 v0, 0x3

    iget-object v1, p0, Lann;->aig:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8105
    :cond_2
    iget v0, p0, Lann;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 8106
    const/4 v0, 0x4

    iget-object v1, p0, Lann;->aih:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8108
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8109
    return-void
.end method

.method public final aD(Ljava/lang/String;)Lann;
    .locals 1

    .prologue
    .line 8040
    if-nez p1, :cond_0

    .line 8041
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8043
    :cond_0
    iput-object p1, p0, Lann;->aig:Ljava/lang/String;

    .line 8044
    iget v0, p0, Lann;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lann;->aez:I

    .line 8045
    return-object p0
.end method

.method public final aE(Ljava/lang/String;)Lann;
    .locals 1

    .prologue
    .line 8062
    if-nez p1, :cond_0

    .line 8063
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8065
    :cond_0
    iput-object p1, p0, Lann;->aih:Ljava/lang/String;

    .line 8066
    iget v0, p0, Lann;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lann;->aez:I

    .line 8067
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 8113
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8114
    iget-object v1, p0, Lann;->aie:Lank;

    if-eqz v1, :cond_0

    .line 8115
    const/4 v1, 0x1

    iget-object v2, p0, Lann;->aie:Lank;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8118
    :cond_0
    iget v1, p0, Lann;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 8119
    const/4 v1, 0x2

    iget v2, p0, Lann;->aif:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 8122
    :cond_1
    iget v1, p0, Lann;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 8123
    const/4 v1, 0x3

    iget-object v2, p0, Lann;->aig:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8126
    :cond_2
    iget v1, p0, Lann;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 8127
    const/4 v1, 0x4

    iget-object v2, p0, Lann;->aih:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8130
    :cond_3
    return v0
.end method

.method public final pv()F
    .locals 1

    .prologue
    .line 8018
    iget v0, p0, Lann;->aif:F

    return v0
.end method

.method public final pw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8037
    iget-object v0, p0, Lann;->aig:Ljava/lang/String;

    return-object v0
.end method

.method public final px()Z
    .locals 1

    .prologue
    .line 8048
    iget v0, p0, Lann;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final py()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8059
    iget-object v0, p0, Lann;->aih:Ljava/lang/String;

    return-object v0
.end method

.method public final pz()Z
    .locals 1

    .prologue
    .line 8070
    iget v0, p0, Lann;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x(F)Lann;
    .locals 1

    .prologue
    .line 8021
    iput p1, p0, Lann;->aif:F

    .line 8022
    iget v0, p0, Lann;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lann;->aez:I

    .line 8023
    return-object p0
.end method
