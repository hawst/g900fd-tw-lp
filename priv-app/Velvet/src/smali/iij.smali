.class final Liij;
.super Liqa;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private dCN:Lijj;


# direct methods
.method constructor <init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Liqa;-><init>()V

    .line 32
    invoke-static {p1, p2}, Lijj;->r(Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    iput-object v0, p0, Liij;->dCN:Lijj;

    .line 34
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Liij;->dCN:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 48
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 49
    if-eqz v0, :cond_0

    .line 53
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 57
    if-ne p1, p0, :cond_0

    .line 58
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    .line 60
    :cond_0
    instance-of v0, p1, Liij;

    if-eqz v0, :cond_1

    .line 61
    check-cast p1, Liij;

    .line 62
    iget-object v0, p0, Liij;->dCN:Lijj;

    iget-object v1, p1, Liij;->dCN:Lijj;

    invoke-virtual {v0, v1}, Lijj;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 64
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Liij;->dCN:Lijj;

    invoke-virtual {v0}, Lijj;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ordering.compound("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Liij;->dCN:Lijj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
