.class public final Lhah;
.super Lgxi;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field private UG:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentView;)V
    .locals 1

    .prologue
    .line 26
    const-string v0, "voicesearchlang"

    invoke-direct {p0, v0, p1}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 27
    return-void
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x10

    return v0
.end method

.method protected final X(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    iget-object v0, v0, Lgyz;->mVelvetFactory:Lgpu;

    invoke-virtual {p0}, Lhah;->aLt()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {p0, v0}, Lgpu;->a(Lhah;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 32
    const v0, 0x7f110499

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhah;->UG:Landroid/widget/TextView;

    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lhah;->a([Landroid/view/View;)V

    .line 36
    invoke-virtual {p0, p0}, Lhah;->a(Lddc;)V

    .line 37
    return-void
.end method

.method public final a(Lddb;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 53
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aas()Ldbj;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Ldbj;->Wn()Z

    move-result v1

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lhym;->aTH()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lhah;->UG:Landroid/widget/TextView;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lhah;->UG:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    :goto_0
    invoke-virtual {v0}, Ldbj;->Wl()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p0, v4}, Lhah;->fX(Z)V

    .line 58
    :cond_0
    return-void

    .line 54
    :cond_1
    iget-object v1, p0, Lhah;->UG:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0, p1}, Lgxi;->a(Letj;)V

    .line 75
    const-string v0, "VoicesearchLanguagePresenter"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method protected final aKF()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhah;->fX(Z)V

    .line 68
    sget-object v0, Lgxi;->cYu:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 69
    invoke-virtual {p0, p0}, Lhah;->b(Lddc;)V

    .line 70
    return-void
.end method
