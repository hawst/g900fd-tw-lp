.class public final Ldmr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bCz:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/16 v0, 0x200

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Ldmr;->bCz:[Ljava/lang/Object;

    return-void
.end method

.method private static a(Ljava/lang/String;[BII)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, p3, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 38
    :goto_1
    if-ge v1, p3, :cond_2

    .line 39
    add-int v2, p2, v1

    aget-byte v2, p1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_0

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 43
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static j([BII)[B
    .locals 2

    .prologue
    .line 117
    new-array v0, p2, [B

    .line 118
    if-eqz p2, :cond_0

    .line 119
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    :cond_0
    return-object v0
.end method


# virtual methods
.method h([BII)I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x0

    move v1, v0

    move v0, p2

    .line 61
    :goto_0
    add-int v2, p2, p3

    if-ge v0, v2, :cond_0

    .line 62
    mul-int/lit8 v1, v1, 0x1f

    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v1, v2

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_0
    ushr-int/lit8 v0, v1, 0x14

    ushr-int/lit8 v2, v1, 0xc

    xor-int/2addr v0, v2

    xor-int/2addr v0, v1

    .line 67
    ushr-int/lit8 v1, v0, 0x7

    ushr-int/lit8 v2, v0, 0x4

    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 68
    iget-object v1, p0, Ldmr;->bCz:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    return v0
.end method

.method public final i([BII)Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    const/16 v0, 0x200

    if-le p3, v0, :cond_1

    .line 77
    new-instance v1, Ljava/lang/String;

    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, p2, p3, v0}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 92
    :cond_0
    :goto_0
    return-object v1

    .line 80
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Ldmr;->h([BII)I

    move-result v2

    .line 82
    iget-object v0, p0, Ldmr;->bCz:[Ljava/lang/Object;

    aget-object v1, v0, v2

    .line 83
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p1, p2, p3}, Ldmr;->a(Ljava/lang/String;[BII)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    check-cast v1, Ljava/lang/String;

    goto :goto_0

    .line 87
    :cond_2
    new-instance v1, Ljava/lang/String;

    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, p2, p3, v0}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 89
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, p3, :cond_0

    .line 90
    iget-object v0, p0, Ldmr;->bCz:[Ljava/lang/Object;

    aput-object v1, v0, v2

    goto :goto_0
.end method
