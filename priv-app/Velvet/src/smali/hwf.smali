.class public final enum Lhwf;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum dsR:Lhwf;

.field public static final enum dsS:Lhwf;

.field public static final enum dsT:Lhwf;

.field public static final enum dsU:Lhwf;

.field public static final enum dsV:Lhwf;

.field public static final enum dsW:Lhwf;

.field private static final synthetic dsX:[Lhwf;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 74
    new-instance v0, Lhwf;

    const-string v1, "AUDIO_NOT_INITIALIZED"

    invoke-direct {v0, v1, v3}, Lhwf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhwf;->dsR:Lhwf;

    .line 75
    new-instance v0, Lhwf;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lhwf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhwf;->dsS:Lhwf;

    .line 76
    new-instance v0, Lhwf;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v5}, Lhwf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhwf;->dsT:Lhwf;

    .line 77
    new-instance v0, Lhwf;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v6}, Lhwf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhwf;->dsU:Lhwf;

    .line 78
    new-instance v0, Lhwf;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v7}, Lhwf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhwf;->dsV:Lhwf;

    .line 79
    new-instance v0, Lhwf;

    const-string v1, "WORKING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhwf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhwf;->dsW:Lhwf;

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Lhwf;

    sget-object v1, Lhwf;->dsR:Lhwf;

    aput-object v1, v0, v3

    sget-object v1, Lhwf;->dsS:Lhwf;

    aput-object v1, v0, v4

    sget-object v1, Lhwf;->dsT:Lhwf;

    aput-object v1, v0, v5

    sget-object v1, Lhwf;->dsU:Lhwf;

    aput-object v1, v0, v6

    sget-object v1, Lhwf;->dsV:Lhwf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhwf;->dsW:Lhwf;

    aput-object v2, v0, v1

    sput-object v0, Lhwf;->dsX:[Lhwf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhwf;
    .locals 1

    .prologue
    .line 73
    const-class v0, Lhwf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhwf;

    return-object v0
.end method

.method public static values()[Lhwf;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lhwf;->dsX:[Lhwf;

    invoke-virtual {v0}, [Lhwf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhwf;

    return-object v0
.end method
