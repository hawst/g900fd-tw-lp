.class public final Lhla;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mEntryProvider:Lfaq;

.field public final mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfcx;Lfaq;Lemp;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lhla;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lhla;->mNetworkClient:Lfcx;

    .line 35
    iput-object p3, p0, Lhla;->mEntryProvider:Lfaq;

    .line 36
    iput-object p4, p0, Lhla;->mClock:Lemp;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lhqq;Lefk;)V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lhqp;

    iget-object v1, p0, Lhla;->mNetworkClient:Lfcx;

    invoke-direct {v0, p2, p1, v1}, Lhqp;-><init>(Lefk;Lhqq;Lfcx;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhqp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 50
    return-void
.end method

.method public final a(Lizj;Ljpd;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 56
    new-instance v4, Ljbp;

    invoke-direct {v4}, Ljbp;-><init>()V

    .line 57
    invoke-virtual {p2}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    invoke-virtual {v4, v0}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    .line 62
    :cond_0
    invoke-static {p1}, Lgbf;->K(Lizj;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 64
    invoke-virtual {v4, v0}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    .line 68
    :cond_1
    new-instance v0, Leyc;

    iget-object v1, p0, Lhla;->mContext:Landroid/content/Context;

    const/16 v2, 0x11

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/16 v5, 0x12

    aput v5, v3, v9

    invoke-static {p1, v2, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v3

    iget-object v5, p0, Lhla;->mNetworkClient:Lfcx;

    iget-object v6, p0, Lhla;->mEntryProvider:Lfaq;

    const/4 v7, 0x0

    iget-object v8, p0, Lhla;->mClock:Lemp;

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Leyc;-><init>(Landroid/content/Context;Lizj;Liwk;Ljbp;Lfcx;Lfaq;Lefk;Lemp;)V

    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leyc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    return-void
.end method
