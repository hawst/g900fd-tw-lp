.class public final Lfzw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cEa:Z

.field final cEb:Z

.field private mIntentUtils:Leom;

.field private final mRunner:Lerp;


# direct methods
.method public constructor <init>(Lerk;ZZ)V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Leon;

    invoke-direct {v0}, Leon;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lfzw;-><init>(Lerp;ZZLeom;)V

    .line 38
    return-void
.end method

.method private constructor <init>(Lerp;ZZLeom;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lfzw;->mRunner:Lerp;

    .line 43
    iput-boolean p2, p0, Lfzw;->cEa:Z

    .line 44
    iput-boolean p3, p0, Lfzw;->cEb:Z

    .line 45
    iput-object p4, p0, Lfzw;->mIntentUtils:Leom;

    .line 46
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/net/Uri;ZILjava/lang/String;)Z
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 110
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 111
    if-eqz v2, :cond_2

    const-string v3, "intent"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 113
    :try_start_0
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 125
    :goto_0
    if-eqz p3, :cond_0

    .line 126
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 129
    :cond_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 130
    invoke-virtual {v2, p5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    :cond_1
    :goto_1
    invoke-virtual {p0, p1, v2, p4}, Lfzw;->a(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    :goto_2
    return v0

    .line 114
    :catch_0
    move-exception v1

    .line 115
    const-string v2, "ActivityHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error parsing uri as intent "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 119
    :cond_2
    if-nez v2, :cond_3

    .line 120
    const-string v2, "ActivityHelper"

    const-string v3, "Scheme not specified for uri: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p2, v4, v0

    invoke-static {v2, v3, v4}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 122
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 131
    :cond_4
    invoke-virtual {p2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "maps.google.com"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v4, :cond_5

    const-string v3, "/maps/"

    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    :cond_5
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfzw;->cEa:Z

    if-eqz v0, :cond_1

    .line 132
    iget-boolean v0, p0, Lfzw;->cEb:Z

    invoke-static {p1, v2, v0}, Lgaz;->a(Landroid/content/Context;Landroid/content/Intent;Z)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private o(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lfzw;->mRunner:Lerp;

    new-instance v1, Lfzx;

    const-string v2, "Toast"

    invoke-direct {v1, p0, v2, p1, p2}, Lfzx;-><init>(Lfzw;Ljava/lang/String;Landroid/content/Context;I)V

    invoke-interface {v0, v1}, Lerp;->a(Lesk;)V

    .line 181
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 4

    .prologue
    .line 165
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    const-string v1, "ActivityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no handler for intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 168
    invoke-direct {p0, p1, p3}, Lfzw;->o(Landroid/content/Context;I)V

    .line 169
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Z)Z
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83
    const/4 v3, 0x0

    const v4, 0x7f0a05df

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lfzw;->a(Landroid/content/Context;Landroid/net/Uri;ZILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Z)Z
    .locals 1

    .prologue
    .line 71
    const v0, 0x7f0a05df

    invoke-virtual {p0, p1, p2, p3, v0}, Lfzw;->a(Landroid/content/Context;Landroid/net/Uri;ZI)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;ZI)Z
    .locals 6

    .prologue
    .line 95
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lfzw;->a(Landroid/content/Context;Landroid/net/Uri;ZILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 58
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lfzw;->a(Landroid/content/Context;Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 204
    const/4 v2, 0x1

    :try_start_0
    invoke-static {p2, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    .line 205
    iget-object v3, p0, Lfzw;->mIntentUtils:Leom;

    invoke-interface {v3, p1, v2}, Leom;->f(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 206
    const v1, 0x7f0a03d9

    invoke-direct {p0, p1, v1}, Lfzw;->o(Landroid/content/Context;I)V

    .line 208
    const-string v1, "ActivityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot handle broadcast "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_0
    return v0

    .line 212
    :cond_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :catch_0
    move-exception v1

    const-string v1, "ActivityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to parse intent for broadcast "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final h(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 150
    const v0, 0x7f0a0656

    invoke-virtual {p0, p1, p2, v0}, Lfzw;->a(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method
