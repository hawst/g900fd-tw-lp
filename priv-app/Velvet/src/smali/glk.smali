.class public final Lglk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cOL:Lgll;

.field private final cOM:Lgjl;

.field private final mContactRetriever:Lglg;

.field public final mGreco3DataManager:Lgix;


# direct methods
.method public constructor <init>(Lgix;Lglg;Lgll;Lgjl;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lglk;->mGreco3DataManager:Lgix;

    .line 38
    iput-object p2, p0, Lglk;->mContactRetriever:Lglg;

    .line 39
    iput-object p3, p0, Lglk;->cOL:Lgll;

    .line 40
    iput-object p4, p0, Lglk;->cOM:Lgjl;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lgkd;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-virtual {p1}, Lgkd;->aGC()Ljzp;

    move-result-object v1

    invoke-virtual {v1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v1

    .line 113
    const-string v2, "en-US"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-object v0

    .line 120
    :cond_1
    :try_start_0
    iget-object v2, p0, Lglk;->cOL:Lgll;

    iget-object v3, p0, Lglk;->cOM:Lgjl;

    invoke-virtual {v3, v1}, Lgjl;->mS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/high16 v3, 0x80000

    invoke-virtual {v2, v1, v3}, Lgll;->G(Ljava/lang/String;I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 127
    if-eqz v1, :cond_0

    .line 155
    iget-object v0, p0, Lglk;->cOM:Lgjl;

    invoke-virtual {v0}, Lgjl;->aGw()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Lglk;->mContactRetriever:Lglg;

    invoke-virtual {v0}, Lglg;->aHe()Ljava/util/List;

    move-result-object v0

    .line 161
    new-instance v2, Lgld;

    invoke-direct {v2}, Lgld;-><init>()V

    .line 162
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglf;

    .line 163
    invoke-virtual {v2, v0}, Lgld;->a(Lglf;)V

    goto :goto_1

    .line 122
    :catch_0
    move-exception v1

    .line 123
    const-string v2, "HandsFreeGrammarCompiler"

    const-string v3, "I/O Exception reading ABNF grammar: "

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 166
    :cond_2
    invoke-virtual {v2, v1}, Lgld;->e(Ljava/lang/StringBuilder;)V

    .line 169
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 68
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lglk;->mGreco3DataManager:Lgix;

    invoke-virtual {v1}, Lgix;->isInitialized()Z

    move-result v1

    invoke-static {v1}, Lifv;->gY(Z)V

    .line 70
    new-instance v1, Lerc;

    invoke-direct {v1}, Lerc;-><init>()V

    .line 71
    invoke-virtual {v1}, Lerc;->avy()Lerc;

    .line 72
    iget-object v2, p0, Lglk;->mGreco3DataManager:Lgix;

    invoke-virtual {v2, p2}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v2

    .line 73
    if-nez v2, :cond_1

    .line 74
    const-string v1, "HandsFreeGrammarCompiler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Grammar compilation failed, no resources for locale :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 78
    :cond_1
    if-eqz p1, :cond_0

    .line 82
    :try_start_1
    invoke-virtual {v1}, Lerc;->avy()Lerc;

    .line 83
    new-instance v3, Lgjm;

    sget-object v4, Lgjo;->cMO:Lgjo;

    invoke-virtual {v2, v4}, Lgkd;->d(Lgjo;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lgkd;->aGB()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lgjm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lgjm;->aGx()Z

    move-result v2

    if-nez v2, :cond_2

    .line 84
    :goto_1
    if-eqz v0, :cond_0

    .line 85
    const-string v2, "HandsFreeGrammarCompiler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Compiled grammar : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lerc;->avz()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ms"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 83
    :cond_2
    :try_start_2
    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, p1, v0, v2}, Lgjm;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v3}, Lgjm;->delete()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
