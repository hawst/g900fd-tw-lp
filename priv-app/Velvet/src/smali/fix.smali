.class public final Lfix;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfja;
.implements Ljava/lang/Runnable;


# instance fields
.field private cpQ:Ljava/util/concurrent/CountDownLatch;

.field private cq:Z

.field private final cts:Livq;

.field private volatile ctt:Lfja;

.field private final dK:Ljava/lang/Object;

.field private final mClock:Lemp;

.field private mObserver:Lfjb;


# direct methods
.method public constructor <init>(Livq;Lemp;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfix;->dK:Ljava/lang/Object;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfix;->cq:Z

    .line 37
    iput-object p1, p0, Lfix;->cts:Livq;

    .line 38
    iput-object p2, p0, Lfix;->mClock:Lemp;

    .line 39
    return-void
.end method

.method private d(JLjava/util/concurrent/TimeUnit;)Lfja;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    iget-object v1, p0, Lfix;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lfix;->ctt:Lfja;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lfix;->ctt:Lfja;

    monitor-exit v1

    .line 84
    :goto_0
    return-object v0

    .line 75
    :cond_0
    iget-object v0, p0, Lfix;->cpQ:Ljava/util/concurrent/CountDownLatch;

    .line 76
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    if-eqz v0, :cond_1

    .line 80
    :try_start_1
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 84
    :cond_1
    :goto_1
    iget-object v0, p0, Lfix;->ctt:Lfja;

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;Z)Ljava/util/Collection;
    .locals 5

    .prologue
    .line 90
    iget-object v0, p0, Lfix;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    .line 91
    invoke-direct {p0, p1, p2, p3}, Lfix;->d(JLjava/util/concurrent/TimeUnit;)Lfja;

    move-result-object v2

    .line 93
    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lfix;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 95
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 96
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 97
    iget-object v2, p0, Lfix;->ctt:Lfja;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v0, v1, v3, p4}, Lfja;->a(JLjava/util/concurrent/TimeUnit;Z)Ljava/util/Collection;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public final a(Lfjb;)V
    .locals 3

    .prologue
    .line 43
    iget-object v1, p0, Lfix;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 44
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lfix;->cq:Z

    .line 45
    iput-object p1, p0, Lfix;->mObserver:Lfjb;

    .line 47
    iget-object v0, p0, Lfix;->ctt:Lfja;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lfix;->ctt:Lfja;

    invoke-interface {v0, p1}, Lfja;->a(Lfjb;)V

    .line 53
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lfix;->cpQ:Ljava/util/concurrent/CountDownLatch;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfix;->cpQ:Ljava/util/concurrent/CountDownLatch;

    .line 51
    iget-object v0, p0, Lfix;->cts:Livq;

    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v2

    invoke-interface {v0, p0, v2}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 106
    iget-object v1, p0, Lfix;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    iget-object v0, p0, Lfix;->cts:Livq;

    invoke-static {v0}, Lgtx;->c(Livq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lenx;

    invoke-interface {v0}, Lenx;->wG()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfja;

    iput-object v0, p0, Lfix;->ctt:Lfja;

    .line 109
    iget-boolean v0, p0, Lfix;->cq:Z

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lfix;->ctt:Lfja;

    iget-object v2, p0, Lfix;->mObserver:Lfjb;

    invoke-interface {v0, v2}, Lfja;->a(Lfjb;)V
    :try_end_0
    .catch Lgos; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, Lfix;->cpQ:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 116
    monitor-exit v1

    return-void

    .line 113
    :catch_0
    move-exception v0

    const-string v0, "ExtraDexTvDetector"

    const-string v2, "Failed to load tv detector, disk full"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final wE()V
    .locals 2

    .prologue
    .line 58
    iget-object v1, p0, Lfix;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 59
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lfix;->cq:Z

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lfix;->mObserver:Lfjb;

    .line 62
    iget-object v0, p0, Lfix;->ctt:Lfja;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lfix;->ctt:Lfja;

    invoke-interface {v0}, Lfja;->wE()V

    .line 65
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
