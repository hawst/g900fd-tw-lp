.class final Lkao;
.super Ljava/net/SecureCacheResponse;
.source "PG"


# instance fields
.field private final ePB:Lkam;

.field private final ePC:Lkbh;

.field private final in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Lkam;Lkbh;)V
    .locals 1

    .prologue
    .line 675
    invoke-direct {p0}, Ljava/net/SecureCacheResponse;-><init>()V

    .line 676
    iput-object p1, p0, Lkao;->ePB:Lkam;

    .line 677
    iput-object p2, p0, Lkao;->ePC:Lkbh;

    .line 678
    invoke-static {p2}, Lkag;->a(Lkbh;)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lkao;->in:Ljava/io/InputStream;

    .line 679
    return-void
.end method

.method static synthetic a(Lkao;)Lkbh;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lkao;->ePC:Lkbh;

    return-object v0
.end method


# virtual methods
.method public final getBody()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lkao;->in:Ljava/io/InputStream;

    return-object v0
.end method

.method public final getCipherSuite()Ljava/lang/String;
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePy:Ljava/lang/String;

    return-object v0
.end method

.method public final getHeaders()Ljava/util/Map;
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePx:Lkcq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkcq;->jK(Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getLocalCertificateChain()Ljava/util/List;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 710
    :cond_0
    const/4 v0, 0x0

    .line 712
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    invoke-virtual {v0}, [Ljava/security/cert/Certificate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final getLocalPrincipal()Ljava/security/Principal;
    .locals 2

    .prologue
    .line 716
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 717
    :cond_0
    const/4 v0, 0x0

    .line 719
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPeerPrincipal()Ljava/security/Principal;
    .locals 2

    .prologue
    .line 702
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 703
    :cond_0
    new-instance v0, Ljavax/net/ssl/SSLPeerUnverifiedException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 705
    :cond_1
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    return-object v0
.end method

.method public final getServerCertificateChain()Ljava/util/List;
    .locals 2

    .prologue
    .line 695
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 696
    :cond_0
    new-instance v0, Ljavax/net/ssl/SSLPeerUnverifiedException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 698
    :cond_1
    iget-object v0, p0, Lkao;->ePB:Lkam;

    iget-object v0, v0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    invoke-virtual {v0}, [Ljava/security/cert/Certificate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
