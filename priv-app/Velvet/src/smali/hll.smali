.class public final Lhll;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldvn;


# instance fields
.field private bWE:Leei;

.field private final djY:Z

.field private final djZ:Ligi;

.field private final dka:Ligi;

.field private final dkb:Ligi;

.field private dkc:Lhlm;

.field private dkd:Lhlp;

.field private dke:Lhlq;

.field private dkf:Lhly;

.field private dkg:Lhmd;

.field private dkh:Lhmb;

.field private dki:Lhmc;

.field private dkj:Lhme;

.field private dkk:Lhmf;

.field private dkl:Lhmg;

.field private dkm:Lhmi;

.field private dkn:Lhmj;

.field private dko:Lhml;

.field private dkp:Lhmk;

.field private dkq:Lhmo;

.field private dkr:Lhmm;

.field private dks:Lhmq;

.field private dkt:Lhlz;

.field private dku:Libs;

.field private dkv:Lduv;

.field private mAppSelectionHelper:Libo;

.field private mCalendarHelper:Libp;

.field private mCalendarTextHelper:Ledt;

.field private final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcfo;

.field private final mIntentStarter:Leoj;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfo;Leoj;Landroid/content/pm/PackageManager;ZLigi;Ligi;Ligi;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lhll;->mContext:Landroid/content/Context;

    .line 99
    iput-object p2, p0, Lhll;->mCoreSearchServices:Lcfo;

    .line 100
    iput-object p3, p0, Lhll;->mIntentStarter:Leoj;

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lhll;->mResources:Landroid/content/res/Resources;

    .line 102
    iput-object p4, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 103
    iput-boolean p5, p0, Lhll;->djY:Z

    .line 104
    iput-object p6, p0, Lhll;->djZ:Ligi;

    .line 105
    iput-object p7, p0, Lhll;->dka:Ligi;

    .line 106
    iput-object p8, p0, Lhll;->dkb:Ligi;

    .line 107
    return-void
.end method

.method private aQC()Libp;
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lhll;->mCalendarHelper:Libp;

    if-nez v0, :cond_0

    .line 328
    new-instance v0, Libp;

    iget-object v1, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v1

    iget-object v2, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Libp;-><init>(Lglm;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lhll;->mCalendarHelper:Libp;

    .line 332
    :cond_0
    iget-object v0, p0, Lhll;->mCalendarHelper:Libp;

    return-object v0
.end method

.method private aQD()Libs;
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lhll;->dku:Libs;

    if-nez v0, :cond_0

    .line 345
    new-instance v0, Libs;

    iget-object v1, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v1

    iget-object v2, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Libs;-><init>(Lglm;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lhll;->dku:Libs;

    .line 349
    :cond_0
    iget-object v0, p0, Lhll;->dku:Libs;

    return-object v0
.end method

.method private aQE()Lhmc;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lhll;->dki:Lhmc;

    if-nez v0, :cond_0

    .line 354
    new-instance v0, Lhmc;

    invoke-direct {v0}, Lhmc;-><init>()V

    iput-object v0, p0, Lhll;->dki:Lhmc;

    .line 356
    :cond_0
    iget-object v0, p0, Lhll;->dki:Lhmc;

    return-object v0
.end method

.method private aQF()Lduv;
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lhll;->dkv:Lduv;

    if-nez v0, :cond_0

    .line 361
    new-instance v0, Lduv;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJs()Lchr;

    move-result-object v1

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-direct {v0, v1}, Lduv;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lhll;->dkv:Lduv;

    .line 364
    :cond_0
    iget-object v0, p0, Lhll;->dkv:Lduv;

    return-object v0
.end method


# virtual methods
.method public final I(Lcom/google/android/search/shared/actions/VoiceAction;)Lhlk;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 111
    invoke-interface {p1, p0}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlk;

    .line 112
    if-nez v0, :cond_0

    .line 113
    invoke-direct {p0}, Lhll;->aQE()Lhmc;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1, p0}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlk;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkc:Lhlm;

    if-nez v0, :cond_1

    new-instance v0, Lhlm;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {p0}, Lhll;->aQC()Libp;

    move-result-object v2

    iget-object v3, p0, Lhll;->mCalendarTextHelper:Ledt;

    if-nez v3, :cond_0

    new-instance v3, Ledt;

    iget-object v4, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Ledt;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lhll;->mCalendarTextHelper:Ledt;

    :cond_0
    iget-object v3, p0, Lhll;->mCalendarTextHelper:Ledt;

    invoke-direct {v0, v1, v2, v3}, Lhlm;-><init>(Leoj;Libp;Ledt;)V

    iput-object v0, p0, Lhll;->dkc:Lhlm;

    :cond_1
    iget-object v0, p0, Lhll;->dkc:Lhlm;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lhmh;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {v0, v1}, Lhmh;-><init>(Leoj;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lhln;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {v0, v1}, Lhln;-><init>(Leoj;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lhll;->aQE()Lhmc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkd:Lhlp;

    if-nez v0, :cond_0

    new-instance v0, Lhlp;

    iget-object v1, p0, Lhll;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v3, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-direct {v0, v1, v2, v3}, Lhlp;-><init>(Landroid/content/Context;Leoj;Lcfo;)V

    iput-object v0, p0, Lhll;->dkd:Lhlp;

    :cond_0
    iget-object v0, p0, Lhll;->dkd:Lhlp;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dke:Lhlq;

    if-nez v0, :cond_0

    new-instance v0, Lhlq;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-direct {p0}, Lhll;->aQD()Libs;

    move-result-object v3

    iget-object v4, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1, v2, v3, v4}, Lhlq;-><init>(Leoj;Lglm;Libs;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lhll;->dke:Lhlq;

    :cond_0
    iget-object v0, p0, Lhll;->dke:Lhlq;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lhll;->aQE()Lhmc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkf:Lhly;

    if-nez v0, :cond_0

    new-instance v0, Lhly;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lhly;-><init>(Leoj;Landroid/content/Context;)V

    iput-object v0, p0, Lhll;->dkf:Lhly;

    :cond_0
    iget-object v0, p0, Lhll;->dkf:Lhly;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkt:Lhlz;

    if-nez v0, :cond_0

    new-instance v0, Lhlz;

    iget-object v1, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhlz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhll;->dkt:Lhlz;

    :cond_0
    iget-object v0, p0, Lhll;->dkt:Lhlz;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lhll;->aQE()Lhmc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkg:Lhmd;

    if-nez v0, :cond_0

    new-instance v0, Lhmd;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {v0, v1}, Lhmd;-><init>(Leoj;)V

    iput-object v0, p0, Lhll;->dkg:Lhmd;

    :cond_0
    iget-object v0, p0, Lhll;->dkg:Lhmd;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkj:Lhme;

    if-nez v0, :cond_0

    new-instance v0, Lhme;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {v0, v1}, Lhme;-><init>(Leoj;)V

    iput-object v0, p0, Lhll;->dkj:Lhme;

    :cond_0
    iget-object v0, p0, Lhll;->dkj:Lhme;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkk:Lhmf;

    if-nez v0, :cond_1

    new-instance v0, Lhmf;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mAppSelectionHelper:Libo;

    if-nez v2, :cond_0

    new-instance v2, Libo;

    invoke-direct {p0}, Lhll;->aQF()Lduv;

    move-result-object v3

    iget-object v4, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v5, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Libo;-><init>(Lduv;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    iput-object v2, p0, Lhll;->mAppSelectionHelper:Libo;

    :cond_0
    iget-object v2, p0, Lhll;->mAppSelectionHelper:Libo;

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Leoj;Libo;)V

    iput-object v0, p0, Lhll;->dkk:Lhmf;

    :cond_1
    iget-object v0, p0, Lhll;->dkk:Lhmf;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkl:Lhmg;

    if-nez v0, :cond_0

    new-instance v0, Lhmg;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {v0, v1}, Lhmg;-><init>(Leoj;)V

    iput-object v0, p0, Lhll;->dkl:Lhmg;

    :cond_0
    iget-object v0, p0, Lhll;->dkl:Lhmg;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lhll;->aQE()Lhmc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lhmh;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {v0, v1}, Lhmh;-><init>(Leoj;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkm:Lhmi;

    if-nez v0, :cond_0

    new-instance v0, Lhmi;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-direct {p0}, Lhll;->aQD()Libs;

    move-result-object v3

    iget-object v4, p0, Lhll;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a08bc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct/range {v0 .. v5}, Lhmi;-><init>(Leoj;Lglm;Libs;Ljava/lang/String;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lhll;->dkm:Lhmi;

    :cond_0
    iget-object v0, p0, Lhll;->dkm:Lhmi;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkn:Lhmj;

    if-nez v0, :cond_0

    new-instance v0, Lhmj;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1, v2}, Lhmj;-><init>(Leoj;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lhll;->dkn:Lhmj;

    :cond_0
    iget-object v0, p0, Lhll;->dkn:Lhmj;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkp:Lhmk;

    if-nez v0, :cond_0

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    new-instance v1, Lhla;

    iget-object v2, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v3

    invoke-virtual {v3}, Lfdb;->axI()Lfcx;

    move-result-object v3

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v4

    invoke-virtual {v4}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v4

    iget-object v5, v0, Lgql;->mClock:Lemp;

    invoke-direct {v1, v2, v3, v4, v5}, Lhla;-><init>(Landroid/content/Context;Lfcx;Lfaq;Lemp;)V

    new-instance v2, Lhmk;

    iget-object v3, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v4, p0, Lhll;->mContext:Landroid/content/Context;

    new-instance v5, Lhqs;

    invoke-virtual {v0}, Lgql;->aJU()Lgpu;

    move-result-object v0

    invoke-virtual {v0}, Lgpu;->aJH()Lhlr;

    move-result-object v0

    invoke-direct {v5, v0, v1}, Lhqs;-><init>(Lhlr;Lhla;)V

    invoke-direct {v2, v3, v4, v5}, Lhmk;-><init>(Leoj;Landroid/content/Context;Lhqs;)V

    iput-object v2, p0, Lhll;->dkp:Lhmk;

    :cond_0
    iget-object v0, p0, Lhll;->dkp:Lhmk;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dko:Lhml;

    if-nez v0, :cond_0

    new-instance v0, Lhml;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1, v2}, Lhml;-><init>(Leoj;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lhll;->dko:Lhml;

    :cond_0
    iget-object v0, p0, Lhll;->dko:Lhml;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkr:Lhmm;

    if-nez v0, :cond_0

    new-instance v0, Lhmm;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    invoke-direct {p0}, Lhll;->aQD()Libs;

    move-result-object v2

    iget-object v3, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3}, Lhmm;-><init>(Leoj;Libs;Landroid/content/Context;)V

    iput-object v0, p0, Lhll;->dkr:Lhmm;

    :cond_0
    iget-object v0, p0, Lhll;->dkr:Lhmm;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkq:Lhmo;

    if-nez v0, :cond_0

    new-instance v0, Lhmo;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mContext:Landroid/content/Context;

    iget-boolean v3, p0, Lhll;->djY:Z

    invoke-direct {v0, v1, v2, v3}, Lhmo;-><init>(Leoj;Landroid/content/Context;Z)V

    iput-object v0, p0, Lhll;->dkq:Lhmo;

    :cond_0
    iget-object v0, p0, Lhll;->dkq:Lhmo;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dks:Lhmq;

    if-nez v0, :cond_0

    new-instance v0, Lhmq;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v0, v1, v2}, Lhmq;-><init>(Leoj;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lhll;->dks:Lhmq;

    :cond_0
    iget-object v0, p0, Lhll;->dks:Lhmq;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 56
    iget-object v0, p0, Lhll;->dkh:Lhmb;

    if-nez v0, :cond_2

    new-instance v0, Lhmb;

    iget-object v1, p0, Lhll;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhll;->mContext:Landroid/content/Context;

    new-instance v3, Ldxd;

    iget-object v4, p0, Lhll;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lhll;->mResources:Landroid/content/res/Resources;

    invoke-direct {v3, v4, v5}, Ldxd;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iget-object v4, p0, Lhll;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v5, p0, Lhll;->djZ:Ligi;

    invoke-interface {v5}, Ligi;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhlr;

    iget-object v6, p0, Lhll;->dka:Ligi;

    invoke-direct {p0}, Lhll;->aQF()Lduv;

    move-result-object v7

    iget-object v8, p0, Lhll;->dkb:Ligi;

    invoke-interface {v8}, Ligi;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lciu;

    invoke-direct {p0}, Lhll;->aQC()Libp;

    move-result-object v9

    iget-object v10, p0, Lhll;->bWE:Leei;

    if-nez v10, :cond_1

    const/4 v10, 0x0

    iget-object v11, p0, Lhll;->mCoreSearchServices:Lcfo;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v11}, Lcfo;->BL()Lchk;

    move-result-object v11

    if-eqz v11, :cond_0

    iget-object v10, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v10}, Lcfo;->BL()Lchk;

    move-result-object v10

    invoke-virtual {v10}, Lchk;->Km()Z

    move-result v10

    :cond_0
    new-instance v11, Leei;

    iget-object v12, p0, Lhll;->mContext:Landroid/content/Context;

    invoke-direct {v11, v12, v10}, Leei;-><init>(Landroid/content/Context;Z)V

    iput-object v11, p0, Lhll;->bWE:Leei;

    :cond_1
    iget-object v10, p0, Lhll;->bWE:Leei;

    iget-object v11, p0, Lhll;->mCoreSearchServices:Lcfo;

    invoke-virtual {v11}, Lcfo;->Eg()Ligi;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lhmb;-><init>(Leoj;Landroid/content/Context;Ldxd;Landroid/content/pm/PackageManager;Lhlr;Ligi;Lduv;Lciu;Libp;Leei;Ligi;)V

    iput-object v0, p0, Lhll;->dkh:Lhmb;

    :cond_2
    iget-object v0, p0, Lhll;->dkh:Lhmb;

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lhll;->aQE()Lhmc;

    move-result-object v0

    return-object v0
.end method
