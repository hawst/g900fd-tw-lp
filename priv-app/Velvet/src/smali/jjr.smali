.class public final Ljjr;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile epk:[Ljjr;


# instance fields
.field private aez:I

.field private dQM:Ljava/lang/String;

.field private eiK:I

.field public epl:[Ljjs;

.field private epm:Ljava/lang/String;

.field public epn:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 661
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 662
    iput v1, p0, Ljjr;->aez:I

    invoke-static {}, Ljjs;->boc()[Ljjs;

    move-result-object v0

    iput-object v0, p0, Ljjr;->epl:[Ljjs;

    const-string v0, ""

    iput-object v0, p0, Ljjr;->epm:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljjr;->epn:[I

    iput v1, p0, Ljjr;->eiK:I

    const-string v0, ""

    iput-object v0, p0, Ljjr;->dQM:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjr;->eCz:I

    .line 663
    return-void
.end method

.method public static bnZ()[Ljjr;
    .locals 2

    .prologue
    .line 579
    sget-object v0, Ljjr;->epk:[Ljjr;

    if-nez v0, :cond_1

    .line 580
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 582
    :try_start_0
    sget-object v0, Ljjr;->epk:[Ljjr;

    if-nez v0, :cond_0

    .line 583
    const/4 v0, 0x0

    new-array v0, v0, [Ljjr;

    sput-object v0, Ljjr;->epk:[Ljjr;

    .line 585
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    :cond_1
    sget-object v0, Ljjr;->epk:[Ljjr;

    return-object v0

    .line 585
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 560
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjr;->epl:[Ljjs;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljjs;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljjr;->epl:[Ljjs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljjs;

    invoke-direct {v3}, Ljjs;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljjr;->epl:[Ljjs;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljjs;

    invoke-direct {v3}, Ljjs;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjr;->epl:[Ljjs;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjr;->epm:Ljava/lang/String;

    iget v0, p0, Ljjr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjr;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_5

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_5
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljjr;->epn:[I

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    if-nez v0, :cond_7

    array-length v3, v5

    if-ne v2, v3, :cond_7

    iput-object v5, p0, Ljjr;->epn:[I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Ljjr;->epn:[I

    array-length v0, v0

    goto :goto_5

    :cond_7
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_8

    iget-object v4, p0, Ljjr;->epn:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljjr;->epn:[I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_6

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    if-eqz v0, :cond_d

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljjr;->epn:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_a

    iget-object v0, p0, Ljjr;->epn:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_8

    :pswitch_2
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_b
    iget-object v2, p0, Ljjr;->epn:[I

    array-length v2, v2

    goto :goto_7

    :cond_c
    iput-object v4, p0, Ljjr;->epn:[I

    :cond_d
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Ljjr;->eiK:I

    iget v0, p0, Ljjr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjr;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjr;->dQM:Ljava/lang/String;

    iget v0, p0, Ljjr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjr;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 680
    iget-object v0, p0, Ljjr;->epl:[Ljjs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljjr;->epl:[Ljjs;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 681
    :goto_0
    iget-object v2, p0, Ljjr;->epl:[Ljjs;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 682
    iget-object v2, p0, Ljjr;->epl:[Ljjs;

    aget-object v2, v2, v0

    .line 683
    if-eqz v2, :cond_0

    .line 684
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 681
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 688
    :cond_1
    iget v0, p0, Ljjr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 689
    const/4 v0, 0x2

    iget-object v2, p0, Ljjr;->epm:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 691
    :cond_2
    iget-object v0, p0, Ljjr;->epn:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljjr;->epn:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 692
    :goto_1
    iget-object v0, p0, Ljjr;->epn:[I

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 693
    const/4 v0, 0x3

    iget-object v2, p0, Ljjr;->epn:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 692
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 696
    :cond_3
    iget v0, p0, Ljjr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 697
    const/4 v0, 0x4

    iget v1, p0, Ljjr;->eiK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 699
    :cond_4
    iget v0, p0, Ljjr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 700
    const/4 v0, 0x5

    iget-object v1, p0, Ljjr;->dQM:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 702
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 703
    return-void
.end method

.method public final boa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Ljjr;->epm:Ljava/lang/String;

    return-object v0
.end method

.method public final bob()Ljava/lang/String;
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Ljjr;->dQM:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 707
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 708
    iget-object v2, p0, Ljjr;->epl:[Ljjs;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljjr;->epl:[Ljjs;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 709
    :goto_0
    iget-object v3, p0, Ljjr;->epl:[Ljjs;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 710
    iget-object v3, p0, Ljjr;->epl:[Ljjs;

    aget-object v3, v3, v0

    .line 711
    if-eqz v3, :cond_0

    .line 712
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 709
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 717
    :cond_2
    iget v2, p0, Ljjr;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 718
    const/4 v2, 0x2

    iget-object v3, p0, Ljjr;->epm:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 721
    :cond_3
    iget-object v2, p0, Ljjr;->epn:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljjr;->epn:[I

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    .line 723
    :goto_1
    iget-object v3, p0, Ljjr;->epn:[I

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 724
    iget-object v3, p0, Ljjr;->epn:[I

    aget v3, v3, v1

    .line 725
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 723
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 728
    :cond_4
    add-int/2addr v0, v2

    .line 729
    iget-object v1, p0, Ljjr;->epn:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 731
    :cond_5
    iget v1, p0, Ljjr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 732
    const/4 v1, 0x4

    iget v2, p0, Ljjr;->eiK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 735
    :cond_6
    iget v1, p0, Ljjr;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 736
    const/4 v1, 0x5

    iget-object v2, p0, Ljjr;->dQM:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 739
    :cond_7
    return v0
.end method

.method public final wn(Ljava/lang/String;)Ljjr;
    .locals 1

    .prologue
    .line 601
    if-nez p1, :cond_0

    .line 602
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 604
    :cond_0
    iput-object p1, p0, Ljjr;->epm:Ljava/lang/String;

    .line 605
    iget v0, p0, Ljjr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjr;->aez:I

    .line 606
    return-object p0
.end method

.method public final wo(Ljava/lang/String;)Ljjr;
    .locals 1

    .prologue
    .line 645
    if-nez p1, :cond_0

    .line 646
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 648
    :cond_0
    iput-object p1, p0, Ljjr;->dQM:Ljava/lang/String;

    .line 649
    iget v0, p0, Ljjr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjr;->aez:I

    .line 650
    return-object p0
.end method
