.class final Lilj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private synthetic dDT:Lilt;


# direct methods
.method constructor <init>(Lili;Lilt;)V
    .locals 0

    .prologue
    .line 853
    iput-object p2, p0, Lilj;->dDT:Lilt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 860
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->aXm()Lils;

    move-result-object v0

    iget-object v0, v0, Lils;->ciy:Ljava/lang/Object;

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 868
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->aXn()Lils;

    move-result-object v0

    iget-object v0, v0, Lils;->ciy:Ljava/lang/Object;

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->previousIndex()I

    move-result v0

    return v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 880
    iget-object v0, p0, Lilj;->dDT:Lilt;

    invoke-virtual {v0}, Lilt;->remove()V

    .line 881
    return-void
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 884
    iget-object v1, p0, Lilj;->dDT:Lilt;

    iget-object v0, v1, Lilt;->dEb:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, v1, Lilt;->dEb:Lils;

    iput-object p1, v0, Lils;->ciy:Ljava/lang/Object;

    .line 885
    return-void

    .line 884
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
