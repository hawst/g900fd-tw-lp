.class public final Lgay;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljbp;Landroid/location/Location;)F
    .locals 9

    .prologue
    .line 121
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 122
    invoke-virtual {p0}, Ljbp;->mR()D

    move-result-wide v0

    invoke-virtual {p0}, Ljbp;->mS()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 124
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public static a(Landroid/content/Context;IFFI)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 190
    packed-switch p4, :pswitch_data_0

    .line 196
    const v0, 0x7f0a039b

    move p3, p2

    .line 200
    :goto_0
    invoke-static {p1, p4}, Lgay;->aZ(II)D

    move-result-wide v2

    .line 203
    float-to-double v6, p3

    cmpg-double v1, v2, v6

    if-gez v1, :cond_2

    .line 204
    float-to-double v2, p3

    move v1, v4

    .line 208
    :goto_1
    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {p0, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 210
    if-eqz v1, :cond_0

    .line 211
    invoke-static {}, Lemg;->auw()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " >"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_2
    invoke-static {v0}, Lemg;->le(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :cond_0
    return-object v0

    .line 192
    :pswitch_0
    const v0, 0x7f0a039a

    .line 194
    goto :goto_0

    .line 214
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "< "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v1, v5

    goto :goto_1

    .line 190
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;II)Ljava/lang/String;
    .locals 6

    .prologue
    .line 175
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    const v0, 0x7f0a039b

    .line 177
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Lgay;->aZ(II)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 175
    :cond_0
    const v0, 0x7f0a039a

    goto :goto_0
.end method

.method private static aZ(II)D
    .locals 4

    .prologue
    .line 226
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const-wide v0, 0x408f400000000000L    # 1000.0

    .line 228
    :goto_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    div-double v0, v2, v0

    int-to-double v2, p0

    mul-double/2addr v0, v2

    return-wide v0

    .line 226
    :cond_0
    const-wide v0, 0x409925604189374cL    # 1609.344

    goto :goto_0
.end method

.method public static ac(Ljava/util/List;)Ljava/util/List;
    .locals 3

    .prologue
    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 63
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 64
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lgay;->n(Landroid/location/Location;)Ljhm;

    move-result-object v0

    goto :goto_1

    .line 67
    :cond_1
    return-object v1
.end method

.method public static d(Landroid/location/Location;Landroid/location/Location;)F
    .locals 9

    .prologue
    .line 75
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 76
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 78
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public static d(Ljbp;)Landroid/location/Location;
    .locals 4
    .param p0    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    .line 153
    :cond_0
    new-instance v0, Landroid/location/Location;

    const-string v1, "unknown"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Ljbp;->mR()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 155
    invoke-virtual {p0}, Ljbp;->mS()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_0
.end method

.method public static e(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Landroid/location/Location;)Ljhm;
    .locals 6

    .prologue
    .line 37
    new-instance v0, Ljhm;

    invoke-direct {v0}, Ljhm;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljhm;->dl(J)Ljhm;

    move-result-object v0

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljhm;->ve(Ljava/lang/String;)Ljhm;

    move-result-object v0

    .line 40
    new-instance v1, Ljbp;

    invoke-direct {v1}, Ljbp;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljbp;->n(D)Ljbp;

    move-result-object v1

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljbp;->o(D)Ljbp;

    move-result-object v1

    iput-object v1, v0, Ljhm;->aeB:Ljbp;

    .line 44
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljhm;->pV(I)Ljhm;

    .line 47
    :cond_0
    return-object v0
.end method

.method public static o(Landroid/location/Location;)Ljbp;
    .locals 4
    .param p0    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 92
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    .line 93
    :cond_0
    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    .line 94
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljbp;->n(D)Ljbp;

    .line 95
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljbp;->o(D)Ljbp;

    goto :goto_0
.end method
