.class public final Ldqy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# static fields
.field public static final bGC:Ldqy;

.field private static final bGd:[F

.field private static final bGe:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6
    new-instance v0, Ldqy;

    invoke-direct {v0}, Ldqy;-><init>()V

    sput-object v0, Ldqy;->bGC:Ldqy;

    .line 18
    const/16 v0, 0x65

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Ldqy;->bGd:[F

    .line 34
    const/high16 v0, 0x3f800000    # 1.0f

    sget-object v1, Ldqy;->bGd:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sput v0, Ldqy;->bGe:F

    return-void

    .line 18
    :array_0
    .array-data 4
        0x0
        0x395b0dc7    # 2.08906E-4f
        0x3a5ba3f8
        0x3af7bd3b    # 0.0018901f
        0x3b5cc53d
        0x3bace653
        0x3bf99090
        0x3c2a3ad2    # 0.01039f
        0x3c5ed3cb    # 0.0136003f
        0x3c8d4d91
        0x3caecbc5
        0x3cd3e708    # 0.025867f
        0x3cfca13b
        0x3d147dd1
        0x3d2c7b1e
        0x3d464869
        0x3d61e4c2
        0x3d7f4ee5
        0x3d8f514f
        0x3d9ffc1f
        0x3db1a42a
        0x3dc4453f
        0x3dd7dae8    # 0.105398f
        0x3dec6046
        0x3e00e842
        0x3e0c12ae    # 0.13679f
        0x3e17acc5    # 0.14812f
        0x3e23b320
        0x3e30229a
        0x3e3cf788
        0x3e4a2e3c
        0x3e57c2ca
        0x3e65b142
        0x3e73f530    # 0.23824f
        0x3e814552
        0x3e88b652
        0x3e904b5e
        0x3e9801f7    # 0.29689f
        0x3e9fd7e4
        0x3ea7ca64
        0x3eafd6d8
        0x3eb7fae3
        0x3ec033a4
        0x3ec87e9e
        0x3ed0d8ed
        0x3ed93fd1
        0x3ee1b0cd
        0x3eea28bb
        0x3ef2a540
        0x3efb2379
        0x3f01d042
        0x3f060cf2
        0x3f0a467c
        0x3f0e7b70
        0x3f12aa8f
        0x3f16d2aa
        0x3f1af263
        0x3f1f0889
        0x3f231401
        0x3f27138c
        0x3f2b060c
        0x3f2eea74
        0x3f32bfba
        0x3f3684e0
        0x3f3a38da
        0x3f3ddae0
        0x3f4169e4
        0x3f44e52e
        0x3f484bf5
        0x3f4b9d5e
        0x3f4ed8c3
        0x3f51fd7a
        0x3f550add
        0x3f580054
        0x3f5add6a
        0x3f5da177
        0x3f604c16
        0x3f62dce4
        0x3f655369
        0x3f67af53
        0x3f69f05f
        0x3f6c1627
        0x3f6e2079
        0x3f700f13
        0x3f71e1d2
        0x3f739885
        0x3f753309
        0x3f76b13e
        0x3f781312
        0x3f795864
        0x3f7a8145
        0x3f7b8da4
        0x3f7c7d80
        0x3f7d50db
        0x3f7e07c4
        0x3f7ea24d
        0x3f7f2086
        0x3f7f826f
        0x3f7fc84b
        0x3f7ff21b
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 5

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 38
    cmpl-float v2, p1, v0

    if-ltz v2, :cond_0

    .line 54
    :goto_0
    return v0

    .line 42
    :cond_0
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_1

    move v0, v1

    .line 43
    goto :goto_0

    .line 46
    :cond_1
    sget-object v0, Ldqy;->bGd:[F

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    sget-object v1, Ldqy;->bGd:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 50
    int-to-float v1, v0

    sget v2, Ldqy;->bGe:F

    mul-float/2addr v1, v2

    .line 51
    sub-float v1, p1, v1

    .line 52
    sget v2, Ldqy;->bGe:F

    div-float/2addr v1, v2

    .line 54
    sget-object v2, Ldqy;->bGd:[F

    aget v2, v2, v0

    sget-object v3, Ldqy;->bGd:[F

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    sget-object v4, Ldqy;->bGd:[F

    aget v0, v4, v0

    sub-float v0, v3, v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0
.end method
