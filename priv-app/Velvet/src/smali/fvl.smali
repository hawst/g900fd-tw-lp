.class public final Lfvl;
.super Lfus;
.source "PG"


# instance fields
.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;Lfyk;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    .line 79
    iput-object p5, p0, Lfvl;->mTimeToLeaveFactory:Lfyk;

    .line 80
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;Lfyk;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 72
    iput-object p5, p0, Lfvl;->mTimeToLeaveFactory:Lfyk;

    .line 73
    return-void
.end method

.method private a(Landroid/content/Context;Ljag;Ljava/lang/String;Z)Lanh;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 213
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 214
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 216
    iget-object v1, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 218
    new-instance v6, Lanq;

    invoke-direct {v6}, Lanq;-><init>()V

    .line 220
    invoke-static {p2, v7}, Lfvl;->a(Ljag;I)Lanr;

    move-result-object v1

    iput-object v1, v6, Lanq;->aij:Lanr;

    .line 221
    invoke-static {p2, v8}, Lfvl;->a(Ljag;I)Lanr;

    move-result-object v1

    iput-object v1, v6, Lanq;->aik:Lanr;

    .line 223
    invoke-static {p1, p2, v6}, Lfvl;->a(Landroid/content/Context;Ljag;Lanq;)Lanq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 224
    invoke-virtual/range {v1 .. v6}, Lfvl;->a(Landroid/content/Context;Ljag;JLanq;)V

    .line 226
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 227
    invoke-virtual {v6, p3}, Lanq;->aN(Ljava/lang/String;)Lanq;

    .line 230
    :cond_0
    if-eqz p4, :cond_1

    .line 231
    const v1, 0x7f0a022e

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljag;->aZD()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lanq;->aI(Ljava/lang/String;)Lanq;

    .line 233
    invoke-virtual {p2}, Ljag;->bdP()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    const v1, 0x7f0a022f

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljag;->bdO()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lanq;->aJ(Ljava/lang/String;)Lanq;

    .line 239
    :cond_1
    iput-object v6, v0, Lanh;->ahd:Lanq;

    .line 241
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljag;Lanq;)Lanq;
    .locals 5

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 271
    invoke-virtual {p1}, Ljag;->getStatusCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 314
    const/4 v1, 0x4

    move v4, v1

    move v1, v0

    move v0, v4

    .line 319
    :goto_0
    invoke-virtual {p2, v0}, Lanq;->ct(I)Lanq;

    .line 320
    if-eqz v1, :cond_0

    .line 321
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lanq;->aK(Ljava/lang/String;)Lanq;

    .line 323
    :cond_0
    return-object p2

    .line 274
    :pswitch_0
    const v1, 0x7f0a01f6

    .line 275
    goto :goto_0

    .line 279
    :pswitch_1
    const v1, 0x7f0a01f7

    .line 280
    goto :goto_0

    .line 283
    :pswitch_2
    const/4 v0, 0x2

    .line 284
    const v1, 0x7f0a01f8

    .line 285
    goto :goto_0

    .line 289
    :pswitch_3
    const v0, 0x7f0a01f9

    move v4, v1

    move v1, v0

    move v0, v4

    .line 290
    goto :goto_0

    .line 294
    :pswitch_4
    const v0, 0x7f0a01fc

    move v4, v1

    move v1, v0

    move v0, v4

    .line 295
    goto :goto_0

    .line 299
    :pswitch_5
    const v0, 0x7f0a01fd

    move v4, v1

    move v1, v0

    move v0, v4

    .line 300
    goto :goto_0

    .line 304
    :pswitch_6
    const v0, 0x7f0a01fe

    move v1, v0

    move v0, v2

    .line 305
    goto :goto_0

    .line 309
    :pswitch_7
    const v0, 0x7f0a01fb

    move v1, v0

    move v0, v2

    .line 310
    goto :goto_0

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private static a(Ljag;I)Lanr;
    .locals 3

    .prologue
    .line 245
    new-instance v1, Lanr;

    invoke-direct {v1}, Lanr;-><init>()V

    .line 248
    invoke-virtual {v1, p1}, Lanr;->cu(I)Lanr;

    .line 250
    if-nez p1, :cond_0

    .line 251
    iget-object v0, p0, Ljag;->dWf:Ljaf;

    .line 252
    invoke-virtual {p0}, Ljag;->bdL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanr;->aS(Ljava/lang/String;)Lanr;

    .line 253
    invoke-virtual {p0}, Ljag;->bdK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanr;->aT(Ljava/lang/String;)Lanr;

    .line 260
    :goto_0
    invoke-virtual {v0}, Ljaf;->pG()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanr;->aO(Ljava/lang/String;)Lanr;

    .line 261
    iget-object v0, v0, Ljaf;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanr;->aP(Ljava/lang/String;)Lanr;

    .line 263
    return-object v1

    .line 255
    :cond_0
    iget-object v0, p0, Ljag;->dWj:Ljaf;

    .line 256
    invoke-virtual {p0}, Ljag;->bdN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanr;->aS(Ljava/lang/String;)Lanr;

    .line 257
    invoke-virtual {p0}, Ljag;->bdM()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanr;->aT(Ljava/lang/String;)Lanr;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const-wide/32 v4, 0xea60

    const/4 v8, 0x1

    .line 431
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 433
    const-wide/32 v2, 0x36ee80

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    const-wide/32 v2, 0x2255100

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 435
    invoke-virtual {p1, p2}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    .line 436
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    sub-long/2addr v2, v6

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 439
    if-eqz v0, :cond_0

    .line 440
    const v0, 0x7f0a0400

    .line 441
    neg-int v1, v1

    .line 445
    :goto_0
    invoke-static {p0, v1, v8}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v1

    .line 446
    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 448
    :goto_1
    return-object v0

    .line 443
    :cond_0
    const v0, 0x7f0a0401

    goto :goto_0

    .line 448
    :cond_1
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const/high16 v6, 0x40000

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Ljaj;Lfvn;)Ljava/util/Calendar;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 510
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    if-nez p0, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-object v0

    .line 516
    :cond_1
    sget-object v1, Lfvm;->cCy:[I

    invoke-virtual {p1}, Lfvn;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 526
    invoke-virtual {p0}, Ljaj;->bea()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527
    invoke-virtual {p0}, Ljaj;->bdZ()J

    move-result-wide v2

    .line 535
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 538
    invoke-virtual {p0}, Ljaj;->bec()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 539
    invoke-virtual {p0}, Ljaj;->beb()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvl;->me(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 543
    :goto_2
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 545
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 546
    new-instance v1, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_0

    .line 519
    :pswitch_0
    invoke-virtual {p0}, Ljaj;->bdY()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 520
    invoke-virtual {p0}, Ljaj;->bdX()J

    move-result-wide v2

    goto :goto_1

    .line 541
    :cond_2
    const-string v0, "UTC"

    goto :goto_2

    .line 516
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Lanr;Ljava/util/Calendar;)V
    .locals 8

    .prologue
    .line 408
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    invoke-static {v1, p2}, Lfzz;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0a03fe

    .line 411
    :goto_0
    if-eqz v0, :cond_2

    .line 412
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    :goto_1
    invoke-virtual {p1, v0}, Lanr;->aQ(Ljava/lang/String;)Lanr;

    .line 424
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 425
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 426
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 427
    invoke-virtual {p1, v0}, Lanr;->aR(Ljava/lang/String;)Lanr;

    .line 428
    return-void

    .line 408
    :cond_0
    invoke-static {v0, p2}, Lfzz;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0416

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 414
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 417
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const v6, 0x80012

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static me(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/high16 v3, 0x42700000    # 60.0f

    .line 577
    if-eqz p0, :cond_1

    const-string v0, "GMT+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GMT-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 581
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 582
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 584
    const/high16 v2, 0x41c00000    # 24.0f

    rem-float/2addr v1, v2

    .line 585
    float-to-int v2, v1

    .line 586
    mul-float/2addr v1, v3

    rem-float/2addr v1, v3

    float-to-int v1, v1

    .line 587
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "GMT%c%d:%d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v0

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 593
    :cond_1
    :goto_0
    return-object p0

    .line 590
    :catch_0
    move-exception v0

    const-string v0, "QpFlightStatusEntryAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid time zone: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lfvl;->cCl:Lftx;

    invoke-virtual {v0, p1}, Lftx;->aS(Landroid/content/Context;)Lang;

    move-result-object v0

    .line 85
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 86
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 115
    iget-object v1, p2, Lizj;->dSi:Ljae;

    if-eqz v1, :cond_1

    iget-object v2, v1, Ljae;->dWa:[Ljag;

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v1, v1, Ljae;->dWa:[Ljag;

    aget-object v1, v1, v3

    .line 116
    :goto_0
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p2}, Lizj;->getReason()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0, v3}, Lfvl;->a(Landroid/content/Context;Ljag;Ljava/lang/String;Z)Lanh;

    move-result-object v0

    .line 120
    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 115
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljag;)Lanh;
    .locals 4

    .prologue
    .line 171
    invoke-virtual {p2}, Ljag;->bdS()Ljava/lang/String;

    move-result-object v0

    .line 172
    new-instance v1, Lfsa;

    const/16 v2, 0xef

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 177
    new-instance v1, Laoj;

    invoke-direct {v1}, Laoj;-><init>()V

    .line 178
    const v2, 0x7f0a0234

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 180
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 181
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 182
    iput-object v1, v2, Lanh;->agz:Laoj;

    .line 183
    iput-object v0, v2, Lanh;->ahs:Lani;

    .line 185
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v2, Lanh;->ahu:Lizj;

    .line 187
    return-object v2
.end method

.method public final a(Landroid/content/Context;Ljag;JLanq;)V
    .locals 15

    .prologue
    .line 329
    new-instance v8, Ljava/util/GregorianCalendar;

    invoke-direct {v8}, Ljava/util/GregorianCalendar;-><init>()V

    .line 330
    new-instance v2, Ljava/util/Date;

    move-wide/from16 v0, p3

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 332
    move-object/from16 v0, p2

    iget-object v2, v0, Ljag;->dWg:Ljaj;

    sget-object v3, Lfvn;->cCz:Lfvn;

    invoke-static {v2, v3}, Lfvl;->a(Ljaj;Lfvn;)Ljava/util/Calendar;

    move-result-object v4

    .line 333
    move-object/from16 v0, p2

    iget-object v2, v0, Ljag;->dWg:Ljaj;

    sget-object v3, Lfvn;->cCA:Lfvn;

    invoke-static {v2, v3}, Lfvl;->a(Ljaj;Lfvn;)Ljava/util/Calendar;

    move-result-object v3

    .line 334
    if-eqz v3, :cond_4

    move-object v2, v3

    .line 337
    :goto_0
    move-object/from16 v0, p2

    iget-object v5, v0, Ljag;->dWk:Ljaj;

    sget-object v6, Lfvn;->cCz:Lfvn;

    invoke-static {v5, v6}, Lfvl;->a(Ljaj;Lfvn;)Ljava/util/Calendar;

    move-result-object v7

    .line 338
    move-object/from16 v0, p2

    iget-object v5, v0, Ljag;->dWk:Ljaj;

    sget-object v6, Lfvn;->cCA:Lfvn;

    invoke-static {v5, v6}, Lfvl;->a(Ljaj;Lfvn;)Ljava/util/Calendar;

    move-result-object v6

    .line 339
    if-eqz v6, :cond_5

    move-object v5, v6

    .line 342
    :goto_1
    if-eqz v2, :cond_0

    .line 343
    move-object/from16 v0, p5

    iget-object v9, v0, Lanq;->aij:Lanr;

    move-object/from16 v0, p1

    invoke-static {v0, v9, v2}, Lfvl;->a(Landroid/content/Context;Lanr;Ljava/util/Calendar;)V

    .line 346
    :cond_0
    if-eqz v5, :cond_1

    .line 347
    move-object/from16 v0, p5

    iget-object v9, v0, Lanq;->aik:Lanr;

    move-object/from16 v0, p1

    invoke-static {v0, v9, v5}, Lfvl;->a(Landroid/content/Context;Lanr;Ljava/util/Calendar;)V

    .line 351
    :cond_1
    if-eqz v2, :cond_6

    invoke-virtual {v8, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 353
    move-object/from16 v0, p1

    invoke-static {v0, v8, v2}, Lfvl;->a(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 354
    const v6, 0x7f0a0213

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Lanq;->aM(Ljava/lang/String;)Lanq;

    .line 357
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 358
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 361
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    .line 362
    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    long-to-int v2, v2

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lanq;->aL(Ljava/lang/String;)Lanq;

    .line 367
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lanq;->y(F)Lanq;

    .line 405
    :cond_3
    :goto_2
    return-void

    :cond_4
    move-object v2, v4

    .line 334
    goto :goto_0

    :cond_5
    move-object v5, v7

    .line 339
    goto :goto_1

    .line 368
    :cond_6
    if-eqz v5, :cond_3

    .line 369
    move-object/from16 v0, p1

    invoke-static {v0, v8, v5}, Lfvl;->a(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 370
    invoke-virtual {v8, v5}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 371
    const v4, 0x7f0a0215

    .line 372
    invoke-virtual/range {p2 .. p2}, Ljag;->getStatusCode()I

    move-result v3

    const/4 v8, 0x5

    if-eq v3, v8, :cond_a

    .line 373
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    sub-long v10, p3, v10

    long-to-double v10, v10

    .line 374
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long v2, v12, v2

    long-to-double v2, v2

    .line 376
    div-double v2, v10, v2

    .line 377
    const-wide/16 v10, 0x0

    cmpg-double v8, v2, v10

    if-gez v8, :cond_8

    .line 378
    const-wide/16 v2, 0x0

    .line 382
    :cond_7
    :goto_3
    double-to-float v2, v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lanq;->y(F)Lanq;

    move v2, v4

    .line 388
    :goto_4
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v9, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lanq;->aM(Ljava/lang/String;)Lanq;

    .line 389
    invoke-virtual {v5, v7}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 390
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 393
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    .line 394
    const-wide/32 v4, 0xea60

    div-long/2addr v2, v4

    long-to-int v2, v2

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lanq;->aL(Ljava/lang/String;)Lanq;

    goto :goto_2

    .line 379
    :cond_8
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v8, v2, v10

    if-lez v8, :cond_7

    .line 380
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto :goto_3

    .line 385
    :cond_9
    const v2, 0x7f0a0216

    .line 386
    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lanq;->y(F)Lanq;

    goto :goto_4

    :cond_a
    move v2, v4

    goto :goto_4
.end method

.method public final aDq()Z
    .locals 1

    .prologue
    .line 598
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/content/Context;Ljag;)Lanh;
    .locals 4

    .prologue
    .line 192
    invoke-virtual {p2}, Ljag;->bdQ()Ljava/lang/String;

    move-result-object v0

    .line 193
    new-instance v1, Lfsa;

    const/16 v2, 0xda

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 198
    new-instance v1, Laoj;

    invoke-direct {v1}, Laoj;-><init>()V

    .line 199
    const v2, 0x7f0a01ff

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 201
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 202
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 203
    iput-object v1, v2, Lanh;->agz:Laoj;

    .line 204
    iput-object v0, v2, Lanh;->ahs:Lani;

    .line 206
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v2, Lanh;->ahu:Lizj;

    .line 208
    return-object v2
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 125
    const/4 v0, 0x5

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 128
    iget-object v0, p2, Lizj;->dSi:Ljae;

    invoke-static {v0}, Lgao;->a(Ljae;)Ljaf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Ljaf;->dMF:[Liyg;

    array-length v1, v1

    if-ne v1, v9, :cond_0

    new-instance v1, Lgca;

    iget-object v2, v0, Ljaf;->dMF:[Liyg;

    aget-object v2, v2, v8

    iget-object v3, p0, Lfuz;->mClock:Lemp;

    invoke-direct {v1, v2, v3}, Lgca;-><init>(Liyg;Lemp;)V

    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgca;->p(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lfzb;

    iget-object v3, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfvl;->mStringEvaluator:Lgbr;

    invoke-direct {v2, v3, v1, v4}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    iget-object v0, v0, Ljaf;->aeB:Ljbp;

    invoke-virtual {v2, p1, p2, v1, v0}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;

    move-result-object v0

    .line 129
    :goto_0
    if-eqz v0, :cond_1

    .line 130
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :goto_1
    iget-object v0, p2, Lizj;->dSi:Ljae;

    .line 140
    if-eqz v0, :cond_4

    iget-object v1, v0, Ljae;->dWa:[Ljag;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 141
    iget-object v5, v0, Ljae;->dWa:[Ljag;

    array-length v6, v5

    move v3, v8

    move v4, v9

    move-object v0, v7

    :goto_2
    if-ge v3, v6, :cond_3

    aget-object v2, v5, v3

    .line 143
    if-nez v0, :cond_7

    .line 145
    invoke-virtual {p2}, Lizj;->getReason()Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    .line 147
    :goto_3
    invoke-direct {p0, p1, v2, v0, v4}, Lfvl;->a(Landroid/content/Context;Ljag;Ljava/lang/String;Z)Lanh;

    move-result-object v0

    invoke-static {v10, v0}, Lfvl;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 141
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v8

    move-object v0, v1

    goto :goto_2

    :cond_0
    move-object v0, v7

    .line 128
    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p2, Lizj;->dSi:Ljae;

    invoke-static {v0}, Lgao;->a(Ljae;)Ljaf;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, v1, Ljaf;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lizj;->dUv:Ljhk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfvl;->mTimeToLeaveFactory:Lfyk;

    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v4, v1, Ljaf;->dMF:[Liyg;

    iget-object v5, v1, Ljaf;->aeB:Ljbp;

    const/16 v6, 0xb4

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;I)Lanh;

    move-result-object v0

    :goto_4
    invoke-static {v10, v0}, Lfvl;->a(Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move-object v0, v7

    goto :goto_4

    :cond_3
    move-object v7, v0

    .line 153
    :cond_4
    if-eqz v7, :cond_6

    .line 154
    iget-object v0, v7, Ljag;->dMX:[Ljbg;

    const/16 v1, 0xdb

    invoke-static {p1, p2, v0, v1}, Lfsx;->a(Landroid/content/Context;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    invoke-static {v10, v0}, Lfvl;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 157
    invoke-virtual {v7}, Ljag;->bdT()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 158
    invoke-virtual {p0, p1, v7}, Lfvl;->a(Landroid/content/Context;Ljag;)Lanh;

    move-result-object v0

    invoke-static {v10, v0}, Lfvl;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 161
    :cond_5
    invoke-virtual {v7}, Ljag;->bdR()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 162
    invoke-virtual {p0, p1, v7}, Lfvl;->b(Landroid/content/Context;Ljag;)Lanh;

    move-result-object v0

    invoke-static {v10, v0}, Lfvl;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 166
    :cond_6
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    return-object v0

    :cond_7
    move-object v1, v0

    move-object v0, v7

    goto :goto_3
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0, p1, p2}, Lfvl;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 94
    iget-object v0, v0, Lang;->ags:[Lanh;

    return-object v0
.end method
