.class Lijo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final dDc:[Ljava/lang/Object;

.field private final dDd:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Lijm;)V
    .locals 5

    .prologue
    .line 405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    invoke-virtual {p1}, Lijm;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lijo;->dDc:[Ljava/lang/Object;

    .line 407
    invoke-virtual {p1}, Lijm;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lijo;->dDd:[Ljava/lang/Object;

    .line 408
    const/4 v0, 0x0

    .line 409
    invoke-virtual {p1}, Lijm;->aWK()Lijp;

    move-result-object v1

    invoke-virtual {v1}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 410
    iget-object v3, p0, Lijo;->dDc:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 411
    iget-object v3, p0, Lijo;->dDd:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v1

    .line 412
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 413
    goto :goto_0

    .line 414
    :cond_0
    return-void
.end method


# virtual methods
.method final a(Lijn;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 420
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lijo;->dDc:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 421
    iget-object v1, p0, Lijo;->dDc:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, Lijo;->dDd:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    .line 420
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 423
    :cond_0
    invoke-virtual {p1}, Lijn;->aXa()Lijm;

    move-result-object v0

    return-object v0
.end method

.method readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 416
    new-instance v0, Lijn;

    invoke-direct {v0}, Lijn;-><init>()V

    .line 417
    invoke-virtual {p0, v0}, Lijo;->a(Lijn;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
