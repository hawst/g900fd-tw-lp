.class public final Lbcg;
.super Ljava/lang/Object;


# instance fields
.field public awA:Z

.field public awB:I

.field public awC:Z

.field public awD:Ljava/lang/String;

.field private final awE:Ljava/util/List;

.field public awz:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbcg;->mName:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lbcg;->awB:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbcg;->awE:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/appdatasearch/Feature;)Lbcg;
    .locals 1

    iget-object v0, p0, Lbcg;->awE:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/Feature;)V

    return-object p0
.end method

.method public final xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 10

    const/4 v8, 0x0

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v1, p0, Lbcg;->mName:Ljava/lang/String;

    iget-object v2, p0, Lbcg;->awz:Ljava/lang/String;

    iget-boolean v3, p0, Lbcg;->awA:Z

    iget v4, p0, Lbcg;->awB:I

    iget-boolean v5, p0, Lbcg;->awC:Z

    iget-object v6, p0, Lbcg;->awD:Ljava/lang/String;

    iget-object v7, p0, Lbcg;->awE:Ljava/util/List;

    iget-object v9, p0, Lbcg;->awE:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-interface {v7, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/android/gms/appdatasearch/Feature;

    move-object v9, v8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;[ILjava/lang/String;)V

    return-object v0
.end method
