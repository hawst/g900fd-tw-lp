.class public final Lfwn;
.super Lfus;
.source "PG"


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 33
    return-void
.end method

.method private static a(Ljge;Ljgf;Z)Laoq;
    .locals 2
    .param p1    # Ljgf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 110
    new-instance v0, Laoq;

    invoke-direct {v0}, Laoq;-><init>()V

    .line 111
    invoke-virtual {p0}, Ljge;->oK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {p0}, Ljge;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->cj(Ljava/lang/String;)Laoq;

    .line 114
    :cond_0
    iget-object v1, p0, Ljge;->dMm:Ljcn;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljge;->dMm:Ljcn;

    invoke-virtual {v1}, Ljcn;->qG()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Ljge;->dMm:Ljcn;

    invoke-virtual {v1}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->ch(Ljava/lang/String;)Laoq;

    .line 117
    :cond_1
    if-nez p2, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljgf;->bjV()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 118
    invoke-virtual {p1}, Ljgf;->bjU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->ck(Ljava/lang/String;)Laoq;

    .line 120
    :cond_2
    return-object v0
.end method

.method private static a(Lixa;Laoj;)V
    .locals 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lixa;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Laoj;->bT(Ljava/lang/String;)Laoj;

    .line 103
    new-instance v0, Lfsa;

    const/16 v1, 0xc9

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    invoke-virtual {p0}, Lixa;->getUrl()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, p1, Laoj;->ajx:Lani;

    .line 106
    return-void
.end method


# virtual methods
.method public final A(Lizj;)Ljau;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p1, Lizj;->dSl:Ljau;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 7

    .prologue
    .line 43
    iget-object v0, p2, Lizj;->dSm:Ljgg;

    .line 44
    iget-object v1, v0, Ljgg;->ejy:Ljgh;

    .line 46
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 48
    const/16 v3, 0x1a

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 49
    iput-object p2, v2, Lanh;->ahu:Lizj;

    .line 51
    new-instance v3, Laop;

    invoke-direct {v3}, Laop;-><init>()V

    .line 52
    invoke-virtual {v0}, Ljgg;->bjX()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 53
    invoke-virtual {v0}, Ljgg;->bjW()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Laop;->cf(Ljava/lang/String;)Laop;

    .line 55
    :cond_0
    invoke-virtual {v1}, Ljgh;->bka()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 56
    invoke-virtual {v1}, Ljgh;->qX()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Laop;->cg(Ljava/lang/String;)Laop;

    .line 58
    :cond_1
    iget-object v4, v1, Ljgh;->ejD:Ljge;

    iget-object v5, v1, Ljgh;->ejF:Ljgf;

    invoke-virtual {v0}, Ljgg;->bjY()Z

    move-result v6

    invoke-static {v4, v5, v6}, Lfwn;->a(Ljge;Ljgf;Z)Laoq;

    move-result-object v4

    iput-object v4, v3, Laop;->ajP:Laoq;

    .line 61
    iget-object v4, v1, Ljgh;->ejE:Ljge;

    iget-object v1, v1, Ljgh;->ejG:Ljgf;

    invoke-virtual {v0}, Ljgg;->bjY()Z

    move-result v0

    invoke-static {v4, v1, v0}, Lfwn;->a(Ljge;Ljgf;Z)Laoq;

    move-result-object v0

    iput-object v0, v3, Laop;->ajQ:Laoq;

    .line 67
    iput-object v3, v2, Lanh;->agK:Laop;

    .line 69
    iget-object v0, p2, Lizj;->dUp:Lixx;

    if-eqz v0, :cond_2

    .line 70
    new-instance v0, Lfsa;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    iget-object v1, p2, Lizj;->dUp:Lixx;

    invoke-virtual {v0, v1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v2, Lanh;->ahs:Lani;

    .line 74
    :cond_2
    return-object v2
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUZ:Lizj;

    iget-object v1, v0, Lizj;->dSl:Ljau;

    .line 90
    iget-object v0, v1, Ljau;->dVf:Lixa;

    if-nez v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 98
    :goto_0
    return-object v0

    .line 93
    :cond_0
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 94
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 95
    iget-object v2, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v2, v2, Lizq;->dUZ:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 96
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 97
    iget-object v1, v1, Ljau;->dVf:Lixa;

    iget-object v2, v0, Lanh;->agz:Laoj;

    invoke-static {v1, v2}, Lfwn;->a(Lixa;Laoj;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/util/List;)Lanh;
    .locals 3

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lfus;->b(Landroid/content/Context;Ljava/util/List;)Lanh;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v1, v1, Lizq;->dUZ:Lizj;

    iget-object v1, v1, Lizj;->dSl:Ljau;

    .line 81
    iget-object v2, v1, Ljau;->dVf:Lixa;

    if-eqz v2, :cond_0

    .line 82
    iget-object v1, v1, Ljau;->dVf:Lixa;

    iget-object v2, v0, Lanh;->agz:Laoj;

    invoke-static {v1, v2}, Lfwn;->a(Lixa;Laoj;)V

    .line 84
    :cond_0
    return-object v0
.end method
