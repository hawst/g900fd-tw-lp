.class public final Ljsm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected final eCr:Ljava/lang/Class;

.field private eCs:Z

.field protected final tag:I

.field private type:I


# direct methods
.method private constructor <init>(ILjava/lang/Class;IZ)V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput p1, p0, Ljsm;->type:I

    .line 150
    iput-object p2, p0, Ljsm;->eCr:Ljava/lang/Class;

    .line 151
    iput p3, p0, Ljsm;->tag:I

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljsm;->eCs:Z

    .line 153
    return-void
.end method

.method public static a(ILjava/lang/Class;I)Ljsm;
    .locals 3

    .prologue
    .line 85
    new-instance v0, Ljsm;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Ljsm;-><init>(ILjava/lang/Class;IZ)V

    return-object v0
.end method

.method private b(Ljsi;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 203
    iget-boolean v0, p0, Ljsm;->eCs:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljsm;->eCr:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    move-object v1, v0

    .line 205
    :goto_0
    :try_start_0
    iget v0, p0, Ljsm;->type:I

    packed-switch v0, :pswitch_data_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Ljsm;->type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 217
    :catch_0
    move-exception v0

    .line 218
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error creating instance of class "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 203
    :cond_0
    iget-object v0, p0, Ljsm;->eCr:Ljava/lang/Class;

    move-object v1, v0

    goto :goto_0

    .line 207
    :pswitch_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 208
    iget v2, p0, Ljsm;->tag:I

    invoke-static {v2}, Ljsu;->sl(I)I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljsi;->a(Ljsr;I)V

    .line 213
    :goto_1
    return-object v0

    .line 211
    :pswitch_1
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 212
    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 220
    :catch_1
    move-exception v0

    .line 221
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error creating instance of class "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 223
    :catch_2
    move-exception v0

    .line 224
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Error reading extension field"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 205
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljava/lang/Object;Ljsj;)V
    .locals 3

    .prologue
    .line 244
    :try_start_0
    iget v0, p0, Ljsm;->tag:I

    invoke-virtual {p2, v0}, Ljsj;->sd(I)V

    .line 245
    iget v0, p0, Ljsm;->type:I

    packed-switch v0, :pswitch_data_0

    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ljsm;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :catch_0
    move-exception v0

    .line 262
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 247
    :pswitch_0
    :try_start_1
    check-cast p1, Ljsr;

    .line 248
    iget v0, p0, Ljsm;->tag:I

    invoke-static {v0}, Ljsu;->sl(I)I

    move-result v0

    .line 249
    invoke-virtual {p1, p2}, Ljsr;->a(Ljsj;)V

    .line 251
    const/4 v1, 0x4

    invoke-virtual {p2, v0, v1}, Ljsj;->by(II)V

    .line 264
    :goto_0
    return-void

    .line 254
    :pswitch_1
    check-cast p1, Ljsr;

    .line 255
    invoke-virtual {p2, p1}, Ljsj;->l(Ljsr;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bE(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 300
    iget v0, p0, Ljsm;->tag:I

    invoke-static {v0}, Ljsu;->sl(I)I

    move-result v0

    .line 301
    iget v1, p0, Ljsm;->type:I

    packed-switch v1, :pswitch_data_0

    .line 309
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ljsm;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :pswitch_0
    check-cast p1, Ljsr;

    .line 304
    invoke-static {v0, p1}, Ljsj;->b(ILjsr;)I

    move-result v0

    .line 307
    :goto_0
    return v0

    .line 306
    :pswitch_1
    check-cast p1, Ljsr;

    .line 307
    invoke-static {v0, p1}, Ljsj;->c(ILjsr;)I

    move-result v0

    goto :goto_0

    .line 301
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method final a(Ljava/lang/Object;Ljsj;)V
    .locals 3

    .prologue
    .line 234
    iget-boolean v0, p0, Ljsm;->eCs:Z

    if-eqz v0, :cond_1

    .line 235
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v2, p2}, Ljsm;->b(Ljava/lang/Object;Ljsj;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_1
    invoke-direct {p0, p1, p2}, Ljsm;->b(Ljava/lang/Object;Ljsj;)V

    .line 239
    :cond_2
    return-void
.end method

.method final aE(Ljava/util/List;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 164
    if-nez p1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-object v3

    :cond_1
    iget-boolean v0, p0, Ljsm;->eCs:Z

    if-eqz v0, :cond_5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljst;

    iget-object v5, v0, Ljst;->aHs:[B

    array-length v5, v5

    if-eqz v5, :cond_2

    iget-object v0, v0, Ljst;->aHs:[B

    array-length v5, v0

    invoke-static {v0, v2, v5}, Ljsi;->l([BII)Ljsi;

    move-result-object v0

    invoke-direct {p0, v0}, Ljsm;->b(Ljsi;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v3

    :goto_2
    move-object v3, v0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Ljsm;->eCr:Ljava/lang/Class;

    iget-object v3, p0, Ljsm;->eCr:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    :goto_3
    if-ge v2, v0, :cond_0

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v3, v2, v1}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljst;

    iget-object v1, p0, Ljsm;->eCr:Ljava/lang/Class;

    iget-object v0, v0, Ljst;->aHs:[B

    array-length v3, v0

    invoke-static {v0, v2, v3}, Ljsi;->l([BII)Ljsi;

    move-result-object v0

    invoke-direct {p0, v0}, Ljsm;->b(Ljsi;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2
.end method

.method final bD(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 278
    iget-boolean v1, p0, Ljsm;->eCs:Z

    if-eqz v1, :cond_1

    .line 279
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-static {p1, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p1, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v3}, Ljsm;->bE(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 281
    :cond_1
    invoke-direct {p0, p1}, Ljsm;->bE(Ljava/lang/Object;)I

    move-result v0

    :cond_2
    return v0
.end method
