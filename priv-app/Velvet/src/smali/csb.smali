.class public final Lcsb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public offset:I

.field public size:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput p1, p0, Lcsb;->offset:I

    .line 166
    iput p2, p0, Lcsb;->size:I

    .line 167
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 171
    instance-of v1, p1, Lcsb;

    if-nez v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 174
    :cond_1
    check-cast p1, Lcsb;

    .line 175
    iget v1, p0, Lcsb;->offset:I

    iget v2, p1, Lcsb;->offset:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcsb;->size:I

    iget v2, p1, Lcsb;->size:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 180
    iget v0, p0, Lcsb;->offset:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcsb;->size:I

    add-int/2addr v0, v1

    return v0
.end method
