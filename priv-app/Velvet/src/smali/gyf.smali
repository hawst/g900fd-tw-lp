.class public final Lgyf;
.super Lgyi;
.source "PG"


# instance fields
.field private final cZi:Lhai;


# direct methods
.method public constructor <init>(Lerp;Lcjs;Lcpd;Lcom/google/android/velvet/ui/MainContentView;Lfmv;Lcin;Lewd;Lfml;Lfnn;)V
    .locals 9

    .prologue
    .line 39
    const-string v1, "suggest"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Lgyi;-><init>(Ljava/lang/String;Lerp;Lcom/google/android/velvet/ui/MainContentView;Lfmv;Lcin;Lewd;Lfml;Lfnn;)V

    .line 41
    new-instance v0, Lhai;

    invoke-direct {v0, p3, p2, p0}, Lhai;-><init>(Lcpd;Lcjs;Lgxi;)V

    iput-object v0, p0, Lgyf;->cZi:Lhai;

    .line 44
    return-void
.end method


# virtual methods
.method protected final X(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lgyi;->X(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lgyf;->cZi:Lhai;

    invoke-virtual {v0}, Lhai;->aMO()V

    .line 72
    return-void
.end method

.method public final a(Lejw;)V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0, p1}, Lgyi;->a(Lejw;)V

    .line 86
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 87
    iget-object v2, p0, Lgyf;->cZi:Lhai;

    invoke-virtual {v2, v0}, Lhai;->bG(Landroid/view/View;)V

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public final a(Lgyv;Lgyt;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 48
    invoke-virtual {p0}, Lgyf;->Nf()Ldda;

    move-result-object v1

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v2

    .line 53
    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v1

    .line 54
    invoke-virtual {p1, p2, v2, v1}, Lgyv;->a(Lgyt;ZZ)Z

    move-result v1

    .line 56
    if-nez v1, :cond_0

    .line 57
    invoke-virtual {p0, v0}, Lgyf;->kH(I)V

    .line 60
    :cond_0
    iget-object v3, p0, Lgyf;->cZi:Lhai;

    if-nez v1, :cond_2

    :goto_0
    invoke-virtual {v3, p1, p2, v2, v0}, Lhai;->a(Lgyv;Lgyt;ZZ)V

    .line 63
    if-eqz v1, :cond_1

    .line 64
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lgyf;->kH(I)V

    .line 66
    :cond_1
    return-void

    .line 60
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aKF()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lgyi;->aKF()V

    .line 78
    sget-object v0, Lgxi;->cYu:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 80
    iget-object v0, p0, Lgyf;->cZi:Lhai;

    invoke-virtual {v0}, Lhai;->aKF()V

    .line 81
    return-void
.end method

.method protected final bD(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 93
    invoke-super {p0, p1}, Lgyi;->bD(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgyf;->cZi:Lhai;

    invoke-virtual {v0, p1}, Lhai;->bH(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
