.class public final Ljco;
.super Ljsl;
.source "PG"


# instance fields
.field private aeB:Ljbp;

.field private aez:I

.field private eaR:I

.field public eaS:[Ljbb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59777
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 59778
    iput v0, p0, Ljco;->aez:I

    iput-object v1, p0, Ljco;->aeB:Ljbp;

    iput v0, p0, Ljco;->eaR:I

    invoke-static {}, Ljbb;->beG()[Ljbb;

    move-result-object v0

    iput-object v0, p0, Ljco;->eaS:[Ljbb;

    iput-object v1, p0, Ljco;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljco;->eCz:I

    .line 59779
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59733
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljco;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljco;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljco;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljco;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljco;->eaR:I

    iget v0, p0, Ljco;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljco;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljco;->eaS:[Ljbb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbb;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljco;->eaS:[Ljbb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljbb;

    invoke-direct {v3}, Ljbb;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljco;->eaS:[Ljbb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljbb;

    invoke-direct {v3}, Ljbb;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljco;->eaS:[Ljbb;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 59794
    iget-object v0, p0, Ljco;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 59795
    const/4 v0, 0x1

    iget-object v1, p0, Ljco;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59797
    :cond_0
    iget v0, p0, Ljco;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 59798
    const/4 v0, 0x2

    iget v1, p0, Ljco;->eaR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 59800
    :cond_1
    iget-object v0, p0, Ljco;->eaS:[Ljbb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljco;->eaS:[Ljbb;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 59801
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljco;->eaS:[Ljbb;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 59802
    iget-object v1, p0, Ljco;->eaS:[Ljbb;

    aget-object v1, v1, v0

    .line 59803
    if-eqz v1, :cond_2

    .line 59804
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 59801
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59808
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 59809
    return-void
.end method

.method public final bgM()I
    .locals 1

    .prologue
    .line 59758
    iget v0, p0, Ljco;->eaR:I

    return v0
.end method

.method public final bgN()Z
    .locals 1

    .prologue
    .line 59766
    iget v0, p0, Ljco;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 59813
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 59814
    iget-object v1, p0, Ljco;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 59815
    const/4 v1, 0x1

    iget-object v2, p0, Ljco;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59818
    :cond_0
    iget v1, p0, Ljco;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 59819
    const/4 v1, 0x2

    iget v2, p0, Ljco;->eaR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 59822
    :cond_1
    iget-object v1, p0, Ljco;->eaS:[Ljbb;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljco;->eaS:[Ljbb;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 59823
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljco;->eaS:[Ljbb;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 59824
    iget-object v2, p0, Ljco;->eaS:[Ljbb;

    aget-object v2, v2, v0

    .line 59825
    if-eqz v2, :cond_2

    .line 59826
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 59823
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 59831
    :cond_4
    return v0
.end method

.method public final oI(I)Ljco;
    .locals 1

    .prologue
    .line 59761
    iput p1, p0, Ljco;->eaR:I

    .line 59762
    iget v0, p0, Ljco;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljco;->aez:I

    .line 59763
    return-object p0
.end method
