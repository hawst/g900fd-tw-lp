.class public final Lcej;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final aTt:[B


# instance fields
.field private final aTu:Lbzh;

.field private final aTv:Ljava/lang/String;

.field private aTw:Z

.field private final aTx:Ljava/nio/ByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcej;->aTt:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lbzh;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/16 v0, 0x400

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    .line 32
    iput-object p1, p0, Lcej;->aTu:Lbzh;

    .line 33
    iput-object p2, p0, Lcej;->aTv:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private b(Ljwv;ZZ)V
    .locals 7

    .prologue
    .line 63
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v6

    .line 64
    iget-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    array-length v1, v6

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 68
    :try_start_0
    iget-object v0, p0, Lcej;->aTu:Lbzh;

    iget-object v1, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lbzh;->a([BIIZZ)V

    .line 69
    iget-object v0, p0, Lcej;->aTu:Lbzh;

    const/4 v2, 0x0

    array-length v3, v6

    move-object v1, v6

    move v4, p2

    move v5, p3

    invoke-interface/range {v0 .. v5}, Lbzh;->a([BIIZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    iget-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    throw v0
.end method


# virtual methods
.method public final a(Ljwv;ZZ)V
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcej;->aTw:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 54
    iget-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcej;->b(Ljwv;ZZ)V

    .line 56
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljwv;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    iget-boolean v0, p0, Lcej;->aTw:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 38
    iget-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 42
    iget-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    sget-object v3, Lcej;->aTt:[B

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 43
    iget-object v0, p0, Lcej;->aTx:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcej;->aTv:Ljava/lang/String;

    const-string v4, "_"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lgnn;->M(Ljava/lang/CharSequence;)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 47
    invoke-direct {p0, p1, v2, v2}, Lcej;->b(Ljwv;ZZ)V

    .line 49
    iput-boolean v1, p0, Lcej;->aTw:Z

    .line 50
    return-void

    :cond_0
    move v0, v2

    .line 37
    goto :goto_0

    :cond_1
    move v0, v2

    .line 38
    goto :goto_1
.end method
