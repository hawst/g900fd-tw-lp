.class public final Liaw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private dwT:Ljava/lang/String;

.field private dwU:Ljava/util/List;

.field private dwV:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Liaw;->dwV:Ljava/util/Map;

    .line 86
    return-void
.end method

.method private az(Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 70
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 71
    invoke-static {v1}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 72
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 73
    iget-object v3, p0, Liaw;->dwV:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-object v2
.end method

.method public static b(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 2

    .prologue
    .line 283
    new-instance v0, Liaw;

    invoke-direct {v0, p2}, Liaw;-><init>(Ljava/util/Map;)V

    .line 284
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v1

    .line 285
    invoke-virtual {v0, p0, v1}, Liaw;->h(Ljava/lang/String;Ljava/util/List;)V

    .line 286
    invoke-static {p1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 287
    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 288
    return-object v1
.end method

.method private oU(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Liaw;->dwV:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 50
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private oV(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Liaw;->dwV:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 61
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcsi;->SE()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final aA(Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 240
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 241
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 242
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 243
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    invoke-direct {p0, v0}, Liaw;->oU(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 245
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 248
    :cond_1
    return-object v2
.end method

.method public final aB(Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 259
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 260
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 261
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 262
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 263
    invoke-direct {p0, v0}, Liaw;->oV(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 264
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 267
    :cond_1
    return-object v2
.end method

.method public final aUH()Ljava/util/List;
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Liaw;->dwT:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liaw;->dwU:Ljava/util/List;

    if-nez v0, :cond_1

    .line 123
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The spoken and additiona languages must be set before building the suggestions list."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_1
    iget-object v0, p0, Liaw;->dwT:Ljava/lang/String;

    invoke-direct {p0, v0}, Liaw;->oV(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    .line 132
    :cond_2
    iget-object v0, p0, Liaw;->dwU:Ljava/util/List;

    invoke-virtual {p0, v0}, Liaw;->aA(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 133
    invoke-virtual {p0, v0}, Liaw;->aB(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 135
    iget-object v2, p0, Liaw;->dwT:Ljava/lang/String;

    invoke-direct {p0, v2}, Liaw;->oU(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 138
    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 139
    invoke-direct {p0, v1}, Liaw;->az(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 143
    :cond_3
    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 144
    invoke-direct {p0, v0}, Liaw;->az(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final bb(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    .prologue
    const/16 v5, 0x2d

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 205
    iget-object v3, p0, Liaw;->dwT:Ljava/lang/String;

    iget-object v4, p0, Liaw;->dwT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 206
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    .line 207
    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 208
    if-nez v4, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    if-eqz v4, :cond_1

    if-nez v3, :cond_4

    .line 209
    :cond_1
    if-eqz v4, :cond_3

    move v0, v1

    .line 229
    :cond_2
    :goto_0
    return v0

    .line 211
    :cond_3
    if-eqz v3, :cond_4

    move v0, v2

    .line 212
    goto :goto_0

    .line 217
    :cond_4
    iget-object v3, p0, Liaw;->dwT:Ljava/lang/String;

    iget-object v4, p0, Liaw;->dwT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 218
    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    .line 219
    invoke-virtual {p2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    .line 220
    if-nez v4, :cond_5

    if-eqz v3, :cond_2

    :cond_5
    if-eqz v4, :cond_6

    if-nez v3, :cond_2

    .line 221
    :cond_6
    if-eqz v4, :cond_7

    move v0, v1

    .line 222
    goto :goto_0

    .line 223
    :cond_7
    if-eqz v3, :cond_2

    move v0, v2

    .line 224
    goto :goto_0
.end method

.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 37
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Liaw;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 153
    invoke-direct {p0, p1}, Liaw;->oV(Ljava/lang/String;)Z

    move-result v2

    .line 154
    invoke-direct {p0, p2}, Liaw;->oV(Ljava/lang/String;)Z

    move-result v3

    .line 155
    if-eq v2, v3, :cond_2

    .line 156
    if-eqz v2, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 156
    goto :goto_0

    .line 160
    :cond_2
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    .line 161
    invoke-virtual {p0, p1, p2}, Liaw;->bb(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 165
    :cond_3
    invoke-direct {p0, p1}, Liaw;->oU(Ljava/lang/String;)Z

    move-result v2

    .line 166
    invoke-direct {p0, p2}, Liaw;->oU(Ljava/lang/String;)Z

    move-result v3

    .line 167
    if-eq v2, v3, :cond_4

    .line 168
    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 172
    :cond_4
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    .line 173
    invoke-virtual {p0, p1, p2}, Liaw;->bb(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 177
    :cond_5
    invoke-virtual {p0, p1, p2}, Liaw;->bb(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 100
    iput-object p1, p0, Liaw;->dwT:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Liaw;->dwU:Ljava/util/List;

    .line 102
    return-void
.end method
