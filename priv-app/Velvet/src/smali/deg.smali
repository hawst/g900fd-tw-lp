.class public final Ldeg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bvi:Ljava/util/Comparator;

.field private static final bvj:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ldeh;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldeh;-><init>(Z)V

    sput-object v0, Ldeg;->bvi:Ljava/util/Comparator;

    .line 25
    new-instance v0, Ldeh;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldeh;-><init>(Z)V

    sput-object v0, Ldeg;->bvj:Ljava/util/Comparator;

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;Z)Ldef;
    .locals 2

    .prologue
    .line 37
    if-eqz p3, :cond_1

    sget-object v0, Ldeg;->bvi:Ljava/util/Comparator;

    :goto_0
    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 41
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 42
    const-string v0, "Search.SuggestionListFactory"

    const-string v1, "null in Suggestion list"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :cond_0
    new-instance v0, Ldei;

    invoke-direct {v0, p0, p1, p2}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    return-object v0

    .line 37
    :cond_1
    sget-object v0, Ldeg;->bvj:Ljava/util/Comparator;

    goto :goto_0
.end method
