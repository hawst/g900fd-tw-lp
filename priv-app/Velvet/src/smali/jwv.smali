.class public final Ljwv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private alY:Ljava/lang/String;

.field private eJJ:Z

.field private eJK:Z

.field private eJL:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljwv;

    const v2, 0xe2d6eea

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 108
    iput v1, p0, Ljwv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljwv;->alY:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwv;->eJJ:Z

    iput-boolean v1, p0, Ljwv;->eJK:Z

    iput-boolean v1, p0, Ljwv;->eJL:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljwv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwv;->eCz:I

    .line 109
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwv;->alY:Ljava/lang/String;

    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwv;->eJJ:Z

    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwv;->eJL:Z

    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwv;->eJK:Z

    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwv;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Ljwv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x1

    iget-object v1, p0, Ljwv;->alY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 128
    :cond_0
    iget v0, p0, Ljwv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 129
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljwv;->eJJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 131
    :cond_1
    iget v0, p0, Ljwv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 132
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljwv;->eJL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 134
    :cond_2
    iget v0, p0, Ljwv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 135
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljwv;->eJK:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 137
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 138
    return-void
.end method

.method public final bvA()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Ljwv;->eJK:Z

    return v0
.end method

.method public final bvB()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Ljwv;->eJL:Z

    return v0
.end method

.method public final bvC()Z
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Ljwv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jj(Z)Ljwv;
    .locals 1

    .prologue
    .line 56
    iput-boolean p1, p0, Ljwv;->eJJ:Z

    .line 57
    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwv;->aez:I

    .line 58
    return-object p0
.end method

.method public final jk(Z)Ljwv;
    .locals 1

    .prologue
    .line 75
    iput-boolean p1, p0, Ljwv;->eJK:Z

    .line 76
    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwv;->aez:I

    .line 77
    return-object p0
.end method

.method public final jl(Z)Ljwv;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwv;->eJL:Z

    .line 95
    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwv;->aez:I

    .line 96
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 142
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 143
    iget v1, p0, Ljwv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 144
    const/4 v1, 0x1

    iget-object v2, p0, Ljwv;->alY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_0
    iget v1, p0, Ljwv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 148
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljwv;->eJJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 151
    :cond_1
    iget v1, p0, Ljwv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 152
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljwv;->eJL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 155
    :cond_2
    iget v1, p0, Ljwv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 156
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljwv;->eJK:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 159
    :cond_3
    return v0
.end method

.method public final tF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ljwv;->alY:Ljava/lang/String;

    return-object v0
.end method

.method public final tG()Z
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Ljwv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final zH(Ljava/lang/String;)Ljwv;
    .locals 1

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Ljwv;->alY:Ljava/lang/String;

    .line 38
    iget v0, p0, Ljwv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwv;->aez:I

    .line 39
    return-object p0
.end method
