.class public final Lkcl;
.super Lkcc;
.source "PG"


# instance fields
.field private eRS:Ljavax/net/ssl/SSLSocket;


# direct methods
.method public constructor <init>(Lkas;Lkcp;Ljava/lang/String;Lkcq;Lkac;Lkcw;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct/range {p0 .. p6}, Lkcc;-><init>(Lkas;Lkcp;Ljava/lang/String;Lkcq;Lkac;Lkcw;)V

    .line 41
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Lkac;->getSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    :goto_0
    iput-object v0, p0, Lkcl;->eRS:Ljavax/net/ssl/SSLSocket;

    .line 42
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/net/CacheResponse;)Z
    .locals 1

    .prologue
    .line 50
    instance-of v0, p1, Ljava/net/SecureCacheResponse;

    return v0
.end method

.method protected final byB()Lkay;
    .locals 5

    .prologue
    .line 63
    iget-object v0, p0, Lkcl;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->aen:Ljava/lang/String;

    .line 64
    if-nez v0, :cond_0

    .line 65
    invoke-static {}, Lkcl;->byz()Ljava/lang/String;

    move-result-object v0

    .line 68
    :cond_0
    iget-object v1, p0, Lkcl;->eRc:Lkcp;

    invoke-interface {v1}, Lkcp;->getURL()Ljava/net/URL;

    move-result-object v1

    .line 69
    new-instance v2, Lkay;

    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lkbt;->d(Ljava/net/URL;)I

    move-result v1

    iget-object v4, p0, Lkcl;->eRp:Lkcs;

    iget-object v4, v4, Lkcs;->eQa:Ljava/lang/String;

    invoke-direct {v2, v3, v1, v0, v4}, Lkay;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public final byN()Ljavax/net/ssl/SSLSocket;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkcl;->eRS:Ljavax/net/ssl/SSLSocket;

    return-object v0
.end method

.method protected final byy()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method protected final c(Lkac;)V
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p1}, Lkac;->getSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iput-object v0, p0, Lkcl;->eRS:Ljavax/net/ssl/SSLSocket;

    .line 46
    invoke-super {p0, p1}, Lkcc;->c(Lkac;)V

    .line 47
    return-void
.end method
