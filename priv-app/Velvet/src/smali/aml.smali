.class public final Laml;
.super Ljsl;
.source "PG"


# instance fields
.field public aeV:[Lamk;

.field public aeW:[Lamm;

.field private aeX:Z

.field private aez:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1285
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1286
    iput v1, p0, Laml;->aez:I

    invoke-static {}, Lamk;->mV()[Lamk;

    move-result-object v0

    iput-object v0, p0, Laml;->aeV:[Lamk;

    invoke-static {}, Lamm;->na()[Lamm;

    move-result-object v0

    iput-object v0, p0, Laml;->aeW:[Lamm;

    iput-boolean v1, p0, Laml;->aeX:Z

    const/4 v0, 0x0

    iput-object v0, p0, Laml;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laml;->eCz:I

    .line 1287
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1241
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laml;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Laml;->aeV:[Lamk;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lamk;

    if-eqz v0, :cond_1

    iget-object v3, p0, Laml;->aeV:[Lamk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lamk;

    invoke-direct {v3}, Lamk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Laml;->aeV:[Lamk;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lamk;

    invoke-direct {v3}, Lamk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Laml;->aeV:[Lamk;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Laml;->aeW:[Lamm;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lamm;

    if-eqz v0, :cond_4

    iget-object v3, p0, Laml;->aeW:[Lamm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lamm;

    invoke-direct {v3}, Lamm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Laml;->aeW:[Lamm;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lamm;

    invoke-direct {v3}, Lamm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Laml;->aeW:[Lamm;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Laml;->aeX:Z

    iget v0, p0, Laml;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laml;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1302
    iget-object v0, p0, Laml;->aeV:[Lamk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laml;->aeV:[Lamk;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 1303
    :goto_0
    iget-object v2, p0, Laml;->aeV:[Lamk;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1304
    iget-object v2, p0, Laml;->aeV:[Lamk;

    aget-object v2, v2, v0

    .line 1305
    if-eqz v2, :cond_0

    .line 1306
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1303
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1310
    :cond_1
    iget-object v0, p0, Laml;->aeW:[Lamm;

    if-eqz v0, :cond_3

    iget-object v0, p0, Laml;->aeW:[Lamm;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1311
    :goto_1
    iget-object v0, p0, Laml;->aeW:[Lamm;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 1312
    iget-object v0, p0, Laml;->aeW:[Lamm;

    aget-object v0, v0, v1

    .line 1313
    if-eqz v0, :cond_2

    .line 1314
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 1311
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1318
    :cond_3
    iget v0, p0, Laml;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 1319
    const/4 v0, 0x3

    iget-boolean v1, p0, Laml;->aeX:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1321
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1322
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1326
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1327
    iget-object v2, p0, Laml;->aeV:[Lamk;

    if-eqz v2, :cond_2

    iget-object v2, p0, Laml;->aeV:[Lamk;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 1328
    :goto_0
    iget-object v3, p0, Laml;->aeV:[Lamk;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1329
    iget-object v3, p0, Laml;->aeV:[Lamk;

    aget-object v3, v3, v0

    .line 1330
    if-eqz v3, :cond_0

    .line 1331
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1328
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1336
    :cond_2
    iget-object v2, p0, Laml;->aeW:[Lamm;

    if-eqz v2, :cond_4

    iget-object v2, p0, Laml;->aeW:[Lamm;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 1337
    :goto_1
    iget-object v2, p0, Laml;->aeW:[Lamm;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 1338
    iget-object v2, p0, Laml;->aeW:[Lamm;

    aget-object v2, v2, v1

    .line 1339
    if-eqz v2, :cond_3

    .line 1340
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1337
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1345
    :cond_4
    iget v1, p0, Laml;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    .line 1346
    const/4 v1, 0x3

    iget-boolean v2, p0, Laml;->aeX:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1349
    :cond_5
    return v0
.end method

.method public final mZ()Z
    .locals 1

    .prologue
    .line 1269
    iget-boolean v0, p0, Laml;->aeX:Z

    return v0
.end method
