.class public final Lhmd;
.super Lhlu;
.source "PG"


# direct methods
.method protected constructor <init>(Leoj;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 18
    return-void
.end method

.method private static getIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "http://www.google.com"

    invoke-static {v2}, Lhmd;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/search/shared/actions/OpenUrlAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/OpenUrlAction;->ahL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lhmd;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/search/shared/actions/OpenUrlAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/OpenUrlAction;->ahL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lhmd;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method
