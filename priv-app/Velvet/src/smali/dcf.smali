.class public Ldcf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bro:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    return-void
.end method

.method private declared-synchronized d(Lcom/google/android/velvet/ActionData;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 143
    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 145
    or-int v2, v0, p2

    .line 146
    if-eq v2, v0, :cond_1

    .line 147
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 154
    :goto_0
    monitor-exit p0

    return v0

    .line 151
    :cond_0
    :try_start_1
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 152
    goto :goto_0

    .line 154
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/velvet/ActionData;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    invoke-virtual {p1}, Lcom/google/android/velvet/ActionData;->aIJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    and-int/lit16 v0, p2, 0xfff

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 60
    and-int/lit16 v0, p2, -0x1000

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 73
    invoke-direct {p0, p1, p2}, Ldcf;->d(Lcom/google/android/velvet/ActionData;I)Z

    .line 75
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 59
    goto :goto_0

    :cond_2
    move v1, v2

    .line 60
    goto :goto_1
.end method

.method public final b(Lcom/google/android/velvet/ActionData;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    invoke-virtual {p1}, Lcom/google/android/velvet/ActionData;->aIJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    and-int/lit16 v0, p2, 0xfff

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 86
    and-int/lit16 v0, p2, -0x1000

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 92
    and-int/lit16 v0, p2, 0xfff

    shl-int/lit8 v0, v0, 0x10

    invoke-direct {p0, p1, v0}, Ldcf;->d(Lcom/google/android/velvet/ActionData;I)Z

    .line 94
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 85
    goto :goto_0

    :cond_2
    move v1, v2

    .line 86
    goto :goto_1
.end method

.method public final c(Lcom/google/android/velvet/ActionData;I)V
    .locals 1

    .prologue
    .line 118
    const/16 v0, 0x1000

    if-ne p2, v0, :cond_0

    .line 119
    invoke-direct {p0, p1, p2}, Ldcf;->d(Lcom/google/android/velvet/ActionData;I)Z

    .line 122
    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/velvet/ActionData;)I
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 102
    ushr-int/lit8 v0, v1, 0x10

    and-int/lit16 v0, v0, 0xfff

    .line 103
    and-int/lit16 v2, v1, 0xfff

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v2

    .line 104
    and-int/lit16 v1, v1, 0xfff

    shl-int/lit8 v1, v1, 0x10

    invoke-direct {p0, p1, v1}, Ldcf;->d(Lcom/google/android/velvet/ActionData;I)Z

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/velvet/ActionData;)I
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Ldcf;->bro:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 131
    ushr-int/lit8 v0, v1, 0x10

    and-int/lit16 v0, v0, 0x1000

    .line 132
    and-int/lit16 v2, v1, 0x1000

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v2

    .line 133
    and-int/lit16 v1, v1, 0x1000

    shl-int/lit8 v1, v1, 0x10

    invoke-direct {p0, p1, v1}, Ldcf;->d(Lcom/google/android/velvet/ActionData;I)Z

    .line 137
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
