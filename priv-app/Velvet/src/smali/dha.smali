.class public Ldha;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final axJ:Lbbt;

.field private final bxG:Z


# direct methods
.method public constructor <init>(Lbbt;Z)V
    .locals 0

    .prologue
    .line 686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 687
    iput-object p1, p0, Ldha;->axJ:Lbbt;

    .line 688
    iput-boolean p2, p0, Ldha;->bxG:Z

    .line 689
    return-void
.end method


# virtual methods
.method public G(J)Lbgm;
    .locals 1

    .prologue
    .line 693
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v0, p1, p2}, Lbbt;->G(J)Lbgm;

    move-result-object v0

    return-object v0
.end method

.method public a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 4

    .prologue
    .line 733
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v0, p1, p2, p3}, Lbbt;->a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 737
    :goto_0
    return-object v0

    .line 734
    :catch_0
    move-exception v0

    .line 735
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling query"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 736
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-eqz v1, :cond_0

    .line 737
    const/4 v0, 0x0

    goto :goto_0

    .line 739
    :cond_0
    throw v0
.end method

.method public a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 4

    .prologue
    .line 761
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v0, p1, p2}, Lbbt;->a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 765
    :goto_0
    return-object v0

    .line 762
    :catch_0
    move-exception v0

    .line 763
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling getPhraseAffinity"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 764
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-eqz v1, :cond_0

    .line 765
    const/4 v0, 0x0

    goto :goto_0

    .line 767
    :cond_0
    throw v0
.end method

.method public a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 4

    .prologue
    .line 720
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbbt;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 724
    :goto_0
    return-object v0

    .line 721
    :catch_0
    move-exception v0

    .line 722
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling queryGlobalSearch"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 723
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-eqz v1, :cond_0

    .line 724
    const/4 v0, 0x0

    goto :goto_0

    .line 726
    :cond_0
    throw v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 6

    .prologue
    .line 747
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lbbt;->a(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 751
    :goto_0
    return-object v0

    .line 748
    :catch_0
    move-exception v0

    .line 749
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling query"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 750
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-eqz v1, :cond_0

    .line 751
    const/4 v0, 0x0

    goto :goto_0

    .line 753
    :cond_0
    throw v0
.end method

.method public a([BZ)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 775
    :try_start_0
    iget-object v1, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v1, p1, p2}, Lbbt;->a([BZ)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 779
    :cond_0
    return v0

    .line 776
    :catch_0
    move-exception v1

    .line 777
    const-string v2, "Search.IcingConnection"

    const-string v3, "Exception when calling setExperimentIds"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 778
    iget-boolean v2, p0, Ldha;->bxG:Z

    if-nez v2, :cond_0

    .line 781
    throw v1
.end method

.method public c(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 708
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, v0, Lbbt;->auz:Lbkm;

    invoke-virtual {v0}, Lbkm;->zb()Lbkn;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lbkn;->c(Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 715
    :cond_0
    :goto_0
    return-void

    .line 708
    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "AppDataSearchClient"

    const-string v2, "Set include in global search failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 709
    :catch_1
    move-exception v0

    .line 710
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling setIncludeInGlobalSearch"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 711
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-nez v1, :cond_0

    .line 712
    throw v0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    iget-object v0, v0, Lbbt;->auz:Lbkm;

    invoke-virtual {v0}, Lbkm;->disconnect()V

    .line 699
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    iget-object v0, v0, Lbbt;->auz:Lbkm;

    invoke-virtual {v0}, Lbkm;->isConnected()Z

    move-result v0

    return v0
.end method

.method public wO()[I
    .locals 4

    .prologue
    .line 788
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v0}, Lbbt;->wO()[I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 792
    :goto_0
    return-object v0

    .line 789
    :catch_0
    move-exception v0

    .line 790
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling getCurrentExperimentIds"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 791
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-eqz v1, :cond_0

    .line 792
    const/4 v0, 0x0

    goto :goto_0

    .line 794
    :cond_0
    throw v0
.end method

.method public wP()[I
    .locals 4

    .prologue
    .line 801
    :try_start_0
    iget-object v0, p0, Ldha;->axJ:Lbbt;

    invoke-virtual {v0}, Lbbt;->wP()[I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 805
    :goto_0
    return-object v0

    .line 802
    :catch_0
    move-exception v0

    .line 803
    const-string v1, "Search.IcingConnection"

    const-string v2, "Exception when calling getPendingExperimentIds"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 804
    iget-boolean v1, p0, Ldha;->bxG:Z

    if-eqz v1, :cond_0

    .line 805
    const/4 v0, 0x0

    goto :goto_0

    .line 807
    :cond_0
    throw v0
.end method
