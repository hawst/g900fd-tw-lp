.class public interface abstract Lbzh;
.super Ljava/lang/Object;
.source "PG"


# virtual methods
.method public abstract a([BIIZZ)V
.end method

.method public abstract addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract connect()V
.end method

.method public abstract disconnect()V
.end method

.method public abstract es(I)V
.end method

.method public abstract getContentLength()I
.end method

.method public abstract getContentType()Ljava/lang/String;
.end method

.method public abstract getHeaderField(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getHeaderFields()Ljava/util/Map;
.end method

.method public abstract getInputStream()Ljava/io/InputStream;
.end method

.method public abstract getResponseCode()I
.end method

.method public abstract getResponseMessage()Ljava/lang/String;
.end method

.method public abstract getURL()Ljava/net/URL;
.end method

.method public abstract i([B)V
.end method

.method public abstract setInstanceFollowRedirects(Z)V
.end method

.method public abstract setRequestMethod(Ljava/lang/String;)V
.end method

.method public abstract setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setUseCaches(Z)V
.end method
