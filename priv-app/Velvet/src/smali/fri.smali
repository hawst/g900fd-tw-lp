.class public final Lfri;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field private final bVI:Ljava/lang/ref/WeakReference;

.field private final cAJ:Landroid/text/style/ImageSpan;

.field private final cAK:Landroid/text/Spannable;


# direct methods
.method public constructor <init>(Landroid/text/style/ImageSpan;Landroid/text/Spannable;Lcom/google/android/sidekick/shared/ui/TextViewWithImages;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lfri;->cAJ:Landroid/text/style/ImageSpan;

    .line 118
    iput-object p2, p0, Lfri;->cAK:Landroid/text/Spannable;

    .line 119
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lfri;->bVI:Ljava/lang/ref/WeakReference;

    .line 120
    return-void
.end method


# virtual methods
.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 111
    check-cast p1, Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lfri;->bVI:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lfri;->cAJ:Landroid/text/style/ImageSpan;

    iget-object v2, p0, Lfri;->cAK:Landroid/text/Spannable;

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->a(Landroid/text/style/ImageSpan;Landroid/graphics/drawable/Drawable;Landroid/text/Spannable;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
