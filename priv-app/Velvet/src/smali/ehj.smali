.class public final Lehj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final acx:Ljava/lang/String;

.field public bKO:I

.field public bKP:I

.field public bVV:I

.field public bVW:I

.field public bVX:Ljava/util/List;

.field public bVZ:I

.field public bWa:I

.field public bWb:Ljava/lang/String;

.field public bWc:J

.field public bWd:J

.field public bWe:J

.field public bWf:J

.field public bWg:J

.field public bWh:J

.field public bWk:J

.field public bWl:J

.field public bWm:I

.field public bWn:I

.field public bWo:J

.field public bWq:I

.field public bWr:I

.field public bWs:I

.field public bZC:I

.field public bZD:J

.field public bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

.field public bZF:I

.field public bZG:I

.field public bZH:I

.field public bZI:J

.field public bZJ:Z

.field public final beJ:Ljava/lang/String;

.field public bfO:I


# direct methods
.method public constructor <init>(Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 2

    .prologue
    .line 563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564
    iget-object v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    iput-object v0, p0, Lehj;->acx:Ljava/lang/String;

    .line 565
    iget-object v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    iput-object v0, p0, Lehj;->beJ:Ljava/lang/String;

    .line 566
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZC:I

    iput v0, p0, Lehj;->bZC:I

    .line 567
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bVV:I

    iput v0, p0, Lehj;->bVV:I

    .line 568
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bVW:I

    iput v0, p0, Lehj;->bVW:I

    .line 570
    iget-object v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bVX:Ljava/util/List;

    iput-object v0, p0, Lehj;->bVX:Ljava/util/List;

    .line 571
    iget-object v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, p0, Lehj;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 572
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    iput v0, p0, Lehj;->bVZ:I

    .line 573
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWa:I

    iput v0, p0, Lehj;->bWa:I

    .line 574
    iget-object v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWb:Ljava/lang/String;

    iput-object v0, p0, Lehj;->bWb:Ljava/lang/String;

    .line 576
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWc:J

    iput-wide v0, p0, Lehj;->bWc:J

    .line 577
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWd:J

    iput-wide v0, p0, Lehj;->bWd:J

    .line 578
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWe:J

    iput-wide v0, p0, Lehj;->bWe:J

    .line 579
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWf:J

    iput-wide v0, p0, Lehj;->bWf:J

    .line 580
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWg:J

    iput-wide v0, p0, Lehj;->bWg:J

    .line 581
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWh:J

    iput-wide v0, p0, Lehj;->bWh:J

    .line 582
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZD:J

    iput-wide v0, p0, Lehj;->bZD:J

    .line 583
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWk:J

    iput-wide v0, p0, Lehj;->bWk:J

    .line 584
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWl:J

    iput-wide v0, p0, Lehj;->bWl:J

    .line 586
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWm:I

    iput v0, p0, Lehj;->bWm:I

    .line 587
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWn:I

    iput v0, p0, Lehj;->bWn:I

    .line 588
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWo:J

    iput-wide v0, p0, Lehj;->bWo:J

    .line 590
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZF:I

    iput v0, p0, Lehj;->bZF:I

    .line 591
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bfO:I

    iput v0, p0, Lehj;->bfO:I

    .line 592
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZG:I

    iput v0, p0, Lehj;->bZG:I

    .line 593
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZH:I

    iput v0, p0, Lehj;->bZH:I

    .line 594
    iget-wide v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZI:J

    iput-wide v0, p0, Lehj;->bZI:J

    .line 596
    iget-boolean v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bZJ:Z

    iput-boolean v0, p0, Lehj;->bZJ:Z

    .line 597
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWq:I

    iput v0, p0, Lehj;->bWq:I

    .line 598
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWr:I

    iput v0, p0, Lehj;->bWr:I

    .line 599
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bWs:I

    iput v0, p0, Lehj;->bWs:I

    .line 600
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bKO:I

    iput v0, p0, Lehj;->bKO:I

    .line 601
    iget v0, p1, Lcom/google/android/shared/search/SearchBoxStats;->bKP:I

    iput v0, p0, Lehj;->bKP:I

    .line 602
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 605
    iput-object p1, p0, Lehj;->acx:Ljava/lang/String;

    .line 606
    iput-object p2, p0, Lehj;->beJ:Ljava/lang/String;

    .line 607
    const/4 v0, 0x0

    iput v0, p0, Lehj;->bZC:I

    .line 609
    const-string v0, ""

    iput-object v0, p0, Lehj;->bWb:Ljava/lang/String;

    .line 610
    sget-object v0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZT:Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, p0, Lehj;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 611
    return-void
.end method


# virtual methods
.method public final a(IIIIJ)Lehj;
    .locals 1

    .prologue
    .line 740
    iput p1, p0, Lehj;->bZF:I

    .line 741
    iput p2, p0, Lehj;->bfO:I

    .line 742
    iput p3, p0, Lehj;->bZG:I

    .line 743
    iput p4, p0, Lehj;->bZH:I

    .line 744
    iput-wide p5, p0, Lehj;->bZI:J

    .line 745
    return-object p0
.end method

.method public final a(IJIJ)Lehj;
    .locals 0

    .prologue
    .line 662
    iput p1, p0, Lehj;->bWm:I

    .line 663
    iput-wide p2, p0, Lehj;->bWf:J

    .line 665
    iput p4, p0, Lehj;->bWn:I

    .line 666
    iput-wide p5, p0, Lehj;->bWo:J

    .line 667
    return-object p0
.end method

.method public final a(JJJJJ)Lehj;
    .locals 1

    .prologue
    .line 639
    iput-wide p1, p0, Lehj;->bWd:J

    .line 640
    iput-wide p3, p0, Lehj;->bWe:J

    .line 641
    iput-wide p5, p0, Lehj;->bWg:J

    .line 642
    iput-wide p7, p0, Lehj;->bWh:J

    .line 643
    iput-wide p9, p0, Lehj;->bWk:J

    .line 645
    return-object p0
.end method

.method public final arV()Lcom/google/android/shared/search/SearchBoxStats;
    .locals 1

    .prologue
    .line 771
    new-instance v0, Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, p0}, Lcom/google/android/shared/search/SearchBoxStats;-><init>(Lehj;)V

    return-object v0
.end method

.method public final b(IIJJ)Lehj;
    .locals 1

    .prologue
    .line 711
    iput p1, p0, Lehj;->bVZ:I

    .line 712
    iput p2, p0, Lehj;->bWa:I

    .line 713
    iput-wide p3, p0, Lehj;->bZD:J

    .line 714
    iput-wide p5, p0, Lehj;->bWl:J

    .line 715
    return-object p0
.end method
