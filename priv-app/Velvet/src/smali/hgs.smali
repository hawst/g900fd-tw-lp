.class public final Lhgs;
.super Lhgx;
.source "PG"


# instance fields
.field private synthetic dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;


# direct methods
.method public constructor <init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-direct {p0}, Lhgx;-><init>()V

    return-void
.end method


# virtual methods
.method public final aOH()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 226
    iget-object v1, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->h(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Landroid/service/voice/AlwaysOnHotwordDetector;

    move-result-object v1

    if-nez v1, :cond_0

    .line 227
    const-string v1, "GsaVoiceInteractionSrv"

    const-string v2, "AlwaysOnHotwordDetector is null"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 230
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->i(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    move-result v0

    goto :goto_0
.end method

.method public final gq(Z)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0, p1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Z)Z

    .line 283
    return-void
.end method

.method public final nQ(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->d(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    if-nez p1, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Locale can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0, p1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Ljava/lang/String;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->d(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    move-result v0

    return v0
.end method

.method public final nR(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 202
    :try_start_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Please call initializeForLocale method before calling this method."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 206
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)I

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public final nS(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 215
    :try_start_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Please call initializeForLocale method before calling this method."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 219
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->g(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public final nT(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Please call initializeForLocale method before calling this method."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 240
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->h(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Landroid/service/voice/AlwaysOnHotwordDetector;

    move-result-object v0

    invoke-virtual {v0}, Landroid/service/voice/AlwaysOnHotwordDetector;->getSupportedRecognitionModes()I

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public final nU(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 246
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 247
    :try_start_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Please call initializeForLocale method before calling this method."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 252
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->h(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Landroid/service/voice/AlwaysOnHotwordDetector;

    move-result-object v0

    invoke-virtual {v0}, Landroid/service/voice/AlwaysOnHotwordDetector;->createEnrollIntent()Landroid/content/Intent;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final nV(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 259
    :try_start_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Please call initializeForLocale method before calling this method."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 264
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->h(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Landroid/service/voice/AlwaysOnHotwordDetector;

    move-result-object v0

    invoke-virtual {v0}, Landroid/service/voice/AlwaysOnHotwordDetector;->createUnEnrollIntent()Landroid/content/Intent;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final nW(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 271
    :try_start_0
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Please call initializeForLocale method before calling this method."

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 276
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhgs;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->h(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Landroid/service/voice/AlwaysOnHotwordDetector;

    move-result-object v0

    invoke-virtual {v0}, Landroid/service/voice/AlwaysOnHotwordDetector;->createReEnrollIntent()Landroid/content/Intent;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
