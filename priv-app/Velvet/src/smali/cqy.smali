.class public final Lcqy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcrf;


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcqy;->mAccountManager:Landroid/accounts/AccountManager;

    .line 26
    return-void
.end method


# virtual methods
.method public final So()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 33
    :try_start_0
    const-string v0, "AccountManagerGoogleAuthAdapter: get token"

    invoke-virtual {p5, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcqy;->mAccountManager:Landroid/accounts/AccountManager;

    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p2, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p3, v2}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 42
    invoke-virtual {p5}, Leua;->endSection()V

    return-object v0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    :catchall_0
    move-exception v0

    invoke-virtual {p5}, Leua;->endSection()V

    throw v0

    .line 39
    :catch_1
    move-exception v0

    .line 40
    :try_start_2
    new-instance v1, Lbfb;

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbfb;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcqy;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1, p2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method
