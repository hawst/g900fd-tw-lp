.class public final Ljyz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHP:D

.field private eMS:Z

.field private eMT:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 105
    iput v2, p0, Ljyz;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljyz;->dHP:D

    iput-boolean v2, p0, Ljyz;->eMS:Z

    iput-boolean v2, p0, Ljyz;->eMT:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljyz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyz;->eCz:I

    .line 106
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 28
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljyz;->dHP:D

    iget v0, p0, Ljyz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljyz;->eMS:Z

    iget v0, p0, Ljyz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljyz;->eMT:Z

    iget v0, p0, Ljyz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 121
    iget v0, p0, Ljyz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x1

    iget-wide v2, p0, Ljyz;->dHP:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 124
    :cond_0
    iget v0, p0, Ljyz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 125
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljyz;->eMS:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 127
    :cond_1
    iget v0, p0, Ljyz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 128
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljyz;->eMT:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 130
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 131
    return-void
.end method

.method public final jD(Z)Ljyz;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljyz;->eMT:Z

    .line 92
    iget v0, p0, Ljyz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyz;->aez:I

    .line 93
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 135
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 136
    iget v1, p0, Ljyz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 137
    const/4 v1, 0x1

    iget-wide v2, p0, Ljyz;->dHP:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 140
    :cond_0
    iget v1, p0, Ljyz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 141
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljyz;->eMS:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 144
    :cond_1
    iget v1, p0, Ljyz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 145
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljyz;->eMT:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 148
    :cond_2
    return v0
.end method

.method public final x(D)Ljyz;
    .locals 1

    .prologue
    .line 53
    iput-wide p1, p0, Ljyz;->dHP:D

    .line 54
    iget v0, p0, Ljyz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyz;->aez:I

    .line 55
    return-object p0
.end method
