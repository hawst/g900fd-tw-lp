.class public final Lfwf;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;Ljdx;J)Ljava/lang/CharSequence;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p1}, Ljdx;->om()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    invoke-virtual {p1}, Ljdx;->ol()Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Ljdx;->bhZ()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljdx;->bhY()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljdx;->bib()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-static {p0, p1, p2, p3}, Lfwf;->b(Landroid/content/Context;Ljdx;J)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljdx;->bia()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const/16 v2, 0x13

    invoke-static {p0, v0, v1, v2}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lizj;J)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    iget-object v1, p1, Lizj;->dSU:Ljdx;

    invoke-static {p0, v1, p2, p3}, Lfwf;->a(Landroid/content/Context;Ljdx;J)Ljava/lang/CharSequence;

    move-result-object v1

    .line 104
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_0
    iget-object v1, p1, Lizj;->dUu:Ljdv;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lizj;->dUu:Ljdv;

    invoke-virtual {v1}, Ljdv;->bhR()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 109
    iget-object v1, p1, Lizj;->dUu:Ljdv;

    invoke-virtual {v1}, Ljdv;->bhR()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 112
    const-string v1, " \u00b7 "

    invoke-static {v1, v0}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljdx;J)Ljava/lang/CharSequence;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 184
    invoke-virtual {p1}, Ljdx;->bib()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 185
    invoke-virtual {p1}, Ljdx;->bia()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v2

    .line 187
    invoke-virtual {p1}, Ljdx;->yj()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljdx;->bic()I

    move-result v1

    move v2, v1

    .line 191
    :goto_0
    const/4 v1, 0x2

    if-ne v2, v1, :cond_3

    invoke-virtual {p1}, Ljdx;->bie()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 194
    invoke-virtual {p1}, Ljdx;->bid()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v1, v0

    .line 208
    :goto_1
    if-eqz v1, :cond_1

    .line 209
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 274
    :goto_2
    return-object v0

    .line 187
    :cond_0
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 196
    :pswitch_0
    const v1, 0x7f0a0412

    .line 197
    goto :goto_1

    .line 199
    :pswitch_1
    const v1, 0x7f0a0413

    .line 200
    goto :goto_1

    .line 202
    :pswitch_2
    const v1, 0x7f0a0414

    .line 203
    goto :goto_1

    .line 205
    :pswitch_3
    const v1, 0x7f0a0415

    goto :goto_1

    .line 213
    :cond_1
    invoke-static {v4, v5, p2, p3}, Lesi;->g(JJ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 215
    invoke-virtual {p1}, Ljdx;->bid()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    move v1, v0

    .line 229
    :goto_3
    if-eqz v1, :cond_2

    .line 230
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 217
    :pswitch_4
    const v1, 0x7f0a0417

    .line 218
    goto :goto_3

    .line 220
    :pswitch_5
    const v1, 0x7f0a0418

    .line 221
    goto :goto_3

    .line 223
    :pswitch_6
    const v1, 0x7f0a0419

    .line 224
    goto :goto_3

    .line 226
    :pswitch_7
    const v1, 0x7f0a041a

    goto :goto_3

    .line 234
    :cond_2
    invoke-static {v4, v5, p2, p3}, Lesi;->h(JJ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 236
    invoke-virtual {p1}, Ljdx;->bid()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 250
    :goto_4
    if-eqz v0, :cond_3

    .line 251
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 238
    :pswitch_8
    const v0, 0x7f0a041c

    .line 239
    goto :goto_4

    .line 241
    :pswitch_9
    const v0, 0x7f0a041d

    .line 242
    goto :goto_4

    .line 244
    :pswitch_a
    const v0, 0x7f0a041e

    .line 245
    goto :goto_4

    .line 247
    :pswitch_b
    const v0, 0x7f0a041f

    goto :goto_4

    .line 256
    :cond_3
    const/4 v0, 0x3

    if-ne v2, v0, :cond_7

    .line 257
    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 258
    const v0, 0x7f0a03fe

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 260
    :cond_4
    invoke-static {v4, v5, p2, p3}, Lesi;->g(JJ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 261
    const v0, 0x7f0a0416

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 263
    :cond_5
    invoke-static {v4, v5, p2, p3}, Lesi;->h(JJ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 264
    const v0, 0x7f0a041b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 266
    :cond_6
    const/16 v0, 0x12

    invoke-static {p0, v4, v5, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 270
    :cond_7
    const/4 v0, 0x4

    if-ne v2, v0, :cond_8

    .line 271
    const v0, 0x7f0a0420

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 274
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 194
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 215
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 236
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private e(Landroid/content/Context;Lfmt;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 311
    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    .line 312
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 313
    iget-object v0, v3, Lizj;->dUu:Ljdv;

    if-eqz v0, :cond_2

    iget-object v0, v3, Lizj;->dUu:Ljdv;

    iget-object v0, v0, Ljdv;->ecV:[Ljdw;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, v3, Lizj;->dUu:Ljdv;

    iget-object v5, v0, Ljdv;->ecV:[Ljdw;

    array-length v6, v5

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v0, v5, v2

    .line 315
    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v7

    new-array v8, v1, [I

    invoke-static {v3, v7, v8}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 317
    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v7

    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v8

    invoke-virtual {v7, v8}, Lfml;->iH(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 322
    new-instance v7, Lfwg;

    invoke-direct {v7, v0, p1}, Lfwg;-><init>(Ljdw;Landroid/content/Context;)V

    .line 326
    invoke-virtual {v7}, Lfwg;->aDH()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lfwg;->aDI()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 327
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 326
    goto :goto_1

    .line 332
    :cond_2
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 13

    .prologue
    .line 51
    new-instance v8, Lang;

    invoke-direct {v8}, Lang;-><init>()V

    .line 52
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    new-instance v2, Lfsa;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0201cd

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lfsa;->jC(I)Lani;

    move-result-object v2

    iget-object v3, v1, Lizj;->dSU:Ljdx;

    new-instance v4, Lftn;

    invoke-virtual {v3}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lftn;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {p1, v1, v6, v7}, Lfwf;->a(Landroid/content/Context;Lizj;J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iput-object v5, v4, Lftn;->ceJ:Ljava/lang/String;

    :cond_0
    iget-object v6, v3, Ljdx;->aeB:Ljbp;

    if-eqz v6, :cond_2

    iget-object v6, v3, Ljdx;->aeB:Ljbp;

    invoke-virtual {v6}, Ljbp;->bfb()Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v5, :cond_1

    iget-object v6, v3, Ljdx;->aeB:Ljbp;

    invoke-virtual {v6}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    iget-object v3, v3, Ljdx;->aeB:Ljbp;

    invoke-virtual {v3}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lftn;->cBj:Ljava/lang/String;

    :cond_2
    invoke-virtual {v4, v2, v1}, Lftn;->a(Lani;Lizj;)Lftn;

    move-result-object v1

    invoke-virtual {v1}, Lftn;->aDh()Lanh;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lizj;->dUu:Ljdv;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v1}, Ljdv;->bhP()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Laoj;

    invoke-direct {v1}, Laoj;-><init>()V

    iget-object v2, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v2}, Ljdv;->bhP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v2, Lfsa;

    const/16 v3, 0x5a

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f020089

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lfsa;->jC(I)Lani;

    move-result-object v2

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lanh;->cm(I)Lanh;

    iput-object v1, v3, Lanh;->agz:Laoj;

    iput-object v2, v3, Lanh;->ahs:Lani;

    iput-object v0, v3, Lanh;->ahu:Lizj;

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-direct {p0, p1, p2}, Lfwf;->e(Landroid/content/Context;Lfmt;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lfwg;

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    new-instance v1, Laoj;

    invoke-direct {v1}, Laoj;-><init>()V

    invoke-virtual {v7}, Lfwg;->aDI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v11, Lanh;

    invoke-direct {v11}, Lanh;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v11, v2}, Lanh;->cm(I)Lanh;

    iput-object v1, v11, Lanh;->agz:Laoj;

    iput-object v0, v11, Lanh;->ahu:Lizj;

    const/4 v0, 0x0

    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    if-eqz v1, :cond_7

    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    invoke-virtual {v1}, Lixx;->baX()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, v7, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v0}, Ljdw;->oP()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v7, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v0

    :goto_1
    new-instance v1, Lfsa;

    invoke-direct {v1, v0}, Lfsa;-><init>(I)V

    invoke-virtual {v7}, Lfwg;->aDH()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    invoke-virtual {v1}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    iput-object v0, v11, Lanh;->ahs:Lani;

    :cond_5
    invoke-static {v9, v11}, Lfwf;->a(Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    const/16 v0, 0xad

    goto :goto_1

    :cond_7
    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->dSf:Lixr;

    if-eqz v1, :cond_8

    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->dSf:Lixr;

    invoke-virtual {v1}, Lixr;->pb()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, v7, Lfwg;->cqQ:Ljdw;

    iget-object v6, v0, Ljdw;->dSf:Lixr;

    const-string v0, ","

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    invoke-virtual {v0}, Lifj;->aVV()Lifj;

    move-result-object v12

    invoke-virtual {v6}, Lixr;->getTitle()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    iget-object v6, v6, Lixr;->dOo:[Ljava/lang/String;

    invoke-virtual {v12, v6}, Lifj;->b([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Ledv;->a(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lfsa;

    const/16 v2, 0xae

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    invoke-virtual {v7}, Lfwg;->aDH()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    goto :goto_2

    :cond_8
    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->dME:Lixn;

    if-eqz v1, :cond_9

    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->dME:Lixn;

    invoke-virtual {v1}, Lixn;->bau()Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v0, Lfsa;

    const/16 v1, 0x94

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    invoke-virtual {v7}, Lfwg;->aDH()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lfwg;->cqQ:Ljdw;

    iget-object v2, v2, Ljdw;->dME:Lixn;

    invoke-virtual {v2}, Lixn;->Ar()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v1}, Ljdw;->getQuery()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Lfsa;

    const/16 v1, 0xad

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    invoke-virtual {v7}, Lfwg;->aDH()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    iget-object v1, v7, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v1}, Ljdw;->getQuery()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    goto/16 :goto_2

    :cond_a
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v8, Lang;->ags:[Lanh;

    .line 53
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lang;->aS(Z)Lang;

    .line 54
    return-object v8
.end method
