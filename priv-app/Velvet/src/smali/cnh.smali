.class public final Lcnh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bcO:[Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLoginHelper:Lcrh;

.field private mPlayLoggerProxy:Lcop;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "dirty"

    aput-object v2, v0, v1

    sput-object v0, Lcnh;->bcO:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcrh;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcrh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcnh;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcnh;->mLoginHelper:Lcrh;

    .line 56
    new-instance v0, Lcop;

    const-string v1, "ContactAccountLogger"

    const/16 v2, 0x23

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcop;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcnh;->mPlayLoggerProxy:Lcop;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcrh;Lcop;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcrh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcop;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcnh;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcnh;->mLoginHelper:Lcrh;

    .line 66
    iput-object p3, p0, Lcnh;->mPlayLoggerProxy:Lcop;

    .line 67
    return-void
.end method


# virtual methods
.method public final aa(J)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 70
    iget-object v0, p0, Lcnh;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v6

    .line 71
    if-nez v6, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 105
    :goto_0
    return v0

    .line 78
    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const-string v2, "10000"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 81
    iget-object v0, p0, Lcnh;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcnh;->bcO:[Ljava/lang/String;

    const-string v5, "times_contacted DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 84
    new-instance v1, Lcni;

    invoke-direct {v1}, Lcni;-><init>()V

    .line 85
    invoke-static {v1, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    .line 86
    invoke-static {v1}, Lcni;->a(Lcni;)Lisy;

    move-result-object v0

    .line 98
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcnh;->mPlayLoggerProxy:Lcop;

    invoke-virtual {v1, v6, p1, p2, v0}, Lcop;->a(Ljava/lang/String;J[B)V

    .line 105
    const/4 v0, 0x1

    goto :goto_0
.end method
