.class public Lebu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leen;


# instance fields
.field final synthetic bUB:Lebm;


# direct methods
.method public constructor <init>(Lebm;)V
    .locals 0

    .prologue
    .line 1381
    iput-object p1, p0, Lebu;->bUB:Lebm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ligi;)Z
    .locals 1

    .prologue
    .line 1438
    const/4 v0, 0x0

    return v0
.end method

.method public final j(Lcom/google/android/shared/search/Suggestion;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1386
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1387
    iget-object v0, p0, Lebu;->bUB:Lebm;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2, v2}, Lebm;->d(IIZ)V

    .line 1388
    iget-object v0, p0, Lebu;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, p0, Lebu;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-static {v1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 1399
    :cond_0
    :goto_0
    iget-object v0, p0, Lebu;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    iget-object v1, p0, Lebu;->bUB:Lebm;

    iget-object v1, v1, Lebm;->bUp:Ledu;

    invoke-virtual {v1, v3, p1}, Ledu;->b(ILcom/google/android/shared/search/Suggestion;)V

    iput v3, v1, Ledu;->bVW:I

    invoke-virtual {v1}, Ledu;->anZ()Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lecq;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    .line 1405
    return-void

    .line 1391
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lebu;->bUB:Lebm;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2, v2}, Lebm;->d(IIZ)V

    .line 1393
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v0

    .line 1394
    iget-object v1, p0, Lebu;->bUB:Lebm;

    iget-object v1, v1, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/SearchPlate;->n(Ljava/lang/CharSequence;)V

    .line 1395
    iget-object v1, p0, Lebu;->bUB:Lebm;

    iget-object v1, v1, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v2, p0, Lebu;->bUB:Lebm;

    iget-object v2, v2, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v0}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-static {v0}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    goto :goto_0
.end method

.method public final k(Lcom/google/android/shared/search/Suggestion;)V
    .locals 3

    .prologue
    .line 1409
    iget-object v0, p0, Lebu;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    iget-object v1, p0, Lebu;->bUB:Lebm;

    iget-object v1, v1, Lebm;->bUp:Ledu;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Ledu;->b(ILcom/google/android/shared/search/Suggestion;)V

    invoke-virtual {v1}, Ledu;->anZ()Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lecq;->b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    .line 1411
    return-void
.end method

.method public final l(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1415
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1417
    iget-object v0, p0, Lebu;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUd:Landroid/content/Context;

    new-instance v2, Lebv;

    invoke-direct {v2, p0, p1}, Lebv;-><init>(Lebu;Lcom/google/android/shared/search/Suggestion;)V

    invoke-static {v0, p1, v2}, Ledg;->a(Landroid/content/Context;Lcom/google/android/shared/search/Suggestion;Ljava/lang/Runnable;)V

    .line 1424
    return v1

    .line 1415
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m(Lcom/google/android/shared/search/Suggestion;)V
    .locals 4

    .prologue
    .line 1429
    iget-object v0, p0, Lebu;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    invoke-virtual {v0, p1}, Ledu;->n(Lcom/google/android/shared/search/Suggestion;)Ledu;

    .line 1430
    iget-object v0, p0, Lebu;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    iget-object v1, p0, Lebu;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apB()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lebq;->z(Lcom/google/android/shared/search/Query;)V

    .line 1433
    return-void
.end method
