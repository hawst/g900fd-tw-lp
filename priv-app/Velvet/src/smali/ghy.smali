.class public final Lghy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cKD:Ljava/lang/String;

.field public static final cKE:Ljava/lang/String;

.field public static final cKF:Ljava/lang/String;

.field public static final cKG:Ljava/lang/String;


# instance fields
.field private final bwj:Ldgm;

.field private final cKH:Lcom/google/android/gms/appdatasearch/QuerySpecification;

.field private final cKI:Lcom/google/android/gms/appdatasearch/QuerySpecification;

.field private final cKJ:Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;

.field private final cKK:Lggh;

.field private final cKL:Lghw;

.field private final cKM:Leum;

.field private cKN:Lcjf;

.field private final mContactLabelConverter:Ldyv;

.field private final mGsaConfigFlags:Lchk;

.field private final mRelationshipManager:Lcjg;

.field private final mRelationshipNameLookup:Leai;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcum;->bjs:Lcum;

    invoke-virtual {v0}, Lcum;->xF()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lghy;->cKD:Ljava/lang/String;

    .line 58
    sget-object v0, Lcum;->bju:Lcum;

    invoke-virtual {v0}, Lcum;->xF()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lghy;->cKE:Ljava/lang/String;

    .line 59
    sget-object v0, Lcum;->bjt:Lcum;

    invoke-virtual {v0}, Lcum;->xF()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lghy;->cKF:Ljava/lang/String;

    .line 60
    sget-object v0, Lcum;->bjv:Lcum;

    invoke-virtual {v0}, Lcum;->xF()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lghy;->cKG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lggh;Ldgm;Leai;Lcjg;Lggq;Leum;Lchk;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lggh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ldgm;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Leai;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lggq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Leum;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p8    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v0

    iput-object v0, p0, Lghy;->mContactLabelConverter:Ldyv;

    .line 111
    iput-object p2, p0, Lghy;->cKK:Lggh;

    .line 112
    iput-object p3, p0, Lghy;->bwj:Ldgm;

    .line 114
    new-instance v0, Lbce;

    invoke-direct {v0}, Lbce;-><init>()V

    iput-boolean v3, v0, Lbce;->awd:Z

    iput-boolean v4, v0, Lbce;->awb:Z

    new-instance v1, Lcom/google/android/gms/appdatasearch/Section;

    const-string v2, "name"

    invoke-direct {v1, v2}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/appdatasearch/Section;

    const-string v2, "givennames"

    invoke-direct {v1, v2}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/appdatasearch/Section;

    const-string v2, "nickname"

    invoke-direct {v1, v2}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/appdatasearch/Section;

    const-string v2, "lookup_key"

    invoke-direct {v1, v2}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v0

    invoke-virtual {v0}, Lbce;->xd()Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v0

    iput-object v0, p0, Lghy;->cKH:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 125
    new-instance v1, Lbce;

    invoke-direct {v1}, Lbce;-><init>()V

    iput-boolean v3, v1, Lbce;->awd:Z

    iput-boolean v4, v1, Lbce;->awb:Z

    new-instance v2, Lcom/google/android/gms/appdatasearch/Section;

    const-string v3, "contact_id"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/appdatasearch/Section;

    const-string v3, "data"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/appdatasearch/Section;

    const-string v3, "label"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/appdatasearch/Section;

    const-string v3, "type"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lbce;->a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;

    move-result-object v1

    invoke-virtual {v1}, Lbce;->xd()Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v1

    iput-object v1, p0, Lghy;->cKI:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    .line 135
    new-instance v1, Lbcd;

    invoke-direct {v1}, Lbcd;-><init>()V

    new-instance v2, Lbcc;

    invoke-direct {v2}, Lbcc;-><init>()V

    iput-object v0, v2, Lbcc;->auP:Ljava/lang/String;

    sget-object v0, Lghy;->cKD:Ljava/lang/String;

    iput-object v0, v2, Lbcc;->auQ:Ljava/lang/String;

    const-string v0, "name"

    const/16 v3, 0x30

    invoke-virtual {v2, v0, v3}, Lbcc;->g(Ljava/lang/String;I)Lbcc;

    move-result-object v0

    const-string v2, "givennames"

    const/16 v3, 0x20

    invoke-virtual {v0, v2, v3}, Lbcc;->g(Ljava/lang/String;I)Lbcc;

    move-result-object v0

    const-string v2, "nickname"

    const/16 v3, 0x10

    invoke-virtual {v0, v2, v3}, Lbcc;->g(Ljava/lang/String;I)Lbcc;

    move-result-object v0

    iget-object v2, v1, Lbcd;->avV:Ljava/util/List;

    iget-object v3, v0, Lbcc;->auP:Ljava/lang/String;

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No package name specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v3, v0, Lbcc;->auQ:Ljava/lang/String;

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No corpus name specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v3, v0, Lbcc;->avR:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No section weights specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v3, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    iget-object v4, v0, Lbcc;->auP:Ljava/lang/String;

    iget-object v5, v0, Lbcc;->auQ:Ljava/lang/String;

    iget-object v0, v0, Lbcc;->avR:Ljava/util/Map;

    invoke-direct {v3, v4, v5, v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v1, Lbcd;->avV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No corpus specs specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v2, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;

    iget-object v1, v1, Lbcd;->avV:Ljava/util/List;

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-direct {v2, v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;-><init>([Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V

    iput-object v2, p0, Lghy;->cKJ:Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;

    .line 146
    new-instance v0, Lghw;

    invoke-direct {v0, p4}, Lghw;-><init>(Leai;)V

    iput-object v0, p0, Lghy;->cKL:Lghw;

    .line 147
    iput-object p4, p0, Lghy;->mRelationshipNameLookup:Leai;

    .line 148
    iput-object p5, p0, Lghy;->mRelationshipManager:Lcjg;

    .line 149
    iput-object p7, p0, Lghy;->cKM:Leum;

    .line 151
    iput-object p8, p0, Lghy;->mGsaConfigFlags:Lchk;

    .line 152
    return-void
.end method

.method public static A(Ljava/util/Collection;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 417
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 418
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 423
    :goto_0
    return-object v0

    .line 420
    :cond_0
    const-string v0, " OR "

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/shared/search/Query;Ljava/util/List;Ldzb;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0xa

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 237
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 238
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v3

    .line 322
    :cond_0
    :goto_0
    return-object v0

    .line 245
    :cond_1
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 246
    iget-object v1, p0, Lghy;->mRelationshipNameLookup:Leai;

    invoke-interface {v1, v0}, Leai;->kH(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object v1, v0

    .line 249
    :goto_1
    invoke-static {p2}, Lghy;->ak(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 250
    if-eqz v1, :cond_a

    .line 251
    iget-object v4, p0, Lghy;->cKN:Lcjf;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lghy;->cKN:Lcjf;

    iput-boolean v7, v4, Lcjf;->aYm:Z

    .line 252
    :cond_2
    invoke-virtual {p0, v2, v5}, Lghy;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v4

    .line 253
    iget-object v5, p0, Lghy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v5}, Lchk;->FX()Z

    move-result v5

    if-nez v5, :cond_3

    .line 254
    iget-object v5, p0, Lghy;->mRelationshipNameLookup:Leai;

    invoke-static {v4, v2, v5}, Lcom/google/android/search/shared/contact/Person;->a(Ljava/util/List;Ljava/util/List;Leai;)V

    .line 256
    :cond_3
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 260
    :goto_2
    iget-object v2, p0, Lghy;->cKN:Lcjf;

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 261
    iget-object v2, p0, Lghy;->cKN:Lcjf;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v2, Lcjf;->aYq:I

    .line 269
    :cond_4
    if-eqz v1, :cond_6

    .line 270
    iget-object v2, p0, Lghy;->mRelationshipManager:Lcjg;

    invoke-virtual {v2, v1}, Lcjg;->gF(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-direct {p0, v2}, Lghy;->z(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 272
    :goto_3
    iget-object v4, p0, Lghy;->cKN:Lcjf;

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 273
    iget-object v4, p0, Lghy;->cKN:Lcjf;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    iput v5, v4, Lcjf;->aYr:I

    .line 275
    :cond_5
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 278
    :cond_6
    new-array v2, v7, [Ldzl;

    new-instance v4, Ldzr;

    invoke-direct {v4}, Ldzr;-><init>()V

    aput-object v4, v2, v6

    invoke-static {v3, v2}, Lcom/google/android/search/shared/contact/Person;->a(Ljava/util/List;[Ldzl;)Ljava/util/List;

    move-result-object v2

    .line 280
    invoke-direct {p0, v2, p4}, Lghy;->f(Ljava/util/List;Ljava/lang/String;)V

    .line 284
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 290
    invoke-static {v2, v0}, Lcom/google/android/search/shared/contact/Person;->b(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v0

    .line 293
    if-eqz v0, :cond_7

    invoke-virtual {v0, p3}, Lcom/google/android/search/shared/contact/Person;->c(Ldzb;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 294
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 295
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    :cond_7
    if-eqz v1, :cond_8

    .line 305
    invoke-virtual {p0, v1, v2}, Lghy;->f(Ljava/lang/String;Ljava/util/List;)V

    .line 307
    :cond_8
    iget-object v0, p0, Lghy;->mRelationshipManager:Lcjg;

    invoke-virtual {v0, v2}, Lcjg;->f(Ljava/util/Collection;)V

    .line 309
    iget-object v0, p0, Lghy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FS()Z

    move-result v0

    if-eqz v0, :cond_c

    if-eqz v1, :cond_c

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 313
    invoke-virtual {p0, v2, v1}, Lghy;->e(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 316
    :goto_4
    iget-object v1, p0, Lghy;->mRelationshipManager:Lcjg;

    invoke-virtual {v1, v0}, Lcjg;->f(Ljava/util/Collection;)V

    .line 318
    if-eqz p3, :cond_0

    .line 321
    invoke-static {v0, p3}, Lghy;->b(Ljava/util/List;Ldzb;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 246
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 258
    :cond_a
    invoke-virtual {p0, v2, v5}, Lghy;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 270
    :cond_b
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_3

    :cond_c
    move-object v0, v2

    goto :goto_4
.end method

.method public static ak(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 428
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 429
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 430
    invoke-static {v0}, Lcom/google/android/search/shared/contact/Person;->kD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 435
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 438
    :cond_1
    return-object v1
.end method

.method public static b(Ljava/util/List;Ldzb;)Ljava/util/List;
    .locals 4

    .prologue
    .line 447
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 449
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 450
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 451
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 452
    sget-object v3, Ldzb;->bRq:Ldzb;

    if-ne p1, v3, :cond_1

    .line 453
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 454
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 455
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 457
    :cond_1
    sget-object v3, Ldzb;->bRp:Ldzb;

    if-ne p1, v3, :cond_2

    .line 458
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 459
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 460
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 462
    :cond_2
    sget-object v3, Ldzb;->bRs:Ldzb;

    if-ne p1, v3, :cond_3

    .line 463
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->amd()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 464
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 465
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 467
    :cond_3
    sget-object v3, Ldzb;->bRr:Ldzb;

    if-ne p1, v3, :cond_0

    .line 468
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiI()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 469
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 470
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 475
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 476
    invoke-interface {v1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 478
    :cond_5
    return-object v1
.end method

.method public static bG(Landroid/content/Context;)Lghy;
    .locals 11

    .prologue
    .line 90
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    .line 91
    new-instance v0, Lghy;

    new-instance v2, Lggh;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v1}, Lgql;->aJV()Lcjg;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lggh;-><init>(Landroid/content/ContentResolver;Lcjg;)V

    invoke-virtual {v1}, Lgql;->aJR()Lcgh;

    move-result-object v3

    invoke-interface {v3}, Lcgh;->ET()Ldgm;

    move-result-object v3

    invoke-virtual {v1}, Lgql;->aJW()Leai;

    move-result-object v4

    invoke-virtual {v1}, Lgql;->aJV()Lcjg;

    move-result-object v5

    invoke-virtual {v1}, Lgql;->aJX()Lggq;

    move-result-object v6

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v7

    iget-object v8, v7, Lhhq;->dhu:Leum;

    if-nez v8, :cond_0

    new-instance v8, Leum;

    iget-object v9, v7, Lhhq;->mContext:Landroid/content/Context;

    iget-object v10, v7, Lhhq;->mAsyncServices:Lema;

    invoke-direct {v8, v9, v10}, Leum;-><init>(Landroid/content/Context;Lerk;)V

    iput-object v8, v7, Lhhq;->dhu:Leum;

    :cond_0
    iget-object v7, v7, Lhhq;->dhu:Leum;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lghy;-><init>(Landroid/content/Context;Lggh;Ldgm;Leai;Lcjg;Lggq;Leum;Lchk;)V

    return-object v0
.end method

.method private f(Ljava/util/List;Ljava/lang/String;)V
    .locals 13
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 590
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 591
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 594
    :cond_0
    const/4 v0, 0x0

    .line 595
    iget-object v2, p0, Lghy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->GK()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 596
    iget-object v0, p0, Lghy;->cKM:Leum;

    invoke-virtual {v0}, Leum;->avZ()Lcgs;

    move-result-object v0

    invoke-virtual {v0}, Lcgs;->Fg()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    move-object v9, v0

    .line 598
    :goto_1
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, Lghy;->cKE:Ljava/lang/String;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    sget-object v4, Lghy;->cKF:Ljava/lang/String;

    aput-object v4, v2, v0

    const/4 v0, 0x2

    sget-object v4, Lghy;->cKG:Ljava/lang/String;

    aput-object v4, v2, v0

    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v10

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lghy;->bwj:Ldgm;

    const/16 v4, 0x19

    iget-object v5, p0, Lghy;->cKI:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-virtual/range {v0 .. v5}, Ldgm;->b(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v10, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    iget-object v1, p0, Lghy;->cKL:Lghw;

    invoke-virtual {v1, v0, p2}, Lghw;->a(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v10, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 603
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/search/shared/contact/Person;

    .line 604
    invoke-virtual {v8}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 605
    if-eqz v0, :cond_3

    .line 606
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_4
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/search/shared/contact/Contact;

    .line 609
    invoke-virtual {v8}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/search/shared/contact/Contact;->setName(Ljava/lang/String;)V

    .line 610
    invoke-virtual {v8}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/search/shared/contact/Contact;->kv(Ljava/lang/String;)V

    .line 611
    invoke-static {v8, v7}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V

    .line 612
    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/Contact;->akB()Ldzb;

    move-result-object v0

    sget-object v1, Ldzb;->bRp:Ldzb;

    if-ne v0, v1, :cond_4

    if-eqz v9, :cond_4

    .line 616
    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 617
    if-eqz v6, :cond_4

    .line 618
    new-instance v0, Lcom/google/android/search/shared/contact/Contact;

    sget-object v1, Ldzb;->bRs:Ldzb;

    invoke-virtual {v8}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v2

    invoke-virtual {v8}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v8, v0}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V

    goto :goto_3

    .line 625
    :cond_5
    return-void

    :cond_6
    move-object v9, v0

    goto/16 :goto_1
.end method

.method private z(Ljava/util/Collection;)Ljava/util/List;
    .locals 5

    .prologue
    .line 199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 200
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 210
    const-string v3, "\\."

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {p0, v0, v3}, Lghy;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 213
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 214
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 218
    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Query;Ldzb;[Ljks;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ldzb;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # [Ljks;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 509
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 510
    array-length v2, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p3, v0

    .line 511
    invoke-virtual {v3}, Ljks;->oK()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 512
    invoke-virtual {v3}, Ljks;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 515
    :cond_1
    invoke-direct {p0, p1, v1, p2, p4}, Lghy;->a(Lcom/google/android/shared/search/Query;Ljava/util/List;Ldzb;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljop;)Ljava/util/List;
    .locals 4
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljop;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 483
    invoke-static {p2}, Ldzb;->a(Ljop;)Ldzb;

    move-result-object v0

    .line 484
    iget-object v1, p2, Ljop;->ewI:[Ljava/lang/String;

    .line 485
    array-length v2, v1

    if-nez v2, :cond_0

    .line 486
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 489
    :goto_0
    return-object v0

    .line 488
    :cond_0
    iget-object v2, p0, Lghy;->mContactLabelConverter:Ldyv;

    iget-object v3, p2, Ljop;->ewy:Ljor;

    invoke-virtual {v2, v3}, Ldyv;->a(Ljor;)Ljava/lang/String;

    move-result-object v2

    .line 489
    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0, v2}, Lghy;->a(Lcom/google/android/shared/search/Query;Ljava/util/List;Ldzb;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcjf;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lghy;->cKN:Lcjf;

    .line 156
    return-void
.end method

.method public final aFN()Ljava/util/List;
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lghy;->mRelationshipManager:Lcjg;

    invoke-virtual {v0}, Lcjg;->KX()Ljava/util/Set;

    move-result-object v0

    .line 223
    invoke-direct {p0, v0}, Lghy;->z(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lghy;->mRelationshipManager:Lcjg;

    invoke-virtual {v1, v0}, Lcjg;->f(Ljava/util/Collection;)V

    .line 225
    return-object v0
.end method

.method public final aj(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 404
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 405
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 406
    iget-object v3, p0, Lghy;->mRelationshipNameLookup:Leai;

    invoke-interface {v3, v0}, Leai;->kI(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 407
    if-eqz v0, :cond_0

    .line 408
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 411
    :cond_1
    return-object v1
.end method

.method public final bz(J)Lcom/google/android/search/shared/contact/Person;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 638
    new-array v0, v8, [Ljava/lang/Long;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lghy;->bwj:Ldgm;

    sget-object v1, Lghy;->cKD:Ljava/lang/String;

    iget-object v5, p0, Lghy;->cKH:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-virtual {v0, v4, v1, v5}, Ldgm;->b([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentResults;->wX()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const-string v0, "IcingContactLookup"

    const-string v1, "getPersonsByContactId() : 0 results"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 640
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v8, :cond_3

    .line 641
    const-string v1, "IcingContactLookup"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fetchContactInfo() : Found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " results for ContactId: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 643
    const v0, 0xd4eb4b

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 644
    iget-object v0, p0, Lghy;->cKK:Lggh;

    invoke-virtual {v0, p1, p2}, Lggh;->bz(J)Lcom/google/android/search/shared/contact/Person;

    move-result-object v0

    .line 646
    :goto_2
    return-object v0

    .line 638
    :cond_2
    iget-object v1, p0, Lghy;->cKL:Lghw;

    invoke-virtual {v1, v3, v0}, Lghw;->a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/DocumentResults;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lghy;->f(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_1

    .line 646
    :cond_3
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    goto :goto_2
.end method

.method public final d(Ljava/util/List;I)Ljava/util/List;
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 386
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 387
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 388
    iget-object v0, p0, Lghy;->bwj:Ldgm;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v4, Lghy;->cKD:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v5, p0, Lghy;->cKH:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Ldgm;->b(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    .line 391
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->wX()I

    move-result v1

    if-eqz v1, :cond_0

    .line 393
    iget-object v1, p0, Lghy;->cKL:Lghw;

    invoke-virtual {v1, v0}, Lghw;->b(Lcom/google/android/gms/appdatasearch/SearchResults;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 400
    :cond_1
    return-object v6
.end method

.method public final e(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 333
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 334
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 335
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->amb()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Relationship;

    .line 336
    invoke-virtual {v1, p2}, Lcom/google/android/search/shared/contact/Relationship;->kF(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1, p2}, Lcom/google/android/search/shared/contact/Relationship;->kG(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 338
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 343
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move-object p1, v2

    .line 345
    :cond_4
    return-object p1
.end method

.method public final f(Ljava/lang/String;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 358
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 359
    iget-object v0, p0, Lghy;->mRelationshipNameLookup:Leai;

    invoke-interface {v0, p1}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    .line 361
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 362
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 364
    iget-object v4, p0, Lghy;->mRelationshipManager:Lcjg;

    invoke-virtual {v4, v0}, Lcjg;->d(Lcom/google/android/search/shared/contact/Person;)Ljava/util/Set;

    move-result-object v4

    .line 366
    if-eqz v2, :cond_0

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 367
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Removed person "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 371
    :cond_1
    iget-object v0, p0, Lghy;->cKN:Lcjf;

    if-eqz v0, :cond_2

    .line 372
    iget-object v0, p0, Lghy;->cKN:Lcjf;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Lcjf;->aYs:I

    .line 374
    :cond_2
    return-void
.end method

.method public final mG(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 522
    invoke-static {p1}, Lcom/google/android/search/shared/contact/Person;->kD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 523
    iget-object v3, p0, Lghy;->bwj:Ldgm;

    new-array v4, v1, [Ljava/lang/String;

    aput-object v2, v4, v0

    iget-object v2, p0, Lghy;->cKJ:Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;

    invoke-virtual {v3, v4, v2}, Ldgm;->b([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v2

    .line 525
    if-nez v2, :cond_1

    .line 536
    :cond_0
    :goto_0
    return v0

    .line 530
    :cond_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->dy(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 531
    invoke-virtual {v2, v0, v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->ac(II)I

    move v0, v1

    .line 533
    goto :goto_0
.end method

.method public final mH(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 546
    iget-object v0, p0, Lghy;->mRelationshipNameLookup:Leai;

    invoke-interface {v0, p1}, Leai;->kI(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 548
    invoke-static {v0}, Lghy;->ak(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 549
    const/16 v0, 0xa

    invoke-virtual {p0, v2, v0}, Lghy;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v3

    .line 551
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 552
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alX()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 558
    :goto_0
    return v0

    .line 557
    :cond_1
    iget-object v0, p0, Lghy;->mRelationshipNameLookup:Leai;

    invoke-static {v3, v2, v0}, Lcom/google/android/search/shared/contact/Person;->a(Ljava/util/List;Ljava/util/List;Leai;)V

    .line 558
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
