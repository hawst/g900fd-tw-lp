.class abstract Lihx;
.super Lihb;
.source "PG"

# interfaces
.implements Liqr;


# static fields
.field private static final serialVersionUID:J = 0x67226fd4cd0928d8L


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lihb;-><init>(Ljava/util/Map;)V

    .line 45
    return-void
.end method


# virtual methods
.method abstract aWC()Ljava/util/Set;
.end method

.method public aWD()Ljava/util/Set;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihb;->aWn()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method synthetic aWm()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lihx;->aWC()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aWn()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lihx;->aWD()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public aWp()Ljava/util/Map;
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lihb;->aWp()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public synthetic bk(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lihx;->bo(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic bl(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lihx;->bn(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bn(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-super {p0, p1}, Lihb;->bl(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public bo(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 81
    invoke-super {p0, p1}, Lihb;->bk(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 128
    invoke-super {p0, p1}, Lihb;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public q(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Lihb;->q(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
