.class public final Ljez;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private egq:Z

.field private egr:Z

.field private egs:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16589
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 16590
    iput v1, p0, Ljez;->aez:I

    iput-boolean v1, p0, Ljez;->egq:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljez;->egr:Z

    iput-boolean v1, p0, Ljez;->egs:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljez;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljez;->eCz:I

    .line 16591
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 16513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljez;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljez;->egq:Z

    iget v0, p0, Ljez;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljez;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljez;->egr:Z

    iget v0, p0, Ljez;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljez;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljez;->egs:Z

    iget v0, p0, Ljez;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljez;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 16606
    iget v0, p0, Ljez;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 16607
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljez;->egq:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 16609
    :cond_0
    iget v0, p0, Ljez;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 16610
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljez;->egr:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 16612
    :cond_1
    iget v0, p0, Ljez;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 16613
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljez;->egs:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 16615
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 16616
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 16620
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 16621
    iget v1, p0, Ljez;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 16622
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljez;->egq:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16625
    :cond_0
    iget v1, p0, Ljez;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 16626
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljez;->egr:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16629
    :cond_1
    iget v1, p0, Ljez;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 16630
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljez;->egs:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16633
    :cond_2
    return v0
.end method
