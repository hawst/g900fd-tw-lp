.class public final Lhyq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aRO:Lhzc;

.field final aRU:Ligi;

.field private final aYZ:Lciu;

.field private final bDG:Lgrx;

.field private final cNN:Z

.field final duT:Licp;

.field private final mActionState:Ldbd;

.field final mContactLabelConverter:Ldyv;

.field final mContactLookup:Lghy;

.field private mContext:Landroid/content/Context;

.field private final mDeviceCapabilityManager:Lenm;

.field final mDiscourseContextSupplier:Ligi;

.field final mGsaConfigFlags:Lchk;

.field final mPersonShortcutManager:Lciy;

.field final mRelationshipManager:Lcjg;

.field private final mSearchUrlHelper:Lcpn;

.field private final mServiceState:Ldcu;


# direct methods
.method public constructor <init>(Ldbd;Ldcu;Landroid/content/Context;Ldyv;Lcha;Lciy;Lcke;Ligi;Lghy;Landroid/content/ContentResolver;Lenm;ZZLchk;Ligi;Lcrh;Lgrx;Lbxy;Libv;Lgpc;Licp;Lcpn;Ligi;Lcjg;Lciu;Lcin;Lbwo;Ldgm;)V
    .locals 30

    .prologue
    .line 156
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 157
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mActionState:Ldbd;

    .line 158
    invoke-static/range {p8 .. p8}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    invoke-static/range {p9 .. p9}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lghy;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->mContactLookup:Lghy;

    .line 160
    invoke-static/range {p10 .. p10}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mContext:Landroid/content/Context;

    .line 162
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mPersonShortcutManager:Lciy;

    .line 163
    invoke-static/range {p11 .. p11}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lenm;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->mDeviceCapabilityManager:Lenm;

    .line 164
    move/from16 v0, p13

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lhyq;->cNN:Z

    .line 165
    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mGsaConfigFlags:Lchk;

    .line 166
    invoke-static/range {p15 .. p15}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ligi;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->mDiscourseContextSupplier:Ligi;

    .line 167
    invoke-static/range {p16 .. p16}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    invoke-static/range {p17 .. p17}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgrx;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->bDG:Lgrx;

    .line 169
    invoke-static/range {p21 .. p21}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Licp;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->duT:Licp;

    .line 170
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mSearchUrlHelper:Lcpn;

    .line 171
    invoke-static/range {p23 .. p23}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ligi;

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->aRU:Ligi;

    .line 172
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mRelationshipManager:Lcjg;

    .line 173
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->aYZ:Lciu;

    .line 174
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mServiceState:Ldcu;

    .line 175
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lhyq;->mContactLabelConverter:Ldyv;

    .line 176
    new-instance v2, Lhzc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhyq;->mContext:Landroid/content/Context;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v4

    invoke-virtual {v4}, Lgql;->aJU()Lgpu;

    move-result-object v4

    invoke-virtual {v4}, Lgpu;->aJM()Ldmh;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v10, v0, Lhyq;->mGsaConfigFlags:Lchk;

    move-object/from16 v0, p0

    iget-object v12, v0, Lhyq;->mContactLookup:Lghy;

    move-object/from16 v0, p0

    iget-object v14, v0, Lhyq;->mDeviceCapabilityManager:Lenm;

    move-object/from16 v0, p0

    iget-object v0, v0, Lhyq;->mDiscourseContextSupplier:Ligi;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lhyq;->duT:Licp;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v22

    new-instance v23, Lgru;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhyq;->mContext:Landroid/content/Context;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v6

    iget-object v6, v6, Lgql;->mClock:Lemp;

    move-object/from16 v0, v23

    invoke-direct {v0, v5, v6}, Lgru;-><init>(Landroid/content/Context;Lemp;)V

    new-instance v24, Lgtj;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhyq;->mContext:Landroid/content/Context;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v6

    iget-object v6, v6, Lgql;->mClock:Lemp;

    move-object/from16 v0, v24

    invoke-direct {v0, v5, v6}, Lgtj;-><init>(Landroid/content/Context;Lemp;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lhyq;->aYZ:Lciu;

    move-object/from16 v26, v0

    new-instance v28, Lbye;

    invoke-direct/range {v28 .. v28}, Lbye;-><init>()V

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p24

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v11, p8

    move-object/from16 v13, p10

    move/from16 v15, p12

    move-object/from16 v17, p16

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v25, p26

    move-object/from16 v27, p27

    move-object/from16 v29, p28

    invoke-direct/range {v2 .. v29}, Lhzc;-><init>(Landroid/content/Context;Ldmh;Ldyv;Lcha;Lcjg;Lciy;Lcke;Lchk;Ligi;Lghy;Landroid/content/ContentResolver;Lenm;ZLigi;Lcrh;Lbxy;Libv;Lgpc;Licp;ILgru;Lgtj;Lcin;Lciu;Lbwo;Lbye;Ldgm;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhyq;->aRO:Lhzc;

    .line 188
    return-void
.end method

.method private a(Lcom/google/android/search/shared/actions/PlayMediaAction;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 268
    new-instance v0, Lhyr;

    invoke-direct {v0, p0, p1, p3, p2}, Lhyr;-><init>(Lhyq;Lcom/google/android/search/shared/actions/PlayMediaAction;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhyr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 288
    return-void
.end method

.method private b(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 842
    new-instance v0, Lcom/google/android/search/shared/actions/PuntAction;

    const v1, 0x7f0a064b

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IZ)V

    invoke-virtual {p0, p1, v0, p2}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    .line 843
    return v2
.end method

.method private gQ(Z)Z
    .locals 2

    .prologue
    .line 698
    if-eqz p1, :cond_0

    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 699
    const/16 v0, 0x9e

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 703
    :cond_0
    return p1
.end method

.method private hM(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lhyq;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->hM(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 848
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V

    .line 849
    return-void
.end method

.method final a(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 859
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 860
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/actions/VoiceAction;

    .line 861
    new-instance v0, Lhyw;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhyw;-><init>(Lhyq;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Ljava/util/List;)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhyw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 890
    :goto_0
    return-void

    .line 888
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lhyq;->b(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Z)Z
    .locals 10

    .prologue
    .line 203
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 204
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_11

    .line 207
    iget-object v0, v0, Lieb;->dzA:[Lidz;

    invoke-static {v0}, Lcll;->b([Lidz;)Lidz;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_11

    iget-object v1, v0, Lidz;->dzw:Lieh;

    invoke-virtual {v1}, Lieh;->aVE()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 210
    iget-object v1, p0, Lhyq;->mActionState:Ldbd;

    iget-object v2, p0, Lhyq;->mContext:Landroid/content/Context;

    iget-object v0, v0, Lidz;->dzw:Lieh;

    invoke-virtual {v0}, Lieh;->aVD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil;->n(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lhgo;->ag(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->F(Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Ldbd;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    .line 214
    iget-object v0, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {v0, p2}, Ldbd;->d(Lcom/google/android/velvet/ActionData;)V

    .line 215
    const/4 v0, 0x1

    .line 250
    :goto_0
    return v0

    .line 219
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v0

    iget-object v0, v0, Lieb;->dzA:[Lidz;

    invoke-static {v0}, Lcll;->c([Lidz;)Ljava/lang/String;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_1

    .line 222
    iget-object v1, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->kV(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Ldbd;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    .line 225
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v0

    iget-object v0, v0, Lieb;->dzA:[Lidz;

    invoke-static {v0}, Lcll;->a([Lidz;)Lidz;

    move-result-object v1

    .line 227
    if-eqz v1, :cond_11

    .line 228
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v0

    invoke-virtual {v0}, Lieb;->aVg()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    invoke-static {v1, p3, v2}, Lcll;->a(Lidz;ZLjava/lang/String;)Ljmh;

    move-result-object v0

    new-instance v1, Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;-><init>(Ljmh;)V

    invoke-direct {p0, v1, p2, p1}, Lhyq;->a(Lcom/google/android/search/shared/actions/PlayMediaAction;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 232
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aIH()Leiq;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 233
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aIH()Leiq;

    move-result-object v0

    new-instance v1, Lcom/google/android/search/shared/actions/PuntAction;

    invoke-static {v0}, Leno;->e(Leiq;)I

    move-result v0

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IZ)V

    invoke-virtual {p0, p2, v1, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto :goto_0

    .line 234
    :cond_4
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aIC()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aID()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 236
    :cond_5
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->getPeanut()Ljrp;

    move-result-object v1

    if-nez v1, :cond_6

    const-string v0, "ActionProcessor"

    const-string v1, "Unknown ActionServerResult type"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    :goto_2
    invoke-direct {p0, v0}, Lhyq;->gQ(Z)Z

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v0, v1, Ljrp;->eBr:Ljoh;

    if-eqz v0, :cond_a

    iget-object v0, v1, Ljrp;->eBr:Ljoh;

    sget-object v2, Ljpe;->exB:Ljsm;

    invoke-virtual {v0, v2}, Ljoh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_9

    sget-object v2, Ljpe;->exB:Ljsm;

    invoke-virtual {v0, v2}, Ljoh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpe;

    invoke-virtual {v0}, Ljpe;->oY()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_8

    iget-object v2, v0, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    if-lez v2, :cond_7

    invoke-virtual {v0}, Ljpe;->brF()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lhyq;->hM(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljpe;->yb(Ljava/lang/String;)Ljpe;

    new-instance v2, Lcom/google/android/search/shared/actions/LocalResultsAction;

    iget-object v3, p0, Lhyq;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v3}, Lenm;->auH()Z

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/search/shared/actions/LocalResultsAction;-><init>(Ljpe;Z)V

    invoke-virtual {p0, p2, v2, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    iget-object v0, v1, Ljrp;->eBt:[Ljkt;

    array-length v0, v0

    if-lez v0, :cond_12

    iget-object v0, v1, Ljrp;->eBt:[Ljkt;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    sget-object v2, Ljmd;->etp:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_d

    sget-object v2, Ljmd;->etp:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmd;

    iget-object v2, v1, Ljrp;->eBo:[Ljrs;

    array-length v2, v2

    if-lez v2, :cond_b

    iget-object v0, v1, Ljrp;->eBo:[Ljrs;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    move-object v1, v0

    :goto_5
    invoke-virtual {v1}, Ljrs;->btE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    :goto_6
    new-instance v2, Lcom/google/android/search/shared/actions/OpenUrlAction;

    invoke-virtual {v1}, Ljrs;->ahK()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljrs;->ahL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljrs;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/google/android/search/shared/actions/OpenUrlAction;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p0, p2, v2, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_b
    new-instance v1, Ljrs;

    invoke-direct {v1}, Ljrs;-><init>()V

    invoke-virtual {v0}, Ljmd;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljrs;->yB(Ljava/lang/String;)Ljrs;

    move-result-object v1

    invoke-virtual {v0}, Ljmd;->bpC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljrs;->yC(Ljava/lang/String;)Ljrs;

    move-result-object v1

    invoke-virtual {v0}, Ljmd;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljrs;->yD(Ljava/lang/String;)Ljrs;

    move-result-object v0

    move-object v1, v0

    goto :goto_5

    :cond_c
    invoke-virtual {v1}, Ljrs;->btE()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lhyq;->hM(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_6

    :cond_d
    sget-object v1, Ljms;->euw:Ljsm;

    invoke-virtual {v0, v1}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    iget-object v0, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apb()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ldbd;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {p0, p2, p1}, Lhyq;->c(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_e
    sget-object v1, Ljmt;->eux:Ljsm;

    invoke-virtual {v0, v1}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_f

    iget-object v0, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->ape()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ldbd;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {p0, p2, p1}, Lhyq;->c(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_f
    iget-object v1, p0, Lhyq;->aRO:Lhzc;

    new-instance v2, Lhyy;

    invoke-direct {v2, p0, p2, p1}, Lhyy;-><init>(Lhyq;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {v1, v0, p1, v2}, Lhzc;->a(Ljkt;Lcom/google/android/shared/search/Query;Lefk;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_10
    new-instance v0, Leir;

    const-string v1, "ActionV2 receieved that we can\'t handle"

    invoke-direct {v0, v1}, Leir;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Leir; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :catch_0
    move-exception v0

    .line 238
    const-string v1, "ActionProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in processing peanut: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Leir;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 239
    const v0, 0x665b5c    # 9.399994E-39f

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 250
    :cond_11
    :goto_7
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 236
    :cond_12
    :try_start_1
    invoke-virtual {v1}, Ljrp;->btz()I

    move-result v0

    const/4 v2, 0x3

    if-ne v2, v0, :cond_14

    iget-object v0, v1, Ljrp;->eBo:[Ljrs;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljrs;->btE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x0

    :goto_8
    new-instance v2, Lcom/google/android/search/shared/actions/OpenUrlAction;

    invoke-virtual {v1}, Ljrs;->ahK()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljrs;->ahL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljrs;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/google/android/search/shared/actions/OpenUrlAction;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p0, p2, v2, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_13
    invoke-virtual {v1}, Ljrs;->btE()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lhyq;->hM(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_8

    :cond_14
    const-string v1, "ActionProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled peanut with primary type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catch Leir; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v0, 0x0

    goto/16 :goto_2

    .line 241
    :cond_15
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aIG()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 243
    :try_start_2
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aIG()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v3

    if-eqz v3, :cond_30

    invoke-virtual {v3}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v2

    const-string v0, "CallContact"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "CallNumber"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    move v1, v0

    :goto_9
    const-string v0, "SendTextToContact"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v0, "AddRelationship"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const-string v0, "RemoveRelationship"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const-string v0, "ConfirmRelationship"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v1, :cond_17

    if-nez v4, :cond_17

    if-nez v5, :cond_17

    if-nez v6, :cond_17

    if-eqz v7, :cond_19

    :cond_17
    const/4 v0, 0x1

    :goto_a
    if-eqz v0, :cond_1b

    iget-object v0, p0, Lhyq;->aRO:Lhzc;

    iget-object v0, p0, Lhyq;->aRO:Lhzc;

    invoke-virtual {v0}, Lhzc;->aUv()Z

    iget-object v0, p0, Lhyq;->aRO:Lhzc;

    invoke-virtual {v0}, Lhzc;->aUw()Z

    move-result v0

    if-eqz v0, :cond_1a

    new-instance v0, Lcom/google/android/search/shared/actions/ContactOptInAction;

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v8, v9}, Lcom/google/android/search/shared/actions/ContactOptInAction;-><init>(ZLjava/lang/String;)V

    invoke-virtual {p0, p2, v0, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    :goto_b
    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_c
    invoke-direct {p0, v0}, Lhyq;->gQ(Z)Z

    move-result v0

    goto/16 :goto_0

    :cond_18
    const/4 v0, 0x0

    move v1, v0

    goto :goto_9

    :cond_19
    const/4 v0, 0x0

    goto :goto_a

    :cond_1a
    const/4 v0, 0x0

    goto :goto_b

    :cond_1b
    if-eqz v1, :cond_1d

    invoke-static {v3}, Licv;->c(Lcom/google/android/speech/embedded/TaggerResult;)V

    iget-object v0, p0, Lhyq;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-direct {p0, p2, p1}, Lhyq;->b(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    goto :goto_c

    :cond_1c
    new-instance v0, Lhys;

    invoke-direct {v0, p0, p1, v3, p2}, Lhys;-><init>(Lhyq;Lcom/google/android/shared/search/Query;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/velvet/ActionData;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhys;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto :goto_c

    :cond_1d
    const-string v0, "AmbiguousCommunicationAction"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lhyq;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-direct {p0, p2, p1}, Lhyq;->b(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    goto :goto_c

    :cond_1e
    new-instance v0, Lhyt;

    invoke-direct {v0, p0, p1, v3, p2}, Lhyt;-><init>(Lhyq;Lcom/google/android/shared/search/Query;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/velvet/ActionData;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhyt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto :goto_c

    :cond_1f
    if-eqz v4, :cond_21

    const-string v0, "SendTextToContact"

    invoke-static {v0, v3}, Licv;->a(Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;)V

    iget-object v0, p0, Lhyq;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-direct {p0, p2, p1}, Lhyq;->b(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    goto :goto_c

    :cond_20
    const-string v0, "Message"

    invoke-virtual {v3, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lhyu;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lhyu;-><init>(Lhyq;Lcom/google/android/shared/search/Query;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/velvet/ActionData;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhyu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto :goto_c

    :cond_21
    const-string v0, "SelectRecipient"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    iget-boolean v0, p0, Lhyq;->cNN:Z

    if-eqz v0, :cond_22

    new-instance v0, Lhyv;

    invoke-direct {v0, p0, p1, v3, p2}, Lhyv;-><init>(Lhyq;Lcom/google/android/shared/search/Query;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/velvet/ActionData;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhyv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_22
    if-eqz v5, :cond_24

    iget-object v0, p0, Lhyq;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FW()Z

    move-result v0

    if-eqz v0, :cond_23

    new-instance v0, Lhyz;

    invoke-direct {v0, p0, p2, p1}, Lhyz;-><init>(Lhyq;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhyz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_23
    const/4 v0, 0x0

    goto/16 :goto_c

    :cond_24
    if-eqz v6, :cond_26

    iget-object v0, p0, Lhyq;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FW()Z

    move-result v0

    if-eqz v0, :cond_25

    new-instance v0, Lhza;

    invoke-direct {v0, p0, p2, p1}, Lhza;-><init>(Lhyq;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhza;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_25
    const/4 v0, 0x0

    goto/16 :goto_c

    :cond_26
    if-eqz v7, :cond_2a

    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agy()Ldxl;

    move-result-object v1

    if-eqz v1, :cond_30

    invoke-virtual {v1}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-eqz v0, :cond_27

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v2

    if-nez v2, :cond_28

    :cond_27
    const/4 v0, 0x0

    goto/16 :goto_c

    :cond_28
    new-instance v2, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    sget-object v3, Lcgg;->aVF:Lcgg;

    invoke-virtual {v3}, Lcgg;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_29

    iget-object v3, p0, Lhyq;->aYZ:Lciu;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lciu;->a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    :goto_d
    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amw()V

    invoke-virtual {v1, v2}, Ldxl;->l(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_29
    iget-object v3, p0, Lhyq;->mRelationshipManager:Lcjg;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcjg;->a(Lcom/google/android/search/shared/contact/Relationship;Lcom/google/android/search/shared/contact/Person;)V
    :try_end_2
    .catch Leir; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_d

    .line 244
    :catch_1
    move-exception v0

    .line 245
    const-string v1, "ActionProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in processing PumpkinTagger result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Leir;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 247
    const v0, 0x665b5c    # 9.399994E-39f

    invoke-static {v0}, Lhwt;->lx(I)V

    goto/16 :goto_7

    .line 243
    :cond_2a
    :try_start_3
    const-string v0, "OpenApp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    new-instance v0, Ljmh;

    invoke-direct {v0}, Ljmh;-><init>()V

    new-instance v1, Ljmi;

    invoke-direct {v1}, Ljmi;-><init>()V

    const-string v2, "AppName"

    invoke-virtual {v3, v2}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljmi;->wW(Ljava/lang/String;)Ljmi;

    move-result-object v1

    iput-object v1, v0, Ljmh;->etD:Ljmi;

    new-instance v1, Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;-><init>(Ljmh;)V

    invoke-direct {p0, v1, p2, p1}, Lhyq;->a(Lcom/google/android/search/shared/actions/PlayMediaAction;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_2b
    const-string v0, "Cancel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-eqz v0, :cond_30

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agl()Z

    iget-object v1, p0, Lhyq;->mPersonShortcutManager:Lciy;

    invoke-virtual {v1, v0}, Lciy;->d(Lcom/google/android/search/shared/actions/VoiceAction;)V

    invoke-virtual {p0, p2, v0, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_2c
    iget-boolean v0, p0, Lhyq;->cNN:Z

    if-eqz v0, :cond_2f

    const-string v0, "Undo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Pr()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    :goto_e
    if-eqz v0, :cond_30

    invoke-virtual {p0, p2, v0, p1}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    const/4 v0, 0x1

    goto/16 :goto_c

    :cond_2d
    const-string v0, "Redo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Ps()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    goto :goto_e

    :cond_2e
    const-string v0, "Selection"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const-string v0, "Name"

    invoke-virtual {v3, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "Type"

    invoke-virtual {v3, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "Num"

    invoke-virtual {v3, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-static {v0, v1, v2, v3}, Lgth;->a(Lcky;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/search/shared/actions/VoiceAction;
    :try_end_3
    .catch Leir; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto :goto_e

    :cond_2f
    const/4 v0, 0x0

    goto :goto_e

    :cond_30
    const/4 v0, 0x0

    goto/16 :goto_c
.end method

.method final b(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V
    .locals 13

    .prologue
    .line 894
    iget-object v0, p0, Lhyq;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v4

    .line 895
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v9, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 901
    :goto_0
    invoke-virtual {v9}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agl()Z

    .line 904
    :cond_0
    iget-object v0, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {v0, p1, p2, v9}, Ldbd;->a(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    .line 910
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 912
    iget-object v0, p0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcky;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v6

    :goto_1
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {p1}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v0

    iget-object v8, v0, Ljyw;->eAp:Liwg;

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/velvet/ActionData;->aoM()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/google/android/velvet/ActionData;->aIA()I

    move-result v0

    if-lez v0, :cond_6

    const/4 v11, 0x1

    :goto_3
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Leqt;->H(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v5 .. v12}, Lcky;->a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;Liwg;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljava/lang/String;ZLjava/lang/String;)V

    .line 924
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Lcom/google/android/search/shared/service/ClientConfig;->ann()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v9}, Lcom/google/android/search/shared/actions/utils/CardDecision;->aln()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {v0}, Ldbd;->Vx()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 929
    iget-object v0, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apj()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ldbd;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    .line 931
    :cond_2
    return-void

    .line 895
    :cond_3
    iget-object v0, p0, Lhyq;->bDG:Lgrx;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v5, 0x0

    iget-object v2, p0, Lhyq;->mActionState:Ldbd;

    invoke-virtual {v2}, Ldbd;->VS()Z

    move-result v6

    move-object v2, p1

    move-object/from16 v3, p3

    invoke-virtual/range {v0 .. v6}, Lgrx;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/service/ClientConfig;ZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v9

    goto/16 :goto_0

    .line 912
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    :cond_6
    const/4 v11, 0x0

    goto :goto_3
.end method

.method final c(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 853
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1, v0, p2}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V

    .line 854
    return-void
.end method
