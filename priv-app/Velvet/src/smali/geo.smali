.class public Lgeo;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private final anB:I

.field private final bWw:Ljava/lang/String;

.field private final cHH:Ljava/io/InputStream;

.field private final cHI:I

.field private final cHJ:I

.field private final cHK:I

.field private final cHL:Ljava/nio/ByteBuffer;

.field private final cHM:Ljava/nio/ByteBuffer;

.field private cHN:Landroid/media/MediaCodec;

.field private cHO:[Ljava/nio/ByteBuffer;

.field private cHP:[Ljava/nio/ByteBuffer;

.field private cHQ:I

.field private cHR:I

.field private cHS:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;IIII)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 57
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lgeo;->cHQ:I

    .line 58
    iput p4, p0, Lgeo;->cHI:I

    .line 59
    iput-object p1, p0, Lgeo;->cHH:Ljava/io/InputStream;

    .line 60
    iput p3, p0, Lgeo;->anB:I

    .line 61
    iput p6, p0, Lgeo;->cHJ:I

    .line 62
    iput-object p2, p0, Lgeo;->bWw:Ljava/lang/String;

    .line 63
    const-string v0, "audio/mp4a-latm"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    iget v0, p0, Lgeo;->anB:I

    const/16 v3, 0x2b11

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 65
    iget v0, p0, Lgeo;->cHJ:I

    if-ne v0, v1, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 66
    iput v2, p0, Lgeo;->cHK:I

    .line 67
    const/4 v0, 0x7

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    .line 77
    :goto_2
    iget v0, p0, Lgeo;->cHI:I

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    .line 78
    iget-object v0, p0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    iget v1, p0, Lgeo;->cHI:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 80
    new-instance v0, Landroid/media/MediaFormat;

    invoke-direct {v0}, Landroid/media/MediaFormat;-><init>()V

    .line 81
    const-string v1, "mime"

    invoke-virtual {v0, v1, p2}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v1, "sample-rate"

    iget v2, p0, Lgeo;->anB:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 83
    const-string v1, "bitrate"

    invoke-virtual {v0, v1, p5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 84
    const-string v1, "channel-count"

    iget v2, p0, Lgeo;->cHJ:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 87
    :try_start_0
    invoke-direct {p0}, Lgeo;->aFo()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_6

    .line 90
    :try_start_1
    const-string v1, "OMX.google.aac.encoder"

    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lgeo;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 100
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 64
    goto :goto_0

    :cond_1
    move v1, v2

    .line 65
    goto :goto_1

    .line 68
    :cond_2
    const-string v0, "audio/amr-wb"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 69
    iget v0, p0, Lgeo;->anB:I

    const/16 v3, 0x3e80

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_4
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 70
    iget v0, p0, Lgeo;->cHJ:I

    if-ne v0, v1, :cond_3

    move v2, v1

    :cond_3
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 71
    iput v1, p0, Lgeo;->cHK:I

    .line 72
    const-string v0, "#!AMR-WB\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    goto :goto_2

    :cond_4
    move v0, v2

    .line 69
    goto :goto_4

    .line 74
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported audio codec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :catch_0
    move-exception v1

    :try_start_2
    invoke-direct {p0, p2, v0}, Lgeo;->a(Ljava/lang/String;Landroid/media/MediaFormat;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 97
    :catch_1
    move-exception v0

    .line 98
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create codec mimeType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 95
    :cond_6
    :try_start_3
    invoke-direct {p0, p2, v0}, Lgeo;->a(Ljava/lang/String;Landroid/media/MediaFormat;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3
.end method

.method private static a(JII)J
    .locals 6

    .prologue
    .line 304
    const-wide/16 v0, -0x1

    rsub-int/lit8 v2, p2, 0x40

    ushr-long/2addr v0, v2

    .line 305
    shl-long v2, p0, p2

    int-to-long v4, p3

    and-long/2addr v0, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 118
    :try_start_0
    iput-object p1, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    .line 119
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 120
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 121
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lgeo;->cHO:[Ljava/nio/ByteBuffer;

    .line 122
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lgeo;->cHP:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Could not create codec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :catch_0
    move-exception v0

    iput-object v4, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    .line 125
    iput-object v4, p0, Lgeo;->cHO:[Ljava/nio/ByteBuffer;

    .line 126
    iput-object v4, p0, Lgeo;->cHP:[Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/media/MediaFormat;)V
    .locals 1

    .prologue
    .line 112
    invoke-static {p1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 113
    invoke-direct {p0, v0, p2}, Lgeo;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V

    .line 114
    return-void
.end method

.method private aFo()Z
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lgeo;->cHK:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final aFp()I
    .locals 1

    .prologue
    .line 291
    iget v0, p0, Lgeo;->cHR:I

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lgeo;->cHH:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 137
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 138
    :cond_0
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    .line 139
    return-void
.end method

.method public finalize()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lgeo;->cHN:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {p0}, Lgeo;->close()V

    .line 298
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no one closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    return-void
.end method

.method public read()I
    .locals 2

    .prologue
    .line 249
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Single-byte read not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([B)I
    .locals 2

    .prologue
    .line 254
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lgeo;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 16

    .prologue
    .line 259
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lgeo;->cHS:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    move v9, v2

    .line 260
    :cond_0
    :goto_0
    if-nez v9, :cond_e

    move-object/from16 v0, p0

    iget v2, v0, Lgeo;->cHQ:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHP:[Ljava/nio/ByteBuffer;

    move-object/from16 v0, p0

    iget v3, v0, Lgeo;->cHQ:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-nez v2, :cond_e

    .line 265
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lgeo;->cHQ:I

    if-ltz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    move-object/from16 v0, p0

    iget v3, v0, Lgeo;->cHQ:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lgeo;->cHQ:I

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v3

    const/4 v2, -0x1

    if-eq v3, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgeo;->cHO:[Ljava/nio/ByteBuffer;

    aget-object v5, v4, v3

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    invoke-static {v4}, Lifv;->gY(Z)V

    :goto_1
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lgeo;->cHI:I

    if-ge v4, v6, :cond_5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lgeo;->cHS:Z

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lgeo;->cHI:I

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    invoke-virtual {v5, v6, v7, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    add-int/2addr v4, v7

    invoke-virtual {v6, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_1

    .line 259
    :cond_3
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_0

    .line 265
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lgeo;->cHH:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    move-object/from16 v0, p0

    iget-object v6, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgeo;->cHL:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    move-object/from16 v0, p0

    iget v6, v0, Lgeo;->cHR:I

    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lgeo;->cHR:I

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-lez v4, :cond_7

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    :cond_6
    :goto_2
    new-instance v4, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v4}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    const-wide/16 v6, 0x2710

    invoke-virtual {v2, v4, v6, v7}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v2

    const/4 v3, -0x2

    if-ne v2, v3, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v3

    move-object/from16 v0, p0

    iget v2, v0, Lgeo;->anB:I

    const-string v5, "sample-rate"

    invoke-virtual {v3, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    if-ne v2, v5, :cond_8

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lifv;->gY(Z)V

    move-object/from16 v0, p0

    iget v2, v0, Lgeo;->cHJ:I

    const-string v5, "channel-count"

    invoke-virtual {v3, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    if-ne v2, v5, :cond_9

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lifv;->gY(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->bWw:Ljava/lang/String;

    const-string v5, "mime"

    invoke-virtual {v3, v5}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lifv;->gY(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    const-wide/16 v6, 0x2710

    invoke-virtual {v2, v4, v6, v7}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v2

    move v3, v2

    :goto_5
    const/4 v2, -0x2

    if-eq v3, v2, :cond_a

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, Lifv;->gY(Z)V

    const/4 v2, -0x1

    if-eq v3, v2, :cond_0

    const/4 v2, -0x3

    if-ne v3, v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lgeo;->cHP:[Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    :cond_7
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lgeo;->cHS:Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x4

    invoke-virtual/range {v2 .. v8}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    :cond_a
    const/4 v2, 0x0

    goto :goto_6

    :cond_b
    const/4 v2, -0x1

    if-eq v3, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHN:Landroid/media/MediaCodec;

    iget v5, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v6, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget-wide v10, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->flags:I

    move-object/from16 v0, p0

    iput v3, v0, Lgeo;->cHQ:I

    invoke-direct/range {p0 .. p0}, Lgeo;->aFo()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/4 v7, 0x7

    if-lt v2, v7, :cond_d

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Lifv;->gY(Z)V

    const-wide/16 v10, 0x0

    const/16 v2, 0xc

    const/16 v7, 0xfff

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x2

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x1

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x2

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x4

    const/16 v7, 0xa

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x3

    const/4 v7, 0x1

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/16 v2, 0xd

    add-int/lit8 v7, v6, 0x7

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/16 v2, 0xb

    const/16 v7, 0x7ff

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/4 v2, 0x2

    const/4 v7, 0x0

    invoke-static {v10, v11, v2, v7}, Lgeo;->a(JII)J

    move-result-wide v10

    const/16 v2, 0x30

    ushr-long v12, v10, v2

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v2, v12

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x28

    ushr-long v12, v10, v2

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v2, v12

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x20

    ushr-long v12, v10, v2

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v2, v12

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x18

    ushr-long v12, v10, v2

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v2, v12

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x10

    ushr-long v12, v10, v2

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v2, v12

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v2, 0x8

    ushr-long v12, v10, v2

    const-wide/16 v14, 0xff

    and-long/2addr v12, v14

    long-to-int v2, v12

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    long-to-int v2, v10

    int-to-byte v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHP:[Ljava/nio/ByteBuffer;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    add-int v3, v5, v6

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto/16 :goto_0

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 268
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lgeo;->cHS:Z

    if-eqz v2, :cond_f

    .line 269
    const/4 v2, -0x1

    .line 285
    :goto_8
    return v2

    .line 272
    :cond_f
    const/4 v2, 0x0

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 274
    move-object/from16 v0, p0

    iget-object v2, v0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    move/from16 v0, p3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lgeo;->cHM:Ljava/nio/ByteBuffer;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 276
    add-int p2, p2, v2

    .line 277
    sub-int p3, p3, v2

    .line 280
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lgeo;->cHP:[Ljava/nio/ByteBuffer;

    move-object/from16 v0, p0

    iget v4, v0, Lgeo;->cHQ:I

    aget-object v3, v3, v4

    .line 281
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    invoke-static {v4}, Lifv;->gY(Z)V

    .line 283
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    move/from16 v0, p3

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 284
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1, v4}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 285
    add-int/2addr v2, v4

    goto :goto_8

    :cond_11
    move v3, v2

    goto/16 :goto_5
.end method
