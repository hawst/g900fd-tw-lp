.class public final Leda;
.super Ledc;
.source "PG"


# instance fields
.field private final bVm:Z

.field private bVn:Landroid/view/View;

.field private bVo:Landroid/view/View$OnLayoutChangeListener;


# direct methods
.method public constructor <init>(ZLedf;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ledc;-><init>(ZLedf;)V

    .line 24
    iput-boolean p1, p0, Leda;->bVm:Z

    .line 25
    return-void
.end method

.method static synthetic a(Leda;)Landroid/view/View;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Leda;->bVn:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final I(F)V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Leda;->hB:Landroid/view/View;

    iget-object v1, p0, Leda;->bVn:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Leda;->bVn:Landroid/view/View;

    iget-object v1, p0, Leda;->bVn:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Leda;->bVn:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setBottom(I)V

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-super {p0, p1}, Ledc;->I(F)V

    goto :goto_0
.end method

.method public final aC(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Leda;->bVn:Landroid/view/View;

    .line 29
    return-void
.end method

.method public final afE()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Ledc;->afE()V

    .line 81
    iget-object v0, p0, Leda;->hB:Landroid/view/View;

    iget-object v1, p0, Leda;->bVn:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p0, Leda;->bVn:Landroid/view/View;

    iget-object v1, p0, Leda;->bVo:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 84
    :cond_0
    return-void
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 33
    invoke-super {p0, p1}, Ledc;->setTarget(Ljava/lang/Object;)V

    .line 34
    iget-object v0, p0, Leda;->bVn:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 36
    const/4 v0, 0x2

    new-array v3, v0, [F

    const/4 v4, 0x0

    iget-boolean v0, p0, Leda;->bVm:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    aput v0, v3, v4

    const/4 v0, 0x1

    iget-boolean v4, p0, Leda;->bVm:Z

    if-eqz v4, :cond_2

    :goto_1
    aput v2, v3, v0

    invoke-virtual {p0, v3}, Leda;->setFloatValues([F)V

    .line 43
    new-instance v0, Ledb;

    invoke-direct {v0, p0}, Ledb;-><init>(Leda;)V

    iput-object v0, p0, Leda;->bVo:Landroid/view/View$OnLayoutChangeListener;

    .line 50
    iget-object v0, p0, Leda;->bVn:Landroid/view/View;

    iget-object v1, p0, Leda;->bVo:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 52
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 36
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Leda;->hB:Landroid/view/View;

    iget-object v1, p0, Leda;->bVn:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 63
    const-wide/16 v0, 0x190

    invoke-virtual {p0, v0, v1}, Leda;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 65
    :cond_0
    invoke-super {p0}, Ledc;->start()V

    .line 66
    return-void
.end method
