.class public final Ldel;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldeb;


# instance fields
.field private final bus:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Ldel;->bus:I

    .line 17
    return-void
.end method


# virtual methods
.method public final a(Ldem;ILdef;Lddw;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    iget v2, p0, Ldel;->bus:I

    invoke-virtual {p1, v2}, Ldem;->fN(I)Ljava/util/List;

    move-result-object v2

    .line 23
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 24
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 25
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 27
    invoke-interface {v0}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 28
    if-ge v1, p2, :cond_1

    .line 29
    invoke-interface {p4, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    .line 32
    add-int/lit8 v1, v1, 0x1

    .line 33
    goto :goto_1

    :cond_0
    move v0, v1

    .line 24
    goto :goto_0

    .line 35
    :cond_1
    iget v0, p0, Ldel;->bus:I

    invoke-virtual {p1, v0}, Ldem;->fK(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36
    invoke-interface {p4}, Lddw;->aaZ()V

    .line 38
    :cond_2
    return-void
.end method
