.class public final Lfha;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static W(Ljava/util/List;)Ljava/util/Map;
    .locals 4

    .prologue
    .line 83
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v1

    .line 84
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 85
    invoke-static {v0}, Lfgy;->g(Ljsr;)Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 88
    :cond_0
    return-object v1
.end method

.method public static a(Ljel;Ljel;)Ljga;
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 30
    new-instance v4, Ljga;

    invoke-direct {v4}, Ljga;-><init>()V

    .line 33
    new-instance v5, Ljel;

    invoke-direct {v5}, Ljel;-><init>()V

    .line 34
    sget-object v2, Lfgz;->crn:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v6, v2, v1

    .line 35
    invoke-static {p0, v6}, Lfgz;->a(Ljel;I)Ljava/lang/Object;

    move-result-object v7

    .line 36
    invoke-static {p1, v6}, Lfgz;->a(Ljel;I)Ljava/lang/Object;

    move-result-object v8

    .line 38
    if-eqz v8, :cond_0

    invoke-static {v7, v8}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 39
    invoke-static {v5, v6, v8}, Lfgz;->b(Ljel;ILjava/lang/Object;)V

    .line 34
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    :cond_1
    sget-object v6, Lfgz;->cro:[I

    array-length v7, v6

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_5

    aget v8, v6, v3

    .line 45
    invoke-static {p0, v8}, Lfgz;->b(Ljel;I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfha;->W(Ljava/util/List;)Ljava/util/Map;

    move-result-object v9

    .line 47
    invoke-static {p1, v8}, Lfgz;->b(Ljel;I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfha;->W(Ljava/util/List;)Ljava/util/Map;

    move-result-object v10

    .line 50
    const/4 v2, 0x0

    .line 51
    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 52
    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljsr;

    .line 53
    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 54
    if-eqz v0, :cond_2

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v12

    invoke-static {v1, v12}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 57
    if-nez v2, :cond_7

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    :goto_3
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_2

    .line 63
    :cond_3
    if-eqz v2, :cond_4

    .line 64
    invoke-static {v5, v8, v2}, Lfgz;->a(Ljel;ILjava/util/List;)V

    .line 44
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 70
    :cond_5
    invoke-static {v5}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_6

    .line 71
    iput-object v5, v4, Ljga;->ejk:Ljel;

    .line 74
    :cond_6
    return-object v4

    :cond_7
    move-object v1, v2

    goto :goto_3
.end method
