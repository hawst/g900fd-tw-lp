.class final Liht;
.super Liho;
.source "PG"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field private synthetic dCh:Lihb;


# direct methods
.method constructor <init>(Lihb;Ljava/lang/Object;Ljava/util/SortedSet;Liho;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Liho;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 658
    iput-object p1, p0, Liht;->dCh:Lihb;

    .line 659
    invoke-direct {p0, p1, p2, p3, p4}, Liho;-><init>(Lihb;Ljava/lang/Object;Ljava/util/Collection;Liho;)V

    .line 660
    return-void
.end method

.method private aWz()Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Liho;->dCu:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 668
    invoke-direct {p0}, Liht;->aWz()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, Liht;->aWt()V

    .line 674
    invoke-direct {p0}, Liht;->aWz()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    .prologue
    .line 685
    invoke-virtual {p0}, Liht;->aWt()V

    .line 686
    new-instance v0, Liht;

    iget-object v1, p0, Liht;->dCh:Lihb;

    iget-object v2, p0, Liho;->dCo:Ljava/lang/Object;

    invoke-direct {p0}, Liht;->aWz()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    iget-object v4, p0, Liho;->dCv:Liho;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, Liht;-><init>(Lihb;Ljava/lang/Object;Ljava/util/SortedSet;Liho;)V

    return-object v0

    :cond_0
    iget-object p0, p0, Liho;->dCv:Liho;

    goto :goto_0
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 679
    invoke-virtual {p0}, Liht;->aWt()V

    .line 680
    invoke-direct {p0}, Liht;->aWz()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    .prologue
    .line 693
    invoke-virtual {p0}, Liht;->aWt()V

    .line 694
    new-instance v0, Liht;

    iget-object v1, p0, Liht;->dCh:Lihb;

    iget-object v2, p0, Liho;->dCo:Ljava/lang/Object;

    invoke-direct {p0}, Liht;->aWz()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    iget-object v4, p0, Liho;->dCv:Liho;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, Liht;-><init>(Lihb;Ljava/lang/Object;Ljava/util/SortedSet;Liho;)V

    return-object v0

    :cond_0
    iget-object p0, p0, Liho;->dCv:Liho;

    goto :goto_0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5

    .prologue
    .line 701
    invoke-virtual {p0}, Liht;->aWt()V

    .line 702
    new-instance v0, Liht;

    iget-object v1, p0, Liht;->dCh:Lihb;

    iget-object v2, p0, Liho;->dCo:Ljava/lang/Object;

    invoke-direct {p0}, Liht;->aWz()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    iget-object v4, p0, Liho;->dCv:Liho;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, Liht;-><init>(Lihb;Ljava/lang/Object;Ljava/util/SortedSet;Liho;)V

    return-object v0

    :cond_0
    iget-object p0, p0, Liho;->dCv:Liho;

    goto :goto_0
.end method
