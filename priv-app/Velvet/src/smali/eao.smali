.class public final Leao;
.super Lerd;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lerd;-><init>()V

    .line 27
    iput-object p1, p0, Leao;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method private w(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 32
    .line 36
    :try_start_0
    iget-object v1, p0, Leao;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 37
    if-nez v4, :cond_1

    .line 38
    :try_start_1
    const-string v1, "Search.ContentImageLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t acquire content provider for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 39
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 59
    invoke-static {v0}, Lesp;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 60
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    :cond_0
    :goto_0
    return-object v0

    .line 41
    :cond_1
    :try_start_2
    const-string v1, "r"

    invoke-virtual {v4, p1, v1}, Landroid/content/ContentProviderClient;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 42
    if-nez v3, :cond_2

    .line 43
    :try_start_3
    const-string v1, "Search.ContentImageLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "openAssetFile() failed for "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 44
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 59
    invoke-static {v3}, Lesp;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 60
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 46
    :cond_2
    :try_start_4
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v2

    .line 47
    if-nez v2, :cond_3

    .line 48
    :try_start_5
    const-string v1, "Search.ContentImageLoader"

    const-string v5, "Failed to create input stream"

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 49
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 59
    invoke-static {v3}, Lesp;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 60
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 51
    :cond_3
    :try_start_6
    iget-object v1, p0, Leao;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v1, v5, v2, v6}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v0

    .line 58
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 59
    invoke-static {v3}, Lesp;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 60
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 52
    :catch_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    .line 55
    :goto_1
    :try_start_7
    const-string v5, "Search.ContentImageLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to load "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 56
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 59
    invoke-static {v3}, Lesp;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 60
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    goto/16 :goto_0

    .line 58
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 59
    invoke-static {v3}, Lesp;->a(Landroid/content/res/AssetFileDescriptor;)V

    .line 60
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    :cond_4
    throw v0

    .line 58
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    goto :goto_2

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_2

    :catchall_3
    move-exception v0

    goto :goto_2

    .line 52
    :catch_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    goto :goto_1

    :catch_2
    move-exception v1

    move-object v2, v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final clearCache()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 71
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic v(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Leao;->w(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
