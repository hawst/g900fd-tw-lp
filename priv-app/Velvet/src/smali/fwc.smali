.class public final Lfwc;
.super Lfus;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgan;Lgbr;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 62
    iput-object p4, p0, Lfwc;->mFifeImageUrlUtil:Lgan;

    .line 63
    return-void
.end method

.method private static D(Lizj;)Ljci;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 401
    iget-object v0, p0, Lizj;->dSB:Ljci;

    if-nez v0, :cond_0

    .line 402
    const-string v0, "QpPackageTrackingEntryAdapter"

    const-string v1, "Unexpected Entry without nearby list entry."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v0, 0x0

    .line 405
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lizj;->dSB:Ljci;

    goto :goto_0
.end method

.method private static a(Ljci;)I
    .locals 2

    .prologue
    const v0, 0x7f0b00b9

    .line 381
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljci;->bdJ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    invoke-virtual {p0}, Ljci;->getStatusCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 396
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 392
    :pswitch_1
    const v0, 0x7f0b00b7

    goto :goto_0

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljci;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 88
    invoke-virtual {p1}, Ljci;->pA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const v0, 0x7f0a0378

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljci;->qn()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a0373

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Landroid/content/Context;JZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 410
    invoke-static {p2, p3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a03fe

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p4, :cond_1

    const v0, 0x18012

    invoke-static {p1, p2, p3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x12

    invoke-static {p1, p2, p3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final A(Lizj;)Ljau;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p1, Lizj;->dSA:Ljau;

    return-object v0
.end method

.method public final C(Lizj;)Lanh;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370
    invoke-static {p1}, Lfwc;->D(Lizj;)Ljci;

    move-result-object v0

    .line 371
    invoke-virtual {v0}, Ljci;->bgt()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Ljci;->dMX:[Ljbg;

    const/16 v2, 0x4e

    invoke-static {v1, p1, v0, v2}, Lfsx;->a(Ljava/lang/String;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lfwc;->cCl:Lftx;

    invoke-virtual {v0, p1}, Lftx;->aS(Landroid/content/Context;)Lang;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 11

    .prologue
    const/16 v10, 0xf3

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 108
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 109
    const/16 v1, 0x1d

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 110
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 112
    invoke-static {p2}, Lfwc;->D(Lizj;)Ljci;

    move-result-object v1

    .line 114
    new-instance v2, Laof;

    invoke-direct {v2}, Laof;-><init>()V

    .line 117
    invoke-virtual {v1}, Ljci;->bgu()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bI(Ljava/lang/String;)Laof;

    .line 118
    invoke-virtual {v1}, Ljci;->bgw()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 119
    invoke-virtual {v1}, Ljci;->bgv()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bJ(Ljava/lang/String;)Laof;

    .line 121
    :cond_0
    invoke-virtual {v1}, Ljci;->pA()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 122
    invoke-virtual {v1}, Ljci;->qn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bK(Ljava/lang/String;)Laof;

    .line 123
    invoke-static {v1}, Lfwc;->a(Ljci;)I

    move-result v3

    invoke-virtual {v2, v3}, Laof;->cC(I)Laof;

    .line 125
    :cond_1
    invoke-virtual {v1}, Ljci;->bgm()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 126
    invoke-virtual {v1}, Ljci;->bgl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bL(Ljava/lang/String;)Laof;

    .line 128
    :cond_2
    invoke-virtual {v1}, Ljci;->bgk()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Ljci;->bgj()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 130
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljci;->bgj()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 132
    const v3, 0x7f0a017f

    new-array v6, v9, [Ljava/lang/Object;

    invoke-direct {p0, p1, v4, v5, v9}, Lfwc;->c(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v8

    invoke-virtual {p1, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bM(Ljava/lang/String;)Laof;

    .line 136
    :cond_3
    iget-object v3, v1, Ljci;->eax:[Ljch;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 137
    iget-object v1, v1, Ljci;->eax:[Ljch;

    aget-object v1, v1, v8

    iget-object v1, v1, Ljch;->aiS:Ljcn;

    .line 138
    if-eqz v1, :cond_4

    .line 139
    iget-object v3, p0, Lfwc;->mFifeImageUrlUtil:Lgan;

    const v4, 0x7f0d015c

    const v5, 0x7f0d015d

    invoke-virtual {v3, p1, v1, v4, v5}, Lgan;->a(Landroid/content/Context;Ljcn;II)Ljava/lang/String;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_4

    .line 142
    invoke-virtual {v2, v1}, Laof;->bN(Ljava/lang/String;)Laof;

    .line 147
    :cond_4
    new-array v1, v8, [I

    invoke-static {p2, v10, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 149
    if-eqz v1, :cond_5

    iget-object v3, v1, Liwk;->afA:Ljbj;

    if-eqz v3, :cond_5

    .line 150
    new-instance v3, Lfsa;

    invoke-direct {v3, v10}, Lfsa;-><init>(I)V

    const/4 v4, 0x0

    invoke-virtual {v3, p2, v1, v4}, Lfsa;->a(Lizj;Liwk;Ljei;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 152
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dSA:Ljau;

    iget-object v1, v1, Ljau;->dQX:Ljhe;

    if-eqz v1, :cond_5

    .line 153
    iget-object v1, v0, Lanh;->ahs:Lani;

    iget-object v1, v1, Lani;->ahH:Laom;

    iget-object v3, p0, Lfwc;->mStringEvaluator:Lgbr;

    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    iget-object v4, v4, Lizj;->dSA:Ljau;

    iget-object v4, v4, Ljau;->dQX:Ljhe;

    invoke-virtual {v3, p1, v4}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Laom;->bY(Ljava/lang/String;)Laom;

    .line 158
    :cond_5
    iput-object v2, v0, Lanh;->agW:Laof;

    .line 159
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;Lemp;)Lanh;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 235
    invoke-static {p2}, Lfwc;->D(Lizj;)Ljci;

    move-result-object v8

    .line 237
    invoke-virtual {v8}, Ljci;->bgx()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Ljci;->bgr()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    const/4 v0, 0x0

    .line 317
    :goto_0
    return-object v0

    .line 242
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 244
    invoke-virtual {v8}, Ljci;->bgk()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Ljci;->bgj()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 246
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8}, Ljci;->bgj()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lfwc;->c(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v0

    .line 248
    new-instance v1, Lfyg;

    invoke-direct {v1, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a017e

    invoke-virtual {v1, v2, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    .line 251
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_1
    invoke-virtual {v8}, Ljci;->bgq()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    const/4 v0, 0x0

    .line 256
    invoke-virtual {v8}, Ljci;->bgi()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v8}, Ljci;->bgh()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 258
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8}, Ljci;->bgh()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-interface {p3}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    const/high16 v6, 0x40000

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 265
    :cond_2
    new-instance v1, Lfyg;

    invoke-direct {v1, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const-string v2, " \u00b7 "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const v5, 0x7f0a017d

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, Ljci;->bgp()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    .line 270
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    :cond_3
    new-instance v7, Lanh;

    invoke-direct {v7}, Lanh;-><init>()V

    .line 274
    iput-object p2, v7, Lanh;->ahu:Lizj;

    .line 276
    invoke-virtual {v8}, Ljci;->bgs()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 277
    invoke-virtual {v8}, Ljci;->bgr()Ljava/lang/String;

    move-result-object v1

    .line 278
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 279
    new-instance v0, Lfsa;

    const/16 v2, 0x4d

    invoke-direct {v0, v2}, Lfsa;-><init>(I)V

    .line 281
    invoke-virtual {v8}, Ljci;->bgn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 282
    invoke-virtual {v8}, Ljci;->bgn()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iput v3, v0, Lfsa;->ceW:I

    iput-object v2, v0, Lfsa;->cBx:Ljava/lang/String;

    .line 286
    :goto_1
    invoke-virtual {v8}, Ljci;->bgy()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 287
    const-string v2, "wallet"

    invoke-virtual {v8}, Ljci;->bgx()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v6, Lgat;->cEw:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lfsa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v7, Lanh;->ahs:Lani;

    .line 301
    :cond_4
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 302
    new-instance v0, Laoj;

    invoke-direct {v0}, Laoj;-><init>()V

    .line 303
    invoke-virtual {v8}, Ljci;->bgx()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 304
    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Lanh;->cm(I)Lanh;

    .line 305
    iput-object v0, v7, Lanh;->agz:Laoj;

    move-object v0, v7

    .line 306
    goto/16 :goto_0

    .line 284
    :cond_5
    const v2, 0x7f02017d

    invoke-virtual {v0, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    goto :goto_1

    .line 294
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v7, Lanh;->ahs:Lani;

    goto :goto_2

    .line 309
    :cond_7
    new-instance v1, Laou;

    invoke-direct {v1}, Laou;-><init>()V

    .line 310
    invoke-virtual {v8}, Ljci;->bgx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    .line 311
    invoke-virtual {v8}, Ljci;->bgo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laou;->ct(Ljava/lang/String;)Laou;

    .line 312
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Laow;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laow;

    iput-object v0, v1, Laou;->akm:[Laow;

    .line 313
    const/16 v0, 0x10

    invoke-virtual {v7, v0}, Lanh;->cm(I)Lanh;

    .line 314
    iput-object v1, v7, Lanh;->agN:Laou;

    move-object v0, v7

    .line 317
    goto/16 :goto_0
.end method

.method public final aAw()Lizj;
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    const/16 v1, 0xf3

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-static {v0, v1, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    iget-object v0, v0, Liwk;->afA:Ljbj;

    if-eqz v0, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lfus;->aAw()Lizj;

    move-result-object v0

    goto :goto_0
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/16 v3, 0xf3

    .line 177
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-static {v1, v3, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_0

    iget-object v2, v1, Liwk;->afA:Ljbj;

    if-nez v2, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-object v0

    .line 182
    :cond_1
    new-instance v2, Lfsa;

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0200bc

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {v2, v3, v1, v0}, Lfsa;->a(Lizj;Liwk;Ljei;)Lani;

    move-result-object v1

    .line 185
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSA:Ljau;

    iget-object v0, v0, Ljau;->dQX:Ljhe;

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, v1, Lani;->ahH:Laom;

    iget-object v2, p0, Lfwc;->mStringEvaluator:Lgbr;

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iget-object v3, v3, Lizj;->dSA:Ljau;

    iget-object v3, v3, Ljau;->dQX:Ljhe;

    invoke-virtual {v2, p1, v3}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Laom;->bY(Ljava/lang/String;)Laom;

    .line 190
    :cond_2
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    .line 191
    const v0, 0x7f0a0180

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 192
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 193
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    .line 194
    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 195
    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 196
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljci;)I
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p2}, Lfwc;->a(Ljci;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 2

    .prologue
    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 165
    invoke-virtual {p0, p1, p2}, Lfwc;->c(Landroid/content/Context;Lizj;)Lanh;

    move-result-object v1

    invoke-static {v0, v1}, Lfwc;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 166
    iget-object v1, p0, Lfuz;->mClock:Lemp;

    invoke-virtual {p0, p1, p2, v1}, Lfwc;->a(Landroid/content/Context;Lizj;Lemp;)Lanh;

    move-result-object v1

    invoke-static {v0, v1}, Lfwc;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 167
    invoke-virtual {p0, p1, p2}, Lfwc;->d(Landroid/content/Context;Lizj;)Lanh;

    move-result-object v1

    invoke-static {v0, v1}, Lfwc;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 168
    invoke-virtual {p0, p2}, Lfwc;->C(Lizj;)Lanh;

    move-result-object v1

    invoke-static {v0, v1}, Lfwc;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 169
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lanh;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    return-object v0
.end method

.method public final c(Landroid/content/Context;Lizj;)Lanh;
    .locals 6

    .prologue
    .line 202
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 203
    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 204
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 206
    invoke-static {p2}, Lfwc;->D(Lizj;)Ljci;

    move-result-object v1

    .line 207
    new-instance v2, Laof;

    invoke-direct {v2}, Laof;-><init>()V

    .line 210
    invoke-virtual {v1}, Ljci;->bgu()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bI(Ljava/lang/String;)Laof;

    .line 211
    invoke-virtual {v1}, Ljci;->bgv()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bJ(Ljava/lang/String;)Laof;

    .line 212
    invoke-virtual {v1}, Ljci;->pA()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 213
    invoke-virtual {v1}, Ljci;->qn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laof;->bK(Ljava/lang/String;)Laof;

    .line 214
    invoke-static {v1}, Lfwc;->a(Ljci;)I

    move-result v3

    invoke-virtual {v2, v3}, Laof;->cC(I)Laof;

    .line 217
    :cond_0
    iget-object v3, v1, Ljci;->eax:[Ljch;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 218
    iget-object v1, v1, Ljci;->eax:[Ljch;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    iget-object v1, v1, Ljch;->aiS:Ljcn;

    .line 219
    if-eqz v1, :cond_1

    .line 220
    iget-object v3, p0, Lfwc;->mFifeImageUrlUtil:Lgan;

    const v4, 0x7f0d015c

    const v5, 0x7f0d015d

    invoke-virtual {v3, p1, v1, v4, v5}, Lgan;->a(Landroid/content/Context;Ljcn;II)Ljava/lang/String;

    move-result-object v1

    .line 222
    if-eqz v1, :cond_1

    .line 223
    invoke-virtual {v2, v1}, Laof;->bN(Ljava/lang/String;)Laof;

    .line 228
    :cond_1
    iput-object v2, v0, Lanh;->agW:Laof;

    .line 229
    return-object v0
.end method

.method public final d(Landroid/content/Context;Lizj;)Lanh;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 323
    invoke-static {p2}, Lfwc;->D(Lizj;)Ljci;

    move-result-object v3

    .line 324
    iget-object v0, v3, Ljci;->dOu:Ljbp;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 364
    :goto_0
    return-object v0

    .line 328
    :cond_0
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 329
    const/16 v0, 0x10

    invoke-virtual {v2, v0}, Lanh;->cm(I)Lanh;

    .line 330
    new-instance v4, Laou;

    invoke-direct {v4}, Laou;-><init>()V

    .line 332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 333
    invoke-virtual {v3}, Ljci;->bgz()Ljava/lang/String;

    .line 335
    iget-object v5, v3, Ljci;->dOu:Ljbp;

    if-eqz v5, :cond_1

    .line 336
    iget-object v5, v3, Ljci;->dOu:Ljbp;

    .line 337
    new-instance v6, Lfyg;

    invoke-direct {v6, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0a017c

    invoke-virtual {v5}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v5

    invoke-virtual {v5}, Lfyg;->aDR()Laow;

    move-result-object v5

    .line 340
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_1
    iget-object v5, v3, Ljci;->eaE:Ljcp;

    if-eqz v5, :cond_2

    .line 344
    iget-object v5, v3, Ljci;->eaE:Ljcp;

    .line 345
    new-instance v6, Lfyg;

    invoke-direct {v6, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Ljcp;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Ljcp;->getValue()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v5, v8}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    move-result-object v5

    invoke-virtual {v5}, Lfyg;->aDR()Laow;

    move-result-object v5

    .line 348
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Laow;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laow;

    iput-object v0, v4, Laou;->akm:[Laow;

    .line 353
    iget-object v0, v3, Ljci;->dOu:Ljbp;

    .line 354
    const/4 v3, 0x0

    invoke-static {v0, v1, v1, v3, v1}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_3

    .line 356
    new-instance v3, Lfsa;

    const/16 v5, 0xf2

    invoke-direct {v3, v5}, Lfsa;-><init>(I)V

    const v5, 0x7f0201b8

    invoke-virtual {v3, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v2, Lanh;->ahs:Lani;

    .line 362
    :cond_3
    iput-object p2, v2, Lanh;->ahu:Lizj;

    .line 363
    iput-object v4, v2, Lanh;->agN:Laou;

    move-object v0, v2

    .line 364
    goto/16 :goto_0
.end method
