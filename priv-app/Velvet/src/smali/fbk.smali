.class public abstract Lfbk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cnW:Z

.field cnX:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-boolean v0, p0, Lfbk;->cnW:Z

    .line 21
    iput-boolean v0, p0, Lfbk;->cnX:Z

    return-void
.end method

.method private d(Lizq;)V
    .locals 6
    .param p1    # Lizq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80
    if-nez p1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p1, Lizq;->dUW:[Lizq;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 86
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 87
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizq;

    .line 89
    iget-object v3, v0, Lizq;->dUZ:Lizj;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lizq;->dUZ:Lizj;

    invoke-static {v3}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v3

    iget-object v4, v0, Lizq;->dUZ:Lizj;

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Lfbk;->a(Lgbg;Lizj;Lizj;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 91
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 93
    :cond_2
    invoke-virtual {p0, v0}, Lfbk;->a(Lizq;)V

    .line 94
    invoke-direct {p0, v0}, Lfbk;->d(Lizq;)V

    goto :goto_1

    .line 97
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p1, Lizq;->dUW:[Lizq;

    array-length v2, v2

    if-eq v0, v2, :cond_4

    .line 98
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizq;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizq;

    iput-object v0, p1, Lizq;->dUW:[Lizq;

    .line 102
    :cond_4
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 103
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 104
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 105
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 106
    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v3

    .line 108
    iget-object v4, p1, Lizq;->dUZ:Lizj;

    invoke-virtual {p0, v3, v0, v4}, Lfbk;->a(Lgbg;Lizj;Lizj;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 111
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 113
    :cond_5
    invoke-virtual {p0, v3, v0}, Lfbk;->a(Lgbg;Lizj;)V

    goto :goto_2

    .line 116
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p1, Lizq;->dUX:[Lizj;

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 117
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizj;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizj;

    iput-object v0, p1, Lizq;->dUX:[Lizj;

    goto/16 :goto_0
.end method


# virtual methods
.method protected a(Lgbg;Lizj;)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method protected a(Lizq;)V
    .locals 0

    .prologue
    .line 141
    return-void
.end method

.method protected a(Lgbg;Lizj;Lizj;)Z
    .locals 1
    .param p3    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method protected d(Lizo;)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public final h(Lizo;)V
    .locals 1
    .param p1    # Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfbk;->cnW:Z

    .line 26
    if-nez p1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    invoke-virtual {p0, p1}, Lfbk;->d(Lizo;)V

    .line 31
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    .line 32
    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0, v0}, Lfbk;->a(Lizq;)V

    .line 34
    invoke-direct {p0, v0}, Lfbk;->d(Lizq;)V

    goto :goto_0
.end method

.method public final i(Lizo;)V
    .locals 1
    .param p1    # Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfbk;->cnX:Z

    .line 48
    invoke-virtual {p0, p1}, Lfbk;->h(Lizo;)V

    .line 49
    return-void
.end method

.method public final j(Lizo;)V
    .locals 1
    .param p1    # Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfbk;->cnX:Z

    .line 53
    invoke-virtual {p0, p1}, Lfbk;->h(Lizo;)V

    .line 54
    return-void
.end method
