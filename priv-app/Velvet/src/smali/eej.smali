.class public abstract Leej;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Leev;


# instance fields
.field private bWH:Landroid/widget/TextView;

.field private bWI:Landroid/widget/ImageView;

.field private bWJ:Landroid/view/View;

.field private bWK:Z

.field private bWL:Z

.field private bWM:Landroid/graphics/drawable/StateListDrawable;

.field private bWN:Landroid/graphics/drawable/StateListDrawable;

.field private bWO:Landroid/graphics/drawable/StateListDrawable;

.field private bWP:Landroid/graphics/drawable/StateListDrawable;

.field public bWQ:Lesm;

.field private bWR:Lcom/google/android/shared/search/Suggestion;

.field private bWS:Leew;

.field private bWT:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Leej;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Leej;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    const/4 v0, 0x1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Leej;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Leej;->bWT:F

    .line 76
    return-void
.end method

.method private d([F)Landroid/graphics/drawable/StateListDrawable;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 300
    const v0, 0x7f0b0079

    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v1, p1, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p0}, Leej;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 301
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 302
    sget-object v1, Leej;->PRESSED_STATE_SET:[I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 303
    sget-object v1, Leej;->FOCUSED_STATE_SET:[I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 304
    sget-object v1, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {p0}, Leej;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 306
    return-object v0
.end method


# virtual methods
.method public final a(Leew;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Leej;->bWS:Leew;

    .line 129
    return-void
.end method

.method public final a(Lesm;Leld;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Leej;->bWQ:Lesm;

    .line 98
    return-void
.end method

.method public a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 105
    iget-object v2, p0, Leej;->bWR:Lcom/google/android/shared/search/Suggestion;

    if-eq v2, p1, :cond_1

    .line 106
    new-instance v2, Leek;

    invoke-direct {v2, p0}, Leek;-><init>(Leej;)V

    invoke-virtual {p0, v2}, Leej;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-boolean v2, p0, Leej;->bWK:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Leej;->bWL:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Leej;->bWO:Landroid/graphics/drawable/StateListDrawable;

    if-nez v2, :cond_0

    new-array v2, v3, [F

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    iget v0, p0, Leej;->bWT:F

    aput v0, v2, v1

    iget v0, p0, Leej;->bWT:F

    aput v0, v2, v5

    const/4 v0, 0x3

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x4

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x5

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x6

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x7

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    invoke-direct {p0, v2}, Leej;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Leej;->bWO:Landroid/graphics/drawable/StateListDrawable;

    :cond_0
    iget-object v0, p0, Leej;->bWO:Landroid/graphics/drawable/StateListDrawable;

    :goto_0
    invoke-virtual {p0, v0}, Leej;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 108
    iput-object p1, p0, Leej;->bWR:Lcom/google/android/shared/search/Suggestion;

    move v0, v1

    .line 111
    :cond_1
    return v0

    .line 107
    :cond_2
    iget-boolean v2, p0, Leej;->bWK:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Leej;->bWM:Landroid/graphics/drawable/StateListDrawable;

    if-nez v2, :cond_3

    new-array v2, v3, [F

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    iget v0, p0, Leej;->bWT:F

    aput v0, v2, v1

    iget v0, p0, Leej;->bWT:F

    aput v0, v2, v5

    const/4 v0, 0x3

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x4

    aput v4, v2, v0

    const/4 v0, 0x5

    aput v4, v2, v0

    const/4 v0, 0x6

    aput v4, v2, v0

    const/4 v0, 0x7

    aput v4, v2, v0

    invoke-direct {p0, v2}, Leej;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Leej;->bWM:Landroid/graphics/drawable/StateListDrawable;

    :cond_3
    iget-object v0, p0, Leej;->bWM:Landroid/graphics/drawable/StateListDrawable;

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Leej;->bWL:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Leej;->bWN:Landroid/graphics/drawable/StateListDrawable;

    if-nez v2, :cond_5

    new-array v2, v3, [F

    aput v4, v2, v0

    aput v4, v2, v1

    aput v4, v2, v5

    const/4 v0, 0x3

    aput v4, v2, v0

    const/4 v0, 0x4

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x5

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x6

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    const/4 v0, 0x7

    iget v3, p0, Leej;->bWT:F

    aput v3, v2, v0

    invoke-direct {p0, v2}, Leej;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Leej;->bWN:Landroid/graphics/drawable/StateListDrawable;

    :cond_5
    iget-object v0, p0, Leej;->bWN:Landroid/graphics/drawable/StateListDrawable;

    goto :goto_0

    :cond_6
    iget-object v0, p0, Leej;->bWP:Landroid/graphics/drawable/StateListDrawable;

    if-nez v0, :cond_7

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-direct {p0, v0}, Leej;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Leej;->bWP:Landroid/graphics/drawable/StateListDrawable;

    :cond_7
    iget-object v0, p0, Leej;->bWP:Landroid/graphics/drawable/StateListDrawable;

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public final aG(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Leej;->bWK:Z

    .line 123
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Leej;->bWL:Z

    .line 124
    return-void

    :cond_0
    move v0, v2

    .line 121
    goto :goto_0

    :cond_1
    move v1, v2

    .line 123
    goto :goto_1
.end method

.method public final aod()Lcom/google/android/shared/search/Suggestion;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Leej;->bWR:Lcom/google/android/shared/search/Suggestion;

    return-object v0
.end method

.method protected final aoe()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Leej;->bWS:Leew;

    iget-object v1, p0, Leej;->bWR:Lcom/google/android/shared/search/Suggestion;

    invoke-interface {v0, v1}, Leew;->j(Lcom/google/android/shared/search/Suggestion;)V

    .line 216
    return-void
.end method

.method public final hn(I)V
    .locals 1

    .prologue
    .line 201
    if-eqz p1, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 203
    iget-object v0, p0, Leej;->bWJ:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 204
    return-void

    .line 201
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 89
    const v0, 0x7f110027

    invoke-virtual {p0, v0}, Leej;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leej;->bWH:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f110028

    invoke-virtual {p0, v0}, Leej;->findViewById(I)Landroid/view/View;

    .line 91
    const v0, 0x7f110417

    invoke-virtual {p0, v0}, Leej;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Leej;->bWI:Landroid/widget/ImageView;

    .line 92
    const v0, 0x7f1102e1

    invoke-virtual {p0, v0}, Leej;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leej;->bWJ:Landroid/view/View;

    .line 93
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 208
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 209
    iget-object v0, p0, Leej;->bWI:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 210
    iget-object v1, p0, Leej;->bWI:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 212
    :cond_0
    return-void

    .line 210
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroid/widget/RelativeLayout;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " suggestion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Leej;->bWR:Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final w(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Leej;->bWH:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    return-void
.end method
