.class public Lgoa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Leti;


# instance fields
.field private aUw:Lebk;

.field public final cQV:Ljava/util/Map;

.field private final cQW:Lgnv;

.field private final cQX:Ljava/util/List;

.field private mSettings:Lcke;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lgoa;->cQV:Ljava/util/Map;

    .line 56
    new-instance v0, Lgnv;

    invoke-direct {v0}, Lgnv;-><init>()V

    iput-object v0, p0, Lgoa;->cQW:Lgnv;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgoa;->cQX:Ljava/util/List;

    .line 190
    return-void
.end method

.method private aIo()V
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Lgoa;->cQV:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 179
    invoke-virtual {p0}, Lgoa;->aIn()Landroid/os/Message;

    move-result-object v1

    .line 180
    iget-object v0, p0, Lgoa;->cQV:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 183
    invoke-virtual {p0, v0, v1}, Lgoa;->a(Landroid/os/Messenger;Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 188
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 76
    const-string v1, "ssb_service:ssb_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "com.google.android.ssb.extra.SSB_CONTEXT"

    const-string v2, "ssb_service:ssb_context"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 81
    const-string v1, "ssb_service:ssb_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 83
    :cond_0
    iget-object v1, p0, Lgoa;->aUw:Lebk;

    invoke-virtual {v1, v0}, Lebk;->y(Landroid/os/Bundle;)V

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_1
    const-string v0, "SsbServiceImpl"

    const-string v1, "No data with prepareOverlay message"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lebk;Lcke;Lemp;)V
    .locals 2

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lgoa;->mSettings:Lcke;

    .line 67
    iput-object p1, p0, Lgoa;->aUw:Lebk;

    .line 69
    iget-object v0, p0, Lgoa;->mSettings:Lcke;

    invoke-interface {v0, p0}, Lcke;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 70
    iget-object v0, p0, Lgoa;->cQW:Lgnv;

    iget-object v1, p0, Lgoa;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgnv;->nm(Ljava/lang/String;)Lgnv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 223
    const-string v0, "SsbState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgoa;->cQW:Lgnv;

    invoke-virtual {v1}, Lgnv;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 224
    const-string v0, "HotwordEnabled"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgoa;->cQW:Lgnv;

    invoke-virtual {v1}, Lgnv;->aIc()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 225
    const-string v0, "Attached Clients"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgoa;->cQV:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 226
    iget-object v0, p0, Lgoa;->cQV:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    const-string v3, "package"

    invoke-virtual {v2, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 237
    :cond_0
    return-void
.end method

.method protected a(Landroid/os/Messenger;Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 149
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 151
    :catch_0
    move-exception v1

    const-string v1, "SsbServiceImpl"

    const-string v2, "Remote call sendSsbState failed."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final aIn()Landroid/os/Message;
    .locals 3

    .prologue
    .line 159
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 160
    const-string v1, "ssb_service:ssb_state"

    iget-object v2, p0, Lgoa;->cQW:Lgnv;

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 161
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 136
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ssb_service:ssb_package_is_google"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {p0}, Lgoa;->aIn()Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lgoa;->a(Landroid/os/Messenger;Landroid/os/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lgoa;->cQV:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ssb_service:ssb_package_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgoa;->aUw:Lebk;

    invoke-virtual {v0}, Lebk;->amK()V

    .line 112
    iget-object v0, p0, Lgoa;->mSettings:Lcke;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lgoa;->mSettings:Lcke;

    invoke-interface {v0, p0}, Lcke;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 114
    iget-object v0, p0, Lgoa;->cQW:Lgnv;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lgnv;->nm(Ljava/lang/String;)Lgnv;

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lgoa;->mSettings:Lcke;

    .line 117
    :cond_0
    iget-object v0, p0, Lgoa;->cQV:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 118
    iget-object v0, p0, Lgoa;->cQX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final fN(Z)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lgoa;->cQW:Lgnv;

    invoke-virtual {v0}, Lgnv;->aIc()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lgoa;->cQW:Lgnv;

    invoke-virtual {v0, p1}, Lgnv;->fM(Z)Lgnv;

    .line 174
    invoke-direct {p0}, Lgoa;->aIo()V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    const-string v0, "google_account"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lgoa;->cQW:Lgnv;

    iget-object v1, p0, Lgoa;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgnv;->nm(Ljava/lang/String;)Lgnv;

    .line 125
    invoke-direct {p0}, Lgoa;->aIo()V

    .line 127
    :cond_0
    return-void
.end method
