.class public final Lgmm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgmj;


# instance fields
.field private final aTA:Ljava/util/concurrent/Future;

.field private aTB:Ljava/util/concurrent/Future;

.field private final aTE:Lces;

.field private final aTr:Ljava/lang/String;

.field private final aTz:Ljava/util/concurrent/Future;

.field private final cHf:Lgni;

.field private final cPl:Ljava/util/concurrent/Future;

.field private final cPm:Lger;

.field private final cPn:Ljwe;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mRequestId:Ljava/lang/String;

.field private final mSessionParams:Lgnj;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lger;Lgni;Lgnj;)V
    .locals 8

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lgmm;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 57
    iput-object p2, p0, Lgmm;->cPm:Lger;

    .line 58
    iput-object p3, p0, Lgmm;->cHf:Lgni;

    .line 59
    new-instance v0, Lces;

    const-wide/16 v2, 0x3e8

    invoke-direct {v0, v2, v3}, Lces;-><init>(J)V

    iput-object v0, p0, Lgmm;->aTE:Lces;

    .line 61
    iput-object p4, p0, Lgmm;->mSessionParams:Lgnj;

    .line 62
    invoke-virtual {p4}, Lgnj;->tF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmm;->aTr:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmm;->mRequestId:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lgmm;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lgmr;

    iget-object v2, p0, Lgmm;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mNetworkInformation:Lgno;

    invoke-direct {v1, v2}, Lgmr;-><init>(Lgno;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmm;->aTz:Ljava/util/concurrent/Future;

    .line 65
    iget-object v0, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->aHC()Lgmx;

    move-result-object v0

    new-instance v1, Ljwe;

    invoke-direct {v1}, Ljwe;-><init>()V

    invoke-virtual {v0}, Lgmx;->getEncoding()I

    move-result v2

    invoke-virtual {v1, v2}, Ljwe;->sF(I)Ljwe;

    move-result-object v1

    invoke-virtual {v0}, Lgmx;->getSamplingRate()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Ljwe;->ac(F)Ljwe;

    move-result-object v0

    iput-object v0, p0, Lgmm;->cPn:Ljwe;

    .line 66
    iget-object v7, p0, Lgmm;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lgmt;

    iget-object v1, p0, Lgmm;->cHf:Lgni;

    iget-object v1, v1, Lgni;->mLocationHelper:Lglo;

    iget-object v2, p0, Lgmm;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mSpeechContext:Lgij;

    iget-object v3, p0, Lgmm;->cHf:Lgni;

    iget-object v3, v3, Lgni;->mSpeechSettings:Lgdo;

    iget-object v4, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v4}, Lgnj;->xU()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lgmm;->cHf:Lgni;

    iget-object v5, v5, Lgni;->mDeviceParams:Lgmz;

    iget-object v6, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v6}, Lgnj;->aHI()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lgmt;-><init>(Lglo;Lgij;Lgdo;Ljava/lang/String;Lgmz;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmm;->aTA:Ljava/util/concurrent/Future;

    .line 67
    invoke-direct {p0}, Lgmm;->aHq()Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmm;->aTB:Ljava/util/concurrent/Future;

    .line 68
    iget-object v0, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->getMode()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lgmm;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lgmw;

    iget-object v3, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->aHS()Z

    move-result v3

    iget-object v4, p0, Lgmm;->cHf:Lgni;

    iget-object v4, v4, Lgni;->mNetworkInformation:Lgno;

    iget-object v5, p0, Lgmm;->cHf:Lgni;

    iget-object v5, v5, Lgni;->mDeviceParams:Lgmz;

    invoke-direct {v2, v3, v4, v5, v0}, Lgmw;-><init>(ZLgno;Lgmz;I)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmm;->cPl:Ljava/util/concurrent/Future;

    .line 69
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aHq()Ljava/util/concurrent/Future;
    .locals 7

    .prologue
    .line 154
    iget-object v6, p0, Lgmm;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lgmm;->cHf:Lgni;

    iget-object v0, v0, Lgni;->mAuthTokenHelper:Lgln;

    iget-object v1, p0, Lgmm;->cHf:Lgni;

    iget-object v1, v1, Lgni;->mSpeechSettings:Lgdo;

    iget-object v2, p0, Lgmm;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mLocationHelper:Lglo;

    iget-object v3, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->aHG()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v5}, Lgnj;->aqW()Landroid/location/Location;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lgmv;->a(Lgln;Lgdo;Lglo;Ljava/lang/String;Ljava/util/List;Landroid/location/Location;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final Dl()Lgmk;
    .locals 13

    .prologue
    const/16 v12, 0x800

    const/4 v11, 0x1

    .line 75
    :try_start_0
    iget-object v0, p0, Lgmm;->cPm:Lger;

    invoke-interface {v0}, Lger;->df()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 79
    new-instance v0, Lgml;

    iget-object v1, p0, Lgmm;->aTz:Ljava/util/concurrent/Future;

    iget-object v2, p0, Lgmm;->aTA:Ljava/util/concurrent/Future;

    iget-object v3, p0, Lgmm;->aTB:Ljava/util/concurrent/Future;

    iget-object v4, p0, Lgmm;->cPl:Ljava/util/concurrent/Future;

    iget-object v5, p0, Lgmm;->cPn:Ljwe;

    iget-object v6, p0, Lgmm;->mRequestId:Ljava/lang/String;

    iget-object v7, p0, Lgmm;->aTr:Ljava/lang/String;

    iget-object v8, p0, Lgmm;->cHf:Lgni;

    iget-object v8, v8, Lgni;->mSpeechSettings:Lgdo;

    invoke-direct/range {v0 .. v8}, Lgml;-><init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljwe;Ljava/lang/String;Ljava/lang/String;Lgdo;)V

    .line 88
    new-instance v1, Lgmd;

    const-string v3, "audio/mp4a-latm"

    iget-object v2, p0, Lgmm;->mSessionParams:Lgnj;

    invoke-virtual {v2}, Lgnj;->aHC()Lgmx;

    move-result-object v2

    invoke-virtual {v2}, Lgmx;->getSamplingRate()I

    move-result v4

    const v8, 0x9c40

    const/16 v9, 0xf

    move-object v2, v10

    move v5, v11

    move v6, v12

    move v7, v12

    invoke-direct/range {v1 .. v9}, Lgmd;-><init>(Ljava/io/InputStream;Ljava/lang/String;IIIIII)V

    .line 97
    new-instance v2, Lgmk;

    const/4 v3, 0x2

    new-array v3, v3, [Lceq;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    aput-object v1, v3, v11

    invoke-direct {v2, v3}, Lgmk;-><init>([Lceq;)V

    return-object v2

    .line 76
    :catch_0
    move-exception v0

    .line 77
    new-instance v1, Leie;

    const v2, 0x10016

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 126
    iget-object v0, p0, Lgmm;->aTz:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 127
    iget-object v0, p0, Lgmm;->aTA:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 128
    iget-object v0, p0, Lgmm;->aTB:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 129
    iget-object v0, p0, Lgmm;->cPl:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 130
    return-void
.end method

.method public final refresh()V
    .locals 4

    .prologue
    .line 104
    :try_start_0
    iget-object v0, p0, Lgmm;->aTE:Lces;

    iget-object v1, p0, Lgmm;->aTB:Ljava/util/concurrent/Future;

    invoke-virtual {v0, v1}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwy;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    if-nez v0, :cond_0

    .line 113
    invoke-direct {p0}, Lgmm;->aHq()Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmm;->aTB:Ljava/util/concurrent/Future;

    .line 121
    :goto_1
    return-void

    .line 106
    :catch_0
    move-exception v0

    const-string v0, "SoundSearchRequestProducerFactory"

    const-string v1, "Could not get S3UserInfo for refresh."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 107
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_0
    iget-object v1, p0, Lgmm;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lgmm;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mAuthTokenHelper:Lgln;

    iget-object v3, p0, Lgmm;->cHf:Lgni;

    iget-object v3, v3, Lgni;->mSpeechSettings:Lgdo;

    invoke-static {v2, v0, v3}, Lgmv;->a(Lgln;Ljwy;Lgdo;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmm;->aTB:Ljava/util/concurrent/Future;

    goto :goto_1
.end method
