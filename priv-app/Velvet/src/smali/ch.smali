.class public abstract Lch;
.super Lck;
.source "PG"


# instance fields
.field volatile ds:Lci;

.field private volatile dt:Lci;

.field du:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lck;-><init>(Landroid/content/Context;)V

    .line 88
    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Lch;->du:J

    .line 93
    return-void
.end method


# virtual methods
.method final a(Lci;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0, p2}, Lch;->onCanceled(Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lch;->dt:Lci;

    if-ne v0, p1, :cond_1

    .line 203
    iget-boolean v0, p0, Lck;->dF:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lck;->dE:Z

    .line 204
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lch;->du:J

    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lch;->dt:Lci;

    .line 206
    invoke-virtual {p0}, Lch;->ai()V

    .line 208
    :cond_1
    return-void
.end method

.method final ai()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    .line 177
    iget-object v0, p0, Lch;->dt:Lci;

    if-nez v0, :cond_1

    iget-object v0, p0, Lch;->ds:Lci;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lch;->ds:Lci;

    iget-boolean v0, v0, Lci;->dv:Z

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lch;->ds:Lci;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lci;->dv:Z

    .line 180
    iget-object v0, p0, Lch;->ds:Lci;

    invoke-virtual {v6, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 182
    :cond_0
    cmp-long v0, v4, v4

    if-lez v0, :cond_2

    .line 183
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 184
    iget-wide v2, p0, Lch;->du:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 189
    iget-object v0, p0, Lch;->ds:Lci;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lci;->dv:Z

    .line 190
    iget-object v0, p0, Lch;->ds:Lci;

    iget-wide v2, p0, Lch;->du:J

    add-long/2addr v2, v4

    invoke-virtual {v6, v0, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 197
    :cond_1
    :goto_0
    return-void

    .line 195
    :cond_2
    iget-object v0, p0, Lch;->ds:Lci;

    sget-object v1, Lcr;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1, v6}, Lci;->a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lcr;

    goto :goto_0
.end method

.method public final cancelLoad()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 136
    iget-object v1, p0, Lch;->ds:Lci;

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lch;->dt:Lci;

    if-eqz v1, :cond_2

    .line 142
    iget-object v1, p0, Lch;->ds:Lci;

    iget-boolean v1, v1, Lci;->dv:Z

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lch;->ds:Lci;

    iput-boolean v0, v1, Lci;->dv:Z

    .line 144
    iget-object v1, p0, Lch;->ds:Lci;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 146
    :cond_0
    iput-object v2, p0, Lch;->ds:Lci;

    .line 166
    :cond_1
    :goto_0
    return v0

    .line 148
    :cond_2
    iget-object v1, p0, Lch;->ds:Lci;

    iget-boolean v1, v1, Lci;->dv:Z

    if-eqz v1, :cond_3

    .line 152
    iget-object v1, p0, Lch;->ds:Lci;

    iput-boolean v0, v1, Lci;->dv:Z

    .line 153
    iget-object v1, p0, Lch;->ds:Lci;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    iput-object v2, p0, Lch;->ds:Lci;

    goto :goto_0

    .line 157
    :cond_3
    iget-object v1, p0, Lch;->ds:Lci;

    invoke-virtual {v1, v0}, Lci;->cancel(Z)Z

    move-result v0

    .line 159
    if-eqz v0, :cond_4

    .line 160
    iget-object v1, p0, Lch;->ds:Lci;

    iput-object v1, p0, Lch;->dt:Lci;

    .line 162
    :cond_4
    iput-object v2, p0, Lch;->ds:Lci;

    goto :goto_0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 268
    invoke-super {p0, p1, p2, p3, p4}, Lck;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lch;->ds:Lci;

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lch;->ds:Lci;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 271
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lch;->ds:Lci;

    iget-boolean v0, v0, Lci;->dv:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 273
    :cond_0
    iget-object v0, p0, Lch;->dt:Lci;

    if-eqz v0, :cond_1

    .line 274
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCancellingTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lch;->dt:Lci;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 275
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lch;->dt:Lci;

    iget-boolean v0, v0, Lci;->dv:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 277
    :cond_1
    cmp-long v0, v2, v2

    if-eqz v0, :cond_2

    .line 278
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUpdateThrottle="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 279
    invoke-static {v2, v3, p3}, Leo;->a(JLjava/io/PrintWriter;)V

    .line 280
    const-string v0, " mLastLoadCompleteTime="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 281
    iget-wide v0, p0, Lch;->du:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p3}, Leo;->a(JJLjava/io/PrintWriter;)V

    .line 283
    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    .line 285
    :cond_2
    return-void
.end method

.method public abstract loadInBackground()Ljava/lang/Object;
.end method

.method public onCanceled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method protected final onForceLoad()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lck;->onForceLoad()V

    .line 112
    invoke-virtual {p0}, Lch;->cancelLoad()Z

    .line 113
    new-instance v0, Lci;

    invoke-direct {v0, p0}, Lci;-><init>(Lch;)V

    iput-object v0, p0, Lch;->ds:Lci;

    .line 115
    invoke-virtual {p0}, Lch;->ai()V

    .line 116
    return-void
.end method
