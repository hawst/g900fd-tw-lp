.class public Lhgf;
.super Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
.source "PG"

# interfaces
.implements Lhgh;


# instance fields
.field private dgf:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhgf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lhgf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method


# virtual methods
.method public final r(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lhgf;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lhgf;->dgf:Z

    .line 48
    return-void
.end method

.method public final s(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lhgf;->dgf:Z

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0, p1}, Lhgf;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 58
    :goto_0
    iget-boolean v0, p0, Lhgf;->dgf:Z

    return v0

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lhgf;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lhgf;->dgf:Z

    goto :goto_0
.end method

.method public final t(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lhgf;->dgf:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0, p1}, Lhgf;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p0, p1}, Lhgf;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public final u(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const v6, 0x800005

    const v5, 0x800003

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-virtual {p0, v2}, Lhgf;->K(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 78
    invoke-virtual {p0}, Lhgf;->getLeft()I

    move-result v3

    add-int/2addr v3, v2

    int-to-float v3, v3

    iget v4, p0, Lhgf;->cbI:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 80
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 81
    invoke-virtual {p0, v6}, Lhgf;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p0, v5}, Lhgf;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 87
    :cond_3
    int-to-float v2, v2

    invoke-virtual {p0}, Lhgf;->getRight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lhgf;->cbI:F

    sub-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    .line 89
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 90
    invoke-virtual {p0, v5}, Lhgf;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 92
    :cond_4
    invoke-virtual {p0, v6}, Lhgf;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 96
    goto :goto_0
.end method
