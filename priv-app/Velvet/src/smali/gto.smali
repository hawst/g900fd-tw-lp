.class public final Lgto;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cVa:I

.field public final cVb:I

.field public final cVc:I

.field public final cVd:I

.field public final cVe:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 6

    .prologue
    .line 59
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lgto;-><init>(IIIII)V

    .line 60
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    if-ltz p4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 65
    if-ltz p5, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 66
    iput p1, p0, Lgto;->cVa:I

    .line 67
    iput p2, p0, Lgto;->cVb:I

    .line 68
    iput p3, p0, Lgto;->cVc:I

    .line 69
    iput p4, p0, Lgto;->cVd:I

    .line 70
    iput p5, p0, Lgto;->cVe:I

    .line 71
    return-void

    :cond_0
    move v0, v2

    .line 64
    goto :goto_0

    :cond_1
    move v1, v2

    .line 65
    goto :goto_1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 76
    const-string v0, "ResourceSet[display=%s tts=%s eyesFreeTts=%s attempts=%s minItems=%s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lgto;->cVa:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lgto;->cVb:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lgto;->cVc:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lgto;->cVd:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lgto;->cVe:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
