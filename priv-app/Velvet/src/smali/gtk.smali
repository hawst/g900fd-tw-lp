.class public final Lgtk;
.super Lgrs;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lchk;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lgrs;-><init>(Landroid/content/res/Resources;Lchk;Z)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Ljrc;Z)Ljqb;
    .locals 13

    .prologue
    const v12, 0x7f0a05fe

    const/4 v11, 0x4

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 35
    invoke-static {p1, v5}, Lgtk;->a(Ljrc;I)Ljqg;

    move-result-object v1

    .line 37
    if-nez v1, :cond_0

    .line 38
    const-string v0, "PhoneCallActionPromptsBuilder"

    const-string v1, "Missing argument: -recipient"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    .line 41
    :cond_0
    new-instance v6, Ljqb;

    invoke-direct {v6}, Ljqb;-><init>()V

    .line 43
    invoke-static {v6, v1}, Lgtk;->a(Ljqb;Ljqg;)Ljqh;

    move-result-object v9

    sget-object v10, Ljqj;->eyK:Ljsm;

    sget-object v3, Lgqu;->cTp:Lgtl;

    sget-object v4, Lgqu;->cTj:Lgra;

    move-object v0, p0

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lgtk;->a(Ljqg;ZLgtl;Lgra;I)Ljqj;

    move-result-object v0

    invoke-virtual {v9, v10, v0}, Ljqh;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 49
    sget-object v0, Ljqi;->eyH:Ljsm;

    invoke-virtual {v1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqi;

    if-eqz v0, :cond_1

    iget-object v2, v0, Ljqi;->eyI:Ljoq;

    if-eqz v2, :cond_1

    iget-object v2, v0, Ljqi;->eyI:Ljoq;

    invoke-virtual {v2}, Ljoq;->oK()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Ljqi;->eyI:Ljoq;

    iget-object v2, v2, Ljoq;->ewM:Ljop;

    if-nez v2, :cond_1

    iget-object v2, v0, Ljqi;->eyI:Ljoq;

    iget-object v2, v2, Ljoq;->ewL:[Ljoj;

    array-length v2, v2

    if-ne v2, v7, :cond_1

    iget-object v0, v0, Ljqi;->eyI:Ljoq;

    iget-object v0, v0, Ljoq;->ewL:[Ljoj;

    aget-object v0, v0, v8

    iget-object v0, v0, Ljoj;->ews:[Ljon;

    array-length v0, v0

    if-ne v0, v7, :cond_1

    move v0, v7

    :goto_1
    if-eqz v0, :cond_2

    .line 51
    const v0, 0x7f0a0600

    new-array v2, v7, [Ljqu;

    invoke-static {v1, v11}, Lgtk;->a(Ljqg;I)Ljqu;

    move-result-object v1

    aput-object v1, v2, v8

    invoke-virtual {p0, v12, v0, v7, v2}, Lgtk;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v0

    iput-object v0, v6, Ljqb;->eyg:Ljrg;

    .line 65
    :goto_2
    const v0, 0x7f0a0969

    new-array v1, v8, [Ljqu;

    invoke-virtual {p0, v0, v8, v8, v1}, Lgtk;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v0

    iput-object v0, v6, Ljqb;->eyh:Ljrg;

    move-object v0, v6

    .line 70
    goto :goto_0

    :cond_1
    move v0, v8

    .line 49
    goto :goto_1

    .line 57
    :cond_2
    const v0, 0x7f0a05ff

    new-array v2, v5, [Ljqu;

    invoke-static {v1, v11}, Lgtk;->a(Ljqg;I)Ljqu;

    move-result-object v3

    aput-object v3, v2, v8

    const/4 v3, 0x5

    invoke-static {v1, v3}, Lgtk;->a(Ljqg;I)Ljqu;

    move-result-object v1

    aput-object v1, v2, v7

    invoke-virtual {p0, v12, v0, v7, v2}, Lgtk;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v0

    iput-object v0, v6, Ljqb;->eyg:Ljrg;

    goto :goto_2
.end method
