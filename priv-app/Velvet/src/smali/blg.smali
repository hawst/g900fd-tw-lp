.class public final Lblg;
.super Lbir;


# static fields
.field private static final aEI:Ljava/lang/Object;

.field private static final aEJ:Ljava/lang/Object;

.field private static final aEo:Lblv;


# instance fields
.field private aAg:D

.field private aAh:Z

.field private final aEA:Ljava/util/concurrent/atomic/AtomicLong;

.field private aEB:Ljava/lang/String;

.field private aEC:Ljava/lang/String;

.field private aED:Landroid/os/Bundle;

.field private aEE:Ljava/util/Map;

.field private aEF:Lbli;

.field private aEG:Lbhh;

.field private aEH:Lbhh;

.field private aEp:Lcom/google/android/gms/cast/ApplicationMetadata;

.field private final aEq:Lcom/google/android/gms/cast/CastDevice;

.field private final aEr:Ljava/util/Map;

.field private final aEs:J

.field private aEt:Lblj;

.field private aEu:Ljava/lang/String;

.field private aEv:Z

.field private aEw:Z

.field private aEx:Z

.field private aEy:I

.field private aEz:I

.field private final azy:Lbfm;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lblv;

    const-string v1, "CastClientImpl"

    invoke-direct {v0, v1}, Lblv;-><init>(Ljava/lang/String;)V

    sput-object v0, Lblg;->aEo:Lblv;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lblg;->aEI:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lblg;->aEJ:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLbfm;Lbhk;Lbhl;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move-object v4, p8

    invoke-direct/range {v0 .. v5}, Lbir;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;[Ljava/lang/String;)V

    iput-object p3, p0, Lblg;->aEq:Lcom/google/android/gms/cast/CastDevice;

    iput-object p6, p0, Lblg;->azy:Lbfm;

    iput-wide p4, p0, Lblg;->aEs:J

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lblg;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lblg;->aEr:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lblg;->aEA:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lblg;->aEE:Ljava/util/Map;

    invoke-direct {p0}, Lblg;->zl()V

    new-instance v0, Lbli;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbli;-><init>(Lblg;B)V

    iput-object v0, p0, Lblg;->aEF:Lbli;

    iget-object v0, p0, Lblg;->aEF:Lbli;

    invoke-virtual {p0, v0}, Lblg;->a(Lbhl;)V

    return-void
.end method

.method static synthetic a(Lblg;Lbhh;)Lbhh;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lblg;->aEG:Lbhh;

    return-object v0
.end method

.method static synthetic a(Lblg;Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 0

    iput-object p1, p0, Lblg;->aEp:Lcom/google/android/gms/cast/ApplicationMetadata;

    return-object p1
.end method

.method static synthetic a(Lblg;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lblg;->aEB:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lblg;)V
    .locals 0

    invoke-direct {p0}, Lblg;->zo()V

    return-void
.end method

.method static synthetic a(Lblg;Lcom/google/android/gms/internal/ig;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/ig;->zj()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lblg;->aEu:Ljava/lang/String;

    invoke-static {v0, v3}, Lblo;->h(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iput-object v0, p0, Lblg;->aEu:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, Lblg;->aEo:Lblv;

    const-string v4, "hasChanged=%b, mFirstApplicationStatusUpdate=%b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lblg;->aEv:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lblg;->azy:Lbfm;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lblg;->aEv:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lblg;->azy:Lbfm;

    invoke-virtual {v0}, Lbfm;->wL()V

    :cond_1
    iput-boolean v2, p0, Lblg;->aEv:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lblg;Lcom/google/android/gms/internal/il;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->xX()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    iput-object v0, p0, Lblg;->aEp:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->zw()D

    move-result-wide v4

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iget-wide v6, p0, Lblg;->aAg:D

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iput-wide v4, p0, Lblg;->aAg:D

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->zx()Z

    move-result v3

    iget-boolean v4, p0, Lblg;->aAh:Z

    if-eq v3, v4, :cond_0

    iput-boolean v3, p0, Lblg;->aAh:Z

    move v0, v1

    :cond_0
    sget-object v3, Lblg;->aEo:Lblv;

    const-string v4, "hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lblg;->aEw:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lblg;->azy:Lbfm;

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lblg;->aEw:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lblg;->azy:Lbfm;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->zy()I

    move-result v0

    iget v3, p0, Lblg;->aEy:I

    if-eq v0, v3, :cond_8

    iput v0, p0, Lblg;->aEy:I

    move v0, v1

    :goto_1
    sget-object v3, Lblg;->aEo:Lblv;

    const-string v4, "hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lblg;->aEw:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lblg;->azy:Lbfm;

    if-eqz v3, :cond_4

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lblg;->aEw:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lblg;->azy:Lbfm;

    iget v0, p0, Lblg;->aEy:I

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->zz()I

    move-result v0

    iget v3, p0, Lblg;->aEz:I

    if-eq v0, v3, :cond_7

    iput v0, p0, Lblg;->aEz:I

    move v0, v1

    :goto_2
    sget-object v3, Lblg;->aEo:Lblv;

    const-string v4, "hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lblg;->aEw:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lblg;->azy:Lbfm;

    if-eqz v1, :cond_6

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lblg;->aEw:Z

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lblg;->azy:Lbfm;

    iget v0, p0, Lblg;->aEz:I

    :cond_6
    iput-boolean v2, p0, Lblg;->aEw:Z

    return-void

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_0
.end method

.method static synthetic b(Lblg;Lbhh;)Lbhh;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lblg;->aEH:Lbhh;

    return-object v0
.end method

.method static synthetic b(Lblg;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lblg;->aEC:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lblg;)V
    .locals 0

    invoke-direct {p0}, Lblg;->zl()V

    return-void
.end method

.method static synthetic c(Lblg;)Lbhh;
    .locals 1

    iget-object v0, p0, Lblg;->aEG:Lbhh;

    return-object v0
.end method

.method static synthetic d(Lblg;)Lbfm;
    .locals 1

    iget-object v0, p0, Lblg;->azy:Lbfm;

    return-object v0
.end method

.method static synthetic e(Lblg;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lblg;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lblg;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lblg;->aEr:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lblg;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    iget-object v0, p0, Lblg;->aEq:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic h(Lblg;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lblg;->aEE:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i(Lblg;)Lbhh;
    .locals 1

    iget-object v0, p0, Lblg;->aEH:Lbhh;

    return-object v0
.end method

.method private zl()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v0, -0x1

    iput-boolean v2, p0, Lblg;->aEx:Z

    iput v0, p0, Lblg;->aEy:I

    iput v0, p0, Lblg;->aEz:I

    iput-object v1, p0, Lblg;->aEp:Lcom/google/android/gms/cast/ApplicationMetadata;

    iput-object v1, p0, Lblg;->aEu:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lblg;->aAg:D

    iput-boolean v2, p0, Lblg;->aAh:Z

    return-void
.end method

.method private zo()V
    .locals 3

    sget-object v0, Lblg;->aEo:Lblv;

    const-string v1, "removing all MessageReceivedCallbacks"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lblg;->aEr:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblg;->aEr:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private zp()V
    .locals 2

    iget-boolean v0, p0, Lblg;->aEx:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblg;->aEt:Lblj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblg;->aEt:Lblj;

    invoke-virtual {v0}, Lblj;->zu()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected to a device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic zq()Lblv;
    .locals 1

    sget-object v0, Lblg;->aEo:Lblv;

    return-object v0
.end method

.method static synthetic zr()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lblg;->aEI:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic zs()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lblg;->aEJ:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x3e9

    const/4 v0, 0x0

    const/4 v5, 0x1

    sget-object v1, Lblg;->aEo:Lblv;

    const-string v2, "in onPostInitHandler; statusCode=%d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    if-ne p1, v6, :cond_2

    :cond_0
    iput-boolean v5, p0, Lblg;->aEx:Z

    iput-boolean v5, p0, Lblg;->aEv:Z

    iput-boolean v5, p0, Lblg;->aEw:Z

    :goto_0
    if-ne p1, v6, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lblg;->aED:Landroid/os/Bundle;

    iget-object v1, p0, Lblg;->aED:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move p1, v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lbir;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_2
    iput-boolean v0, p0, Lblg;->aEx:Z

    goto :goto_0
.end method

.method protected final a(Lbjm;Lbiv;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v0, Lblg;->aEo:Lblv;

    const-string v1, "getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lblg;->aEB:Ljava/lang/String;

    aput-object v3, v2, v6

    const/4 v3, 0x1

    iget-object v4, p0, Lblg;->aEC:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lblg;->aEq:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/CastDevice;->k(Landroid/os/Bundle;)V

    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    iget-wide v2, p0, Lblg;->aEs:J

    invoke-virtual {v5, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lblg;->aEB:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_application_id"

    iget-object v1, p0, Lblg;->aEB:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lblg;->aEC:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_session_id"

    iget-object v1, p0, Lblg;->aEC:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lblj;

    invoke-direct {v0, p0, v6}, Lblj;-><init>(Lblg;B)V

    iput-object v0, p0, Lblg;->aEt:Lblj;

    const v2, 0x5d3f18

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lblg;->aEt:Lblj;

    invoke-virtual {v0}, Lblj;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lbjm;->a(Lbjj;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lbfn;)V
    .locals 2

    invoke-static {p1}, Lblo;->ft(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lblg;->fs(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lblg;->aEr:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblg;->aEr:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lblg;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lblq;

    invoke-interface {v0, p1}, Lblq;->fv(Ljava/lang/String;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lbhh;)V
    .locals 4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The message payload cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message exceeds maximum size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Lblo;->ft(Ljava/lang/String;)V

    invoke-direct {p0}, Lblg;->zp()V

    iget-object v0, p0, Lblg;->aEA:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    :try_start_0
    iget-object v0, p0, Lblg;->aEE:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lblg;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lblq;

    invoke-interface {v0, p1, p2, v2, v3}, Lblq;->b(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lblg;->aEE:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lbhh;)V
    .locals 6

    const/4 v5, 0x0

    sget-object v1, Lblg;->aEI:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblg;->aEG:Lbhh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblg;->aEG:Lbhh;

    new-instance v2, Lblh;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const/16 v4, 0x7d2

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v2, v3}, Lblh;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v0, v2}, Lbhh;->al(Ljava/lang/Object;)V

    :cond_0
    iput-object p3, p0, Lblg;->aEG:Lbhh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lblg;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lblq;

    invoke-interface {v0, v5, v5}, Lblq;->t(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lblr;->k(Landroid/os/IBinder;)Lblq;

    move-result-object v0

    return-object v0
.end method

.method public final disconnect()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lblg;->aEo:Lblv;

    const-string v1, "disconnect(); ServiceListener=%s, isConnected=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lblg;->aEt:Lblj;

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lblg;->isConnected()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lblg;->aEt:Lblj;

    const/4 v1, 0x0

    iput-object v1, p0, Lblg;->aEt:Lblj;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lblj;->zt()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lblg;->aEo:Lblv;

    const-string v1, "already disposed, so short-circuiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lblg;->zo()V

    :try_start_0
    invoke-virtual {p0}, Lblg;->isConnected()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lblg;->isConnecting()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lblg;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lblq;

    invoke-interface {v0}, Lblq;->disconnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-super {p0}, Lbir;->disconnect()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v1, Lblg;->aEo:Lblv;

    const-string v2, "Error while disconnecting the controller interface: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lblv;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lbir;->disconnect()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lbir;->disconnect()V

    throw v0
.end method

.method public final fs(Ljava/lang/String;)V
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel namespace cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lblg;->aEr:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblg;->aEr:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfn;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lblg;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lblq;

    invoke-interface {v0, p1}, Lblq;->fw(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    sget-object v1, Lblg;->aEo:Lblv;

    const-string v2, "Error unregistering namespace (%s): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lblv;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final xX()Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 1

    invoke-direct {p0}, Lblg;->zp()V

    iget-object v0, p0, Lblg;->aEp:Lcom/google/android/gms/cast/ApplicationMetadata;

    return-object v0
.end method

.method public final yE()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lblg;->aED:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblg;->aED:Landroid/os/Bundle;

    const/4 v1, 0x0

    iput-object v1, p0, Lblg;->aED:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lbir;->yE()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.internal.ICastDeviceController"

    return-object v0
.end method

.method public final zm()V
    .locals 1

    invoke-virtual {p0}, Lblg;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lblq;

    invoke-interface {v0}, Lblq;->zm()V

    return-void
.end method

.method public final zn()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lblg;->zp()V

    iget-object v0, p0, Lblg;->aEu:Ljava/lang/String;

    return-object v0
.end method
