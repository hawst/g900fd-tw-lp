.class public final Lgdh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgim;


# instance fields
.field private aTn:Z

.field private final bbd:Lenw;

.field private final cFD:Lggg;

.field private final cFT:Lgda;

.field private final cGK:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lggg;Lgda;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lgdh;->bbd:Lenw;

    .line 49
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lgdh;->cGK:Ljava/util/Map;

    .line 50
    iput-object p1, p0, Lgdh;->cFD:Lggg;

    .line 51
    iput-object p2, p0, Lgdh;->cFT:Lgda;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lehu;)V
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lgdh;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 74
    iget-boolean v0, p0, Lgdh;->aTn:Z

    if-eqz v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v0, p0, Lgdh;->cGK:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcz;

    if-eqz v0, :cond_1

    invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehu;

    invoke-interface {v0, v1}, Lgcz;->a(Lehu;)V

    goto :goto_0

    :cond_1
    const-string v0, "ResponseHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No RecognitionResponseProcessor found to handle response: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Leiq;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lgdh;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 98
    iget-boolean v0, p0, Lgdh;->aTn:Z

    if-eqz v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lgdh;->cFD:Lggg;

    invoke-interface {v0, p1}, Lggg;->a(Leiq;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;Lgcz;)V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lgdh;->cGK:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already have a RecognitionResponseProcessor for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    iget-object v0, p0, Lgdh;->cGK:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void
.end method

.method public final b(Leiq;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lgdh;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 110
    iget-boolean v0, p0, Lgdh;->aTn:Z

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lgdh;->cFD:Lggg;

    invoke-interface {v0, p1}, Lggg;->b(Leiq;)V

    goto :goto_0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lgdh;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdh;->aTn:Z

    .line 122
    iget-object v0, p0, Lgdh;->cFT:Lgda;

    invoke-interface {v0}, Lgda;->invalidate()V

    .line 123
    return-void
.end method
