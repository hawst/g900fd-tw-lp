.class public final Ldpg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bEG:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bEH:Lcom/google/android/search/shared/actions/VoiceAction;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mActionState:Ldbd;

.field private final mContext:Landroid/content/Context;

.field final mMessageManager:Lbxy;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mServiceState:Ldcu;

.field final mTtsState:Ldcw;

.field private final mVoiceSearchServices:Lhhq;


# direct methods
.method public constructor <init>(Lcom/google/android/search/core/state/QueryState;Ldcw;Ldbd;Ldcu;Lhhq;Lbxy;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 63
    iput-object p2, p0, Ldpg;->mTtsState:Ldcw;

    .line 64
    iput-object p3, p0, Ldpg;->mActionState:Ldbd;

    .line 65
    iput-object p4, p0, Ldpg;->mServiceState:Ldcu;

    .line 66
    iput-object p5, p0, Ldpg;->mVoiceSearchServices:Lhhq;

    .line 67
    iput-object p6, p0, Ldpg;->mMessageManager:Lbxy;

    .line 68
    iput-object p7, p0, Ldpg;->mContext:Landroid/content/Context;

    .line 69
    return-void
.end method

.method private static a(Ldbd;Lcom/google/android/shared/search/Query;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324
    invoke-virtual {p0, p1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {p0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIW()Ljava/lang/String;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lddb;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 72
    iget-object v0, p0, Ldpg;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amS()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    :cond_0
    iget-object v1, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v2, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v2

    iget-object v3, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3, v1}, Lcom/google/android/search/core/state/QueryState;->X(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    iget-object v4, p0, Ldpg;->mActionState:Ldbd;

    invoke-virtual {v4, v1}, Ldbd;->B(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiR()Z

    move-result v6

    if-eqz v6, :cond_6

    if-eqz v3, :cond_1

    if-nez v4, :cond_6

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->ani()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldpg;->bEG:Lcom/google/android/shared/search/Query;

    if-eq v1, v3, :cond_6

    iput-object v1, p0, Ldpg;->bEG:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Ldpg;->mTtsState:Ldcw;

    const/4 v3, 0x0

    invoke-virtual {v1, v5, v2, v8, v3}, Ldcw;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V

    .line 82
    :cond_2
    :goto_0
    iget-object v1, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 84
    iget-object v2, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ldcw;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Ldcw;->isDone()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Ldpg;->bEG:Lcom/google/android/shared/search/Query;

    if-eq v1, v3, :cond_3

    iput-object v1, p0, Ldpg;->bEG:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "notification-message"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v5, v3, v8, v8}, Ldcw;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V

    .line 87
    :cond_3
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amS()Z

    move-result v0

    if-nez v0, :cond_5

    .line 91
    iget-object v0, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v0, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v0}, Ldcw;->Zo()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Ldpg;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPm()Lhix;

    move-result-object v0

    iget-object v1, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v1}, Ldcw;->Zp()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lhix;->X([B)V

    new-instance v1, Ldph;

    invoke-direct {v1, p0, v3}, Ldph;-><init>(Ldpg;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {v0, v1}, Lhix;->a(Lhiy;)V

    .line 93
    :cond_5
    :goto_1
    return-void

    .line 79
    :cond_6
    iget-object v2, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2, v1}, Lcom/google/android/search/core/state/QueryState;->W(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Ldpg;->mActionState:Ldbd;

    invoke-virtual {v2, v1}, Ldbd;->B(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Ldpg;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2, v1}, Lcom/google/android/search/core/state/QueryState;->X(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    iget-object v3, p0, Ldpg;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0931

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqh()Z

    move-result v2

    if-nez v2, :cond_7

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anj()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Ldpg;->bEG:Lcom/google/android/shared/search/Query;

    if-eq v1, v2, :cond_8

    iput-object v1, p0, Ldpg;->bEG:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v1, v5, v3, v8, v8}, Ldcw;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v2, v1}, Ldcw;->at(Lcom/google/android/shared/search/Query;)V

    :cond_8
    iget-object v1, p0, Ldpg;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldpg;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Ldpg;->bEH:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eq v1, v2, :cond_2

    iput-object v1, p0, Ldpg;->bEH:Lcom/google/android/search/shared/actions/VoiceAction;

    iget-object v2, p0, Ldpg;->mActionState:Ldbd;

    invoke-virtual {v2, v1}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alf()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ale()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->Zv()Z

    move-result v2

    invoke-virtual {v3, v1, v4, v2, v8}, Ldcw;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_9
    iget-object v2, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v2, v1}, Ldcw;->t(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto/16 :goto_0

    .line 91
    :cond_a
    const/4 v1, 0x3

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v0}, Ldcw;->Zm()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "notification-sender"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_b
    iget-object v0, p0, Ldpg;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPj()Lice;

    move-result-object v7

    new-instance v0, Ldpi;

    const-string v2, "TTS done"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldpi;-><init>(Ldpg;Ljava/lang/String;Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    iget-object v1, p0, Ldpg;->mActionState:Ldbd;

    invoke-static {v1, v3}, Ldpg;->a(Ldbd;Lcom/google/android/shared/search/Query;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v6, v0, v8, v1}, Lice;->a(Ljava/lang/String;Lesk;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    if-ne v0, v8, :cond_5

    iget-object v0, p0, Ldpg;->mTtsState:Ldcw;

    invoke-virtual {v0}, Ldcw;->Zm()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldpg;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->aPj()Lice;

    move-result-object v1

    new-instance v2, Ldpj;

    const-string v4, "TTS done"

    invoke-direct {v2, p0, v4, v3}, Ldpj;-><init>(Ldpg;Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    iget-object v4, p0, Ldpg;->mActionState:Ldbd;

    invoke-static {v4, v3}, Ldpg;->a(Ldbd;Lcom/google/android/shared/search/Query;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lice;->a(Ljava/lang/String;Lesk;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
