.class public final Lcqw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bgI:Landroid/database/DataSetObservable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bgJ:Ldfw;

.field public final bgp:Landroid/database/DataSetObserver;

.field mClosed:Z

.field final mLoginHelper:Lcrh;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lcke;Lcrh;Landroid/database/DataSetObservable;)V
    .locals 2
    .param p3    # Landroid/database/DataSetObservable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcqw;->mSettings:Lcke;

    .line 56
    iput-object p2, p0, Lcqw;->mLoginHelper:Lcrh;

    .line 58
    new-instance v0, Lcqx;

    invoke-direct {v0, p0}, Lcqx;-><init>(Lcqw;)V

    iput-object v0, p0, Lcqw;->bgp:Landroid/database/DataSetObserver;

    .line 65
    invoke-direct {p0}, Lcqw;->Sn()Ldfw;

    move-result-object v0

    iput-object v0, p0, Lcqw;->bgJ:Ldfw;

    .line 66
    invoke-virtual {p0}, Lcqw;->Sm()V

    .line 68
    iget-object v0, p0, Lcqw;->mLoginHelper:Lcrh;

    iget-object v1, p0, Lcqw;->bgp:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 70
    iput-object p3, p0, Lcqw;->bgI:Landroid/database/DataSetObservable;

    .line 71
    return-void
.end method

.method private Sn()Ldfw;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Lcqw;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NR()Ljava/lang/String;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_0

    .line 133
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 134
    const-string v0, "source"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 135
    const-string v0, "account"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    const-string v0, "q"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    sget-object v5, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v5, v0}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v5

    .line 139
    new-instance v0, Ldfw;

    invoke-direct {v0, v3, v5, v4}, Ldfw;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    .line 141
    const-string v3, "suggestions"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 142
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 143
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 144
    new-instance v5, Lded;

    invoke-direct {v5}, Lded;-><init>()V

    iput-object v4, v5, Lded;->bux:Ljava/lang/CharSequence;

    const-string v6, "android.intent.action.WEB_SEARCH"

    iput-object v6, v5, Lded;->buE:Ljava/lang/String;

    iput-object v4, v5, Lded;->buJ:Ljava/lang/String;

    const/4 v4, 0x1

    iput-boolean v4, v5, Lded;->buN:Z

    const/16 v4, 0x19

    invoke-virtual {v5, v4}, Lded;->fI(I)Lded;

    move-result-object v4

    invoke-virtual {v4}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v4

    invoke-virtual {v0, v4}, Ldfw;->d(Lcom/google/android/shared/search/Suggestion;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcqw;->mSettings:Lcke;

    invoke-interface {v0, v1}, Lcke;->gR(Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    .line 158
    :cond_1
    return-object v0
.end method

.method private b(Ldfw;)Z
    .locals 2

    .prologue
    .line 94
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ldfw;->aby()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ldfw;->abx()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldfw;->abx()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcqw;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ldfw;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 164
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 165
    const-string v0, "source"

    invoke-virtual {p0}, Ldfw;->QJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 166
    const-string v0, "account"

    invoke-virtual {p0}, Ldfw;->abx()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 167
    const-string v0, "q"

    invoke-virtual {p0}, Ldfw;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 169
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 170
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ldfw;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 171
    invoke-virtual {p0, v0}, Ldfw;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_0
    const-string v0, "suggestions"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 175
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 179
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method final Sm()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcqw;->bgJ:Ldfw;

    invoke-direct {p0, v0}, Lcqw;->b(Ldfw;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcqw;->clearCache()V

    .line 91
    :cond_0
    return-void
.end method

.method public final c(Ldfw;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcqw;->b(Ldfw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iput-object p1, p0, Lcqw;->bgJ:Ldfw;

    .line 103
    iget-object v0, p0, Lcqw;->bgJ:Ldfw;

    invoke-static {v0}, Lcqw;->d(Ldfw;)Ljava/lang/String;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcqw;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->NR()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    iget-object v1, p0, Lcqw;->mSettings:Lcke;

    invoke-interface {v1, v0}, Lcke;->gR(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcqw;->bgI:Landroid/database/DataSetObservable;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcqw;->bgI:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 111
    :cond_0
    return-void
.end method

.method public final clearCache()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 115
    iput-object v1, p0, Lcqw;->bgJ:Ldfw;

    .line 116
    iget-object v0, p0, Lcqw;->mSettings:Lcke;

    invoke-interface {v0, v1}, Lcke;->gR(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public final ix(Ljava/lang/String;)Ldfw;
    .locals 1

    .prologue
    .line 74
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcqw;->bgJ:Ldfw;

    .line 78
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
