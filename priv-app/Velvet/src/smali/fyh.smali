.class public final Lfyh;
.super Lfro;
.source "PG"


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 28
    return-void
.end method

.method private static a(Laow;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 116
    .line 117
    iget-object v2, p0, Laow;->aks:[Laov;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 118
    iget-object v5, v4, Laov;->akp:[Ljava/lang/String;

    array-length v5, v5

    if-le v5, v0, :cond_0

    .line 119
    iget-object v0, v4, Laov;->akp:[Ljava/lang/String;

    array-length v0, v0

    .line 117
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_1
    return v0
.end method

.method private a(Laow;IZI)Landroid/widget/TableRow;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v7, Landroid/widget/TableRow;

    iget-object v0, p0, Lfyh;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 83
    iget-object v8, p1, Laow;->aks:[Laov;

    array-length v9, v8

    move v6, v2

    move v1, v5

    :goto_0
    if-ge v6, v9, :cond_5

    aget-object v10, v8, v6

    .line 84
    invoke-virtual {v10}, Laov;->rC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v10}, Laov;->rB()I

    move-result v0

    move v4, v0

    .line 86
    :goto_1
    if-eqz p3, :cond_1

    .line 87
    iget-object v0, p0, Lfyh;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f04012f

    invoke-virtual {v0, v3, v7, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v3, v0

    .line 92
    :goto_2
    if-nez p4, :cond_2

    .line 93
    invoke-virtual {v10}, Laov;->rA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :goto_3
    new-instance v10, Landroid/widget/TableRow$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {v10, v2, v0}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 101
    iput v4, v10, Landroid/widget/TableRow$LayoutParams;->span:I

    .line 103
    if-nez v1, :cond_4

    .line 104
    iget-object v0, p0, Lfyh;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d0136

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v10, Landroid/widget/TableRow$LayoutParams;->leftMargin:I

    move v0, v1

    .line 110
    :goto_4
    invoke-virtual {v7, v3, v10}, Landroid/widget/TableRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v4, v5

    .line 84
    goto :goto_1

    .line 90
    :cond_1
    iget-object v0, p0, Lfyh;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p2, v7, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v3, v0

    goto :goto_2

    .line 94
    :cond_2
    iget-object v0, v10, Laov;->akp:[Ljava/lang/String;

    array-length v0, v0

    if-lt v0, p4, :cond_3

    .line 95
    iget-object v0, v10, Laov;->akp:[Ljava/lang/String;

    add-int/lit8 v10, p4, -0x1

    aget-object v0, v0, v10

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 97
    :cond_3
    const-string v0, ""

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    move v0, v2

    .line 107
    goto :goto_4

    .line 112
    :cond_5
    return-object v7
.end method


# virtual methods
.method protected final aCZ()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 38
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 39
    const v0, 0x7f110311

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    .line 41
    iget-object v4, p0, Lfro;->cBc:Lanh;

    .line 42
    iget-object v4, v4, Lanh;->agN:Laou;

    .line 44
    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 46
    const v5, 0x7f1101a9

    invoke-virtual {v4}, Laou;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v5, v6}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 47
    const v5, 0x7f110080

    invoke-virtual {v4}, Laou;->ol()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v5, v6}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 48
    const v5, 0x7f1102f0

    invoke-virtual {v4}, Laou;->on()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v5, v6}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 51
    invoke-virtual {v4}, Laou;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v4}, Laou;->ol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 56
    :goto_0
    iget-object v7, v4, Laou;->akm:[Laow;

    array-length v8, v7

    move v6, v3

    move v5, v2

    :goto_1
    if-ge v6, v8, :cond_2

    aget-object v9, v7, v6

    .line 57
    new-instance v10, Landroid/widget/TableLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v11, -0x2

    invoke-direct {v10, v4, v11}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    const v11, 0x7f04012e

    if-eqz v5, :cond_0

    if-eqz v1, :cond_0

    move v4, v2

    :goto_2
    invoke-direct {p0, v9, v11, v4, v3}, Lfyh;->a(Laow;IZI)Landroid/widget/TableRow;

    move-result-object v4

    invoke-virtual {v0, v4, v10}, Landroid/widget/TableLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v4, v2

    :goto_3
    invoke-static {v9}, Lfyh;->a(Laow;)I

    move-result v11

    if-gt v4, v11, :cond_1

    const v11, 0x7f04012d

    invoke-direct {p0, v9, v11, v3, v4}, Lfyh;->a(Laow;IZI)Landroid/widget/TableRow;

    move-result-object v11

    invoke-virtual {v0, v11, v10}, Landroid/widget/TableLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_0
    move v4, v3

    goto :goto_2

    .line 58
    :cond_1
    if-eqz v5, :cond_3

    move v4, v3

    .line 56
    :goto_4
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_1

    .line 62
    :cond_2
    return-void

    :cond_3
    move v4, v5

    goto :goto_4

    :cond_4
    move v1, v3

    goto :goto_0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 32
    iput-object p1, p0, Lfyh;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 33
    const v0, 0x7f040130

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
