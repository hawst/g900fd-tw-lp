.class public final Legm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Legh;


# instance fields
.field public final aTs:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bYA:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bYu:I

.field public final bYv:Z

.field public final bYw:Ljava/lang/Integer;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bYx:Legb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bYy:Legk;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bYz:Z

.field public final mRequestId:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Legm;->bYu:I

    .line 52
    iput-boolean p2, p0, Legm;->bYv:Z

    .line 53
    iput-object p3, p0, Legm;->mRequestId:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Legm;->bYw:Ljava/lang/Integer;

    .line 55
    iput-object p5, p0, Legm;->bYx:Legb;

    .line 56
    iput-object p6, p0, Legm;->bYy:Legk;

    .line 57
    iput-boolean p7, p0, Legm;->bYz:Z

    .line 58
    iput-object p8, p0, Legm;->aTs:Ljava/lang/String;

    .line 59
    iput-object p9, p0, Legm;->bYA:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static a(Legb;)Legm;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 78
    new-instance v0, Legm;

    const/4 v1, -0x4

    move-object v4, v3

    move-object v5, p0

    move-object v6, v3

    move v7, v2

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Legk;)Legm;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 83
    new-instance v0, Legm;

    const/4 v1, -0x5

    move-object v4, v3

    move-object v5, v3

    move-object v6, p0

    move v7, v2

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static aoz()Legm;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 88
    new-instance v0, Legm;

    const/4 v1, -0x6

    const/4 v2, 0x0

    const/4 v7, 0x1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static hu(I)Legm;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 73
    new-instance v0, Legm;

    const/4 v1, -0x3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move v7, v2

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static kR(Ljava/lang/String;)Legm;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 68
    new-instance v0, Legm;

    const/4 v1, -0x2

    move-object v3, p0

    move-object v5, v4

    move-object v6, v4

    move v7, v2

    move-object v8, v4

    move-object v9, v4

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static kS(Ljava/lang/String;)Legm;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 93
    new-instance v0, Legm;

    const/4 v1, -0x7

    const/4 v2, 0x0

    const/4 v7, 0x1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v8, p0

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    const-string v1, "LogMetadata[mEventType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    iget v1, p0, Legm;->bYu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 108
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget-boolean v1, p0, Legm;->bYv:Z

    if-eqz v1, :cond_0

    .line 110
    const-string v1, "mClearRequestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    iget-boolean v1, p0, Legm;->bYv:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    iget-object v1, p0, Legm;->mRequestId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 115
    const-string v1, "mRequestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget-object v1, p0, Legm;->mRequestId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_1
    iget-object v1, p0, Legm;->bYw:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 120
    const-string v1, "mRequestType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    iget-object v1, p0, Legm;->bYw:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_2
    iget-object v1, p0, Legm;->bYx:Legb;

    if-eqz v1, :cond_3

    .line 125
    const-string v1, "mCookieSwitchData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    iget-object v1, p0, Legm;->bYx:Legb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_3
    iget-object v1, p0, Legm;->bYy:Legk;

    if-eqz v1, :cond_4

    .line 130
    const-string v1, "mGoogleAccountData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    iget-object v1, p0, Legm;->bYy:Legk;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 132
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_4
    iget-boolean v1, p0, Legm;->bYz:Z

    if-eqz v1, :cond_5

    .line 135
    const-string v1, "mClearApplicationIdAndTrigger="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    iget-boolean v1, p0, Legm;->bYz:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_5
    iget-object v1, p0, Legm;->aTs:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 140
    const-string v1, "mApplicationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    iget-object v1, p0, Legm;->aTs:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_6
    iget-object v1, p0, Legm;->bYA:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 145
    const-string v1, "mTriggerApplicationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    iget-object v1, p0, Legm;->bYA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_7
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
