.class public final Lfrj;
.super Landroid/graphics/drawable/ClipDrawable;
.source "PG"


# instance fields
.field private final cAP:Landroid/graphics/Rect;

.field private final sd:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 101
    const v0, 0x800003

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfrj;->cAP:Landroid/graphics/Rect;

    .line 102
    iput-object p1, p0, Lfrj;->sd:Landroid/graphics/drawable/Drawable;

    .line 103
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 115
    iget-object v0, p0, Lfrj;->cAP:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 116
    iget-object v0, p0, Lfrj;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 117
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 118
    return-void
.end method

.method public final g(IIII)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lfrj;->cAP:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 107
    iget-object v0, p0, Lfrj;->cAP:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 108
    iget-object v0, p0, Lfrj;->cAP:Landroid/graphics/Rect;

    iput p3, v0, Landroid/graphics/Rect;->right:I

    .line 109
    iget-object v0, p0, Lfrj;->cAP:Landroid/graphics/Rect;

    iput p4, v0, Landroid/graphics/Rect;->bottom:I

    .line 110
    return-void
.end method
