.class public final Liao;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private synthetic dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ui/LanguagePreference;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 150
    check-cast p2, Landroid/widget/CheckedTextView;

    .line 152
    invoke-virtual {p2}, Landroid/widget/CheckedTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 155
    iget-object v3, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3, v2}, Lgnq;->k(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v3

    .line 157
    if-nez v3, :cond_0

    .line 181
    :goto_0
    return v0

    .line 163
    :cond_0
    invoke-virtual {p2}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v4, v4, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget v5, v5, Lcom/google/android/voicesearch/ui/LanguagePreference;->drQ:I

    if-lt v4, v5, :cond_1

    .line 165
    invoke-virtual {p2, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 166
    iget-object v2, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a086f

    invoke-static {v2, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 168
    goto :goto_0

    .line 171
    :cond_1
    iget-object v0, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-virtual {v3}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    .line 172
    iget-object v0, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-virtual {v3}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-static {v0}, Lgnq;->an(Ljava/util/List;)V

    .line 177
    iget-object v0, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, p0, Liao;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v3, v3, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/voicesearch/ui/LanguagePreference;->a(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-virtual {p2, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    move v0, v1

    .line 181
    goto :goto_0
.end method
