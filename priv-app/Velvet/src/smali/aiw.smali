.class public final Laiw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static aar:Ljava/util/HashMap;

.field private static final aas:Ljava/util/HashMap;

.field private static final aat:Ljava/util/HashMap;


# instance fields
.field public ZU:I

.field public ZV:Ljava/lang/String;

.field public ZW:I

.field public ZX:I

.field public ZY:[I

.field public ZZ:I

.field public aaa:[I

.field public aab:I

.field public aac:[I

.field public aad:I

.field public aae:[I

.field public aaf:[I

.field public aag:I

.field public aah:[I

.field public aai:I

.field public aaj:[I

.field public aak:I

.field public aal:[I

.field public aam:I

.field public aan:[I

.field public aao:I

.field public aap:[I

.field public aaq:I

.field public count:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    const-string v0, "EventRecur"

    sput-object v0, Laiw;->TAG:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 84
    sput-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "FREQ"

    new-instance v2, Laji;

    invoke-direct {v2, v3}, Laji;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "UNTIL"

    new-instance v2, Lajk;

    invoke-direct {v2, v3}, Lajk;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "COUNT"

    new-instance v2, Lajh;

    invoke-direct {v2, v3}, Lajh;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "INTERVAL"

    new-instance v2, Lajj;

    invoke-direct {v2, v3}, Lajj;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYSECOND"

    new-instance v2, Lajd;

    invoke-direct {v2, v3}, Lajd;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYMINUTE"

    new-instance v2, Laja;

    invoke-direct {v2, v3}, Laja;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYHOUR"

    new-instance v2, Laiz;

    invoke-direct {v2, v3}, Laiz;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYDAY"

    new-instance v2, Laiy;

    invoke-direct {v2, v3}, Laiy;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYMONTHDAY"

    new-instance v2, Lajc;

    invoke-direct {v2, v3}, Lajc;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYYEARDAY"

    new-instance v2, Lajg;

    invoke-direct {v2, v3}, Lajg;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYWEEKNO"

    new-instance v2, Lajf;

    invoke-direct {v2, v3}, Lajf;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYMONTH"

    new-instance v2, Lajb;

    invoke-direct {v2, v3}, Lajb;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "BYSETPOS"

    new-instance v2, Laje;

    invoke-direct {v2, v3}, Laje;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    const-string v1, "WKST"

    new-instance v2, Lajl;

    invoke-direct {v2, v3}, Lajl;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 119
    sput-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "SECONDLY"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "MINUTELY"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "HOURLY"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "DAILY"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "WEEKLY"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "MONTHLY"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    const-string v1, "YEARLY"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 131
    sput-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "SU"

    const/high16 v2, 0x10000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "MO"

    const/high16 v2, 0x20000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "TU"

    const/high16 v2, 0x40000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "WE"

    const/high16 v2, 0x80000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "TH"

    const/high16 v2, 0x100000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "FR"

    const/high16 v2, 0x200000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    const-string v1, "SA"

    const/high16 v2, 0x400000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 954
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;I)V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Laiw;->aaf:[I

    aget v0, v0, p2

    .line 314
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 318
    :cond_0
    iget-object v0, p0, Laiw;->aae:[I

    aget v0, v0, p2

    invoke-static {v0}, Laiw;->bV(I)Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V
    .locals 3

    .prologue
    .line 300
    if-lez p2, :cond_1

    .line 301
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    add-int/lit8 v1, p2, -0x1

    .line 303
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 304
    aget v2, p3, v0

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 305
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_0
    aget v0, p3, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 309
    :cond_1
    return-void
.end method

.method private static a([II[II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 513
    if-eq p1, p3, :cond_1

    .line 522
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 517
    :goto_1
    if-ge v1, p1, :cond_2

    .line 518
    aget v2, p0, v1

    aget v3, p2, v1

    if-ne v2, v3, :cond_0

    .line 517
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 522
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static bT(I)I
    .locals 3

    .prologue
    .line 195
    packed-switch p0, :pswitch_data_0

    .line 212
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bad day of week: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :pswitch_0
    const/high16 v0, 0x10000

    .line 210
    :goto_0
    return v0

    .line 200
    :pswitch_1
    const/high16 v0, 0x20000

    goto :goto_0

    .line 202
    :pswitch_2
    const/high16 v0, 0x40000

    goto :goto_0

    .line 204
    :pswitch_3
    const/high16 v0, 0x80000

    goto :goto_0

    .line 206
    :pswitch_4
    const/high16 v0, 0x100000

    goto :goto_0

    .line 208
    :pswitch_5
    const/high16 v0, 0x200000

    goto :goto_0

    .line 210
    :pswitch_6
    const/high16 v0, 0x400000

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static bU(I)I
    .locals 3

    .prologue
    .line 217
    sparse-switch p0, :sswitch_data_0

    .line 234
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bad day of week: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :sswitch_0
    const/4 v0, 0x0

    .line 232
    :goto_0
    return v0

    .line 222
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 224
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 226
    :sswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 228
    :sswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 230
    :sswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 232
    :sswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 217
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x40000 -> :sswitch_2
        0x80000 -> :sswitch_3
        0x100000 -> :sswitch_4
        0x200000 -> :sswitch_5
        0x400000 -> :sswitch_6
    .end sparse-switch
.end method

.method private static bV(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    sparse-switch p0, :sswitch_data_0

    .line 293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bad day argument: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :sswitch_0
    const-string v0, "SU"

    .line 291
    :goto_0
    return-object v0

    .line 281
    :sswitch_1
    const-string v0, "MO"

    goto :goto_0

    .line 283
    :sswitch_2
    const-string v0, "TU"

    goto :goto_0

    .line 285
    :sswitch_3
    const-string v0, "WE"

    goto :goto_0

    .line 287
    :sswitch_4
    const-string v0, "TH"

    goto :goto_0

    .line 289
    :sswitch_5
    const-string v0, "FR"

    goto :goto_0

    .line 291
    :sswitch_6
    const-string v0, "SA"

    goto :goto_0

    .line 277
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x40000 -> :sswitch_2
        0x80000 -> :sswitch_3
        0x100000 -> :sswitch_4
        0x200000 -> :sswitch_5
        0x400000 -> :sswitch_6
    .end sparse-switch
.end method

.method static synthetic mc()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Laiw;->aas:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic md()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Laiw;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic me()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Laiw;->aat:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 527
    if-ne p0, p1, :cond_1

    .line 535
    :cond_0
    :goto_0
    return v0

    .line 530
    :cond_1
    instance-of v2, p1, Laiw;

    if-nez v2, :cond_2

    move v0, v1

    .line 531
    goto :goto_0

    .line 534
    :cond_2
    check-cast p1, Laiw;

    .line 535
    iget v2, p0, Laiw;->ZU:I

    iget v3, p1, Laiw;->ZU:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Laiw;->ZV:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Laiw;->ZV:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Laiw;->count:I

    iget v3, p1, Laiw;->count:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Laiw;->ZW:I

    iget v3, p1, Laiw;->ZW:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Laiw;->ZX:I

    iget v3, p1, Laiw;->ZX:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Laiw;->ZY:[I

    iget v3, p0, Laiw;->ZZ:I

    iget-object v4, p1, Laiw;->ZY:[I

    iget v5, p1, Laiw;->ZZ:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aaa:[I

    iget v3, p0, Laiw;->aab:I

    iget-object v4, p1, Laiw;->aaa:[I

    iget v5, p1, Laiw;->aab:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aac:[I

    iget v3, p0, Laiw;->aad:I

    iget-object v4, p1, Laiw;->aac:[I

    iget v5, p1, Laiw;->aad:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aae:[I

    iget v3, p0, Laiw;->aag:I

    iget-object v4, p1, Laiw;->aae:[I

    iget v5, p1, Laiw;->aag:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aaf:[I

    iget v3, p0, Laiw;->aag:I

    iget-object v4, p1, Laiw;->aaf:[I

    iget v5, p1, Laiw;->aag:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aah:[I

    iget v3, p0, Laiw;->aai:I

    iget-object v4, p1, Laiw;->aah:[I

    iget v5, p1, Laiw;->aai:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aaj:[I

    iget v3, p0, Laiw;->aak:I

    iget-object v4, p1, Laiw;->aaj:[I

    iget v5, p1, Laiw;->aak:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aal:[I

    iget v3, p0, Laiw;->aam:I

    iget-object v4, p1, Laiw;->aal:[I

    iget v5, p1, Laiw;->aam:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aan:[I

    iget v3, p0, Laiw;->aao:I

    iget-object v4, p1, Laiw;->aan:[I

    iget v5, p1, Laiw;->aao:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laiw;->aap:[I

    iget v3, p0, Laiw;->aaq:I

    iget-object v4, p1, Laiw;->aap:[I

    iget v5, p1, Laiw;->aaq:I

    invoke-static {v2, v3, v4, v5}, Laiw;->a([II[II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Laiw;->ZV:Ljava/lang/String;

    iget-object v3, p1, Laiw;->ZV:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 556
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final lZ()Z
    .locals 5

    .prologue
    const/4 v3, 0x5

    const/4 v0, 0x0

    .line 399
    iget v1, p0, Laiw;->ZU:I

    if-eq v1, v3, :cond_1

    .line 415
    :cond_0
    :goto_0
    return v0

    .line 403
    :cond_1
    iget v2, p0, Laiw;->aag:I

    .line 404
    if-ne v2, v3, :cond_0

    move v1, v0

    .line 408
    :goto_1
    if-ge v1, v2, :cond_2

    .line 409
    iget-object v3, p0, Laiw;->aae:[I

    aget v3, v3, v1

    .line 410
    const/high16 v4, 0x10000

    if-eq v3, v4, :cond_0

    const/high16 v4, 0x400000

    if-eq v3, v4, :cond_0

    .line 408
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 415
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ma()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 465
    iget v2, p0, Laiw;->ZU:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    .line 477
    :cond_0
    :goto_0
    return v0

    .line 469
    :cond_1
    iget v2, p0, Laiw;->aag:I

    if-ne v2, v1, :cond_0

    iget v2, p0, Laiw;->aai:I

    if-nez v2, :cond_0

    .line 473
    iget-object v2, p0, Laiw;->aaf:[I

    aget v2, v2, v0

    if-gtz v2, :cond_2

    iget-object v2, p0, Laiw;->aaf:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 477
    goto :goto_0
.end method

.method public final mb()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 487
    iget v2, p0, Laiw;->ZU:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    .line 499
    :cond_0
    :goto_0
    return v0

    .line 491
    :cond_1
    iget v2, p0, Laiw;->aai:I

    if-ne v2, v1, :cond_0

    iget v2, p0, Laiw;->aag:I

    if-nez v2, :cond_0

    .line 495
    iget-object v2, p0, Laiw;->aah:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 499
    goto :goto_0
.end method

.method public final parse(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 648
    const/4 v0, 0x0

    iput-object v0, p0, Laiw;->ZV:Ljava/lang/String;

    iput v3, p0, Laiw;->aaq:I

    iput v3, p0, Laiw;->aao:I

    iput v3, p0, Laiw;->aam:I

    iput v3, p0, Laiw;->aak:I

    iput v3, p0, Laiw;->aai:I

    iput v3, p0, Laiw;->aag:I

    iput v3, p0, Laiw;->aad:I

    iput v3, p0, Laiw;->aab:I

    iput v3, p0, Laiw;->ZZ:I

    iput v3, p0, Laiw;->ZW:I

    iput v3, p0, Laiw;->count:I

    iput v3, p0, Laiw;->ZU:I

    .line 653
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 657
    array-length v5, v4

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v0, v4, v2

    .line 659
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 660
    const/16 v6, 0x3d

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 663
    if-gtz v6, :cond_0

    .line 665
    new-instance v1, Laix;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing LHS in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Laix;-><init>(Ljava/lang/String;)V

    throw v1

    .line 668
    :cond_0
    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 669
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 670
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 671
    new-instance v1, Laix;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing RHS in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Laix;-><init>(Ljava/lang/String;)V

    throw v1

    .line 678
    :cond_1
    sget-object v0, Laiw;->aar:Ljava/util/HashMap;

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajm;

    .line 679
    if-nez v0, :cond_2

    .line 680
    const-string v0, "X-"

    invoke-virtual {v7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 682
    new-instance v0, Laix;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t find parser for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laix;-><init>(Ljava/lang/String;)V

    throw v0

    .line 686
    :cond_2
    invoke-virtual {v0, v6, p0}, Lajm;->a(Ljava/lang/String;Laiw;)I

    move-result v0

    .line 687
    and-int v6, v1, v0

    if-eqz v6, :cond_3

    .line 688
    new-instance v0, Laix;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Part "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was specified twice"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laix;-><init>(Ljava/lang/String;)V

    throw v0

    .line 690
    :cond_3
    or-int/2addr v0, v1

    .line 657
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto/16 :goto_0

    .line 695
    :cond_4
    and-int/lit16 v0, v1, 0x2000

    if-nez v0, :cond_5

    .line 696
    const/high16 v0, 0x20000

    iput v0, p0, Laiw;->ZX:I

    .line 700
    :cond_5
    and-int/lit8 v0, v1, 0x1

    if-nez v0, :cond_6

    .line 701
    new-instance v0, Laix;

    const-string v1, "Must specify a FREQ value"

    invoke-direct {v0, v1}, Laix;-><init>(Ljava/lang/String;)V

    throw v0

    .line 705
    :cond_6
    and-int/lit8 v0, v1, 0x6

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    .line 709
    sget-object v0, Laiw;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Warning: rrule has both UNTIL and COUNT: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    :cond_7
    return-void

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 325
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    const-string v0, "FREQ="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    iget v0, p0, Laiw;->ZU:I

    packed-switch v0, :pswitch_data_0

    .line 353
    :goto_0
    iget-object v0, p0, Laiw;->ZV:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    const-string v0, ";UNTIL="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    iget-object v0, p0, Laiw;->ZV:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    :cond_0
    iget v0, p0, Laiw;->count:I

    if-eqz v0, :cond_1

    .line 359
    const-string v0, ";COUNT="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    iget v0, p0, Laiw;->count:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 363
    :cond_1
    iget v0, p0, Laiw;->ZW:I

    if-eqz v0, :cond_2

    .line 364
    const-string v0, ";INTERVAL="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    iget v0, p0, Laiw;->ZW:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 368
    :cond_2
    iget v0, p0, Laiw;->ZX:I

    if-eqz v0, :cond_3

    .line 369
    const-string v0, ";WKST="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    iget v0, p0, Laiw;->ZX:I

    invoke-static {v0}, Laiw;->bV(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    :cond_3
    const-string v0, ";BYSECOND="

    iget v2, p0, Laiw;->ZZ:I

    iget-object v3, p0, Laiw;->ZY:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 374
    const-string v0, ";BYMINUTE="

    iget v2, p0, Laiw;->aab:I

    iget-object v3, p0, Laiw;->aaa:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 375
    const-string v0, ";BYSECOND="

    iget v2, p0, Laiw;->aad:I

    iget-object v3, p0, Laiw;->aac:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 378
    iget v0, p0, Laiw;->aag:I

    .line 379
    if-lez v0, :cond_5

    .line 380
    const-string v2, ";BYDAY="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    add-int/lit8 v2, v0, -0x1

    .line 382
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    .line 383
    invoke-direct {p0, v1, v0}, Laiw;->a(Ljava/lang/StringBuilder;I)V

    .line 384
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 331
    :pswitch_0
    const-string v0, "SECONDLY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 334
    :pswitch_1
    const-string v0, "MINUTELY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 337
    :pswitch_2
    const-string v0, "HOURLY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 340
    :pswitch_3
    const-string v0, "DAILY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 343
    :pswitch_4
    const-string v0, "WEEKLY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 346
    :pswitch_5
    const-string v0, "MONTHLY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 349
    :pswitch_6
    const-string v0, "YEARLY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 386
    :cond_4
    invoke-direct {p0, v1, v2}, Laiw;->a(Ljava/lang/StringBuilder;I)V

    .line 389
    :cond_5
    const-string v0, ";BYMONTHDAY="

    iget v2, p0, Laiw;->aai:I

    iget-object v3, p0, Laiw;->aah:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 390
    const-string v0, ";BYYEARDAY="

    iget v2, p0, Laiw;->aak:I

    iget-object v3, p0, Laiw;->aaj:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 391
    const-string v0, ";BYWEEKNO="

    iget v2, p0, Laiw;->aam:I

    iget-object v3, p0, Laiw;->aal:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 392
    const-string v0, ";BYMONTH="

    iget v2, p0, Laiw;->aao:I

    iget-object v3, p0, Laiw;->aan:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 393
    const-string v0, ";BYSETPOS="

    iget v2, p0, Laiw;->aaq:I

    iget-object v3, p0, Laiw;->aap:[I

    invoke-static {v1, v0, v2, v3}, Laiw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 395
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
