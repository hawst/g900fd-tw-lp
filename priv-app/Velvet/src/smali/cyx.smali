.class public final Lcyx;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Lefk;


# instance fields
.field private bnw:Lcyy;

.field private bnx:Ldjs;

.field private bny:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 251
    return-void
.end method

.method public static a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfhs;Ljava/lang/String;)Lcyx;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    const-string v1, "WORKER_FRAGMENT_TAG_KEY"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v1, "ADD_TEAM_QUESTION_KEY"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 65
    new-instance v1, Lcyx;

    invoke-direct {v1}, Lcyx;-><init>()V

    .line 66
    invoke-virtual {v1, v0}, Lcyx;->setArguments(Landroid/os/Bundle;)V

    .line 67
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Lcyx;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 68
    return-object v1
.end method


# virtual methods
.method public final synthetic ar(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 43
    check-cast p1, Lczb;

    invoke-virtual {p0}, Lcyx;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhs;

    iget-object v1, p0, Lcyx;->bny:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0, p1, v1}, Lfhs;->a(Lczb;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V

    invoke-virtual {p0}, Lcyx;->dismiss()V

    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 108
    iget-object v0, p0, Lcyx;->bnw:Lcyy;

    iget-object v0, v0, Lcyy;->bnz:Lcza;

    iget-object v1, p0, Lcyx;->bnx:Ldjs;

    invoke-virtual {v1, v0}, Ldjs;->a(Landroid/widget/BaseAdapter;)V

    .line 109
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 73
    invoke-virtual {p0}, Lcyx;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 75
    invoke-virtual {p0}, Lcyx;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "WORKER_FRAGMENT_TAG_KEY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcyy;

    iput-object v0, p0, Lcyx;->bnw:Lcyy;

    .line 77
    invoke-virtual {p0}, Lcyx;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ADD_TEAM_QUESTION_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    iput-object v0, p0, Lcyx;->bny:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 79
    new-instance v0, Ldjs;

    invoke-virtual {p0}, Lcyx;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f040013

    iget-object v4, p0, Lcyx;->bnw:Lcyy;

    const v5, 0x7f0a03c7

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Ldjs;-><init>(Landroid/content/Context;ILefk;Ldjw;I)V

    iput-object v0, p0, Lcyx;->bnx:Ldjs;

    .line 85
    iget-object v0, p0, Lcyx;->bnx:Ldjs;

    const v1, 0x7f0a033a

    invoke-virtual {v0, v1}, Ldjs;->setTitle(I)V

    .line 86
    iget-object v0, p0, Lcyx;->bnx:Ldjs;

    invoke-virtual {v0}, Ldjs;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 89
    iget-object v0, p0, Lcyx;->bnx:Ldjs;

    return-object v0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 118
    iget-object v0, p0, Lcyx;->bnw:Lcyy;

    iget-object v0, v0, Lcyy;->bnz:Lcza;

    invoke-virtual {v0}, Lcza;->clear()V

    .line 119
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 97
    invoke-virtual {p0}, Lcyx;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 98
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 99
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 100
    const/4 v2, -0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 101
    const/4 v2, -0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 102
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 103
    return-void
.end method
