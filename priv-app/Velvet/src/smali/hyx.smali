.class abstract Lhyx;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field protected final brs:Lcom/google/android/velvet/ActionData;

.field private synthetic duW:Lhyq;

.field protected final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method protected constructor <init>(Lhyq;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 607
    iput-object p1, p0, Lhyx;->duW:Lhyq;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 608
    iput-object p2, p0, Lhyx;->brs:Lcom/google/android/velvet/ActionData;

    .line 609
    iput-object p3, p0, Lhyx;->mQuery:Lcom/google/android/shared/search/Query;

    .line 610
    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 602
    iget-object v0, p0, Lhyx;->brs:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIG()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v0

    const-string v1, "Relationship"

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Relationship"

    invoke-virtual {v0, v2}, Lcom/google/android/speech/embedded/TaggerResult;->nc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v10, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v10, v0, v1}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhyx;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lhyx;->duW:Lhyq;

    iget-object v1, v1, Lhyq;->mContactLabelConverter:Ldyv;

    iget-object v2, p0, Lhyx;->duW:Lhyq;

    iget-object v2, v2, Lhyq;->mRelationshipManager:Lcjg;

    iget-object v3, p0, Lhyx;->duW:Lhyq;

    iget-object v3, v3, Lhyq;->mPersonShortcutManager:Lciy;

    iget-object v4, p0, Lhyx;->duW:Lhyq;

    iget-object v4, v4, Lhyq;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lhyx;->brs:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v5}, Lcom/google/android/velvet/ActionData;->aIG()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v5

    iget-object v6, p0, Lhyx;->duW:Lhyq;

    iget-object v6, v6, Lhyq;->mContactLookup:Lghy;

    sget-object v7, Ldzb;->bRt:Ldzb;

    iget-object v8, p0, Lhyx;->duW:Lhyq;

    iget-object v8, v8, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-virtual {p0, v10}, Lhyx;->g(Lcom/google/android/search/shared/contact/Relationship;)Lifw;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Licv;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Lghy;Ldzb;Ligi;Lifw;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    new-instance v1, Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-direct {v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;-><init>()V

    invoke-virtual {v1, v10}, Lcom/google/android/search/shared/contact/RelationshipStatus;->f(Lcom/google/android/search/shared/contact/Relationship;)V

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amu()V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Lcom/google/android/search/shared/contact/RelationshipStatus;)V

    return-object v0
.end method

.method protected g(Lcom/google/android/search/shared/contact/Relationship;)Lifw;
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x0

    return-object v0
.end method
