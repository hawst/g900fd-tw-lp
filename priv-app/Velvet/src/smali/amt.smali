.class public final Lamt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile afy:[Lamt;


# instance fields
.field private aez:I

.field public afA:Ljbj;

.field public afB:[Ljie;

.field private afz:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lamt;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Lamt;->afz:I

    iput-object v1, p0, Lamt;->afA:Ljbj;

    invoke-static {}, Ljie;->bmS()[Ljie;

    move-result-object v0

    iput-object v0, p0, Lamt;->afB:[Ljie;

    iput-object v1, p0, Lamt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamt;->eCz:I

    .line 165
    return-void
.end method

.method public static nJ()[Lamt;
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lamt;->afy:[Lamt;

    if-nez v0, :cond_1

    .line 126
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    sget-object v0, Lamt;->afy:[Lamt;

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    new-array v0, v0, [Lamt;

    sput-object v0, Lamt;->afy:[Lamt;

    .line 131
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_1
    sget-object v0, Lamt;->afy:[Lamt;

    return-object v0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Lamt;->afz:I

    iget v0, p0, Lamt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamt;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lamt;->afA:Ljbj;

    if-nez v0, :cond_1

    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    iput-object v0, p0, Lamt;->afA:Ljbj;

    :cond_1
    iget-object v0, p0, Lamt;->afA:Ljbj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lamt;->afB:[Ljie;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljie;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lamt;->afB:[Ljie;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljie;

    invoke-direct {v3}, Ljie;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lamt;->afB:[Ljie;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljie;

    invoke-direct {v3}, Ljie;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lamt;->afB:[Ljie;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 180
    iget v0, p0, Lamt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, 0x1

    iget v1, p0, Lamt;->afz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 183
    :cond_0
    iget-object v0, p0, Lamt;->afA:Ljbj;

    if-eqz v0, :cond_1

    .line 184
    const/4 v0, 0x2

    iget-object v1, p0, Lamt;->afA:Ljbj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 186
    :cond_1
    iget-object v0, p0, Lamt;->afB:[Ljie;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lamt;->afB:[Ljie;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 187
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lamt;->afB:[Ljie;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 188
    iget-object v1, p0, Lamt;->afB:[Ljie;

    aget-object v1, v1, v0

    .line 189
    if-eqz v1, :cond_2

    .line 190
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 187
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 195
    return-void
.end method

.method public final cf(I)Lamt;
    .locals 1

    .prologue
    .line 144
    iput p1, p0, Lamt;->afz:I

    .line 145
    iget v0, p0, Lamt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamt;->aez:I

    .line 146
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 199
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 200
    iget v1, p0, Lamt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 201
    const/4 v1, 0x1

    iget v2, p0, Lamt;->afz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_0
    iget-object v1, p0, Lamt;->afA:Ljbj;

    if-eqz v1, :cond_1

    .line 205
    const/4 v1, 0x2

    iget-object v2, p0, Lamt;->afA:Ljbj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_1
    iget-object v1, p0, Lamt;->afB:[Ljie;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lamt;->afB:[Ljie;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 209
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lamt;->afB:[Ljie;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 210
    iget-object v2, p0, Lamt;->afB:[Ljie;

    aget-object v2, v2, v0

    .line 211
    if-eqz v2, :cond_2

    .line 212
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 209
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 217
    :cond_4
    return v0
.end method

.method public final nK()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lamt;->afz:I

    return v0
.end method

.method public final nL()Z
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lamt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
