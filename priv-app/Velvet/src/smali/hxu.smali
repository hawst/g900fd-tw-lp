.class public final Lhxu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final EMPTY_LIST:Ljava/util/List;


# instance fields
.field private final bbd:Lenw;

.field private bbe:Ljava/lang/String;

.field private final dup:Lhxy;

.field private final duq:Z

.field private dur:Lhyb;

.field private final mMainThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mRecognizer:Lgdb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    sput-object v0, Lhxu;->EMPTY_LIST:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lgdb;Lhxy;Ljava/util/concurrent/Executor;Z)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lhxv;

    invoke-direct {v0, p0}, Lhxv;-><init>(Lhxu;)V

    iput-object v0, p0, Lhxu;->dur:Lhyb;

    .line 50
    iput-object p1, p0, Lhxu;->mRecognizer:Lgdb;

    .line 51
    iput-object p2, p0, Lhxu;->dup:Lhxy;

    .line 52
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lhxu;->bbd:Lenw;

    .line 53
    iput-object p3, p0, Lhxu;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    .line 54
    iput-boolean p4, p0, Lhxu;->duq:Z

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Lhxt;Landroid/speech/RecognitionService$Callback;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 58
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lhxu;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 62
    iget-boolean v0, p0, Lhxu;->duq:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhxt;->cQc:Ljava/util/List;

    .line 65
    :goto_0
    const-string v2, "GoogleRecognitionServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#startListening ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lhxt;->cQb:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v2, Lgmy;

    invoke-direct {v2}, Lgmy;-><init>()V

    iget-object v3, p1, Lhxt;->dtf:Landroid/net/Uri;

    iput-object v3, v2, Lgmy;->cPM:Landroid/net/Uri;

    invoke-virtual {v2}, Lgmy;->aHB()Lgmx;

    move-result-object v2

    .line 70
    new-instance v3, Lgnk;

    invoke-direct {v3}, Lgnk;-><init>()V

    invoke-virtual {v3, v1}, Lgnk;->kt(I)Lgnk;

    move-result-object v3

    iget-boolean v4, p1, Lhxt;->dun:Z

    if-nez v4, :cond_1

    :goto_1
    iput-boolean v1, v3, Lgnk;->cFP:Z

    iget-object v1, p1, Lhxt;->cQb:Ljava/lang/String;

    iput-object v1, v3, Lgnk;->cQb:Ljava/lang/String;

    iput-object v0, v3, Lgnk;->cQc:Ljava/util/List;

    iget-boolean v0, p1, Lhxt;->cPu:Z

    iput-boolean v0, v3, Lgnk;->cPu:Z

    sget-object v0, Lgjo;->cMK:Lgjo;

    iput-object v0, v3, Lgnk;->cQe:Lgjo;

    new-instance v0, Ljtp;

    invoke-direct {v0}, Ljtp;-><init>()V

    iput-object v0, v3, Lgnk;->cPw:Ljtp;

    iget-object v0, p1, Lhxt;->cQd:Ljava/lang/String;

    iput-object v0, v3, Lgnk;->cQd:Ljava/lang/String;

    iput-object v2, v3, Lgnk;->cPY:Lgmx;

    invoke-virtual {v3}, Lgnk;->aHW()Lgnj;

    move-result-object v6

    .line 82
    new-instance v0, Lhya;

    iget-object v2, p0, Lhxu;->dur:Lhyb;

    iget-boolean v3, p1, Lhxt;->dun:Z

    iget-boolean v4, p1, Lhxt;->dum:Z

    iget-boolean v5, p0, Lhxu;->duq:Z

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lhya;-><init>(Landroid/speech/RecognitionService$Callback;Lhyb;ZZZ)V

    .line 84
    invoke-virtual {v6}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhxu;->bbe:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lhxu;->mRecognizer:Lgdb;

    iget-object v2, p0, Lhxu;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    invoke-interface {v1, v6, v0, v2, v3}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 88
    iget-object v1, p0, Lhxu;->dup:Lhxy;

    invoke-virtual {v1}, Lhxy;->stop()V

    .line 89
    iget-object v1, p0, Lhxu;->dup:Lhxy;

    invoke-virtual {v1, v0}, Lhxy;->a(Lhya;)V

    .line 90
    return-void

    .line 62
    :cond_0
    sget-object v0, Lhxu;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 70
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method final aTu()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lhxu;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 99
    iget-object v0, p0, Lhxu;->mRecognizer:Lgdb;

    iget-object v1, p0, Lhxu;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lhxu;->bbe:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lhxu;->dup:Lhxy;

    invoke-virtual {v0}, Lhxy;->stop()V

    .line 102
    return-void
.end method

.method public final cancel()V
    .locals 0

    .prologue
    .line 94
    invoke-virtual {p0}, Lhxu;->aTu()V

    .line 95
    return-void
.end method

.method public final destroy()V
    .locals 0

    .prologue
    .line 113
    invoke-virtual {p0}, Lhxu;->aTu()V

    .line 114
    return-void
.end method

.method public final stopListening()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lhxu;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 107
    iget-object v0, p0, Lhxu;->mRecognizer:Lgdb;

    iget-object v1, p0, Lhxu;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->mr(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lhxu;->dup:Lhxy;

    invoke-virtual {v0}, Lhxy;->stop()V

    .line 109
    return-void
.end method
