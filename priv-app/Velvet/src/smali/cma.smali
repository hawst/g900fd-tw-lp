.class public Lcma;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldkj;
.implements Ljava/io/Closeable;


# instance fields
.field private aTF:Z

.field private bbA:Z

.field bbB:Lefq;

.field private final bbv:Ldki;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bbw:Ldki;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bbx:Ldlh;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private bby:Lcmc;

.field private bbz:Z

.field final dK:Ljava/lang/Object;

.field mHeaders:Ldyj;


# direct methods
.method public constructor <init>(Lbzh;Ljava/util/concurrent/ExecutorService;Lchk;Lcly;Legl;Lcpq;Lcoi;Lcpt;)V
    .locals 8
    .param p1    # Lbzh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lcly;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Lcoi;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcpt;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcma;->dK:Ljava/lang/Object;

    .line 77
    new-instance v0, Ldki;

    invoke-direct {v0, p0}, Ldki;-><init>(Ldkj;)V

    .line 78
    new-instance v1, Ldki;

    invoke-direct {v1, p0}, Ldki;-><init>(Ldkj;)V

    .line 79
    iput-object v0, p0, Lcma;->bbv:Ldki;

    .line 80
    iput-object v1, p0, Lcma;->bbw:Ldki;

    .line 81
    const/4 v0, 0x2

    new-array v0, v0, [Ldki;

    const/4 v1, 0x0

    iget-object v2, p0, Lcma;->bbv:Ldki;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcma;->bbw:Ldki;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {p1}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcng;

    move-object v1, p3

    move-object v3, p7

    move-object/from16 v4, p8

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcng;-><init>(Lchk;Ljava/lang/String;Lcoi;Lcpt;Lcpq;)V

    new-instance v1, Ldli;

    move-object v2, p4

    move-object v3, v6

    move-object v4, v0

    move-object v5, p2

    move-object v6, p5

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Ldli;-><init>(Lisu;Ljava/util/List;Lcng;Ljava/util/concurrent/ExecutorService;Legl;Lbzh;)V

    invoke-virtual {v1}, Ldlh;->start()V

    iput-object v1, p0, Lcma;->bbx:Ldlh;

    .line 85
    return-void
.end method

.method public constructor <init>(Ldlh;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcma;->dK:Ljava/lang/Object;

    .line 62
    new-instance v0, Ldki;

    invoke-direct {v0, p0}, Ldki;-><init>(Ldkj;)V

    .line 63
    new-instance v1, Ldki;

    invoke-direct {v1, p0}, Ldki;-><init>(Ldkj;)V

    .line 64
    iput-object v0, p0, Lcma;->bbv:Ldki;

    .line 65
    iput-object v1, p0, Lcma;->bbw:Ldki;

    .line 66
    iput-object p1, p0, Lcma;->bbx:Ldlh;

    .line 67
    return-void
.end method

.method static synthetic a(Lcma;Z)Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcma;->bbz:Z

    return v0
.end method


# virtual methods
.method final PM()V
    .locals 3

    .prologue
    .line 101
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 103
    :try_start_0
    iget-boolean v2, p0, Lcma;->bbz:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcma;->bby:Lcmc;

    if-eqz v2, :cond_0

    .line 104
    iget-object v0, p0, Lcma;->bby:Lcmc;

    .line 105
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcma;->bbz:Z

    .line 107
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    if-eqz v0, :cond_1

    .line 109
    invoke-interface {v0}, Lcmc;->PW()V

    .line 111
    :cond_1
    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final PO()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 115
    iget-object v3, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v3

    .line 116
    :try_start_0
    iget-boolean v2, p0, Lcma;->bbA:Z

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 117
    iget-boolean v2, p0, Lcma;->aTF:Z

    if-nez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcma;->bbz:Z

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcma;->aTF:Z

    .line 119
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    invoke-virtual {p0}, Lcma;->PM()V

    .line 121
    return-void

    :cond_0
    move v2, v1

    .line 116
    goto :goto_0

    :cond_1
    move v0, v1

    .line 117
    goto :goto_1

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final PP()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 125
    iget-object v3, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v3

    .line 126
    :try_start_0
    iget-boolean v2, p0, Lcma;->aTF:Z

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 127
    iget-boolean v2, p0, Lcma;->bbA:Z

    if-nez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcma;->bbz:Z

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcma;->bbA:Z

    .line 129
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-virtual {p0}, Lcma;->PM()V

    .line 131
    return-void

    :cond_0
    move v2, v1

    .line 126
    goto :goto_0

    :cond_1
    move v0, v1

    .line 127
    goto :goto_1

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final PQ()Ldyj;
    .locals 2

    .prologue
    .line 145
    iget-object v1, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_0
    iget-object v0, p0, Lcma;->mHeaders:Ldyj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 147
    iget-object v0, p0, Lcma;->mHeaders:Ldyj;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final PR()Z
    .locals 2

    .prologue
    .line 152
    iget-object v1, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    :try_start_0
    iget-object v0, p0, Lcma;->bbB:Lefq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final PS()Lefq;
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    :try_start_0
    iget-object v0, p0, Lcma;->bbB:Lefq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 172
    iget-object v0, p0, Lcma;->bbB:Lefq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final PT()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcma;->bbv:Ldki;

    return-object v0
.end method

.method public final PU()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcma;->bbw:Ldki;

    return-object v0
.end method

.method public final PV()Lclz;
    .locals 1

    .prologue
    .line 218
    new-instance v0, Lcmb;

    invoke-direct {v0, p0}, Lcmb;-><init>(Lcma;)V

    return-object v0
.end method

.method public final a(Lcmc;)V
    .locals 2
    .param p1    # Lcmc;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 93
    iget-object v1, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_0
    iget-object v0, p0, Lcma;->bby:Lcmc;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 95
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmc;

    iput-object v0, p0, Lcma;->bby:Lcmc;

    .line 96
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    invoke-virtual {p0}, Lcma;->PM()V

    .line 98
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcma;->bbx:Ldlh;

    invoke-virtual {v0}, Ldlh;->stop()V

    .line 206
    return-void
.end method

.method public final hasHeaders()Z
    .locals 2

    .prologue
    .line 134
    iget-object v1, p0, Lcma;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    iget-object v0, p0, Lcma;->mHeaders:Ldyj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isComplete()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcma;->aTF:Z

    return v0
.end method

.method public final isFailed()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcma;->bbA:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AsyncHttpResponse{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcma;->bbv:Ldki;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcma;->bbw:Ldki;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
