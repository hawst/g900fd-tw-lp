.class public final Lflo;
.super Lfnm;
.source "PG"

# interfaces
.implements Ldyh;


# instance fields
.field private final cuZ:Ligi;

.field private mSpeechLevelSource:Lequ;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ligi;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lfnm;-><init>()V

    .line 22
    iput-object p1, p0, Lflo;->cuZ:Ligi;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(ILequ;)V
    .locals 1

    .prologue
    .line 52
    iput-object p2, p0, Lflo;->mSpeechLevelSource:Lequ;

    .line 53
    packed-switch p1, :pswitch_data_0

    .line 64
    :goto_0
    return-void

    .line 55
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflo;->fE(I)V

    goto :goto_0

    .line 58
    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lflo;->fE(I)V

    goto :goto_0

    .line 61
    :pswitch_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lflo;->fE(I)V

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final aAr()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lflo;->cuZ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    .line 28
    if-nez v0, :cond_0

    .line 29
    const-string v0, "GelTvRecognitionManager"

    const-string v1, "Cannot start TV content recognition: search is not available."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :goto_0
    return-void

    .line 31
    :cond_0
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apf()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ldyg;->a(Lcom/google/android/shared/search/Query;Ldyh;)V

    goto :goto_0
.end method

.method public final aAs()Lequ;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lflo;->mSpeechLevelSource:Lequ;

    return-object v0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Lfnm;->ag:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflo;->fE(I)V

    .line 48
    :cond_0
    return-void
.end method
