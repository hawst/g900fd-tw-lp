.class public final Livg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1236
    invoke-static {}, Liqa;->aYi()Liqa;

    move-result-object v0

    new-instance v1, Livk;

    invoke-direct {v1}, Livk;-><init>()V

    invoke-virtual {v0, v1}, Liqa;->b(Lifg;)Liqa;

    move-result-object v0

    invoke-virtual {v0}, Liqa;->aYf()Liqa;

    return-void
.end method

.method public static A(Ljava/lang/Iterable;)Livq;
    .locals 4

    .prologue
    .line 867
    new-instance v0, Livn;

    invoke-static {p0}, Lijj;->q(Ljava/lang/Iterable;)Lijj;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Livn;-><init>(Lijj;ZLjava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static a(Livq;Lifg;)Livq;
    .locals 4

    .prologue
    .line 446
    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v0

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Livi;

    invoke-direct {v1, p1}, Livi;-><init>(Lifg;)V

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Livl;

    new-instance v3, Livh;

    invoke-direct {v3, v1}, Livh;-><init>(Lifg;)V

    const/4 v1, 0x0

    invoke-direct {v2, v3, p0, v1}, Livl;-><init>(Liuz;Livq;B)V

    invoke-interface {p0, v2, v0}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-object v2
.end method

.method public static a(Livq;Liuz;)Livq;
    .locals 3

    .prologue
    .line 336
    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v0

    new-instance v1, Livl;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p0, v2}, Livl;-><init>(Liuz;Livq;B)V

    invoke-interface {p0, v1, v0}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-object v1
.end method

.method public static a(Livq;Livf;)V
    .locals 1

    .prologue
    .line 915
    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v0

    invoke-static {p0, p1, v0}, Livg;->a(Livq;Livf;Ljava/util/concurrent/Executor;)V

    .line 916
    return-void
.end method

.method public static a(Livq;Livf;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 967
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    new-instance v0, Livj;

    invoke-direct {v0, p0, p1}, Livj;-><init>(Livq;Livf;)V

    .line 985
    invoke-interface {p0, v0, p2}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 986
    return-void
.end method

.method public static bC(Ljava/lang/Object;)Livq;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91
    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p0}, Livy;->bB(Ljava/lang/Object;)Z

    .line 93
    return-object v0
.end method

.method public static c(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1173
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1175
    :try_start_0
    invoke-static {p0}, Liwc;->d(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1176
    :catch_0
    move-exception v0

    .line 1177
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_0

    new-instance v1, Livc;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Livc;-><init>(Ljava/lang/Error;)V

    throw v1

    :cond_0
    new-instance v1, Liwb;

    invoke-direct {v1, v0}, Liwb;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static i(Ljava/lang/Throwable;)Livq;
    .locals 1

    .prologue
    .line 129
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v0

    .line 131
    invoke-virtual {v0, p0}, Livy;->h(Ljava/lang/Throwable;)Z

    .line 132
    return-object v0
.end method
