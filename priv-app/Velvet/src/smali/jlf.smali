.class public final Ljlf;
.super Ljsl;
.source "PG"


# static fields
.field public static final erl:Ljsm;


# instance fields
.field private aez:I

.field private erm:Ljava/lang/String;

.field private ern:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11962
    const/16 v0, 0xb

    const-class v1, Ljlf;

    const v2, 0x16db0f8a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljlf;->erl:Ljsm;

    .line 11968
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12016
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12017
    iput v1, p0, Ljlf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlf;->erm:Ljava/lang/String;

    iput v1, p0, Ljlf;->ern:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljlf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlf;->eCz:I

    .line 12018
    return-void
.end method


# virtual methods
.method public final Pq()I
    .locals 1

    .prologue
    .line 12000
    iget v0, p0, Ljlf;->ern:I

    return v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11955
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlf;->erm:Ljava/lang/String;

    iget v0, p0, Ljlf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlf;->ern:I

    iget v0, p0, Ljlf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12032
    iget v0, p0, Ljlf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 12033
    const/4 v0, 0x1

    iget-object v1, p0, Ljlf;->erm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12035
    :cond_0
    iget v0, p0, Ljlf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 12036
    const/4 v0, 0x2

    iget v1, p0, Ljlf;->ern:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 12038
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12039
    return-void
.end method

.method public final boQ()Z
    .locals 1

    .prologue
    .line 12008
    iget v0, p0, Ljlf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12043
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12044
    iget v1, p0, Ljlf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 12045
    const/4 v1, 0x1

    iget-object v2, p0, Ljlf;->erm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12048
    :cond_0
    iget v1, p0, Ljlf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 12049
    const/4 v1, 0x2

    iget v2, p0, Ljlf;->ern:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12052
    :cond_1
    return v0
.end method

.method public final qB(I)Ljlf;
    .locals 1

    .prologue
    .line 12003
    iput p1, p0, Ljlf;->ern:I

    .line 12004
    iget v0, p0, Ljlf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlf;->aez:I

    .line 12005
    return-object p0
.end method
