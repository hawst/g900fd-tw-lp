.class public final Ljzk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eNJ:I

.field private eNK:I

.field private eNL:I

.field private eNM:I

.field private eNN:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1279
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1280
    iput v0, p0, Ljzk;->aez:I

    iput v0, p0, Ljzk;->eNJ:I

    iput v0, p0, Ljzk;->eNK:I

    iput v0, p0, Ljzk;->eNL:I

    iput v0, p0, Ljzk;->eNM:I

    iput v0, p0, Ljzk;->eNN:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljzk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzk;->eCz:I

    .line 1281
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzk;->eNJ:I

    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzk;->eNK:I

    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzk;->eNL:I

    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzk;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzk;->eNM:I

    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzk;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzk;->eNN:I

    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1298
    iget v0, p0, Ljzk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1299
    const/4 v0, 0x1

    iget v1, p0, Ljzk;->eNJ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1301
    :cond_0
    iget v0, p0, Ljzk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1302
    const/4 v0, 0x2

    iget v1, p0, Ljzk;->eNK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1304
    :cond_1
    iget v0, p0, Ljzk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1305
    const/4 v0, 0x3

    iget v1, p0, Ljzk;->eNL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1307
    :cond_2
    iget v0, p0, Ljzk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1308
    const/4 v0, 0x4

    iget v1, p0, Ljzk;->eNM:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1310
    :cond_3
    iget v0, p0, Ljzk;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1311
    const/4 v0, 0x5

    iget v1, p0, Ljzk;->eNN:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1313
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1314
    return-void
.end method

.method public final bwZ()I
    .locals 1

    .prologue
    .line 1244
    iget v0, p0, Ljzk;->eNM:I

    return v0
.end method

.method public final bxa()I
    .locals 1

    .prologue
    .line 1263
    iget v0, p0, Ljzk;->eNN:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1318
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1319
    iget v1, p0, Ljzk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1320
    const/4 v1, 0x1

    iget v2, p0, Ljzk;->eNJ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1323
    :cond_0
    iget v1, p0, Ljzk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1324
    const/4 v1, 0x2

    iget v2, p0, Ljzk;->eNK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1327
    :cond_1
    iget v1, p0, Ljzk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1328
    const/4 v1, 0x3

    iget v2, p0, Ljzk;->eNL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1331
    :cond_2
    iget v1, p0, Ljzk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1332
    const/4 v1, 0x4

    iget v2, p0, Ljzk;->eNM:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1335
    :cond_3
    iget v1, p0, Ljzk;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1336
    const/4 v1, 0x5

    iget v2, p0, Ljzk;->eNN:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1339
    :cond_4
    return v0
.end method

.method public final tu(I)Ljzk;
    .locals 1

    .prologue
    .line 1247
    iput p1, p0, Ljzk;->eNM:I

    .line 1248
    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzk;->aez:I

    .line 1249
    return-object p0
.end method

.method public final tv(I)Ljzk;
    .locals 1

    .prologue
    .line 1266
    iput p1, p0, Ljzk;->eNN:I

    .line 1267
    iget v0, p0, Ljzk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzk;->aez:I

    .line 1268
    return-object p0
.end method
