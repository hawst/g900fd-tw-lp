.class final Lggp;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic cJF:Lggo;


# direct methods
.method private constructor <init>(Lggo;)V
    .locals 2

    .prologue
    .line 87
    iput-object p1, p0, Lggp;->cJF:Lggo;

    .line 88
    const-string v0, "ContactLabelSupplier"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 89
    return-void
.end method

.method synthetic constructor <init>(Lggo;B)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lggp;-><init>(Lggo;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/16 v5, 0x14

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 94
    iget-object v0, p0, Lggp;->cJF:Lggo;

    iget-object v0, v0, Lggo;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->CS()Z

    move-result v0

    .line 95
    const-string v1, "ContactLabelSupplier"

    const-string v2, "get() : OptedIn = %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 97
    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lggp;->cJF:Lggo;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lggo;->a(Ljava/util/List;J)V

    .line 111
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v0, "ContactLabelSupplier"

    const-string v1, "get() : Looking up %d labels"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 103
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 104
    iget-object v2, p0, Lggp;->cJF:Lggo;

    iget-object v2, v2, Lggo;->cJA:Lggk;

    invoke-virtual {v2, v5}, Lggk;->kl(I)Ljava/util/List;

    move-result-object v2

    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 106
    const-string v3, "ContactLabelSupplier"

    const-string v6, "get() : Lookup time = %d ms"

    new-array v7, v7, [Ljava/lang/Object;

    sub-long v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v3, v6, v7}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lggp;->cJF:Lggo;

    invoke-virtual {v0, v2, v4, v5}, Lggo;->a(Ljava/util/List;J)V

    goto :goto_0
.end method
