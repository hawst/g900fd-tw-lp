.class public final Lcxn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final bmr:Landroid/content/Intent;

.field private final bms:Ligi;

.field private final mActivity:Landroid/app/Activity;

.field private final mLoginHelper:Lcrh;

.field private final mNowOptInSettings:Lcin;

.field private final mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>(Lcin;Lcrh;Landroid/content/Intent;Landroid/app/Activity;Lcpx;Ligi;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcxn;->mNowOptInSettings:Lcin;

    .line 53
    iput-object p2, p0, Lcxn;->mLoginHelper:Lcrh;

    .line 54
    iput-object p3, p0, Lcxn;->bmr:Landroid/content/Intent;

    .line 55
    iput-object p4, p0, Lcxn;->mActivity:Landroid/app/Activity;

    .line 56
    iput-object p5, p0, Lcxn;->mUserInteractionLogger:Lcpx;

    .line 57
    iput-object p6, p0, Lcxn;->bms:Ligi;

    .line 58
    return-void
.end method

.method static a(Landroid/app/Fragment;)Lcxn;
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 129
    instance-of v1, v0, Lcwl;

    if-eqz v1, :cond_0

    .line 130
    check-cast v0, Lcwl;

    invoke-interface {v0}, Lcwl;->Tq()Lcxn;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    .line 132
    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 133
    instance-of v1, v0, Lcwl;

    if-eqz v1, :cond_1

    .line 134
    check-cast v0, Lcwl;

    invoke-interface {v0}, Lcwl;->Tq()Lcxn;

    move-result-object v0

    goto :goto_0

    .line 136
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final TD()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcxn;->bms:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcxn;->a(Landroid/widget/Switch;)V

    .line 85
    return-void
.end method

.method public final a(Landroid/widget/Switch;)V
    .locals 4
    .param p1    # Landroid/widget/Switch;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 95
    if-nez p1, :cond_0

    .line 120
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 103
    iget-object v0, p0, Lcxn;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    .line 104
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcxn;->mNowOptInSettings:Lcin;

    invoke-interface {v0, v3}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 105
    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 108
    if-nez v3, :cond_3

    .line 111
    iget-object v0, p0, Lcxn;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_2

    .line 117
    :goto_2
    invoke-virtual {p1, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 119
    invoke-virtual {p1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 104
    goto :goto_1

    :cond_2
    move v1, v2

    .line 111
    goto :goto_2

    .line 113
    :cond_3
    iget-object v0, p0, Lcxn;->mNowOptInSettings:Lcin;

    invoke-interface {v0, v3}, Lcin;->i(Landroid/accounts/Account;)I

    move-result v0

    .line 114
    if-eqz v0, :cond_4

    if-ne v0, v1, :cond_5

    :cond_4
    move v2, v1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method final cO(Z)V
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcxn;->mUserInteractionLogger:Lcpx;

    const-string v1, "BUTTON_PRESS"

    const-string v2, "SETTINGS_OPT_OUT"

    invoke-virtual {v0, v1, v2}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcxn;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    .line 144
    if-nez v1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v0, p0, Lcxn;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 151
    if-eqz v2, :cond_0

    .line 156
    const-string v0, "opt_out_progress"

    invoke-virtual {v2, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcxm;

    .line 158
    if-nez v0, :cond_2

    .line 159
    new-instance v0, Lcxm;

    invoke-direct {v0}, Lcxm;-><init>()V

    .line 160
    const-string v3, "opt_out_progress"

    invoke-virtual {v0, v2, v3}, Lcxm;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 163
    :cond_2
    const-string v0, "opt_out_worker_fragment"

    invoke-virtual {v2, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 164
    invoke-static {v1, p1}, Lcxo;->b(Landroid/accounts/Account;Z)Lcxo;

    move-result-object v0

    .line 166
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "opt_out_worker_fragment"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcxn;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcxn;->mNowOptInSettings:Lcin;

    invoke-interface {v1, v0}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v0

    .line 69
    if-nez p2, :cond_1

    if-eqz v0, :cond_1

    .line 70
    new-instance v0, Lcxj;

    invoke-direct {v0}, Lcxj;-><init>()V

    .line 71
    iget-object v1, p0, Lcxn;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "optout_dialog"

    invoke-virtual {v0, v1, v2}, Lcxj;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    if-eqz p2, :cond_0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcxn;->mUserInteractionLogger:Lcpx;

    const-string v1, "BUTTON_PRESS"

    const-string v2, "SETTINGS_OPT_IN"

    invoke-virtual {v0, v1, v2}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcxn;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcxn;->bmr:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
