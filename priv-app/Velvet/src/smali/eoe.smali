.class public final Leoe;
.super Leod;
.source "PG"

# interfaces
.implements Leqo;


# instance fields
.field private final cgA:Ljava/util/Map;

.field private final cgz:Landroid/os/MessageQueue;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Leod;-><init>(Landroid/os/Handler;)V

    .line 31
    iget-object v0, p0, Leoe;->mHandler:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Leoe;->cgz:Landroid/os/MessageQueue;

    .line 32
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Leoe;->cgA:Ljava/util/Map;

    .line 33
    return-void

    .line 31
    :cond_0
    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/MessageQueue;

    new-instance v3, Leof;

    invoke-direct {v3, v2, v1}, Leof;-><init>([Landroid/os/MessageQueue;Landroid/os/ConditionVariable;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    const/4 v0, 0x0

    aget-object v0, v2, v0

    goto :goto_0
.end method

.method private static o(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 121
    instance-of v0, p0, Lesl;

    if-nez v0, :cond_0

    .line 122
    instance-of v0, p0, Lepm;

    if-eqz v0, :cond_1

    .line 123
    const-string v0, "HandlerScheduledExecutor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NamedRunnable posted on HandlerScheduledExecutor. Either change to NamedUiRunnable, or move to the non-UI executor. Runnable="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v4, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    instance-of v0, p0, Lerj;

    if-eqz v0, :cond_2

    .line 126
    const-string v0, "HandlerScheduledExecutor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TaggedTask posted on HandlerScheduledExecutor. Should be run on the non-UI executor. Runnable="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v4, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 130
    :cond_2
    const-string v0, "HandlerScheduledExecutor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Task does not implement UiTask. Consider using NamedUiRunnable for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v4, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final Du()Z
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Leoe;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 71
    invoke-static {p1}, Leoe;->o(Ljava/lang/Runnable;)V

    .line 72
    iget-object v0, p0, Leoe;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 73
    return-void
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 59
    invoke-static {p1}, Leoe;->o(Ljava/lang/Runnable;)V

    .line 60
    invoke-super {p0, p1}, Leod;->execute(Ljava/lang/Runnable;)V

    .line 61
    return-void
.end method

.method public final getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Leoe;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public final i(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Leod;->i(Ljava/lang/Runnable;)V

    .line 66
    iget-object v0, p0, Leoe;->cgz:Landroid/os/MessageQueue;

    invoke-virtual {p0, p1}, Leoe;->n(Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 67
    return-void
.end method

.method public final j(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 77
    invoke-static {p1}, Leoe;->o(Ljava/lang/Runnable;)V

    .line 78
    new-instance v0, Leog;

    invoke-direct {v0, p0, p1}, Leog;-><init>(Leoe;Ljava/lang/Runnable;)V

    .line 79
    iget-object v1, p0, Leoe;->cgA:Ljava/util/Map;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v2, p0, Leoe;->cgA:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v2, p0, Leoe;->cgz:Landroid/os/MessageQueue;

    invoke-virtual {v2, v0}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 82
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final n(Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;
    .locals 2

    .prologue
    .line 96
    iget-object v1, p0, Leoe;->cgA:Ljava/util/Map;

    monitor-enter v1

    .line 97
    :try_start_0
    iget-object v0, p0, Leoe;->cgA:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Leoe;->cgA:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/MessageQueue$IdleHandler;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :goto_0
    return-object v0

    .line 100
    :cond_0
    monitor-exit v1

    .line 101
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
