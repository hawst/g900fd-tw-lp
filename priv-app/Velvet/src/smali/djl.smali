.class final Ldjl;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic bAk:Ldjk;


# direct methods
.method varargs constructor <init>(Ldjk;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Ldjl;->bAk:Ldjk;

    invoke-direct {p0, p2, p3, p4, p5}, Lenp;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 141
    iget-object v2, p0, Ldjl;->bAk:Ldjk;

    iget-object v0, v2, Ldjk;->bAf:Landroid/app/SearchManager;

    invoke-virtual {v0}, Landroid/app/SearchManager;->getSearchablesInGlobalSearch()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Search.SearchableSources"

    const-string v1, "getSearchablesInGlobalSearch() returned null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchableInfo;

    invoke-virtual {v2, v0}, Ldjk;->a(Landroid/app/SearchableInfo;)Ldiq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v4, v2, Ldjk;->mConfig:Lcjs;

    invoke-virtual {v4, v0}, Lcjs;->a(Ldgb;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v0}, Ldiq;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldiq;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lifv;->gY(Z)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 141
    check-cast p1, Ljava/util/Map;

    iget-object v0, p0, Ldjl;->bAk:Ldjk;

    iget-object v1, v0, Ldjk;->bAi:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ldjl;->bAk:Ldjk;

    iget-object v0, v0, Ldjk;->bAi:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Ldjl;->bAk:Ldjk;

    iget-object v0, v0, Ldjk;->bAi:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v0, p0, Ldjl;->bAk:Ldjk;

    iget-object v2, p0, Ldjl;->bAk:Ldjk;

    iget-object v2, v2, Ldjk;->bAi:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v2

    iput-object v2, v0, Ldjk;->bAj:Lijj;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Ldjl;->bAk:Ldjk;

    iget-object v0, v0, Ldjk;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
