.class public final Lhlm;
.super Lhlu;
.source "PG"


# instance fields
.field private final mCalendarHelper:Libp;

.field private final mCalendarTextHelper:Ledt;


# direct methods
.method public constructor <init>(Leoj;Libp;Ledt;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 23
    iput-object p2, p0, Lhlm;->mCalendarHelper:Libp;

    .line 24
    iput-object p3, p0, Lhlm;->mCalendarTextHelper:Ledt;

    .line 25
    return-void
.end method

.method private b(Lcom/google/android/search/shared/actions/AddEventAction;)[Landroid/content/Intent;
    .locals 9

    .prologue
    .line 53
    const/4 v0, 0x1

    new-array v7, v0, [Landroid/content/Intent;

    const/4 v8, 0x0

    invoke-direct {p0, p1}, Lhlm;->c(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agN()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agQ()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Ledv;->a(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v7, v8

    return-object v7
.end method

.method private c(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lhlm;->mCalendarTextHelper:Ledt;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->pO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agL()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ledt;->d(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 15
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-wide/32 v2, 0xf423f

    invoke-static {v2, v3}, Ledv;->aE(J)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 15
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.APP_CALENDAR"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/google/android/search/shared/actions/AddEventAction;

    invoke-direct {p0, p1}, Lhlm;->b(Lcom/google/android/search/shared/actions/AddEventAction;)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/google/android/search/shared/actions/AddEventAction;

    invoke-direct {p0, p1}, Lhlm;->b(Lcom/google/android/search/shared/actions/AddEventAction;)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 15
    check-cast p1, Lcom/google/android/search/shared/actions/AddEventAction;

    if-ne p2, v0, :cond_0

    iget-object v1, p0, Lhlm;->mCalendarHelper:Libp;

    invoke-direct {p0, p1}, Lhlm;->c(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agN()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agQ()J

    move-result-wide v8

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agM()Ljava/util/List;

    move-result-object v10

    move-object v3, v2

    invoke-virtual/range {v1 .. v10}, Libp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhlu;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    goto :goto_0
.end method
