.class final Ldsq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final bIV:Landroid/graphics/Rect;

.field private synthetic bIW:Ldsp;


# direct methods
.method constructor <init>(Ldsp;)V
    .locals 1

    .prologue
    .line 53
    iput-object p1, p0, Ldsq;->bIW:Ldsp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldsq;->bIV:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v0, v0, Ldsp;->bIP:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v0, v0, Ldsp;->bIP:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget v0, v0, Ldsp;->bIQ:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget v0, v0, Ldsp;->bIR:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 62
    :cond_2
    iget-object v0, p0, Ldsq;->bIV:Landroid/graphics/Rect;

    iget-object v1, p0, Ldsq;->bIW:Ldsp;

    iget-object v1, v1, Ldsp;->bIP:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 63
    iget-object v0, p0, Ldsq;->bIV:Landroid/graphics/Rect;

    iget-object v1, p0, Ldsq;->bIW:Ldsp;

    iget-object v1, v1, Ldsp;->bIP:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 64
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v0, v0, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, p0, Ldsq;->bIW:Ldsp;

    iget-object v1, v1, Ldsp;->bIP:Landroid/view/View;

    iget-object v2, p0, Ldsq;->bIV:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 65
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v1, p0, Ldsq;->bIV:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Ldsq;->bIW:Ldsp;

    iget-object v2, v2, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SearchPlate;->getLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Ldsp;->bIQ:F

    .line 66
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v1, p0, Ldsq;->bIV:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Ldsq;->bIW:Ldsp;

    iget-object v2, v2, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SearchPlate;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Ldsp;->bIR:F

    .line 67
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v0, v0, Ldsp;->bIO:Ldss;

    iget-object v1, p0, Ldsq;->bIW:Ldsp;

    iget v1, v1, Ldsp;->bIQ:F

    iget-object v2, p0, Ldsq;->bIW:Ldsp;

    iget v2, v2, Ldsp;->bIR:F

    invoke-virtual {v0, v1, v2}, Ldss;->s(FF)V

    .line 69
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 70
    iget-object v0, p0, Ldsq;->bIW:Ldsp;

    iget-object v2, v0, Ldsp;->bIO:Ldss;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, v0, v1}, Ldss;->r(FF)V

    goto/16 :goto_0
.end method
