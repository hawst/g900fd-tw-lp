.class public final Lcvp;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final mDebugFeatures:Lckw;

.field private final mSearchConfig:Lcjs;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lcke;Lcjs;Lckw;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 30
    iput-object p1, p0, Lcvp;->mSearchSettings:Lcke;

    .line 31
    iput-object p2, p0, Lcvp;->mSearchConfig:Lcjs;

    .line 32
    iput-object p3, p0, Lcvp;->mDebugFeatures:Lckw;

    .line 33
    return-void
.end method

.method private a(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcvp;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NI()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcvp;->mSearchConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Lc()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {v1}, Lcvp;->iV(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcvp;->iV(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    invoke-static {p2}, Lcvp;->iU(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 93
    :goto_0
    return-void

    .line 90
    :cond_1
    const-string v0, "[Enabled for sandbox domain overrides]"

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 91
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private static iU(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 77
    invoke-static {p0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const-string p0, "[No override set]"

    .line 80
    :cond_0
    return-object p0
.end method

.method private static iV(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 96
    if-eqz p0, :cond_0

    const-string v0, ".sandbox.google.com"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debug_search_domain_override"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcvp;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NI()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcvp;->iU(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debug_search_scheme_override"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    iget-object v0, p0, Lcvp;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcvp;->iU(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debug_search_host_param"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    iget-object v0, p0, Lcvp;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NJ()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcvp;->a(Landroid/preference/Preference;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    .line 56
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debug_js_server_address"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcvp;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NG()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcvp;->iU(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 38
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debug_js_server_address"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcvp;->mDebugFeatures:Lckw;

    invoke-virtual {v1}, Lckw;->Pa()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 64
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debug_search_host_param"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcvp;->a(Landroid/preference/Preference;Ljava/lang/String;)V

    .line 73
    :goto_0
    return v0

    .line 67
    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debug_search_scheme_override"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debug_search_domain_override"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debug_js_server_address"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 70
    :cond_1
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Lcvp;->iU(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 73
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
