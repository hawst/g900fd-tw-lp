.class public final Ljsc;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eCa:[Ljsc;


# instance fields
.field private aez:I

.field private dHA:I

.field private dHz:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 66
    iput v0, p0, Ljsc;->aez:I

    iput v0, p0, Ljsc;->dHz:I

    iput v0, p0, Ljsc;->dHA:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljsc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljsc;->eCz:I

    .line 67
    return-void
.end method

.method public static btI()[Ljsc;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljsc;->eCa:[Ljsc;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljsc;->eCa:[Ljsc;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljsc;

    sput-object v0, Ljsc;->eCa:[Ljsc;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljsc;->eCa:[Ljsc;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljsc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsc;->dHz:I

    iget v0, p0, Ljsc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsc;->dHA:I

    iget v0, p0, Ljsc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 81
    iget v0, p0, Ljsc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 82
    const/4 v0, 0x1

    iget v1, p0, Ljsc;->dHz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 84
    :cond_0
    iget v0, p0, Ljsc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 85
    const/4 v0, 0x2

    iget v1, p0, Ljsc;->dHA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 87
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 88
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 92
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 93
    iget v1, p0, Ljsc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 94
    const/4 v1, 0x1

    iget v2, p0, Ljsc;->dHz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_0
    iget v1, p0, Ljsc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 98
    const/4 v1, 0x2

    iget v2, p0, Ljsc;->dHA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_1
    return v0
.end method
