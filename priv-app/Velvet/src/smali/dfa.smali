.class public final Ldfa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldes;


# instance fields
.field private final boT:Ldeb;

.field private final boU:Ldeb;

.field private final bvO:I

.field private final bvP:I


# direct methods
.method public constructor <init>(Ldeb;Ldeb;II)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Ldfa;->boT:Ldeb;

    .line 18
    iput-object p2, p0, Ldfa;->boU:Ldeb;

    .line 19
    iput p3, p0, Ldfa;->bvO:I

    .line 20
    iput p4, p0, Ldfa;->bvP:I

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Ldem;Landroid/util/SparseArray;Landroid/util/SparseArray;)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 29
    invoke-virtual {p1}, Ldem;->abj()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 31
    new-instance v4, Ldej;

    const-string v0, "web"

    invoke-direct {v4, v0, v3}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 32
    iget-object v5, p0, Ldfa;->boT:Ldeb;

    iget v6, p0, Ldfa;->bvO:I

    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v5, p1, v6, v0, v4}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 35
    new-instance v5, Ldej;

    const-string v0, "summons"

    invoke-direct {v5, v0, v3}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 36
    iget-object v3, p0, Ldfa;->boU:Ldeb;

    iget v6, p0, Ldfa;->bvP:I

    invoke-virtual {p2, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v3, p1, v6, v0, v5}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 39
    invoke-virtual {p3, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 40
    invoke-virtual {p3, v7, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 41
    invoke-interface {v4}, Lddw;->abc()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Lddw;->abc()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0

    :cond_1
    move v1, v2

    .line 41
    goto :goto_1
.end method
