.class public final Lcxc;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final ble:Lhxr;

.field private blk:Landroid/preference/Preference;

.field private final mActivity:Landroid/app/Activity;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Lcke;Landroid/app/Activity;Lhym;Lhxr;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 33
    iput-object p2, p0, Lcxc;->mActivity:Landroid/app/Activity;

    .line 34
    iput-object p3, p0, Lcxc;->mVoiceSettings:Lhym;

    .line 35
    iput-object p4, p0, Lcxc;->ble:Lhxr;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Lcxc;->blk:Landroid/preference/Preference;

    .line 41
    iget-object v0, p0, Lcxc;->blk:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 42
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcxc;->ble:Lhxr;

    invoke-virtual {v0}, Lhxr;->aTt()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 51
    const/16 v0, 0x2f

    invoke-static {v0}, Lege;->ht(I)V

    .line 53
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcxc;->mVoiceSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNc:Ljzu;

    invoke-virtual {v2}, Ljzu;->bxt()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 55
    iget-object v1, p0, Lcxc;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 56
    const/4 v0, 0x1

    return v0
.end method
