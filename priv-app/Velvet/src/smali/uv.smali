.class public final Luv;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# static fields
.field private static GA:J

.field private static GB:Z

.field private static Gz:Landroid/view/ViewTreeObserver$OnDrawListener;


# instance fields
.field private Gw:J

.field private Gx:Z

.field private Gy:Z

.field private hB:Landroid/view/View;

.field private hV:J


# direct methods
.method public constructor <init>(Landroid/animation/ValueAnimator;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Luv;->hV:J

    .line 48
    iput-object p2, p0, Luv;->hB:Landroid/view/View;

    .line 49
    invoke-virtual {p1, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Luv;->hV:J

    .line 53
    iput-object p2, p0, Luv;->hB:Landroid/view/View;

    .line 54
    invoke-virtual {p1, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 55
    return-void
.end method

.method public static N(Z)V
    .locals 0

    .prologue
    .line 65
    sput-boolean p0, Luv;->GB:Z

    .line 66
    return-void
.end method

.method public static O(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Luv;->Gz:Landroid/view/ViewTreeObserver$OnDrawListener;

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    sget-object v1, Luv;->Gz:Landroid/view/ViewTreeObserver$OnDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnDrawListener(Landroid/view/ViewTreeObserver$OnDrawListener;)V

    .line 72
    :cond_0
    new-instance v0, Luw;

    invoke-direct {v0}, Luw;-><init>()V

    sput-object v0, Luv;->Gz:Landroid/view/ViewTreeObserver$OnDrawListener;

    .line 83
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    sget-object v1, Luv;->Gz:Landroid/view/ViewTreeObserver$OnDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnDrawListener(Landroid/view/ViewTreeObserver$OnDrawListener;)V

    .line 84
    const/4 v0, 0x1

    sput-boolean v0, Luv;->GB:Z

    .line 85
    return-void
.end method

.method static synthetic go()J
    .locals 4

    .prologue
    .line 32
    sget-wide v0, Luv;->GA:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    sput-wide v2, Luv;->GA:J

    return-wide v0
.end method


# virtual methods
.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 59
    check-cast p1, Landroid/animation/ValueAnimator;

    .line 60
    invoke-virtual {p1, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 61
    invoke-virtual {p0, p1}, Luv;->onAnimationUpdate(Landroid/animation/ValueAnimator;)V

    .line 62
    return-void
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10

    .prologue
    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 89
    iget-wide v2, p0, Luv;->hV:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 90
    sget-wide v2, Luv;->GA:J

    iput-wide v2, p0, Luv;->Gw:J

    .line 91
    iput-wide v0, p0, Luv;->hV:J

    .line 94
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v2

    .line 95
    iget-boolean v4, p0, Luv;->Gx:Z

    if-nez v4, :cond_2

    sget-boolean v4, Luv;->GB:Z

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    .line 101
    const/4 v4, 0x1

    iput-boolean v4, p0, Luv;->Gx:Z

    .line 102
    sget-wide v4, Luv;->GA:J

    iget-wide v6, p0, Luv;->Gw:J

    sub-long/2addr v4, v6

    .line 106
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-nez v6, :cond_3

    iget-wide v6, p0, Luv;->hV:J

    const-wide/16 v8, 0x3e8

    add-long/2addr v6, v8

    cmp-long v6, v0, v6

    if-gez v6, :cond_3

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_3

    .line 109
    iget-object v0, p0, Luv;->hB:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 110
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 130
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Luv;->Gx:Z

    .line 134
    :cond_2
    return-void

    .line 114
    :cond_3
    const-wide/16 v6, 0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_4

    iget-wide v6, p0, Luv;->hV:J

    const-wide/16 v8, 0x3e8

    add-long/2addr v6, v8

    cmp-long v6, v0, v6

    if-gez v6, :cond_4

    iget-boolean v6, p0, Luv;->Gy:Z

    if-nez v6, :cond_4

    iget-wide v6, p0, Luv;->hV:J

    const-wide/16 v8, 0x10

    add-long/2addr v6, v8

    cmp-long v0, v0, v6

    if-lez v0, :cond_4

    const-wide/16 v0, 0x10

    cmp-long v0, v2, v0

    if-lez v0, :cond_4

    .line 118
    const-wide/16 v0, 0x10

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Luv;->Gy:Z

    goto :goto_0

    .line 121
    :cond_4
    const-wide/16 v0, 0x1

    cmp-long v0, v4, v0

    if-lez v0, :cond_1

    .line 122
    iget-object v0, p0, Luv;->hB:Landroid/view/View;

    new-instance v1, Lux;

    invoke-direct {v1, p0, p1}, Lux;-><init>(Luv;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
