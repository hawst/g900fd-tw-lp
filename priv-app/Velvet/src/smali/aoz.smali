.class public final Laoz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile akA:[Laoz;


# instance fields
.field private aez:I

.field private ahL:Ljava/lang/String;

.field private aiC:Ljava/lang/String;

.field private ajc:Ljava/lang/String;

.field private akv:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7776
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7777
    iput v1, p0, Laoz;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laoz;->aiC:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laoz;->ahL:Ljava/lang/String;

    iput v1, p0, Laoz;->akv:I

    const-string v0, ""

    iput-object v0, p0, Laoz;->ajc:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laoz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laoz;->eCz:I

    .line 7778
    return-void
.end method

.method public static rL()[Laoz;
    .locals 2

    .prologue
    .line 7678
    sget-object v0, Laoz;->akA:[Laoz;

    if-nez v0, :cond_1

    .line 7679
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 7681
    :try_start_0
    sget-object v0, Laoz;->akA:[Laoz;

    if-nez v0, :cond_0

    .line 7682
    const/4 v0, 0x0

    new-array v0, v0, [Laoz;

    sput-object v0, Laoz;->akA:[Laoz;

    .line 7684
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7686
    :cond_1
    sget-object v0, Laoz;->akA:[Laoz;

    return-object v0

    .line 7684
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 7672
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laoz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoz;->aiC:Ljava/lang/String;

    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoz;->ahL:Ljava/lang/String;

    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Laoz;->akv:I

    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laoz;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoz;->ajc:Ljava/lang/String;

    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laoz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 7794
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7795
    const/4 v0, 0x1

    iget-object v1, p0, Laoz;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7797
    :cond_0
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7798
    const/4 v0, 0x2

    iget-object v1, p0, Laoz;->ahL:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7800
    :cond_1
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 7801
    const/4 v0, 0x3

    iget v1, p0, Laoz;->akv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 7803
    :cond_2
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 7804
    const/4 v0, 0x4

    iget-object v1, p0, Laoz;->ajc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7806
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7807
    return-void
.end method

.method public final cC(Ljava/lang/String;)Laoz;
    .locals 1

    .prologue
    .line 7697
    if-nez p1, :cond_0

    .line 7698
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7700
    :cond_0
    iput-object p1, p0, Laoz;->aiC:Ljava/lang/String;

    .line 7701
    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoz;->aez:I

    .line 7702
    return-object p0
.end method

.method public final cD(Ljava/lang/String;)Laoz;
    .locals 1

    .prologue
    .line 7719
    if-nez p1, :cond_0

    .line 7720
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7722
    :cond_0
    iput-object p1, p0, Laoz;->ahL:Ljava/lang/String;

    .line 7723
    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoz;->aez:I

    .line 7724
    return-object p0
.end method

.method public final cE(Ljava/lang/String;)Laoz;
    .locals 1

    .prologue
    .line 7760
    if-nez p1, :cond_0

    .line 7761
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7763
    :cond_0
    iput-object p1, p0, Laoz;->ajc:Ljava/lang/String;

    .line 7764
    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laoz;->aez:I

    .line 7765
    return-object p0
.end method

.method public final cN(I)Laoz;
    .locals 1

    .prologue
    .line 7741
    iput p1, p0, Laoz;->akv:I

    .line 7742
    iget v0, p0, Laoz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laoz;->aez:I

    .line 7743
    return-object p0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7694
    iget-object v0, p0, Laoz;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method public final hasText()Z
    .locals 1

    .prologue
    .line 7705
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 7811
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7812
    iget v1, p0, Laoz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7813
    const/4 v1, 0x1

    iget-object v2, p0, Laoz;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7816
    :cond_0
    iget v1, p0, Laoz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7817
    const/4 v1, 0x2

    iget-object v2, p0, Laoz;->ahL:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7820
    :cond_1
    iget v1, p0, Laoz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 7821
    const/4 v1, 0x3

    iget v2, p0, Laoz;->akv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7824
    :cond_2
    iget v1, p0, Laoz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 7825
    const/4 v1, 0x4

    iget-object v2, p0, Laoz;->ajc:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7828
    :cond_3
    return v0
.end method

.method public final pc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7716
    iget-object v0, p0, Laoz;->ahL:Ljava/lang/String;

    return-object v0
.end method

.method public final pd()Z
    .locals 1

    .prologue
    .line 7727
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7757
    iget-object v0, p0, Laoz;->ajc:Ljava/lang/String;

    return-object v0
.end method

.method public final rM()I
    .locals 1

    .prologue
    .line 7738
    iget v0, p0, Laoz;->akv:I

    return v0
.end method

.method public final rN()Z
    .locals 1

    .prologue
    .line 7746
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rO()Z
    .locals 1

    .prologue
    .line 7768
    iget v0, p0, Laoz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
