.class public final Lane;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agj:Ljava/lang/String;

.field private agk:Ljava/lang/String;

.field private agl:Ljava/lang/String;

.field private agm:I

.field private agn:I

.field private ago:Ljava/lang/String;

.field private agp:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17831
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 17832
    iput v1, p0, Lane;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lane;->agj:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lane;->agk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lane;->agl:Ljava/lang/String;

    iput v1, p0, Lane;->agm:I

    iput v1, p0, Lane;->agn:I

    const-string v0, ""

    iput-object v0, p0, Lane;->ago:Ljava/lang/String;

    iput-boolean v1, p0, Lane;->agp:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lane;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lane;->eCz:I

    .line 17833
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 17667
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lane;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lane;->agj:Ljava/lang/String;

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lane;->agk:Ljava/lang/String;

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lane;->agl:Ljava/lang/String;

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lane;->agm:I

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lane;->agn:I

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lane;->ago:Ljava/lang/String;

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lane;->agp:Z

    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lane;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 17852
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 17853
    const/4 v0, 0x1

    iget-object v1, p0, Lane;->agj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 17855
    :cond_0
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 17856
    const/4 v0, 0x2

    iget-object v1, p0, Lane;->agk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 17858
    :cond_1
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 17859
    const/4 v0, 0x3

    iget-object v1, p0, Lane;->agl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 17861
    :cond_2
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 17862
    const/4 v0, 0x4

    iget v1, p0, Lane;->agm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 17864
    :cond_3
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 17865
    const/4 v0, 0x5

    iget v1, p0, Lane;->agn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 17867
    :cond_4
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 17868
    const/4 v0, 0x6

    iget-object v1, p0, Lane;->ago:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 17870
    :cond_5
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 17871
    const/4 v0, 0x7

    iget-boolean v1, p0, Lane;->agp:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 17873
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 17874
    return-void
.end method

.method public final aR(Z)Lane;
    .locals 1

    .prologue
    .line 17818
    iput-boolean p1, p0, Lane;->agp:Z

    .line 17819
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lane;->aez:I

    .line 17820
    return-object p0
.end method

.method public final ah(Ljava/lang/String;)Lane;
    .locals 1

    .prologue
    .line 17692
    if-nez p1, :cond_0

    .line 17693
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17695
    :cond_0
    iput-object p1, p0, Lane;->agj:Ljava/lang/String;

    .line 17696
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lane;->aez:I

    .line 17697
    return-object p0
.end method

.method public final ai(Ljava/lang/String;)Lane;
    .locals 1

    .prologue
    .line 17714
    if-nez p1, :cond_0

    .line 17715
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17717
    :cond_0
    iput-object p1, p0, Lane;->agk:Ljava/lang/String;

    .line 17718
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lane;->aez:I

    .line 17719
    return-object p0
.end method

.method public final aj(Ljava/lang/String;)Lane;
    .locals 1

    .prologue
    .line 17736
    if-nez p1, :cond_0

    .line 17737
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17739
    :cond_0
    iput-object p1, p0, Lane;->agl:Ljava/lang/String;

    .line 17740
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lane;->aez:I

    .line 17741
    return-object p0
.end method

.method public final ak(Ljava/lang/String;)Lane;
    .locals 1

    .prologue
    .line 17796
    if-nez p1, :cond_0

    .line 17797
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17799
    :cond_0
    iput-object p1, p0, Lane;->ago:Ljava/lang/String;

    .line 17800
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lane;->aez:I

    .line 17801
    return-object p0
.end method

.method public final ck(I)Lane;
    .locals 1

    .prologue
    .line 17758
    iput p1, p0, Lane;->agm:I

    .line 17759
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lane;->aez:I

    .line 17760
    return-object p0
.end method

.method public final cl(I)Lane;
    .locals 1

    .prologue
    .line 17777
    iput p1, p0, Lane;->agn:I

    .line 17778
    iget v0, p0, Lane;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lane;->aez:I

    .line 17779
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 17878
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 17879
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 17880
    const/4 v1, 0x1

    iget-object v2, p0, Lane;->agj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17883
    :cond_0
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 17884
    const/4 v1, 0x2

    iget-object v2, p0, Lane;->agk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17887
    :cond_1
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 17888
    const/4 v1, 0x3

    iget-object v2, p0, Lane;->agl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17891
    :cond_2
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 17892
    const/4 v1, 0x4

    iget v2, p0, Lane;->agm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17895
    :cond_3
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 17896
    const/4 v1, 0x5

    iget v2, p0, Lane;->agn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17899
    :cond_4
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 17900
    const/4 v1, 0x6

    iget-object v2, p0, Lane;->ago:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17903
    :cond_5
    iget v1, p0, Lane;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 17904
    const/4 v1, 0x7

    iget-boolean v2, p0, Lane;->agp:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17907
    :cond_6
    return v0
.end method

.method public final oA()Z
    .locals 1

    .prologue
    .line 17722
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17733
    iget-object v0, p0, Lane;->agl:Ljava/lang/String;

    return-object v0
.end method

.method public final oC()Z
    .locals 1

    .prologue
    .line 17744
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oD()I
    .locals 1

    .prologue
    .line 17755
    iget v0, p0, Lane;->agm:I

    return v0
.end method

.method public final oE()Z
    .locals 1

    .prologue
    .line 17763
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oF()I
    .locals 1

    .prologue
    .line 17774
    iget v0, p0, Lane;->agn:I

    return v0
.end method

.method public final oG()Z
    .locals 1

    .prologue
    .line 17782
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17793
    iget-object v0, p0, Lane;->ago:Ljava/lang/String;

    return-object v0
.end method

.method public final oI()Z
    .locals 1

    .prologue
    .line 17804
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oJ()Z
    .locals 1

    .prologue
    .line 17815
    iget-boolean v0, p0, Lane;->agp:Z

    return v0
.end method

.method public final ox()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17689
    iget-object v0, p0, Lane;->agj:Ljava/lang/String;

    return-object v0
.end method

.method public final oy()Z
    .locals 1

    .prologue
    .line 17700
    iget v0, p0, Lane;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oz()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17711
    iget-object v0, p0, Lane;->agk:Ljava/lang/String;

    return-object v0
.end method
