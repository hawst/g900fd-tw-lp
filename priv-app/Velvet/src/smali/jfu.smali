.class public final Ljfu;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eiU:[Ljfu;


# instance fields
.field private aez:I

.field private eiV:I

.field private eiW:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16155
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 16156
    const/4 v0, 0x0

    iput v0, p0, Ljfu;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljfu;->eiV:I

    const-string v0, ""

    iput-object v0, p0, Ljfu;->eiW:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljfu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfu;->eCz:I

    .line 16157
    return-void
.end method

.method public static bjL()[Ljfu;
    .locals 2

    .prologue
    .line 16101
    sget-object v0, Ljfu;->eiU:[Ljfu;

    if-nez v0, :cond_1

    .line 16102
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 16104
    :try_start_0
    sget-object v0, Ljfu;->eiU:[Ljfu;

    if-nez v0, :cond_0

    .line 16105
    const/4 v0, 0x0

    new-array v0, v0, [Ljfu;

    sput-object v0, Ljfu;->eiU:[Ljfu;

    .line 16107
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16109
    :cond_1
    sget-object v0, Ljfu;->eiU:[Ljfu;

    return-object v0

    .line 16107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 16082
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfu;->eiV:I

    iget v0, p0, Ljfu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfu;->eiW:Ljava/lang/String;

    iget v0, p0, Ljfu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 16171
    iget v0, p0, Ljfu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 16172
    const/4 v0, 0x1

    iget v1, p0, Ljfu;->eiV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 16174
    :cond_0
    iget v0, p0, Ljfu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 16175
    const/4 v0, 0x2

    iget-object v1, p0, Ljfu;->eiW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 16177
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 16178
    return-void
.end method

.method public final bjM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16136
    iget-object v0, p0, Ljfu;->eiW:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 16182
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 16183
    iget v1, p0, Ljfu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 16184
    const/4 v1, 0x1

    iget v2, p0, Ljfu;->eiV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16187
    :cond_0
    iget v1, p0, Ljfu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 16188
    const/4 v1, 0x2

    iget-object v2, p0, Ljfu;->eiW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16191
    :cond_1
    return v0
.end method

.method public final ut(Ljava/lang/String;)Ljfu;
    .locals 1

    .prologue
    .line 16139
    if-nez p1, :cond_0

    .line 16140
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16142
    :cond_0
    iput-object p1, p0, Ljfu;->eiW:Ljava/lang/String;

    .line 16143
    iget v0, p0, Ljfu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfu;->aez:I

    .line 16144
    return-object p0
.end method
