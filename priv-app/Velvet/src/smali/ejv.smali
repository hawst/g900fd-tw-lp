.class public final Lejv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lekg;
.implements Leti;


# instance fields
.field private Zy:I

.field private ccc:I

.field private final ccd:Z

.field private cce:I

.field private ccf:I

.field private ccg:I

.field private cch:I

.field private cci:Z

.field private ccj:Z

.field private cck:I

.field private ccl:I

.field private ccm:Lejv;

.field private ccn:Lejv;

.field private final mScrollViewControl:Lekf;

.field private final mView:Landroid/view/View;

.field private zE:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lekf;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lejv;->mView:Landroid/view/View;

    .line 110
    iput-object p2, p0, Lejv;->mScrollViewControl:Lekf;

    .line 111
    iput-boolean p3, p0, Lejv;->ccd:Z

    .line 112
    iput-object p0, p0, Lejv;->ccm:Lejv;

    .line 113
    iput-object p0, p0, Lejv;->ccn:Lejv;

    .line 114
    iget-boolean v0, p0, Lejv;->zE:Z

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lejv;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lejv;->mScrollViewControl:Lekf;

    invoke-interface {v0, p0}, Lekf;->a(Lekg;)V

    iput-boolean v1, p0, Lejv;->zE:Z

    .line 115
    :cond_0
    return-void
.end method

.method private aQ(II)F
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 354
    iget-boolean v1, p0, Lejv;->ccj:Z

    if-eqz v1, :cond_1

    .line 355
    iput-boolean v2, p0, Lejv;->ccj:Z

    .line 356
    iput-boolean v2, p0, Lejv;->cci:Z

    .line 364
    :cond_0
    :goto_0
    return v0

    .line 359
    :cond_1
    neg-int v1, p2

    if-gt p1, v1, :cond_2

    .line 360
    const/4 v0, 0x0

    goto :goto_0

    .line 361
    :cond_2
    if-gez p1, :cond_0

    .line 364
    int-to-float v1, p1

    neg-int v2, p2

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method private aR(II)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const-wide/16 v8, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 370
    iget v0, p0, Lejv;->Zy:I

    if-nez v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    invoke-direct {p0}, Lejv;->atO()I

    move-result v3

    .line 385
    iget v0, p0, Lejv;->ccc:I

    if-gez v0, :cond_9

    .line 386
    iget v0, p0, Lejv;->ccc:I

    neg-int v0, v0

    .line 397
    :goto_1
    iget v2, p0, Lejv;->Zy:I

    .line 398
    iget v5, p0, Lejv;->cch:I

    if-eqz v5, :cond_2

    .line 399
    iget v5, p0, Lejv;->cch:I

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 401
    :cond_2
    iget v5, p0, Lejv;->ccf:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_3

    and-int/lit8 v5, p2, 0x4

    if-eqz v5, :cond_a

    .line 403
    :cond_3
    add-int/2addr v0, v2

    neg-int v0, v0

    .line 432
    :cond_4
    :goto_2
    iget v3, p0, Lejv;->ccl:I

    if-eq v0, v3, :cond_0

    .line 450
    iget v3, p0, Lejv;->ccf:I

    if-ne v3, v7, :cond_d

    move v1, v4

    :cond_5
    :goto_3
    if-nez v1, :cond_11

    .line 454
    iget v1, p0, Lejv;->ccg:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 456
    :goto_4
    iget-boolean v3, p0, Lejv;->ccd:Z

    if-nez v3, :cond_6

    .line 457
    neg-int v1, v1

    .line 459
    :cond_6
    iget v3, p0, Lejv;->ccl:I

    sub-int v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v3, v4, :cond_7

    and-int/lit8 v3, p2, 0x1

    if-eqz v3, :cond_f

    .line 462
    :cond_7
    iget-object v3, p0, Lejv;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 463
    iget-object v3, p0, Lejv;->mView:Landroid/view/View;

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 464
    iget-boolean v1, p0, Lejv;->cci:Z

    if-eqz v1, :cond_8

    .line 465
    iget-object v1, p0, Lejv;->mView:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lejv;->aQ(II)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 478
    :cond_8
    :goto_5
    iput v0, p0, Lejv;->ccl:I

    goto :goto_0

    .line 387
    :cond_9
    iget v0, p0, Lejv;->ccc:I

    iget v2, p0, Lejv;->cck:I

    if-le v0, v2, :cond_13

    .line 388
    iget v0, p0, Lejv;->ccc:I

    iget v2, p0, Lejv;->cck:I

    sub-int/2addr v0, v2

    goto :goto_1

    .line 404
    :cond_a
    iget v5, p0, Lejv;->ccf:I

    if-ne v5, v7, :cond_b

    .line 407
    iget v5, p0, Lejv;->ccg:I

    sub-int v3, v5, v3

    .line 408
    neg-int v5, v2

    add-int/2addr v0, v3

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    .line 409
    :cond_b
    iget v5, p0, Lejv;->ccg:I

    if-le v5, v3, :cond_c

    .line 412
    iget v5, p0, Lejv;->ccg:I

    sub-int v3, v5, v3

    add-int/2addr v0, v3

    goto :goto_2

    .line 413
    :cond_c
    iget v3, p0, Lejv;->ccf:I

    if-eq v3, v4, :cond_4

    and-int/lit8 v3, p2, 0x2

    if-nez v3, :cond_4

    .line 419
    iget v3, p0, Lejv;->ccl:I

    add-int/2addr v3, p1

    .line 420
    add-int v5, v2, v0

    neg-int v5, v5

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 421
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 423
    iget v5, p0, Lejv;->ccf:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_12

    .line 426
    invoke-direct {p0}, Lejv;->atP()I

    move-result v5

    add-int/2addr v0, v5

    neg-int v0, v0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_2

    .line 450
    :cond_d
    iget v3, p0, Lejv;->ccg:I

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lejv;->ccd:Z

    if-eqz v3, :cond_e

    iget v3, p0, Lejv;->ccc:I

    if-gez v3, :cond_5

    move v1, v4

    goto/16 :goto_3

    :cond_e
    iget v3, p0, Lejv;->ccc:I

    iget v5, p0, Lejv;->cck:I

    if-le v3, v5, :cond_5

    move v1, v4

    goto/16 :goto_3

    .line 469
    :cond_f
    iget-object v3, p0, Lejv;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 470
    iget-boolean v3, p0, Lejv;->cci:Z

    if-eqz v3, :cond_10

    .line 471
    invoke-direct {p0, v0, v2}, Lejv;->aQ(II)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 473
    :cond_10
    iget v2, p0, Lejv;->cce:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 474
    cmp-long v2, v8, v8

    if-lez v2, :cond_8

    .line 475
    invoke-virtual {v1, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_5

    :cond_11
    move v1, v0

    goto/16 :goto_4

    :cond_12
    move v0, v3

    goto/16 :goto_2

    :cond_13
    move v0, v1

    goto/16 :goto_1
.end method

.method private atO()I
    .locals 3

    .prologue
    .line 346
    const/4 v1, 0x0

    iget-boolean v0, p0, Lejv;->ccd:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lejv;->ccc:I

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    iget v0, p0, Lejv;->cck:I

    iget v2, p0, Lejv;->ccc:I

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method private atP()I
    .locals 2

    .prologue
    .line 350
    iget v0, p0, Lejv;->cck:I

    iget v1, p0, Lejv;->ccc:I

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private atQ()Z
    .locals 2

    .prologue
    .line 493
    iget v0, p0, Lejv;->Zy:I

    if-eqz v0, :cond_0

    iget v0, p0, Lejv;->ccl:I

    neg-int v0, v0

    iget v1, p0, Lejv;->Zy:I

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final C(IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    iget v0, p0, Lejv;->ccg:I

    if-eq p1, v0, :cond_0

    .line 224
    iput p1, p0, Lejv;->ccg:I

    .line 225
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lejv;->aR(II)V

    .line 227
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 225
    goto :goto_0
.end method

.method public final a(Lejv;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lejv;->ccm:Lejv;

    .line 145
    return-void
.end method

.method public final a(Letj;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 535
    const-string v0, "mTopAt"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    iget-boolean v0, p0, Lejv;->ccd:Z

    if-eqz v0, :cond_1

    const-string v0, "TOP"

    :goto_0
    invoke-virtual {v1, v0, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 536
    const-string v0, "mViewHeight"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lejv;->Zy:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 537
    const-string v0, "mCurrentOffset"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lejv;->ccl:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 538
    const-string v0, "mStickiness"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    iget v0, p0, Lejv;->ccf:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "INVALID_STATE"

    :goto_1
    invoke-virtual {v1, v0, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 539
    iget v0, p0, Lejv;->ccg:I

    if-eqz v0, :cond_0

    .line 540
    const-string v0, "mOffsetFromEdge"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lejv;->ccg:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->bd(J)V

    .line 542
    :cond_0
    return-void

    .line 535
    :cond_1
    const-string v0, "BOTTOM"

    goto :goto_0

    .line 538
    :pswitch_0
    const-string v0, "STICKY"

    goto :goto_1

    :pswitch_1
    const-string v0, "STUCK_TO_SCROLLING_VIEW"

    goto :goto_1

    :pswitch_2
    const-string v0, "STUCK_OFF_SCREEN"

    goto :goto_1

    :pswitch_3
    const-string v0, "NOT_STICKY"

    goto :goto_1

    :pswitch_4
    const-string v0, "NOT_STICKY_TOP_ONLY"

    goto :goto_1

    :pswitch_5
    const-string v0, "NOT_STICKY_WITH_REVEAL_AT_SCROLL_END"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final aa(II)V
    .locals 4

    .prologue
    .line 264
    iget v0, p0, Lejv;->ccc:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lejv;->cck:I

    if-eq p2, v0, :cond_2

    .line 265
    :cond_0
    iget v0, p0, Lejv;->ccc:I

    sub-int v1, v0, p1

    .line 266
    iput p1, p0, Lejv;->ccc:I

    .line 267
    iput p2, p0, Lejv;->cck:I

    .line 269
    const/4 v0, 0x0

    .line 270
    iget v2, p0, Lejv;->ccf:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 274
    iget v0, p0, Lejv;->ccc:I

    iget v2, p0, Lejv;->ccg:I

    if-ge v0, v2, :cond_3

    const/4 v0, 0x2

    .line 277
    :cond_1
    :goto_0
    invoke-direct {p0, v1, v0}, Lejv;->aR(II)V

    .line 279
    :cond_2
    return-void

    .line 274
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final b(Lejv;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lejv;->ccn:Lejv;

    .line 149
    return-void
.end method

.method public final d(IZZ)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 192
    iget v0, p0, Lejv;->ccf:I

    if-eq p1, v0, :cond_1

    .line 194
    iget v0, p0, Lejv;->ccf:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lejv;->ccf:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_2

    :cond_0
    move v0, v2

    .line 196
    :goto_0
    iput p1, p0, Lejv;->ccf:I

    .line 197
    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    move v0, v3

    :goto_1
    if-eqz p2, :cond_4

    :goto_2
    or-int/2addr v0, v2

    invoke-direct {p0, v1, v0}, Lejv;->aR(II)V

    .line 201
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 194
    goto :goto_0

    :cond_3
    move v0, v1

    .line 197
    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2
.end method

.method public final dr(I)V
    .locals 0

    .prologue
    .line 508
    return-void
.end method

.method public final eW(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 241
    if-eqz p1, :cond_0

    .line 242
    iput-boolean v0, p0, Lejv;->cci:Z

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lejv;->ccj:Z

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    iput-boolean v0, p0, Lejv;->ccj:Z

    goto :goto_0
.end method

.method public final hT(I)V
    .locals 0

    .prologue
    .line 179
    iput p1, p0, Lejv;->cce:I

    .line 180
    return-void
.end method

.method public final hU(I)V
    .locals 2

    .prologue
    .line 231
    iget v0, p0, Lejv;->cch:I

    if-eq p1, v0, :cond_0

    .line 232
    iput p1, p0, Lejv;->cch:I

    .line 233
    iget v0, p0, Lejv;->cch:I

    if-eqz v0, :cond_0

    iget v0, p0, Lejv;->ccl:I

    neg-int v1, p1

    if-ge v0, v1, :cond_0

    .line 234
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lejv;->aR(II)V

    .line 237
    :cond_0
    return-void
.end method

.method public final hide()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lejv;->aR(II)V

    .line 216
    return-void
.end method

.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 252
    sub-int v3, p5, p3

    .line 253
    iget v0, p0, Lejv;->Zy:I

    if-eq v3, v0, :cond_0

    .line 254
    iget v0, p0, Lejv;->Zy:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lejv;->Zy:I

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput v3, p0, Lejv;->Zy:I

    if-eqz v0, :cond_2

    :goto_1
    invoke-direct {p0, v2, v1}, Lejv;->aR(II)V

    .line 256
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 254
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final show()V
    .locals 2

    .prologue
    .line 205
    iget v0, p0, Lejv;->ccf:I

    if-eqz v0, :cond_0

    iget v0, p0, Lejv;->ccf:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 206
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lejv;->aR(II)V

    .line 211
    :cond_1
    return-void
.end method

.method public final vS()V
    .locals 0

    .prologue
    .line 307
    invoke-virtual {p0}, Lejv;->vT()V

    .line 308
    return-void
.end method

.method public final vT()V
    .locals 7

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    iget v4, p0, Lejv;->ccf:I

    if-eq v4, v2, :cond_0

    iget v4, p0, Lejv;->ccf:I

    if-eq v4, v0, :cond_0

    iget v4, p0, Lejv;->ccf:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    iget v4, p0, Lejv;->ccf:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_3

    :cond_0
    move v4, v1

    :goto_0
    if-eqz v4, :cond_2

    .line 285
    iget v4, p0, Lejv;->ccc:I

    if-gtz v4, :cond_6

    .line 301
    :cond_1
    :goto_1
    if-eqz v0, :cond_a

    move v0, v2

    :goto_2
    invoke-direct {p0, v1, v0}, Lejv;->aR(II)V

    .line 303
    :cond_2
    return-void

    .line 283
    :cond_3
    iget-boolean v4, p0, Lejv;->ccd:Z

    if-nez v4, :cond_4

    iget v4, p0, Lejv;->ccg:I

    if-nez v4, :cond_4

    move v4, v0

    goto :goto_0

    :cond_4
    iget v4, p0, Lejv;->ccg:I

    iget v5, p0, Lejv;->Zy:I

    add-int/2addr v4, v5

    invoke-direct {p0}, Lejv;->atO()I

    move-result v5

    if-gt v4, v5, :cond_5

    move v4, v0

    goto :goto_0

    :cond_5
    move v4, v1

    goto :goto_0

    .line 288
    :cond_6
    iget v4, p0, Lejv;->ccf:I

    if-ne v4, v3, :cond_8

    .line 291
    invoke-direct {p0}, Lejv;->atP()I

    move-result v4

    .line 292
    iget-object v5, p0, Lejv;->ccm:Lejv;

    invoke-direct {v5}, Lejv;->atQ()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lejv;->ccn:Lejv;

    iget v6, v5, Lejv;->Zy:I

    iget v5, v5, Lejv;->ccg:I

    add-int/2addr v5, v6

    if-gt v4, v5, :cond_7

    move v4, v0

    :goto_3
    if-nez v4, :cond_1

    move v0, v1

    goto :goto_1

    :cond_7
    move v4, v1

    goto :goto_3

    .line 294
    :cond_8
    iget v0, p0, Lejv;->ccc:I

    iget v4, p0, Lejv;->cck:I

    if-lt v0, v4, :cond_9

    move v0, v1

    .line 296
    goto :goto_1

    .line 299
    :cond_9
    iget-object v0, p0, Lejv;->ccm:Lejv;

    invoke-direct {v0}, Lejv;->atQ()Z

    move-result v0

    goto :goto_1

    :cond_a
    move v0, v3

    .line 301
    goto :goto_2
.end method

.method public final vU()V
    .locals 0

    .prologue
    .line 503
    return-void
.end method

.method public final vV()V
    .locals 0

    .prologue
    .line 513
    return-void
.end method

.method public final vW()V
    .locals 0

    .prologue
    .line 518
    return-void
.end method
