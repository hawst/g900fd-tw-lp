.class public final Lfyy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;
.implements Landroid/text/style/LineHeightSpan;


# instance fields
.field private final cDy:I

.field private final eX:I

.field private final sd:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;II)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lfyy;->sd:Landroid/graphics/drawable/Drawable;

    .line 143
    iput p2, p0, Lfyy;->eX:I

    .line 144
    iput p3, p0, Lfyy;->cDy:I

    .line 145
    return-void
.end method


# virtual methods
.method public final chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V
    .locals 3

    .prologue
    .line 180
    check-cast p1, Landroid/text/Spanned;

    invoke-interface {p1, p0}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    if-ne p3, v0, :cond_1

    .line 181
    iget v0, p0, Lfyy;->eX:I

    .line 183
    iget v1, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int/2addr v1, p5

    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, p4

    sub-int v1, v0, v1

    .line 184
    if-lez v1, :cond_0

    .line 185
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int/2addr v1, v2

    iput v1, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 187
    :cond_0
    iget v1, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    add-int/2addr v1, p5

    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, p4

    sub-int/2addr v0, v1

    .line 188
    if-lez v0, :cond_1

    .line 189
    iget v1, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    add-int/2addr v0, v1

    iput v0, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 191
    :cond_1
    return-void
.end method

.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 5

    .prologue
    .line 158
    if-nez p11, :cond_0

    .line 174
    :goto_0
    return-void

    .line 160
    :cond_0
    check-cast p8, Landroid/text/Spanned;

    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 161
    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v2

    .line 163
    if-gez p4, :cond_1

    .line 164
    iget v1, p0, Lfyy;->eX:I

    sub-int/2addr p3, v1

    .line 169
    :cond_1
    iget v1, p0, Lfyy;->eX:I

    if-ge v1, p7, :cond_2

    iget v1, p0, Lfyy;->eX:I

    sub-int v1, p7, v1

    div-int/lit8 v1, v1, 0x2

    .line 170
    :goto_1
    iget v3, p0, Lfyy;->eX:I

    add-int/2addr v3, v1

    .line 172
    iget-object v4, p0, Lfyy;->sd:Landroid/graphics/drawable/Drawable;

    add-int/2addr v1, v2

    iget v2, p0, Lfyy;->eX:I

    add-int/2addr v2, p3

    invoke-virtual {v4, p3, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 173
    iget-object v1, p0, Lfyy;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 169
    goto :goto_1
.end method

.method public final getLeadingMargin(Z)I
    .locals 2

    .prologue
    .line 150
    if-eqz p1, :cond_0

    iget v0, p0, Lfyy;->eX:I

    iget v1, p0, Lfyy;->cDy:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
