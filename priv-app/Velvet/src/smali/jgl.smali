.class public final Ljgl;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afY:Ljava/lang/String;

.field private aia:Ljava/lang/String;

.field private ekc:I

.field private ekd:Ljava/lang/String;

.field private eke:Ljava/lang/String;

.field private ekf:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53293
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 53294
    const/4 v0, 0x0

    iput v0, p0, Ljgl;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljgl;->ekc:I

    const-string v0, ""

    iput-object v0, p0, Ljgl;->aia:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgl;->afY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgl;->ekd:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgl;->eke:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgl;->ekf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljgl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgl;->eCz:I

    .line 53295
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 53136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljgl;->ekc:I

    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgl;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgl;->aia:Ljava/lang/String;

    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgl;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgl;->afY:Ljava/lang/String;

    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgl;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgl;->ekd:Ljava/lang/String;

    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljgl;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgl;->eke:Ljava/lang/String;

    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljgl;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgl;->ekf:Ljava/lang/String;

    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljgl;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 53313
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 53314
    const/4 v0, 0x1

    iget v1, p0, Ljgl;->ekc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 53316
    :cond_0
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 53317
    const/4 v0, 0x2

    iget-object v1, p0, Ljgl;->aia:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 53319
    :cond_1
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 53320
    const/4 v0, 0x3

    iget-object v1, p0, Ljgl;->afY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 53322
    :cond_2
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 53323
    const/4 v0, 0x4

    iget-object v1, p0, Ljgl;->ekd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 53325
    :cond_3
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 53326
    const/4 v0, 0x5

    iget-object v1, p0, Ljgl;->eke:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 53328
    :cond_4
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 53329
    const/4 v0, 0x6

    iget-object v1, p0, Ljgl;->ekf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 53331
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 53332
    return-void
.end method

.method public final bkn()I
    .locals 1

    .prologue
    .line 53167
    iget v0, p0, Ljgl;->ekc:I

    return v0
.end method

.method public final bko()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53230
    iget-object v0, p0, Ljgl;->ekd:Ljava/lang/String;

    return-object v0
.end method

.method public final bkp()Z
    .locals 1

    .prologue
    .line 53241
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bkq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53252
    iget-object v0, p0, Ljgl;->eke:Ljava/lang/String;

    return-object v0
.end method

.method public final bkr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53274
    iget-object v0, p0, Ljgl;->ekf:Ljava/lang/String;

    return-object v0
.end method

.method public final bks()Z
    .locals 1

    .prologue
    .line 53285
    iget v0, p0, Ljgl;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 53336
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 53337
    iget v1, p0, Ljgl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 53338
    const/4 v1, 0x1

    iget v2, p0, Ljgl;->ekc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 53341
    :cond_0
    iget v1, p0, Ljgl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 53342
    const/4 v1, 0x2

    iget-object v2, p0, Ljgl;->aia:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53345
    :cond_1
    iget v1, p0, Ljgl;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 53346
    const/4 v1, 0x3

    iget-object v2, p0, Ljgl;->afY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53349
    :cond_2
    iget v1, p0, Ljgl;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 53350
    const/4 v1, 0x4

    iget-object v2, p0, Ljgl;->ekd:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53353
    :cond_3
    iget v1, p0, Ljgl;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 53354
    const/4 v1, 0x5

    iget-object v2, p0, Ljgl;->eke:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53357
    :cond_4
    iget v1, p0, Ljgl;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 53358
    const/4 v1, 0x6

    iget-object v2, p0, Ljgl;->ekf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53361
    :cond_5
    return v0
.end method

.method public final pB(I)Ljgl;
    .locals 1

    .prologue
    .line 53170
    const/4 v0, 0x2

    iput v0, p0, Ljgl;->ekc:I

    .line 53171
    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgl;->aez:I

    .line 53172
    return-object p0
.end method

.method public final pr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53186
    iget-object v0, p0, Ljgl;->aia:Ljava/lang/String;

    return-object v0
.end method

.method public final uG(Ljava/lang/String;)Ljgl;
    .locals 1

    .prologue
    .line 53233
    if-nez p1, :cond_0

    .line 53234
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53236
    :cond_0
    iput-object p1, p0, Ljgl;->ekd:Ljava/lang/String;

    .line 53237
    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljgl;->aez:I

    .line 53238
    return-object p0
.end method

.method public final uH(Ljava/lang/String;)Ljgl;
    .locals 1

    .prologue
    .line 53255
    if-nez p1, :cond_0

    .line 53256
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53258
    :cond_0
    iput-object p1, p0, Ljgl;->eke:Ljava/lang/String;

    .line 53259
    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljgl;->aez:I

    .line 53260
    return-object p0
.end method

.method public final uI(Ljava/lang/String;)Ljgl;
    .locals 1

    .prologue
    .line 53277
    if-nez p1, :cond_0

    .line 53278
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53280
    :cond_0
    iput-object p1, p0, Ljgl;->ekf:Ljava/lang/String;

    .line 53281
    iget v0, p0, Ljgl;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljgl;->aez:I

    .line 53282
    return-object p0
.end method
