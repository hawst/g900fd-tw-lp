.class final Lhdl;
.super Lbwu;
.source "PG"


# instance fields
.field private synthetic ddr:Lhcu;

.field private dds:I

.field private ddt:I


# direct methods
.method public constructor <init>(Lhcu;Landroid/app/Activity;IIII)V
    .locals 9

    .prologue
    .line 315
    iput-object p1, p0, Lhdl;->ddr:Lhcu;

    .line 317
    const v3, 0x7f0a06c0

    const v5, 0x7f0a06c4

    iget-object v6, p1, Lhcu;->ddi:Lesk;

    const v7, 0x7f0a06c5

    iget-object v8, p1, Lhcu;->ddj:Lesk;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v4, p6

    invoke-direct/range {v0 .. v8}, Lbwu;-><init>(Landroid/app/Activity;IIIILesk;ILesk;)V

    .line 325
    iput p4, p0, Lhdl;->dds:I

    .line 326
    iput p5, p0, Lhdl;->ddt:I

    .line 327
    return-void
.end method


# virtual methods
.method public final Be()V
    .locals 5

    .prologue
    .line 331
    invoke-super {p0}, Lbwu;->Be()V

    .line 335
    iget-object v0, p0, Lhdl;->ddr:Lhcu;

    iget-boolean v0, v0, Lhcu;->dcY:Z

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lhdl;->ddr:Lhcu;

    invoke-virtual {v0}, Lhcu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a06c1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lhdl;->ddr:Lhcu;

    iget-object v4, v4, Lhcu;->dcX:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 338
    sget v1, Lbwu;->aMP:I

    invoke-virtual {p0, v1, v0}, Lhdl;->a(ILjava/lang/CharSequence;)V

    .line 342
    :cond_0
    iget v0, p0, Lhdl;->dds:I

    if-eqz v0, :cond_1

    .line 343
    sget v0, Lbwu;->aMM:I

    iget-object v1, p0, Lhdl;->ddr:Lhcu;

    invoke-virtual {v1}, Lhcu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lhdl;->dds:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhdl;->a(ILjava/lang/CharSequence;)V

    .line 346
    :cond_1
    iget v0, p0, Lhdl;->ddt:I

    if-eqz v0, :cond_2

    .line 347
    sget v0, Lbwu;->aMN:I

    iget-object v1, p0, Lhdl;->ddr:Lhcu;

    invoke-virtual {v1}, Lhcu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lhdl;->ddt:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhdl;->a(ILjava/lang/CharSequence;)V

    .line 350
    :cond_2
    return-void
.end method
