.class public final Lgmv;
.super Lgmq;
.source "PG"


# instance fields
.field private final bZx:Landroid/location/Location;

.field private final cPB:Ljava/lang/String;

.field private final cPC:Ljava/util/List;

.field private final cPD:Ljwy;

.field private final mAuthTokenHelper:Lgln;

.field private final mLocationHelper:Lglo;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Lgln;Lgdo;Lglo;Ljava/lang/String;Ljava/util/List;Landroid/location/Location;Ljwy;)V
    .locals 1

    .prologue
    .line 69
    const-string v0, "S3UserInfoBuilderTask"

    invoke-direct {p0, v0}, Lgmq;-><init>(Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lgmv;->mAuthTokenHelper:Lgln;

    .line 71
    iput-object p2, p0, Lgmv;->mSpeechSettings:Lgdo;

    .line 72
    iput-object p3, p0, Lgmv;->mLocationHelper:Lglo;

    .line 73
    iput-object p4, p0, Lgmv;->cPB:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lgmv;->cPC:Ljava/util/List;

    .line 75
    iput-object p6, p0, Lgmv;->bZx:Landroid/location/Location;

    .line 76
    iput-object p7, p0, Lgmv;->cPD:Ljwy;

    .line 77
    return-void
.end method

.method public static a(Lgln;Lgdo;Lglo;Ljava/lang/String;Ljava/util/List;Landroid/location/Location;)Ljava/util/concurrent/Callable;
    .locals 8
    .param p0    # Lgln;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lgdo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lglo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54
    new-instance v0, Lgmv;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lgmv;-><init>(Lgln;Lgdo;Lglo;Ljava/lang/String;Ljava/util/List;Landroid/location/Location;Ljwy;)V

    return-object v0
.end method

.method public static a(Lgln;Ljwy;Lgdo;)Ljava/util/concurrent/Callable;
    .locals 8
    .param p0    # Lgln;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljwy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lgdo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 61
    new-instance v0, Lgmv;

    move-object v1, p0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lgmv;-><init>(Lgln;Lgdo;Lglo;Ljava/lang/String;Ljava/util/List;Landroid/location/Location;Ljwy;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljwy;)V
    .locals 7

    .prologue
    .line 112
    iget-object v0, p0, Lgmv;->mAuthTokenHelper:Lgln;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, p1, v2, v3}, Lgln;->l(Ljava/lang/String;J)Ljava/util/Collection;

    move-result-object v2

    .line 114
    if-eqz v2, :cond_1

    .line 115
    iget-object v0, p2, Ljwy;->eJQ:[Ljws;

    array-length v1, v0

    .line 116
    iget-object v0, p2, Ljwy;->eJQ:[Ljws;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v0, v3}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljws;

    iput-object v0, p2, Ljwy;->eJQ:[Ljws;

    .line 117
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 118
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 119
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 120
    iget-object v4, p0, Lgmv;->mAuthTokenHelper:Lgln;

    invoke-interface {v4, v1}, Lgln;->iB(Ljava/lang/String;)Z

    move-result v4

    .line 122
    iget-object v5, p2, Ljwy;->eJQ:[Ljws;

    new-instance v6, Ljws;

    invoke-direct {v6}, Ljws;-><init>()V

    const-string v1, "oauth2:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v6, v1}, Ljws;->zw(Ljava/lang/String;)Ljws;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljws;->ji(Z)Ljws;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljws;->zx(Ljava/lang/String;)Ljws;

    move-result-object v0

    aput-object v0, v5, v2

    .line 126
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 127
    goto :goto_0

    :cond_0
    move-object v1, p1

    .line 122
    goto :goto_1

    .line 129
    :cond_1
    const-string v0, "S3UserInfoBuilderTask"

    const-string v1, "Failed fetching auth."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 131
    :cond_2
    return-void
.end method


# virtual methods
.method protected final synthetic aHr()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lgmv;->cPD:Ljwy;

    if-nez v0, :cond_5

    new-instance v0, Ljwy;

    invoke-direct {v0}, Ljwy;-><init>()V

    iget-object v1, p0, Lgmv;->mSpeechSettings:Lgdo;

    invoke-interface {v1}, Lgdo;->aFb()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljwy;->zJ(Ljava/lang/String;)Ljwy;

    move-result-object v1

    new-instance v0, Ljwt;

    invoke-direct {v0}, Ljwt;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljwt;->zy(Ljava/lang/String;)Ljwt;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljwt;->sM(I)Ljwt;

    move-result-object v0

    iput-object v0, v1, Ljwy;->eJT:Ljwt;

    iget-object v0, p0, Lgmv;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->IW()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lgmv;->a(Ljava/lang/String;Ljwy;)V

    iget-object v0, p0, Lgmv;->mLocationHelper:Lglo;

    invoke-interface {v0}, Lglo;->aHh()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgmv;->mLocationHelper:Lglo;

    invoke-interface {v0}, Lglo;->aHg()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lgmv;->bZx:Landroid/location/Location;

    if-eqz v3, :cond_0

    iget-object v0, p0, Lgmv;->bZx:Landroid/location/Location;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcqg;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljwy;->zK(Ljava/lang/String;)Ljwy;

    :goto_0
    new-instance v0, Ljwt;

    invoke-direct {v0}, Ljwt;-><init>()V

    iget-object v3, p0, Lgmv;->cPB:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljwt;->zy(Ljava/lang/String;)Ljwt;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljwt;->sM(I)Ljwt;

    move-result-object v0

    iput-object v0, v1, Ljwy;->eJR:Ljwt;

    iget-object v0, p0, Lgmv;->cPC:Ljava/util/List;

    if-nez v0, :cond_3

    move v3, v2

    :goto_1
    if-lez v3, :cond_4

    new-array v0, v3, [Ljwt;

    iput-object v0, v1, Ljwy;->eJS:[Ljwt;

    :goto_2
    if-ge v2, v3, :cond_4

    iget-object v4, v1, Ljwy;->eJS:[Ljwt;

    new-instance v5, Ljwt;

    invoke-direct {v5}, Ljwt;-><init>()V

    iget-object v0, p0, Lgmv;->cPC:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljwt;->zy(Ljava/lang/String;)Ljwt;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljwt;->sM(I)Ljwt;

    move-result-object v0

    aput-object v0, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljwy;->zK(Ljava/lang/String;)Ljwy;

    goto :goto_0

    :cond_1
    const-string v0, "w "

    invoke-virtual {v1, v0}, Ljwy;->zK(Ljava/lang/String;)Ljwy;

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v2}, Ljwy;->jm(Z)Ljwy;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lgmv;->cPC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    :goto_3
    return-object v0

    :cond_5
    iget-object v0, p0, Lgmv;->cPD:Ljwy;

    iget-object v1, v0, Ljwy;->eJQ:[Ljws;

    array-length v3, v1

    move v0, v2

    :goto_4
    if-ge v0, v3, :cond_6

    aget-object v4, v1, v0

    iget-object v5, p0, Lgmv;->mAuthTokenHelper:Lgln;

    invoke-virtual {v4}, Ljws;->bvw()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Lgln;->iC(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lgmv;->cPD:Ljwy;

    new-instance v1, Ljwy;

    invoke-direct {v1}, Ljwy;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljwy;

    new-array v1, v2, [Ljws;

    iput-object v1, v0, Ljwy;->eJQ:[Ljws;

    iget-object v1, p0, Lgmv;->mSpeechSettings:Lgdo;

    invoke-interface {v1}, Lgdo;->IW()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lgmv;->a(Ljava/lang/String;Ljwy;)V

    goto :goto_3
.end method
