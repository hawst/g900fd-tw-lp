.class public final Lgio;
.super Ljul;
.source "PG"


# instance fields
.field private final bhA:Landroid/content/res/AssetManager;

.field private final cLA:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, v0, v0, v0}, Ljul;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 52
    invoke-static {p2}, Lgio;->mL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgio;->cLA:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lgio;->bhA:Landroid/content/res/AssetManager;

    .line 54
    return-void
.end method

.method public static S([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lgio;->eFt:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    invoke-virtual {v0, p0}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->S([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v0

    return-object v0
.end method

.method private mK(Ljava/lang/String;)[B
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lgio;->bhA:Landroid/content/res/AssetManager;

    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v2

    long-to-int v1, v2

    new-array v1, v1, [B

    .line 112
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v0

    .line 114
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public static mL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    const-string v0, "en-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string p0, "en-US"

    .line 127
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final aGb()V
    .locals 4

    .prologue
    .line 60
    const-string v0, "%s/config.pumpkin"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lgio;->cLA:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgio;->mK(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lgio;->eFs:[B

    .line 61
    return-void
.end method

.method public final aGc()[B
    .locals 4

    .prologue
    .line 97
    const-string v0, "%s/action.pumpkin"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lgio;->cLA:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgio;->mK(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final aGd()[B
    .locals 4

    .prologue
    .line 101
    const-string v0, "%s/action_disambig.pumpkin"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lgio;->cLA:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgio;->mK(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final aGe()[B
    .locals 4

    .prologue
    .line 105
    const-string v0, "%s/action_select_recipient.pumpkin"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lgio;->cLA:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgio;->mK(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final ed()V
    .locals 0

    .prologue
    .line 65
    invoke-static {}, Lgju;->aGA()V

    .line 66
    invoke-super {p0}, Ljul;->ed()V

    .line 67
    return-void
.end method
