.class public final Ltr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private CV:F

.field private synthetic CW:J

.field private synthetic CX:I

.field private sY:I


# direct methods
.method public constructor <init>(Lcom/android/launcher3/DeleteDropTarget;JI)V
    .locals 2

    .prologue
    .line 520
    iput-wide p2, p0, Ltr;->CW:J

    iput p4, p0, Ltr;->CX:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    const/4 v0, -0x1

    iput v0, p0, Ltr;->sY:I

    .line 522
    const/4 v0, 0x0

    iput v0, p0, Ltr;->CV:F

    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 6

    .prologue
    .line 526
    iget v0, p0, Ltr;->sY:I

    if-gez v0, :cond_1

    .line 527
    iget v0, p0, Ltr;->sY:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltr;->sY:I

    .line 533
    :cond_0
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Ltr;->CV:F

    add-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0

    .line 528
    :cond_1
    iget v0, p0, Ltr;->sY:I

    if-nez v0, :cond_0

    .line 529
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ltr;->CW:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    iget v2, p0, Ltr;->CX:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Ltr;->CV:F

    .line 531
    iget v0, p0, Ltr;->sY:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltr;->sY:I

    goto :goto_0
.end method
