.class public Lebk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aPk:Lcom/google/android/search/shared/service/ClientConfig;

.field private final acx:Ljava/lang/String;

.field private bTY:Lebi;

.field private final beJ:Ljava/lang/String;

.field private final blv:Lerp;

.field private final mAppContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/search/shared/service/ClientConfig;Lerp;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lebk;->mAppContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lebk;->beJ:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lebk;->acx:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lebk;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    .line 36
    iput-object p5, p0, Lebk;->blv:Lerp;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Landroid/widget/FrameLayout;Lerp;Leqp;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;Landroid/os/Bundle;)Lebi;
    .locals 13

    .prologue
    .line 98
    new-instance v0, Lebm;

    const/4 v4, 0x0

    new-instance v5, Leli;

    invoke-direct {v5}, Leli;-><init>()V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v9, p6

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-direct/range {v0 .. v12}, Lebm;-><init>(Landroid/widget/FrameLayout;Lerp;Leqp;Leax;Leld;Ldtc;ZLandroid/graphics/Rect;Landroid/os/Bundle;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final declared-synchronized amK()V
    .locals 2

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lebk;->bTY:Lebi;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lebk;->bTY:Lebi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lebi;->bW(Z)Z

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lebk;->bTY:Lebi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized y(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lebk;->bTY:Lebi;

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lebk;->z(Landroid/os/Bundle;)Lebi;

    move-result-object v0

    iput-object v0, p0, Lebk;->bTY:Lebi;

    .line 49
    iget-object v0, p0, Lebk;->bTY:Lebi;

    iget-object v1, p0, Lebk;->beJ:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lebi;->b(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_0
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized z(Landroid/os/Bundle;)Lebi;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 68
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lebk;->bTY:Lebi;

    .line 70
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lebk;->bTY:Lebi;

    .line 71
    if-nez v0, :cond_1

    .line 72
    new-instance v0, Lemc;

    iget-object v1, p0, Lebk;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lemc;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lemc;->setView(Landroid/view/View;)V

    new-instance v3, Lenh;

    invoke-direct {v3, v0}, Lenh;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lebk;->blv:Lerp;

    iget-object v4, p0, Lebk;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    iget-object v5, p0, Lebk;->acx:Ljava/lang/String;

    move-object v0, p0

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lebk;->a(Landroid/widget/FrameLayout;Lerp;Leqp;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;Landroid/os/Bundle;)Lebi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 76
    :cond_1
    monitor-exit p0

    return-object v0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
