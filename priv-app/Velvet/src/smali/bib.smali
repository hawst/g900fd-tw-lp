.class public final Lbib;
.super Landroid/support/v4/app/Fragment;

# interfaces
.implements Lan;
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field private aBO:Z

.field private aBP:I

.field private aBQ:Lbgm;

.field private final aBR:Landroid/os/Handler;

.field private final aBS:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lbib;->aBP:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbib;->aBR:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    return-void
.end method

.method private a(ILbgm;)V
    .locals 2

    const-string v0, "GmsSupportLifecycleFragment"

    const-string v1, "Unresolved error while connecting client. Stopping auto-manage."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbid;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v1

    invoke-virtual {v1, p1}, Lam;->destroyLoader(I)V

    iget-object v1, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, v0, Lbid;->aBW:Lbhl;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lbhl;->a(Lbgm;)V

    :cond_0
    invoke-direct {p0}, Lbib;->yF()V

    return-void
.end method

.method static synthetic a(Lbib;)V
    .locals 0

    invoke-direct {p0}, Lbib;->yF()V

    return-void
.end method

.method static synthetic a(Lbib;ILbgm;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbib;->a(ILbgm;)V

    return-void
.end method

.method public static b(Lq;)Lbib;
    .locals 4

    const-string v0, "Must be called from main thread of process"

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p0}, Lq;->C()Lv;

    move-result-object v1

    :try_start_0
    const-string v0, "GmsSupportLifecycleFragment"

    invoke-virtual {v1, v0}, Lv;->b(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbib;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->ar:Z

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Lbib;

    invoke-direct {v0}, Lbib;-><init>()V

    invoke-virtual {v1}, Lv;->E()Lae;

    move-result-object v2

    const-string v3, "GmsSupportLifecycleFragment"

    invoke-virtual {v2, v0, v3}, Lae;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lae;

    move-result-object v2

    invoke-virtual {v2}, Lae;->commit()I

    invoke-virtual {v1}, Lv;->executePendingTransactions()Z

    :cond_2
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fragment with tag GmsSupportLifecycleFragment is not a SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private dI(I)Lbic;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v0

    invoke-virtual {v0, p1}, Lam;->f(I)Lck;

    move-result-object v0

    check-cast v0, Lbic;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown loader in SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private yF()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lbib;->aBO:Z

    const/4 v0, -0x1

    iput v0, p0, Lbib;->aBP:I

    iput-object v6, p0, Lbib;->aBQ:Lbgm;

    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-direct {p0, v3}, Lbib;->dI(I)Lbic;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-boolean v5, v4, Lbic;->aBU:Z

    if-eqz v5, :cond_0

    iput-boolean v1, v4, Lbic;->aBU:Z

    iget-boolean v5, v4, Lck;->cq:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lck;->dC:Z

    if-nez v5, :cond_0

    iget-object v4, v4, Lbic;->aBT:Lbhi;

    invoke-interface {v4}, Lbhi;->connect()V

    :cond_0
    invoke-virtual {v2, v3, v6, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lck;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Lck;
    .locals 3

    new-instance v1, Lbic;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    iget-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbid;

    iget-object v0, v0, Lbid;->aBT:Lbhi;

    invoke-direct {v1, v2, v0}, Lbic;-><init>(Landroid/content/Context;Lbhi;)V

    return-object v1
.end method

.method public final a(II)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbib;->yF()V

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    invoke-static {v2}, Lbgt;->T(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    :cond_1
    iget v0, p0, Lbib;->aBP:I

    iget-object v1, p0, Lbib;->aBQ:Lbgm;

    invoke-direct {p0, v0, v1}, Lbib;->a(ILbgm;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILbhi;Lbhl;)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "GoogleApiClient instance cannot be null"

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already managing a GoogleApiClient with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbjr;->a(ZLjava/lang/Object;)V

    new-instance v0, Lbid;

    invoke-direct {v0, p2, p3, v1}, Lbid;-><init>(Lbhi;Lbhl;B)V

    iget-object v1, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lck;

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lck;)V
    .locals 2

    iget v0, p1, Lck;->cs:I

    iget v1, p0, Lbib;->aBP:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lbib;->yF()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Lck;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lbgm;

    invoke-virtual {p2}, Lbgm;->yk()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Lck;->cs:I

    iget v1, p0, Lbib;->aBP:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lbib;->yF()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Lck;->cs:I

    iget-boolean v1, p0, Lbib;->aBO:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lbib;->aBO:Z

    iput v0, p0, Lbib;->aBP:I

    iput-object p2, p0, Lbib;->aBQ:Lbgm;

    iget-object v1, p0, Lbib;->aBR:Landroid/os/Handler;

    new-instance v2, Lbie;

    invoke-direct {v2, p0, v0, p2}, Lbie;-><init>(Lbib;ILbgm;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final dH(I)Lbhi;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lbib;->dI(I)Lbic;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lbic;->aBT:Lbhi;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-direct {p0, v2}, Lbib;->dI(I)Lbic;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbid;

    iget-object v0, v0, Lbid;->aBT:Lbhi;

    iget-object v3, v3, Lbic;->aBT:Lbhi;

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lam;->b(ILandroid/os/Bundle;Lan;)Lck;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lck;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget v0, p0, Lbib;->aBP:I

    iget-object v1, p0, Lbib;->aBQ:Lbgm;

    invoke-direct {p0, v0, v1}, Lbib;->a(ILbgm;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbib;->aBO:Z

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbib;->aBP:I

    iget v0, p0, Lbib;->aBP:I

    if-ltz v0, :cond_0

    new-instance v1, Lbgm;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lbgm;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lbib;->aBQ:Lbgm;

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lbib;->aBO:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v0, p0, Lbib;->aBP:I

    if-ltz v0, :cond_0

    const-string v0, "failed_client_id"

    iget v1, p0, Lbib;->aBP:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, Lbib;->aBQ:Lbgm;

    invoke-virtual {v1}, Lbgm;->getErrorCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, Lbib;->aBQ:Lbgm;

    invoke-virtual {v1}, Lbgm;->yl()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 4

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    iget-boolean v0, p0, Lbib;->aBO:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lbib;->e()Lam;

    move-result-object v1

    iget-object v2, p0, Lbib;->aBS:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lck;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
