.class public final Ljbq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dZa:I

.field private dZb:I

.field private dZc:I

.field private dZd:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19698
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 19699
    iput v0, p0, Ljbq;->aez:I

    iput v0, p0, Ljbq;->dZa:I

    iput v0, p0, Ljbq;->dZb:I

    iput v0, p0, Ljbq;->dZc:I

    iput v0, p0, Ljbq;->dZd:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljbq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbq;->eCz:I

    .line 19700
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 19603
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljbq;->dZa:I

    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljbq;->dZb:I

    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljbq;->dZc:I

    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbq;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljbq;->dZd:I

    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 19716
    iget v0, p0, Ljbq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 19717
    const/4 v0, 0x1

    iget v1, p0, Ljbq;->dZa:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 19719
    :cond_0
    iget v0, p0, Ljbq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 19720
    const/4 v0, 0x2

    iget v1, p0, Ljbq;->dZb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 19722
    :cond_1
    iget v0, p0, Ljbq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 19723
    const/4 v0, 0x3

    iget v1, p0, Ljbq;->dZc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 19725
    :cond_2
    iget v0, p0, Ljbq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 19726
    const/4 v0, 0x4

    iget v1, p0, Ljbq;->dZd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 19728
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 19729
    return-void
.end method

.method public final bfi()I
    .locals 1

    .prologue
    .line 19625
    iget v0, p0, Ljbq;->dZa:I

    return v0
.end method

.method public final bfj()I
    .locals 1

    .prologue
    .line 19644
    iget v0, p0, Ljbq;->dZb:I

    return v0
.end method

.method public final bfk()I
    .locals 1

    .prologue
    .line 19663
    iget v0, p0, Ljbq;->dZc:I

    return v0
.end method

.method public final bfl()I
    .locals 1

    .prologue
    .line 19682
    iget v0, p0, Ljbq;->dZd:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 19733
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 19734
    iget v1, p0, Ljbq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 19735
    const/4 v1, 0x1

    iget v2, p0, Ljbq;->dZa:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 19738
    :cond_0
    iget v1, p0, Ljbq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 19739
    const/4 v1, 0x2

    iget v2, p0, Ljbq;->dZb:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 19742
    :cond_1
    iget v1, p0, Ljbq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 19743
    const/4 v1, 0x3

    iget v2, p0, Ljbq;->dZc:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 19746
    :cond_2
    iget v1, p0, Ljbq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 19747
    const/4 v1, 0x4

    iget v2, p0, Ljbq;->dZd:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 19750
    :cond_3
    return v0
.end method

.method public final ot(I)Ljbq;
    .locals 1

    .prologue
    .line 19628
    iput p1, p0, Ljbq;->dZa:I

    .line 19629
    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbq;->aez:I

    .line 19630
    return-object p0
.end method

.method public final ou(I)Ljbq;
    .locals 1

    .prologue
    .line 19647
    iput p1, p0, Ljbq;->dZb:I

    .line 19648
    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbq;->aez:I

    .line 19649
    return-object p0
.end method

.method public final ov(I)Ljbq;
    .locals 1

    .prologue
    .line 19666
    iput p1, p0, Ljbq;->dZc:I

    .line 19667
    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbq;->aez:I

    .line 19668
    return-object p0
.end method

.method public final ow(I)Ljbq;
    .locals 1

    .prologue
    .line 19685
    iput p1, p0, Ljbq;->dZd:I

    .line 19686
    iget v0, p0, Ljbq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbq;->aez:I

    .line 19687
    return-object p0
.end method
