.class public Lgmx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cHB:Z

.field private final cIK:Z

.field private final cPF:Z

.field private final cPG:Z

.field private final cPH:Z

.field private final cPI:I

.field private final cPJ:I

.field private final cPK:I

.field private final cPL:I

.field private final cPM:Landroid/net/Uri;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cPN:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mAudio:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(ZZZZZZIIIILandroid/net/Uri;[B[B)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean p1, p0, Lgmx;->cIK:Z

    .line 63
    iput-boolean p2, p0, Lgmx;->cPF:Z

    .line 64
    iput-boolean p3, p0, Lgmx;->cPG:Z

    .line 65
    iput-boolean p4, p0, Lgmx;->cPH:Z

    .line 66
    iput-boolean p5, p0, Lgmx;->cHB:Z

    .line 67
    iput p7, p0, Lgmx;->cPI:I

    .line 69
    iput p8, p0, Lgmx;->cPJ:I

    .line 70
    iput p9, p0, Lgmx;->cPK:I

    .line 71
    iput p10, p0, Lgmx;->cPL:I

    .line 72
    iput-object p11, p0, Lgmx;->cPM:Landroid/net/Uri;

    .line 73
    iput-object p12, p0, Lgmx;->mAudio:[B

    .line 74
    iput-object p13, p0, Lgmx;->cPN:[B

    .line 75
    return-void
.end method


# virtual methods
.method public final aHA()[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lgmx;->cPN:[B

    return-object v0
.end method

.method public final aHs()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lgmx;->cIK:Z

    return v0
.end method

.method public final aHt()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lgmx;->cPF:Z

    return v0
.end method

.method public final aHu()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lgmx;->cPG:Z

    return v0
.end method

.method public final aHv()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lgmx;->cPH:Z

    return v0
.end method

.method public final aHw()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lgmx;->cHB:Z

    return v0
.end method

.method public final aHx()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lgmx;->cPJ:I

    return v0
.end method

.method public final aHy()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lgmx;->cPL:I

    return v0
.end method

.method public final aHz()Landroid/net/Uri;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lgmx;->cPM:Landroid/net/Uri;

    return-object v0
.end method

.method public final asV()[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lgmx;->mAudio:[B

    return-object v0
.end method

.method public final getEncoding()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lgmx;->cPI:I

    return v0
.end method

.method public final getSamplingRate()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lgmx;->cPK:I

    return v0
.end method
