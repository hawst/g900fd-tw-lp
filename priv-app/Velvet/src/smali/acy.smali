.class public final Lacy;
.super Lacw;
.source "PG"


# instance fields
.field public RQ:Landroid/appwidget/AppWidgetProviderInfo;

.field public RR:Landroid/appwidget/AppWidgetHostView;

.field public RS:Landroid/os/Bundle;

.field private RT:Landroid/os/Parcelable;

.field private icon:I

.field private mimeType:Ljava/lang/String;

.field public minHeight:I

.field private minResizeHeight:I

.field private minResizeWidth:I

.field public minWidth:I

.field public previewImage:I


# direct methods
.method public constructor <init>(Lacy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Lacw;-><init>()V

    .line 59
    iput-object v0, p0, Lacy;->RS:Landroid/os/Bundle;

    .line 84
    iget v1, p1, Lacy;->minWidth:I

    iput v1, p0, Lacy;->minWidth:I

    .line 85
    iget v1, p1, Lacy;->minHeight:I

    iput v1, p0, Lacy;->minHeight:I

    .line 86
    iget v1, p1, Lacy;->minResizeWidth:I

    iput v1, p0, Lacy;->minResizeWidth:I

    .line 87
    iget v1, p1, Lacy;->minResizeHeight:I

    iput v1, p0, Lacy;->minResizeHeight:I

    .line 88
    iget v1, p1, Lacy;->previewImage:I

    iput v1, p0, Lacy;->previewImage:I

    .line 89
    iget v1, p1, Lacy;->icon:I

    iput v1, p0, Lacy;->icon:I

    .line 90
    iget-object v1, p1, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v1, p0, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    .line 91
    iget-object v1, p1, Lacy;->RR:Landroid/appwidget/AppWidgetHostView;

    iput-object v1, p0, Lacy;->RR:Landroid/appwidget/AppWidgetHostView;

    .line 92
    iget-object v1, p1, Lacy;->mimeType:Ljava/lang/String;

    iput-object v1, p0, Lacy;->mimeType:Ljava/lang/String;

    .line 93
    iget-object v1, p1, Lacy;->RT:Landroid/os/Parcelable;

    iput-object v1, p0, Lacy;->RT:Landroid/os/Parcelable;

    .line 94
    iget-object v1, p1, Lacy;->xr:Landroid/content/ComponentName;

    iput-object v1, p0, Lacy;->xr:Landroid/content/ComponentName;

    .line 95
    iget v1, p1, Lacy;->Jz:I

    iput v1, p0, Lacy;->Jz:I

    .line 96
    iget v1, p1, Lacy;->AY:I

    iput v1, p0, Lacy;->AY:I

    .line 97
    iget v1, p1, Lacy;->AZ:I

    iput v1, p0, Lacy;->AZ:I

    .line 98
    iget v1, p1, Lacy;->JB:I

    iput v1, p0, Lacy;->JB:I

    .line 99
    iget v1, p1, Lacy;->JC:I

    iput v1, p0, Lacy;->JC:I

    .line 100
    iget-object v1, p1, Lacy;->RS:Landroid/os/Bundle;

    if-nez v1, :cond_0

    :goto_0
    iput-object v0, p0, Lacy;->RS:Landroid/os/Bundle;

    .line 101
    return-void

    .line 100
    :cond_0
    iget-object v0, p1, Lacy;->RS:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lacw;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lacy;->RS:Landroid/os/Bundle;

    .line 67
    const/4 v0, 0x4

    iput v0, p0, Lacy;->Jz:I

    .line 68
    iput-object p1, p0, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    .line 69
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iput-object v0, p0, Lacy;->xr:Landroid/content/ComponentName;

    .line 70
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iput v0, p0, Lacy;->minWidth:I

    .line 71
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    iput v0, p0, Lacy;->minHeight:I

    .line 72
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    iput v0, p0, Lacy;->minResizeWidth:I

    .line 73
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    iput v0, p0, Lacy;->minResizeHeight:I

    .line 74
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iput v0, p0, Lacy;->previewImage:I

    .line 75
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    iput v0, p0, Lacy;->icon:I

    .line 76
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Widget: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lacy;->xr:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
