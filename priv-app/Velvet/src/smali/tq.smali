.class public final Ltq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic CN:Lcom/android/launcher3/DragLayer;

.field private synthetic CO:Landroid/animation/TimeInterpolator;

.field private synthetic CP:F

.field private synthetic CQ:F

.field private synthetic CR:F

.field private synthetic CS:F

.field private synthetic CT:F

.field private synthetic CU:F


# direct methods
.method public constructor <init>(Lcom/android/launcher3/DeleteDropTarget;Lcom/android/launcher3/DragLayer;Landroid/animation/TimeInterpolator;FFFFFF)V
    .locals 0

    .prologue
    .line 406
    iput-object p2, p0, Ltq;->CN:Lcom/android/launcher3/DragLayer;

    iput-object p3, p0, Ltq;->CO:Landroid/animation/TimeInterpolator;

    iput p4, p0, Ltq;->CP:F

    iput p5, p0, Ltq;->CQ:F

    iput p6, p0, Ltq;->CR:F

    iput p7, p0, Ltq;->CS:F

    iput p8, p0, Ltq;->CT:F

    iput p9, p0, Ltq;->CU:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 12

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x3f800000    # 1.0f

    .line 409
    iget-object v0, p0, Ltq;->CN:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fN()Landroid/view/View;

    move-result-object v0

    check-cast v0, Luj;

    .line 410
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 411
    iget-object v2, p0, Ltq;->CO:Landroid/animation/TimeInterpolator;

    invoke-interface {v2, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    .line 412
    invoke-virtual {v0}, Luj;->ga()F

    move-result v3

    .line 413
    invoke-virtual {v0}, Luj;->getScaleX()F

    move-result v4

    .line 415
    sub-float v5, v9, v4

    invoke-virtual {v0}, Luj;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v10

    .line 416
    sub-float v4, v9, v4

    invoke-virtual {v0}, Luj;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v4, v6

    div-float/2addr v4, v10

    .line 417
    sub-float v6, v9, v1

    sub-float v7, v9, v1

    mul-float/2addr v6, v7

    iget v7, p0, Ltq;->CP:F

    sub-float/2addr v7, v5

    mul-float/2addr v6, v7

    sub-float v7, v9, v1

    mul-float/2addr v7, v10

    mul-float/2addr v7, v1

    iget v8, p0, Ltq;->CQ:F

    sub-float/2addr v8, v5

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float v7, v1, v1

    iget v8, p0, Ltq;->CR:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    .line 419
    sub-float v7, v9, v1

    sub-float v8, v9, v1

    mul-float/2addr v7, v8

    iget v8, p0, Ltq;->CS:F

    sub-float v4, v8, v4

    mul-float/2addr v4, v7

    sub-float v7, v9, v1

    mul-float/2addr v7, v10

    mul-float/2addr v7, v1

    iget v8, p0, Ltq;->CT:F

    sub-float v5, v8, v5

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    mul-float/2addr v1, v1

    iget v5, p0, Ltq;->CU:F

    mul-float/2addr v1, v5

    add-float/2addr v1, v4

    .line 422
    invoke-virtual {v0, v6}, Luj;->setTranslationX(F)V

    .line 423
    invoke-virtual {v0, v1}, Luj;->setTranslationY(F)V

    .line 424
    sub-float v1, v9, v2

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Luj;->setScaleX(F)V

    .line 425
    sub-float v1, v9, v2

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Luj;->setScaleY(F)V

    .line 426
    sub-float v1, v9, v2

    mul-float/2addr v1, v11

    add-float/2addr v1, v11

    invoke-virtual {v0, v1}, Luj;->setAlpha(F)V

    .line 427
    return-void
.end method
