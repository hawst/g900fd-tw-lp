.class public Ldrb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final bGI:Landroid/widget/ProgressBar;

.field bGJ:Z

.field bGK:Z

.field private final bGL:Landroid/view/View;

.field private final bGM:Landroid/animation/ValueAnimator;

.field private final bGN:Landroid/animation/ValueAnimator;

.field private bGO:I

.field private bGP:I


# direct methods
.method public constructor <init>(Landroid/widget/ProgressBar;Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/widget/ProgressBar;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    .line 80
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Ldrb;->bGL:Landroid/view/View;

    .line 82
    iget-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 84
    new-instance v0, Ldrc;

    invoke-direct {v0, p0}, Ldrc;-><init>(Ldrb;)V

    invoke-virtual {p0, v0}, Ldrb;->a(Landroid/animation/Animator$AnimatorListener;)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    .line 96
    iget-object v0, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 97
    iget-object v0, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 98
    iget-object v0, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 101
    invoke-virtual {p0}, Ldrb;->aeU()Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    .line 102
    iget-object v0, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1d4c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 103
    iget-object v0, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 104
    iget-object v0, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 105
    return-void
.end method


# virtual methods
.method a(Landroid/animation/Animator$AnimatorListener;)Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 109
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 110
    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 111
    return-object v0
.end method

.method aeU()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 116
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    return-object v0
.end method

.method final aeV()V
    .locals 5

    .prologue
    .line 197
    iget-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v0

    .line 198
    iget v1, p0, Ldrb;->bGP:I

    rsub-int/lit8 v1, v1, 0x14

    add-int/2addr v1, v0

    .line 199
    iget-object v2, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v0, 0x1

    aput v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 200
    iget-object v0, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 201
    return-void
.end method

.method public final dN(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    iget-object v0, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldrb;->bGJ:Z

    .line 138
    :goto_0
    iput v1, p0, Ldrb;->bGP:I

    .line 139
    iput-boolean v1, p0, Ldrb;->bGK:Z

    .line 141
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0, v1}, Ldrb;->dO(Z)V

    goto :goto_0
.end method

.method dO(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x96

    const/4 v2, 0x4

    .line 209
    if-eqz p1, :cond_0

    .line 211
    iget-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 213
    iget-object v0, p0, Ldrb;->bGL:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 219
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Ldrb;->bGL:Landroid/view/View;

    invoke-static {v0}, Ldtp;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 216
    iget-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-static {v0, v2}, Ldtp;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final fv(I)V
    .locals 7

    .prologue
    const/16 v0, 0x64

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 152
    if-le p1, v0, :cond_1

    move p1, v0

    .line 159
    :cond_0
    :goto_0
    iget v3, p0, Ldrb;->bGO:I

    if-ne v3, p1, :cond_2

    .line 194
    :goto_1
    return-void

    .line 154
    :cond_1
    if-gez p1, :cond_0

    move p1, v1

    .line 155
    goto :goto_0

    .line 163
    :cond_2
    iget-object v3, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 164
    iput-boolean v1, p0, Ldrb;->bGK:Z

    .line 165
    iget-object v3, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 173
    :cond_3
    :goto_2
    iput p1, p0, Ldrb;->bGO:I

    .line 174
    if-ne p1, v0, :cond_7

    move v0, v2

    .line 177
    :goto_3
    if-eqz v0, :cond_4

    .line 178
    const/16 v3, 0x14

    iput v3, p0, Ldrb;->bGP:I

    .line 180
    :cond_4
    iget v3, p0, Ldrb;->bGP:I

    add-int/2addr v3, p1

    .line 183
    iget-object v4, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v4

    .line 184
    iget-object v5, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v5

    if-nez v5, :cond_8

    sub-int v5, v3, v4

    const/16 v6, 0xa

    if-lt v5, v6, :cond_8

    .line 186
    iget-object v5, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    const/4 v6, 0x2

    new-array v6, v6, [I

    aput v4, v6, v1

    aput v3, v6, v2

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 187
    iget-object v3, p0, Ldrb;->bGM:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 188
    if-nez v0, :cond_5

    move v1, v2

    :cond_5
    iput-boolean v1, p0, Ldrb;->bGK:Z

    goto :goto_1

    .line 166
    :cond_6
    iget-object v3, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 168
    iget-object v3, p0, Ldrb;->bGN:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 169
    iget-object v3, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v3

    iget v4, p0, Ldrb;->bGO:I

    sub-int/2addr v3, v4

    iput v3, p0, Ldrb;->bGP:I

    goto :goto_2

    :cond_7
    move v0, v1

    .line 174
    goto :goto_3

    .line 190
    :cond_8
    iget-object v0, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 192
    invoke-virtual {p0}, Ldrb;->aeV()V

    goto :goto_1
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 225
    iget-object v1, p0, Ldrb;->bGI:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 226
    return-void
.end method
