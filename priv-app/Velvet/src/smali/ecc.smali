.class public Lecc;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private bUF:Landroid/view/View$OnKeyListener;

.field private bUG:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lecc;->bUG:Landroid/graphics/Point;

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lecc;->setChildrenDrawingOrderEnabled(Z)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnKeyListener;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lecc;->bUF:Landroid/view/View$OnKeyListener;

    .line 48
    return-void
.end method

.method public aeD()I
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lecc;->getWidth()I

    move-result v0

    return v0
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lecc;->bUF:Landroid/view/View$OnKeyListener;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lecc;->bUF:Landroid/view/View$OnKeyListener;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-interface {v0, p0, v1, p1}, Landroid/view/View$OnKeyListener;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lecc;->bUG:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 38
    iget-object v0, p0, Lecc;->bUG:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 39
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
