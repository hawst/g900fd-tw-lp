.class public Ldcm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field protected ag:I

.field private bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

.field public bqv:Lcom/google/android/shared/search/Query;

.field private final bse:Ljava/lang/String;

.field private bsf:Ljava/lang/Object;

.field public bsg:Z

.field public bsh:Z

.field public bsi:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2785
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    .line 2799
    iput-object p1, p0, Ldcm;->bse:Ljava/lang/String;

    .line 2800
    const/4 v0, 0x0

    iput v0, p0, Ldcm;->ag:I

    .line 2801
    const/4 v0, 0x0

    iput-object v0, p0, Ldcm;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 2802
    return-void
.end method


# virtual methods
.method public final G(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    .line 2937
    iget-object v0, p0, Ldcm;->bsf:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    .line 2925
    iget-object v0, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Yq()V
    .locals 1

    .prologue
    .line 2847
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Ldcm;->setState(I)V

    .line 2848
    return-void
.end method

.method public final Yr()V
    .locals 1

    .prologue
    .line 2851
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Ldcm;->setState(I)V

    .line 2852
    return-void
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 2953
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ldcm;->bse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldcm;->ag:I

    invoke-static {v1}, Lcom/google/android/search/core/state/QueryState;->fD(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 2954
    const-string v0, "Current Query"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 2955
    const-string v0, "Newly committed"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldcm;->bsg:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2956
    const-string v0, "Newly loaded"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldcm;->bsh:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2957
    iget-object v0, p0, Ldcm;->bsf:Ljava/lang/Object;

    instance-of v0, v0, Leti;

    if-eqz v0, :cond_0

    .line 2958
    const-string v0, "Loaded data"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    iget-object v0, p0, Ldcm;->bsf:Ljava/lang/Object;

    check-cast v0, Leti;

    invoke-virtual {v1, v0}, Letn;->b(Leti;)V

    .line 2962
    :goto_0
    const-string v0, "Voice done"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldcm;->bsi:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2963
    return-void

    .line 2960
    :cond_0
    const-string v0, "Loaded data"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldcm;->bsf:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public final aA(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2836
    iput-object p1, p0, Ldcm;->bsf:Ljava/lang/Object;

    .line 2837
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ldcm;->setState(I)V

    .line 2838
    return-void
.end method

.method public final af(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 2829
    iput-object p1, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    .line 2830
    const/4 v0, 0x0

    iput-object v0, p0, Ldcm;->bsf:Ljava/lang/Object;

    .line 2831
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldcm;->setState(I)V

    .line 2832
    return-void
.end method

.method public final ag(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/errors/SearchError;
    .locals 1

    .prologue
    .line 2888
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2889
    iget-object v0, p0, Ldcm;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 2891
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ah(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2896
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldcm;->ag:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ai(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2900
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aj(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2904
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ak(Lcom/google/android/shared/search/Query;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2908
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Ldcm;->ag:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldcm;->ag:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final al(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2912
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final am(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2917
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final an(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2921
    invoke-virtual {p0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ao(Lcom/google/android/shared/search/Query;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0, p1}, Ldcm;->G(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcm;->bsf:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 1

    .prologue
    .line 2842
    iput-object p1, p0, Ldcm;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 2843
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcm;->setState(I)V

    .line 2844
    return-void
.end method

.method public final hasError()Z
    .locals 2

    .prologue
    .line 2884
    iget v0, p0, Ldcm;->ag:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 2822
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldcm;->setState(I)V

    .line 2823
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    .line 2824
    const/4 v0, 0x0

    iput-object v0, p0, Ldcm;->bsf:Ljava/lang/Object;

    .line 2825
    return-void
.end method

.method protected setState(I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2809
    iget v2, p0, Ldcm;->ag:I

    if-eq v2, v0, :cond_1

    if-ne p1, v0, :cond_1

    .line 2810
    iput-boolean v1, p0, Ldcm;->bsi:Z

    .line 2811
    iput-boolean v0, p0, Ldcm;->bsg:Z

    .line 2815
    :cond_0
    :goto_0
    iget v2, p0, Ldcm;->ag:I

    if-eq v2, v3, :cond_2

    if-ne p1, v3, :cond_2

    :goto_1
    iput-boolean v0, p0, Ldcm;->bsh:Z

    .line 2816
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Ldcm;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/errors/SearchError;

    :goto_2
    iput-object v0, p0, Ldcm;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 2817
    iput p1, p0, Ldcm;->ag:I

    .line 2818
    return-void

    .line 2812
    :cond_1
    if-eq p1, v0, :cond_0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    if-eq p1, v3, :cond_0

    .line 2813
    iput-boolean v1, p0, Ldcm;->bsg:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2815
    goto :goto_1

    .line 2816
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ldcm;->bse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldcm;->ag:I

    invoke-static {v1}, Lcom/google/android/search/core/state/QueryState;->fD(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NC="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ldcm;->bsg:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ldcm;->bsh:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " D="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcm;->bsf:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " VoiceDone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ldcm;->bsi:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
