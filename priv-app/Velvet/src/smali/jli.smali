.class public final Ljli;
.super Ljsl;
.source "PG"


# static fields
.field public static final erx:Ljsm;


# instance fields
.field private aez:I

.field public erA:[Ljlj;

.field private erB:Z

.field private erC:Z

.field public ery:Ljmu;

.field public erz:Ljmu;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10282
    const/16 v0, 0xb

    const-class v1, Ljli;

    const v2, 0xd829fba

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljli;->erx:Ljsm;

    .line 11087
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 11141
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 11142
    iput v1, p0, Ljli;->aez:I

    iput-object v2, p0, Ljli;->ery:Ljmu;

    iput-object v2, p0, Ljli;->erz:Ljmu;

    invoke-static {}, Ljlj;->boR()[Ljlj;

    move-result-object v0

    iput-object v0, p0, Ljli;->erA:[Ljlj;

    iput-boolean v1, p0, Ljli;->erB:Z

    iput-boolean v1, p0, Ljli;->erC:Z

    iput-object v2, p0, Ljli;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljli;->eCz:I

    .line 11143
    return-void
.end method

.method public static at([B)Ljli;
    .locals 1

    .prologue
    .line 11279
    new-instance v0, Ljli;

    invoke-direct {v0}, Ljli;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljli;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10275
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljli;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljli;->ery:Ljmu;

    if-nez v0, :cond_1

    new-instance v0, Ljmu;

    invoke-direct {v0}, Ljmu;-><init>()V

    iput-object v0, p0, Ljli;->ery:Ljmu;

    :cond_1
    iget-object v0, p0, Ljli;->ery:Ljmu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljli;->erz:Ljmu;

    if-nez v0, :cond_2

    new-instance v0, Ljmu;

    invoke-direct {v0}, Ljmu;-><init>()V

    iput-object v0, p0, Ljli;->erz:Ljmu;

    :cond_2
    iget-object v0, p0, Ljli;->erz:Ljmu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljli;->erA:[Ljlj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlj;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljli;->erA:[Ljlj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljlj;

    invoke-direct {v3}, Ljlj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljli;->erA:[Ljlj;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljlj;

    invoke-direct {v3}, Ljlj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljli;->erA:[Ljlj;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljli;->erB:Z

    iget v0, p0, Ljli;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljli;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljli;->erC:Z

    iget v0, p0, Ljli;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljli;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 11160
    iget-object v0, p0, Ljli;->ery:Ljmu;

    if-eqz v0, :cond_0

    .line 11161
    const/4 v0, 0x1

    iget-object v1, p0, Ljli;->ery:Ljmu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 11163
    :cond_0
    iget-object v0, p0, Ljli;->erz:Ljmu;

    if-eqz v0, :cond_1

    .line 11164
    const/4 v0, 0x2

    iget-object v1, p0, Ljli;->erz:Ljmu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 11166
    :cond_1
    iget-object v0, p0, Ljli;->erA:[Ljlj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljli;->erA:[Ljlj;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 11167
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljli;->erA:[Ljlj;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 11168
    iget-object v1, p0, Ljli;->erA:[Ljlj;

    aget-object v1, v1, v0

    .line 11169
    if-eqz v1, :cond_2

    .line 11170
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 11167
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11174
    :cond_3
    iget v0, p0, Ljli;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 11175
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljli;->erB:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11177
    :cond_4
    iget v0, p0, Ljli;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 11178
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljli;->erC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11180
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 11181
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 11185
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 11186
    iget-object v1, p0, Ljli;->ery:Ljmu;

    if-eqz v1, :cond_0

    .line 11187
    const/4 v1, 0x1

    iget-object v2, p0, Ljli;->ery:Ljmu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11190
    :cond_0
    iget-object v1, p0, Ljli;->erz:Ljmu;

    if-eqz v1, :cond_1

    .line 11191
    const/4 v1, 0x2

    iget-object v2, p0, Ljli;->erz:Ljmu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11194
    :cond_1
    iget-object v1, p0, Ljli;->erA:[Ljlj;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljli;->erA:[Ljlj;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 11195
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljli;->erA:[Ljlj;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 11196
    iget-object v2, p0, Ljli;->erA:[Ljlj;

    aget-object v2, v2, v0

    .line 11197
    if-eqz v2, :cond_2

    .line 11198
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 11195
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 11203
    :cond_4
    iget v1, p0, Ljli;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    .line 11204
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljli;->erB:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11207
    :cond_5
    iget v1, p0, Ljli;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 11208
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljli;->erC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11211
    :cond_6
    return v0
.end method
