.class public final Lfrc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lejx;
.implements Lfqx;


# instance fields
.field private final cAi:Lizj;

.field private final mCardContainer:Lfmt;


# direct methods
.method public constructor <init>(Lizj;Lfmt;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lfrc;->cAi:Lizj;

    .line 27
    iput-object p2, p0, Lfrc;->mCardContainer:Lfmt;

    .line 28
    return-void
.end method

.method private static f(Lejw;)Lizj;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lejw;->cco:Lijj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 70
    const v1, 0x7f110002

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    return-object v0
.end method


# virtual methods
.method public final c(Lejw;)V
    .locals 3

    .prologue
    .line 48
    invoke-static {p1}, Lfrc;->f(Lejw;)Lizj;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    iget-object v1, p0, Lfrc;->mCardContainer:Lfmt;

    iget-object v2, p0, Lfrc;->cAi:Lizj;

    invoke-interface {v1, v2, v0}, Lfmt;->a(Lizj;Lizj;)V

    .line 52
    :cond_0
    return-void
.end method

.method public final d(Lejw;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 58
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lfln;->b(Landroid/view/View;Z)V

    goto :goto_0

    .line 61
    :cond_0
    invoke-static {p1}, Lfrc;->f(Lejw;)Lizj;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_1

    .line 63
    iget-object v1, p0, Lfrc;->mCardContainer:Lfmt;

    invoke-interface {v1, v0}, Lfmt;->v(Lizj;)V

    .line 65
    :cond_1
    return-void
.end method

.method public final e(Lejw;)V
    .locals 3

    .prologue
    .line 32
    invoke-static {p1}, Lfrc;->f(Lejw;)Lizj;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p1, p0}, Lejw;->a(Lejx;)V

    .line 36
    iget-object v1, p0, Lfrc;->mCardContainer:Lfmt;

    invoke-interface {v1, v0}, Lfmt;->u(Lizj;)V

    .line 37
    iget-object v1, p0, Lfrc;->mCardContainer:Lfmt;

    invoke-interface {v1, p1, v0}, Lfmt;->a(Lejw;Lizj;)V

    .line 40
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 41
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lfln;->b(Landroid/view/View;Z)V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method
