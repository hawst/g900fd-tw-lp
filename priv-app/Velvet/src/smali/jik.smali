.class public final Ljik;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dSC:Ljcf;

.field private eno:Ljava/lang/String;

.field private enu:Lixx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46279
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 46280
    const/4 v0, 0x0

    iput v0, p0, Ljik;->aez:I

    iput-object v1, p0, Ljik;->dSC:Ljcf;

    const-string v0, ""

    iput-object v0, p0, Ljik;->eno:Ljava/lang/String;

    iput-object v1, p0, Ljik;->enu:Lixx;

    iput-object v1, p0, Ljik;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljik;->eCz:I

    .line 46281
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 46232
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljik;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljik;->dSC:Ljcf;

    if-nez v0, :cond_1

    new-instance v0, Ljcf;

    invoke-direct {v0}, Ljcf;-><init>()V

    iput-object v0, p0, Ljik;->dSC:Ljcf;

    :cond_1
    iget-object v0, p0, Ljik;->dSC:Ljcf;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljik;->eno:Ljava/lang/String;

    iget v0, p0, Ljik;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljik;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljik;->enu:Lixx;

    if-nez v0, :cond_2

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Ljik;->enu:Lixx;

    :cond_2
    iget-object v0, p0, Ljik;->enu:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x42 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 46296
    iget-object v0, p0, Ljik;->dSC:Ljcf;

    if-eqz v0, :cond_0

    .line 46297
    const/4 v0, 0x1

    iget-object v1, p0, Ljik;->dSC:Ljcf;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46299
    :cond_0
    iget v0, p0, Ljik;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 46300
    const/4 v0, 0x2

    iget-object v1, p0, Ljik;->eno:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46302
    :cond_1
    iget-object v0, p0, Ljik;->enu:Lixx;

    if-eqz v0, :cond_2

    .line 46303
    const/16 v0, 0x8

    iget-object v1, p0, Ljik;->enu:Lixx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46305
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 46306
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 46310
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 46311
    iget-object v1, p0, Ljik;->dSC:Ljcf;

    if-eqz v1, :cond_0

    .line 46312
    const/4 v1, 0x1

    iget-object v2, p0, Ljik;->dSC:Ljcf;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46315
    :cond_0
    iget v1, p0, Ljik;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 46316
    const/4 v1, 0x2

    iget-object v2, p0, Ljik;->eno:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46319
    :cond_1
    iget-object v1, p0, Ljik;->enu:Lixx;

    if-eqz v1, :cond_2

    .line 46320
    const/16 v1, 0x8

    iget-object v2, p0, Ljik;->enu:Lixx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46323
    :cond_2
    return v0
.end method
