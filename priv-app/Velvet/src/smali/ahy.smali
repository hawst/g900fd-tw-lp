.class final Lahy;
.super Landroid/content/pm/PackageInstaller$SessionCallback;
.source "PG"


# instance fields
.field private synthetic YH:Lahx;


# direct methods
.method constructor <init>(Lahx;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lahy;->YH:Lahx;

    invoke-direct {p0}, Landroid/content/pm/PackageInstaller$SessionCallback;-><init>()V

    return-void
.end method

.method private bS(I)V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lahy;->YH:Lahx;

    invoke-static {v0}, Lahx;->b(Lahx;)Landroid/content/pm/PackageInstaller;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->getSessionInfo(I)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_1

    .line 192
    iget-object v1, p0, Lahy;->YH:Lahx;

    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lahx;->a(Lahx;Landroid/content/pm/PackageInstaller$SessionInfo;Lahz;)V

    .line 193
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lahy;->YH:Lahx;

    invoke-static {v1}, Lahx;->c(Lahx;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_0
    iget-object v1, p0, Lahy;->YH:Lahx;

    invoke-static {v1}, Lahx;->a(Lahx;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lahy;->YH:Lahx;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lahx;->a(Lahx;Lahv;)V

    .line 199
    :cond_1
    return-void
.end method


# virtual methods
.method public final onActiveChanged(IZ)V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public final onBadgingChanged(I)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lahy;->bS(I)V

    .line 187
    return-void
.end method

.method public final onCreated(I)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lahy;->bS(I)V

    .line 156
    return-void
.end method

.method public final onFinished(IZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Lahy;->YH:Lahx;

    invoke-static {v0}, Lahx;->a(Lahx;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 161
    iget-object v0, p0, Lahy;->YH:Lahx;

    invoke-static {v0}, Lahx;->b(Lahx;)Landroid/content/pm/PackageInstaller;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->getSessionInfo(I)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 163
    iget-object v2, p0, Lahy;->YH:Lahx;

    invoke-static {v2}, Lahx;->c(Lahx;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 167
    iget-object v2, p0, Lahy;->YH:Lahx;

    new-instance v3, Lahv;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v0, v1}, Lahv;-><init>(Ljava/lang/String;II)V

    invoke-static {v2, v3}, Lahx;->a(Lahx;Lahv;)V

    .line 170
    :cond_0
    return-void

    .line 167
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final onProgressChanged(IF)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lahy;->YH:Lahx;

    invoke-static {v0}, Lahx;->b(Lahx;)Landroid/content/pm/PackageInstaller;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->getSessionInfo(I)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_0

    .line 176
    iget-object v1, p0, Lahy;->YH:Lahx;

    invoke-static {v1}, Lahx;->a(Lahx;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 177
    iget-object v0, p0, Lahy;->YH:Lahx;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lahx;->a(Lahx;Lahv;)V

    .line 179
    :cond_0
    return-void
.end method
