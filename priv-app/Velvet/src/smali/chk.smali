.class public Lchk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcox;
.implements Leti;
.implements Lgdm;


# instance fields
.field private aWS:Lijm;

.field private aWT:Ljava/util/Map;

.field private aWU:Lijm;

.field private volatile aWV:J

.field private aWW:[I

.field private final aWy:Ljava/util/List;

.field private final dK:Ljava/lang/Object;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lcke;)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lchk;->dK:Ljava/lang/Object;

    .line 65
    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v0

    iput-object v0, p0, Lchk;->aWS:Lijm;

    .line 72
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lchk;->aWT:Ljava/util/Map;

    .line 80
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lchk;->aWW:[I

    .line 84
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lchk;->aWV:J

    .line 85
    iput-object p1, p0, Lchk;->mSettings:Lcke;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lchk;->aWy:Ljava/util/List;

    .line 87
    iget-object v0, p0, Lchk;->aWy:Ljava/util/List;

    new-instance v1, Lchl;

    invoke-direct {v1, p0}, Lchl;-><init>(Lchk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lchk;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Oj()Ljjf;

    move-result-object v0

    iget-object v1, p0, Lchk;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->Ok()Ljjf;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lchk;->a(Ljjf;Ljjf;)V

    .line 104
    return-void
.end method

.method private ah(II)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1054
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1055
    invoke-virtual {p0, p1, v0}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 1056
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1060
    :goto_1
    return v0

    .line 1055
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1060
    goto :goto_1
.end method

.method private aj(II)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1713
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1714
    if-eqz v0, :cond_3

    .line 1715
    invoke-virtual {v0}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    .line 1716
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1717
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 1733
    :goto_0
    return-object v0

    .line 1720
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 1721
    :try_start_0
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1722
    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    .line 1723
    invoke-static {v0}, Lesp;->lr(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1725
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown string array encoding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1728
    :catch_0
    move-exception v0

    .line 1729
    const-string v1, "string array"

    invoke-virtual {p0, v1, p1, v0}, Lchk;->a(Ljava/lang/String;ILjava/lang/RuntimeException;)V

    .line 1733
    :cond_3
    invoke-static {p1}, Lchh;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private eO(I)[I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1755
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1756
    if-eqz v0, :cond_2

    .line 1757
    invoke-virtual {v0}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    .line 1758
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1759
    new-array v0, v1, [I

    .line 1773
    :cond_0
    :goto_0
    return-object v0

    .line 1761
    :cond_1
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1762
    array-length v0, v2

    new-array v0, v0, [I

    .line 1764
    :goto_1
    :try_start_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 1765
    aget-object v3, v2, v1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v0, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1764
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1768
    :catch_0
    move-exception v0

    .line 1769
    const-string v1, "int array"

    invoke-virtual {p0, v1, p1, v0}, Lchk;->a(Ljava/lang/String;ILjava/lang/RuntimeException;)V

    .line 1773
    :cond_2
    invoke-static {p1}, Lchh;->getIntArray(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method private eQ(I)[[I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1795
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1796
    if-eqz v0, :cond_3

    .line 1797
    invoke-virtual {v0}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    .line 1798
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1799
    filled-new-array {v2, v2}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    .line 1817
    :cond_0
    :goto_0
    return-object v0

    .line 1801
    :cond_1
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1802
    array-length v0, v4

    new-array v0, v0, [[I

    move v3, v2

    .line 1804
    :goto_1
    :try_start_0
    array-length v1, v4

    if-ge v3, v1, :cond_0

    .line 1805
    aget-object v1, v4, v3

    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1806
    array-length v1, v5

    new-array v1, v1, [I

    aput-object v1, v0, v3

    move v1, v2

    .line 1807
    :goto_2
    array-length v6, v5

    if-ge v1, v6, :cond_2

    .line 1808
    aget-object v6, v0, v3

    aget-object v7, v5, v1

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v6, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1807
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1804
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1812
    :catch_0
    move-exception v0

    .line 1813
    const-string v1, "two dimensional int array"

    invoke-virtual {p0, v1, p1, v0}, Lchk;->a(Ljava/lang/String;ILjava/lang/RuntimeException;)V

    .line 1817
    :cond_3
    invoke-static {p1}, Lchh;->eF(I)[[I

    move-result-object v0

    goto :goto_0
.end method

.method private eS(I)[J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1839
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1840
    if-eqz v0, :cond_2

    .line 1841
    invoke-virtual {v0}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    .line 1842
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1843
    new-array v0, v1, [J

    .line 1857
    :cond_0
    :goto_0
    return-object v0

    .line 1845
    :cond_1
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1846
    array-length v0, v2

    new-array v0, v0, [J

    .line 1848
    :goto_1
    :try_start_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 1849
    aget-object v3, v2, v1

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v0, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1848
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1852
    :catch_0
    move-exception v0

    .line 1853
    const-string v1, "long array"

    invoke-virtual {p0, v1, p1, v0}, Lchk;->a(Ljava/lang/String;ILjava/lang/RuntimeException;)V

    .line 1857
    :cond_2
    invoke-static {p1}, Lchh;->eG(I)[J

    move-result-object v0

    goto :goto_0
.end method

.method private eT(I)Z
    .locals 1

    .prologue
    .line 1861
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1862
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljjg;->TO()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lchh;->eE(I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final Ey()Z
    .locals 1

    .prologue
    .line 1479
    const/16 v0, 0xdd

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Ez()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public final FA()I
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FB()I
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FC()Z
    .locals 1

    .prologue
    .line 371
    const/16 v0, 0xfc

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FD()Z
    .locals 1

    .prologue
    .line 375
    const/16 v0, 0x10f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FE()Z
    .locals 1

    .prologue
    .line 379
    const/16 v0, 0x110

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FF()Ljava/util/Set;
    .locals 2

    .prologue
    .line 383
    const/16 v0, 0xe2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final FG()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 391
    const/16 v2, 0xc1

    invoke-virtual {p0, v2, v0}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v3

    .line 393
    if-eqz v3, :cond_1

    .line 394
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 395
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 401
    :goto_1
    return v0

    .line 394
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 401
    goto :goto_1
.end method

.method public final FH()Z
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FI()I
    .locals 1

    .prologue
    .line 426
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FJ()I
    .locals 1

    .prologue
    .line 433
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FK()I
    .locals 1

    .prologue
    .line 440
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FL()Z
    .locals 1

    .prologue
    .line 448
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FM()I
    .locals 1

    .prologue
    .line 456
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FN()I
    .locals 1

    .prologue
    .line 464
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FO()I
    .locals 1

    .prologue
    .line 472
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FP()I
    .locals 1

    .prologue
    .line 481
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FQ()Z
    .locals 1

    .prologue
    .line 489
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FR()I
    .locals 1

    .prologue
    .line 497
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final FS()Z
    .locals 1

    .prologue
    .line 501
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FT()Z
    .locals 1

    .prologue
    .line 508
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FU()Z
    .locals 1

    .prologue
    .line 515
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FV()Z
    .locals 1

    .prologue
    .line 522
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FW()Z
    .locals 1

    .prologue
    .line 530
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FX()Z
    .locals 1

    .prologue
    .line 538
    const/16 v0, 0xf8

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FY()Z
    .locals 1

    .prologue
    .line 545
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final FZ()Z
    .locals 1

    .prologue
    .line 552
    const/16 v0, 0x1b

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Fr()J
    .locals 2

    .prologue
    .line 224
    iget-wide v0, p0, Lchk;->aWV:J

    return-wide v0
.end method

.method public final Fs()Ljava/util/Map;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lchk;->aWS:Lijm;

    return-object v0
.end method

.method public final Ft()[I
    .locals 2

    .prologue
    .line 236
    iget-object v1, p0, Lchk;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 237
    :try_start_0
    iget-object v0, p0, Lchk;->aWW:[I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Fu()[J
    .locals 1

    .prologue
    .line 259
    const/16 v0, 0x120

    invoke-virtual {p0, v0}, Lchk;->eR(I)[J

    move-result-object v0

    return-object v0
.end method

.method public final Fv()[J
    .locals 1

    .prologue
    .line 266
    const/16 v0, 0x122

    invoke-virtual {p0, v0}, Lchk;->eR(I)[J

    move-result-object v0

    return-object v0
.end method

.method public final Fw()[I
    .locals 1

    .prologue
    .line 273
    const/16 v0, 0x121

    invoke-virtual {p0, v0}, Lchk;->eN(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final Fx()[[I
    .locals 1

    .prologue
    .line 280
    const/16 v0, 0x123

    invoke-virtual {p0, v0}, Lchk;->eP(I)[[I

    move-result-object v0

    return-object v0
.end method

.method public final Fy()J
    .locals 2

    .prologue
    .line 287
    const/16 v0, 0x118

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final Fz()I
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final GA()Z
    .locals 1

    .prologue
    .line 676
    const/16 v0, 0x3a

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GB()Z
    .locals 1

    .prologue
    .line 680
    const/16 v0, 0x39

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GC()Z
    .locals 1

    .prologue
    .line 684
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GD()Z
    .locals 1

    .prologue
    .line 688
    const/16 v0, 0x3b

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GE()Z
    .locals 1

    .prologue
    .line 692
    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GF()Z
    .locals 1

    .prologue
    .line 696
    const/16 v0, 0x3c

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GG()Z
    .locals 1

    .prologue
    .line 700
    const/16 v0, 0x33

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GH()Z
    .locals 1

    .prologue
    .line 704
    const/16 v0, 0x3d

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GI()Z
    .locals 1

    .prologue
    .line 708
    const/16 v0, 0x34

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GJ()Z
    .locals 1

    .prologue
    .line 712
    const/16 v0, 0x3e

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GK()Z
    .locals 1

    .prologue
    .line 716
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GL()Lcom/google/android/search/shared/actions/RemindersConfigFlags;
    .locals 4

    .prologue
    .line 720
    new-instance v0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    const/16 v1, 0x54

    invoke-direct {p0, v1}, Lchk;->eT(I)Z

    move-result v1

    const/16 v2, 0x56

    invoke-direct {p0, v2}, Lchk;->eT(I)Z

    move-result v2

    const/16 v3, 0x57

    invoke-direct {p0, v3}, Lchk;->eT(I)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;-><init>(ZZZ)V

    return-object v0
.end method

.method public final GM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 727
    const/16 v0, 0x41

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final GN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 731
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final GO()Z
    .locals 1

    .prologue
    .line 735
    const/16 v0, 0x43

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GP()Z
    .locals 1

    .prologue
    .line 739
    const/16 v0, 0x44

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GQ()Z
    .locals 1

    .prologue
    .line 743
    const/16 v0, 0x45

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GR()Z
    .locals 1

    .prologue
    .line 747
    const/16 v0, 0x47

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GS()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 766
    const/16 v0, 0xfa

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final GT()Z
    .locals 1

    .prologue
    .line 770
    const/16 v0, 0x48

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GU()Z
    .locals 1

    .prologue
    .line 774
    const/16 v0, 0x49

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GV()Z
    .locals 1

    .prologue
    .line 778
    const/16 v0, 0x4b

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final GW()J
    .locals 2

    .prologue
    .line 782
    const/16 v0, 0x4a

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final GX()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 786
    const/16 v0, 0x4d

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final GY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 794
    const/16 v0, 0x4e

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final GZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 798
    const/16 v0, 0xcb

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ga()Z
    .locals 1

    .prologue
    .line 559
    const/16 v0, 0xcc

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 566
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Gc()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 571
    const/16 v0, 0x1c

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Gd()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 578
    const/16 v0, 0x1d

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ge()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 585
    const/16 v0, 0x1e

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Gf()Z
    .locals 1

    .prologue
    .line 590
    const/16 v0, 0x23

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gg()Z
    .locals 1

    .prologue
    .line 594
    const/16 v0, 0x24

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gh()Z
    .locals 1

    .prologue
    .line 598
    const/16 v0, 0x25

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gi()Z
    .locals 1

    .prologue
    .line 602
    const/16 v0, 0x26

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gj()Z
    .locals 1

    .prologue
    .line 606
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 612
    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Gl()I
    .locals 1

    .prologue
    .line 616
    const/16 v0, 0x27

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gm()I
    .locals 1

    .prologue
    .line 620
    const/16 v0, 0x28

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gn()I
    .locals 1

    .prologue
    .line 624
    const/16 v0, 0x29

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Go()I
    .locals 1

    .prologue
    .line 628
    const/16 v0, 0x2a

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gp()I
    .locals 1

    .prologue
    .line 632
    const/16 v0, 0x2b

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gq()I
    .locals 1

    .prologue
    .line 636
    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gr()I
    .locals 1

    .prologue
    .line 640
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gs()I
    .locals 1

    .prologue
    .line 644
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gt()I
    .locals 1

    .prologue
    .line 648
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Gu()Z
    .locals 1

    .prologue
    .line 652
    const/16 v0, 0x36

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gv()Z
    .locals 1

    .prologue
    .line 656
    const/16 v0, 0x3f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gw()Z
    .locals 1

    .prologue
    .line 660
    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gx()Z
    .locals 1

    .prologue
    .line 664
    const/16 v0, 0x35

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gy()Z
    .locals 1

    .prologue
    .line 668
    const/16 v0, 0x38

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Gz()Z
    .locals 1

    .prologue
    .line 672
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HA()Z
    .locals 1

    .prologue
    .line 912
    const/16 v0, 0x63

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HB()Z
    .locals 1

    .prologue
    .line 916
    const/16 v0, 0x67

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HC()Z
    .locals 1

    .prologue
    .line 920
    const/16 v0, 0x68

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HD()I
    .locals 1

    .prologue
    .line 924
    const/16 v0, 0x69

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final HE()Z
    .locals 1

    .prologue
    .line 928
    const/16 v0, 0x6a

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HF()Z
    .locals 1

    .prologue
    .line 932
    const/16 v0, 0x6b

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HG()I
    .locals 1

    .prologue
    .line 936
    const/16 v0, 0x6c

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final HH()Z
    .locals 1

    .prologue
    .line 943
    const/16 v0, 0x6d

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HI()Z
    .locals 1

    .prologue
    .line 947
    const/16 v0, 0x6f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HJ()Z
    .locals 1

    .prologue
    .line 951
    const/16 v0, 0x70

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HK()[F
    .locals 1

    .prologue
    .line 955
    const/16 v0, 0xce

    invoke-virtual {p0, v0}, Lchk;->eI(I)[F

    move-result-object v0

    return-object v0
.end method

.method public final HL()[F
    .locals 1

    .prologue
    .line 959
    const/16 v0, 0xcf

    invoke-virtual {p0, v0}, Lchk;->eI(I)[F

    move-result-object v0

    return-object v0
.end method

.method public final HM()[F
    .locals 1

    .prologue
    .line 963
    const/16 v0, 0xd0

    invoke-virtual {p0, v0}, Lchk;->eI(I)[F

    move-result-object v0

    return-object v0
.end method

.method public final HN()F
    .locals 2

    .prologue
    .line 983
    const/16 v0, 0xcd

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    .line 984
    if-ltz v0, :cond_0

    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    .line 985
    :cond_0
    const/4 v0, 0x0

    .line 987
    :goto_0
    return v0

    :cond_1
    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public final HO()Z
    .locals 1

    .prologue
    .line 992
    const/16 v0, 0xc7

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HP()Z
    .locals 1

    .prologue
    .line 996
    const/16 v0, 0xc9

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HQ()Z
    .locals 1

    .prologue
    .line 1000
    const/16 v0, 0xca

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HR()Z
    .locals 1

    .prologue
    .line 1004
    const/16 v0, 0xd2

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HS()I
    .locals 1

    .prologue
    .line 1008
    const/16 v0, 0xd3

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final HT()Lijm;
    .locals 3

    .prologue
    .line 1012
    iget-object v0, p0, Lchk;->aWU:Lijm;

    if-nez v0, :cond_0

    .line 1013
    const/16 v0, 0x26

    const/16 v1, 0x3d

    const/16 v2, 0x74

    invoke-virtual {p0, v2}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lesp;->a(CCLjava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    iput-object v0, p0, Lchk;->aWU:Lijm;

    .line 1016
    :cond_0
    iget-object v0, p0, Lchk;->aWU:Lijm;

    return-object v0
.end method

.method public final HU()Z
    .locals 1

    .prologue
    .line 1064
    const/16 v0, 0xc2

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HV()Z
    .locals 1

    .prologue
    .line 1068
    const/16 v0, 0x73

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1072
    const/16 v0, 0x75

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final HX()Z
    .locals 1

    .prologue
    .line 1076
    const/16 v0, 0x76

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final HY()Z
    .locals 1

    .prologue
    .line 1081
    const/16 v0, 0x77

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x12d

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final HZ()Z
    .locals 1

    .prologue
    .line 1085
    invoke-virtual {p0}, Lchk;->HY()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Ha()Z
    .locals 1

    .prologue
    .line 803
    const/16 v0, 0x4f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcgg;->aVE:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Hb()I
    .locals 1

    .prologue
    .line 808
    const/16 v0, 0x50

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Hc()I
    .locals 1

    .prologue
    .line 812
    const/16 v0, 0x51

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Hd()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 816
    const/16 v0, 0x52

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final He()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 820
    const/16 v0, 0x53

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Hf()Z
    .locals 1

    .prologue
    .line 824
    const/16 v0, 0x58

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hg()Z
    .locals 1

    .prologue
    .line 828
    const/16 v0, 0x59

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 832
    const/16 v0, 0x5a

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Hi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 836
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Hj()Z
    .locals 1

    .prologue
    .line 840
    const/16 v0, 0x5b

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 844
    const/16 v0, 0x5c

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Hl()Z
    .locals 1

    .prologue
    .line 848
    const/16 v0, 0x5d

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hm()I
    .locals 1

    .prologue
    .line 852
    const/16 v0, 0x5e

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Hn()I
    .locals 1

    .prologue
    .line 856
    const/16 v0, 0x5f

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ho()Z
    .locals 1

    .prologue
    .line 860
    const/16 v0, 0x60

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hp()Z
    .locals 1

    .prologue
    .line 864
    const/16 v0, 0x61

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hq()Z
    .locals 1

    .prologue
    .line 868
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hr()Z
    .locals 1

    .prologue
    .line 872
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hs()I
    .locals 1

    .prologue
    .line 876
    const/16 v0, 0x66

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ht()Z
    .locals 1

    .prologue
    .line 880
    const/16 v0, 0x126

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Hu()[I
    .locals 1

    .prologue
    .line 884
    const/16 v0, 0x127

    invoke-virtual {p0, v0}, Lchk;->eN(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final Hv()[I
    .locals 1

    .prologue
    .line 888
    const/16 v0, 0x128

    invoke-virtual {p0, v0}, Lchk;->eN(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final Hw()[I
    .locals 1

    .prologue
    .line 892
    const/16 v0, 0x129

    invoke-virtual {p0, v0}, Lchk;->eN(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final Hx()[I
    .locals 1

    .prologue
    .line 896
    const/16 v0, 0x12a

    invoke-virtual {p0, v0}, Lchk;->eN(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final Hy()[I
    .locals 1

    .prologue
    .line 900
    const/16 v0, 0x12b

    invoke-virtual {p0, v0}, Lchk;->eN(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final Hz()I
    .locals 1

    .prologue
    .line 908
    const/16 v0, 0x62

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IA()Z
    .locals 1

    .prologue
    .line 1218
    const/16 v0, 0xba

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final IB()I
    .locals 1

    .prologue
    .line 1222
    const/16 v0, 0xbb

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IC()I
    .locals 1

    .prologue
    .line 1226
    const/16 v0, 0xbc

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final ID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1230
    const/16 v0, 0x79

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IE()I
    .locals 1

    .prologue
    .line 1234
    const/16 v0, 0x7a

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IF()I
    .locals 1

    .prologue
    .line 1238
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1242
    const/16 v0, 0xab

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1246
    const/16 v0, 0xac

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final II()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1250
    const/16 v0, 0xad

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1254
    const/16 v0, 0xae

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1258
    const/16 v0, 0xaf

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1262
    const/16 v0, 0xb5

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IM()I
    .locals 1

    .prologue
    .line 1270
    const/16 v0, 0xaa

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IN()I
    .locals 1

    .prologue
    .line 1274
    const/16 v0, 0xc6

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IO()I
    .locals 1

    .prologue
    .line 1282
    const/16 v0, 0xb4

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IP()I
    .locals 1

    .prologue
    .line 1290
    const/16 v0, 0xb3

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IQ()I
    .locals 1

    .prologue
    .line 1301
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IR()I
    .locals 1

    .prologue
    .line 1305
    const/16 v0, 0xb6

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IS()I
    .locals 1

    .prologue
    .line 1309
    const/16 v0, 0xb7

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final IT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1313
    const/16 v0, 0xb2

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1317
    const/16 v0, 0xb1

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1326
    const/16 v0, 0xb8

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1330
    const/16 v0, 0x9b

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1334
    const/16 v0, 0x9c

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1338
    const/16 v0, 0x9d

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final IZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1342
    const/16 v0, 0x9e

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ia()Z
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lchk;->HY()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12e

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Ib()Z
    .locals 1

    .prologue
    .line 1093
    invoke-virtual {p0}, Lchk;->HY()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x130

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Ic()Z
    .locals 1

    .prologue
    .line 1097
    invoke-virtual {p0}, Lchk;->HY()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12c

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Id()Z
    .locals 1

    .prologue
    .line 1101
    const/16 v0, 0x109

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Ie()Z
    .locals 1

    .prologue
    .line 1105
    const/16 v0, 0xf1

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final If()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109
    const/16 v0, 0xf2

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ig()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1113
    const/16 v0, 0x8f

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ih()J
    .locals 4

    .prologue
    .line 1118
    const/16 v0, 0x90

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Ii()J
    .locals 4

    .prologue
    .line 1122
    const/16 v0, 0x93

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Ij()J
    .locals 4

    .prologue
    .line 1126
    const/16 v0, 0x91

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Ik()I
    .locals 1

    .prologue
    .line 1132
    const/16 v0, 0x92

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Il()J
    .locals 4

    .prologue
    .line 1136
    const/16 v0, 0x97

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Im()I
    .locals 1

    .prologue
    .line 1141
    const/16 v0, 0x94

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final In()I
    .locals 1

    .prologue
    .line 1145
    const/16 v0, 0x95

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Io()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1152
    const/16 v0, 0x96

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ip()I
    .locals 1

    .prologue
    .line 1157
    const/16 v0, 0xa0

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Iq()I
    .locals 1

    .prologue
    .line 1165
    const/16 v0, 0xa1

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ir()I
    .locals 1

    .prologue
    .line 1178
    const/16 v0, 0xa9

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Is()I
    .locals 1

    .prologue
    .line 1186
    const/16 v0, 0xa8

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final It()I
    .locals 1

    .prologue
    .line 1190
    const/16 v0, 0xa3

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Iu()I
    .locals 1

    .prologue
    .line 1194
    const/16 v0, 0xa4

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Iv()I
    .locals 1

    .prologue
    .line 1198
    const/16 v0, 0xa5

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Iw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1202
    const/16 v0, 0xa6

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1206
    const/16 v0, 0xa7

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Iy()Z
    .locals 1

    .prologue
    .line 1210
    const/16 v0, 0x113

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Iz()Z
    .locals 1

    .prologue
    .line 1214
    const/16 v0, 0x98

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JA()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1463
    const/16 v0, 0x86

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JB()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1467
    const/16 v0, 0x87

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JC()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1471
    const/16 v0, 0x88

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JD()I
    .locals 1

    .prologue
    .line 1475
    const/16 v0, 0x89

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final JE()Z
    .locals 1

    .prologue
    .line 1483
    const/16 v0, 0xe0

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JF()Z
    .locals 1

    .prologue
    .line 1487
    const/16 v0, 0xdf

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JG()Z
    .locals 1

    .prologue
    .line 1491
    const/16 v0, 0xde

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JH()Z
    .locals 1

    .prologue
    .line 1495
    const/16 v0, 0xe1

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JI()Z
    .locals 1

    .prologue
    .line 1499
    const/16 v0, 0xe4

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JJ()Z
    .locals 1

    .prologue
    .line 1507
    const/16 v0, 0x8b

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JK()Z
    .locals 1

    .prologue
    .line 1511
    const/16 v0, 0x8c

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JL()I
    .locals 1

    .prologue
    .line 1515
    const/16 v0, 0x8d

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final JM()I
    .locals 1

    .prologue
    .line 1520
    const/16 v0, 0x8e

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final JN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1525
    const/16 v0, 0xe3

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JO()Z
    .locals 1

    .prologue
    .line 1533
    const/16 v0, 0xe5

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1538
    const/16 v0, 0xe6

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JQ()Z
    .locals 1

    .prologue
    .line 1543
    const/16 v0, 0xe8

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JR()Z
    .locals 1

    .prologue
    .line 1548
    const/16 v0, 0x102

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JS()Z
    .locals 1

    .prologue
    .line 1553
    const/16 v0, 0x106

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JT()I
    .locals 1

    .prologue
    .line 1560
    const/16 v0, 0x107

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final JU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1565
    const/16 v0, 0x108

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JV()I
    .locals 1

    .prologue
    .line 1569
    const/16 v0, 0xf9

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final JW()Z
    .locals 1

    .prologue
    .line 1573
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JX()Z
    .locals 1

    .prologue
    .line 1577
    const/16 v0, 0xf7

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JY()Z
    .locals 1

    .prologue
    .line 1581
    const/16 v0, 0xe9

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final JZ()Z
    .locals 1

    .prologue
    .line 1585
    const/16 v0, 0xf6

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Ja()I
    .locals 1

    .prologue
    .line 1346
    const/16 v0, 0x9f

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1350
    const/16 v0, 0xb9

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Jc()I
    .locals 1

    .prologue
    .line 1354
    const/16 v0, 0xd8

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jd()I
    .locals 1

    .prologue
    .line 1358
    const/16 v0, 0xc3

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Je()J
    .locals 4

    .prologue
    .line 1362
    const/16 v0, 0xc4

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Jf()J
    .locals 4

    .prologue
    .line 1367
    const/16 v0, 0xc5

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Jg()Z
    .locals 1

    .prologue
    .line 1371
    const/16 v0, 0xd1

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Jh()I
    .locals 1

    .prologue
    .line 1375
    const/16 v0, 0xfb

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ji()I
    .locals 1

    .prologue
    .line 1379
    const/16 v0, 0xd4

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jj()I
    .locals 1

    .prologue
    .line 1383
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jk()I
    .locals 1

    .prologue
    .line 1387
    const/16 v0, 0xd6

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jl()Z
    .locals 1

    .prologue
    .line 1391
    const/16 v0, 0x7c

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Jm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1395
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Jn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1399
    const/16 v0, 0x7e

    invoke-virtual {p0, v0}, Lchk;->eM(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Jo()I
    .locals 1

    .prologue
    .line 1414
    const/16 v0, 0x80

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jp()J
    .locals 4

    .prologue
    .line 1418
    const/16 v0, 0x81

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Jq()I
    .locals 1

    .prologue
    .line 1422
    const/16 v0, 0x82

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jr()Z
    .locals 1

    .prologue
    .line 1426
    const/16 v0, 0x83

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Js()I
    .locals 1

    .prologue
    .line 1430
    const/16 v0, 0x84

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jt()I
    .locals 1

    .prologue
    .line 1434
    const/16 v0, 0x85

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ju()I
    .locals 1

    .prologue
    .line 1438
    const/16 v0, 0xd9

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jv()I
    .locals 1

    .prologue
    .line 1442
    const/16 v0, 0xda

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jw()I
    .locals 1

    .prologue
    .line 1446
    const/16 v0, 0xdb

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jx()I
    .locals 1

    .prologue
    .line 1450
    const/16 v0, 0xdc

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jy()I
    .locals 1

    .prologue
    .line 1455
    const/16 v0, 0xfe

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Jz()I
    .locals 1

    .prologue
    .line 1459
    const/16 v0, 0xff

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ka()Z
    .locals 1

    .prologue
    .line 1589
    const/16 v0, 0xf3

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kb()J
    .locals 4

    .prologue
    .line 1593
    const/16 v0, 0xf4

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Kc()Z
    .locals 1

    .prologue
    .line 1601
    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kd()Z
    .locals 1

    .prologue
    .line 1605
    const/16 v0, 0x14e

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Ke()Z
    .locals 1

    .prologue
    .line 1609
    const/16 v0, 0x14f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kf()Z
    .locals 1

    .prologue
    .line 1619
    const/16 v0, 0x105

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kg()I
    .locals 1

    .prologue
    .line 1626
    const/16 v0, 0x112

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Kh()I
    .locals 1

    .prologue
    .line 1633
    const/16 v0, 0x111

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Ki()Z
    .locals 1

    .prologue
    .line 1637
    const/16 v0, 0x103

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kj()Z
    .locals 1

    .prologue
    .line 1641
    const/16 v0, 0x11d

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kk()Z
    .locals 1

    .prologue
    .line 1645
    const/16 v0, 0x11f

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kl()Z
    .locals 1

    .prologue
    .line 1649
    const/16 v0, 0x11a

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Km()Z
    .locals 1

    .prologue
    .line 1653
    const/16 v0, 0x125

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kn()Z
    .locals 1

    .prologue
    .line 1657
    const/16 v0, 0x131

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Ko()I
    .locals 1

    .prologue
    .line 1661
    const/16 v0, 0x13b

    invoke-virtual {p0, v0}, Lchk;->eL(I)I

    move-result v0

    return v0
.end method

.method public final Kp()Z
    .locals 1

    .prologue
    .line 1670
    const/16 v0, 0x146

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    return v0
.end method

.method public final Kq()Z
    .locals 2

    .prologue
    .line 1676
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/16 v0, 0x161

    invoke-direct {p0, v0}, Lchk;->eT(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Kr()I
    .locals 2

    .prologue
    .line 1935
    iget-object v1, p0, Lchk;->aWy:Ljava/util/List;

    monitor-enter v1

    .line 1936
    :try_start_0
    iget-object v0, p0, Lchk;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 1937
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lchm;)V
    .locals 2

    .prologue
    .line 140
    iget-object v1, p0, Lchk;->aWy:Ljava/util/List;

    monitor-enter v1

    .line 141
    :try_start_0
    iget-object v0, p0, Lchk;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Letj;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1891
    iget-object v0, p0, Lchk;->aWS:Lijm;

    invoke-virtual {v0}, Lijm;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1892
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    .line 1893
    const-string v0, "Experiment flags"

    invoke-virtual {v2, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1895
    iget-object v0, p0, Lchk;->aWS:Lijm;

    invoke-virtual {v0}, Lijm;->aWL()Lijp;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1896
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1897
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1898
    iget-object v1, p0, Lchk;->aWS:Lijm;

    invoke-virtual {v1, v0}, Lijm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljjg;

    .line 1899
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 1900
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1901
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lchh;->eH(I)I

    move-result v0

    .line 1902
    if-nez v0, :cond_0

    .line 1903
    invoke-virtual {v2, v4}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v1}, Ljjg;->TO()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    goto :goto_0

    .line 1904
    :cond_0
    const/4 v5, 0x1

    if-ne v0, v5, :cond_1

    .line 1905
    invoke-virtual {v2, v4}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v1}, Ljjg;->getIntValue()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Letn;->be(J)V

    goto :goto_0

    .line 1907
    :cond_1
    invoke-virtual {v2, v4}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 1912
    :cond_2
    iget-object v1, p0, Lchk;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 1913
    :try_start_0
    iget-object v0, p0, Lchk;->aWW:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lchk;->aWW:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1914
    const-string v0, "Experiment Ids"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const-string v2, ","

    iget-object v3, p0, Lchk;->aWW:[I

    invoke-static {v2, v3}, Lius;->e(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1917
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected a(Ljava/lang/String;ILjava/lang/RuntimeException;)V
    .locals 5

    .prologue
    .line 1886
    const-string v0, "GsaConfigFlags"

    const-string v1, "Failed to decode %s for flag: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, p3, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1887
    return-void
.end method

.method public final a(Ljje;Z)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p1, Ljje;->eoB:Ljjf;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p1, Ljje;->eoB:Ljjf;

    .line 119
    iget-object v1, p0, Lchk;->mSettings:Lcke;

    invoke-interface {v1, v0}, Lcke;->a(Ljjf;)V

    .line 120
    iget-object v1, p0, Lchk;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->Ok()Ljjf;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lchk;->a(Ljjf;Ljjf;Z)V

    .line 124
    :cond_0
    return-void
.end method

.method public final a(Ljjf;Ljjf;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 186
    :try_start_0
    invoke-virtual {p1}, Ljjf;->bnP()J

    move-result-wide v2

    iput-wide v2, p0, Lchk;->aWV:J

    .line 187
    iget-object v1, p0, Lchk;->dK:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :try_start_1
    iget-object v2, p1, Ljjf;->clientExperimentIds:[I

    iget-object v3, p1, Ljjf;->clientExperimentIds:[I

    array-length v3, v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    iput-object v2, p0, Lchk;->aWW:[I

    .line 191
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    :try_start_2
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 195
    iget-object v3, p1, Ljjf;->eoH:[Ljjg;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 196
    invoke-virtual {v5}, Ljjg;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    const-string v1, "GsaConfigFlags"

    const-string v2, "Unable to parse GsaConfig."

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 219
    :goto_1
    return-void

    .line 198
    :cond_0
    :try_start_3
    iget-object v3, p2, Ljjf;->eoH:[Ljjg;

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 199
    invoke-virtual {v5}, Ljjg;->mY()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-virtual {v5}, Ljjg;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    invoke-virtual {v5}, Ljjg;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v5}, Ljsr;->m(Ljsr;)[B

    move-result-object v5

    invoke-static {v0, v5}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 198
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 208
    :cond_2
    const-string v0, "GsaConfigFlags"

    const-string v6, "Override config contains a flag that is no longer in the %s %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "server config, this must be a custom override:"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v5}, Ljjg;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-static {v8, v0, v6, v7}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 210
    invoke-virtual {v5}, Ljjg;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 213
    :cond_3
    invoke-static {v2}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    iput-object v0, p0, Lchk;->aWS:Lijm;

    .line 214
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lchk;->aWT:Ljava/util/Map;

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lchk;->aWU:Lijm;
    :try_end_3
    .catch Ljsq; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1
.end method

.method public final a(Ljjf;Ljjf;Z)V
    .locals 2

    .prologue
    .line 161
    .line 162
    if-nez p3, :cond_0

    .line 163
    invoke-virtual {p0, p1, p2}, Lchk;->a(Ljjf;Ljjf;)V

    .line 165
    new-instance p1, Ljjf;

    invoke-direct {p1}, Ljjf;-><init>()V

    iget-wide v0, p0, Lchk;->aWV:J

    invoke-virtual {p1, v0, v1}, Ljjf;->dt(J)Ljjf;

    iget-object v0, p0, Lchk;->aWW:[I

    iget-object v1, p0, Lchk;->aWW:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p1, Ljjf;->clientExperimentIds:[I

    iget-object v0, p0, Lchk;->aWS:Lijm;

    invoke-virtual {v0}, Lijm;->aWM()Lijd;

    move-result-object v0

    iget-object v1, p0, Lchk;->aWS:Lijm;

    invoke-virtual {v1}, Lijm;->aWM()Lijd;

    move-result-object v1

    invoke-virtual {v1}, Lijd;->size()I

    move-result v1

    new-array v1, v1, [Ljjg;

    invoke-virtual {v0, v1}, Lijd;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljjg;

    iput-object v0, p1, Ljjf;->eoH:[Ljjg;

    .line 167
    :cond_0
    iget-object v0, p0, Lchk;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchm;

    .line 168
    invoke-interface {v0, p1, p3}, Lchm;->a(Ljjf;Z)V

    goto :goto_0

    .line 170
    :cond_1
    return-void
.end method

.method public final ai(II)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1702
    iget-object v0, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    .line 1703
    if-eqz v0, :cond_0

    .line 1708
    :goto_0
    return-object v0

    .line 1706
    :cond_0
    invoke-direct {p0, p1, p2}, Lchk;->aj(II)[Ljava/lang/String;

    move-result-object v0

    .line 1707
    iget-object v1, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Lchm;)V
    .locals 2

    .prologue
    .line 150
    iget-object v1, p0, Lchk;->aWy:Ljava/util/List;

    monitor-enter v1

    .line 151
    :try_start_0
    iget-object v0, p0, Lchk;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 152
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ci(Z)V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lchk;->mSettings:Lcke;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcke;->a(Ljjf;)V

    .line 129
    if-nez p1, :cond_0

    .line 130
    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v0

    iput-object v0, p0, Lchk;->aWS:Lijm;

    .line 132
    :cond_0
    return-void
.end method

.method public final eI(I)[F
    .locals 5

    .prologue
    .line 969
    invoke-virtual {p0, p1}, Lchk;->eN(I)[I

    move-result-object v1

    .line 970
    array-length v0, v1

    new-array v2, v0, [F

    .line 971
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 972
    aget v3, v1, v0

    .line 973
    if-ltz v3, :cond_0

    const/16 v4, 0x64

    if-le v3, v4, :cond_1

    .line 974
    :cond_0
    const/4 v3, 0x0

    aput v3, v2, v0

    .line 971
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 976
    :cond_1
    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    aput v3, v2, v0

    goto :goto_1

    .line 979
    :cond_2
    return-object v2
.end method

.method public final eJ(I)Z
    .locals 1

    .prologue
    .line 1027
    const/16 v0, 0x71

    invoke-direct {p0, v0, p1}, Lchk;->ah(II)Z

    move-result v0

    return v0
.end method

.method public final eK(I)Z
    .locals 1

    .prologue
    .line 1039
    const/16 v0, 0x72

    invoke-direct {p0, v0, p1}, Lchk;->ah(II)Z

    move-result v0

    return v0
.end method

.method public final eL(I)I
    .locals 1

    .prologue
    .line 1684
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1685
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljjg;->getIntValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lchh;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method public final eM(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1689
    invoke-virtual {p0, p1}, Lchk;->eU(I)Ljjg;

    move-result-object v0

    .line 1690
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lchh;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final eN(I)[I
    .locals 3

    .prologue
    .line 1744
    iget-object v0, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    .line 1745
    if-eqz v0, :cond_0

    .line 1750
    :goto_0
    return-object v0

    .line 1748
    :cond_0
    invoke-direct {p0, p1}, Lchk;->eO(I)[I

    move-result-object v0

    .line 1749
    iget-object v1, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final eP(I)[[I
    .locals 3

    .prologue
    .line 1784
    iget-object v0, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    check-cast v0, [[I

    .line 1785
    if-eqz v0, :cond_0

    .line 1790
    :goto_0
    return-object v0

    .line 1788
    :cond_0
    invoke-direct {p0, p1}, Lchk;->eQ(I)[[I

    move-result-object v0

    .line 1789
    iget-object v1, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final eR(I)[J
    .locals 3

    .prologue
    .line 1828
    iget-object v0, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    check-cast v0, [J

    .line 1829
    if-eqz v0, :cond_0

    .line 1834
    :goto_0
    return-object v0

    .line 1832
    :cond_0
    invoke-direct {p0, p1}, Lchk;->eS(I)[J

    move-result-object v0

    .line 1833
    iget-object v1, p0, Lchk;->aWT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected eU(I)Ljjg;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867
    iget-object v0, p0, Lchk;->aWS:Lijm;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1868
    iget-object v0, p0, Lchk;->aWS:Lijm;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjg;

    .line 1879
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHotwordConfigMap()Ljava/util/Map;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 321
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 322
    invoke-virtual {p0}, Lchk;->FG()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 367
    :goto_0
    return-object v0

    .line 326
    :cond_0
    const/16 v0, 0xbe

    invoke-virtual {p0, v0, v7}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v3

    .line 328
    if-nez v3, :cond_1

    move-object v0, v1

    .line 329
    goto :goto_0

    .line 331
    :cond_1
    array-length v4, v3

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 332
    new-instance v6, Lcsi;

    invoke-direct {v6}, Lcsi;-><init>()V

    .line 333
    invoke-virtual {v6, v5}, Lcsi;->iM(Ljava/lang/String;)Lcsi;

    .line 334
    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 338
    :cond_2
    const/16 v0, 0xc0

    invoke-virtual {p0, v0, v7}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v4

    .line 340
    if-eqz v4, :cond_5

    .line 341
    array-length v5, v4

    .line 342
    rem-int/lit8 v0, v5, 0x2

    if-nez v0, :cond_4

    move v3, v2

    .line 343
    :goto_2
    if-ge v3, v5, :cond_5

    .line 345
    aget-object v0, v4, v3

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 346
    if-eqz v0, :cond_3

    .line 347
    add-int/lit8 v6, v3, 0x1

    aget-object v6, v4, v6

    invoke-virtual {v0, v6}, Lcsi;->iN(Ljava/lang/String;)Lcsi;

    .line 343
    :cond_3
    add-int/lit8 v0, v3, 0x2

    move v3, v0

    goto :goto_2

    .line 352
    :cond_4
    const-string v0, "GsaConfigFlags"

    const-string v3, "Invalid input: hotword_models_locations"

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v0, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 356
    :cond_5
    const/16 v0, 0xbf

    invoke-virtual {p0, v0, v7}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v3

    .line 358
    if-eqz v3, :cond_7

    .line 359
    array-length v4, v3

    :goto_3
    if-ge v2, v4, :cond_7

    aget-object v0, v3, v2

    .line 360
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 361
    if-eqz v0, :cond_6

    .line 362
    invoke-virtual {v0, v7}, Lcsi;->cG(Z)Lcsi;

    .line 359
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_7
    move-object v0, v1

    .line 367
    goto :goto_0
.end method

.method public final gm(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 305
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lesp;->a([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final gn(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 412
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lesp;->a([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final go(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 753
    const/16 v2, 0x1f

    invoke-virtual {p0, v2, v0}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v3

    .line 755
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 756
    if-eqz v5, :cond_0

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 762
    :goto_1
    return v0

    .line 755
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 762
    goto :goto_1
.end method

.method public final gp(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1410
    const/16 v0, 0x7f

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lchk;->ai(II)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lesp;->a([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
