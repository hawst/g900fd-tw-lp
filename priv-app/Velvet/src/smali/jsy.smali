.class public final Ljsy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eDd:[Ljsy;


# instance fields
.field private aez:I

.field private eDe:I

.field private eDf:I

.field private eDg:I

.field private eDh:Z

.field private eDi:Z

.field private eiV:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 200
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 201
    iput v0, p0, Ljsy;->aez:I

    iput v0, p0, Ljsy;->eiV:I

    iput v0, p0, Ljsy;->eDe:I

    iput v0, p0, Ljsy;->eDf:I

    iput v0, p0, Ljsy;->eDg:I

    iput-boolean v0, p0, Ljsy;->eDh:Z

    iput-boolean v0, p0, Ljsy;->eDi:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljsy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljsy;->eCz:I

    .line 202
    return-void
.end method

.method public static bum()[Ljsy;
    .locals 2

    .prologue
    .line 73
    sget-object v0, Ljsy;->eDd:[Ljsy;

    if-nez v0, :cond_1

    .line 74
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    sget-object v0, Ljsy;->eDd:[Ljsy;

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    new-array v0, v0, [Ljsy;

    sput-object v0, Ljsy;->eDd:[Ljsy;

    .line 79
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_1
    sget-object v0, Ljsy;->eDd:[Ljsy;

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 42
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljsy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljsy;->eiV:I

    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljsy;->eDe:I

    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsy;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsy;->eDf:I

    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsy;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsy;->eDh:Z

    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljsy;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsy;->eDg:I

    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljsy;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsy;->eDi:Z

    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljsy;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 220
    iget v0, p0, Ljsy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    iget v1, p0, Ljsy;->eiV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 223
    :cond_0
    iget v0, p0, Ljsy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 224
    const/4 v0, 0x2

    iget v1, p0, Ljsy;->eDe:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 226
    :cond_1
    iget v0, p0, Ljsy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 227
    const/4 v0, 0x3

    iget v1, p0, Ljsy;->eDf:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 229
    :cond_2
    iget v0, p0, Ljsy;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 230
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljsy;->eDh:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 232
    :cond_3
    iget v0, p0, Ljsy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 233
    const/4 v0, 0x5

    iget v1, p0, Ljsy;->eDg:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 235
    :cond_4
    iget v0, p0, Ljsy;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 236
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljsy;->eDi:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 238
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 239
    return-void
.end method

.method public final bun()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Ljsy;->eiV:I

    return v0
.end method

.method public final buo()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Ljsy;->eDe:I

    return v0
.end method

.method public final ja(Z)Ljsy;
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljsy;->eDi:Z

    .line 188
    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljsy;->aez:I

    .line 189
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 243
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 244
    iget v1, p0, Ljsy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 245
    const/4 v1, 0x1

    iget v2, p0, Ljsy;->eiV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_0
    iget v1, p0, Ljsy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 249
    const/4 v1, 0x2

    iget v2, p0, Ljsy;->eDe:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_1
    iget v1, p0, Ljsy;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 253
    const/4 v1, 0x3

    iget v2, p0, Ljsy;->eDf:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    :cond_2
    iget v1, p0, Ljsy;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 257
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljsy;->eDh:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 260
    :cond_3
    iget v1, p0, Ljsy;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 261
    const/4 v1, 0x5

    iget v2, p0, Ljsy;->eDg:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_4
    iget v1, p0, Ljsy;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 265
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljsy;->eDi:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 268
    :cond_5
    return v0
.end method

.method public final sp(I)Ljsy;
    .locals 1

    .prologue
    .line 92
    iput p1, p0, Ljsy;->eiV:I

    .line 93
    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsy;->aez:I

    .line 94
    return-object p0
.end method

.method public final sq(I)Ljsy;
    .locals 1

    .prologue
    .line 111
    iput p1, p0, Ljsy;->eDe:I

    .line 112
    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsy;->aez:I

    .line 113
    return-object p0
.end method

.method public final sr(I)Ljsy;
    .locals 1

    .prologue
    .line 130
    iput p1, p0, Ljsy;->eDf:I

    .line 131
    iget v0, p0, Ljsy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsy;->aez:I

    .line 132
    return-object p0
.end method
