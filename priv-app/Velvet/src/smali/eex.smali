.class public final Leex;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leew;


# instance fields
.field private bXu:Leen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Leen;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Leex;->bXu:Leen;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t reset click listener"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 17
    iput-object p1, p0, Leex;->bXu:Leen;

    .line 18
    return-void

    .line 14
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ligi;)Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Leex;->bXu:Leen;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Leex;->bXu:Leen;

    invoke-interface {v0, p1, p2, p3}, Leen;->a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ligi;)Z

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(Lcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Leex;->bXu:Leen;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Leex;->bXu:Leen;

    invoke-interface {v0, p1}, Leen;->j(Lcom/google/android/shared/search/Suggestion;)V

    .line 25
    :cond_0
    return-void
.end method

.method public final k(Lcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Leex;->bXu:Leen;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Leex;->bXu:Leen;

    invoke-interface {v0, p1}, Leen;->k(Lcom/google/android/shared/search/Suggestion;)V

    .line 32
    :cond_0
    return-void
.end method

.method public final l(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Leex;->bXu:Leen;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Leex;->bXu:Leen;

    invoke-interface {v0, p1}, Leen;->l(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v0

    .line 39
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m(Lcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Leex;->bXu:Leen;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Leex;->bXu:Leen;

    invoke-interface {v0, p1}, Leen;->m(Lcom/google/android/shared/search/Suggestion;)V

    .line 48
    :cond_0
    return-void
.end method
