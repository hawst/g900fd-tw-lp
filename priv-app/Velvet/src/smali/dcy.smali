.class public Ldcy;
.super Lddj;
.source "PG"


# instance fields
.field private So:I

.field private final bqt:Lcom/google/android/shared/util/BitFlags;

.field private bqv:Lcom/google/android/shared/search/Query;

.field private brv:Lcom/google/android/shared/search/Query;

.field private btg:Lhha;

.field private bth:Lcom/google/android/velvet/ActionData;

.field private bti:I

.field private btj:I

.field private btk:Z

.field private btl:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mActionState:Ldbd;

.field private final mClock:Lemp;

.field private mCorporaOrder:[I

.field private final mEventBus:Ldda;

.field private final mGsaConfigFlags:Lchk;

.field private final mNetworkClient:Lfcx;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Ldda;Lchk;Lcke;Lfcx;Lemp;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 112
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 85
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 87
    sget-object v0, Lhha;->dgR:Lhha;

    iput-object v0, p0, Ldcy;->btg:Lhha;

    .line 88
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    .line 91
    iput v2, p0, Ldcy;->bti:I

    .line 92
    iput v2, p0, Ldcy;->btj:I

    .line 93
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcy;->brv:Lcom/google/android/shared/search/Query;

    .line 113
    iput-object p1, p0, Ldcy;->mEventBus:Ldda;

    .line 114
    iget-object v0, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 115
    iget-object v0, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    iput-object v0, p0, Ldcy;->mActionState:Ldbd;

    .line 116
    iput-object p2, p0, Ldcy;->mGsaConfigFlags:Lchk;

    .line 117
    iput-object p3, p0, Ldcy;->mSearchSettings:Lcke;

    .line 118
    iput-object p4, p0, Ldcy;->mNetworkClient:Lfcx;

    .line 119
    iput-object p5, p0, Ldcy;->mClock:Lemp;

    .line 120
    iput-boolean v2, p0, Ldcy;->btk:Z

    .line 122
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x800

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 123
    return-void
.end method

.method private ZW()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 647
    iget-object v0, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldcy;->btl:Ljava/lang/Object;

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 650
    :goto_0
    const/16 v3, 0x400

    invoke-direct {p0, v0, v3}, Ldcy;->d(ZI)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x800

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 647
    goto :goto_0

    :cond_2
    move v2, v1

    .line 650
    goto :goto_1
.end method

.method private ZY()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 818
    sget-object v1, Lcgg;->aVK:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 851
    :cond_0
    :goto_0
    const/high16 v1, 0x40000

    invoke-direct {p0, v0, v1}, Ldcy;->d(ZI)Z

    move-result v0

    return v0

    .line 821
    :cond_1
    iget-object v1, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aao()Ldcu;

    move-result-object v1

    invoke-virtual {v1}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->ano()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 824
    iget-object v1, p0, Ldcy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Jl()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 827
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v1

    iget-object v2, p0, Ldcy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->Jq()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 831
    iget-object v1, p0, Ldcy;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->Oz()I

    move-result v1

    iget-object v2, p0, Ldcy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->Jo()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 836
    iget-object v1, p0, Ldcy;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->Oy()J

    move-result-wide v2

    .line 837
    iget-object v1, p0, Ldcy;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 839
    sub-long v2, v4, v2

    iget-object v1, p0, Ldcy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Jp()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 842
    iget-object v1, p0, Ldcy;->mNetworkClient:Lfcx;

    invoke-interface {v1}, Lfcx;->awo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 847
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private an(II)Z
    .locals 4

    .prologue
    .line 959
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 960
    invoke-static {p2}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 962
    const/4 v0, 0x1

    .line 964
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ao(II)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 921
    iget v1, p0, Ldcy;->bti:I

    if-ne v1, p1, :cond_0

    iget v1, p0, Ldcy;->btj:I

    if-eq v1, p2, :cond_3

    .line 922
    :cond_0
    iput p1, p0, Ldcy;->bti:I

    .line 923
    iput p2, p0, Ldcy;->btj:I

    .line 924
    iget v1, p0, Ldcy;->bti:I

    if-ne v1, v4, :cond_4

    .line 925
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1000

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 926
    iput v0, p0, Ldcy;->bti:I

    .line 931
    :cond_1
    :goto_0
    iget v1, p0, Ldcy;->btj:I

    if-ne v1, v4, :cond_5

    .line 932
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 933
    iput v0, p0, Ldcy;->btj:I

    .line 941
    :cond_2
    :goto_1
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    .line 943
    :cond_3
    return v0

    .line 927
    :cond_4
    iget v1, p0, Ldcy;->bti:I

    if-ne v1, v5, :cond_1

    .line 928
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x80000

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 929
    iput v0, p0, Ldcy;->bti:I

    goto :goto_0

    .line 934
    :cond_5
    iget v1, p0, Ldcy;->btj:I

    if-ne v1, v5, :cond_2

    .line 935
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x100000

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 936
    iput v0, p0, Ldcy;->btj:I

    goto :goto_1
.end method

.method private b(Lcom/google/android/shared/search/Query;ZZ)Z
    .locals 8

    .prologue
    .line 393
    const/4 v0, 0x0

    .line 395
    iget-object v1, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 396
    iget-object v1, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    .line 399
    if-eqz p3, :cond_0

    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v1

    if-nez v1, :cond_0

    .line 400
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x100

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    .line 403
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqc()Z

    move-result v1

    if-nez v1, :cond_0

    .line 405
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x10

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    .line 406
    const/4 v1, 0x0

    iput-object v1, p0, Ldcy;->mCorporaOrder:[I

    .line 410
    :cond_0
    iget-object v1, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Qk()[I

    move-result-object v1

    .line 411
    iget-object v4, p0, Ldcy;->mCorporaOrder:[I

    if-nez v4, :cond_1

    if-eqz v1, :cond_1

    .line 413
    iput-object v1, p0, Ldcy;->mCorporaOrder:[I

    .line 414
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x10

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 419
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    .line 425
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->XA()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move v4, v2

    .line 428
    :goto_1
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v2}, Ldbd;->yo()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v2}, Ldbd;->VK()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Ldcy;->btg:Lhha;

    invoke-virtual {v2}, Lhha;->aOI()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_2
    iget-object v2, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x100

    invoke-virtual {v2, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    .line 433
    :goto_2
    iget-object v3, p0, Ldcy;->mCorporaOrder:[I

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x10

    invoke-virtual {v3, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_3
    const/4 v3, 0x1

    .line 438
    :goto_3
    if-nez v1, :cond_4

    if-nez v4, :cond_4

    if-nez v2, :cond_4

    if-nez p2, :cond_4

    if-eqz v3, :cond_9

    :cond_4
    const/4 v1, 0x1

    .line 452
    :goto_4
    if-nez v1, :cond_a

    .line 453
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    .line 455
    or-int/2addr v0, v1

    .line 459
    :goto_5
    return v0

    .line 419
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 425
    :cond_6
    const/4 v2, 0x0

    move v4, v2

    goto :goto_1

    .line 428
    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 433
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    .line 438
    :cond_9
    const/4 v1, 0x0

    goto :goto_4

    .line 457
    :cond_a
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    const-wide/16 v4, 0x80

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_5
.end method

.method private b(ZII)Z
    .locals 4

    .prologue
    .line 955
    if-eqz p1, :cond_0

    invoke-direct {p0, p2, p3}, Ldcy;->an(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0
.end method

.method private d(ZI)Z
    .locals 4

    .prologue
    .line 947
    if-eqz p1, :cond_0

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final ZA()Z
    .locals 4

    .prologue
    .line 256
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZB()Z
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZC()Z
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZD()Z
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZE()Z
    .locals 4

    .prologue
    .line 272
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZF()Z
    .locals 4

    .prologue
    .line 276
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x800

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final ZG()V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x800

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 281
    return-void
.end method

.method public final ZH()Z
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x200000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZI()Z
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ZJ()V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aat()Ldcf;

    move-result-object v0

    iget-object v1, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Ldcf;->a(Lcom/google/android/velvet/ActionData;I)V

    .line 301
    :cond_0
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Ldcy;->b(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 305
    :cond_1
    return-void
.end method

.method public final ZK()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 308
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x200

    invoke-virtual {v2, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 309
    iget-object v2, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x80

    invoke-virtual {v2, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 313
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final ZL()[I
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Ldcy;->mCorporaOrder:[I

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iget-object v0, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Ldcy;->b(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 335
    :cond_0
    iget-object v0, p0, Ldcy;->mCorporaOrder:[I

    .line 337
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZM()Lhha;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Ldcy;->btg:Lhha;

    return-object v0
.end method

.method public final ZN()Z
    .locals 4

    .prologue
    .line 349
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final ZO()Z
    .locals 4

    .prologue
    .line 356
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZP()Z
    .locals 4

    .prologue
    .line 363
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZQ()Z
    .locals 4

    .prologue
    .line 370
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x100000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZR()I
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ldcy;->bti:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZS()I
    .locals 1

    .prologue
    .line 380
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ldcy;->btj:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZT()Z
    .locals 4

    .prologue
    .line 467
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final ZU()Z
    .locals 4

    .prologue
    .line 475
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x40000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final ZV()Z
    .locals 4

    .prologue
    .line 483
    iget-object v0, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    invoke-virtual {v0}, Lddk;->aaM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    const/4 v0, 0x0

    .line 486
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x400000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0
.end method

.method public final ZX()Z
    .locals 1

    .prologue
    .line 727
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldcy;->ZB()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ZZ()V
    .locals 4

    .prologue
    .line 860
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 863
    :cond_0
    return-void
.end method

.method public final Zx()Z
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final Zy()Z
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final Zz()Z
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;II)V
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 914
    invoke-direct {p0, p2, p3}, Ldcy;->ao(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 915
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 918
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    iget v0, p0, Ldcy;->bti:I

    if-eqz v0, :cond_0

    .line 235
    const-string v0, "UiState:header_state"

    iget v1, p0, Ldcy;->bti:I

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 237
    :cond_0
    iget v0, p0, Ldcy;->btj:I

    if-eqz v0, :cond_1

    .line 238
    const-string v0, "UiState:footer_state"

    iget v1, p0, Ldcy;->btj:I

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 241
    :cond_1
    return-void
.end method

.method public final a(Lddb;)V
    .locals 11

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    const/4 v1, 0x0

    .line 130
    iget-object v5, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    .line 131
    iget-object v2, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 133
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v2

    if-eqz v2, :cond_3f

    .line 134
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    iget-object v2, p0, Ldcy;->brv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-eqz v2, :cond_0

    .line 137
    const/4 v2, 0x1

    iput-boolean v2, p0, Ldcy;->btk:Z

    .line 138
    iput-object v3, p0, Ldcy;->brv:Lcom/google/android/shared/search/Query;

    .line 141
    :cond_0
    iget-boolean v2, p0, Ldcy;->btk:Z

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 142
    invoke-static {v3}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v2

    .line 143
    sget-object v4, Lgyt;->cZB:Lgyt;

    if-eq v2, v4, :cond_1

    sget-object v4, Lgyt;->cZD:Lgyt;

    if-ne v2, v4, :cond_19

    .line 145
    :cond_1
    invoke-direct {p0}, Ldcy;->ZY()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 146
    const/4 v2, 0x0

    iput-boolean v2, p0, Ldcy;->btk:Z

    .line 157
    :cond_2
    :goto_0
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    iget-object v2, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-eqz v2, :cond_3e

    .line 158
    iget-object v2, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v6, 0xe000

    const-wide/16 v8, 0x1000

    invoke-virtual {v2, v6, v7, v8, v9}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v2

    or-int/2addr v2, v0

    .line 162
    iget-object v0, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, v3}, Lcom/google/android/search/core/state/QueryState;->J(Lcom/google/android/shared/search/Query;)Landroid/os/Bundle;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_3d

    .line 164
    const-string v1, "UiState:header_state"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v4, "UiState:footer_state"

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v1, v0}, Ldcy;->ao(II)Z

    move-result v0

    .line 167
    or-int v1, v2, v0

    .line 170
    :goto_1
    iput-object v3, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    .line 173
    :goto_2
    const/4 v2, 0x0

    .line 174
    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    iget-object v4, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3, v4}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 175
    iget-object v2, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v2}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    .line 180
    iget-object v2, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    invoke-static {v3, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_3
    iget-object v2, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->agw()Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_4
    const/4 v2, 0x1

    .line 184
    :goto_3
    iput-object v3, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    .line 186
    if-eqz v2, :cond_1b

    iget-object v3, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x20

    invoke-virtual {v3, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v3

    if-eqz v3, :cond_1b

    const/4 v3, 0x1

    :goto_4
    or-int/2addr v1, v3

    .line 188
    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->VS()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 190
    iget-object v3, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x2000

    invoke-virtual {v3, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v3

    or-int/2addr v1, v3

    .line 194
    :cond_5
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v6

    .line 195
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 196
    sget-object v3, Lcgg;->aVm:Lcgg;

    invoke-virtual {v3}, Lcgg;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1d

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v3

    if-eqz v3, :cond_1c

    const/4 v3, 0x1

    :goto_5
    const/4 v4, 0x4

    invoke-direct {p0, v3, v4}, Ldcy;->d(ZI)Z

    move-result v3

    :goto_6
    or-int/2addr v1, v3

    .line 197
    invoke-direct {p0}, Ldcy;->ZW()Z

    move-result v3

    or-int/2addr v3, v1

    .line 198
    iget-object v1, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->XZ()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, Ldcy;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Kc()Z

    move-result v1

    if-eqz v1, :cond_1e

    const/4 v1, 0x1

    :goto_7
    const/high16 v4, 0x200000

    invoke-direct {p0, v1, v4}, Ldcy;->d(ZI)Z

    move-result v1

    :goto_8
    or-int/2addr v1, v3

    .line 200
    :cond_6
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 201
    :cond_7
    const/4 v4, 0x0

    iget-object v3, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aao()Ldcu;

    move-result-object v3

    invoke-virtual {v3}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/shared/service/ClientConfig;->amX()Z

    move-result v3

    if-eqz v3, :cond_20

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xc()Z

    move-result v3

    if-nez v3, :cond_20

    const/4 v3, 0x1

    :goto_9
    if-eqz v3, :cond_21

    iget-object v3, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x10000

    invoke-virtual {v3, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v3

    or-int/lit8 v3, v3, 0x0

    :goto_a
    iget-object v7, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v7}, Lcom/google/android/search/core/state/QueryState;->WZ()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v4, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x20000

    invoke-virtual {v4, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v4

    or-int/lit8 v4, v4, 0x0

    :cond_8
    if-eqz v3, :cond_9

    iget-object v7, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x20000

    invoke-virtual {v7, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v7

    or-int/2addr v4, v7

    :cond_9
    if-nez v3, :cond_a

    if-eqz v4, :cond_22

    :cond_a
    const/4 v3, 0x1

    :goto_b
    or-int/2addr v1, v3

    .line 203
    :cond_b
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {p1}, Lddb;->aaF()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 205
    :cond_c
    iget-object v3, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    if-eqz v3, :cond_24

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->XL()Z

    move-result v3

    if-nez v3, :cond_d

    iget-object v3, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->ZM()Lhha;

    move-result-object v3

    invoke-virtual {v3}, Lhha;->aOI()Z

    move-result v3

    if-eqz v3, :cond_23

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->VU()Z

    move-result v3

    if-nez v3, :cond_d

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->VK()Z

    move-result v3

    if-nez v3, :cond_23

    :cond_d
    sget-object v3, Lhha;->dgR:Lhha;

    :goto_c
    iget-object v4, p0, Ldcy;->btg:Lhha;

    if-eq v3, v4, :cond_24

    iput-object v3, p0, Ldcy;->btg:Lhha;

    const/4 v3, 0x1

    :goto_d
    or-int/2addr v3, v1

    .line 206
    iget-object v1, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Yo()Z

    move-result v1

    if-eqz v1, :cond_25

    const/4 v1, 0x0

    :goto_e
    const/4 v4, 0x2

    const/16 v7, 0xaa

    invoke-direct {p0, v1, v4, v7}, Ldcy;->b(ZII)Z

    move-result v4

    .line 207
    or-int v7, v3, v4

    .line 208
    const/4 v1, 0x0

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yb()Z

    move-result v3

    if-nez v3, :cond_27

    const/4 v1, 0x1

    :cond_e
    :goto_f
    if-eqz v1, :cond_34

    const/4 v1, 0x0

    :goto_10
    const/4 v3, 0x1

    const/16 v8, 0xab

    invoke-direct {p0, v1, v3, v8}, Ldcy;->b(ZII)Z

    move-result v3

    .line 209
    or-int v1, v7, v3

    .line 211
    if-nez v4, :cond_f

    if-eqz v3, :cond_3c

    :cond_f
    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v7

    if-eqz v7, :cond_3c

    if-nez v0, :cond_3c

    .line 213
    const/4 v0, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, v0, v7}, Ldcy;->ao(II)Z

    move-result v0

    or-int/2addr v0, v1

    .line 217
    :goto_11
    if-nez v4, :cond_10

    if-eqz v3, :cond_11

    .line 218
    :cond_10
    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x400000

    invoke-virtual {v1, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 220
    :cond_11
    const/4 v1, 0x0

    iget-object v3, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aar()Lddk;

    move-result-object v3

    iget-object v4, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Xh()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-virtual {v3}, Lddk;->aaN()Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v4

    if-eqz v4, :cond_13

    iget-object v4, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Yd()Z

    move-result v4

    if-nez v4, :cond_13

    iget-object v4, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Yf()Z

    move-result v4

    if-nez v4, :cond_13

    sget-object v4, Lcgg;->aVn:Lcgg;

    invoke-virtual {v4}, Lcgg;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_12

    iget-object v4, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v4}, Ldbd;->VK()Z

    move-result v4

    if-nez v4, :cond_13

    :cond_12
    const/4 v1, 0x1

    :cond_13
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v3

    if-eqz v3, :cond_14

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->yo()Z

    move-result v3

    if-nez v3, :cond_14

    const/4 v1, 0x1

    :cond_14
    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yo()Z

    move-result v3

    if-eqz v3, :cond_15

    const/4 v1, 0x1

    :cond_15
    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v3

    if-eqz v3, :cond_16

    const/4 v1, 0x0

    :cond_16
    if-eqz v1, :cond_39

    const/16 v1, 0x8

    const/16 v3, 0xc8

    invoke-direct {p0, v1, v3}, Ldcy;->an(II)Z

    move-result v1

    :goto_12
    or-int/2addr v1, v0

    .line 221
    const v0, 0x3f4ccccd    # 0.8f

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yg()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iget v3, p0, Ldcy;->So:I

    if-eq v3, v0, :cond_3b

    iput v0, p0, Ldcy;->So:I

    const/4 v0, 0x1

    :goto_13
    or-int/2addr v0, v1

    .line 222
    invoke-direct {p0, v5, v2, v6}, Ldcy;->b(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v1

    or-int/2addr v1, v0

    .line 225
    :cond_17
    if-eqz v1, :cond_18

    .line 227
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 229
    :cond_18
    return-void

    .line 147
    :cond_19
    sget-object v4, Lgyt;->cZA:Lgyt;

    if-ne v2, v4, :cond_2

    .line 150
    iget-object v2, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 151
    invoke-direct {p0}, Ldcy;->ZY()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 152
    const/4 v2, 0x0

    iput-boolean v2, p0, Ldcy;->btk:Z

    goto/16 :goto_0

    .line 180
    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 186
    :cond_1b
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 196
    :cond_1c
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_1d
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 198
    :cond_1e
    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_1f
    const/4 v1, 0x0

    goto/16 :goto_8

    .line 201
    :cond_20
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_21
    iget-object v3, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x10000

    invoke-virtual {v3, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v3

    or-int/lit8 v3, v3, 0x0

    goto/16 :goto_a

    :cond_22
    const/4 v3, 0x0

    goto/16 :goto_b

    .line 205
    :cond_23
    iget-object v3, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->ZM()Lhha;

    move-result-object v3

    goto/16 :goto_c

    :cond_24
    const/4 v3, 0x0

    goto/16 :goto_d

    .line 206
    :cond_25
    iget-object v1, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->VH()Z

    move-result v1

    if-eqz v1, :cond_26

    iget-object v1, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->VG()Z

    move-result v1

    if-nez v1, :cond_26

    const/4 v1, 0x0

    goto/16 :goto_e

    :cond_26
    iget-object v1, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->VK()Z

    move-result v1

    goto/16 :goto_e

    .line 208
    :cond_27
    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yo()Z

    move-result v3

    if-eqz v3, :cond_28

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_28
    iget-object v3, p0, Ldcy;->btg:Lhha;

    invoke-virtual {v3}, Lhha;->aOJ()Z

    move-result v3

    if-eqz v3, :cond_29

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_29
    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yc()Z

    move-result v3

    if-eqz v3, :cond_2a

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_2a
    iget-object v3, p0, Ldcy;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aav()Ldcv;

    move-result-object v3

    invoke-virtual {v3}, Ldcv;->Zl()Z

    move-result v3

    if-eqz v3, :cond_2b

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_2b
    sget-object v3, Lcgg;->aVn:Lcgg;

    invoke-virtual {v3}, Lcgg;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2d

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v3

    if-eqz v3, :cond_2c

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    if-eqz v3, :cond_2c

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v3

    if-eqz v3, :cond_2c

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->VK()Z

    move-result v3

    if-eqz v3, :cond_2c

    const/4 v3, 0x1

    :goto_14
    if-eqz v3, :cond_2d

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_2c
    const/4 v3, 0x0

    goto :goto_14

    :cond_2d
    iget-object v3, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    if-eqz v3, :cond_2e

    iget-object v3, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v3

    if-nez v3, :cond_2e

    iget-object v3, p0, Ldcy;->btg:Lhha;

    invoke-virtual {v3}, Lhha;->aOI()Z

    move-result v3

    if-eqz v3, :cond_2e

    const/4 v3, 0x1

    :goto_15
    if-eqz v3, :cond_30

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    iget-object v8, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v8}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v8

    invoke-virtual {v3, v8}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_2f

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Ye()Z

    move-result v3

    if-nez v3, :cond_2f

    const/4 v3, 0x1

    :goto_16
    if-eqz v3, :cond_30

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_2e
    const/4 v3, 0x0

    goto :goto_15

    :cond_2f
    const/4 v3, 0x0

    goto :goto_16

    :cond_30
    sget-object v3, Lcgg;->aVn:Lcgg;

    invoke-virtual {v3}, Lcgg;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_33

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->XO()Z

    move-result v3

    if-eqz v3, :cond_33

    iget-object v3, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v3

    if-eqz v3, :cond_32

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->yo()Z

    move-result v3

    if-eqz v3, :cond_31

    iget-object v3, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v3}, Ldbd;->VK()Z

    move-result v3

    if-eqz v3, :cond_32

    :cond_31
    const/4 v3, 0x1

    :goto_17
    if-eqz v3, :cond_e

    const/4 v1, 0x1

    goto/16 :goto_f

    :cond_32
    const/4 v3, 0x0

    goto :goto_17

    :cond_33
    const/4 v3, 0x0

    goto :goto_17

    :cond_34
    iget-object v1, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aar()Lddk;

    move-result-object v1

    invoke-virtual {v1}, Lddk;->aaN()Z

    move-result v3

    if-nez v3, :cond_38

    invoke-virtual {p0}, Ldcy;->ZA()Z

    move-result v3

    if-eqz v3, :cond_35

    invoke-virtual {v1}, Lddk;->aaO()Z

    move-result v1

    if-nez v1, :cond_38

    :cond_35
    sget-object v1, Lcgg;->aVn:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_36

    iget-object v1, p0, Ldcy;->mActionState:Ldbd;

    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v1

    if-nez v1, :cond_36

    const/4 v1, 0x1

    :goto_18
    if-eqz v1, :cond_37

    const/4 v1, 0x0

    goto/16 :goto_10

    :cond_36
    const/4 v1, 0x0

    goto :goto_18

    :cond_37
    iget-object v1, p0, Ldcy;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Ya()Z

    move-result v1

    if-nez v1, :cond_38

    const/4 v1, 0x0

    goto/16 :goto_10

    :cond_38
    const/4 v1, 0x1

    goto/16 :goto_10

    .line 220
    :cond_39
    const/16 v1, 0x8

    const/16 v3, 0xc9

    iget-object v4, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v8, v1

    invoke-virtual {v4, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    if-eqz v1, :cond_3a

    invoke-static {v3}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v3, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    const/4 v1, 0x1

    goto/16 :goto_12

    :cond_3a
    const/4 v1, 0x0

    goto/16 :goto_12

    .line 221
    :cond_3b
    const/4 v0, 0x0

    goto/16 :goto_13

    :cond_3c
    move v0, v1

    goto/16 :goto_11

    :cond_3d
    move v0, v1

    move v1, v2

    goto/16 :goto_1

    :cond_3e
    move v10, v1

    move v1, v0

    move v0, v10

    goto/16 :goto_1

    :cond_3f
    move v10, v1

    move v1, v0

    move v0, v10

    goto/16 :goto_2
.end method

.method public final aB(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 497
    iget-object v0, p0, Ldcy;->btl:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    iput-object p1, p0, Ldcy;->btl:Ljava/lang/Object;

    .line 499
    invoke-direct {p0}, Ldcy;->ZW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 503
    :cond_0
    return-void
.end method

.method public final aC(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 511
    iget-object v0, p0, Ldcy;->btl:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    const/4 v0, 0x0

    iput-object v0, p0, Ldcy;->btl:Ljava/lang/Object;

    .line 513
    invoke-direct {p0}, Ldcy;->ZW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 517
    :cond_0
    return-void
.end method

.method public final aaa()V
    .locals 4

    .prologue
    .line 871
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 872
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 874
    :cond_0
    return-void
.end method

.method public final aab()V
    .locals 4

    .prologue
    .line 881
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 882
    invoke-virtual {p0}, Ldcy;->notifyChanged()V

    .line 884
    :cond_0
    return-void
.end method

.method public final aac()Z
    .locals 4

    .prologue
    .line 895
    iget-object v0, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x10000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final f(Lcom/google/android/shared/search/Query;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x1

    .line 905
    if-eqz p2, :cond_0

    .line 906
    invoke-virtual {p0, p1, v1, v1}, Ldcy;->a(Lcom/google/android/shared/search/Query;II)V

    .line 910
    :goto_0
    return-void

    .line 908
    :cond_0
    invoke-virtual {p0, p1, v0, v0}, Ldcy;->a(Lcom/google/android/shared/search/Query;II)V

    goto :goto_0
.end method

.method public final getProgress()I
    .locals 1

    .prologue
    .line 774
    iget v0, p0, Ldcy;->So:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 978
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UiState[query="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldcy;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "effects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcy;->btg:Lhha;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actionData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcy;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcy;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
