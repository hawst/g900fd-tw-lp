.class public final Ljnc;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile euJ:[Ljnc;


# instance fields
.field private aez:I

.field private dYl:Ljava/lang/String;

.field private dYm:Ljava/lang/String;

.field private dYn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Ljnc;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnc;->dYl:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnc;->dYm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnc;->dYn:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljnc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnc;->eCz:I

    .line 95
    return-void
.end method

.method public static bqy()[Ljnc;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljnc;->euJ:[Ljnc;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljnc;->euJ:[Ljnc;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljnc;

    sput-object v0, Ljnc;->euJ:[Ljnc;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljnc;->euJ:[Ljnc;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnc;->dYl:Ljava/lang/String;

    iget v0, p0, Ljnc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnc;->dYm:Ljava/lang/String;

    iget v0, p0, Ljnc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnc;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnc;->dYn:Ljava/lang/String;

    iget v0, p0, Ljnc;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnc;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Ljnc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x1

    iget-object v1, p0, Ljnc;->dYl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 113
    :cond_0
    iget v0, p0, Ljnc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 114
    const/4 v0, 0x2

    iget-object v1, p0, Ljnc;->dYm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 116
    :cond_1
    iget v0, p0, Ljnc;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 117
    const/4 v0, 0x3

    iget-object v1, p0, Ljnc;->dYn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 119
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 120
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 124
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 125
    iget v1, p0, Ljnc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 126
    const/4 v1, 0x1

    iget-object v2, p0, Ljnc;->dYl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_0
    iget v1, p0, Ljnc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 130
    const/4 v1, 0x2

    iget-object v2, p0, Ljnc;->dYm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_1
    iget v1, p0, Ljnc;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 134
    const/4 v1, 0x3

    iget-object v2, p0, Ljnc;->dYn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_2
    return v0
.end method
