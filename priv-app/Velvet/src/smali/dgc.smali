.class public Ldgc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static bwO:Ljava/util/Map;

.field private static final bwP:Ljava/util/Map;


# instance fields
.field private final bwQ:Ljava/util/Map;

.field private final bwR:Ljava/util/Map;

.field private final bwa:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    invoke-static {}, Lijm;->aWZ()Lijn;

    move-result-object v0

    const-string v1, "content://applications/search_suggest_query"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "applications"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const-string v1, "content://com.android.contacts/search_suggest_query"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "contacts"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    move-result-object v0

    sput-object v0, Ldgc;->bwO:Ljava/util/Map;

    .line 31
    invoke-static {}, Lijm;->aWZ()Lijn;

    move-result-object v0

    sget-object v1, Lcum;->bjr:Lcum;

    const-string v2, "applications"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    sget-object v1, Lcum;->bjs:Lcum;

    const-string v2, "contacts"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    move-result-object v0

    sput-object v0, Ldgc;->bwP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Ldgc;->bwO:Ljava/util/Map;

    sget-object v1, Ldgc;->bwP:Ljava/util/Map;

    invoke-direct {p0, p1, v0, v1}, Ldgc;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 46
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Ldgc;->bwa:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Ldgc;->bwQ:Ljava/util/Map;

    .line 53
    iput-object p3, p0, Ldgc;->bwR:Ljava/util/Map;

    .line 54
    return-void
.end method

.method public static d(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static jA(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    return-object p0
.end method

.method private jB(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ldgc;->bwa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static jC(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 98
    return-object p0
.end method


# virtual methods
.method public final a(Lbch;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Ldgc;->bwa:Ljava/lang/String;

    invoke-virtual {p1}, Lbch;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p1}, Lbch;->xg()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ldgc;->jB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lbch;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcum;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p1}, Lcum;->xF()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ldgc;->jB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcum;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Ldgc;->bwR:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 94
    iget-object v0, p0, Ldgc;->bwR:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final d(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Ldgc;->bwQ:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Ldgc;->bwQ:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method
