.class public final Lfut;
.super Lfus;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 56
    return-void
.end method

.method private a(Landroid/content/Context;Lixh;Z)Lane;
    .locals 9

    .prologue
    .line 102
    new-instance v8, Lane;

    invoke-direct {v8}, Lane;-><init>()V

    .line 104
    iget-object v0, p2, Lixh;->dME:Lixn;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lixh;->dME:Lixn;

    invoke-virtual {v0}, Lixn;->oK()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p2, Lixh;->dME:Lixn;

    invoke-virtual {v0}, Lixn;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lane;->ah(Ljava/lang/String;)Lane;

    .line 110
    :goto_0
    iget-object v0, p2, Lixh;->dNq:Ljbu;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lixh;->dNq:Ljbu;

    invoke-static {v0}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 112
    iget-object v0, p2, Lixh;->dNq:Ljbu;

    invoke-static {v0}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lane;->ai(Ljava/lang/String;)Lane;

    .line 117
    :goto_1
    iget-object v0, p2, Lixh;->dNp:Ljhl;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lixh;->dNp:Ljhl;

    invoke-virtual {v0}, Ljhl;->blS()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p2, Lixh;->dNp:Ljhl;

    invoke-virtual {v1}, Ljhl;->akO()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 119
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 120
    new-instance v1, Ljava/util/Date;

    iget-object v4, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 121
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    int-to-long v6, v1

    add-long/2addr v4, v6

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v4

    .line 123
    invoke-static {v0, v1, v2, v3}, Lesi;->k(JJ)I

    move-result v0

    .line 124
    const/4 v1, 0x1

    if-le v0, v1, :cond_4

    .line 125
    const v1, 0x7f0a04c6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lane;->aj(Ljava/lang/String;)Lane;

    .line 131
    :cond_0
    :goto_2
    const/4 v1, 0x2

    if-le v0, v1, :cond_7

    .line 132
    if-eqz p3, :cond_6

    .line 133
    const v0, 0x7f0b00b2

    invoke-virtual {v8, v0}, Lane;->ck(I)Lane;

    .line 134
    const v0, 0x7f0b00ba

    invoke-virtual {v8, v0}, Lane;->cl(I)Lane;

    .line 148
    :goto_3
    new-instance v1, Ljava/util/Formatter;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    const v6, 0x8001a

    const-string v7, "UTC"

    move-object v0, p1

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-virtual {v8, v0}, Lane;->ak(Ljava/lang/String;)Lane;

    .line 152
    invoke-virtual {v8, p3}, Lane;->aR(Z)Lane;

    .line 154
    :cond_1
    return-object v8

    .line 107
    :cond_2
    const-string v0, "QpBillListEntryAdapter"

    const-string v1, "Unexpected Entry without bill\'s vendor name."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 114
    :cond_3
    const-string v0, "QpBillListEntryAdapter"

    const-string v1, "Unexpected Entry without bill\'s amount due."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 126
    :cond_4
    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 127
    const v1, 0x7f0a04c7

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lane;->aj(Ljava/lang/String;)Lane;

    goto :goto_2

    .line 128
    :cond_5
    if-nez v0, :cond_0

    .line 129
    const v1, 0x7f0a04c8

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lane;->aj(Ljava/lang/String;)Lane;

    goto :goto_2

    .line 136
    :cond_6
    const v0, 0x7f0b00ba

    invoke-virtual {v8, v0}, Lane;->ck(I)Lane;

    .line 137
    const v0, 0x7f0b00b2

    invoke-virtual {v8, v0}, Lane;->cl(I)Lane;

    goto :goto_3

    .line 140
    :cond_7
    if-eqz p3, :cond_8

    .line 141
    const v0, 0x7f0b00b2

    invoke-virtual {v8, v0}, Lane;->ck(I)Lane;

    .line 142
    const v0, 0x7f0b00b7

    invoke-virtual {v8, v0}, Lane;->cl(I)Lane;

    goto :goto_3

    .line 144
    :cond_8
    const v0, 0x7f0b00b7

    invoke-virtual {v8, v0}, Lane;->ck(I)Lane;

    .line 145
    const v0, 0x7f0b00b2

    invoke-virtual {v8, v0}, Lane;->cl(I)Lane;

    goto :goto_3
.end method

.method private static a(Ljbu;)Ljava/lang/String;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 259
    invoke-virtual {p0}, Ljbu;->bfw()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljbu;->bfy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    :try_start_0
    invoke-virtual {p0}, Ljbu;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    .line 262
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 263
    invoke-virtual {v2, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 264
    invoke-virtual {p0}, Ljbu;->bfx()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 273
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    .line 266
    :catch_0
    move-exception v1

    const-string v1, "QpBillListEntryAdapter"

    const-string v2, "Wrong currency code."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 268
    :catch_1
    move-exception v1

    const-string v1, "QpBillListEntryAdapter"

    const-string v2, "Wrong currency formatting."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 270
    :catch_2
    move-exception v1

    const-string v1, "QpBillListEntryAdapter"

    const-string v2, "Wrong rounding mode."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    :cond_1
    invoke-virtual {p0}, Ljbu;->bfz()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Lfus;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 68
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 69
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lfut;->cCl:Lftx;

    invoke-virtual {v0, p1, p3}, Lftx;->j(Landroid/content/Context;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 3

    .prologue
    .line 243
    iget-object v0, p2, Lizj;->dTT:Lixh;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lfut;->a(Landroid/content/Context;Lixh;Z)Lane;

    move-result-object v0

    .line 245
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 246
    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    .line 247
    iput-object v0, v1, Lanh;->ahl:Lane;

    .line 248
    return-object v1
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 253
    const v0, 0x7f0a04c9

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v3, v3, Lizq;->dUX:[Lizj;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 12

    .prologue
    .line 90
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 91
    iget-object v0, p2, Lizj;->dTT:Lixh;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lfut;->a(Landroid/content/Context;Lixh;Z)Lane;

    move-result-object v0

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    iput-object v0, v1, Lanh;->ahl:Lane;

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v1, Lanh;->ahu:Lizj;

    invoke-static {v4, v1}, Lfut;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 92
    iget-object v5, p2, Lizj;->dTT:Lixh;

    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    const/16 v0, 0x10

    invoke-virtual {v2, v0}, Lanh;->cm(I)Lanh;

    new-instance v6, Laou;

    invoke-direct {v6}, Laou;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Lixh;->aZY()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v5}, Lixh;->aZY()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v5, Lixh;->dME:Lixn;

    if-eqz v0, :cond_7

    iget-object v0, v5, Lixh;->dME:Lixn;

    invoke-virtual {v0}, Lixn;->oK()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f0a04be

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v5, Lixh;->dME:Lixn;

    invoke-virtual {v9}, Lixn;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v3, v8

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v6, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    invoke-virtual {v6, v1}, Laou;->ct(Ljava/lang/String;)Laou;

    new-instance v0, Lfsa;

    const/16 v3, 0xb2

    invoke-direct {v0, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0200c0

    invoke-virtual {v0, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v2, Lanh;->ahs:Lani;

    :cond_0
    new-instance v8, Lfyg;

    invoke-direct {v8, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v5}, Lixh;->aZX()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    iget-object v9, v5, Lixh;->dNr:Ljbu;

    if-eqz v9, :cond_1

    iget-object v9, v5, Lixh;->dNr:Ljbu;

    invoke-static {v9}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    new-instance v1, Landroid/util/Pair;

    const v9, 0x7f0a04c2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, v5, Lixh;->dNr:Ljbu;

    invoke-static {v10}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v9, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iget-object v9, v5, Lixh;->dNs:Ljbu;

    if-eqz v9, :cond_b

    iget-object v9, v5, Lixh;->dNs:Ljbu;

    invoke-static {v9}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_b

    if-nez v1, :cond_9

    new-instance v1, Landroid/util/Pair;

    const v9, 0x7f0a04c3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, v5, Lixh;->dNs:Ljbu;

    invoke-static {v10}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v9, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v11, v3

    move-object v3, v1

    move-object v1, v11

    :goto_2
    if-eqz v0, :cond_2

    const v9, 0x7f0a04c1

    invoke-virtual {v5}, Lixh;->aZX()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v9, v5}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    :cond_2
    if-eqz v3, :cond_4

    if-nez v0, :cond_3

    const-string v0, ""

    const-string v5, ""

    const/4 v9, 0x1

    invoke-virtual {v8, v0, v5, v9}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    :cond_3
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v5, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    :cond_4
    invoke-virtual {v8}, Lfyg;->aDR()Laow;

    move-result-object v0

    iget-object v3, v0, Laow;->aks:[Laov;

    array-length v3, v3

    if-lez v3, :cond_5

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    if-eqz v1, :cond_6

    new-instance v0, Lfyg;

    invoke-direct {v0, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const-string v3, ""

    const-string v5, ""

    const/4 v8, 0x1

    invoke-virtual {v0, v3, v5, v8}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    move-result-object v3

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_3
    invoke-static {v4, v0}, Lfut;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 94
    const v0, 0x7f0a0383

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v2, p2, Lizj;->dTT:Lixh;

    iget-object v2, v2, Lixh;->dMX:[Ljbg;

    const/16 v3, 0xb3

    invoke-static {v0, v1, v2, v3}, Lfsx;->a(Ljava/lang/String;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    .line 96
    invoke-static {v4, v0}, Lfut;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 98
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    return-object v0

    :cond_7
    move-object v0, v1

    .line 92
    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_9
    new-instance v3, Landroid/util/Pair;

    const v9, 0x7f0a04c3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, v5, Lixh;->dNs:Ljbu;

    invoke-static {v10}, Lfut;->a(Ljbu;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v9, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v11, v3

    move-object v3, v1

    move-object v1, v11

    goto/16 :goto_2

    :cond_a
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Laow;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laow;

    iput-object v0, v6, Laou;->akm:[Laow;

    iput-object v6, v2, Lanh;->agN:Laou;

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v2, Lanh;->ahu:Lizj;

    move-object v0, v2

    goto :goto_3

    :cond_b
    move-object v11, v3

    move-object v3, v1

    move-object v1, v11

    goto/16 :goto_2
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lfut;->aAw()Lizj;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 82
    invoke-virtual {p0, p1, v0}, Lfut;->b(Landroid/content/Context;Lizj;)[Lanh;

    move-result-object v0

    return-object v0
.end method
