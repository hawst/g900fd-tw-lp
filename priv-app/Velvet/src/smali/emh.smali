.class public final Lemh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static aB(Landroid/os/Parcel;)Lcom/google/android/shared/util/BitFlags;
    .locals 4

    .prologue
    .line 309
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 311
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 312
    if-eqz v1, :cond_0

    .line 314
    :try_start_0
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;J)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    invoke-direct {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;-><init>(J)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    invoke-static {p1}, Lemh;->aB(Landroid/os/Parcel;)Lcom/google/android/shared/util/BitFlags;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    new-array v0, p1, [Lcom/google/android/shared/util/BitFlags;

    return-object v0
.end method
