.class public final Lhwu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bgq:Lchk;

.field private final dtD:Ljava/lang/String;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lchk;Lcke;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lhwu;->dtD:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lhwu;->bgq:Lchk;

    .line 27
    iput-object p3, p0, Lhwu;->mSearchSettings:Lcke;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Ljvi;Ljava/lang/String;)Ljava/util/List;
    .locals 8

    .prologue
    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 55
    iget-object v2, p1, Ljvi;->eHp:[Litu;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 56
    invoke-virtual {p0}, Lhwu;->aTl()Ljyt;

    move-result-object v5

    .line 58
    invoke-virtual {p1}, Ljvi;->xU()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljyt;->zW(Ljava/lang/String;)Ljyt;

    move-result-object v6

    invoke-virtual {p1}, Ljvi;->buW()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljyt;->zX(Ljava/lang/String;)Ljyt;

    move-result-object v6

    invoke-virtual {p1}, Ljvi;->buX()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljyt;->zY(Ljava/lang/String;)Ljyt;

    move-result-object v6

    invoke-virtual {p1}, Ljvi;->buY()I

    move-result v7

    invoke-virtual {v6, v7}, Ljyt;->tn(I)Ljyt;

    move-result-object v6

    invoke-virtual {p1}, Ljvi;->buZ()I

    move-result v7

    invoke-virtual {v6, v7}, Ljyt;->to(I)Ljyt;

    .line 63
    if-eqz p2, :cond_0

    .line 64
    invoke-virtual {v5, p2}, Ljyt;->zV(Ljava/lang/String;)Ljyt;

    .line 67
    :cond_0
    iput-object v4, v5, Ljyt;->eMt:Litu;

    .line 68
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    return-object v1
.end method

.method public final aTl()Ljyt;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljyt;

    invoke-direct {v0}, Ljyt;-><init>()V

    .line 37
    iget-object v1, p0, Lhwu;->dtD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljyt;->zU(Ljava/lang/String;)Ljyt;

    .line 38
    iget-object v1, p0, Lhwu;->bgq:Lchk;

    invoke-virtual {v1}, Lchk;->Ft()[I

    move-result-object v1

    iput-object v1, v0, Ljyt;->clientExperimentIds:[I

    .line 39
    iget-object v1, p0, Lhwu;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->OG()[I

    move-result-object v1

    iput-object v1, v0, Ljyt;->serverExperimentIds:[I

    .line 40
    return-object v0
.end method
