.class public Lhwv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static dtE:Ljava/lang/String;


# instance fields
.field private final bdX:I

.field private dtF:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile dtG:Lbur;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 43
    const-string v0, ""

    sput-object v0, Lhwv;->dtE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lhwv;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lhwv;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 62
    iput p3, p0, Lhwv;->bdX:I

    .line 63
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lhwv;->dtF:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 64
    sput-object p4, Lhwv;->dtE:Ljava/lang/String;

    .line 65
    return-void
.end method

.method static synthetic a(Lhwv;)Lbur;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/Executor;ILjava/lang/String;Z)Lhwv;
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 191
    if-eqz p4, :cond_0

    .line 192
    new-instance v0, Lhwv;

    invoke-direct {v0, p0, p1, v1, p3}, Lhwv;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;ILjava/lang/String;)V

    .line 194
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhwx;

    invoke-direct {v0, p0, p1, v1, p3}, Lhwx;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;ILjava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lhwv;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lhwv;->dtF:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method


# virtual methods
.method public final Cx()Lbur;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    return-object v0
.end method

.method public Y([B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    sget-object v0, Lemo;->cfz:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhwv;->dtG:Lbur;

    if-eqz v0, :cond_0

    .line 148
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 149
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    sget-object v1, Lhwv;->dtE:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Lbur;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    const-string v0, "ClearcutLogger"

    const-string v1, "playlogger connection FAILED, logEvent was dropped. "

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final aTm()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lhwv;->dtF:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final os(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 163
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    invoke-virtual {v0, p1}, Lbur;->fH(Ljava/lang/String;)Lbur;

    .line 168
    :cond_0
    return-void
.end method

.method public start()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 73
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    if-eqz v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 77
    :cond_0
    new-instance v5, Lemo;

    invoke-direct {v5}, Lemo;-><init>()V

    .line 78
    new-instance v0, Lbur;

    iget-object v1, p0, Lhwv;->mContext:Landroid/content/Context;

    iget v2, p0, Lhwv;->bdX:I

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lbur;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lbus;Z)V

    iput-object v0, p0, Lhwv;->dtG:Lbur;

    .line 84
    iget-object v0, p0, Lhwv;->dtF:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 93
    iget-object v0, p0, Lhwv;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhww;

    const-string v2, "Call PlayLogger.start()"

    new-array v3, v6, [I

    invoke-direct {v1, p0, v2, v3}, Lhww;-><init>(Lhwv;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lhwv;->dtF:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwv;->dtG:Lbur;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    iget-object v0, v0, Lbur;->aGS:Lbpd;

    invoke-virtual {v0}, Lbpd;->stop()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lhwv;->dtG:Lbur;

    .line 139
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lhwv;->dtG:Lbur;

    goto :goto_0
.end method
