.class public final enum Laig;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum YR:Laig;

.field public static final enum YS:Laig;

.field public static final enum YT:Laig;

.field private static final synthetic YU:[Laig;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163
    new-instance v0, Laig;

    const-string v1, "NOT_LOADED"

    invoke-direct {v0, v1, v2}, Laig;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laig;->YR:Laig;

    new-instance v0, Laig;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v3}, Laig;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laig;->YS:Laig;

    new-instance v0, Laig;

    const-string v1, "ERROR_LOADING"

    invoke-direct {v0, v1, v4}, Laig;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laig;->YT:Laig;

    const/4 v0, 0x3

    new-array v0, v0, [Laig;

    sget-object v1, Laig;->YR:Laig;

    aput-object v1, v0, v2

    sget-object v1, Laig;->YS:Laig;

    aput-object v1, v0, v3

    sget-object v1, Laig;->YT:Laig;

    aput-object v1, v0, v4

    sput-object v0, Laig;->YU:[Laig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laig;
    .locals 1

    .prologue
    .line 163
    const-class v0, Laig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laig;

    return-object v0
.end method

.method public static values()[Laig;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Laig;->YU:[Laig;

    invoke-virtual {v0}, [Laig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laig;

    return-object v0
.end method
