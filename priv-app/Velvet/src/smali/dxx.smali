.class public final Ldxx;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lier;)Laiw;
    .locals 8
    .param p0    # Lier;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x5

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 60
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lier;->aVI()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v0, v2

    .line 123
    :cond_1
    :goto_0
    return-object v0

    .line 63
    :cond_2
    new-instance v1, Laiw;

    invoke-direct {v1}, Laiw;-><init>()V

    .line 64
    invoke-virtual {p0}, Lier;->getFrequency()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 109
    const-string v1, "RecurrenceHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The frequency "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lier;->getFrequency()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not supported."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v3, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v2

    .line 112
    :goto_1
    invoke-virtual {p0}, Lier;->aVK()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {p0}, Lier;->aVJ()I

    move-result v1

    iput v1, v0, Laiw;->ZW:I

    goto :goto_0

    .line 66
    :pswitch_0
    const/4 v0, 0x4

    iput v0, v1, Laiw;->ZU:I

    move-object v0, v1

    .line 67
    goto :goto_1

    .line 69
    :pswitch_1
    iput v7, v1, Laiw;->ZU:I

    .line 70
    if-eqz p0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lier;->getFrequency()I

    move-result v2

    if-eq v2, v5, :cond_4

    :cond_3
    const-string v2, "RecurrenceHelper"

    const-string v3, "The recurrence is not a valid WEEKLY recurrence."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lier;->dAI:Liew;

    iget-object v2, v2, Liew;->dAZ:[I

    array-length v2, v2

    if-nez v2, :cond_5

    const-string v2, "RecurrenceHelper"

    const-string v3, "No weekDay in the recurrence: %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0}, Leqh;->d(Ljsr;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lier;->dAI:Liew;

    iget-object v2, v2, Liew;->dAZ:[I

    array-length v2, v2

    iput v2, v1, Laiw;->aag:I

    iget v2, v1, Laiw;->aag:I

    new-array v2, v2, [I

    iput-object v2, v1, Laiw;->aae:[I

    iget v2, v1, Laiw;->aag:I

    new-array v2, v2, [I

    iput-object v2, v1, Laiw;->aaf:[I

    :goto_2
    iget-object v2, p0, Lier;->dAI:Liew;

    iget-object v2, v2, Liew;->dAZ:[I

    array-length v2, v2

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lier;->dAI:Liew;

    iget-object v2, v2, Liew;->dAZ:[I

    aget v2, v2, v0

    invoke-static {v2}, Ldxx;->gS(I)I

    move-result v2

    if-lez v2, :cond_6

    iget-object v3, v1, Laiw;->aae:[I

    aput v2, v3, v0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move-object v0, v1

    .line 71
    goto :goto_1

    .line 73
    :pswitch_2
    const/4 v2, 0x6

    iput v2, v1, Laiw;->ZU:I

    .line 74
    iget-object v2, p0, Lier;->dAJ:Liet;

    .line 75
    if-nez v2, :cond_8

    .line 76
    const-string v2, "RecurrenceHelper"

    const-string v3, "monthlyPattern shouldn\'t be NULL."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 77
    goto/16 :goto_1

    .line 79
    :cond_8
    invoke-virtual {v2}, Liet;->aVO()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 80
    invoke-virtual {v2}, Liet;->aVN()I

    move-result v3

    invoke-static {v3}, Ldxx;->gS(I)I

    move-result v3

    .line 82
    if-lez v3, :cond_e

    .line 83
    iput v5, v1, Laiw;->aag:I

    .line 84
    new-array v4, v5, [I

    aput v3, v4, v0

    iput-object v4, v1, Laiw;->aae:[I

    .line 88
    invoke-virtual {v2}, Liet;->aVR()Z

    move-result v3

    if-nez v3, :cond_9

    invoke-virtual {v2}, Liet;->aVP()I

    move-result v3

    if-ne v3, v7, :cond_a

    .line 89
    :cond_9
    new-array v2, v5, [I

    aput v6, v2, v0

    iput-object v2, v1, Laiw;->aaf:[I

    move-object v0, v1

    goto/16 :goto_1

    .line 90
    :cond_a
    invoke-virtual {v2}, Liet;->aVP()I

    move-result v3

    if-gt v3, v7, :cond_b

    invoke-virtual {v2}, Liet;->aVP()I

    move-result v3

    if-gtz v3, :cond_c

    .line 92
    :cond_b
    iput v0, v1, Laiw;->aag:I

    .line 93
    const-string v3, "RecurrenceHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid weekdayNumber: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Liet;->aVP()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 94
    goto/16 :goto_1

    .line 96
    :cond_c
    iput v5, v1, Laiw;->aag:I

    .line 97
    new-array v3, v5, [I

    invoke-virtual {v2}, Liet;->aVP()I

    move-result v2

    aput v2, v3, v0

    iput-object v3, v1, Laiw;->aaf:[I

    move-object v0, v1

    .line 99
    goto/16 :goto_1

    :cond_d
    iget-object v3, v2, Liet;->dAN:[I

    array-length v3, v3

    if-lez v3, :cond_e

    iget-object v2, v2, Liet;->dAN:[I

    aget v2, v2, v0

    if-ne v2, v6, :cond_e

    .line 101
    iput v5, v1, Laiw;->aai:I

    .line 102
    new-array v2, v5, [I

    aput v6, v2, v0

    iput-object v2, v1, Laiw;->aah:[I

    move-object v0, v1

    goto/16 :goto_1

    .line 106
    :pswitch_3
    const/4 v0, 0x7

    iput v0, v1, Laiw;->ZU:I

    move-object v0, v1

    .line 107
    goto/16 :goto_1

    :cond_e
    move-object v0, v1

    goto/16 :goto_1

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Laiw;J)Landroid/text/format/Time;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 669
    if-eqz p0, :cond_0

    iget v0, p0, Laiw;->ZU:I

    if-eq v0, v3, :cond_1

    :cond_0
    move-object v0, v4

    .line 695
    :goto_0
    return-object v0

    .line 673
    :cond_1
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    .line 674
    :goto_1
    iget v2, p0, Laiw;->aag:I

    if-ge v0, v2, :cond_2

    .line 675
    iget-object v2, p0, Laiw;->aae:[I

    aget v2, v2, v0

    sparse-switch v2, :sswitch_data_0

    const/4 v2, -0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 674
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 675
    :sswitch_0
    const/4 v2, 0x1

    goto :goto_2

    :sswitch_1
    const/4 v2, 0x2

    goto :goto_2

    :sswitch_2
    const/4 v2, 0x3

    goto :goto_2

    :sswitch_3
    const/4 v2, 0x4

    goto :goto_2

    :sswitch_4
    move v2, v3

    goto :goto_2

    :sswitch_5
    const/4 v2, 0x6

    goto :goto_2

    :sswitch_6
    move v2, v1

    goto :goto_2

    .line 679
    :cond_2
    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v4

    .line 680
    goto :goto_0

    .line 686
    :cond_3
    add-int/lit8 v1, v1, 0x1

    :cond_4
    const/4 v0, 0x7

    if-ge v1, v0, :cond_5

    .line 687
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 688
    int-to-long v2, v1

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v2, v6

    add-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 689
    iget v2, v0, Landroid/text/format/Time;->weekDay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_5
    move-object v0, v4

    .line 695
    goto :goto_0

    .line 675
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_6
        0x20000 -> :sswitch_0
        0x40000 -> :sswitch_1
        0x80000 -> :sswitch_2
        0x100000 -> :sswitch_3
        0x200000 -> :sswitch_4
        0x400000 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(Laiw;JLdyf;)Lier;
    .locals 11
    .param p0    # Laiw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ldyf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x5

    const/4 v8, -0x1

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    if-nez p0, :cond_0

    move-object v0, v3

    .line 492
    :goto_0
    return-object v0

    .line 375
    :cond_0
    new-instance v4, Lier;

    invoke-direct {v4}, Lier;-><init>()V

    .line 377
    iget v0, p0, Laiw;->ZW:I

    if-eqz v0, :cond_1

    .line 378
    iget v0, p0, Laiw;->ZW:I

    invoke-virtual {v4, v0}, Lier;->lT(I)Lier;

    .line 382
    :cond_1
    new-instance v0, Liev;

    invoke-direct {v0}, Liev;-><init>()V

    .line 385
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 386
    invoke-virtual {v5, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 387
    new-instance v6, Lieo;

    invoke-direct {v6}, Lieo;-><init>()V

    .line 388
    invoke-virtual {v5, v1}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lieo;->lP(I)Lieo;

    .line 390
    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Lieo;->lQ(I)Lieo;

    .line 391
    invoke-virtual {v5, v9}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v6, v5}, Lieo;->lR(I)Lieo;

    .line 392
    iput-object v6, v0, Liev;->dAX:Lieo;

    .line 393
    iput-object v0, v4, Lier;->dAF:Liev;

    .line 395
    new-instance v5, Lieu;

    invoke-direct {v5}, Lieu;-><init>()V

    .line 397
    iget v0, p0, Laiw;->count:I

    if-eqz v0, :cond_13

    .line 398
    iget v0, p0, Laiw;->count:I

    invoke-virtual {v5, v0}, Lieu;->lX(I)Lieu;

    move v0, v1

    .line 401
    :goto_1
    iget-object v6, p0, Laiw;->ZV:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 402
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 403
    iget-object v6, p0, Laiw;->ZV:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 404
    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lieu;->bP(J)Lieu;

    move v0, v1

    .line 411
    :cond_2
    if-eqz v0, :cond_3

    .line 412
    iput-object v5, v4, Lier;->dAG:Lieu;

    .line 415
    :cond_3
    new-instance v0, Lies;

    invoke-direct {v0}, Lies;-><init>()V

    if-eqz p3, :cond_6

    iget v5, p3, Ldyf;->bQI:I

    packed-switch v5, :pswitch_data_0

    const-string v0, "RecurrenceHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported symbolicTime "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for DAILY reminders."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v9, v0, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 416
    :goto_2
    if-eqz v0, :cond_4

    .line 417
    iput-object v0, v4, Lier;->dAH:Lies;

    .line 419
    :cond_4
    iget v0, p0, Laiw;->ZU:I

    const/4 v5, 0x4

    if-ne v0, v5, :cond_7

    .line 420
    invoke-virtual {v4, v2}, Lier;->lS(I)Lier;

    :cond_5
    :goto_3
    move-object v0, v4

    .line 492
    goto/16 :goto_0

    .line 415
    :pswitch_0
    invoke-virtual {v0, v1}, Lies;->lU(I)Lies;

    goto :goto_2

    :pswitch_1
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Lies;->lU(I)Lies;

    goto :goto_2

    :pswitch_2
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Lies;->lU(I)Lies;

    goto :goto_2

    :pswitch_3
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Lies;->lU(I)Lies;

    goto :goto_2

    :pswitch_4
    move-object v0, v3

    goto :goto_2

    :cond_6
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    new-instance v6, Liep;

    invoke-direct {v6}, Liep;-><init>()V

    const/16 v7, 0xb

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    iput v7, v6, Liep;->hour:I

    const/16 v7, 0xc

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    iput v7, v6, Liep;->minute:I

    const/16 v7, 0xd

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iput v5, v6, Liep;->second:I

    iput-object v6, v0, Lies;->dAL:Liep;

    goto :goto_2

    .line 421
    :cond_7
    iget v0, p0, Laiw;->ZU:I

    if-ne v0, v9, :cond_a

    .line 422
    invoke-virtual {v4, v1}, Lier;->lS(I)Lier;

    .line 423
    new-instance v1, Liew;

    invoke-direct {v1}, Liew;-><init>()V

    .line 425
    iget v0, p0, Laiw;->aag:I

    new-array v0, v0, [I

    iput-object v0, v1, Liew;->dAZ:[I

    move v0, v2

    .line 426
    :goto_4
    iget v5, p0, Laiw;->aag:I

    if-ge v0, v5, :cond_9

    .line 427
    iget-object v5, p0, Laiw;->aae:[I

    aget v5, v5, v0

    .line 428
    invoke-static {v5}, Ldxx;->gQ(I)I

    move-result v6

    .line 429
    if-ne v6, v8, :cond_8

    .line 430
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Failed to convert byday: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " to caribou Weekday."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 431
    goto/16 :goto_0

    .line 433
    :cond_8
    iget-object v5, v1, Liew;->dAZ:[I

    aput v6, v5, v0

    .line 426
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 436
    :cond_9
    iget-object v0, v1, Liew;->dAZ:[I

    array-length v0, v0

    if-eqz v0, :cond_5

    .line 437
    iput-object v1, v4, Lier;->dAI:Liew;

    goto/16 :goto_3

    .line 439
    :cond_a
    iget v0, p0, Laiw;->ZU:I

    const/4 v5, 0x6

    if-ne v0, v5, :cond_11

    .line 440
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Lier;->lS(I)Lier;

    .line 441
    new-instance v0, Liet;

    invoke-direct {v0}, Liet;-><init>()V

    .line 442
    iget v5, p0, Laiw;->aag:I

    if-lez v5, :cond_f

    .line 444
    iget-object v5, p0, Laiw;->aae:[I

    if-eqz v5, :cond_b

    iget-object v5, p0, Laiw;->aaf:[I

    if-nez v5, :cond_c

    .line 445
    :cond_b
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "byday and bydayNum can\'t be NULL for monthly reminder, eventRecurrence: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Laiw;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 447
    goto/16 :goto_0

    .line 450
    :cond_c
    iget-object v5, p0, Laiw;->aae:[I

    aget v5, v5, v2

    .line 451
    invoke-static {v5}, Ldxx;->gQ(I)I

    move-result v6

    .line 452
    if-ne v6, v8, :cond_d

    .line 453
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Failed to convert byday: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " to caribou Weekday."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 454
    goto/16 :goto_0

    .line 456
    :cond_d
    iget-object v3, p0, Laiw;->aaf:[I

    aget v3, v3, v2

    if-ne v3, v8, :cond_e

    .line 458
    invoke-virtual {v0, v1}, Liet;->gW(Z)Liet;

    .line 462
    :goto_5
    invoke-virtual {v0, v6}, Liet;->lV(I)Liet;

    .line 472
    :goto_6
    iput-object v0, v4, Lier;->dAJ:Liet;

    goto/16 :goto_3

    .line 460
    :cond_e
    iget-object v1, p0, Laiw;->aaf:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Liet;->lW(I)Liet;

    goto :goto_5

    .line 463
    :cond_f
    iget v3, p0, Laiw;->aai:I

    if-lez v3, :cond_10

    iget-object v3, p0, Laiw;->aah:[I

    aget v3, v3, v2

    if-ne v3, v8, :cond_10

    .line 465
    new-array v1, v1, [I

    aput v8, v1, v2

    iput-object v1, v0, Liet;->dAN:[I

    goto :goto_6

    .line 468
    :cond_10
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 469
    invoke-virtual {v3, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 470
    new-array v1, v1, [I

    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, v1, v2

    iput-object v1, v0, Liet;->dAN:[I

    goto :goto_6

    .line 473
    :cond_11
    iget v0, p0, Laiw;->ZU:I

    const/4 v5, 0x7

    if-ne v0, v5, :cond_12

    .line 474
    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Lier;->lS(I)Lier;

    .line 475
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 476
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 477
    new-instance v3, Liet;

    invoke-direct {v3}, Liet;-><init>()V

    .line 479
    new-array v5, v1, [I

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    aput v6, v5, v2

    iput-object v5, v3, Liet;->dAN:[I

    .line 481
    new-instance v5, Liex;

    invoke-direct {v5}, Liex;-><init>()V

    .line 482
    iput-object v3, v5, Liex;->dAJ:Liet;

    .line 485
    new-array v1, v1, [I

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    aput v0, v1, v2

    iput-object v1, v5, Liex;->dBa:[I

    .line 486
    iput-object v5, v4, Lier;->dAK:Liex;

    goto/16 :goto_3

    .line 488
    :cond_12
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "The frequency "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Laiw;->ZU:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is not supported."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 489
    goto/16 :goto_0

    :cond_13
    move v0, v2

    goto/16 :goto_1

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(JLier;)Ljkr;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 537
    if-nez p2, :cond_0

    .line 614
    :goto_0
    return-object v0

    .line 541
    :cond_0
    new-instance v1, Ljkr;

    invoke-direct {v1}, Ljkr;-><init>()V

    .line 544
    iget-object v2, p2, Lier;->dAF:Liev;

    if-eqz v2, :cond_1

    .line 545
    iget-object v2, p2, Lier;->dAF:Liev;

    .line 546
    invoke-virtual {v2}, Liev;->aVU()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 547
    invoke-virtual {v2}, Liev;->aVT()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljkr;->dz(J)Ljkr;

    .line 563
    :cond_1
    :goto_1
    iget-object v2, p2, Lier;->dAH:Lies;

    if-nez v2, :cond_4

    .line 565
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Ljkr;->qw(I)Ljkr;

    .line 610
    :goto_2
    invoke-virtual {v1}, Ljkr;->bom()Z

    move-result v0

    if-nez v0, :cond_2

    .line 611
    invoke-virtual {v1, p0, p1}, Ljkr;->dz(J)Ljkr;

    :cond_2
    move-object v0, v1

    .line 614
    goto :goto_0

    .line 548
    :cond_3
    iget-object v3, v2, Liev;->dAX:Lieo;

    if-eqz v3, :cond_1

    .line 549
    iget-object v2, v2, Liev;->dAX:Lieo;

    .line 554
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 555
    invoke-virtual {v2}, Lieo;->getYear()I

    move-result v4

    invoke-virtual {v2}, Lieo;->getMonth()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2}, Lieo;->getDay()I

    move-result v2

    invoke-virtual {v3, v4, v5, v2}, Ljava/util/Calendar;->set(III)V

    .line 557
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljkr;->dz(J)Ljkr;

    goto :goto_1

    .line 566
    :cond_4
    iget-object v2, p2, Lier;->dAH:Lies;

    invoke-virtual {v2}, Lies;->aVM()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 568
    iget-object v0, p2, Lier;->dAH:Lies;

    invoke-virtual {v0}, Lies;->aVL()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 586
    const-string v0, "RecurrenceHelper"

    const-string v2, "Invalid day_period from daily_pattern: %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p2, Lier;->dAH:Lies;

    invoke-static {v4}, Leqh;->d(Ljsr;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v8, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 570
    :pswitch_0
    invoke-virtual {v1, v6}, Ljkr;->qw(I)Ljkr;

    goto :goto_2

    .line 574
    :pswitch_1
    invoke-virtual {v1, v7}, Ljkr;->qw(I)Ljkr;

    goto :goto_2

    .line 578
    :pswitch_2
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljkr;->qw(I)Ljkr;

    goto :goto_2

    .line 582
    :pswitch_3
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Ljkr;->qw(I)Ljkr;

    goto :goto_2

    .line 590
    :cond_5
    iget-object v2, p2, Lier;->dAH:Lies;

    iget-object v2, v2, Lies;->dAL:Liep;

    if-eqz v2, :cond_7

    .line 591
    invoke-virtual {v1}, Ljkr;->bom()Z

    move-result v2

    if-nez v2, :cond_6

    .line 592
    const-string v1, "RecurrenceHelper"

    const-string v2, "The time_ms should have been populated."

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v8, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 595
    :cond_6
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 596
    invoke-virtual {v1}, Ljkr;->Pu()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 598
    iget-object v2, p2, Lier;->dAH:Lies;

    iget-object v2, v2, Lies;->dAL:Liep;

    .line 599
    const/16 v3, 0xb

    iget v4, v2, Liep;->hour:I

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 600
    const/16 v3, 0xc

    iget v4, v2, Liep;->minute:I

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 601
    const/16 v3, 0xd

    iget v2, v2, Liep;->second:I

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 602
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljkr;->dz(J)Ljkr;

    goto/16 :goto_2

    .line 604
    :cond_7
    const-string v1, "RecurrenceHelper"

    const-string v2, "The DailyPattern should have day_period or time_of_day field. DailyPattern: %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p2, Lier;->dAH:Lies;

    invoke-static {v4}, Leqh;->d(Ljsr;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v8, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljdy;Lcom/google/android/search/shared/actions/SetReminderAction;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 164
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    invoke-virtual {p0}, Ljdy;->big()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    invoke-virtual {p0}, Ljdy;->ait()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->kn(Ljava/lang/String;)V

    .line 171
    :cond_2
    new-instance v0, Laiw;

    invoke-direct {v0}, Laiw;-><init>()V

    .line 172
    invoke-virtual {p0}, Ljdy;->aVK()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    invoke-virtual {p0}, Ljdy;->aVJ()I

    move-result v1

    iput v1, v0, Laiw;->ZW:I

    .line 175
    :cond_3
    invoke-virtual {p0}, Ljdy;->aVU()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 180
    invoke-virtual {p0}, Ljdy;->aVT()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->ax(J)V

    .line 182
    :cond_4
    invoke-virtual {p0}, Ljdy;->bii()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 183
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 184
    invoke-virtual {p0}, Ljdy;->bih()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 185
    const-string v2, "UTC"

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v1, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 187
    invoke-virtual {v1}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Laiw;->ZV:Ljava/lang/String;

    .line 189
    :cond_5
    invoke-virtual {p0}, Ljdy;->bik()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 190
    invoke-virtual {p0}, Ljdy;->bij()I

    move-result v1

    iput v1, v0, Laiw;->count:I

    .line 194
    :cond_6
    iget-object v1, p0, Ljdy;->edi:Ljdz;

    invoke-static {v1, p1}, Ldxx;->a(Ljdz;Lcom/google/android/search/shared/actions/SetReminderAction;)V

    .line 195
    invoke-virtual {p0}, Ljdy;->getFrequency()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 212
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid ReminderEntry Frequency: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljdy;->getFrequency()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 197
    :pswitch_0
    const/4 v1, 0x4

    iput v1, v0, Laiw;->ZU:I

    .line 216
    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->b(Laiw;)V

    goto/16 :goto_0

    .line 200
    :pswitch_1
    const/4 v1, 0x5

    iput v1, v0, Laiw;->ZU:I

    .line 201
    iget-object v1, p0, Ljdy;->edj:Ljeb;

    invoke-static {v1, v0}, Ldxx;->a(Ljeb;Laiw;)V

    goto :goto_1

    .line 204
    :pswitch_2
    const/4 v1, 0x6

    iput v1, v0, Laiw;->ZU:I

    .line 205
    iget-object v1, p0, Ljdy;->edk:Ljea;

    invoke-static {v1, v0}, Ldxx;->a(Ljea;Laiw;)V

    goto :goto_1

    .line 208
    :pswitch_3
    const/4 v1, 0x7

    iput v1, v0, Laiw;->ZU:I

    .line 209
    iget-object v1, p0, Ljdy;->edl:Ljec;

    invoke-static {v1, v0}, Ldxx;->a(Ljec;Laiw;)V

    goto :goto_1

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljdz;Lcom/google/android/search/shared/actions/SetReminderAction;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 226
    if-nez p0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-virtual {p0}, Ljdz;->bie()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    invoke-virtual {p0}, Ljdz;->bid()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 245
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported day part: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljdz;->bid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v5, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 233
    :pswitch_0
    sget-object v0, Ldyf;->bQC:Ldyf;

    .line 250
    :goto_1
    sget-object v1, Ldxw;->bPT:[Ldyf;

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->b(Ldyf;Ljava/util/List;)V

    goto :goto_0

    .line 236
    :pswitch_1
    sget-object v0, Ldyf;->bQD:Ldyf;

    goto :goto_1

    .line 239
    :pswitch_2
    sget-object v0, Ldyf;->bQE:Ldyf;

    goto :goto_1

    .line 242
    :pswitch_3
    sget-object v0, Ldyf;->bQF:Ldyf;

    goto :goto_1

    .line 252
    :cond_1
    invoke-virtual {p0}, Ljdz;->bil()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljdz;->bim()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 253
    invoke-virtual {p0}, Ljdz;->getHour()I

    move-result v0

    invoke-virtual {p0}, Ljdz;->getMinute()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->p(II)V

    goto :goto_0

    .line 255
    :cond_2
    const-string v0, "RecurrenceHelper"

    const-string v1, "Invalid daily pattern: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Leqh;->d(Ljsr;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v5, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljea;Laiw;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 310
    if-nez p1, :cond_0

    .line 311
    const-string v1, "RecurrenceHelper"

    const-string v2, "No eventRecurrence to update"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 313
    :cond_0
    if-nez p0, :cond_1

    .line 314
    const-string v1, "RecurrenceHelper"

    const-string v2, "Missing monthly pattern"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 338
    :goto_0
    return-void

    .line 317
    :cond_1
    invoke-virtual {p0}, Ljea;->aVO()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 318
    invoke-virtual {p0}, Ljea;->aVR()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ljea;->aVP()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 319
    :cond_2
    new-array v1, v5, [I

    const/4 v2, -0x1

    aput v2, v1, v0

    iput-object v1, p1, Laiw;->aaf:[I

    .line 327
    :goto_1
    invoke-virtual {p0}, Ljea;->aVN()I

    move-result v1

    invoke-static {v1}, Ldxx;->gP(I)I

    move-result v1

    .line 329
    iput v5, p1, Laiw;->aag:I

    .line 330
    new-array v2, v5, [I

    aput v1, v2, v0

    iput-object v2, p1, Laiw;->aae:[I

    goto :goto_0

    .line 320
    :cond_3
    invoke-virtual {p0}, Ljea;->aVP()I

    move-result v1

    if-gt v1, v4, :cond_4

    invoke-virtual {p0}, Ljea;->aVP()I

    move-result v1

    if-gtz v1, :cond_5

    .line 322
    :cond_4
    const-string v1, "RecurrenceHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad weekday number: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljea;->aVP()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 325
    :cond_5
    new-array v1, v5, [I

    invoke-virtual {p0}, Ljea;->aVP()I

    move-result v2

    aput v2, v1, v0

    iput-object v1, p1, Laiw;->aaf:[I

    goto :goto_1

    .line 332
    :cond_6
    iget-object v1, p0, Ljea;->dAN:[I

    array-length v1, v1

    new-array v1, v1, [I

    iput-object v1, p1, Laiw;->aah:[I

    .line 333
    :goto_2
    iget-object v1, p0, Ljea;->dAN:[I

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 334
    iget-object v1, p1, Laiw;->aah:[I

    iget-object v2, p0, Ljea;->dAN:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 336
    :cond_7
    iget-object v0, p1, Laiw;->aah:[I

    array-length v0, v0

    iput v0, p1, Laiw;->aai:I

    goto :goto_0
.end method

.method public static a(Ljeb;Laiw;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 263
    if-nez p1, :cond_0

    .line 264
    const-string v1, "RecurrenceHelper"

    const-string v2, "No eventRecurrence to update"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 266
    :cond_0
    if-nez p0, :cond_2

    .line 267
    const-string v1, "RecurrenceHelper"

    const-string v2, "Missing weekly pattern"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 283
    :cond_1
    return-void

    .line 270
    :cond_2
    iget-object v1, p0, Ljeb;->dAZ:[I

    array-length v1, v1

    iput v1, p1, Laiw;->aag:I

    .line 271
    iget v1, p1, Laiw;->aag:I

    new-array v1, v1, [I

    iput-object v1, p1, Laiw;->aae:[I

    .line 275
    iget v1, p1, Laiw;->aag:I

    new-array v1, v1, [I

    iput-object v1, p1, Laiw;->aaf:[I

    .line 276
    :goto_0
    iget v1, p1, Laiw;->aag:I

    if-ge v0, v1, :cond_1

    .line 277
    iget-object v1, p0, Ljeb;->dAZ:[I

    aget v1, v1, v0

    invoke-static {v1}, Ldxx;->gP(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 279
    if-eqz v1, :cond_3

    .line 280
    iget-object v2, p1, Laiw;->aae:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v2, v0

    .line 276
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljec;Laiw;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 343
    if-nez p1, :cond_0

    .line 344
    const-string v1, "RecurrenceHelper"

    const-string v2, "No eventRecurrence to update"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 346
    :cond_0
    if-nez p0, :cond_2

    .line 347
    const-string v1, "RecurrenceHelper"

    const-string v2, "Missing yearly pattern"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 360
    :cond_1
    return-void

    .line 350
    :cond_2
    iget-object v1, p0, Ljec;->edk:Ljea;

    if-eqz v1, :cond_3

    .line 351
    iget-object v1, p0, Ljec;->edk:Ljea;

    invoke-static {v1, p1}, Ldxx;->a(Ljea;Laiw;)V

    .line 353
    :cond_3
    iget-object v1, p0, Ljec;->dBa:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 354
    iget-object v1, p0, Ljec;->dBa:[I

    array-length v1, v1

    iput v1, p1, Laiw;->aao:I

    .line 355
    iget v1, p1, Laiw;->aao:I

    new-array v1, v1, [I

    iput-object v1, p1, Laiw;->aan:[I

    .line 356
    :goto_0
    iget v1, p1, Laiw;->aao:I

    if-ge v0, v1, :cond_1

    .line 357
    iget-object v1, p1, Laiw;->aan:[I

    iget-object v2, p0, Ljec;->dBa:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(JLaiw;)Z
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 733
    if-eqz p2, :cond_0

    iget v0, p2, Laiw;->ZU:I

    const/4 v3, 0x6

    if-eq v0, v3, :cond_1

    :cond_0
    move v0, v1

    .line 765
    :goto_0
    return v0

    .line 737
    :cond_1
    iget v0, p2, Laiw;->aag:I

    if-ne v0, v2, :cond_2

    iget-object v0, p2, Laiw;->aae:[I

    if-eqz v0, :cond_2

    iget-object v0, p2, Laiw;->aaf:[I

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 741
    goto :goto_0

    .line 744
    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 745
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 747
    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ldxx;->gR(I)I

    move-result v3

    .line 749
    if-gez v3, :cond_4

    .line 750
    const-string v0, "RecurrenceHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Failed to convert Calendar weekday:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to EventRecurrence.Weekday."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 752
    goto :goto_0

    .line 755
    :cond_4
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 756
    if-lez v0, :cond_5

    if-le v0, v5, :cond_6

    .line 757
    :cond_5
    const-string v2, "RecurrenceHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "weekdayNumber should be in range [1,5] but was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 758
    goto :goto_0

    .line 762
    :cond_6
    iget-object v4, p2, Laiw;->aae:[I

    aput v3, v4, v1

    .line 763
    iget-object v3, p2, Laiw;->aaf:[I

    if-ne v0, v5, :cond_7

    const/4 v0, -0x1

    :cond_7
    aput v0, v3, v1

    move v0, v2

    .line 765
    goto :goto_0
.end method

.method public static b(JLaiw;)Z
    .locals 6

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 778
    if-eqz p2, :cond_0

    iget v2, p2, Laiw;->ZU:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    .line 796
    :cond_0
    :goto_0
    return v0

    .line 782
    :cond_1
    iget v2, p2, Laiw;->aai:I

    if-ne v2, v1, :cond_0

    iget-object v2, p2, Laiw;->aah:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 788
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 789
    invoke-virtual {v2, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 790
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 791
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 795
    iget-object v2, p2, Laiw;->aah:[I

    aput v3, v2, v0

    move v0, v1

    .line 796
    goto :goto_0
.end method

.method private static gP(I)I
    .locals 3

    .prologue
    .line 287
    packed-switch p0, :pswitch_data_0

    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid weekday: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289
    :pswitch_0
    const/high16 v0, 0x20000

    .line 301
    :goto_0
    return v0

    .line 291
    :pswitch_1
    const/high16 v0, 0x40000

    goto :goto_0

    .line 293
    :pswitch_2
    const/high16 v0, 0x80000

    goto :goto_0

    .line 295
    :pswitch_3
    const/high16 v0, 0x100000

    goto :goto_0

    .line 297
    :pswitch_4
    const/high16 v0, 0x200000

    goto :goto_0

    .line 299
    :pswitch_5
    const/high16 v0, 0x400000

    goto :goto_0

    .line 301
    :pswitch_6
    const/high16 v0, 0x10000

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static gQ(I)I
    .locals 3

    .prologue
    .line 500
    sparse-switch p0, :sswitch_data_0

    .line 516
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The day "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 517
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 502
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 504
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 506
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 508
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 510
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 512
    :sswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 514
    :sswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 500
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_6
        0x20000 -> :sswitch_0
        0x40000 -> :sswitch_1
        0x80000 -> :sswitch_2
        0x100000 -> :sswitch_3
        0x200000 -> :sswitch_4
        0x400000 -> :sswitch_5
    .end sparse-switch
.end method

.method public static gR(I)I
    .locals 1

    .prologue
    .line 806
    packed-switch p0, :pswitch_data_0

    .line 822
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 808
    :pswitch_0
    const/high16 v0, 0x20000

    goto :goto_0

    .line 810
    :pswitch_1
    const/high16 v0, 0x40000

    goto :goto_0

    .line 812
    :pswitch_2
    const/high16 v0, 0x80000

    goto :goto_0

    .line 814
    :pswitch_3
    const/high16 v0, 0x100000

    goto :goto_0

    .line 816
    :pswitch_4
    const/high16 v0, 0x200000

    goto :goto_0

    .line 818
    :pswitch_5
    const/high16 v0, 0x400000

    goto :goto_0

    .line 820
    :pswitch_6
    const/high16 v0, 0x10000

    goto :goto_0

    .line 806
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static gS(I)I
    .locals 3

    .prologue
    .line 830
    packed-switch p0, :pswitch_data_0

    .line 846
    const-string v0, "RecurrenceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid weekday: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 847
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 832
    :pswitch_0
    const/high16 v0, 0x20000

    goto :goto_0

    .line 834
    :pswitch_1
    const/high16 v0, 0x40000

    goto :goto_0

    .line 836
    :pswitch_2
    const/high16 v0, 0x80000

    goto :goto_0

    .line 838
    :pswitch_3
    const/high16 v0, 0x100000

    goto :goto_0

    .line 840
    :pswitch_4
    const/high16 v0, 0x200000

    goto :goto_0

    .line 842
    :pswitch_5
    const/high16 v0, 0x400000

    goto :goto_0

    .line 844
    :pswitch_6
    const/high16 v0, 0x10000

    goto :goto_0

    .line 830
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
