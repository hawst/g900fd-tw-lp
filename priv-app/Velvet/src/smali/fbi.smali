.class final Lfbi;
.super Lfbk;
.source "PG"


# instance fields
.field final cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

.field private final cnS:Z

.field final cnT:Ljava/util/List;

.field cnU:Z

.field final mFreshenRequestManager:Lfil;


# direct methods
.method private constructor <init>(Lfil;Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)V
    .locals 1
    .param p2    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-direct {p0}, Lfbk;-><init>()V

    .line 60
    iput-object p1, p0, Lfbi;->mFreshenRequestManager:Lfil;

    .line 61
    invoke-static {p2, p3, p4, p5}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->b(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;)Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    move-result-object v0

    iput-object v0, p0, Lfbi;->cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    .line 63
    iput-boolean p6, p0, Lfbi;->cnS:Z

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfbi;->cnT:Ljava/util/List;

    .line 65
    return-void
.end method

.method static a(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)Lfbi;
    .locals 9
    .param p0    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    new-instance v1, Lfbi;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axH()Lfil;

    move-result-object v2

    move-object v3, p0

    move-wide v4, p1

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v1 .. v7}, Lfbi;-><init>(Lfil;Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)V

    return-object v1
.end method

.method private static b(Ljie;)Ljie;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 185
    iget-object v1, p0, Ljie;->end:[I

    array-length v1, v1

    new-array v3, v1, [I

    .line 187
    iget-object v4, p0, Ljie;->end:[I

    array-length v5, v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_0

    aget v6, v4, v2

    .line 188
    const/16 v0, 0xb

    if-ne v6, v0, :cond_2

    .line 189
    add-int/lit8 v0, v1, 0x1

    aput v6, v3, v1

    .line 187
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 192
    :cond_0
    if-nez v1, :cond_1

    .line 193
    const/4 p0, 0x0

    .line 196
    :goto_2
    return-object p0

    .line 195
    :cond_1
    invoke-static {v3, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Ljie;->end:[I

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected final a(Lgbg;Lizj;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 160
    iget-object v0, p2, Lizj;->dRY:Ljie;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p2, Lizj;->dRY:Ljie;

    .line 166
    iget-object v1, p0, Lfbi;->cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->a(Ljie;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p2, Lizj;->dRY:Ljie;

    invoke-static {v0}, Lfbi;->b(Ljie;)Ljie;

    move-result-object v0

    iput-object v0, p2, Lizj;->dRY:Ljie;

    .line 168
    iget-boolean v0, p0, Lfbi;->cnS:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lizj;->bcV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p2}, Lizj;->bcU()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 171
    :pswitch_0
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    .line 172
    invoke-virtual {v0, v3}, Ljbj;->om(I)Ljbj;

    .line 173
    new-array v1, v3, [I

    const/4 v2, 0x0

    invoke-virtual {p2}, Lizj;->getType()I

    move-result v3

    aput v3, v1, v2

    iput-object v1, v0, Ljbj;->dYx:[I

    .line 174
    iget-object v1, p0, Lfbi;->cnT:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final d(Lizo;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 134
    iget-object v1, p1, Lizo;->dUS:[Lizp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 135
    iget-object v4, v3, Lizp;->dRY:Ljie;

    if-eqz v4, :cond_0

    .line 137
    iget-object v4, v3, Lizp;->dRY:Ljie;

    .line 141
    iget-object v5, p0, Lfbi;->cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    invoke-virtual {v5, v4, v6}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->a(Ljie;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    iget-object v4, v3, Lizp;->dRY:Ljie;

    invoke-static {v4}, Lfbi;->b(Ljie;)Ljie;

    move-result-object v4

    iput-object v4, v3, Lizp;->dRY:Ljie;

    .line 143
    iget-boolean v4, p0, Lfbi;->cnS:Z

    if-eqz v4, :cond_0

    .line 144
    iget-object v4, v3, Lizp;->afA:Ljbj;

    if-eqz v4, :cond_0

    .line 145
    iget-object v3, v3, Lizp;->afA:Ljbj;

    .line 146
    iget-object v4, v3, Ljbj;->dYx:[I

    array-length v4, v4

    if-lez v4, :cond_1

    .line 147
    iget-object v4, p0, Lfbi;->cnT:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_1
    iput-boolean v6, p0, Lfbi;->cnU:Z

    goto :goto_1

    .line 156
    :cond_2
    return-void
.end method
