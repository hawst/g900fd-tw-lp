.class public final Leyn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leym;


# instance fields
.field private final ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final cks:Ljava/util/concurrent/CountDownLatch;

.field public volatile clT:Leyx;

.field final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;

.field private final mPredictiveCardsPreferences:Lcxs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemp;Lcxs;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    sget-object v0, Leyx;->cmi:Leyx;

    iput-object v0, p0, Leyn;->clT:Leyx;

    .line 77
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Leyn;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 78
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Leyn;->cks:Ljava/util/concurrent/CountDownLatch;

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Leyn;->mAppContext:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Leyn;->mClock:Lemp;

    .line 84
    iput-object p3, p0, Leyn;->mPredictiveCardsPreferences:Lcxs;

    .line 85
    return-void
.end method

.method private a(Lizq;Ljava/util/Collection;)V
    .locals 12

    .prologue
    .line 502
    iget-object v0, p1, Lizq;->dUW:[Lizq;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 503
    iget-object v1, p1, Lizq;->dUW:[Lizq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 504
    invoke-direct {p0, v3, p2}, Leyn;->a(Lizq;Ljava/util/Collection;)V

    .line 503
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 507
    :cond_0
    iget-object v5, p1, Lizq;->dUX:[Lizj;

    array-length v6, v5

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_c

    aget-object v1, v5, v4

    .line 508
    invoke-virtual {v1}, Lizj;->getType()I

    move-result v0

    const/16 v2, 0xe

    if-ne v0, v2, :cond_1

    iget-object v0, v1, Lizj;->dSf:Lixr;

    if-eqz v0, :cond_1

    .line 510
    iget-object v0, v1, Lizj;->dSf:Lixr;

    .line 511
    invoke-virtual {v0}, Lixr;->baD()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 512
    const-string v0, "CalendarDataProviderImpl"

    const-string v1, "Received CalendarEntry from server without hash"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :cond_1
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 514
    :cond_2
    new-instance v7, Lamp;

    invoke-direct {v7}, Lamp;-><init>()V

    .line 515
    invoke-virtual {v0}, Lixr;->baD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lamp;->X(Ljava/lang/String;)Lamp;

    .line 516
    invoke-virtual {v0}, Lixr;->baG()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 517
    invoke-virtual {v0}, Lixr;->baF()I

    move-result v2

    div-int/lit8 v2, v2, 0x3c

    invoke-virtual {v7, v2}, Lamp;->ce(I)Lamp;

    .line 521
    :cond_3
    iget-object v2, v0, Lixr;->aeB:Ljbp;

    if-eqz v2, :cond_4

    .line 522
    iget-object v2, v0, Lixr;->aeB:Ljbp;

    invoke-virtual {v2}, Ljbp;->nH()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, v0, Lixr;->aeB:Ljbp;

    invoke-virtual {v2}, Ljbp;->nI()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 524
    new-instance v2, Lamq;

    invoke-direct {v2}, Lamq;-><init>()V

    .line 525
    iget-object v3, v0, Lixr;->aeB:Ljbp;

    invoke-virtual {v3}, Ljbp;->mR()D

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lamq;->d(D)Lamq;

    .line 526
    iget-object v0, v0, Lixr;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->mS()D

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lamq;->e(D)Lamq;

    .line 528
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lamp;->aK(Z)Lamp;

    .line 529
    iput-object v2, v7, Lamp;->afr:Lamq;

    .line 539
    :cond_4
    :goto_3
    iget-object v0, v1, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_9

    iget-object v0, v1, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_9

    iget-object v0, v1, Lizj;->dUr:Ljcg;

    iget-object v0, v0, Ljcg;->dRY:Ljie;

    if-eqz v0, :cond_9

    iget-object v0, v1, Lizj;->dUr:Ljcg;

    iget-object v2, v0, Ljcg;->dRY:Ljie;

    iget-object v3, v2, Ljie;->end:[I

    array-length v8, v3

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v8, :cond_9

    aget v9, v3, v0

    const/4 v10, 0x5

    if-ne v9, v10, :cond_8

    invoke-virtual {v2}, Ljie;->bkv()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {v2}, Ljie;->bmV()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 540
    :goto_5
    if-eqz v0, :cond_5

    .line 541
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Lamp;->w(J)Lamp;

    .line 546
    :cond_5
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-static {v1, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    iget-object v1, v1, Lizj;->dSf:Lixr;

    new-instance v2, Lixr;

    invoke-direct {v2}, Lixr;-><init>()V

    invoke-virtual {v1}, Lixr;->baI()Z

    move-result v3

    invoke-virtual {v2, v3}, Lixr;->hh(Z)Lixr;

    move-result-object v2

    invoke-virtual {v1}, Lixr;->baD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lixr;->qO(Ljava/lang/String;)Lixr;

    move-result-object v1

    iput-object v1, v0, Lizj;->dSf:Lixr;

    const/4 v2, 0x0

    iget-object v8, v0, Lizj;->dUo:[Liwk;

    array-length v9, v8

    const/4 v1, 0x0

    move v3, v1

    :goto_6
    if-ge v3, v9, :cond_6

    aget-object v1, v8, v3

    invoke-virtual {v1}, Liwk;->getType()I

    move-result v10

    const/16 v11, 0xc

    if-ne v10, v11, :cond_a

    move-object v2, v1

    :cond_6
    if-eqz v2, :cond_b

    const/4 v1, 0x1

    new-array v1, v1, [Liwk;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    :goto_7
    iput-object v1, v0, Lizj;->dUo:[Liwk;

    iput-object v0, v7, Lamp;->afw:Lizj;

    .line 548
    invoke-interface {p2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 531
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lamp;->aK(Z)Lamp;

    goto/16 :goto_3

    .line 539
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    .line 546
    :cond_a
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    :cond_b
    invoke-static {}, Liwk;->aZn()[Liwk;

    move-result-object v1

    goto :goto_7

    .line 553
    :cond_c
    return-void
.end method

.method private awD()V
    .locals 6

    .prologue
    .line 425
    new-instance v3, Laml;

    invoke-direct {v3}, Laml;-><init>()V

    .line 427
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->values()Ljava/util/Collection;

    move-result-object v0

    .line 428
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Lamk;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamk;

    iput-object v0, v3, Laml;->aeV:[Lamk;

    .line 430
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awH()Ljava/util/Collection;

    move-result-object v0

    .line 431
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Lamm;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamm;

    iput-object v0, v3, Laml;->aeW:[Lamm;

    .line 433
    const/4 v2, 0x0

    .line 437
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    iget-object v0, p0, Leyn;->mAppContext:Landroid/content/Context;

    const-string v4, "calendar_store"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    :try_start_1
    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 440
    array-length v2, v0

    const/high16 v3, 0x20000

    if-ge v2, v3, :cond_0

    .line 441
    invoke-virtual {v1, v0}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 448
    :goto_0
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 449
    :goto_1
    return-void

    .line 443
    :cond_0
    :try_start_2
    const-string v2, "CalendarDataProviderImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Disk store too big to write ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " bytes)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 445
    :catch_0
    move-exception v0

    .line 446
    :goto_2
    :try_start_3
    const-string v2, "CalendarDataProviderImpl"

    const-string v3, "Failed flushing to disk store"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 448
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 445
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method private awE()Z
    .locals 2

    .prologue
    .line 463
    :try_start_0
    iget-object v0, p0, Leyn;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 465
    :catch_0
    move-exception v0

    const-string v0, "CalendarDataProviderImpl"

    const-string v1, "Initialization latch wait interrupted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 468
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q(Ljava/util/Collection;)Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 173
    invoke-direct {p0}, Leyn;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 191
    :goto_0
    return v0

    .line 176
    :cond_0
    monitor-enter p0

    .line 177
    :try_start_0
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awJ()Leza;

    move-result-object v4

    .line 179
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamp;

    .line 180
    invoke-static {v0}, Leyx;->a(Lamp;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_1

    .line 181
    const-string v0, "CalendarDataProviderImpl"

    const-string v1, "ServerData from server contains invalid data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lamp;->nG()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "CalendarMemoryStore"

    const-string v1, "Incoming ServerData has dataClearedBecauseEventChanged; unexpected!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v1, v4, Leza;->cmp:Leyx;

    invoke-virtual {v0}, Lamp;->nu()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Leyx;->lF(Ljava/lang/String;)Lamk;

    move-result-object v6

    if-eqz v6, :cond_8

    iget-object v7, v6, Lamk;->aeR:Lamp;

    invoke-virtual {v7}, Lamp;->nA()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {v7}, Lamp;->nD()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-static {v0, v7}, Leyx;->a(Lamp;Lamp;)Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x0

    invoke-virtual {v0}, Lamp;->nA()Z

    move-result v8

    if-nez v8, :cond_4

    new-instance v1, Lamp;

    invoke-direct {v1}, Lamp;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Lamp;

    invoke-virtual {v7}, Lamp;->nz()I

    move-result v8

    invoke-virtual {v1, v8}, Lamp;->ce(I)Lamp;

    :cond_4
    invoke-virtual {v0}, Lamp;->nD()Z

    move-result v8

    if-nez v8, :cond_6

    if-nez v1, :cond_5

    new-instance v1, Lamp;

    invoke-direct {v1}, Lamp;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Lamp;

    :cond_5
    invoke-virtual {v7}, Lamp;->nC()J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lamp;->w(J)Lamp;

    :cond_6
    if-eqz v1, :cond_19

    :goto_3
    invoke-virtual {v7}, Lamp;->nF()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v1}, Lamp;->nF()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_7
    move v0, v2

    :goto_4
    if-nez v0, :cond_8

    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    invoke-static {v6, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    iput-object v1, v0, Lamk;->aeR:Lamp;

    iget-object v1, v4, Leza;->cmq:Leyy;

    invoke-virtual {v1, v0}, Leyy;->c(Lamk;)Z

    iget-object v0, v4, Leza;->cmt:Ljava/util/Set;

    iget-object v1, v6, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nh()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_8
    move v0, v3

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v7}, Lamp;->nu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lamp;->nu()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v2

    goto :goto_4

    :cond_a
    invoke-virtual {v7}, Lamp;->nx()Z

    move-result v0

    invoke-virtual {v1}, Lamp;->nx()Z

    move-result v8

    if-eq v0, v8, :cond_b

    move v0, v2

    goto :goto_4

    :cond_b
    invoke-virtual {v7}, Lamp;->nx()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {v7}, Lamp;->nw()Z

    move-result v0

    invoke-virtual {v1}, Lamp;->nw()Z

    move-result v8

    if-eq v0, v8, :cond_c

    move v0, v2

    goto :goto_4

    :cond_c
    invoke-virtual {v7}, Lamp;->nw()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, v7, Lamp;->afr:Lamq;

    if-eqz v0, :cond_d

    iget-object v0, v1, Lamp;->afr:Lamq;

    if-nez v0, :cond_e

    :cond_d
    move v0, v2

    goto :goto_4

    :cond_e
    iget-object v0, v7, Lamp;->afr:Lamq;

    iget-object v8, v1, Lamp;->afr:Lamq;

    invoke-virtual {v0}, Lamq;->mR()D

    move-result-wide v10

    invoke-virtual {v8}, Lamq;->mR()D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Leza;->b(DD)Z

    move-result v9

    if-eqz v9, :cond_f

    invoke-virtual {v0}, Lamq;->mS()D

    move-result-wide v10

    invoke-virtual {v8}, Lamq;->mS()D

    move-result-wide v8

    invoke-static {v10, v11, v8, v9}, Leza;->b(DD)Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v3

    :goto_5
    if-nez v0, :cond_10

    move v0, v2

    goto/16 :goto_4

    :cond_f
    move v0, v2

    goto :goto_5

    :cond_10
    invoke-virtual {v7}, Lamp;->nA()Z

    move-result v0

    invoke-virtual {v1}, Lamp;->nA()Z

    move-result v8

    if-eq v0, v8, :cond_11

    move v0, v2

    goto/16 :goto_4

    :cond_11
    invoke-virtual {v7}, Lamp;->nz()I

    move-result v0

    invoke-virtual {v1}, Lamp;->nz()I

    move-result v8

    if-eq v0, v8, :cond_12

    move v0, v2

    goto/16 :goto_4

    :cond_12
    invoke-virtual {v7}, Lamp;->nD()Z

    move-result v0

    invoke-virtual {v1}, Lamp;->nD()Z

    move-result v8

    if-eq v0, v8, :cond_13

    move v0, v2

    goto/16 :goto_4

    :cond_13
    invoke-virtual {v7}, Lamp;->nC()J

    move-result-wide v8

    invoke-virtual {v1}, Lamp;->nC()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-eqz v0, :cond_14

    move v0, v2

    goto/16 :goto_4

    :cond_14
    move v0, v3

    goto/16 :goto_4

    .line 185
    :cond_15
    invoke-virtual {v4}, Leza;->awN()Z

    move-result v1

    .line 186
    if-eqz v1, :cond_16

    .line 187
    invoke-virtual {v4}, Leza;->awN()Z

    move-result v0

    if-nez v0, :cond_17

    iget-object v0, v4, Leza;->cmp:Leyx;

    :goto_6
    iput-object v0, p0, Leyn;->clT:Leyx;

    .line 188
    invoke-direct {p0}, Leyn;->awD()V

    .line 191
    :cond_16
    monitor-exit p0

    move v0, v1

    goto/16 :goto_0

    .line 187
    :cond_17
    iget-object v0, v4, Leza;->cmp:Leyx;

    invoke-virtual {v0}, Leyx;->awK()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v2, v4, Leza;->cmt:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, v4, Leza;->cmq:Leyy;

    iget-object v5, v4, Leza;->cmp:Leyx;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Leyx;->bj(J)Lamk;

    move-result-object v0

    invoke-virtual {v3, v0}, Leyy;->c(Lamk;)Z

    goto :goto_7

    :cond_18
    iget-object v0, v4, Leza;->cmq:Leyy;

    iget-object v2, v4, Leza;->cmp:Leyx;

    invoke-virtual {v2}, Leyx;->awH()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v2}, Leyy;->g(Ljava/lang/Iterable;)Z

    iget-object v0, v4, Leza;->cmq:Leyy;

    iget-object v2, v4, Leza;->cmp:Leyx;

    iget-boolean v2, v2, Leyx;->cmm:Z

    iput-boolean v2, v0, Leyy;->cmm:Z

    iget-object v0, v4, Leza;->cmq:Leyy;

    invoke-virtual {v0}, Leyy;->awM()Leyx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_6

    :cond_19
    move-object v1, v0

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p8    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 415
    new-instance v0, Leyg;

    iget-object v1, p0, Leyn;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Leyg;-><init>(Landroid/content/Context;)V

    const/4 v9, 0x0

    move-wide v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Leyg;->a(JLjava/lang/String;JJLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Liyb;)V
    .locals 3

    .prologue
    .line 407
    iget-object v0, p1, Liyb;->dPk:[Ljim;

    invoke-virtual {p0}, Leyn;->awF()Ljava/util/List;

    move-result-object v1

    sget-object v2, Leyq;->clW:Leyq;

    invoke-static {v1, v2}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v1

    sget-object v2, Leyt;->clZ:Leyt;

    invoke-static {v1, v2}, Likm;->b(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljim;

    iput-object v0, p1, Liyb;->dPk:[Ljim;

    .line 409
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awx()Z

    move-result v0

    invoke-virtual {p1, v0}, Liyb;->hl(Z)Liyb;

    .line 410
    return-void
.end method

.method public final a(Lizo;)Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    if-nez v0, :cond_0

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 140
    :goto_0
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 157
    :goto_1
    return v0

    .line 138
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p1, Lizo;->dUQ:Lizq;

    invoke-direct {p0, v1, v0}, Leyn;->a(Lizq;Ljava/util/Collection;)V

    goto :goto_0

    .line 141
    :cond_1
    invoke-direct {p0, v0}, Leyn;->q(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;Ljava/util/Collection;Z)Z
    .locals 4

    .prologue
    .line 108
    invoke-direct {p0}, Leyn;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    .line 111
    :cond_0
    monitor-enter p0

    .line 112
    :try_start_0
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awI()Leyz;

    move-result-object v1

    .line 114
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamo;

    .line 115
    invoke-virtual {v1, v0}, Leyz;->d(Lamo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    const-string v0, "CalendarDataProviderImpl"

    const-string v3, "EventData from calendar contains invalid data"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119
    :cond_2
    :try_start_1
    invoke-virtual {v1, p2}, Leyz;->r(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 120
    const-string v0, "CalendarDataProviderImpl"

    const-string v2, "CalendarInfo from calendar contains invalid data"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_3
    iget-object v0, v1, Leyz;->cmq:Leyy;

    iget-boolean v0, v0, Leyy;->cmm:Z

    if-eq v0, p3, :cond_4

    iget-object v0, v1, Leyz;->cmq:Leyy;

    iput-boolean p3, v0, Leyy;->cmm:Z

    const/4 v0, 0x1

    iput-boolean v0, v1, Leyz;->bbz:Z

    .line 124
    :cond_4
    invoke-virtual {v1}, Leyz;->awN()Z

    move-result v0

    .line 125
    if-eqz v0, :cond_5

    .line 126
    iget-object v1, v1, Leyz;->cmq:Leyy;

    invoke-virtual {v1}, Leyy;->awM()Leyx;

    move-result-object v1

    iput-object v1, p0, Leyn;->clT:Leyx;

    .line 127
    invoke-direct {p0}, Leyn;->awD()V

    .line 130
    :cond_5
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final awA()Ljava/lang/Long;
    .locals 7

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    .line 229
    .line 231
    invoke-virtual {p0}, Leyn;->awF()Ljava/util/List;

    move-result-object v0

    sget-object v1, Leyr;->clX:Leyr;

    invoke-static {v0, v1}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    .line 232
    iget-object v0, v0, Lamk;->aeR:Lamp;

    invoke-virtual {v0}, Lamp;->nC()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 233
    goto :goto_0

    .line 235
    :cond_0
    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1
.end method

.method public final awB()Ljava/lang/Iterable;
    .locals 6

    .prologue
    .line 240
    invoke-virtual {p0}, Leyn;->awF()Ljava/util/List;

    move-result-object v0

    new-instance v1, Leys;

    iget-object v2, p0, Leyn;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Leys;-><init>(J)V

    invoke-static {v0, v1}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized awC()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 291
    monitor-enter p0

    .line 292
    :try_start_0
    new-instance v2, Leyy;

    invoke-direct {v2}, Leyy;-><init>()V

    .line 293
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    .line 295
    iget-object v4, v0, Lamk;->aeS:Lamn;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lamk;->aeS:Lamn;

    invoke-virtual {v4}, Lamn;->ne()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lamk;->aeS:Lamn;

    invoke-virtual {v4}, Lamn;->nf()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lamk;->aeS:Lamn;

    invoke-virtual {v4}, Lamn;->ng()Z

    move-result v4

    if-nez v4, :cond_0

    .line 298
    new-instance v1, Lamk;

    invoke-direct {v1}, Lamk;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    .line 299
    iget-object v1, v0, Lamk;->aeS:Lamn;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lamn;->aG(Z)Lamn;

    .line 300
    const/4 v1, 0x1

    .line 304
    :cond_0
    invoke-virtual {v2, v0}, Leyy;->c(Lamk;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 306
    :cond_1
    if-eqz v1, :cond_2

    .line 307
    :try_start_1
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awH()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v2, v0}, Leyy;->g(Ljava/lang/Iterable;)Z

    .line 308
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awx()Z

    move-result v0

    iput-boolean v0, v2, Leyy;->cmm:Z

    .line 309
    invoke-virtual {v2}, Leyy;->awM()Leyx;

    move-result-object v0

    iput-object v0, p0, Leyn;->clT:Leyx;

    .line 310
    invoke-direct {p0}, Leyn;->awD()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final awF()Ljava/util/List;
    .locals 10

    .prologue
    .line 622
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awH()Ljava/util/Collection;

    move-result-object v1

    .line 623
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->values()Ljava/util/Collection;

    move-result-object v0

    .line 625
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 627
    iget-object v3, p0, Leyn;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v3}, Lcxs;->TH()Lcxi;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lcxi;->fs(I)Lijj;

    move-result-object v3

    .line 631
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    .line 632
    iget-object v5, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v5}, Lamo;->nt()J

    move-result-wide v6

    .line 636
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 637
    invoke-static {v6, v7, v1}, Lfzy;->a(JLjava/util/Collection;)Lamm;

    move-result-object v5

    .line 639
    if-eqz v5, :cond_0

    .line 642
    invoke-static {v5, v3}, Lfzy;->a(Lamm;Ljava/util/Collection;)Ljey;

    move-result-object v5

    .line 647
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljey;->bjf()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 648
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 653
    :cond_2
    return-object v2
.end method

.method public final awi()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Leyn;->mAppContext:Landroid/content/Context;

    const-string v1, "calendar_store"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 397
    sget-object v0, Leyx;->cmi:Leyx;

    iput-object v0, p0, Leyn;->clT:Leyx;

    .line 398
    return-void
.end method

.method public final aww()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awH()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final awx()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0}, Leyx;->awx()Z

    move-result v0

    return v0
.end method

.method public final awz()Ljava/lang/Iterable;
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p0}, Leyn;->awF()Ljava/util/List;

    move-result-object v0

    sget-object v1, Leyp;->clV:Leyp;

    invoke-static {v0, v1}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    sget-object v1, Leyt;->clZ:Leyt;

    invoke-static {v0, v1}, Likm;->b(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized bg(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 258
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Leyn;->clT:Leyx;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Leyx;->a(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Leyx;

    move-result-object v1

    .line 259
    if-eqz v1, :cond_0

    .line 260
    iput-object v1, p0, Leyn;->clT:Leyx;

    .line 261
    invoke-direct {p0}, Leyn;->awD()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_0
    if-eqz v1, :cond_1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized bh(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Leyn;->clT:Leyx;

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Leyx;->a(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Leyx;

    move-result-object v1

    .line 270
    if-eqz v1, :cond_0

    .line 271
    iput-object v1, p0, Leyn;->clT:Leyx;

    .line 272
    invoke-direct {p0}, Leyn;->awD()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :cond_0
    if-eqz v1, :cond_1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized bi(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 280
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Leyn;->clT:Leyx;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Leyx;->a(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Leyx;

    move-result-object v1

    .line 281
    if-eqz v1, :cond_0

    .line 282
    iput-object v1, p0, Leyn;->clT:Leyx;

    .line 283
    invoke-direct {p0}, Leyn;->awD()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :cond_0
    if-eqz v1, :cond_1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final cV()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Leyn;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Leyo;

    invoke-direct {v0, p0}, Leyo;-><init>(Leyn;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Leyo;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 103
    :cond_0
    return-void
.end method

.method public final lE(Ljava/lang/String;)Lamk;
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Leyn;->clT:Leyx;

    invoke-virtual {v0, p1}, Leyx;->lF(Ljava/lang/String;)Lamk;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_0

    iget-object v1, v0, Lamk;->aeS:Lamn;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lamk;->aeS:Lamn;

    invoke-virtual {v1}, Lamn;->nf()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    const/4 v0, 0x0

    .line 252
    :cond_0
    return-object v0
.end method
