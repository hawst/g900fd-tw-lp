.class final Lilz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private dCb:Z

.field private dEh:Z

.field private synthetic dEi:Ljava/util/ListIterator;

.field private synthetic dEj:Lily;


# direct methods
.method constructor <init>(Lily;Ljava/util/ListIterator;)V
    .locals 0

    .prologue
    .line 824
    iput-object p1, p0, Lilz;->dEj:Lily;

    iput-object p2, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 831
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 832
    const/4 v0, 0x0

    iput-boolean v0, p0, Lilz;->dCb:Z

    iput-boolean v0, p0, Lilz;->dEh:Z

    .line 833
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 840
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 844
    invoke-virtual {p0}, Lilz;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 845
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 847
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lilz;->dCb:Z

    iput-boolean v0, p0, Lilz;->dEh:Z

    .line 848
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Lilz;->dEj:Lily;

    iget-object v1, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    invoke-static {v0, v1}, Lily;->a(Lily;I)I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 856
    invoke-virtual {p0}, Lilz;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    .line 857
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 859
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lilz;->dCb:Z

    iput-boolean v0, p0, Lilz;->dEh:Z

    .line 860
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 864
    invoke-virtual {p0}, Lilz;->nextIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 868
    iget-boolean v0, p0, Lilz;->dCb:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 869
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    .line 870
    const/4 v0, 0x0

    iput-boolean v0, p0, Lilz;->dEh:Z

    iput-boolean v0, p0, Lilz;->dCb:Z

    .line 871
    return-void
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 874
    iget-boolean v0, p0, Lilz;->dEh:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 875
    iget-object v0, p0, Lilz;->dEi:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 876
    return-void
.end method
