.class public final Lcof;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcne;


# instance fields
.field private final bdA:Z

.field private final bdv:I

.field private final bdw:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bdx:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bdy:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bdz:Z

.field private final mExtrasConsumer:Lcoi;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mInputStream:Ljava/io/InputStream;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPreloadTaskHandler:Lcpt;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mStaticContentCache:Lcpq;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;ILcoi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcpt;Lcpq;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcoi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcpt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p10    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcof;->mInputStream:Ljava/io/InputStream;

    .line 47
    iput p2, p0, Lcof;->bdv:I

    .line 48
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoi;

    iput-object v0, p0, Lcof;->mExtrasConsumer:Lcoi;

    .line 49
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcof;->bdw:Ljava/lang/String;

    .line 50
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcof;->bdx:Ljava/lang/String;

    .line 51
    iput-object p6, p0, Lcof;->bdy:Ljava/lang/String;

    .line 52
    iput-boolean p7, p0, Lcof;->bdz:Z

    .line 53
    iput-boolean p8, p0, Lcof;->bdA:Z

    .line 54
    iput-object p9, p0, Lcof;->mPreloadTaskHandler:Lcpt;

    .line 55
    iput-object p10, p0, Lcof;->mStaticContentCache:Lcpq;

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Lcnf;)V
    .locals 9
    .param p1    # Lcnf;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    .line 61
    .line 63
    iget-boolean v0, p0, Lcof;->bdA:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcon;

    iget-object v1, p0, Lcof;->mInputStream:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Lcon;-><init>(Ljava/io/InputStream;)V

    move-object v7, v0

    .line 66
    :goto_0
    new-instance v0, Lcoh;

    iget-object v2, p0, Lcof;->mExtrasConsumer:Lcoi;

    iget-object v3, p0, Lcof;->bdw:Ljava/lang/String;

    iget-boolean v4, p0, Lcof;->bdz:Z

    iget-object v5, p0, Lcof;->mPreloadTaskHandler:Lcpt;

    iget-object v6, p0, Lcof;->mStaticContentCache:Lcpq;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcoh;-><init>(Lcnf;Lcoi;Ljava/lang/String;ZLcpt;Lcpq;)V

    move v1, v8

    move v2, v8

    .line 71
    :cond_0
    :try_start_0
    const-string v3, "PelletChunkReader requires non-stopped sink"

    invoke-interface {p1, v3}, Lcnf;->ht(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :try_start_1
    invoke-virtual {v7}, Lcol;->QY()Lcog;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 83
    if-nez v3, :cond_3

    .line 84
    if-eqz v2, :cond_2

    if-nez v1, :cond_2

    .line 85
    :try_start_2
    new-instance v1, Lefs;

    const v2, 0x30003

    invoke-direct {v1, v2}, Lefs;-><init>(I)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcoh;->QX()V

    throw v1

    .line 63
    :cond_1
    new-instance v0, Lcom;

    iget-object v1, p0, Lcof;->mInputStream:Ljava/io/InputStream;

    iget-object v2, p0, Lcof;->bdy:Ljava/lang/String;

    iget-object v3, p0, Lcof;->bdx:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    :try_start_3
    invoke-interface {p1, v1}, Lcnf;->b(Ljava/lang/Exception;)V

    .line 79
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 107
    :cond_2
    invoke-virtual {v0}, Lcoh;->QX()V

    return-void

    .line 90
    :cond_3
    :try_start_4
    iget-object v4, v3, Lcog;->bdB:Lkem;

    iget-object v4, v4, Lkem;->eUR:Lken;

    if-eqz v4, :cond_4

    .line 91
    const/4 v2, 0x1

    .line 92
    iget-object v4, v3, Lcog;->bdB:Lkem;

    iget-object v4, v4, Lkem;->eUR:Lken;

    invoke-virtual {v4}, Lken;->acS()[B

    move-result-object v4

    array-length v4, v4

    add-int/2addr v1, v4

    .line 95
    :cond_4
    invoke-virtual {v0, v3}, Lcoh;->a(Lcog;)Z

    .line 96
    iget v3, p0, Lcof;->bdv:I

    if-le v1, v3, :cond_5

    .line 100
    new-instance v1, Lefs;

    const v2, 0x30002

    invoke-direct {v1, v2}, Lefs;-><init>(I)V

    throw v1

    .line 102
    :cond_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-direct {v1}, Ljava/lang/InterruptedException;-><init>()V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
