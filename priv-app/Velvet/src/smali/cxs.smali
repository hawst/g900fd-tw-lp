.class public final Lcxs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bmx:Ljava/lang/Object;


# instance fields
.field private final bmy:Ljava/lang/Object;

.field private mNowConfigurationPreferences:Lcxi;

.field private final mPrefController:Lchr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcxs;->bmx:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lchr;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcxs;->bmy:Ljava/lang/Object;

    .line 77
    iput-object p1, p0, Lcxs;->mPrefController:Lchr;

    .line 78
    return-void
.end method

.method private static A(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "configuration_bytes_key_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/accounts/Account;Liyl;)V
    .locals 4

    .prologue
    .line 201
    invoke-virtual {p0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    iget-object v1, p2, Liyl;->dQB:Ljel;

    invoke-virtual {v0, v1}, Lcxi;->b(Ljel;)V

    .line 202
    iget-object v0, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 203
    const-string v1, "configuration_account"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "configuration_version"

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 208
    iget-object v1, p2, Liyl;->dQH:Ljbs;

    if-eqz v1, :cond_0

    .line 209
    const-string v1, "location_reporting_configuration"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 212
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 213
    return-void
.end method

.method private z(Landroid/accounts/Account;)Ljga;
    .locals 3

    .prologue
    .line 277
    sget-object v1, Lcxs;->bmx:Ljava/lang/Object;

    monitor-enter v1

    .line 278
    :try_start_0
    invoke-virtual {p0, p1}, Lcxs;->x(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Attemp to diff settings across accounts"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 285
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    invoke-virtual {v0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 286
    if-nez v0, :cond_1

    .line 287
    invoke-virtual {p0, p1}, Lcxs;->w(Landroid/accounts/Account;)V

    .line 290
    :cond_1
    invoke-virtual {p0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v2

    iget-object v2, v2, Liyl;->dQB:Ljel;

    invoke-virtual {v0, v2}, Lcxi;->c(Ljel;)Ljga;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method


# virtual methods
.method public final TG()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v1

    iget-object v2, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    const-string v3, "configuration_version"

    invoke-interface {v2, v3, v0}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final TH()Lcxi;
    .locals 4

    .prologue
    .line 93
    iget-object v1, p0, Lcxs;->bmy:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_0
    iget-object v0, p0, Lcxs;->mNowConfigurationPreferences:Lcxi;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcxi;

    iget-object v2, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    const-string v3, "configuration_working"

    invoke-direct {v0, v2, v3}, Lcxi;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iput-object v0, p0, Lcxs;->mNowConfigurationPreferences:Lcxi;

    .line 98
    :cond_0
    iget-object v0, p0, Lcxs;->mNowConfigurationPreferences:Lcxi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final TI()Lcyg;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    return-object v0
.end method

.method public final TJ()Ljel;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 220
    invoke-virtual {p0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    invoke-virtual {v0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    return-object v0
.end method

.method public final TK()V
    .locals 5

    .prologue
    .line 243
    sget-object v1, Lcxs;->bmx:Ljava/lang/Object;

    monitor-enter v1

    .line 244
    :try_start_0
    invoke-virtual {p0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    .line 245
    iget-object v2, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    invoke-interface {v2}, Lcyg;->EH()Lcyh;

    move-result-object v2

    const-string v3, "configuration_account"

    invoke-interface {v2, v3}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v2

    invoke-interface {v2}, Lcyh;->apply()V

    .line 246
    iget-object v2, v0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, v0, Lcxi;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v4, v0, Lcxi;->bmn:Ljava/lang/String;

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v3, 0x0

    iput-object v3, v0, Lcxi;->bmm:Ljel;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    :try_start_2
    monitor-exit v1

    return-void

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 247
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/accounts/Account;Liyl;Z)V
    .locals 5

    .prologue
    .line 162
    sget-object v1, Lcxs;->bmx:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    invoke-virtual {p0, p1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_1

    .line 170
    :goto_0
    invoke-static {v0, p2}, Lfhb;->e(Ljsr;Ljsr;)V

    .line 172
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    .line 173
    iget-object v3, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v3}, Lchr;->Kt()Lcyg;

    move-result-object v3

    invoke-interface {v3}, Lcyg;->EH()Lcyh;

    move-result-object v3

    .line 174
    invoke-static {p1}, Lcxs;->A(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    .line 175
    invoke-interface {v3}, Lcyh;->apply()V

    .line 177
    if-eqz p3, :cond_0

    .line 178
    invoke-direct {p0, p1, v0}, Lcxs;->a(Landroid/accounts/Account;Liyl;)V

    .line 180
    :cond_0
    monitor-exit v1

    return-void

    .line 168
    :cond_1
    new-instance v0, Liyl;

    invoke-direct {v0}, Liyl;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final t(Landroid/accounts/Account;)Liyl;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-virtual {p0, p1}, Lcxs;->u(Landroid/accounts/Account;)[B

    move-result-object v0

    .line 117
    if-nez v0, :cond_0

    move-object v0, v1

    .line 129
    :goto_0
    return-object v0

    .line 123
    :cond_0
    :try_start_0
    new-instance v2, Liyl;

    invoke-direct {v2}, Liyl;-><init>()V

    invoke-static {v2, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Liyl;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-static {p1}, Lcxs;->A(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    move-object v0, v1

    .line 127
    goto :goto_0
.end method

.method public final u(Landroid/accounts/Account;)[B
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-static {p1}, Lcxs;->A(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final v(Landroid/accounts/Account;)Lizg;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    .line 149
    iget-object v0, v0, Liyl;->dQG:Lizg;

    .line 151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w(Landroid/accounts/Account;)V
    .locals 4

    .prologue
    .line 190
    sget-object v1, Lcxs;->bmx:Ljava/lang/Object;

    monitor-enter v1

    .line 191
    :try_start_0
    invoke-virtual {p0, p1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 192
    if-nez v0, :cond_0

    .line 193
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Account "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not have a master configuration"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 196
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcxs;->a(Landroid/accounts/Account;Liyl;)V

    .line 197
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final x(Landroid/accounts/Account;)Z
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcxs;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 235
    const-string v1, "configuration_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final y(Landroid/accounts/Account;)Ljgu;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 259
    sget-object v1, Lcxs;->bmx:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_0
    invoke-virtual {p0}, Lcxs;->TH()Lcxi;

    move-result-object v2

    invoke-virtual {v2}, Lcxi;->isDirty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 261
    monitor-exit v1

    .line 272
    :cond_0
    :goto_0
    return-object v0

    .line 263
    :cond_1
    invoke-direct {p0, p1}, Lcxs;->z(Landroid/accounts/Account;)Ljga;

    move-result-object v2

    .line 264
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    if-eqz v2, :cond_0

    .line 270
    new-instance v0, Ljgu;

    invoke-direct {v0}, Ljgu;-><init>()V

    .line 271
    iput-object v2, v0, Ljgu;->ekW:Ljga;

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
