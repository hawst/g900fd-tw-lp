.class abstract enum Lims;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field private static enum dEP:Lims;

.field private static enum dEQ:Lims;

.field private static enum dER:Lims;

.field private static enum dES:Lims;

.field private static enum dET:Lims;

.field private static enum dEU:Lims;

.field private static enum dEV:Lims;

.field private static enum dEW:Lims;

.field private static enum dEX:Lims;

.field private static enum dEY:Lims;

.field private static enum dEZ:Lims;

.field private static enum dFa:Lims;

.field private static dFb:[[Lims;

.field private static final synthetic dFc:[Lims;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 353
    new-instance v0, Limt;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Limt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEP:Lims;

    .line 360
    new-instance v0, Limx;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Limx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEQ:Lims;

    .line 375
    new-instance v0, Limy;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Limy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dER:Lims;

    .line 390
    new-instance v0, Limz;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Limz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dES:Lims;

    .line 407
    new-instance v0, Lina;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v7}, Lina;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dET:Lims;

    .line 414
    new-instance v0, Linb;

    const-string v1, "SOFT_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Linb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEU:Lims;

    .line 429
    new-instance v0, Linc;

    const-string v1, "SOFT_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Linc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEV:Lims;

    .line 444
    new-instance v0, Lind;

    const-string v1, "SOFT_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEW:Lims;

    .line 461
    new-instance v0, Line;

    const-string v1, "WEAK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Line;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEX:Lims;

    .line 468
    new-instance v0, Limu;

    const-string v1, "WEAK_EXPIRABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Limu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEY:Lims;

    .line 483
    new-instance v0, Limv;

    const-string v1, "WEAK_EVICTABLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Limv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dEZ:Lims;

    .line 498
    new-instance v0, Limw;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Limw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lims;->dFa:Lims;

    .line 352
    const/16 v0, 0xc

    new-array v0, v0, [Lims;

    sget-object v1, Lims;->dEP:Lims;

    aput-object v1, v0, v3

    sget-object v1, Lims;->dEQ:Lims;

    aput-object v1, v0, v4

    sget-object v1, Lims;->dER:Lims;

    aput-object v1, v0, v5

    sget-object v1, Lims;->dES:Lims;

    aput-object v1, v0, v6

    sget-object v1, Lims;->dET:Lims;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lims;->dEU:Lims;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lims;->dEV:Lims;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lims;->dEW:Lims;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lims;->dEX:Lims;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lims;->dEY:Lims;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lims;->dEZ:Lims;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lims;->dFa:Lims;

    aput-object v2, v0, v1

    sput-object v0, Lims;->dFc:[Lims;

    .line 525
    new-array v0, v6, [[Lims;

    new-array v1, v7, [Lims;

    sget-object v2, Lims;->dEP:Lims;

    aput-object v2, v1, v3

    sget-object v2, Lims;->dEQ:Lims;

    aput-object v2, v1, v4

    sget-object v2, Lims;->dER:Lims;

    aput-object v2, v1, v5

    sget-object v2, Lims;->dES:Lims;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v7, [Lims;

    sget-object v2, Lims;->dET:Lims;

    aput-object v2, v1, v3

    sget-object v2, Lims;->dEU:Lims;

    aput-object v2, v1, v4

    sget-object v2, Lims;->dEV:Lims;

    aput-object v2, v1, v5

    sget-object v2, Lims;->dEW:Lims;

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Lims;

    sget-object v2, Lims;->dEX:Lims;

    aput-object v2, v1, v3

    sget-object v2, Lims;->dEY:Lims;

    aput-object v2, v1, v4

    sget-object v2, Lims;->dEZ:Lims;

    aput-object v2, v1, v5

    sget-object v2, Lims;->dFa:Lims;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lims;->dFb:[[Lims;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Lims;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Linz;ZZ)Lims;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 533
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 534
    sget-object v1, Lims;->dFb:[[Lims;

    invoke-virtual {p0}, Linz;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    .line 533
    goto :goto_0
.end method

.method static c(Linr;Linr;)V
    .locals 2

    .prologue
    .line 564
    invoke-interface {p0}, Linr;->zL()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Linr;->bR(J)V

    .line 566
    invoke-interface {p0}, Linr;->aXJ()Linr;

    move-result-object v0

    invoke-static {v0, p1}, Limn;->a(Linr;Linr;)V

    .line 567
    invoke-interface {p0}, Linr;->aXI()Linr;

    move-result-object v0

    invoke-static {p1, v0}, Limn;->a(Linr;Linr;)V

    .line 569
    invoke-static {p0}, Limn;->d(Linr;)V

    .line 570
    return-void
.end method

.method static d(Linr;Linr;)V
    .locals 1

    .prologue
    .line 576
    invoke-interface {p0}, Linr;->aXL()Linr;

    move-result-object v0

    invoke-static {v0, p1}, Limn;->b(Linr;Linr;)V

    .line 577
    invoke-interface {p0}, Linr;->aXK()Linr;

    move-result-object v0

    invoke-static {p1, v0}, Limn;->b(Linr;Linr;)V

    .line 579
    invoke-static {p0}, Limn;->e(Linr;)V

    .line 580
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lims;
    .locals 1

    .prologue
    .line 352
    const-class v0, Lims;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lims;

    return-object v0
.end method

.method public static values()[Lims;
    .locals 1

    .prologue
    .line 352
    sget-object v0, Lims;->dFc:[Lims;

    invoke-virtual {v0}, [Lims;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lims;

    return-object v0
.end method


# virtual methods
.method a(Lins;Linr;Linr;)Linr;
    .locals 2

    .prologue
    .line 557
    invoke-interface {p2}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Linr;->aXH()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lims;->a(Lins;Ljava/lang/Object;ILinr;)Linr;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lins;Ljava/lang/Object;ILinr;)Linr;
    .param p4    # Linr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method
