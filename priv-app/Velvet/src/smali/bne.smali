.class public final Lbne;
.super Lbir;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbir;-><init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final K(J)I
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lbne;->yS()V

    invoke-virtual {p0}, Lbne;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnb;

    invoke-interface {v0, p1, p2}, Lbnb;->J(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method

.method public final a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lbne;->yS()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/reporting/UploadRequest;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lbne;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnb;

    invoke-interface {v0, p1}, Lbnb;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lbjm;Lbiv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5d3f18

    iget-object v2, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjm;->c(Lbjj;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lbnc;->o(Landroid/os/IBinder;)Lbnb;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lbne;->yS()V

    invoke-virtual {p0}, Lbne;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnb;

    invoke-interface {v0, p1}, Lbnb;->c(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method

.method public final d(Landroid/accounts/Account;)I
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lbne;->yS()V

    invoke-virtual {p0}, Lbne;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnb;

    invoke-interface {v0, p1}, Lbnb;->d(Landroid/accounts/Account;)I

    move-result v0

    invoke-static {v0}, Lbsb;->ed(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/16 v0, 0x9

    goto :goto_0
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.reporting.service.START"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.reporting.internal.IReportingService"

    return-object v0
.end method
