.class final Lihr;
.super Lihp;
.source "PG"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private synthetic dCz:Lihq;


# direct methods
.method constructor <init>(Lihq;)V
    .locals 0

    .prologue
    .line 803
    iput-object p1, p0, Lihr;->dCz:Lihq;

    invoke-direct {p0, p1}, Lihp;-><init>(Liho;)V

    return-void
.end method

.method public constructor <init>(Lihq;I)V
    .locals 1

    .prologue
    .line 805
    iput-object p1, p0, Lihr;->dCz:Lihq;

    .line 806
    invoke-virtual {p1}, Lihq;->aWx()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lihp;-><init>(Liho;Ljava/util/Iterator;)V

    .line 807
    return-void
.end method

.method private aWy()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 810
    invoke-virtual {p0}, Lihp;->aWw()V

    iget-object v0, p0, Lihp;->dCl:Ljava/util/Iterator;

    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 840
    iget-object v0, p0, Lihr;->dCz:Lihq;

    invoke-virtual {v0}, Lihq;->isEmpty()Z

    move-result v0

    .line 841
    invoke-direct {p0}, Lihr;->aWy()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 842
    iget-object v1, p0, Lihr;->dCz:Lihq;

    iget-object v1, v1, Lihq;->dCh:Lihb;

    invoke-static {v1}, Lihb;->c(Lihb;)I

    .line 843
    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lihr;->dCz:Lihq;

    invoke-virtual {v0}, Lihq;->aWv()V

    .line 846
    :cond_0
    return-void
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 815
    invoke-direct {p0}, Lihr;->aWy()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 825
    invoke-direct {p0}, Lihr;->aWy()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820
    invoke-direct {p0}, Lihr;->aWy()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 830
    invoke-direct {p0}, Lihr;->aWy()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 835
    invoke-direct {p0}, Lihr;->aWy()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 836
    return-void
.end method
