.class public final Lckm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field aZP:Lckj;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private aZQ:Ljava/lang/Object;

.field private aZR:Ljava/util/concurrent/Future;

.field mClock:Lemp;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final mHttpHelper:Ldkx;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Ldkx;Lcpn;Ljava/util/concurrent/ScheduledExecutorService;Lemp;)V
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lckm;->aZQ:Ljava/lang/Object;

    .line 70
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lckm;->mContext:Landroid/content/Context;

    .line 71
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkx;

    iput-object v0, p0, Lckm;->mHttpHelper:Ldkx;

    .line 73
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lckm;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 75
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemp;

    iput-object v0, p0, Lckm;->mClock:Lemp;

    .line 76
    new-instance v0, Lckj;

    iget-object v1, p0, Lckm;->mClock:Lemp;

    iget-object v2, p0, Lckm;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lckm;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v1, v2, v3}, Lckj;-><init>(Lemp;Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v0, p0, Lckm;->aZP:Lckj;

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lemy;)V
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 174
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lckm;->aZQ:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lckm;->aZR:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lckm;->aZR:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_1
    :try_start_1
    iget-object v7, p0, Lckm;->mHttpExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lckn;

    const-string v2, "base-page-prefetch"

    const/4 v1, 0x2

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lckn;-><init>(Lckm;Ljava/lang/String;[ILjava/lang/String;Lemy;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lckm;->aZR:Ljava/util/concurrent/Future;

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method
