.class public final Lfek;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cpL:Ljava/util/LinkedList;

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lemp;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    .line 65
    iput-object p1, p0, Lfek;->mClock:Lemp;

    .line 66
    return-void
.end method


# virtual methods
.method public final declared-synchronized addLocation(Landroid/location/Location;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v1, 0x0

    .line 79
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 128
    :cond_0
    monitor-exit p0

    return-void

    .line 85
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    iget-object v0, p0, Lfek;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 86
    iget-object v0, p0, Lfek;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 90
    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 91
    mul-long/2addr v2, v6

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 94
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 124
    :cond_3
    :goto_0
    iget-object v0, p0, Lfek;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x124f80

    sub-long v2, v0, v2

    .line 125
    :goto_1
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 126
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 97
    :cond_4
    const/4 v2, 0x1

    move v3, v1

    .line 99
    :goto_2
    :try_start_2
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 100
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 101
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_6

    .line 102
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    cmpg-float v0, v2, v0

    if-gez v0, :cond_5

    .line 104
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0, v3, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    :cond_5
    move v0, v1

    .line 118
    :goto_3
    if-eqz v0, :cond_3

    .line 119
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :cond_6
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_7

    .line 112
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0, v3, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 114
    goto :goto_3

    .line 99
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_3
.end method

.method public final declared-synchronized ayG()Ljava/util/List;
    .locals 1

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized ayH()V
    .locals 1

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    monitor-exit p0

    return-void

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized ayy()Landroid/location/Location;
    .locals 2

    .prologue
    .line 139
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lfek;->iE(I)Ljava/util/List;

    move-result-object v0

    .line 140
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized ayz()Ljava/util/List;
    .locals 3

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 154
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lfek;->iE(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 155
    if-eqz v0, :cond_0

    .line 156
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 159
    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized iE(I)Ljava/util/List;
    .locals 14

    .prologue
    const/4 v0, 0x3

    const-wide/32 v12, 0x493e0

    const/4 v2, 0x0

    .line 187
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 188
    iget-object v1, p0, Lfek;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 192
    if-le p1, v0, :cond_0

    move p1, v0

    .line 201
    :cond_0
    iget-object v0, p0, Lfek;->cpL:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 206
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-ge v7, p1, :cond_7

    .line 207
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    cmp-long v7, v8, v4

    if-gtz v7, :cond_1

    .line 215
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    sub-long v10, v4, v12

    cmp-long v7, v8, v10

    if-gez v7, :cond_3

    .line 222
    if-eqz v1, :cond_2

    .line 223
    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 225
    :cond_2
    sub-long/2addr v4, v12

    move-object v1, v2

    .line 231
    :cond_3
    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    const/high16 v8, 0x42c80000    # 100.0f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_5

    .line 236
    if-eqz v0, :cond_4

    .line 237
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 239
    :cond_4
    sub-long/2addr v4, v12

    move-object v1, v2

    .line 241
    goto :goto_0

    .line 244
    :cond_5
    if-nez v1, :cond_6

    move-object v1, v0

    .line 247
    goto :goto_0

    .line 250
    :cond_6
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_b

    :goto_1
    move-object v1, v0

    .line 255
    goto :goto_0

    .line 257
    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v0, p1, :cond_8

    .line 258
    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 261
    :cond_8
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    if-eqz v0, :cond_9

    .line 269
    :cond_a
    monitor-exit p0

    return-object v3

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_b
    move-object v0, v1

    goto :goto_1
.end method
