.class public final Lcwm;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final aNq:Lcse;

.field private final amF:Lcry;

.field private final amY:Z

.field private final blV:Leon;

.field private blW:Landroid/preference/SwitchPreference;

.field private blX:Landroid/preference/SwitchPreference;

.field private blY:Landroid/preference/SwitchPreference;

.field private final blw:Z

.field private final mAlwaysOnHotwordAdapter:Lcsq;

.field private final mContext:Landroid/content/Context;

.field private final mGsaConfigFlags:Lchk;

.field private final mGsaPreferenceController:Lchr;

.field private final mIntentStarter:Leoj;

.field private final mSearchSettings:Lcke;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Lhym;Landroid/content/Context;Lcse;Lchr;Lchk;Lcke;Leoj;Lcry;Lcsq;)V
    .locals 1
    .param p9    # Lcsq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 85
    iput-object p1, p0, Lcwm;->mVoiceSettings:Lhym;

    .line 86
    iput-object p2, p0, Lcwm;->mContext:Landroid/content/Context;

    .line 87
    iput-object p3, p0, Lcwm;->aNq:Lcse;

    .line 88
    iput-object p4, p0, Lcwm;->mGsaPreferenceController:Lchr;

    .line 89
    iput-object p5, p0, Lcwm;->mGsaConfigFlags:Lchk;

    .line 90
    iput-object p6, p0, Lcwm;->mSearchSettings:Lcke;

    .line 91
    iput-object p7, p0, Lcwm;->mIntentStarter:Leoj;

    .line 92
    new-instance v0, Leon;

    invoke-direct {v0}, Leon;-><init>()V

    iput-object v0, p0, Lcwm;->blV:Leon;

    .line 93
    iput-object p8, p0, Lcwm;->amF:Lcry;

    .line 94
    iput-object p9, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    .line 95
    invoke-virtual {p1}, Lhym;->aTR()Z

    move-result v0

    iput-boolean v0, p0, Lcwm;->amY:Z

    .line 96
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTR()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcwm;->blw:Z

    .line 97
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Tr()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 224
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0, v4}, Lcsq;->c(Lcsr;)V

    .line 227
    iget-object v0, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    invoke-virtual {p0, v0}, Lcwm;->c(Landroid/preference/SwitchPreference;)V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lcwm;->amF:Lcry;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.action.QUERY_DSP_INFO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "language"

    iget-object v0, v0, Lcry;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "keyword"

    const-string v2, "okgoogle"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FE()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwm;->amF:Lcry;

    invoke-static {}, Lcry;->Sz()Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcwm;->blV:Leon;

    iget-object v3, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Leon;->e(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwm;->mContext:Landroid/content/Context;

    const-string v2, "com.google.android.googlequicksearchbox.LAUNCH_FROM_DSP_HOTWORD"

    new-instance v3, Lcwv;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Lcwv;-><init>(Lcwm;B)V

    const/4 v5, -0x1

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private Ts()Z
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 272
    iget-object v1, p0, Lcwm;->aNq:Lcse;

    invoke-virtual {v1, v0}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v0}, Lgcq;->aEU()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Tt()Z
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 278
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcsi;->SD()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Tu()Z
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lcgg;->aVr:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HO()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTE()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Ga()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-static {v0}, Leoh;->at(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aKb()Lelo;

    move-result-object v0

    invoke-virtual {v0}, Lelo;->aum()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Tv()V
    .locals 4

    .prologue
    .line 575
    iget-object v0, p0, Lcwm;->mIntentStarter:Leoj;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcwm;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/e300/E300Activity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcwq;

    invoke-direct {v2, p0}, Lcwq;-><init>(Lcwm;)V

    invoke-interface {v0, v1, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    .line 588
    return-void
.end method

.method static synthetic a(Lcwm;)Landroid/preference/SwitchPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic a(Lcwm;Z)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcwm;->cK(Z)V

    return-void
.end method

.method static synthetic b(Lcwm;)Landroid/preference/SwitchPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->blY:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic c(Lcwm;)Landroid/preference/SwitchPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method private cJ(Z)V
    .locals 4

    .prologue
    .line 591
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_1

    .line 594
    iget-object v0, p0, Lcwm;->mIntentStarter:Leoj;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcwm;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/e300/E300Activity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "retrainvoicemodel"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcwr;

    invoke-direct {v2, p0}, Lcwr;-><init>(Lcwm;)V

    invoke-interface {v0, v1, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    iget-object v0, p0, Lcwm;->amF:Lcry;

    invoke-static {}, Lcry;->Sz()Landroid/content/Intent;

    move-result-object v0

    .line 608
    if-eqz v0, :cond_0

    .line 613
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcwm;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/e300/E300Activity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skipspeakerid"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 616
    iget-object v1, p0, Lcwm;->mIntentStarter:Leoj;

    new-instance v2, Lcww;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcww;-><init>(Lcwm;B)V

    invoke-interface {v1, v0, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    goto :goto_0
.end method

.method private cK(Z)V
    .locals 3

    .prologue
    .line 623
    iget-object v0, p0, Lcwm;->aNq:Lcse;

    const/4 v1, 0x0

    iget-object v2, p0, Lcwm;->mSearchSettings:Lcke;

    invoke-interface {v2}, Lcke;->ND()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcse;->a([BLjava/lang/String;)V

    .line 624
    iget-object v0, p0, Lcwm;->aNq:Lcse;

    iget-object v1, p0, Lcwm;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcse;->iJ(Ljava/lang/String;)V

    .line 625
    if-eqz p1, :cond_0

    .line 626
    iget-object v0, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 627
    iget-object v0, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 629
    :cond_0
    iget-object v0, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 630
    return-void
.end method

.method static synthetic d(Lcwm;)Lcsq;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    return-object v0
.end method

.method static synthetic e(Lcwm;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcwm;->Tr()V

    return-void
.end method

.method static synthetic f(Lcwm;)Lcry;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->amF:Lcry;

    return-object v0
.end method

.method static synthetic g(Lcwm;)Leoj;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->mIntentStarter:Leoj;

    return-object v0
.end method

.method static synthetic h(Lcwm;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcwm;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private k(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcwm;->amY:Z

    if-eqz v0, :cond_0

    .line 168
    const-string v0, "alwaysOnHotword"

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    const-string v0, "voiceEverywhereEnabled"

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private tR()Ljava/lang/String;
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 744
    iget-object v1, p0, Lcwm;->aNq:Lcse;

    invoke-virtual {v1, v0}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v0}, Lgcq;->getPrompt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 6

    .prologue
    const v5, 0x7f0a098e

    const v2, 0x7f0a098d

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 101
    instance-of v0, p1, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_1

    .line 102
    check-cast p1, Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcwm;->d(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lcwm;->a(Landroid/preference/Preference;)V

    goto :goto_1

    .line 104
    :cond_1
    const-string v0, "hotword"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    iget-object v0, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FG()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 107
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 157
    :cond_2
    :goto_2
    return-void

    .line 108
    :cond_3
    invoke-direct {p0}, Lcwm;->Ts()Z

    move-result v0

    if-nez v0, :cond_4

    .line 109
    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 110
    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_2

    .line 112
    :cond_4
    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 113
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 115
    :cond_5
    const-string v0, "hotwordDetector"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 116
    invoke-direct {p0}, Lcwm;->tR()Ljava/lang/String;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->FG()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 118
    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 119
    invoke-virtual {p1, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 133
    :goto_3
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    .line 120
    :cond_6
    invoke-direct {p0}, Lcwm;->Ts()Z

    move-result v1

    if-nez v1, :cond_7

    .line 121
    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 122
    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_3

    .line 124
    :cond_7
    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 125
    iget-object v1, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lhgn;->bI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 126
    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a098f

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 129
    :cond_8
    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0990

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 134
    :cond_9
    const-string v0, "voiceEverywhereEnabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 135
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 136
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    goto/16 :goto_2

    .line 137
    :cond_a
    const-string v0, "speaker_id_retrain_voice_model"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 138
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 139
    invoke-direct {p0, p1}, Lcwm;->k(Landroid/preference/Preference;)V

    goto/16 :goto_2

    .line 140
    :cond_b
    const-string v0, "speaker_id_delete_voice_model"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 141
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 142
    invoke-direct {p0, p1}, Lcwm;->k(Landroid/preference/Preference;)V

    goto/16 :goto_2

    .line 143
    :cond_c
    const-string v0, "pref_category_voice_model"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 144
    invoke-direct {p0, p1}, Lcwm;->k(Landroid/preference/Preference;)V

    goto/16 :goto_2

    .line 145
    :cond_d
    const-string v0, "hotword_from_lock_screen"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 146
    invoke-direct {p0}, Lcwm;->tR()Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0993

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 150
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcwm;->blY:Landroid/preference/SwitchPreference;

    .line 151
    iget-object v0, p0, Lcwm;->blY:Landroid/preference/SwitchPreference;

    invoke-direct {p0, v0}, Lcwm;->k(Landroid/preference/Preference;)V

    goto/16 :goto_2

    .line 152
    :cond_e
    const-string v0, "alwaysOnHotword"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 154
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    goto/16 :goto_2
.end method

.method public final b(Landroid/preference/SwitchPreference;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 439
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcwm;->Ts()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcwm;->Tu()Z

    move-result v0

    if-nez v0, :cond_1

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    iget-object v0, p0, Lcwm;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->ND()Ljava/lang/String;

    move-result-object v3

    .line 446
    if-nez v3, :cond_3

    .line 448
    const v0, 0x7f0a099a

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setSummary(I)V

    move v0, v1

    .line 461
    :goto_1
    if-eqz v0, :cond_5

    .line 462
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 472
    :cond_2
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0

    .line 450
    :cond_3
    iget-object v0, p0, Lcwm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FG()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 452
    const v0, 0x7f0a098d

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setSummary(I)V

    move v0, v1

    .line 453
    goto :goto_1

    .line 454
    :cond_4
    invoke-direct {p0}, Lcwm;->Tt()Z

    move-result v0

    if-nez v0, :cond_6

    .line 457
    const v0, 0x7f0a098e

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setSummary(I)V

    move v0, v1

    .line 458
    goto :goto_1

    .line 465
    :cond_5
    invoke-virtual {p1, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 466
    const v0, 0x7f0a099b

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 467
    iget-object v0, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v0, v3}, Lhym;->oC(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 469
    invoke-virtual {p1, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public final c(Landroid/preference/SwitchPreference;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 480
    if-nez p1, :cond_0

    .line 554
    :goto_0
    return-void

    .line 492
    :cond_0
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->SL()Z

    move-result v5

    .line 494
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->SM()Z

    move-result v4

    .line 495
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->SN()Z

    move-result v3

    .line 496
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->SJ()Z

    move-result v1

    .line 497
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->SO()Z

    move-result v0

    .line 515
    :goto_1
    if-eqz v5, :cond_2

    .line 516
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 517
    const v5, 0x7f0a0995

    invoke-virtual {p1, v5}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 527
    iget-object v5, p0, Lcwm;->mSearchSettings:Lcke;

    invoke-interface {v5}, Lcke;->ND()Ljava/lang/String;

    move-result-object v5

    .line 528
    if-nez v5, :cond_3

    .line 530
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 531
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 532
    const v0, 0x7f0a099a

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setSummary(I)V

    goto :goto_0

    .line 499
    :cond_1
    iget-object v0, p0, Lcwm;->amF:Lcry;

    iget-boolean v4, v0, Lcry;->bhq:Z

    .line 500
    iget-object v0, p0, Lcwm;->amF:Lcry;

    iget-boolean v3, v0, Lcry;->bhp:Z

    .line 501
    iget-object v0, p0, Lcwm;->amF:Lcry;

    iget-boolean v1, v0, Lcry;->bhr:Z

    .line 502
    iget-object v0, p0, Lcwm;->amF:Lcry;

    .line 503
    iget-object v0, p0, Lcwm;->amF:Lcry;

    iget-boolean v0, v0, Lcry;->bhs:Z

    move v5, v4

    move v4, v3

    move v3, v1

    move v1, v2

    goto :goto_1

    .line 520
    :cond_2
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    goto :goto_0

    .line 533
    :cond_3
    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcwm;->Tt()Z

    move-result v1

    if-nez v1, :cond_5

    .line 536
    :cond_4
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 537
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 538
    const v0, 0x7f0a098e

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setSummary(I)V

    goto :goto_0

    .line 539
    :cond_5
    if-nez v4, :cond_6

    .line 542
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0

    .line 543
    :cond_6
    if-nez v0, :cond_7

    .line 546
    invoke-virtual {p1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto/16 :goto_0

    .line 548
    :cond_7
    iget-object v0, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFi()Z

    move-result v0

    .line 552
    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 248
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 249
    invoke-direct {p0}, Lcwm;->Tu()Z

    move-result v3

    .line 253
    const-string v4, "voiceEverywhereEnabled"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 256
    iget-boolean v2, p0, Lcwm;->amY:Z

    if-nez v2, :cond_0

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    .line 267
    :cond_1
    :goto_0
    return v0

    .line 257
    :cond_2
    const-string v4, "alwaysOnHotword"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 259
    iget-boolean v2, p0, Lcwm;->amY:Z

    if-eqz v2, :cond_3

    if-nez v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 260
    :cond_4
    const-string v4, "speaker_id_retrain_voice_model"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "speaker_id_delete_voice_model"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "pref_category_voice_model"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "hotword_from_lock_screen"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 265
    :cond_5
    if-nez v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 300
    const-string v2, "hotwordDetector"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 301
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 303
    iget-object v3, p0, Lcwm;->mGsaPreferenceController:Lchr;

    iget-object v3, v3, Lchr;->mGelStartupPrefs:Ldku;

    const-string v4, "GSAPrefs.hotword_enabled"

    invoke-virtual {v3, v4, v2}, Ldku;->m(Ljava/lang/String;Z)V

    .line 306
    iget-object v3, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 309
    iget-boolean v3, p0, Lcwm;->amY:Z

    if-eqz v3, :cond_1

    .line 310
    iget-boolean v3, p0, Lcwm;->blw:Z

    if-eqz v3, :cond_2

    .line 311
    iget-object v3, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v3}, Lcsq;->SO()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcwm;->mVoiceSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aFi()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 313
    iget-object v3, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v3, v2, v1}, Lcsq;->j(ZZ)Z

    .line 315
    :cond_0
    invoke-direct {p0}, Lcwm;->Tr()V

    .line 425
    :cond_1
    :goto_0
    return v0

    .line 316
    :cond_2
    if-nez v2, :cond_1

    .line 317
    iget-object v2, p0, Lcwm;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcwm;->amF:Lcry;

    invoke-static {v1}, Lcry;->cF(Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 319
    invoke-direct {p0}, Lcwm;->Tr()V

    goto :goto_0

    .line 322
    :cond_3
    const-string v2, "voiceEverywhereEnabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 324
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 325
    iget-object v2, p0, Lcwm;->mVoiceSettings:Lhym;

    iget-object v3, p0, Lcwm;->mSearchSettings:Lcke;

    invoke-interface {v3}, Lcke;->ND()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhym;->oD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 327
    iget-object v1, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    invoke-virtual {p0, v1}, Lcwm;->b(Landroid/preference/SwitchPreference;)V

    .line 328
    iget-object v1, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 334
    :cond_4
    invoke-direct {p0}, Lcwm;->Tv()V

    move v0, v1

    .line 336
    goto :goto_0

    .line 339
    :cond_5
    iget-object v2, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    invoke-virtual {p0, v2}, Lcwm;->b(Landroid/preference/SwitchPreference;)V

    .line 340
    iget-object v2, p0, Lcwm;->blY:Landroid/preference/SwitchPreference;

    invoke-virtual {v2, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 341
    iget-object v1, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 343
    :cond_6
    const-string v2, "hotword_from_lock_screen"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 344
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 345
    invoke-direct {p0}, Lcwm;->tR()Ljava/lang/String;

    move-result-object v2

    .line 346
    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 347
    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 349
    const/high16 v5, 0x1040000

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 351
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcwm;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0a0996

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v2, v0, v1

    invoke-virtual {v3, v7, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a0997

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcwp;

    invoke-direct {v2, p0}, Lcwp;-><init>(Lcwm;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcwo;

    invoke-direct {v2, p0}, Lcwo;-><init>(Lcwm;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move v0, v1

    .line 369
    goto/16 :goto_0

    .line 371
    :cond_7
    const-string v2, "alwaysOnHotword"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 373
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 376
    iget-boolean v3, p0, Lcwm;->blw:Z

    if-eqz v3, :cond_c

    .line 377
    iget-object v3, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v3}, Lcsq;->SJ()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v3}, Lcsq;->SK()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 379
    :cond_8
    if-eqz v2, :cond_b

    .line 380
    iget-object v2, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v2}, Lcsq;->SO()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 386
    iget-object v2, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v2}, Lcsq;->SJ()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcwm;->mVoiceSettings:Lhym;

    iget-object v3, p0, Lcwm;->mSearchSettings:Lcke;

    invoke-interface {v3}, Lcke;->ND()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhym;->oD(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 389
    invoke-direct {p0, v0}, Lcwm;->cJ(Z)V

    move v0, v1

    .line 390
    goto/16 :goto_0

    .line 392
    :cond_9
    iget-object v1, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v1, v0, v0}, Lcsq;->j(ZZ)Z

    move-result v0

    goto/16 :goto_0

    .line 394
    :cond_a
    invoke-direct {p0, v1}, Lcwm;->cJ(Z)V

    move v0, v1

    .line 395
    goto/16 :goto_0

    .line 398
    :cond_b
    iget-object v2, p0, Lcwm;->blY:Landroid/preference/SwitchPreference;

    invoke-virtual {v2, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 399
    iget-object v2, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v2, v1, v0}, Lcsq;->j(ZZ)Z

    move-result v0

    goto/16 :goto_0

    .line 404
    :cond_c
    if-eqz v2, :cond_e

    .line 405
    iget-object v2, p0, Lcwm;->amF:Lcry;

    iget-boolean v2, v2, Lcry;->bhs:Z

    if-eqz v2, :cond_d

    .line 407
    iget-object v1, p0, Lcwm;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcwm;->amF:Lcry;

    invoke-static {v0}, Lcry;->cF(Z)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 409
    iget-object v1, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    invoke-virtual {p0, v1}, Lcwm;->c(Landroid/preference/SwitchPreference;)V

    goto/16 :goto_0

    .line 413
    :cond_d
    invoke-direct {p0, v1}, Lcwm;->cJ(Z)V

    move v0, v1

    .line 414
    goto/16 :goto_0

    .line 417
    :cond_e
    iget-object v2, p0, Lcwm;->blY:Landroid/preference/SwitchPreference;

    invoke-virtual {v2, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 418
    iget-object v2, p0, Lcwm;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcwm;->amF:Lcry;

    invoke-static {v1}, Lcry;->cF(Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 423
    :cond_f
    const-string v2, "HotwordSettingsCntrlr"

    const-string v3, "Unexpected preference change: %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 558
    const-string v0, "speaker_id_retrain_voice_model"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 559
    iget-boolean v0, p0, Lcwm;->amY:Z

    if-eqz v0, :cond_1

    .line 560
    invoke-direct {p0, v8}, Lcwm;->cJ(Z)V

    .line 571
    :cond_0
    :goto_0
    return v8

    .line 562
    :cond_1
    invoke-direct {p0}, Lcwm;->Tv()V

    goto :goto_0

    .line 564
    :cond_2
    const-string v0, "speaker_id_delete_voice_model"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    iget-boolean v0, p0, Lcwm;->amY:Z

    if-eqz v0, :cond_4

    .line 566
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    new-instance v1, Lcws;

    invoke-direct {v1, p0}, Lcws;-><init>(Lcwm;)V

    invoke-interface {v0, v1}, Lcsq;->f(Lcsr;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcwm;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcwm;->amF:Lcry;

    invoke-static {}, Lcry;->SA()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.googlequicksearchbox.LAUNCH_FROM_DSP_HOTWORD"

    new-instance v3, Lcwt;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Lcwt;-><init>(Lcwm;B)V

    const/4 v5, -0x1

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 568
    :cond_4
    invoke-direct {p0, v8}, Lcwm;->cK(Z)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcwm;->blW:Landroid/preference/SwitchPreference;

    invoke-virtual {p0, v0}, Lcwm;->b(Landroid/preference/SwitchPreference;)V

    .line 206
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    invoke-direct {p0}, Lcwm;->Tr()V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcwm;->blX:Landroid/preference/SwitchPreference;

    invoke-virtual {p0, v0}, Lcwm;->c(Landroid/preference/SwitchPreference;)V

    .line 218
    invoke-direct {p0}, Lcwm;->Tr()V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcyc;->onStart()V

    .line 179
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    new-instance v1, Lcwn;

    invoke-direct {v1, p0}, Lcwn;-><init>(Lcwm;)V

    invoke-interface {v0, v1}, Lcsq;->a(Lcsr;)V

    .line 189
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcwm;->blw:Z

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcwm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->disconnect()V

    .line 197
    :cond_0
    invoke-super {p0}, Lcyc;->onStop()V

    .line 198
    return-void
.end method
