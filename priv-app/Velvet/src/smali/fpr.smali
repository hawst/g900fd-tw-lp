.class public final Lfpr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final czb:Ljdg;

.field private czc:Lgbg;


# direct methods
.method public constructor <init>(Ljdg;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iput-object p1, p0, Lfpr;->czb:Ljdg;

    .line 25
    return-void
.end method

.method private aCJ()Lgbg;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lfpr;->czc:Lgbg;

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iput-object v0, p0, Lfpr;->czc:Lgbg;

    .line 36
    :cond_0
    iget-object v0, p0, Lfpr;->czc:Lgbg;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 74
    :cond_0
    :goto_0
    return v2

    .line 43
    :cond_1
    instance-of v0, p1, Lfpr;

    if-eqz v0, :cond_0

    .line 45
    check-cast p1, Lfpr;

    .line 47
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->ebK:Ljfh;

    if-eqz v0, :cond_2

    move v0, v1

    .line 48
    :goto_1
    iget-object v3, p1, Lfpr;->czb:Ljdg;

    iget-object v3, v3, Ljdg;->ebK:Ljfh;

    if-eqz v3, :cond_3

    move v3, v1

    .line 49
    :goto_2
    if-ne v0, v3, :cond_0

    .line 54
    if-eqz v0, :cond_4

    .line 55
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->ebK:Ljfh;

    .line 56
    iget-object v3, p1, Lfpr;->czb:Ljdg;

    iget-object v3, v3, Ljdg;->ebK:Ljfh;

    .line 58
    invoke-virtual {v0}, Ljfh;->bjo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljfh;->bjo()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljfh;->bew()I

    move-result v0

    invoke-virtual {v3}, Ljfh;->bew()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 47
    goto :goto_1

    :cond_3
    move v3, v2

    .line 48
    goto :goto_2

    .line 62
    :cond_4
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->bnG:Ljfl;

    if-eqz v0, :cond_5

    move v0, v1

    .line 63
    :goto_3
    iget-object v3, p1, Lfpr;->czb:Ljdg;

    iget-object v3, v3, Ljdg;->bnG:Ljfl;

    if-eqz v3, :cond_6

    move v3, v1

    .line 64
    :goto_4
    if-ne v0, v3, :cond_0

    .line 69
    if-eqz v0, :cond_7

    .line 70
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->bnG:Ljfl;

    invoke-virtual {v0}, Ljfl;->bjv()J

    move-result-wide v4

    iget-object v0, p1, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->bnG:Ljfl;

    invoke-virtual {v0}, Ljfl;->bjv()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 62
    goto :goto_3

    :cond_6
    move v3, v2

    .line 63
    goto :goto_4

    .line 74
    :cond_7
    invoke-direct {p0}, Lfpr;->aCJ()Lgbg;

    move-result-object v0

    invoke-direct {p1}, Lfpr;->aCJ()Lgbg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 79
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->ebK:Ljfh;

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lfpr;->czb:Ljdg;

    iget-object v1, v1, Ljdg;->ebK:Ljfh;

    invoke-virtual {v1}, Ljfh;->bjo()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lfpr;->czb:Ljdg;

    iget-object v1, v1, Ljdg;->ebK:Ljfh;

    invoke-virtual {v1}, Ljfh;->bew()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 85
    :goto_0
    return v0

    .line 82
    :cond_0
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    iget-object v0, v0, Ljdg;->bnG:Ljfl;

    if-eqz v0, :cond_1

    .line 83
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lfpr;->czb:Ljdg;

    iget-object v1, v1, Ljdg;->bnG:Ljfl;

    invoke-virtual {v1}, Ljfl;->bjv()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    .line 85
    :cond_1
    invoke-direct {p0}, Lfpr;->aCJ()Lgbg;

    move-result-object v0

    invoke-virtual {v0}, Lgbg;->hashCode()I

    move-result v0

    goto :goto_0
.end method
