.class final Lhbv;
.super Landroid/webkit/WebViewClient;
.source "PG"


# instance fields
.field private synthetic dcc:Lhbq;


# direct methods
.method constructor <init>(Lhbq;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lhbv;->dcc:Lhbq;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbV:Lhby;

    invoke-virtual {v0, p2}, Lhby;->nL(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    invoke-interface {v0}, Lhbx;->pageReady()V

    .line 307
    :cond_0
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 331
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    new-instance v1, Ldls;

    const-string v2, "Page load failed: errorCode %d, description %s, failingUrl %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    invoke-direct {v1, v2, v3}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Lhbx;->aW(Ljava/lang/Object;)V

    .line 334
    return-void
.end method

.method public final onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    const-string v1, "Page load failed - received HTTP Auth request"

    invoke-interface {v0, v1}, Lhbx;->aW(Ljava/lang/Object;)V

    .line 340
    return-void
.end method

.method public final onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    const-string v1, "Page load failed - received Login request"

    invoke-interface {v0, v1}, Lhbx;->aW(Ljava/lang/Object;)V

    .line 346
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    const-string v1, "Page load failed - received SSL error"

    invoke-interface {v0, v1}, Lhbx;->aW(Ljava/lang/Object;)V

    .line 351
    return-void
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 316
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbS:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbT:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbR:Lhao;

    iget-object v0, v0, Lhao;->mWebPage:Ldyo;

    invoke-virtual {v0}, Ldyo;->alL()Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 325
    :goto_0
    return-object v0

    .line 320
    :cond_0
    iget-object v0, p0, Lhbv;->dcc:Lhbq;

    new-instance v1, Ldls;

    const-string v2, "Content for %s apparently requested multiple times"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v1, v2, v3}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lhbq;->aV(Ljava/lang/Object;)V

    .line 325
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 311
    iget-object v2, p0, Lhbv;->dcc:Lhbq;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v0, v2, Lhbq;->mUrlHelper:Lcpn;

    invoke-virtual {v0, v3}, Lcpn;->q(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v3}, Lhbq;->S(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    iget-object v4, v2, Lhbq;->dbW:Ljava/util/Set;

    invoke-static {v4}, Lijp;->G(Ljava/util/Collection;)Lijp;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/google/android/velvet/presenter/inappwebpage/Request;-><init>(Landroid/net/Uri;Lijp;)V

    iget-object v2, v2, Lhbq;->dbU:Lhbx;

    invoke-interface {v2, v0}, Lhbx;->b(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    :goto_1
    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v2, Lhbq;->dbU:Lhbx;

    invoke-interface {v0, v3}, Lhbx;->R(Landroid/net/Uri;)V

    goto :goto_1
.end method
