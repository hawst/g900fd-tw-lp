.class abstract Ldzo;
.super Ldzl;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Ldzl;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Person;)Lcom/google/android/search/shared/contact/Person;
    .locals 9
    .param p1    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 248
    invoke-virtual {p0, p1}, Ldzo;->j(Lcom/google/android/search/shared/contact/Person;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 249
    invoke-virtual {p0, p2}, Ldzo;->j(Lcom/google/android/search/shared/contact/Person;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    .line 250
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    move-object p2, v4

    .line 287
    :cond_0
    :goto_0
    return-object p2

    .line 254
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object p2, v4

    .line 256
    goto :goto_0

    .line 259
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 260
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 263
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    .line 264
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 265
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 266
    invoke-virtual {p0, v7, v0}, Ldzo;->ax(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 267
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    move v0, v3

    :goto_2
    move v1, v0

    .line 270
    goto :goto_1

    .line 272
    :cond_4
    if-nez v1, :cond_5

    move-object p2, v4

    .line 273
    goto :goto_0

    .line 275
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 276
    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 278
    invoke-virtual {p0, v7, v0}, Ldzo;->ax(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 279
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 284
    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    move-object p2, v4

    .line 285
    goto :goto_0

    .line 287
    :cond_8
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v3

    :goto_4
    add-int/lit8 v1, v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v3

    :goto_5
    add-int/lit8 v4, v0, 0x0

    if-eq v1, v4, :cond_c

    if-le v1, v4, :cond_0

    :cond_9
    :goto_6
    move-object p2, p1

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_5

    :cond_c
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->ame()Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v3

    :goto_7
    add-int/2addr v0, v1

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->ame()Z

    move-result v1

    if-eqz v1, :cond_e

    :goto_8
    add-int v1, v4, v3

    if-eq v0, v1, :cond_f

    if-le v0, v1, :cond_0

    goto :goto_6

    :cond_d
    move v0, v2

    goto :goto_7

    :cond_e
    move v3, v2

    goto :goto_8

    :cond_f
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_10

    if-le v0, v1, :cond_0

    goto :goto_6

    :cond_10
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_11

    if-le v0, v1, :cond_0

    goto :goto_6

    :cond_11
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->aiI()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->aiI()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_9

    if-le v0, v1, :cond_0

    goto :goto_6

    :cond_12
    move v0, v1

    goto/16 :goto_2
.end method

.method protected ax(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v0, v1

    .line 233
    goto :goto_0

    .line 235
    :cond_3
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected abstract j(Lcom/google/android/search/shared/contact/Person;)Ljava/util/List;
.end method
