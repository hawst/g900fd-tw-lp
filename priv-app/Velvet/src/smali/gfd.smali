.class public Lgfd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cIq:Ljava/util/UUID;

.field private static cIr:Ljava/lang/reflect/Constructor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "58b4b260-8e06-11e0-aa8e-0002a5d5c51b"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lgfd;->cIq:Ljava/util/UUID;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method public static a(Lgfc;)Lgff;
    .locals 3

    .prologue
    .line 116
    invoke-virtual {p0}, Lgfc;->getSampleRate()I

    move-result v0

    const/16 v1, 0x3e80

    if-ne v0, v1, :cond_0

    .line 117
    sget-object v0, Lgff;->cIu:Lgff;

    .line 119
    :goto_0
    return-object v0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lgfc;->getSampleRate()I

    move-result v0

    const/16 v1, 0x1f40

    if-ne v0, v1, :cond_1

    .line 119
    sget-object v0, Lgff;->cIt:Lgff;

    goto :goto_0

    .line 121
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported sample rate: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lgfc;->getSampleRate()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Ljzv;)Ljava/util/List;
    .locals 8

    .prologue
    .line 218
    if-nez p0, :cond_1

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 240
    :cond_0
    :goto_0
    return-object v0

    .line 222
    :cond_1
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 223
    iget-object v1, p0, Ljzv;->eOw:[Ljava/lang/String;

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 224
    invoke-static {}, Landroid/media/audiofx/AudioEffect;->queryEffects()[Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v3

    .line 226
    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 227
    sget-object v6, Lgfd;->cIq:Ljava/util/UUID;

    iget-object v7, v5, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    invoke-virtual {v6, v7}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 228
    iget-object v5, v5, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 229
    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 230
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 234
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public static a(Lgff;[B)[B
    .locals 3

    .prologue
    .line 127
    sget-object v0, Lgfe;->cIs:[I

    invoke-virtual {p0}, Lgff;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Encoding not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :pswitch_0
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lgfd;->a([BIZ)[B

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    :pswitch_1
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lgfd;->a([BIZ)[B

    move-result-object v0

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a([BIZ)[B
    .locals 5

    .prologue
    .line 142
    const/4 v1, 0x0

    .line 144
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0, p1}, Lgfd;->b(Ljava/io/InputStream;I)Ljava/io/InputStream;

    move-result-object v1

    .line 146
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 147
    if-eqz p2, :cond_0

    .line 148
    const-string v2, "#!AMR\n"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 150
    :cond_0
    const/16 v2, 0x180

    new-array v2, v2, [B

    .line 151
    :goto_0
    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v1, v2, v3, v4}, Leoo;->a(Ljava/io/InputStream;[BII)I

    move-result v3

    if-lez v3, :cond_1

    .line 153
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    invoke-static {v1}, Leoo;->i(Ljava/io/InputStream;)V

    throw v0

    .line 155
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 157
    invoke-static {v1}, Leoo;->i(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public static b(Ljava/io/InputStream;I)Ljava/io/InputStream;
    .locals 7

    .prologue
    .line 166
    if-nez p1, :cond_0

    .line 171
    :goto_0
    return-object p0

    .line 168
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 169
    invoke-static {p0}, Lgfd;->k(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p0

    goto :goto_0

    .line 170
    :cond_1
    const/16 v0, 0x9

    if-ne p1, v0, :cond_2

    .line 171
    new-instance v0, Lgeo;

    const-string v2, "audio/amr-wb"

    const/16 v3, 0x3e80

    const/16 v4, 0x800

    const/16 v5, 0x5d2a

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lgeo;-><init>(Ljava/io/InputStream;Ljava/lang/String;IIII)V

    move-object p0, v0

    goto :goto_0

    .line 173
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported encoding:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static k(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 189
    :try_start_0
    const-class v1, Lgfd;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :try_start_1
    sget-object v0, Lgfd;->cIr:Ljava/lang/reflect/Constructor;

    if-nez v0, :cond_0

    .line 192
    const-string v0, "android.media.AmrInputStream"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 195
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/io/InputStream;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    sput-object v0, Lgfd;->cIr:Ljava/lang/reflect/Constructor;

    .line 197
    :cond_0
    sget-object v0, Lgfd;->cIr:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 198
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Exception while instantiating AmrInputStream"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static kh(I)I
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 103
    const/16 v0, 0x180

    .line 105
    :goto_0
    return v0

    .line 104
    :cond_0
    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    .line 105
    const/16 v0, 0x300

    goto :goto_0

    .line 107
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported AMR encoding:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
