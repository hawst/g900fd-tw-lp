.class public final Lfrd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lekg;


# static fields
.field private static final cAv:Landroid/view/animation/Interpolator;


# instance fields
.field private cAA:I

.field private cAB:I

.field private final cAw:Lfrf;

.field private final cAx:F

.field private final cAy:Ljava/lang/Runnable;

.field private cAz:Landroid/view/ViewPropertyAnimator;

.field private ccy:Z

.field private final cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field private final cvs:Lfrg;

.field private final mScrollViewControl:Lekf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lfrd;->cAv:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lekf;Lcom/google/android/shared/ui/SuggestionGridLayout;Lfrg;Lfrf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lfre;

    invoke-direct {v0, p0}, Lfre;-><init>(Lfrd;)V

    iput-object v0, p0, Lfrd;->cAy:Ljava/lang/Runnable;

    .line 38
    iput-boolean v1, p0, Lfrd;->ccy:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lfrd;->cAz:Landroid/view/ViewPropertyAnimator;

    .line 42
    iput v1, p0, Lfrd;->cAA:I

    .line 46
    iput v1, p0, Lfrd;->cAB:I

    .line 66
    iput-object p2, p0, Lfrd;->mScrollViewControl:Lekf;

    .line 67
    iput-object p3, p0, Lfrd;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 68
    iput-object p4, p0, Lfrd;->cvs:Lfrg;

    .line 69
    iput-object p5, p0, Lfrd;->cAw:Lfrf;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    neg-float v0, v0

    iput v0, p0, Lfrd;->cAx:F

    .line 72
    return-void
.end method

.method private aCV()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-boolean v0, p0, Lfrd;->ccy:Z

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lfrd;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lfrd;->cAz:Landroid/view/ViewPropertyAnimator;

    .line 130
    iput v2, p0, Lfrd;->cAA:I

    .line 131
    iput v2, p0, Lfrd;->cAB:I

    .line 132
    iput-boolean v2, p0, Lfrd;->ccy:Z

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method final aCW()V
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lfrd;->ccy:Z

    if-eqz v0, :cond_0

    .line 139
    invoke-direct {p0}, Lfrd;->aCV()V

    .line 140
    iget-object v0, p0, Lfrd;->cvs:Lfrg;

    invoke-interface {v0}, Lfrg;->aCS()V

    .line 142
    :cond_0
    return-void
.end method

.method public final aa(II)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public final axt()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lfrd;->mScrollViewControl:Lekf;

    invoke-interface {v0, p0}, Lekf;->a(Lekg;)V

    .line 76
    return-void
.end method

.method public final dr(I)V
    .locals 4

    .prologue
    .line 84
    iget-boolean v0, p0, Lfrd;->ccy:Z

    if-nez v0, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget v0, p0, Lfrd;->cAB:I

    if-eqz v0, :cond_2

    .line 88
    iget v0, p0, Lfrd;->cAB:I

    add-int/2addr p1, v0

    .line 91
    :cond_2
    iput p1, p0, Lfrd;->cAA:I

    .line 92
    iget-object v0, p0, Lfrd;->cvs:Lfrg;

    invoke-interface {v0}, Lfrg;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    if-gez p1, :cond_0

    .line 93
    int-to-float v0, p1

    iget v1, p0, Lfrd;->cAx:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 95
    int-to-float v0, p1

    iget v1, p0, Lfrd;->cAx:F

    div-float/2addr v0, v1

    .line 96
    sget-object v1, Lfrd;->cAv:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 99
    iget-object v1, p0, Lfrd;->cAz:Landroid/view/ViewPropertyAnimator;

    if-eqz v1, :cond_3

    .line 100
    iget-object v1, p0, Lfrd;->cAz:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 101
    const/4 v1, 0x0

    iput-object v1, p0, Lfrd;->cAz:Landroid/view/ViewPropertyAnimator;

    .line 104
    :cond_3
    iget-object v1, p0, Lfrd;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget v2, p0, Lfrd;->cAx:F

    neg-float v2, v2

    mul-float/2addr v2, v0

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setTranslationY(F)V

    .line 105
    iget-object v1, p0, Lfrd;->cvs:Lfrg;

    invoke-interface {v1, v0}, Lfrg;->setTriggerPercentage(F)V

    .line 108
    iget-object v0, p0, Lfrd;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, p0, Lfrd;->cAy:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 109
    iget-object v0, p0, Lfrd;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, p0, Lfrd;->cAy:Ljava/lang/Runnable;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 112
    :cond_4
    iget-object v0, p0, Lfrd;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, p0, Lfrd;->cAy:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 115
    iget-object v0, p0, Lfrd;->cvs:Lfrg;

    invoke-interface {v0}, Lfrg;->start()V

    .line 116
    iget-object v0, p0, Lfrd;->cAw:Lfrf;

    const/16 v1, 0x25

    invoke-interface {v0, v1}, Lfrf;->ds(I)V

    .line 119
    invoke-direct {p0}, Lfrd;->aCV()V

    goto :goto_0
.end method

.method public final unregister()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfrd;->mScrollViewControl:Lekf;

    invoke-interface {v0, p0}, Lekf;->b(Lekg;)V

    .line 80
    return-void
.end method

.method public final vS()V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public final vT()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public final vU()V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method public final vV()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrd;->ccy:Z

    .line 147
    iget-object v0, p0, Lfrd;->cvs:Lfrg;

    invoke-interface {v0}, Lfrg;->aCR()V

    .line 148
    return-void
.end method

.method public final vW()V
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lfrd;->ccy:Z

    if-eqz v0, :cond_0

    .line 155
    iget v0, p0, Lfrd;->cAA:I

    iput v0, p0, Lfrd;->cAB:I

    .line 157
    :cond_0
    return-void
.end method
