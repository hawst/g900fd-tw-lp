.class public final Licn;
.super Licq;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Licq;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    .line 19
    return-void
.end method


# virtual methods
.method public final oX(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.TEXT_OPEN_FROM_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final oY(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Licn;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    invoke-virtual {v0}, Ljmh;->bpN()Z

    move-result v0

    if-nez v0, :cond_1

    .line 30
    const/4 v0, 0x0

    .line 39
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Licn;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v2

    invoke-virtual {v2}, Ljmh;->bpM()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "com.google.android.apps.books"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "books:addToMyEBooks"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 36
    if-eqz p1, :cond_0

    .line 37
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final oZ(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Licn;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    iget-object v0, v0, Ljmh;->etC:Ljmj;

    invoke-virtual {v0}, Ljmj;->bpV()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    const/4 v0, 0x0

    .line 55
    :cond_0
    :goto_0
    return-object v0

    .line 48
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.apps.books"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 51
    if-eqz p1, :cond_0

    .line 52
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
