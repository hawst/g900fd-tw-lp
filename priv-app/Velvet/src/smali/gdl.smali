.class public final Lgdl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdp;


# instance fields
.field private cGN:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljww;)Leie;
    .locals 2

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 27
    new-instance v0, Leii;

    invoke-virtual {p1}, Ljww;->getErrorCode()I

    move-result v1

    invoke-direct {v0, v1}, Leii;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Leie;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 37
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lgdl;->cGN:Z

    if-eqz v2, :cond_1

    .line 39
    const/16 v1, 0x1b

    invoke-static {v1}, Lege;->ht(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 44
    :cond_1
    :try_start_1
    instance-of v2, p1, Leig;

    if-nez v2, :cond_2

    instance-of v2, p1, Leii;

    if-eqz v2, :cond_3

    :cond_2
    invoke-virtual {p1}, Leie;->aja()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdl;->cGN:Z

    .line 48
    invoke-virtual {p1}, Leie;->aja()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 49
    const/16 v0, 0x1a

    invoke-static {v0}, Lege;->ht(I)V

    :goto_1
    move v0, v1

    .line 55
    goto :goto_0

    .line 51
    :cond_4
    const/16 v0, 0x19

    invoke-static {v0}, Lege;->ht(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
