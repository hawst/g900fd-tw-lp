.class public final Lezf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lezi;


# instance fields
.field private final cmw:Lixr;

.field private final mClock:Lemp;

.field private final mEntry:Lizj;

.field private final mNotificationStore:Lffp;


# direct methods
.method public constructor <init>(Lizj;Lffp;Lemp;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lezf;->mEntry:Lizj;

    .line 21
    iget-object v0, p1, Lizj;->dSf:Lixr;

    iput-object v0, p0, Lezf;->cmw:Lixr;

    .line 22
    iput-object p2, p0, Lezf;->mNotificationStore:Lffp;

    .line 23
    iput-object p3, p0, Lezf;->mClock:Lemp;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lezg;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 30
    iget-object v1, p2, Lezg;->cmx:Lezn;

    iget-object v0, p0, Lezf;->cmw:Lixr;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lezf;->cmw:Lixr;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    aget-object v0, v0, v4

    :goto_0
    invoke-virtual {v1, p1, v0}, Lezn;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Liyg;)V

    .line 34
    iget-object v0, p2, Lezg;->cmz:Leze;

    iget-object v1, p0, Lezf;->cmw:Lixr;

    invoke-virtual {v1}, Lixr;->baD()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Leze;->mCalendarDataProvider:Leym;

    invoke-interface {v0, v1}, Leym;->lE(Ljava/lang/String;)Lamk;

    move-result-object v2

    sget-object v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyp:Ljava/lang/String;

    new-instance v3, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    invoke-direct {v3}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;-><init>()V

    invoke-virtual {p1, v0, v3}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->a(Ljava/lang/String;Lamk;)V

    .line 38
    :cond_0
    iget-object v0, p0, Lezf;->cmw:Lixr;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lezf;->cmw:Lixr;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    aget-object v0, v0, v4

    iget-object v0, v0, Liyg;->dPL:Liyh;

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lezf;->mNotificationStore:Lffp;

    iget-object v1, p0, Lezf;->mEntry:Lizj;

    iget-object v2, p0, Lezf;->cmw:Lixr;

    iget-object v2, v2, Lixr;->dMF:[Liyg;

    aget-object v2, v2, v4

    iget-object v3, p0, Lezf;->mClock:Lemp;

    invoke-static {p1, v0, v1, v2, v3}, Lezp;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lffp;Lizj;Liyg;Lemp;)V

    .line 42
    :cond_1
    return-void

    .line 30
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
