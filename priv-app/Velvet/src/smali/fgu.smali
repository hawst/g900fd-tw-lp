.class public final Lfgu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aWy:Ljava/util/List;

.field cpc:Lezt;

.field private cqs:Ljbj;

.field crg:Lfgw;

.field public crh:I

.field public cri:Lizo;

.field public crj:Z

.field public mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

.field mEntryTreePruner:Lfbh;

.field mLocationOracle:Lfdr;

.field mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Lfcx;Lfdr;Lfbh;Lezt;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>()V

    iput-object v0, p0, Lfgu;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfgu;->aWy:Ljava/util/List;

    .line 104
    iput-object p1, p0, Lfgu;->mNetworkClient:Lfcx;

    .line 105
    iput-object p2, p0, Lfgu;->mLocationOracle:Lfdr;

    .line 106
    iput-object p3, p0, Lfgu;->mEntryTreePruner:Lfbh;

    .line 107
    iput-object p4, p0, Lfgu;->cpc:Lezt;

    .line 108
    return-void
.end method

.method private a(ILjava/util/List;)V
    .locals 8
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 268
    iget-object v0, p0, Lfgu;->crg:Lfgw;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lfgu;->crg:Lfgw;

    invoke-virtual {v0, v1}, Lfgw;->cancel(Z)Z

    .line 271
    :cond_0
    new-instance v3, Lfgw;

    if-nez p2, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {v3, p0, p1, v0}, Lfgw;-><init>(Lfgu;IZ)V

    iput-object v3, p0, Lfgu;->crg:Lfgw;

    .line 273
    if-nez p2, :cond_3

    .line 274
    iget-object v0, p0, Lfgu;->cqs:Ljbj;

    const/4 v3, 0x0

    iput-object v3, v0, Ljbj;->dYD:[J

    .line 281
    :cond_1
    iget-object v0, p0, Lfgu;->crg:Lfgw;

    new-array v1, v1, [Ljbj;

    iget-object v3, p0, Lfgu;->cqs:Ljbj;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lfgw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 282
    return-void

    :cond_2
    move v0, v2

    .line 271
    goto :goto_0

    .line 276
    :cond_3
    iget-object v0, p0, Lfgu;->cqs:Ljbj;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [J

    iput-object v3, v0, Ljbj;->dYD:[J

    move v3, v2

    .line 277
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 278
    iget-object v0, p0, Lfgu;->cqs:Ljbj;

    iget-object v4, v0, Ljbj;->dYD:[J

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v3

    .line 277
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method


# virtual methods
.method public final Qn()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lfgu;->crg:Lfgw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjbj;)V
    .locals 2

    .prologue
    .line 114
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Use a specific RequestTrace"

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 117
    invoke-static {p2}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljbj;

    iput-object v0, p0, Lfgu;->cqs:Ljbj;

    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfgu;->a(ILjava/util/List;)V

    .line 119
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lfgx;)V
    .locals 1

    .prologue
    .line 148
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lfgu;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lfgu;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_0
    return-void
.end method

.method public final azQ()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lfgu;->cqs:Ljbj;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Initial load should start before loadAdditionalEntries"

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 133
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 134
    iget-object v0, p0, Lfgu;->cri:Lizo;

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    iget-object v4, v0, Lizq;->dUW:[Lizq;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    .line 135
    iget-object v6, v0, Lizq;->dUX:[Lizj;

    if-eqz v6, :cond_2

    .line 136
    iget-object v6, v0, Lizq;->dUX:[Lizj;

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    .line 137
    iget-object v8, v8, Lizj;->dTy:Ljef;

    .line 138
    if-eqz v8, :cond_0

    .line 139
    invoke-virtual {v8}, Ljef;->biu()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 144
    :cond_3
    const/16 v0, 0x34

    invoke-direct {p0, v0, v3}, Lfgu;->a(ILjava/util/List;)V

    .line 145
    return-void
.end method

.method public final b(Lfgx;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lfgu;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 156
    return-void
.end method

.method public final clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Lfgu;->crg:Lfgw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfgu;->crg:Lfgw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfgw;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    :cond_0
    iput-object v2, p0, Lfgu;->crg:Lfgw;

    .line 211
    iput-object v2, p0, Lfgu;->cri:Lizo;

    .line 213
    :cond_1
    iput-object v2, p0, Lfgu;->cqs:Ljbj;

    .line 214
    return-void
.end method
