.class public final Ljzd;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eMY:I

.field private eMZ:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 706
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 707
    iput v0, p0, Ljzd;->aez:I

    iput v0, p0, Ljzd;->eMY:I

    iput v0, p0, Ljzd;->eMZ:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljzd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzd;->eCz:I

    .line 708
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 649
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzd;->eMY:I

    iget v0, p0, Ljzd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzd;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzd;->eMZ:I

    iget v0, p0, Ljzd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzd;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 722
    iget v0, p0, Ljzd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 723
    const/4 v0, 0x1

    iget v1, p0, Ljzd;->eMY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 725
    :cond_0
    iget v0, p0, Ljzd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 726
    const/4 v0, 0x2

    iget v1, p0, Ljzd;->eMZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 728
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 729
    return-void
.end method

.method public final bwN()I
    .locals 1

    .prologue
    .line 690
    iget v0, p0, Ljzd;->eMZ:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 733
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 734
    iget v1, p0, Ljzd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 735
    const/4 v1, 0x1

    iget v2, p0, Ljzd;->eMY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    :cond_0
    iget v1, p0, Ljzd;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 739
    const/4 v1, 0x2

    iget v2, p0, Ljzd;->eMZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_1
    return v0
.end method

.method public final tp(I)Ljzd;
    .locals 1

    .prologue
    .line 674
    const/16 v0, 0x3e8

    iput v0, p0, Ljzd;->eMY:I

    .line 675
    iget v0, p0, Ljzd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzd;->aez:I

    .line 676
    return-object p0
.end method

.method public final tq(I)Ljzd;
    .locals 1

    .prologue
    .line 693
    iput p1, p0, Ljzd;->eMZ:I

    .line 694
    iget v0, p0, Ljzd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzd;->aez:I

    .line 695
    return-object p0
.end method
