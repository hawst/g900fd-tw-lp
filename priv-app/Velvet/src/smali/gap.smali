.class public final Lgap;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static p(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 16
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 17
    array-length v1, v0

    if-lt p1, v1, :cond_0

    .line 18
    const-string v1, "FlightStatusFormatter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown status code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    const/4 p1, 0x0

    .line 21
    :cond_0
    aget-object v0, v0, p1

    return-object v0
.end method

.method public static q(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 27
    packed-switch p1, :pswitch_data_0

    .line 41
    const v0, 0x7f0b00ba

    .line 45
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 31
    :pswitch_0
    const v0, 0x7f0b00b9

    .line 32
    goto :goto_0

    .line 38
    :pswitch_1
    const v0, 0x7f0b00b7

    .line 39
    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
