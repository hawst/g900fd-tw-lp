.class public final Lfdj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdi;


# instance fields
.field final cph:Landroid/util/LruCache;

.field private final cpi:I

.field private final cpj:I

.field private final mAppContext:Landroid/content/Context;

.field private final mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Lfaq;Landroid/content/Context;Lfcx;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p2, p0, Lfdj;->mAppContext:Landroid/content/Context;

    .line 75
    iput-object p3, p0, Lfdj;->mNetworkClient:Lfcx;

    .line 76
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lfdj;->cph:Landroid/util/LruCache;

    .line 77
    new-instance v0, Lfdk;

    invoke-direct {v0, p0}, Lfdk;-><init>(Lfdj;)V

    invoke-virtual {p1, v0}, Lfaq;->a(Lfbb;)V

    .line 78
    invoke-static {p2}, Leot;->au(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lfdj;->cpi:I

    .line 79
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lfdj;->cpj:I

    .line 81
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-static {}, Lenu;->auQ()V

    .line 87
    invoke-static {p1}, Lgbn;->c(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Lgbn;

    move-result-object v4

    .line 88
    iget-object v0, p0, Lfdj;->cph:Landroid/util/LruCache;

    invoke-virtual {v0, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 91
    if-nez v0, :cond_3

    .line 92
    iget-object v0, v4, Lgbn;->cFc:Lgbg;

    invoke-virtual {v0}, Lgbg;->aEf()Ljsr;

    move-result-object v0

    check-cast v0, Ljbp;

    iget-object v1, v4, Lgbn;->cFd:Lgbg;

    invoke-virtual {v1}, Lgbg;->aEf()Ljsr;

    move-result-object v1

    check-cast v1, Ljal;

    iget-boolean v2, v4, Lgbn;->cxZ:Z

    iget-boolean v5, v4, Lgbn;->cFe:Z

    iget-boolean v6, v4, Lgbn;->cyb:Z

    iget-boolean v7, v4, Lgbn;->cyc:Z

    iget-object v8, v4, Lgbn;->cyd:Ljava/lang/Integer;

    iget-object v9, v4, Lgbn;->cye:Ljava/lang/Integer;

    invoke-static {}, Lenu;->auQ()V

    new-instance v10, Ljal;

    invoke-direct {v10}, Ljal;-><init>()V

    invoke-static {v1, v10}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Ljal;

    iget-object v10, v1, Ljal;->dWL:Ljak;

    if-eqz v10, :cond_0

    iget-object v10, v1, Ljal;->dWL:Ljak;

    iput-object v3, v10, Ljak;->clz:Ljcu;

    iget-object v10, v1, Ljal;->dWL:Ljak;

    iput-object v3, v10, Ljak;->dWH:[Ljcu;

    :cond_0
    if-nez v2, :cond_1

    invoke-static {}, Liyg;->bbv()[Liyg;

    move-result-object v2

    iput-object v2, v1, Ljal;->dMF:[Liyg;

    :cond_1
    new-instance v10, Ljgv;

    invoke-direct {v10}, Ljgv;-><init>()V

    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_0
    invoke-virtual {v10, v2}, Ljgv;->pO(I)Ljgv;

    move-result-object v8

    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    invoke-virtual {v8, v2}, Ljgv;->pP(I)Ljgv;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljgv;->ih(Z)Ljgv;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljgv;->ii(Z)Ljgv;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljgv;->ij(Z)Ljgv;

    move-result-object v2

    iput-object v1, v2, Ljgv;->ekX:Ljal;

    if-eqz v0, :cond_2

    iput-object v0, v2, Ljgv;->ekY:Ljbp;

    :cond_2
    const/4 v0, 0x7

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljed;->hK(Z)Ljed;

    move-result-object v0

    iput-object v2, v0, Ljed;->edt:Ljgv;

    iget-object v1, p0, Lfdj;->mNetworkClient:Lfcx;

    invoke-interface {v1, v0}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v1, v0, Ljeh;->eeh:Ljgw;

    if-eqz v1, :cond_6

    iget-object v0, v0, Ljeh;->eeh:Ljgw;

    invoke-virtual {v0}, Ljgw;->blk()[B

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 94
    :goto_2
    if-eqz v0, :cond_3

    .line 95
    iget-object v1, p0, Lfdj;->cph:Landroid/util/LruCache;

    invoke-virtual {v1, v4, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_3
    return-object v0

    .line 92
    :cond_4
    iget v2, p0, Lfdj;->cpi:I

    goto :goto_0

    :cond_5
    iget v2, p0, Lfdj;->cpj:I

    goto :goto_1

    :cond_6
    move-object v0, v3

    goto :goto_2
.end method

.method public final ayr()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 155
    .line 156
    new-instance v3, Ljal;

    invoke-direct {v3}, Ljal;-><init>()V

    .line 158
    :try_start_0
    iget-object v1, p0, Lfdj;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "sample_route.txt.bin"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 159
    :try_start_1
    invoke-static {v3, v2}, Leqh;->a(Ljsr;Ljava/io/InputStream;)Ljsr;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 167
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 172
    :goto_0
    new-instance v1, Ljgv;

    invoke-direct {v1}, Ljgv;-><init>()V

    iget v2, p0, Lfdj;->cpi:I

    invoke-virtual {v1, v2}, Ljgv;->pO(I)Ljgv;

    move-result-object v1

    iget v2, p0, Lfdj;->cpj:I

    invoke-virtual {v1, v2}, Ljgv;->pP(I)Ljgv;

    move-result-object v1

    .line 175
    iput-object v3, v1, Ljgv;->ekX:Ljal;

    .line 176
    new-instance v2, Ljbp;

    invoke-direct {v2}, Ljbp;-><init>()V

    const-wide v4, 0x4042b6ecbfb15b57L    # 37.4291

    invoke-virtual {v2, v4, v5}, Ljbp;->n(D)Ljbp;

    move-result-object v2

    const-wide v4, -0x3fa1752bd3c36113L    # -122.1692

    invoke-virtual {v2, v4, v5}, Ljbp;->o(D)Ljbp;

    move-result-object v2

    .line 178
    new-instance v3, Ljhm;

    invoke-direct {v3}, Ljhm;-><init>()V

    .line 179
    iput-object v2, v3, Ljhm;->aeB:Ljbp;

    .line 180
    new-instance v2, Ljej;

    invoke-direct {v2}, Ljej;-><init>()V

    .line 181
    const/4 v4, 0x1

    new-array v4, v4, [Ljhm;

    aput-object v3, v4, v6

    iput-object v4, v2, Ljej;->eew:[Ljhm;

    .line 182
    const/4 v3, 0x6

    invoke-static {v3}, Lfjw;->iQ(I)Ljed;

    move-result-object v3

    .line 184
    iput-object v1, v3, Ljed;->edt:Ljgv;

    .line 185
    iput-object v2, v3, Ljed;->edv:Ljej;

    .line 186
    iget-object v1, p0, Lfdj;->mNetworkClient:Lfcx;

    invoke-interface {v1, v3}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_0

    .line 189
    iget-object v2, v1, Ljeh;->eeh:Ljgw;

    if-eqz v2, :cond_0

    .line 190
    iget-object v0, v1, Ljeh;->eeh:Ljgw;

    invoke-virtual {v0}, Ljgw;->blk()[B

    move-result-object v0

    .line 191
    array-length v1, v0

    invoke-static {v0, v6, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 195
    :cond_0
    return-object v0

    .line 160
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 161
    :goto_1
    :try_start_2
    const-string v4, "StaticMapCacheImpl"

    const-string v5, "File not found: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 167
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 162
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 163
    :goto_2
    :try_start_3
    const-string v4, "StaticMapCacheImpl"

    const-string v5, "IO Exception: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 167
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 164
    :catch_2
    move-exception v1

    move-object v2, v0

    .line 165
    :goto_3
    :try_start_4
    const-string v4, "StaticMapCacheImpl"

    const-string v5, "IO Exception: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 167
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 164
    :catch_3
    move-exception v1

    goto :goto_3

    .line 162
    :catch_4
    move-exception v1

    goto :goto_2

    .line 160
    :catch_5
    move-exception v1

    goto :goto_1
.end method
