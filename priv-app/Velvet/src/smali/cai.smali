.class public final Lcai;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static at(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 67
    if-nez p0, :cond_1

    .line 94
    :cond_0
    return-void

    .line 72
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ladh;

    if-eqz v0, :cond_2

    .line 73
    const v0, 0x7f0c00d4

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 75
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lvy;

    if-eqz v0, :cond_3

    .line 76
    const v0, 0x7f0c010b

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    move-object v0, p0

    .line 77
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v2

    move v0, v1

    :goto_0
    invoke-virtual {v2}, Lcom/android/launcher3/Folder;->gs()Lcom/android/launcher3/CellLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher3/CellLayout;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Folder;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcai;->at(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_3
    instance-of v0, p0, Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_4

    .line 80
    const v0, 0x7f0c0145

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 82
    :cond_4
    instance-of v0, p0, Lyx;

    if-eqz v0, :cond_5

    .line 83
    const v0, 0x7f0c0166

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 87
    :cond_5
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 88
    check-cast p0, Landroid/view/ViewGroup;

    .line 89
    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 90
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lcai;->at(Landroid/view/View;)V

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
