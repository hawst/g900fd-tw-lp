.class public final Ljjm;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajm:Ljava/lang/String;

.field private eoX:J

.field private eoY:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 88
    iput v2, p0, Ljjm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljjm;->ajm:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljjm;->eoX:J

    iput-boolean v2, p0, Ljjm;->eoY:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljjm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjm;->eCz:I

    .line 89
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjm;->ajm:Ljava/lang/String;

    iget v0, p0, Ljjm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljjm;->eoX:J

    iget v0, p0, Ljjm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjm;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljjm;->eoY:Z

    iget v0, p0, Ljjm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjm;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 104
    iget v0, p0, Ljjm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iget-object v1, p0, Ljjm;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 107
    :cond_0
    iget v0, p0, Ljjm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 108
    const/4 v0, 0x2

    iget-wide v2, p0, Ljjm;->eoX:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 110
    :cond_1
    iget v0, p0, Ljjm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 111
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljjm;->eoY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 113
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 114
    return-void
.end method

.method public final bnU()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Ljjm;->eoX:J

    return-wide v0
.end method

.method public final bnV()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Ljjm;->eoY:Z

    return v0
.end method

.method public final du(J)Ljjm;
    .locals 1

    .prologue
    .line 55
    iput-wide p1, p0, Ljjm;->eoX:J

    .line 56
    iget v0, p0, Ljjm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjm;->aez:I

    .line 57
    return-object p0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljjm;->ajm:Ljava/lang/String;

    return-object v0
.end method

.method public final io(Z)Ljjm;
    .locals 1

    .prologue
    .line 74
    iput-boolean p1, p0, Ljjm;->eoY:Z

    .line 75
    iget v0, p0, Ljjm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjm;->aez:I

    .line 76
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 118
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 119
    iget v1, p0, Ljjm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 120
    const/4 v1, 0x1

    iget-object v2, p0, Ljjm;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_0
    iget v1, p0, Ljjm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 124
    const/4 v1, 0x2

    iget-wide v2, p0, Ljjm;->eoX:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_1
    iget v1, p0, Ljjm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 128
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljjm;->eoY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 131
    :cond_2
    return v0
.end method

.method public final wk(Ljava/lang/String;)Ljjm;
    .locals 1

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Ljjm;->ajm:Ljava/lang/String;

    .line 37
    iget v0, p0, Ljjm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjm;->aez:I

    .line 38
    return-object p0
.end method
