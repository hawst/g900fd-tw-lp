.class public final Ldj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private cZ:I

.field private er:Z

.field private et:Ldq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ldi;->b(Ljava/util/Locale;)Z

    move-result v0

    invoke-direct {p0, v0}, Ldj;->e(Z)V

    .line 139
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    invoke-static {p1}, Ldi;->b(Ljava/util/Locale;)Z

    move-result v0

    invoke-direct {p0, v0}, Ldj;->e(Z)V

    .line 157
    return-void
.end method

.method private e(Z)V
    .locals 1

    .prologue
    .line 165
    iput-boolean p1, p0, Ldj;->er:Z

    .line 166
    invoke-static {}, Ldi;->al()Ldq;

    move-result-object v0

    iput-object v0, p0, Ldj;->et:Ldq;

    .line 167
    const/4 v0, 0x2

    iput v0, p0, Ldj;->cZ:I

    .line 168
    return-void
.end method


# virtual methods
.method public final ao()Ldi;
    .locals 5

    .prologue
    .line 203
    iget v0, p0, Ldj;->cZ:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Ldj;->et:Ldq;

    invoke-static {}, Ldi;->al()Ldq;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 205
    iget-boolean v0, p0, Ldj;->er:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ldi;->am()Ldi;

    move-result-object v0

    .line 207
    :goto_0
    return-object v0

    .line 205
    :cond_0
    invoke-static {}, Ldi;->an()Ldi;

    move-result-object v0

    goto :goto_0

    .line 207
    :cond_1
    new-instance v0, Ldi;

    iget-boolean v1, p0, Ldj;->er:Z

    iget v2, p0, Ldj;->cZ:I

    iget-object v3, p0, Ldj;->et:Ldq;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Ldi;-><init>(ZILdq;B)V

    goto :goto_0
.end method
