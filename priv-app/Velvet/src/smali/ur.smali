.class public final Lur;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static Gl:F

.field public static Gm:F


# instance fields
.field private Gi:Ltu;

.field private Gj:F

.field private Gk:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/high16 v0, 0x42700000    # 60.0f

    sput v0, Lur;->Gl:F

    .line 37
    const/4 v0, 0x0

    sput v0, Lur;->Gm:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;IIIIII)V
    .locals 16

    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual/range {p2 .. p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v14

    .line 57
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 59
    invoke-static {}, Lyu;->im()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    move v13, v1

    .line 60
    :goto_0
    sget v1, Lur;->Gl:F

    invoke-static {v1, v14}, Lur;->a(FLandroid/util/DisplayMetrics;)I

    move-result v1

    int-to-float v1, v1

    sput v1, Lur;->Gm:F

    .line 62
    new-instance v1, Ltu;

    const-string v2, "Super Short Stubby"

    const/high16 v3, 0x437f0000    # 255.0f

    const/high16 v4, 0x43960000    # 300.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x40400000    # 3.0f

    const/high16 v7, 0x42400000    # 48.0f

    const/high16 v8, 0x41500000    # 13.0f

    if-eqz v13, :cond_1

    const/4 v9, 0x3

    :goto_1
    int-to-float v9, v9

    const/high16 v10, 0x42400000    # 48.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v1, Ltu;

    const-string v2, "Shorter Stubby"

    const/high16 v3, 0x437f0000    # 255.0f

    const/high16 v4, 0x43c80000    # 400.0f

    const/high16 v5, 0x40400000    # 3.0f

    const/high16 v6, 0x40400000    # 3.0f

    const/high16 v7, 0x42400000    # 48.0f

    const/high16 v8, 0x41500000    # 13.0f

    if-eqz v13, :cond_2

    const/4 v9, 0x3

    :goto_2
    int-to-float v9, v9

    const/high16 v10, 0x42400000    # 48.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v1, Ltu;

    const-string v2, "Short Stubby"

    const v3, 0x43898000    # 275.0f

    const/high16 v4, 0x43d20000    # 420.0f

    const/high16 v5, 0x40400000    # 3.0f

    const/high16 v6, 0x40800000    # 4.0f

    const/high16 v7, 0x42400000    # 48.0f

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x42400000    # 48.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    new-instance v1, Ltu;

    const-string v2, "Stubby"

    const/high16 v3, 0x437f0000    # 255.0f

    const/high16 v4, 0x43e10000    # 450.0f

    const/high16 v5, 0x40400000    # 3.0f

    const/high16 v6, 0x40800000    # 4.0f

    const/high16 v7, 0x42400000    # 48.0f

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x42400000    # 48.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v1, Ltu;

    const-string v2, "Nexus S"

    const/high16 v3, 0x43940000    # 296.0f

    const v4, 0x43f5aa3d

    const/high16 v5, 0x40800000    # 4.0f

    const/high16 v6, 0x40800000    # 4.0f

    const/high16 v7, 0x42400000    # 48.0f

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x42400000    # 48.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    new-instance v1, Ltu;

    const-string v2, "Nexus 4"

    const v3, 0x43a78000    # 335.0f

    const v4, 0x440dc000    # 567.0f

    const/high16 v5, 0x40800000    # 4.0f

    const/high16 v6, 0x40800000    # 4.0f

    sget v7, Lur;->Gl:F

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x42600000    # 56.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v1, Ltu;

    const-string v2, "Nexus 5"

    const v3, 0x43b38000    # 359.0f

    const v4, 0x440dc000    # 567.0f

    const/high16 v5, 0x40800000    # 4.0f

    const/high16 v6, 0x40800000    # 4.0f

    sget v7, Lur;->Gl:F

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x42600000    # 56.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    new-instance v1, Ltu;

    const-string v2, "Large Phone"

    const/high16 v3, 0x43cb0000    # 406.0f

    const v4, 0x442d8000    # 694.0f

    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v6, 0x40a00000    # 5.0f

    const/high16 v7, 0x42800000    # 64.0f

    const v8, 0x41666666    # 14.4f

    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x42600000    # 56.0f

    const v11, 0x7f070009

    const v12, 0x7f07000b

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance v1, Ltu;

    const-string v2, "Nexus 7"

    const v3, 0x440fc000    # 575.0f

    const/high16 v4, 0x44620000    # 904.0f

    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v6, 0x40c00000    # 6.0f

    const/high16 v7, 0x42900000    # 72.0f

    const v8, 0x41666666    # 14.4f

    const/high16 v9, 0x40e00000    # 7.0f

    const/high16 v10, 0x42700000    # 60.0f

    const v11, 0x7f07000d

    const v12, 0x7f07000f

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v1, Ltu;

    const-string v2, "Nexus 10"

    const v3, 0x4435c000    # 727.0f

    const v4, 0x4496e000    # 1207.0f

    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v6, 0x40c00000    # 6.0f

    const/high16 v7, 0x42980000    # 76.0f

    const v8, 0x41666666    # 14.4f

    const/high16 v9, 0x40e00000    # 7.0f

    const/high16 v10, 0x42800000    # 64.0f

    const v11, 0x7f07000d

    const v12, 0x7f07000f

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v1, Ltu;

    const-string v2, "20-inch Tablet"

    const v3, 0x44bee000    # 1527.0f

    const v4, 0x451df000    # 2527.0f

    const/high16 v5, 0x40e00000    # 7.0f

    const/high16 v6, 0x40e00000    # 7.0f

    const/high16 v7, 0x42c80000    # 100.0f

    const/high16 v8, 0x41a00000    # 20.0f

    const/high16 v9, 0x40e00000    # 7.0f

    const/high16 v10, 0x42900000    # 72.0f

    const v11, 0x7f070005

    const v12, 0x7f070007

    invoke-direct/range {v1 .. v12}, Ltu;-><init>(Ljava/lang/String;FFFFFFFFII)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    move/from16 v0, p3

    invoke-static {v0, v14}, Lur;->a(ILandroid/util/DisplayMetrics;)F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lur;->Gj:F

    .line 99
    move/from16 v0, p4

    invoke-static {v0, v14}, Lur;->a(ILandroid/util/DisplayMetrics;)F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lur;->Gk:F

    .line 100
    new-instance v1, Ltu;

    move-object/from16 v0, p0

    iget v4, v0, Lur;->Gj:F

    move-object/from16 v0, p0

    iget v5, v0, Lur;->Gk:F

    move-object/from16 v2, p1

    move-object v3, v15

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p2

    invoke-direct/range {v1 .. v10}, Ltu;-><init>(Landroid/content/Context;Ljava/util/ArrayList;FFIIIILandroid/content/res/Resources;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lur;->Gi:Ltu;

    .line 105
    return-void

    .line 59
    :cond_0
    const/4 v1, 0x0

    move v13, v1

    goto/16 :goto_0

    .line 62
    :cond_1
    const/4 v9, 0x5

    goto/16 :goto_1

    .line 65
    :cond_2
    const/4 v9, 0x5

    goto/16 :goto_2
.end method

.method public static a(ILandroid/util/DisplayMetrics;)F
    .locals 2

    .prologue
    .line 40
    iget v0, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    .line 41
    int-to-float v1, p0

    div-float v0, v1, v0

    return v0
.end method

.method public static a(FLandroid/util/DisplayMetrics;)I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    invoke-static {v0, p0, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static b(FLandroid/util/DisplayMetrics;)I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final gl()Ltu;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lur;->Gi:Ltu;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "-------- DYNAMIC GRID ------- \nWd: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->De:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Hd: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->Df:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", W: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->Dx:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", H: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->Dy:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [r: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->Dg:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", c: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->Dh:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->DI:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", its: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->DJ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cw: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->DL:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ch: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->DM:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->Di:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", his: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lur;->Gi:Ltu;

    iget v1, v1, Ltu;->DW:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
