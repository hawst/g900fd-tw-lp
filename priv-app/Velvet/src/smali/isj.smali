.class final Lisj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lisg;


# instance fields
.field private bvx:Z

.field private final dGU:Ljava/nio/ByteBuffer;

.field private final digest:Ljava/security/MessageDigest;


# direct methods
.method private constructor <init>(Ljava/security/MessageDigest;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lisj;->digest:Ljava/security/MessageDigest;

    .line 69
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lisj;->dGU:Ljava/nio/ByteBuffer;

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Ljava/security/MessageDigest;B)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lisj;-><init>(Ljava/security/MessageDigest;)V

    return-void
.end method

.method private aYv()V
    .locals 2

    .prologue
    .line 165
    iget-boolean v0, p0, Lisj;->bvx:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot use Hasher after calling #hash() on it"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 166
    return-void

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Q(Ljava/lang/CharSequence;)Lisg;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 144
    move v0, v1

    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 145
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-direct {p0}, Lisj;->aYv()V

    iget-object v3, p0, Lisj;->dGU:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lisj;->digest:Ljava/security/MessageDigest;

    iget-object v3, p0, Lisj;->dGU:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v1, v4}, Ljava/security/MessageDigest;->update([BII)V

    iget-object v2, p0, Lisj;->dGU:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_0
    return-object p0
.end method

.method public final aYm()Lisb;
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lisj;->bvx:Z

    .line 170
    iget-object v0, p0, Lisj;->digest:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lisc;->ab([B)Lisb;

    move-result-object v0

    return-object v0
.end method

.method public final aa([B)Lisg;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lisj;->aYv()V

    .line 80
    iget-object v0, p0, Lisj;->digest:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 81
    return-object p0
.end method
