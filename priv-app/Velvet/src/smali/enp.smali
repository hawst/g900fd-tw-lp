.class public abstract Lenp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aWp:Ljava/lang/String;

.field private final cgf:[I

.field final cgg:Ljava/util/concurrent/Executor;

.field final cgh:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field final mRunner:Lerk;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;Lerk;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lenp;->cgh:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 53
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lenp;->aWp:Ljava/lang/String;

    .line 54
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerk;

    iput-object v0, p0, Lenp;->mRunner:Lerk;

    .line 55
    iput-object v2, p0, Lenp;->cgg:Ljava/util/concurrent/Executor;

    .line 56
    iput-object v2, p0, Lenp;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 57
    iput-object p3, p0, Lenp;->cgf:[I

    .line 58
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lenp;->cgh:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lenp;->aWp:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lenp;->mRunner:Lerk;

    .line 47
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lenp;->cgg:Ljava/util/concurrent/Executor;

    .line 48
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lenp;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 49
    iput-object p4, p0, Lenp;->cgf:[I

    .line 50
    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)Lenp;
    .locals 6

    .prologue
    .line 61
    new-instance v0, Lenq;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lenp;->aWp:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", background"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lenp;->cgf:[I

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lenq;-><init>(Lenp;Ljava/lang/Class;Ljava/lang/String;[I[Ljava/lang/Object;)V

    .line 70
    iget-object v1, p0, Lenp;->mRunner:Lerk;

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lenp;->mRunner:Lerk;

    invoke-interface {v1, v0}, Lerk;->a(Leri;)V

    .line 75
    :goto_0
    return-object p0

    .line 73
    :cond_0
    iget-object v1, p0, Lenp;->mBgExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lenp;->cgh:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 83
    return-void
.end method

.method protected varargs abstract doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method
