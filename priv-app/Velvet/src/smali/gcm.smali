.class public final Lgcm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgcl;


# instance fields
.field private final cFR:Z

.field private final mSessionParams:Lgnj;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Lgnj;Lgdo;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lgcm;->mSessionParams:Lgnj;

    .line 31
    iput-object p2, p0, Lgcm;->mSpeechSettings:Lgdo;

    .line 32
    iput-boolean p3, p0, Lgcm;->cFR:Z

    .line 33
    return-void
.end method

.method private aEQ()Z
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->getMode()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lgcm;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFg()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->aHE()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgcm;->cFR:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aER()Ljze;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lgcm;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    return-object v0
.end method

.method private fI(Z)Z
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->getMode()I

    move-result v0

    invoke-static {v0}, Lgnj;->kr(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->getMode()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->aHE()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    iget-object v0, p0, Lgcm;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFf()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final aEN()Ljava/util/List;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-static {v5}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 38
    invoke-direct {p0, v0}, Lgcm;->fI(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_0
    invoke-direct {p0}, Lgcm;->aEQ()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 42
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    :cond_1
    iget-object v2, p0, Lgcm;->mSpeechSettings:Lgdo;

    invoke-interface {v2}, Lgdo;->aFd()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v2}, Lgnj;->aHL()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v2}, Lgnj;->getMode()I

    move-result v2

    if-ne v2, v4, :cond_2

    invoke-direct {p0}, Lgcm;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNu:Ljzz;

    if-nez v2, :cond_7

    :cond_2
    move v2, v1

    :goto_0
    if-nez v2, :cond_4

    iget-object v2, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v2}, Lgnj;->getMode()I

    move-result v2

    if-ne v2, v6, :cond_3

    invoke-direct {p0}, Lgcm;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNu:Ljzz;

    if-nez v2, :cond_8

    :cond_3
    move v2, v1

    :goto_1
    if-eqz v2, :cond_9

    :cond_4
    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 45
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_5
    iget-object v2, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v2}, Lgnj;->getMode()I

    move-result v2

    if-ne v2, v6, :cond_a

    :goto_3
    if-eqz v0, :cond_6

    .line 48
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_6
    return-object v3

    .line 44
    :cond_7
    invoke-direct {p0}, Lgcm;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNu:Ljzz;

    invoke-virtual {v2}, Ljzz;->bxw()Z

    move-result v2

    goto :goto_0

    :cond_8
    invoke-direct {p0}, Lgcm;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNu:Ljzz;

    invoke-virtual {v2}, Ljzz;->bxA()Z

    move-result v2

    goto :goto_1

    :cond_9
    move v2, v1

    goto :goto_2

    :cond_a
    move v0, v1

    .line 47
    goto :goto_3
.end method

.method public final aEO()I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 55
    iget-object v3, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->getMode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 69
    :pswitch_0
    invoke-direct {p0}, Lgcm;->aEQ()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 58
    :pswitch_1
    invoke-direct {p0, v2}, Lgcm;->fI(Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 60
    invoke-direct {p0}, Lgcm;->aEQ()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_1
    const-string v0, "EngineSelectorImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No primary engine for mode: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->getMode()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v2

    .line 64
    goto :goto_0

    .line 67
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 71
    :cond_2
    invoke-direct {p0, v2}, Lgcm;->fI(Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    iget-boolean v1, p0, Lgcm;->cFR:Z

    if-nez v1, :cond_0

    .line 73
    const-string v1, "EngineSelectorImpl"

    const-string v3, "Offline: Embedded engine only"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :cond_3
    const-string v0, "EngineSelectorImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No primary engine for mode: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->getMode()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v2

    .line 78
    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final aEP()I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 85
    iget-object v3, p0, Lgcm;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->getMode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 96
    :pswitch_0
    invoke-virtual {p0}, Lgcm;->aEO()I

    move-result v3

    if-ne v3, v1, :cond_0

    invoke-direct {p0, v0}, Lgcm;->fI(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 100
    :cond_0
    :goto_0
    :pswitch_1
    return v0

    .line 90
    :pswitch_2
    invoke-virtual {p0}, Lgcm;->aEO()I

    move-result v3

    if-ne v3, v2, :cond_0

    invoke-direct {p0}, Lgcm;->aEQ()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 91
    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
