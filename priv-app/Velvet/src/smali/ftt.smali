.class public final Lftt;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 37
    return-void
.end method

.method private a(Landroid/view/View;Lapp;I)V
    .locals 7

    .prologue
    const/16 v6, 0x11

    const/4 v5, 0x0

    .line 122
    if-nez p2, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    const v0, 0x7f1102e2

    invoke-virtual {p2}, Lapp;->tf()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lftt;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 126
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 127
    invoke-virtual {p2}, Lapp;->tg()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lftt;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lftt;->mContext:Landroid/content/Context;

    const v3, 0x7f0a04d1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/text/SpannableStringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    const/4 v4, -0x1

    invoke-direct {v3, v4}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, v3, v5, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, v3, v5, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v3, Landroid/text/style/TypefaceSpan;

    const-string v4, "sans-serif-condensed"

    invoke-direct {v3, v4}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, v3, v5, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v2, v3, v5, v0, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 130
    const-string v0, " "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 132
    :cond_2
    invoke-virtual {p2}, Lapp;->th()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 133
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 134
    const v0, 0x7f1102e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 135
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected final aCZ()V
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    const/4 v8, 0x0

    const v7, 0x7f1101a8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47
    invoke-virtual {p0, v2}, Lftt;->fA(Z)V

    .line 49
    iget-object v3, p0, Lfro;->mView:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v4, v0, Lanh;->aho:Lanc;

    .line 52
    const v0, 0x7f1101a9

    invoke-virtual {v4}, Lanc;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lftt;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 53
    iget-object v0, v4, Lanc;->agd:Lapp;

    if-nez v0, :cond_3

    .line 54
    const v0, 0x7f1102e2

    invoke-virtual {v4}, Lanc;->on()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lftt;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 55
    const v0, 0x7f1100b0

    invoke-virtual {v4}, Lanc;->oo()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v5}, Lftt;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 61
    :goto_0
    invoke-virtual {v4}, Lanc;->oj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v4}, Lanc;->oq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lanc;->or()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v1

    :goto_1
    if-nez v0, :cond_1

    .line 62
    invoke-virtual {v4}, Lanc;->oj()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v7, v0}, Lftt;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 65
    :cond_1
    invoke-virtual {v4}, Lanc;->op()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const v0, 0x7f1100b0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    iget-object v3, p0, Lftt;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201cd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 71
    const/16 v4, 0xa8

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 72
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 73
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-double v4, v2

    mul-double/2addr v4, v10

    double-to-int v2, v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v10

    double-to-int v4, v4

    invoke-virtual {v3, v1, v1, v2, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 75
    new-instance v2, Landroid/graphics/drawable/ScaleDrawable;

    const/high16 v4, 0x3f000000    # 0.5f

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-direct {v2, v3, v1, v4, v5}, Landroid/graphics/drawable/ScaleDrawable;-><init>(Landroid/graphics/drawable/Drawable;IFF)V

    .line 76
    invoke-static {v0, v2, v8, v8, v8}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 79
    :cond_2
    invoke-virtual {p0, v7}, Lftt;->jy(I)V

    .line 80
    return-void

    .line 57
    :cond_3
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->aho:Lanc;

    iget-object v0, v0, Lanc;->agd:Lapp;

    iget-object v5, p0, Lftt;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00b1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {p0, v3, v0, v5}, Lftt;->a(Landroid/view/View;Lapp;I)V

    goto :goto_0

    .line 61
    :cond_4
    invoke-virtual {v4}, Lanc;->gj()I

    move-result v0

    invoke-virtual {v4}, Lanc;->gi()I

    move-result v5

    if-gt v0, v5, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iget-object v5, p0, Lftt;->mContext:Landroid/content/Context;

    invoke-static {v5}, Leot;->au(Landroid/content/Context;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4}, Lanc;->gi()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual {v0}, Lcom/google/android/search/shared/ui/WebImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    invoke-virtual {v0, v6}, Lcom/google/android/search/shared/ui/WebImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v5}, Lcom/google/android/search/shared/ui/WebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v4}, Lanc;->oj()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lftt;->mCardContainer:Lfmt;

    invoke-interface {v6}, Lfmt;->aAD()Lfml;

    move-result-object v6

    iget-object v6, v6, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v5, v6}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    move v0, v2

    goto/16 :goto_1
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 42
    const v0, 0x7f040104

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final jw(I)V
    .locals 6

    .prologue
    .line 113
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 114
    const v1, 0x7f1101ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 115
    const/16 v2, 0xe5

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    .line 117
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 118
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->aho:Lanc;

    iget-object v1, v1, Lanc;->agd:Lapp;

    invoke-direct {p0, v0, v1, v2}, Lftt;->a(Landroid/view/View;Lapp;I)V

    .line 119
    return-void
.end method
