.class public final Ljlj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile erD:[Ljlj;


# instance fields
.field private aez:I

.field public erE:Ljmu;

.field public erF:[Ljlk;

.field private erG:[Ljlp;

.field private erH:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10929
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10930
    const/4 v0, 0x0

    iput v0, p0, Ljlj;->aez:I

    iput-object v1, p0, Ljlj;->erE:Ljmu;

    invoke-static {}, Ljlk;->boS()[Ljlk;

    move-result-object v0

    iput-object v0, p0, Ljlj;->erF:[Ljlk;

    invoke-static {}, Ljlp;->bpp()[Ljlp;

    move-result-object v0

    iput-object v0, p0, Ljlj;->erG:[Ljlp;

    const-string v0, ""

    iput-object v0, p0, Ljlj;->erH:Ljava/lang/String;

    iput-object v1, p0, Ljlj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlj;->eCz:I

    .line 10931
    return-void
.end method

.method public static boR()[Ljlj;
    .locals 2

    .prologue
    .line 10885
    sget-object v0, Ljlj;->erD:[Ljlj;

    if-nez v0, :cond_1

    .line 10886
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 10888
    :try_start_0
    sget-object v0, Ljlj;->erD:[Ljlj;

    if-nez v0, :cond_0

    .line 10889
    const/4 v0, 0x0

    new-array v0, v0, [Ljlj;

    sput-object v0, Ljlj;->erD:[Ljlj;

    .line 10891
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10893
    :cond_1
    sget-object v0, Ljlj;->erD:[Ljlj;

    return-object v0

    .line 10891
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10288
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljlj;->erE:Ljmu;

    if-nez v0, :cond_1

    new-instance v0, Ljmu;

    invoke-direct {v0}, Ljmu;-><init>()V

    iput-object v0, p0, Ljlj;->erE:Ljmu;

    :cond_1
    iget-object v0, p0, Ljlj;->erE:Ljmu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlj;->erF:[Ljlk;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlk;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljlj;->erF:[Ljlk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljlk;

    invoke-direct {v3}, Ljlk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljlj;->erF:[Ljlk;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljlk;

    invoke-direct {v3}, Ljlk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljlj;->erF:[Ljlk;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlj;->erG:[Ljlp;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlp;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljlj;->erG:[Ljlp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljlp;

    invoke-direct {v3}, Ljlp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljlj;->erG:[Ljlp;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljlp;

    invoke-direct {v3}, Ljlp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljlj;->erG:[Ljlp;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlj;->erH:Ljava/lang/String;

    iget v0, p0, Ljlj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlj;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10947
    iget-object v0, p0, Ljlj;->erE:Ljmu;

    if-eqz v0, :cond_0

    .line 10948
    const/4 v0, 0x1

    iget-object v2, p0, Ljlj;->erE:Ljmu;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 10950
    :cond_0
    iget-object v0, p0, Ljlj;->erF:[Ljlk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljlj;->erF:[Ljlk;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 10951
    :goto_0
    iget-object v2, p0, Ljlj;->erF:[Ljlk;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 10952
    iget-object v2, p0, Ljlj;->erF:[Ljlk;

    aget-object v2, v2, v0

    .line 10953
    if-eqz v2, :cond_1

    .line 10954
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 10951
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10958
    :cond_2
    iget-object v0, p0, Ljlj;->erG:[Ljlp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljlj;->erG:[Ljlp;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 10959
    :goto_1
    iget-object v0, p0, Ljlj;->erG:[Ljlp;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 10960
    iget-object v0, p0, Ljlj;->erG:[Ljlp;

    aget-object v0, v0, v1

    .line 10961
    if-eqz v0, :cond_3

    .line 10962
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 10959
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 10966
    :cond_4
    iget v0, p0, Ljlj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 10967
    const/4 v0, 0x5

    iget-object v1, p0, Ljlj;->erH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 10969
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10970
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 10974
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10975
    iget-object v2, p0, Ljlj;->erE:Ljmu;

    if-eqz v2, :cond_0

    .line 10976
    const/4 v2, 0x1

    iget-object v3, p0, Ljlj;->erE:Ljmu;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 10979
    :cond_0
    iget-object v2, p0, Ljlj;->erF:[Ljlk;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljlj;->erF:[Ljlk;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 10980
    :goto_0
    iget-object v3, p0, Ljlj;->erF:[Ljlk;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 10981
    iget-object v3, p0, Ljlj;->erF:[Ljlk;

    aget-object v3, v3, v0

    .line 10982
    if-eqz v3, :cond_1

    .line 10983
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 10980
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 10988
    :cond_3
    iget-object v2, p0, Ljlj;->erG:[Ljlp;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljlj;->erG:[Ljlp;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 10989
    :goto_1
    iget-object v2, p0, Ljlj;->erG:[Ljlp;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 10990
    iget-object v2, p0, Ljlj;->erG:[Ljlp;

    aget-object v2, v2, v1

    .line 10991
    if-eqz v2, :cond_4

    .line 10992
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 10989
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 10997
    :cond_5
    iget v1, p0, Ljlj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    .line 10998
    const/4 v1, 0x5

    iget-object v2, p0, Ljlj;->erH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11001
    :cond_6
    return v0
.end method
