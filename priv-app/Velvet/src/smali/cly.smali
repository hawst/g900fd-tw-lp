.class final Lcly;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lisu;


# instance fields
.field private final aTu:Lbzh;

.field private final bbn:[B

.field private final bbo:I

.field final bbp:Ljava/lang/Object;

.field bbq:Lclz;

.field private bbr:Z

.field private bbs:Lefq;

.field private final bbt:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private synthetic bbu:Lclu;

.field private mHeaders:Ldyj;


# direct methods
.method constructor <init>(Lclu;Lbzh;[BI)V
    .locals 2
    .param p2    # Lbzh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 114
    iput-object p1, p0, Lcly;->bbu:Lclu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcly;->bbp:Ljava/lang/Object;

    .line 111
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcly;->bbt:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 115
    iput-object p2, p0, Lcly;->aTu:Lbzh;

    .line 116
    iput-object p3, p0, Lcly;->bbn:[B

    .line 117
    iput p4, p0, Lcly;->bbo:I

    .line 118
    return-void
.end method

.method private PL()Lcnr;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 126
    iget-object v0, p0, Lcly;->bbt:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 128
    iget v0, p0, Lcly;->bbo:I

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 130
    iget-object v0, p0, Lcly;->bbu:Lclu;

    iget-object v0, v0, Lclu;->mSdchManager:Lczz;

    iget-object v1, p0, Lcly;->aTu:Lbzh;

    invoke-virtual {v0, v1}, Lczz;->a(Lbzh;)V

    .line 131
    const/4 v1, 0x0

    .line 134
    :try_start_0
    iget-object v0, p0, Lcly;->bbu:Lclu;

    iget-object v0, p0, Lcly;->aTu:Lbzh;

    iget-object v4, p0, Lcly;->bbn:[B

    invoke-static {v0, v4}, Lclu;->a(Lbzh;[B)V

    .line 135
    new-instance v0, Ldyj;

    iget-object v4, p0, Lcly;->aTu:Lbzh;

    invoke-interface {v4}, Lbzh;->getHeaderFields()Ljava/util/Map;

    move-result-object v4

    invoke-direct {v0, v4}, Ldyj;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lefs; {:try_start_0 .. :try_end_0} :catch_1

    .line 136
    :try_start_1
    iget-object v4, p0, Lcly;->bbp:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catch Left; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lefs; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    iget-object v1, p0, Lcly;->mHeaders:Ldyj;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcly;->bbs:Lefq;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lifv;->gY(Z)V

    iput-object v0, p0, Lcly;->mHeaders:Ldyj;

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p0}, Lcly;->PM()V
    :try_end_3
    .catch Left; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lefs; {:try_start_3 .. :try_end_3} :catch_1

    .line 149
    :goto_1
    iget-object v1, p0, Lcly;->bbu:Lclu;

    iget-object v1, v1, Lclu;->mSdchManager:Lczz;

    iget-object v2, p0, Lcly;->aTu:Lbzh;

    invoke-virtual {v1, v2}, Lczz;->b(Lbzh;)Ljava/io/InputStream;

    move-result-object v1

    .line 150
    new-instance v2, Lcnr;

    iget-object v3, p0, Lcly;->bbu:Lclu;

    iget v3, v3, Lclu;->bbl:I

    iget-object v4, p0, Lcly;->bbu:Lclu;

    iget-object v4, v4, Lclu;->mGsaConfig:Lchk;

    invoke-direct {v2, v0, v1, v3, v4}, Lcnr;-><init>(Ldyj;Ljava/io/InputStream;ILchk;)V

    return-object v2

    :cond_0
    move v1, v3

    .line 136
    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v4

    throw v1
    :try_end_4
    .catch Left; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lefs; {:try_start_4 .. :try_end_4} :catch_1

    .line 137
    :catch_0
    move-exception v1

    .line 139
    :goto_2
    new-instance v4, Ldyj;

    iget-object v5, p0, Lcly;->aTu:Lbzh;

    invoke-interface {v5}, Lbzh;->getHeaderFields()Ljava/util/Map;

    move-result-object v5

    invoke-direct {v4, v5}, Ldyj;-><init>(Ljava/util/Map;)V

    iget-object v5, p0, Lcly;->bbp:Ljava/lang/Object;

    monitor-enter v5

    :try_start_5
    iget-object v6, p0, Lcly;->mHeaders:Ldyj;

    if-nez v6, :cond_1

    iget-object v6, p0, Lcly;->bbs:Lefq;

    if-nez v6, :cond_1

    :goto_3
    invoke-static {v2}, Lifv;->gY(Z)V

    iput-object v4, p0, Lcly;->mHeaders:Ldyj;

    iput-object v1, p0, Lcly;->bbs:Lefq;

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-virtual {p0}, Lcly;->PM()V

    goto :goto_1

    :cond_1
    move v2, v3

    goto :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    .line 140
    :catch_1
    move-exception v0

    .line 145
    iget-object v4, p0, Lcly;->bbp:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    iget-object v1, p0, Lcly;->mHeaders:Ldyj;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcly;->bbs:Lefq;

    if-nez v1, :cond_2

    move v1, v2

    :goto_4
    invoke-static {v1}, Lifv;->gY(Z)V

    iput-object v0, p0, Lcly;->bbs:Lefq;

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    invoke-virtual {p0}, Lcly;->PM()V

    .line 146
    throw v0

    :cond_2
    move v1, v3

    .line 145
    goto :goto_4

    :catchall_2
    move-exception v0

    monitor-exit v4

    throw v0

    .line 137
    :catch_2
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_2
.end method


# virtual methods
.method PM()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 187
    .line 191
    iget-object v3, p0, Lcly;->bbp:Ljava/lang/Object;

    monitor-enter v3

    .line 192
    :try_start_0
    iget-boolean v1, p0, Lcly;->bbr:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcly;->bbq:Lclz;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcly;->mHeaders:Ldyj;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcly;->bbs:Lefq;

    if-eqz v1, :cond_3

    .line 195
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcly;->bbr:Z

    .line 196
    iget-object v2, p0, Lcly;->bbq:Lclz;

    .line 197
    iget-object v1, p0, Lcly;->mHeaders:Ldyj;

    .line 198
    iget-object v0, p0, Lcly;->bbs:Lefq;

    .line 200
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    if-eqz v2, :cond_1

    .line 203
    if-eqz v1, :cond_2

    .line 204
    check-cast v0, Left;

    invoke-interface {v2, v1, v0}, Lclz;->a(Ldyj;Left;)V

    .line 209
    :cond_1
    :goto_1
    return-void

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 206
    :cond_2
    invoke-interface {v2, v0}, Lclz;->a(Lefq;)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    move-object v2, v0

    goto :goto_0
.end method

.method public final synthetic PN()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcly;->PL()Lcnr;

    move-result-object v0

    return-object v0
.end method
