.class public final Lcad;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static aPX:Ljava/util/concurrent/atomic/AtomicLong;

.field private static final aPY:Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;

.field private static volatile aPZ:Lbur;

.field private static aPv:Lcah;

.field private static aQa:Z

.field public static aQb:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static aQc:Lcaf;

.field public static volatile aQd:Ljava/lang/String;

.field public static aQe:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcad;->aPX:Ljava/util/concurrent/atomic/AtomicLong;

    .line 66
    new-instance v0, Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;

    invoke-direct {v0}, Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;-><init>()V

    sput-object v0, Lcad;->aPY:Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;

    .line 72
    sput-boolean v1, Lcad;->aQa:Z

    .line 73
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcad;->aQb:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 74
    new-instance v0, Lcaf;

    invoke-direct {v0}, Lcaf;-><init>()V

    sput-object v0, Lcad;->aQc:Lcaf;

    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcad;->aQd:Ljava/lang/String;

    .line 77
    const-string v0, "GEL"

    sput-object v0, Lcad;->aQe:Ljava/lang/String;

    return-void
.end method

.method private static declared-synchronized Cu()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 134
    const-class v1, Lcad;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcad;->aPv:Lcah;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 144
    :cond_0
    :goto_0
    monitor-exit v1

    return-object v0

    .line 138
    :cond_1
    :try_start_1
    sget-object v0, Lcad;->aPv:Lcah;

    const-string v2, "GEL.app_install_id"

    iget-object v0, v0, Lcah;->KH:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    if-nez v0, :cond_0

    .line 140
    invoke-static {}, Leoi;->auW()Ljava/lang/String;

    move-result-object v0

    .line 141
    sget-object v2, Lcad;->aPv:Lcah;

    const-string v3, "GEL.app_install_id"

    iget-object v2, v2, Lcah;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static Cv()Z
    .locals 4

    .prologue
    .line 344
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 345
    sget-object v2, Lcad;->aPX:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1499700

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static Cw()Z
    .locals 1

    .prologue
    .line 703
    sget-boolean v0, Lcad;->aQa:Z

    return v0
.end method

.method public static Cx()Lbur;
    .locals 1

    .prologue
    .line 715
    sget-object v0, Lcad;->aPZ:Lbur;

    return-object v0
.end method

.method static synthetic Cy()Lbur;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcad;->aPZ:Lbur;

    return-object v0
.end method

.method static synthetic Cz()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcad;->Cu()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILandroid/view/View;I)V
    .locals 3

    .prologue
    .line 279
    :try_start_0
    new-instance v0, Ljxz;

    invoke-direct {v0}, Ljxz;-><init>()V

    invoke-static {p0, p1, v0, p2}, Lcad;->a(ILandroid/view/View;Ljxz;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :goto_0
    return-void

    .line 280
    :catch_0
    move-exception v0

    .line 282
    const-string v1, "GelLogger"

    const-string v2, "Crash in logInteraction(userAction, view)."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(ILandroid/view/View;Ljxz;I)V
    .locals 5

    .prologue
    .line 416
    invoke-static {}, Lenu;->auR()V

    .line 417
    sget-boolean v0, Lcad;->aQa:Z

    if-nez v0, :cond_0

    .line 432
    :goto_0
    return-void

    .line 421
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 422
    new-instance v2, Ljyb;

    invoke-direct {v2}, Ljyb;-><init>()V

    .line 423
    invoke-static {p1}, Lehd;->aG(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    invoke-virtual {v2, v3}, Ljyb;->tb(I)Ljyb;

    .line 424
    :goto_1
    invoke-virtual {v2, p0}, Ljyb;->td(I)Ljyb;

    .line 425
    invoke-static {p1, p2}, Lcad;->b(Landroid/view/View;Ljxz;)V

    .line 426
    invoke-static {p1, p2}, Lcad;->c(Landroid/view/View;Ljxz;)V

    .line 427
    invoke-static {p2}, Lcad;->a(Ljxz;)V

    .line 428
    invoke-static {p2}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljyb;->aE([B)Ljyb;

    .line 429
    invoke-static {v2}, Lcad;->a(Ljyb;)V

    .line 430
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    .line 431
    invoke-static {v0, v1, v2, v3}, Lcad;->b(JJ)V

    goto :goto_0

    .line 423
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    :cond_2
    invoke-virtual {v2, p3}, Ljyb;->tb(I)Ljyb;

    goto :goto_1
.end method

.method public static a(Landroid/view/View;Ljxz;)V
    .locals 0

    .prologue
    .line 495
    invoke-static {p0, p1}, Lcad;->b(Landroid/view/View;Ljxz;)V

    .line 496
    invoke-static {p0, p1}, Lcad;->c(Landroid/view/View;Ljxz;)V

    .line 497
    return-void
.end method

.method public static a(Lbur;)V
    .locals 0

    .prologue
    .line 709
    sput-object p0, Lcad;->aPZ:Lbur;

    .line 710
    return-void
.end method

.method public static a(Lcaf;)V
    .locals 0

    .prologue
    .line 733
    sput-object p0, Lcad;->aQc:Lcaf;

    .line 734
    return-void
.end method

.method private static a(Ljxz;)V
    .locals 1

    .prologue
    .line 565
    sget-object v0, Lcad;->aQc:Lcaf;

    iget v0, v0, Lcaf;->aQf:I

    invoke-virtual {p0, v0}, Ljxz;->sW(I)Ljxz;

    .line 566
    return-void
.end method

.method private static a(Ljyb;)V
    .locals 4

    .prologue
    .line 469
    invoke-static {}, Lenu;->auR()V

    .line 470
    sget-boolean v0, Lcad;->aQa:Z

    if-nez v0, :cond_1

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    sget-object v0, Lemo;->cfz:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    new-instance v0, Ljxx;

    invoke-direct {v0}, Ljxx;-><init>()V

    .line 476
    iput-object p0, v0, Ljxx;->eLs:Ljyb;

    .line 477
    invoke-virtual {p0}, Ljyb;->bvX()I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_2

    .line 479
    iget-object v1, v0, Ljxx;->eLs:Ljyb;

    sget-object v2, Lcad;->aQc:Lcaf;

    iget v2, v2, Lcaf;->aQg:I

    invoke-virtual {v1, v2}, Ljyb;->te(I)Ljyb;

    .line 482
    :cond_2
    iget-object v1, v0, Ljxx;->eLs:Ljyb;

    invoke-virtual {p0}, Ljyb;->bwa()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljyb;->aE([B)Ljyb;

    .line 484
    sget-object v1, Lcad;->aPZ:Lbur;

    sget-object v2, Lcad;->aQe:Ljava/lang/String;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lbur;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lern;ZLjava/lang/String;Lcah;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 95
    :try_start_0
    sget-boolean v0, Lcad;->aQa:Z

    if-ne p2, v0, :cond_0

    move v0, v7

    .line 124
    :goto_0
    return v0

    .line 106
    :cond_0
    sput-boolean p2, Lcad;->aQa:Z

    .line 107
    sput-object p4, Lcad;->aPv:Lcah;

    .line 108
    sput-object p3, Lcad;->aQe:Ljava/lang/String;

    .line 109
    if-nez p2, :cond_1

    .line 112
    invoke-static {}, Lcad;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v7

    .line 117
    goto :goto_0

    .line 114
    :cond_1
    :try_start_1
    invoke-static {}, Lenu;->auR()V

    sget-boolean v0, Lcad;->aQa:Z

    if-nez v0, :cond_3

    :cond_2
    :goto_1
    move v0, v8

    .line 115
    goto :goto_0

    .line 114
    :cond_3
    sget-object v0, Lcad;->aPZ:Lbur;

    if-nez v0, :cond_2

    new-instance v5, Lemo;

    invoke-direct {v5}, Lemo;-><init>()V

    new-instance v0, Lbur;

    const/16 v2, 0x1c

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbur;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lbus;Z)V

    sput-object v0, Lcad;->aPZ:Lbur;

    sget-object v0, Lcad;->aQb:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Lcae;

    const-string v1, "Call GelLogger.start()"

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Lcae;-><init>(Ljava/lang/String;[I)V

    invoke-interface {p1, v0}, Lern;->a(Leri;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "GelLogger"

    const-string v2, "Crash in start."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 118
    :catch_1
    move-exception v0

    .line 120
    const-string v1, "GelLogger"

    const-string v2, "Crash in updateLoggingStatus."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    .line 124
    goto :goto_0

    .line 114
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static af(II)V
    .locals 4

    .prologue
    .line 244
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 245
    invoke-static {}, Lenu;->auR()V

    .line 246
    sget-boolean v2, Lcad;->aQa:Z

    if-nez v2, :cond_0

    .line 267
    :goto_0
    return-void

    .line 251
    :cond_0
    new-instance v2, Ljyb;

    invoke-direct {v2}, Ljyb;-><init>()V

    .line 252
    invoke-virtual {v2, p1}, Ljyb;->tb(I)Ljyb;

    .line 253
    invoke-virtual {v2, p0}, Ljyb;->td(I)Ljyb;

    .line 254
    new-instance v3, Ljxz;

    invoke-direct {v3}, Ljxz;-><init>()V

    .line 255
    invoke-static {v3}, Lcad;->a(Ljxz;)V

    .line 256
    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljyb;->aE([B)Ljyb;

    .line 257
    invoke-static {v2}, Lcad;->a(Ljyb;)V

    .line 258
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    .line 259
    invoke-static {v0, v1, v2, v3}, Lcad;->b(JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 262
    const-string v1, "GelLogger"

    const-string v2, "Crash in logInteraction(userAction, veType)."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(ILandroid/view/View;I)V
    .locals 3

    .prologue
    .line 298
    :try_start_0
    new-instance v0, Ljxz;

    invoke-direct {v0}, Ljxz;-><init>()V

    .line 299
    sget-object v1, Lcad;->aQc:Lcaf;

    iget v1, v1, Lcaf;->aQi:I

    if-lez v1, :cond_0

    .line 300
    sget-object v1, Lcad;->aQc:Lcaf;

    iget v1, v1, Lcaf;->aQi:I

    invoke-virtual {v0, v1}, Ljxz;->ta(I)Ljxz;

    .line 302
    :cond_0
    invoke-static {p0, p1, v0, p2}, Lcad;->a(ILandroid/view/View;Ljxz;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :goto_0
    return-void

    .line 303
    :catch_0
    move-exception v0

    .line 305
    const-string v1, "GelLogger"

    const-string v2, "Crash in logAllAppsInteraction()."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(JJ)V
    .locals 4

    .prologue
    .line 440
    sub-long v0, p2, p0

    long-to-int v0, v0

    .line 441
    new-instance v1, Ljxx;

    invoke-direct {v1}, Ljxx;-><init>()V

    .line 442
    new-instance v2, Ljxy;

    invoke-direct {v2}, Ljxy;-><init>()V

    iput-object v2, v1, Ljxx;->eLt:Ljxy;

    .line 443
    iget-object v2, v1, Ljxx;->eLt:Ljxy;

    invoke-virtual {v2, v0}, Ljxy;->sV(I)Ljxy;

    .line 444
    sget-object v0, Lcad;->aPZ:Lbur;

    sget-object v2, Lcad;->aQe:Ljava/lang/String;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lbur;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 446
    return-void
.end method

.method public static b(Landroid/view/View;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 356
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    .line 357
    invoke-static {}, Lenu;->auR()V

    .line 358
    sget-boolean v0, Lcad;->aQa:Z

    if-nez v0, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    sget-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    if-eqz p1, :cond_2

    const v0, 0x7f0c00d1

    invoke-static {p1, v0}, Lehd;->r(Landroid/view/View;I)V

    :cond_2
    const v0, 0x7f1103c5

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0146

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    const v0, 0x7f11022f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0119

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    const v0, 0x7f11029f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0165

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    const v0, 0x7f1102a0

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0167

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    const v0, 0x7f1102a1

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0155

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    const v0, 0x7f11023e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcai;->at(Landroid/view/View;)V

    const v0, 0x7f11023f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/Hotseat;

    const v1, 0x7f0c0111

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    invoke-static {v0}, Lcai;->at(Landroid/view/View;)V

    .line 369
    sget-object v0, Lcad;->aPY:Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lehd;->a(Landroid/view/View;Lehe;Z)Ljava/util/List;

    move-result-object v0

    .line 371
    new-instance v1, Ljxx;

    invoke-direct {v1}, Ljxx;-><init>()V

    .line 372
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljyc;

    iput-object v4, v1, Ljxx;->eLr:[Ljyc;

    .line 374
    const-class v4, Ljyc;

    invoke-static {v0, v4}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljyc;

    iput-object v0, v1, Ljxx;->eLr:[Ljyc;

    .line 378
    sget-object v0, Lcad;->aPZ:Lbur;

    sget-object v4, Lcad;->aQe:Ljava/lang/String;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v6}, Lbur;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 379
    sget-object v0, Lcad;->aPX:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 380
    iget-object v0, v1, Ljxx;->eLr:[Ljyc;

    invoke-static {v0}, Lehd;->a([Ljyc;)V

    .line 381
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 382
    sub-long/2addr v0, v2

    long-to-int v0, v0

    new-instance v1, Ljxx;

    invoke-direct {v1}, Ljxx;-><init>()V

    new-instance v2, Ljxy;

    invoke-direct {v2}, Ljxy;-><init>()V

    iput-object v2, v1, Ljxx;->eLt:Ljxy;

    iget-object v2, v1, Ljxx;->eLt:Ljxy;

    invoke-virtual {v2, v0}, Ljxy;->sU(I)Ljxy;

    sget-object v0, Lcad;->aPZ:Lbur;

    sget-object v2, Lcad;->aQe:Ljava/lang/String;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lbur;->a(Ljava/lang/String;[B[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 390
    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public static b(Landroid/view/View;Ljxz;)V
    .locals 2

    .prologue
    .line 507
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ladh;

    if-nez v0, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 511
    invoke-virtual {v0}, Ladh;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_0

    .line 516
    sget-object v1, Lcad;->aQd:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 519
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 524
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcad;->aQd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 529
    invoke-virtual {p1, v0}, Ljxz;->sX(I)Ljxz;

    goto :goto_0
.end method

.method private static c(Landroid/view/View;Ljxz;)V
    .locals 4

    .prologue
    .line 540
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lwq;

    if-nez v0, :cond_1

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 544
    iget-wide v2, v0, Lwq;->id:J

    .line 546
    invoke-virtual {p1, v2, v3}, Ljxz;->dP(J)Ljxz;

    .line 547
    iget v1, v0, Lwq;->Bb:I

    .line 549
    invoke-virtual {p1, v1}, Ljxz;->sY(I)Ljxz;

    .line 550
    iget v1, v0, Lwq;->Bc:I

    .line 552
    invoke-virtual {p1, v1}, Ljxz;->sZ(I)Ljxz;

    .line 553
    iget-wide v0, v0, Lwq;->Bd:J

    .line 555
    invoke-virtual {p1, v0, v1}, Ljxz;->dO(J)Ljxz;

    goto :goto_0
.end method

.method public static ca(Z)V
    .locals 0

    .prologue
    .line 721
    sput-boolean p0, Lcad;->aQa:Z

    .line 722
    return-void
.end method

.method public static cb(Z)V
    .locals 2

    .prologue
    .line 727
    sget-object v0, Lemo;->cfz:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 728
    return-void
.end method

.method public static ev(I)V
    .locals 1

    .prologue
    .line 699
    sget-object v0, Lcad;->aQc:Lcaf;

    iput p0, v0, Lcaf;->aQi:I

    .line 700
    return-void
.end method

.method public static l(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 321
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lrr;

    if-eqz v0, :cond_0

    .line 324
    const/16 v0, 0x1f

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v0, v1}, Lcad;->af(II)V

    .line 336
    :goto_0
    return-void

    .line 327
    :cond_0
    const/16 v0, 0x1f

    const/4 v1, -0x1

    invoke-static {v0, p0, v1}, Lcad;->a(ILandroid/view/View;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 329
    :catch_0
    move-exception v0

    .line 331
    const-string v1, "GelLogger"

    const-string v2, "Crash in logLongPressInteraction."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static m(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 675
    :try_start_0
    sget-object v0, Lcad;->aQc:Lcaf;

    invoke-virtual {v0, p1}, Lcaf;->ew(I)I

    .line 676
    sget-object v0, Lcad;->aQc:Lcaf;

    iget v0, v0, Lcaf;->aQg:I

    if-eqz v0, :cond_0

    .line 678
    const/16 v1, 0x15

    sget-object v0, Lcad;->aQc:Lcaf;

    iget-object v2, v0, Lcaf;->aQh:Landroid/view/View;

    if-nez p0, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-static {v1, v2, v0}, Lcad;->a(ILandroid/view/View;I)V

    .line 684
    :cond_0
    sget-object v0, Lcad;->aQc:Lcaf;

    invoke-virtual {v0, p1, p0}, Lcaf;->b(ILandroid/view/View;)V

    .line 692
    :goto_1
    return-void

    .line 678
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0145

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 685
    :catch_0
    move-exception v0

    .line 687
    const-string v1, "GelLogger"

    const-string v2, "Crash in logPageSwitch()."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static stop()V
    .locals 3

    .prologue
    .line 209
    :try_start_0
    invoke-static {}, Lenu;->auR()V

    .line 212
    sget-object v0, Lcad;->aPZ:Lbur;

    if-eqz v0, :cond_0

    .line 213
    sget-object v0, Lcad;->aPZ:Lbur;

    iget-object v0, v0, Lbur;->aGS:Lbpd;

    invoke-virtual {v0}, Lbpd;->stop()V

    .line 215
    sget-object v0, Lcad;->aQb:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 216
    const/4 v0, 0x0

    sput-object v0, Lcad;->aPZ:Lbur;

    .line 217
    const/4 v0, 0x0

    sput-boolean v0, Lcad;->aQa:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 226
    :catch_0
    move-exception v0

    .line 228
    const-string v1, "GelLogger"

    const-string v2, "Crash in stop()."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
