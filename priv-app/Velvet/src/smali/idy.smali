.class public final Lidy;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dza:Ljava/lang/String;

.field public dzb:[I

.field private dzc:I

.field private dzd:Z

.field private dze:Ljava/lang/String;

.field private dzf:Ljava/lang/String;

.field private dzg:Ljava/lang/String;

.field private dzh:Ljava/lang/String;

.field private dzi:Ljava/lang/String;

.field private dzj:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 370
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 371
    iput v1, p0, Lidy;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lidy;->dza:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Lidy;->dzb:[I

    iput v1, p0, Lidy;->dzc:I

    iput-boolean v1, p0, Lidy;->dzd:Z

    const-string v0, ""

    iput-object v0, p0, Lidy;->dze:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lidy;->dzf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lidy;->dzg:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lidy;->dzh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lidy;->dzi:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lidy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lidy;->eCz:I

    .line 372
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 175
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lidy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidy;->dza:Ljava/lang/String;

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lidy;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lidy;->dzb:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lidy;->dzb:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lidy;->dzb:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lidy;->dzb:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lidy;->dzb:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Lidy;->dzb:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lidy;->dzb:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lidy;->dzb:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lidy;->dzb:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lidy;->dzd:Z

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidy;->dze:Ljava/lang/String;

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidy;->dzf:Ljava/lang/String;

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidy;->dzg:Ljava/lang/String;

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidy;->dzi:Ljava/lang/String;

    iget v0, p0, Lidy;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lidy;->dzc:I

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidy;->dzh:Ljava/lang/String;

    iget v0, p0, Lidy;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lidy;->aez:I

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v3, p0, Lidy;->dzj:[Ljava/lang/String;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_7

    :cond_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    iput-object v1, p0, Lidy;->dzj:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 394
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 395
    const/4 v0, 0x1

    iget-object v2, p0, Lidy;->dza:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 397
    :cond_0
    iget-object v0, p0, Lidy;->dzb:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lidy;->dzb:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 398
    :goto_0
    iget-object v2, p0, Lidy;->dzb:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 399
    const/4 v2, 0x2

    iget-object v3, p0, Lidy;->dzb:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 398
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 402
    :cond_1
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 403
    const/4 v0, 0x3

    iget-boolean v2, p0, Lidy;->dzd:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 405
    :cond_2
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 406
    const/4 v0, 0x4

    iget-object v2, p0, Lidy;->dze:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 408
    :cond_3
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 409
    const/4 v0, 0x5

    iget-object v2, p0, Lidy;->dzf:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 411
    :cond_4
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 412
    const/4 v0, 0x6

    iget-object v2, p0, Lidy;->dzg:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 414
    :cond_5
    iget v0, p0, Lidy;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 415
    const/4 v0, 0x7

    iget-object v2, p0, Lidy;->dzi:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 417
    :cond_6
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 418
    const/16 v0, 0x8

    iget v2, p0, Lidy;->dzc:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 420
    :cond_7
    iget v0, p0, Lidy;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 421
    const/16 v0, 0x9

    iget-object v2, p0, Lidy;->dzh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 423
    :cond_8
    iget-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 424
    :goto_1
    iget-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 425
    iget-object v0, p0, Lidy;->dzj:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 426
    if-eqz v0, :cond_9

    .line 427
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 424
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 431
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 432
    return-void
.end method

.method public final aVd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lidy;->dzi:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 436
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 437
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 438
    const/4 v1, 0x1

    iget-object v3, p0, Lidy;->dza:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 441
    :cond_0
    iget-object v1, p0, Lidy;->dzb:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lidy;->dzb:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 443
    :goto_0
    iget-object v4, p0, Lidy;->dzb:[I

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 444
    iget-object v4, p0, Lidy;->dzb:[I

    aget v4, v4, v1

    .line 445
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 443
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 448
    :cond_1
    add-int/2addr v0, v3

    .line 449
    iget-object v1, p0, Lidy;->dzb:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 451
    :cond_2
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 452
    const/4 v1, 0x3

    iget-boolean v3, p0, Lidy;->dzd:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 455
    :cond_3
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 456
    const/4 v1, 0x4

    iget-object v3, p0, Lidy;->dze:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    :cond_4
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 460
    const/4 v1, 0x5

    iget-object v3, p0, Lidy;->dzf:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 463
    :cond_5
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 464
    const/4 v1, 0x6

    iget-object v3, p0, Lidy;->dzg:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_6
    iget v1, p0, Lidy;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 468
    const/4 v1, 0x7

    iget-object v3, p0, Lidy;->dzi:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_7
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_8

    .line 472
    const/16 v1, 0x8

    iget v3, p0, Lidy;->dzc:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    :cond_8
    iget v1, p0, Lidy;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 476
    const/16 v1, 0x9

    iget-object v3, p0, Lidy;->dzh:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    :cond_9
    iget-object v1, p0, Lidy;->dzj:[Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lidy;->dzj:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_c

    move v1, v2

    move v3, v2

    .line 482
    :goto_1
    iget-object v4, p0, Lidy;->dzj:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_b

    .line 483
    iget-object v4, p0, Lidy;->dzj:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 484
    if-eqz v4, :cond_a

    .line 485
    add-int/lit8 v3, v3, 0x1

    .line 486
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 482
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 490
    :cond_b
    add-int/2addr v0, v1

    .line 491
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 493
    :cond_c
    return v0
.end method

.method public final ph(Ljava/lang/String;)Lidy;
    .locals 1

    .prologue
    .line 351
    if-nez p1, :cond_0

    .line 352
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 354
    :cond_0
    iput-object p1, p0, Lidy;->dzi:Ljava/lang/String;

    .line 355
    iget v0, p0, Lidy;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lidy;->aez:I

    .line 356
    return-object p0
.end method
