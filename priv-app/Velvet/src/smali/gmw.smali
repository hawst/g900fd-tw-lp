.class public final Lgmw;
.super Lgmq;
.source "PG"


# instance fields
.field private final cPE:Z

.field private final cwm:I

.field private final mDeviceParams:Lgmz;

.field private final mNetworkInformation:Lgno;


# direct methods
.method public constructor <init>(ZLgno;Lgmz;I)V
    .locals 1
    .param p3    # Lgmz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 26
    const-string v0, "SoundSearchInfoBuilderTask"

    invoke-direct {p0, v0}, Lgmq;-><init>(Ljava/lang/String;)V

    .line 27
    iput-boolean p1, p0, Lgmw;->cPE:Z

    .line 28
    iput-object p2, p0, Lgmw;->mNetworkInformation:Lgno;

    .line 29
    iput-object p3, p0, Lgmw;->mDeviceParams:Lgmz;

    .line 30
    iput p4, p0, Lgmw;->cwm:I

    .line 31
    return-void
.end method


# virtual methods
.method protected final synthetic aHr()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lidy;

    invoke-direct {v0}, Lidy;-><init>()V

    iget-object v1, p0, Lgmw;->mNetworkInformation:Lgno;

    invoke-virtual {v1}, Lgno;->aAo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Lidy;->ph(Ljava/lang/String;)Lidy;

    :cond_0
    :goto_0
    new-array v1, v4, [I

    iput-object v1, v0, Lidy;->dzb:[I

    iget v1, p0, Lgmw;->cwm:I

    if-nez v1, :cond_2

    iget-object v1, v0, Lidy;->dzb:[I

    aput v3, v1, v3

    :goto_1
    new-instance v1, Liec;

    invoke-direct {v1}, Liec;-><init>()V

    invoke-virtual {v1, v5}, Liec;->lM(I)Liec;

    invoke-virtual {v1, v5}, Liec;->lN(I)Liec;

    new-instance v2, Ljxk;

    invoke-direct {v2}, Ljxk;-><init>()V

    iput-object v0, v2, Ljxk;->eKO:Lidy;

    iput-object v1, v2, Ljxk;->eKP:Liec;

    iget-boolean v0, p0, Lgmw;->cPE:Z

    invoke-virtual {v2, v0}, Ljxk;->jt(Z)Ljxk;

    return-object v2

    :cond_1
    iget-object v1, p0, Lgmw;->mDeviceParams:Lgmz;

    invoke-interface {v1}, Lgmz;->NB()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lidy;->ph(Ljava/lang/String;)Lidy;

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lidy;->dzb:[I

    aput v4, v1, v3

    goto :goto_1
.end method
