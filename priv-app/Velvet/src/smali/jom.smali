.class public final Ljom;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewC:[Ljom;


# instance fields
.field public ewD:Ljpd;

.field public ewy:Ljor;

.field private ewz:Ljos;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1277
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1278
    iput-object v0, p0, Ljom;->ewD:Ljpd;

    iput-object v0, p0, Ljom;->ewy:Ljor;

    iput-object v0, p0, Ljom;->ewz:Ljos;

    iput-object v0, p0, Ljom;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljom;->eCz:I

    .line 1279
    return-void
.end method

.method public static brq()[Ljom;
    .locals 2

    .prologue
    .line 1257
    sget-object v0, Ljom;->ewC:[Ljom;

    if-nez v0, :cond_1

    .line 1258
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1260
    :try_start_0
    sget-object v0, Ljom;->ewC:[Ljom;

    if-nez v0, :cond_0

    .line 1261
    const/4 v0, 0x0

    new-array v0, v0, [Ljom;

    sput-object v0, Ljom;->ewC:[Ljom;

    .line 1263
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1265
    :cond_1
    sget-object v0, Ljom;->ewC:[Ljom;

    return-object v0

    .line 1263
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1251
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljom;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljom;->ewD:Ljpd;

    if-nez v0, :cond_1

    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    iput-object v0, p0, Ljom;->ewD:Ljpd;

    :cond_1
    iget-object v0, p0, Ljom;->ewD:Ljpd;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljom;->ewy:Ljor;

    if-nez v0, :cond_2

    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    iput-object v0, p0, Ljom;->ewy:Ljor;

    :cond_2
    iget-object v0, p0, Ljom;->ewy:Ljor;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljom;->ewz:Ljos;

    if-nez v0, :cond_3

    new-instance v0, Ljos;

    invoke-direct {v0}, Ljos;-><init>()V

    iput-object v0, p0, Ljom;->ewz:Ljos;

    :cond_3
    iget-object v0, p0, Ljom;->ewz:Ljos;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1293
    iget-object v0, p0, Ljom;->ewD:Ljpd;

    if-eqz v0, :cond_0

    .line 1294
    const/4 v0, 0x1

    iget-object v1, p0, Ljom;->ewD:Ljpd;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1296
    :cond_0
    iget-object v0, p0, Ljom;->ewy:Ljor;

    if-eqz v0, :cond_1

    .line 1297
    const/4 v0, 0x2

    iget-object v1, p0, Ljom;->ewy:Ljor;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1299
    :cond_1
    iget-object v0, p0, Ljom;->ewz:Ljos;

    if-eqz v0, :cond_2

    .line 1300
    const/4 v0, 0x3

    iget-object v1, p0, Ljom;->ewz:Ljos;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1302
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1303
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1307
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1308
    iget-object v1, p0, Ljom;->ewD:Ljpd;

    if-eqz v1, :cond_0

    .line 1309
    const/4 v1, 0x1

    iget-object v2, p0, Ljom;->ewD:Ljpd;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1312
    :cond_0
    iget-object v1, p0, Ljom;->ewy:Ljor;

    if-eqz v1, :cond_1

    .line 1313
    const/4 v1, 0x2

    iget-object v2, p0, Ljom;->ewy:Ljor;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1316
    :cond_1
    iget-object v1, p0, Ljom;->ewz:Ljos;

    if-eqz v1, :cond_2

    .line 1317
    const/4 v1, 0x3

    iget-object v2, p0, Ljom;->ewz:Ljos;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1320
    :cond_2
    return v0
.end method
