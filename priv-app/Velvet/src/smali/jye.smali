.class public final Ljye;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dzl:F

.field public eBt:[Ljkt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Ljye;->aez:I

    const/4 v0, 0x0

    iput v0, p0, Ljye;->dzl:F

    invoke-static {}, Ljkt;->bot()[Ljkt;

    move-result-object v0

    iput-object v0, p0, Ljye;->eBt:[Ljkt;

    const/4 v0, 0x0

    iput-object v0, p0, Ljye;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljye;->eCz:I

    .line 83
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljye;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljye;->dzl:F

    iget v0, p0, Ljye;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljye;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljye;->eBt:[Ljkt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljkt;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljye;->eBt:[Ljkt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljkt;

    invoke-direct {v3}, Ljkt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljye;->eBt:[Ljkt;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljkt;

    invoke-direct {v3}, Ljkt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljye;->eBt:[Ljkt;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 97
    iget v0, p0, Ljye;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    iget v1, p0, Ljye;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 100
    :cond_0
    iget-object v0, p0, Ljye;->eBt:[Ljkt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljye;->eBt:[Ljkt;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 101
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljye;->eBt:[Ljkt;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 102
    iget-object v1, p0, Ljye;->eBt:[Ljkt;

    aget-object v1, v1, v0

    .line 103
    if-eqz v1, :cond_1

    .line 104
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 101
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 109
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 113
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 114
    iget v1, p0, Ljye;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 115
    const/4 v1, 0x1

    iget v2, p0, Ljye;->dzl:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 118
    :cond_0
    iget-object v1, p0, Ljye;->eBt:[Ljkt;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljye;->eBt:[Ljkt;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 119
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljye;->eBt:[Ljkt;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 120
    iget-object v2, p0, Ljye;->eBt:[Ljkt;

    aget-object v2, v2, v0

    .line 121
    if-eqz v2, :cond_1

    .line 122
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 119
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 127
    :cond_3
    return v0
.end method
