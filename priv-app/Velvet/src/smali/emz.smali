.class public final Lemz;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lena;

    invoke-direct {v0}, Lena;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/concurrent/Executor;Lemy;)Lemy;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lenc;

    invoke-direct {v0, p0, p1}, Lenc;-><init>(Ljava/util/concurrent/Executor;Lemy;)V

    return-object v0
.end method

.method public static a(Leps;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 104
    const-wide/16 v0, -0x1

    :try_start_0
    invoke-static {p0, v0, v1}, Lemz;->a(Leps;J)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 105
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Leps;J)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 117
    invoke-interface {p0}, Leps;->auB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 119
    new-instance v1, Lene;

    invoke-direct {v1, v0}, Lene;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    invoke-interface {p0, v1}, Leps;->e(Lemy;)V

    .line 126
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_1

    .line 127
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_0
    invoke-interface {p0}, Leps;->auC()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 129
    :cond_1
    :try_start_1
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "Timed out getting Now from NowOrLater"

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to get Now from NowOrLater"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Livq;Lemy;Lerp;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lend;

    invoke-direct {v0, p3, p0, p1}, Lend;-><init>(Ljava/lang/String;Livq;Lemy;)V

    invoke-interface {p2}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 91
    return-void
.end method

.method public static a(Ljava/util/concurrent/Executor;Lemy;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 31
    if-nez p0, :cond_0

    .line 32
    invoke-interface {p1, p2}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 46
    :goto_0
    return-void

    .line 35
    :cond_0
    new-instance v0, Lenb;

    invoke-direct {v0, p1, p2}, Lenb;-><init>(Lemy;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
