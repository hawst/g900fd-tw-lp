.class public final Lcqq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bgA:Ljava/lang/Object;

.field final bgB:Landroid/util/LruCache;

.field final bgC:Lcqw;

.field final bgz:I

.field final mClock:Lemp;


# direct methods
.method public constructor <init>(Lcfo;)V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcqq;->bgA:Ljava/lang/Object;

    .line 34
    invoke-virtual {p1}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->IP()I

    move-result v0

    iput v0, p0, Lcqq;->bgz:I

    .line 35
    invoke-virtual {p1}, Lcfo;->DC()Lemp;

    move-result-object v0

    iput-object v0, p0, Lcqq;->mClock:Lemp;

    .line 36
    new-instance v0, Landroid/util/LruCache;

    invoke-virtual {p1}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->IO()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcqq;->bgB:Landroid/util/LruCache;

    .line 38
    new-instance v0, Lcqw;

    invoke-virtual {p1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-virtual {p1}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-virtual {p1}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcqw;-><init>(Lcke;Lcrh;Landroid/database/DataSetObservable;)V

    iput-object v0, p0, Lcqq;->bgC:Lcqw;

    .line 41
    invoke-virtual {p1}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v0

    new-instance v1, Lcqr;

    invoke-direct {v1, p0}, Lcqr;-><init>(Lcqq;)V

    invoke-virtual {v0, v1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 58
    return-void
.end method


# virtual methods
.method final Sl()V
    .locals 2

    .prologue
    .line 62
    iget-object v1, p0, Lcqq;->bgA:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lcqq;->bgB:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 64
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final iu(Ljava/lang/String;)Ldfw;
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lcqq;->bgA:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcqq;->bgC:Lcqw;

    invoke-virtual {v0, p1}, Lcqw;->ix(Ljava/lang/String;)Ldfw;

    move-result-object v0

    monitor-exit v1

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcqq;->bgB:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfw;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
