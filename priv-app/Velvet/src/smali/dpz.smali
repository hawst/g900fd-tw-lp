.class public final Ldpz;
.super Lebm;
.source "PG"

# interfaces
.implements Ldpx;


# instance fields
.field final bFc:Ldqa;

.field private bFd:F

.field bFe:Ldpy;

.field final bFf:Ldqk;

.field bFg:Lcom/google/android/shared/search/Suggestion;

.field final bvh:Leeh;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Lerp;Leqp;Leax;ZLandroid/graphics/Rect;Landroid/os/Bundle;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;)V
    .locals 13
    .param p7    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 72
    new-instance v5, Ldpv;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, Ldpv;-><init>(Landroid/content/Context;)V

    new-instance v6, Lcaj;

    invoke-direct {v6}, Lcaj;-><init>()V

    const/4 v12, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v0 .. v12}, Lebm;-><init>(Landroid/widget/FrameLayout;Lerp;Leqp;Leax;Leld;Ldtc;ZLandroid/graphics/Rect;Landroid/os/Bundle;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;Z)V

    .line 77
    new-instance v0, Ldqk;

    iget-object v1, p0, Ldpz;->bUf:Lcom/google/android/search/shared/ui/ReverseDrawRestrictedLayout;

    iget-object v2, p0, Ldpz;->bUh:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, v2}, Ldqk;-><init>(Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Ldpz;->bFf:Ldqk;

    .line 78
    new-instance v0, Ldqa;

    invoke-direct {v0, p0}, Ldqa;-><init>(Ldpz;)V

    iput-object v0, p0, Ldpz;->bFc:Ldqa;

    .line 79
    iget-object v0, p0, Ldpz;->bUg:Landroid/widget/FrameLayout;

    new-instance v1, Ldqe;

    invoke-direct {v1, p0}, Ldqe;-><init>(Ldpz;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 80
    new-instance v0, Leeh;

    iget-object v1, p0, Ldpz;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Leeh;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldpz;->bvh:Leeh;

    .line 81
    return-void
.end method

.method static synthetic a(Ldpz;)V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0}, Ldpz;->amL()V

    return-void
.end method

.method static synthetic a(Ldpz;Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Ldpz;->aP(Lcom/google/android/shared/search/Query;)V

    return-void
.end method

.method static synthetic b(Ldpz;)Lequ;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldpz;->mSpeechLevelSource:Lequ;

    return-object v0
.end method

.method static synthetic c(Ldpz;)Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldpz;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    return-object v0
.end method

.method static synthetic d(Ldpz;)Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldpz;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    return-object v0
.end method

.method static synthetic e(Ldpz;)Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldpz;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    return-object v0
.end method

.method static synthetic f(Ldpz;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldpz;->bUg:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic g(Ldpz;)Lecc;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldpz;->bTW:Lecc;

    return-object v0
.end method


# virtual methods
.method protected final Cq()V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Ldpz;->bFe:Ldpy;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Ldpz;->bFe:Ldpy;

    invoke-interface {v0}, Ldpy;->Cq()V

    .line 317
    :cond_0
    return-void
.end method

.method public final D(F)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    .line 169
    cmpl-float v0, p1, v5

    if-lez v0, :cond_0

    iget v0, p0, Ldpz;->bFd:F

    cmpg-float v0, v0, v5

    if-lez v0, :cond_1

    :cond_0
    cmpg-float v0, p1, v5

    if-gtz v0, :cond_4

    iget v0, p0, Ldpz;->bFd:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_4

    :cond_1
    move v0, v1

    .line 172
    :goto_0
    iput p1, p0, Ldpz;->bFd:F

    .line 173
    iget-object v3, p0, Ldpz;->bFf:Ldqk;

    invoke-virtual {v3, p1}, Ldqk;->D(F)V

    .line 174
    iget-boolean v3, p0, Ldpz;->bUy:Z

    if-eqz v3, :cond_2

    .line 177
    iget-object v3, p0, Ldpz;->bFf:Ldqk;

    iget v3, v3, Ldqk;->bFd:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    .line 180
    :cond_2
    if-eqz v0, :cond_3

    .line 181
    iget v0, p0, Ldpz;->bFd:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_5

    :goto_1
    invoke-virtual {p0, v1}, Ldpz;->eH(Z)V

    .line 183
    iget v0, p0, Ldpz;->bUx:I

    if-nez v0, :cond_3

    .line 184
    const/4 v0, 0x2

    invoke-virtual {p0, v2, v0, v2}, Ldpz;->d(IIZ)V

    .line 188
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 169
    goto :goto_0

    :cond_5
    move v1, v2

    .line 181
    goto :goto_1
.end method

.method public final a(Landroid/view/View;Luq;ZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 290
    iget-object v2, p0, Ldpz;->bFg:Lcom/google/android/shared/search/Suggestion;

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Ldpz;->bFg:Lcom/google/android/shared/search/Suggestion;

    .line 292
    if-eqz v2, :cond_0

    .line 293
    iget-object v3, p0, Lebm;->ann:Lecq;

    if-eqz p4, :cond_2

    if-nez p3, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Ldpz;->bUp:Ledu;

    invoke-virtual {v4}, Ledu;->anZ()Lehj;

    move-result-object v4

    invoke-virtual {v4}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v4

    invoke-virtual {v3, v2, v0, v4}, Lecq;->a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V

    .line 296
    :cond_0
    if-nez p4, :cond_1

    .line 297
    iput-boolean v1, p2, Luq;->Gh:Z

    .line 299
    :cond_1
    invoke-virtual {p0}, Ldpz;->amL()V

    .line 300
    return-void

    :cond_2
    move v0, v1

    .line 293
    goto :goto_0
.end method

.method public final a(Ldpy;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Ldpz;->bFe:Ldpy;

    .line 198
    return-void
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 391
    invoke-super {p0, p1}, Lebm;->a(Letj;)V

    .line 392
    const-string v0, "mProximityToNow"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Ldpz;->bFd:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 393
    const-string v0, "mDraggingSuggestion"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldpz;->bFg:Lcom/google/android/shared/search/Suggestion;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 395
    return-void
.end method

.method protected final aO(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 219
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 223
    :cond_0
    invoke-super {p0, v0}, Lebm;->aO(Lcom/google/android/shared/search/Query;)V

    .line 224
    return-void
.end method

.method protected final aeA()Z
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lebm;->aeA()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldpz;->bFc:Ldqa;

    invoke-virtual {v0}, Ldqa;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldpz;->bFg:Lcom/google/android/shared/search/Suggestion;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aeB()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Ldpz;->bFc:Ldqa;

    invoke-virtual {v0}, Ldqa;->isActive()Z

    move-result v0

    return v0
.end method

.method protected final aeC()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Ldpz;->bFc:Ldqa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldqa;->dL(Z)V

    .line 208
    return-void
.end method

.method public final aet()Ldyg;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Ldpz;->bFc:Ldqa;

    return-object v0
.end method

.method protected final aew()Lebz;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ldqc;

    invoke-direct {v0, p0}, Ldqc;-><init>(Ldpz;)V

    return-object v0
.end method

.method protected final aex()Leen;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Ldqd;

    invoke-direct {v0, p0}, Ldqd;-><init>(Ldpz;)V

    return-object v0
.end method

.method protected final aey()Ldsk;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Ldqb;

    invoke-direct {v0, p0}, Ldqb;-><init>(Ldpz;)V

    return-object v0
.end method

.method protected final aez()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Ldpz;->bFf:Ldqk;

    invoke-virtual {v0}, Ldqk;->aeI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    .line 108
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lebm;->aez()I

    move-result v0

    goto :goto_0
.end method

.method protected final ao(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final dJ(Z)V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0, p1}, Lebm;->dJ(Z)V

    .line 142
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->anx()V

    .line 143
    return-void
.end method

.method public final dK(Z)V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Ldpz;->bFf:Ldqk;

    iput-boolean p1, v0, Ldqk;->bFu:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldqk;->hN:Z

    iget-object v1, v0, Ldqk;->bFp:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v0}, Ldqk;->getTranslationY()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v0, v0, Ldqk;->bFr:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 310
    return-void
.end method

.method public final ek()V
    .locals 0

    .prologue
    .line 285
    return-void
.end method

.method public final el()Z
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    return v0
.end method

.method public final em()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method public final en()Z
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    return v0
.end method

.method public final eo()F
    .locals 1

    .prologue
    .line 304
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final ge(I)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Ldpz;->bFf:Ldqk;

    invoke-virtual {v0, p1}, Ldqk;->gh(I)V

    .line 193
    return-void
.end method

.method protected final kf(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Ldpz;->bFf:Ldqk;

    invoke-virtual {v0}, Ldqk;->aeI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const-string v0, "android-search-app"

    .line 120
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lebm;->kf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final v(IZ)V
    .locals 2

    .prologue
    .line 147
    if-nez p1, :cond_0

    iget-boolean v0, p0, Ldpz;->bUy:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldpz;->bFd:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 148
    :cond_0
    iget-object v0, p0, Ldpz;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-boolean v1, p0, Ldpz;->bUy:Z

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->gY(I)V

    .line 150
    :cond_1
    return-void
.end method

.method protected final x(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 126
    invoke-super {p0, p1}, Lebm;->x(Landroid/os/Bundle;)V

    .line 127
    if-eqz p1, :cond_0

    .line 128
    const-string v0, "search_overlay_impl:search_plate_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "search_overlay_impl:search_plate_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 131
    iget-object v1, p0, Ldpz;->bFf:Ldqk;

    invoke-virtual {v1, v0}, Ldqk;->gf(I)V

    .line 132
    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldpz;->bUz:Z

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Ldpz;->d(IIZ)V

    .line 137
    :cond_0
    return-void
.end method
