.class public final Lagw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field private synthetic WN:Lcom/android/launcher3/Workspace;

.field private XB:F

.field private XC:F

.field private XD:Z

.field private XE:Landroid/view/Choreographer;

.field private XF:J

.field private XG:F

.field private XH:I

.field private hN:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Workspace;)V
    .locals 2

    .prologue
    .line 1328
    iput-object p1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1315
    const/4 v0, 0x0

    iput v0, p0, Lagw;->XB:F

    .line 1316
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lagw;->XC:F

    .line 1323
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Lagw;->XE:Landroid/view/Choreographer;

    .line 1330
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lagw;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 1331
    return-void
.end method

.method private ax(Z)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    const v9, 0x33d6bf95    # 1.0E-7f

    const/4 v2, 0x0

    .line 1339
    iget-boolean v0, p0, Lagw;->XD:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_2

    .line 1340
    :cond_0
    iput-boolean v2, p0, Lagw;->XD:Z

    .line 1341
    iget v3, p0, Lagw;->XC:F

    iget-boolean v0, p0, Lagw;->hN:Z

    if-eqz v0, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lagw;->XF:J

    sub-long/2addr v4, v6

    long-to-float v0, v4

    const/high16 v6, 0x437a0000    # 250.0f

    div-float/2addr v0, v6

    iget-object v6, p0, Lagw;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v6, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    iget v6, p0, Lagw;->XG:F

    iget v7, p0, Lagw;->XB:F

    iget v8, p0, Lagw;->XG:F

    sub-float/2addr v7, v8

    mul-float/2addr v0, v7

    add-float/2addr v0, v6

    iput v0, p0, Lagw;->XC:F

    const-wide/16 v6, 0xfa

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lagw;->hN:Z

    :goto_1
    iget v0, p0, Lagw;->XC:F

    iget v4, p0, Lagw;->XB:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v9

    if-lez v0, :cond_1

    invoke-direct {p0}, Lagw;->lD()V

    :cond_1
    iget v0, p0, Lagw;->XC:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v9

    if-lez v0, :cond_5

    :goto_2
    if-eqz v1, :cond_2

    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->f(Lcom/android/launcher3/Workspace;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1343
    :try_start_0
    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->e(Lcom/android/launcher3/Workspace;)Landroid/app/WallpaperManager;

    move-result-object v0

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1}, Lcom/android/launcher3/Workspace;->f(Lcom/android/launcher3/Workspace;)Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    iget-object v2, v2, Lcom/android/launcher3/Workspace;->Wf:Lagw;

    iget v2, v2, Lagw;->XC:F

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    .line 1345
    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->h(Lcom/android/launcher3/Workspace;)I

    move-result v0

    int-to-float v0, v0

    div-float v0, v10, v0

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/Workspace;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1}, Lcom/android/launcher3/Workspace;->e(Lcom/android/launcher3/Workspace;)Landroid/app/WallpaperManager;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0, v2}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1, v0}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Workspace;F)F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1351
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v0, v2

    .line 1341
    goto :goto_0

    :cond_4
    iget v0, p0, Lagw;->XB:F

    iput v0, p0, Lagw;->XC:F

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    .line 1346
    :catch_0
    move-exception v0

    .line 1347
    const-string v1, "Launcher.Workspace"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error updating wallpaper offset: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private lA()I
    .locals 2

    .prologue
    .line 1422
    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1423
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1424
    const/4 v0, 0x1

    .line 1426
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lB()I
    .locals 2

    .prologue
    .line 1431
    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    invoke-direct {p0}, Lagw;->lA()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1432
    return v0
.end method

.method private lD()V
    .locals 1

    .prologue
    .line 1477
    iget-boolean v0, p0, Lagw;->XD:Z

    if-nez v0, :cond_0

    .line 1478
    iget-object v0, p0, Lagw;->XE:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lagw;->XD:Z

    .line 1481
    :cond_0
    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 1

    .prologue
    .line 1335
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lagw;->ax(Z)V

    .line 1336
    return-void
.end method

.method public final lC()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1436
    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-gt v0, v6, :cond_2

    move v0, v3

    .line 1437
    :goto_0
    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    iget-object v1, v1, Lcom/android/launcher3/Workspace;->Wf:Lagw;

    invoke-direct {v1}, Lagw;->lD()V

    invoke-static {v0, v7}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, v1, Lagw;->XB:F

    invoke-direct {v1}, Lagw;->lB()I

    move-result v0

    iget v2, v1, Lagw;->XH:I

    if-eq v0, v2, :cond_1

    iget v0, v1, Lagw;->XH:I

    if-lez v0, :cond_0

    iput-boolean v6, v1, Lagw;->hN:Z

    iget v0, v1, Lagw;->XC:F

    iput v0, v1, Lagw;->XG:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lagw;->XF:J

    :cond_0
    invoke-direct {v1}, Lagw;->lB()I

    move-result v0

    iput v0, v1, Lagw;->XH:I

    .line 1438
    :cond_1
    invoke-direct {p0, v6}, Lagw;->ax(Z)V

    .line 1439
    return-void

    .line 1436
    :cond_2
    invoke-direct {p0}, Lagw;->lA()I

    move-result v0

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v1

    iget-object v4, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    sub-int v0, v4, v0

    iget-object v4, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_1
    iget-object v4, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4, v0}, Lcom/android/launcher3/Workspace;->bz(I)I

    move-result v0

    iget-object v4, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4, v1}, Lcom/android/launcher3/Workspace;->bz(I)I

    move-result v1

    sub-int/2addr v1, v0

    if-nez v1, :cond_3

    move v0, v3

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->getScrollX()I

    move-result v4

    sub-int v0, v4, v0

    iget-object v4, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4, v2}, Lcom/android/launcher3/Workspace;->bA(I)I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v7, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-direct {p0}, Lagw;->lB()I

    move-result v5

    iget-object v0, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->g(Lcom/android/launcher3/Workspace;)Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 v0, v5, -0x1

    :goto_2
    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1, v0}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Workspace;I)I

    iget-object v1, p0, Lagw;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v1

    if-eqz v1, :cond_5

    sub-int v1, v0, v5

    add-int/lit8 v1, v1, 0x1

    :goto_3
    add-int/2addr v1, v5

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    int-to-float v0, v0

    div-float v0, v1, v0

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x3

    add-int/lit8 v1, v5, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_1
.end method

.method public final lE()V
    .locals 1

    .prologue
    .line 1484
    iget v0, p0, Lagw;->XB:F

    iput v0, p0, Lagw;->XC:F

    .line 1485
    return-void
.end method
