.class public final Lhbq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public volatile bLI:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final dbQ:Lhfp;

.field final dbR:Lhao;

.field final dbS:Ljava/lang/String;

.field final dbT:Ljava/util/concurrent/atomic/AtomicBoolean;

.field dbU:Lhbx;

.field final dbV:Lhby;

.field dbW:Ljava/util/Set;

.field dbX:Ljava/util/Set;

.field final mUrlHelper:Lcpn;


# direct methods
.method constructor <init>(Lcpn;Lhfp;Lcom/google/android/velvet/presenter/inappwebpage/Request;Lhao;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lhbq;->dbT:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 75
    new-instance v0, Lhby;

    invoke-direct {v0, p0}, Lhby;-><init>(Lhbq;)V

    iput-object v0, p0, Lhbq;->dbV:Lhby;

    .line 77
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lhbq;->dbW:Ljava/util/Set;

    .line 80
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lhbq;->dbX:Ljava/util/Set;

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lhbq;->bLI:Ljava/lang/String;

    .line 94
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpn;

    iput-object v0, p0, Lhbq;->mUrlHelper:Lcpn;

    .line 95
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfp;

    iput-object v0, p0, Lhbq;->dbQ:Lhfp;

    .line 96
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhao;

    iput-object v0, p0, Lhbq;->dbR:Lhao;

    .line 97
    iget-object v0, p4, Lhao;->mWebPage:Ldyo;

    invoke-virtual {v0}, Ldyo;->alJ()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbq;->dbS:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lhbq;->dbW:Ljava/util/Set;

    invoke-virtual {p3}, Lcom/google/android/velvet/presenter/inappwebpage/Request;->aNn()Lijp;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 100
    return-void
.end method


# virtual methods
.method S(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    .line 217
    iget-object v1, p0, Lhbq;->dbW:Ljava/util/Set;

    monitor-enter v1

    .line 218
    :try_start_0
    iget-object v0, p0, Lhbq;->dbW:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 219
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    :goto_0
    return v0

    .line 227
    :cond_1
    monitor-exit v1

    .line 228
    const/4 v0, 0x0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Xb()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lhbq;->dbQ:Lhfp;

    iget-object v0, v0, Lhfp;->dfy:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lhbq;->dbQ:Lhfp;

    iget-object v0, v0, Lhfp;->dfy:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 153
    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/Menu;I)V
    .locals 6

    .prologue
    .line 181
    iget-object v1, p0, Lhbq;->dbX:Ljava/util/Set;

    monitor-enter v1

    .line 182
    :try_start_0
    iget-object v0, p0, Lhbq;->dbX:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr;

    .line 183
    const/4 v3, 0x0

    iget v4, v0, Lhbr;->aUU:I

    iget-object v5, v0, Lhbr;->dbY:Ljava/lang/String;

    invoke-interface {p1, p2, v3, v4, v5}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v4, v0, Lhbr;->dca:Z

    if-nez v4, :cond_0

    iget-object v0, v0, Lhbr;->dcb:Landroid/content/Intent;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 183
    :cond_0
    :try_start_1
    new-instance v4, Lhbs;

    invoke-direct {v4, v0}, Lhbs;-><init>(Lhbr;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 185
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lhbx;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lhbq;->dbU:Lhbx;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 128
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbx;

    iput-object v0, p0, Lhbq;->dbU:Lhbx;

    .line 129
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aNq()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lhbq;->dbU:Lhbx;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lhbq;->dbQ:Lhfp;

    iget-object v1, p0, Lhbq;->dbS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhfp;->loadUrl(Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method final aV(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lhbq;->dbU:Lhbx;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lhbq;->dbU:Lhbx;

    invoke-interface {v0, p1}, Lhbx;->aV(Ljava/lang/Object;)V

    .line 239
    :cond_0
    return-void
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lhbq;->dbQ:Lhfp;

    iget-object v0, v0, Lhfp;->dfy:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 164
    iget-object v0, p0, Lhbq;->dbR:Lhao;

    iget-object v0, v0, Lhao;->daS:Ljava/io/Closeable;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 165
    return-void
.end method
