.class public final enum Lgjo;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cMK:Lgjo;

.field public static final enum cML:Lgjo;

.field public static final enum cMM:Lgjo;

.field public static final enum cMN:Lgjo;

.field public static final enum cMO:Lgjo;

.field public static final enum cMP:Lgjo;

.field private static final cMQ:Ljava/util/Map;

.field private static final synthetic cMR:[Lgjo;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lgjo;

    const-string v1, "DICTATION"

    invoke-direct {v0, v1, v3}, Lgjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgjo;->cMK:Lgjo;

    .line 13
    new-instance v0, Lgjo;

    const-string v1, "ENDPOINTER_VOICESEARCH"

    invoke-direct {v0, v1, v4}, Lgjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgjo;->cML:Lgjo;

    .line 14
    new-instance v0, Lgjo;

    const-string v1, "ENDPOINTER_DICTATION"

    invoke-direct {v0, v1, v5}, Lgjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgjo;->cMM:Lgjo;

    .line 15
    new-instance v0, Lgjo;

    const-string v1, "HOTWORD"

    invoke-direct {v0, v1, v6}, Lgjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgjo;->cMN:Lgjo;

    .line 16
    new-instance v0, Lgjo;

    const-string v1, "COMPILER"

    invoke-direct {v0, v1, v7}, Lgjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgjo;->cMO:Lgjo;

    .line 17
    new-instance v0, Lgjo;

    const-string v1, "GRAMMAR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgjo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgjo;->cMP:Lgjo;

    .line 11
    const/4 v0, 0x6

    new-array v0, v0, [Lgjo;

    sget-object v1, Lgjo;->cMK:Lgjo;

    aput-object v1, v0, v3

    sget-object v1, Lgjo;->cML:Lgjo;

    aput-object v1, v0, v4

    sget-object v1, Lgjo;->cMM:Lgjo;

    aput-object v1, v0, v5

    sget-object v1, Lgjo;->cMN:Lgjo;

    aput-object v1, v0, v6

    sget-object v1, Lgjo;->cMO:Lgjo;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgjo;->cMP:Lgjo;

    aput-object v2, v0, v1

    sput-object v0, Lgjo;->cMR:[Lgjo;

    .line 21
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    .line 22
    const-string v1, "dictation"

    sget-object v2, Lgjo;->cMK:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    const-string v1, "endpointer_voicesearch"

    sget-object v2, Lgjo;->cML:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    const-string v1, "endpointer_dictation"

    sget-object v2, Lgjo;->cMM:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    const-string v1, "google_hotword"

    sget-object v2, Lgjo;->cMN:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    const-string v1, "hotword"

    sget-object v2, Lgjo;->cMN:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const-string v1, "compile_grammar"

    sget-object v2, Lgjo;->cMO:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    const-string v1, "grammar"

    sget-object v2, Lgjo;->cMP:Lgjo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sput-object v0, Lgjo;->cMQ:Ljava/util/Map;

    .line 30
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static j(Ljava/io/File;)Lgjo;
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 39
    :cond_0
    sget-object v1, Lgjo;->cMQ:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjo;

    return-object v0
.end method

.method public static k(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".ascii_proto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lgjo;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lgjo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgjo;

    return-object v0
.end method

.method public static values()[Lgjo;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lgjo;->cMR:[Lgjo;

    invoke-virtual {v0}, [Lgjo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgjo;

    return-object v0
.end method


# virtual methods
.method public final aGy()Z
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lgjo;->cMM:Lgjo;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgjo;->cML:Lgjo;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
