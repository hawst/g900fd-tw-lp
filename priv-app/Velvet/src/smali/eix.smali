.class public final Leix;
.super Landroid/animation/ValueAnimator;
.source "PG"


# static fields
.field private static final caA:Landroid/view/View$OnLayoutChangeListener;

.field private static final cay:Landroid/animation/TimeInterpolator;

.field private static final caz:Landroid/animation/TimeInterpolator;


# instance fields
.field private final bIH:Z

.field private bII:Landroid/view/View;

.field private final caB:I

.field private caC:Lekn;

.field private caD:I

.field caE:Landroid/animation/Animator;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Ldqs;->bGc:Ldqs;

    sput-object v0, Leix;->cay:Landroid/animation/TimeInterpolator;

    .line 65
    sget-object v0, Ldqs;->bGc:Ldqs;

    sput-object v0, Leix;->caz:Landroid/animation/TimeInterpolator;

    .line 69
    new-instance v0, Leiy;

    invoke-direct {v0}, Leiy;-><init>()V

    sput-object v0, Leix;->caA:Landroid/view/View$OnLayoutChangeListener;

    return-void
.end method

.method public constructor <init>(ZI)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 87
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Leix;->caD:I

    .line 88
    iput-boolean p1, p0, Leix;->bIH:Z

    .line 89
    iput p2, p0, Leix;->caB:I

    .line 90
    new-instance v0, Leiz;

    invoke-direct {v0, p0}, Leiz;-><init>(Leix;)V

    invoke-virtual {p0, v0}, Leix;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 102
    new-instance v0, Leja;

    invoke-direct {v0, p0}, Leja;-><init>(Leix;)V

    invoke-virtual {p0, v0}, Leix;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 108
    iget-boolean v0, p0, Leix;->bIH:Z

    if-eqz v0, :cond_0

    .line 109
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Leix;->setFloatValues([F)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Leix;->setFloatValues([F)V

    goto :goto_0

    .line 109
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 111
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private a(Landroid/view/View;FZ)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 303
    const/4 v0, 0x0

    mul-float v1, v3, p2

    add-float/2addr v1, v0

    .line 305
    if-eqz p3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 306
    :goto_0
    sub-float v2, v3, p2

    mul-float/2addr v0, v2

    .line 311
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 312
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 313
    return-void

    .line 305
    :cond_0
    iget v0, p0, Leix;->caB:I

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_0
.end method

.method public static a(Lekn;)Z
    .locals 1

    .prologue
    .line 349
    invoke-static {p0}, Leix;->c(Lekn;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Leix;->b(Lekn;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lekn;)Z
    .locals 1

    .prologue
    .line 362
    sget-object v0, Lekn;->cdN:Lekn;

    if-eq p0, v0, :cond_0

    sget-object v0, Lekn;->cdP:Lekn;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lekn;)Z
    .locals 1

    .prologue
    .line 367
    sget-object v0, Lekn;->cdM:Lekn;

    if-eq p0, v0, :cond_0

    sget-object v0, Lekn;->cdL:Lekn;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final afD()V
    .locals 8

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 231
    iget-object v0, p0, Leix;->caE:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    invoke-virtual {p0}, Leix;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 235
    sget-object v0, Lejb;->caG:[I

    iget-object v1, p0, Leix;->caC:Lekn;

    invoke-virtual {v1}, Lekn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 260
    invoke-virtual {p0}, Leix;->cancel()V

    goto :goto_0

    .line 237
    :pswitch_0
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    iget v1, p0, Leix;->caB:I

    int-to-float v1, v1

    sub-float v2, v4, v3

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 241
    :pswitch_1
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 244
    :pswitch_2
    iget-object v1, p0, Leix;->bII:Landroid/view/View;

    iget-boolean v0, p0, Leix;->bIH:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v1, v3, v0}, Leix;->a(Landroid/view/View;FZ)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 247
    :pswitch_3
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    iget-boolean v1, p0, Leix;->bIH:Z

    invoke-direct {p0, v0, v3, v1}, Leix;->a(Landroid/view/View;FZ)V

    goto :goto_0

    .line 250
    :pswitch_4
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    mul-float v1, v2, v3

    add-float/2addr v1, v2

    iget-boolean v2, p0, Leix;->bIH:Z

    invoke-direct {p0, v0, v1, v2}, Leix;->a(Landroid/view/View;FZ)V

    goto :goto_0

    .line 254
    :pswitch_5
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setBottom(I)V

    goto :goto_0

    .line 257
    :pswitch_6
    iget-object v2, p0, Leix;->bII:Landroid/view/View;

    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    sub-float/2addr v4, v3

    instance-of v1, v2, Lejl;

    if-eqz v1, :cond_3

    iget v1, v0, Lekm;->cdE:I

    int-to-float v1, v1

    mul-float v5, v4, v1

    move-object v1, v2

    check-cast v1, Lejl;

    float-to-int v5, v5

    iget v6, v0, Lekm;->cdF:I

    invoke-interface {v1, v4, v5, v6}, Lejl;->b(FII)I

    :cond_3
    iget-boolean v1, p0, Leix;->bIH:Z

    if-nez v1, :cond_0

    iget v1, v0, Lekm;->cdG:I

    iget v5, v0, Lekm;->cdH:I

    sub-int/2addr v1, v5

    int-to-float v1, v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iget-object v4, v0, Lekm;->cdJ:Landroid/graphics/Rect;

    if-nez v4, :cond_4

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, v0, Lekm;->cdJ:Landroid/graphics/Rect;

    :cond_4
    iget-object v4, v0, Lekm;->cdJ:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    iget v6, v0, Lekm;->cdH:I

    add-int/2addr v6, v1

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget v7, v0, Lekm;->cdH:I

    add-int/2addr v1, v7

    iget v0, v0, Lekm;->cdI:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    add-int/2addr v0, v1

    invoke-virtual {v4, v5, v6, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method final afE()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 267
    iget-object v0, p0, Leix;->caC:Lekn;

    sget-object v1, Lekn;->cdO:Lekn;

    if-eq v0, v1, :cond_0

    .line 268
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 269
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    .line 270
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 271
    iget-object v1, p0, Leix;->bII:Landroid/view/View;

    iget-boolean v0, p0, Leix;->bIH:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 276
    iget-object v0, p0, Leix;->caC:Lekn;

    invoke-static {v0}, Leix;->a(Lekn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 281
    :cond_0
    iget-object v0, p0, Leix;->caC:Lekn;

    sget-object v1, Lekn;->cdR:Lekn;

    if-ne v0, v1, :cond_1

    .line 282
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    sget-object v1, Leix;->caA:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 284
    :cond_1
    iget-object v0, p0, Leix;->caC:Lekn;

    sget-object v1, Lekn;->cdT:Lekn;

    if-ne v0, v1, :cond_2

    .line 285
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    instance-of v0, v0, Lejl;

    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 287
    iget-object v1, p0, Leix;->bII:Landroid/view/View;

    check-cast v1, Lejl;

    iget-boolean v5, p0, Leix;->bIH:Z

    if-eqz v5, :cond_4

    :goto_1
    iget-boolean v3, p0, Leix;->bIH:Z

    if-eqz v3, :cond_5

    move v3, v4

    :goto_2
    iget v0, v0, Lekm;->cdF:I

    invoke-interface {v1, v2, v3, v0}, Lejl;->b(FII)I

    .line 291
    :cond_2
    return-void

    .line 271
    :cond_3
    iget v0, p0, Leix;->caB:I

    int-to-float v0, v0

    goto :goto_0

    :cond_4
    move v2, v3

    .line 287
    goto :goto_1

    :cond_5
    iget v3, v0, Lekm;->cdE:I

    goto :goto_2
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 167
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Leix;->bII:Landroid/view/View;

    .line 168
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lekm;

    if-eqz v0, :cond_a

    .line 169
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 170
    iget-boolean v1, p0, Leix;->bIH:Z

    if-eqz v1, :cond_8

    iget-object v1, v0, Lekm;->cdz:Lekn;

    :goto_0
    iput-object v1, p0, Leix;->caC:Lekn;

    .line 171
    iget-object v1, p0, Leix;->caC:Lekn;

    if-nez v1, :cond_0

    .line 173
    iget-boolean v1, p0, Leix;->bIH:Z

    if-eqz v1, :cond_9

    sget-object v1, Lekn;->cdK:Lekn;

    :goto_1
    iput-object v1, p0, Leix;->caC:Lekn;

    .line 175
    :cond_0
    iget-object v1, p0, Leix;->caC:Lekn;

    sget-object v4, Lekn;->cdS:Lekn;

    if-ne v1, v4, :cond_1

    .line 176
    iget-object v1, p0, Leix;->bII:Landroid/view/View;

    sget v4, Lesp;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v4, v7, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lekm;

    iget v4, v1, Lekm;->cdD:I

    iget-object v7, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v7, p0, Leix;->bII:Landroid/view/View;

    iget v1, v1, Lekm;->cdD:I

    int-to-float v4, v4

    invoke-static {v7, v1, v3, v4, v6}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v1

    iput-object v1, p0, Leix;->caE:Landroid/animation/Animator;

    iget-object v1, p0, Leix;->caE:Landroid/animation/Animator;

    const-wide/16 v8, 0x190

    invoke-virtual {v1, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 178
    :cond_1
    iget v0, v0, Lekm;->cdy:I

    iput v0, p0, Leix;->caD:I

    .line 182
    :goto_2
    iget-boolean v0, p0, Leix;->bIH:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    sget-object v0, Lekn;->cdO:Lekn;

    iput-object v0, p0, Leix;->caC:Lekn;

    .line 186
    :cond_2
    iget-object v0, p0, Leix;->caC:Lekn;

    sget-object v1, Lekn;->cdK:Lekn;

    if-ne v0, v1, :cond_c

    move v0, v2

    .line 187
    :goto_3
    iget-object v1, p0, Leix;->caC:Lekn;

    sget-object v4, Lekn;->cdO:Lekn;

    if-ne v1, v4, :cond_d

    move v1, v2

    .line 188
    :goto_4
    iget-object v4, p0, Leix;->caC:Lekn;

    sget-object v7, Lekn;->cdR:Lekn;

    if-ne v4, v7, :cond_e

    move v4, v2

    .line 189
    :goto_5
    iget-object v7, p0, Leix;->caC:Lekn;

    sget-object v8, Lekn;->cdT:Lekn;

    if-ne v7, v8, :cond_f

    .line 190
    :goto_6
    if-nez v0, :cond_3

    if-nez v1, :cond_3

    if-nez v4, :cond_3

    if-nez v2, :cond_3

    iget-boolean v1, p0, Leix;->bIH:Z

    if-eqz v1, :cond_3

    .line 192
    iget-object v1, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setAlpha(F)V

    .line 194
    :cond_3
    if-eqz v0, :cond_4

    .line 197
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    iget v1, p0, Leix;->caB:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 199
    :cond_4
    if-eqz v4, :cond_5

    .line 201
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 202
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setTranslationY(F)V

    .line 203
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    sget-object v1, Leix;->caA:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 205
    :cond_5
    if-eqz v2, :cond_7

    .line 206
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    instance-of v0, v0, Lejl;

    if-eqz v0, :cond_7

    .line 207
    iget-object v0, p0, Leix;->bII:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 208
    iget-object v1, p0, Leix;->bII:Landroid/view/View;

    check-cast v1, Lejl;

    iget-boolean v2, p0, Leix;->bIH:Z

    if-eqz v2, :cond_10

    move v2, v5

    :goto_7
    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_6

    iget v3, v0, Lekm;->cdE:I

    :cond_6
    iget v0, v0, Lekm;->cdF:I

    invoke-interface {v1, v2, v3, v0}, Lejl;->b(FII)I

    .line 215
    :cond_7
    return-void

    .line 170
    :cond_8
    iget-object v1, v0, Lekm;->cdA:Lekn;

    goto/16 :goto_0

    .line 173
    :cond_9
    sget-object v1, Lekn;->cdN:Lekn;

    goto/16 :goto_1

    .line 180
    :cond_a
    iget-boolean v0, p0, Leix;->bIH:Z

    if-eqz v0, :cond_b

    sget-object v0, Lekn;->cdK:Lekn;

    :goto_8
    iput-object v0, p0, Leix;->caC:Lekn;

    goto/16 :goto_2

    :cond_b
    sget-object v0, Lekn;->cdN:Lekn;

    goto :goto_8

    :cond_c
    move v0, v3

    .line 186
    goto :goto_3

    :cond_d
    move v1, v3

    .line 187
    goto :goto_4

    :cond_e
    move v4, v3

    .line 188
    goto :goto_5

    :cond_f
    move v2, v3

    .line 189
    goto :goto_6

    :cond_10
    move v2, v6

    .line 208
    goto :goto_7
.end method

.method public final start()V
    .locals 6

    .prologue
    const-wide/16 v0, 0x190

    const-wide/16 v2, 0xc8

    .line 122
    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Leix;->caC:Lekn;

    sget-object v5, Lekn;->cdK:Lekn;

    if-ne v4, v5, :cond_2

    iget v4, p0, Leix;->caD:I

    mul-int/lit8 v4, v4, 0x64

    int-to-long v4, v4

    invoke-virtual {p0, v4, v5}, Leix;->setStartDelay(J)V

    sget-object v4, Leix;->cay:Landroid/animation/TimeInterpolator;

    invoke-virtual {p0, v4}, Leix;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :goto_0
    iget-object v4, p0, Leix;->caC:Lekn;

    invoke-static {v4}, Leix;->c(Lekn;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_5

    :goto_1
    invoke-virtual {p0, v0, v1}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    :goto_2
    iget-object v0, p0, Leix;->caE:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leix;->caE:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 123
    :cond_0
    iget-object v0, p0, Leix;->caE:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Leix;->caE:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 126
    :cond_1
    invoke-super {p0}, Landroid/animation/ValueAnimator;->start()V

    .line 127
    return-void

    .line 122
    :cond_2
    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Leix;->caC:Lekn;

    sget-object v5, Lekn;->cdP:Lekn;

    if-ne v4, v5, :cond_3

    iget v4, p0, Leix;->caD:I

    mul-int/lit8 v4, v4, 0x64

    add-int/lit16 v4, v4, 0x12c

    int-to-long v4, v4

    invoke-virtual {p0, v4, v5}, Leix;->setStartDelay(J)V

    goto :goto_0

    :cond_3
    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Leix;->caC:Lekn;

    sget-object v5, Lekn;->cdQ:Lekn;

    if-ne v4, v5, :cond_4

    sget-object v4, Leix;->cay:Landroid/animation/TimeInterpolator;

    invoke-virtual {p0, v4}, Leix;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_4
    const-wide/16 v4, 0x0

    invoke-virtual {p0, v4, v5}, Leix;->setStartDelay(J)V

    goto :goto_0

    :cond_5
    move-wide v0, v2

    goto :goto_1

    :cond_6
    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Leix;->caC:Lekn;

    sget-object v5, Lekn;->cdQ:Lekn;

    if-ne v4, v5, :cond_7

    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_2

    :cond_7
    iget-boolean v4, p0, Leix;->bIH:Z

    if-eqz v4, :cond_8

    iget-object v4, p0, Leix;->caC:Lekn;

    invoke-static {v4}, Leix;->b(Lekn;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p0, v2, v3}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_2

    :cond_8
    iget-object v2, p0, Leix;->caC:Lekn;

    sget-object v3, Lekn;->cdT:Lekn;

    if-ne v2, v3, :cond_a

    iget-boolean v2, p0, Leix;->bIH:Z

    if-eqz v2, :cond_9

    const-wide/16 v0, 0x258

    invoke-virtual {p0, v0, v1}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_2

    :cond_9
    invoke-virtual {p0, v0, v1}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    sget-object v0, Leix;->caz:Landroid/animation/TimeInterpolator;

    invoke-virtual {p0, v0}, Leix;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_2

    :cond_a
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Leix;->setDuration(J)Landroid/animation/ValueAnimator;

    goto/16 :goto_2
.end method
