.class public final Lczw;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static C([B)Landroid/util/Pair;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v8, 0x8

    const/4 v0, 0x0

    const/4 v5, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 29
    :try_start_0
    const-string v1, "SHA256"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 35
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 36
    array-length v2, v1

    const/16 v3, 0xc

    if-ge v2, v3, :cond_0

    .line 37
    const-string v2, "Sdch.DictionaryUtils"

    const-string v3, "Unexpected digest length: %d"

    new-array v4, v7, [Ljava/lang/Object;

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 51
    :goto_0
    return-object v0

    .line 30
    :catch_0
    move-exception v1

    .line 31
    const-string v2, "Sdch.DictionaryUtils"

    const-string v3, "No SHA256 Implementation on platform: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :cond_0
    new-instance v2, Ljava/lang/String;

    const/16 v3, 0xa

    invoke-static {v1, v6, v5, v3}, Landroid/util/Base64;->encode([BIII)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 43
    new-instance v3, Ljava/lang/String;

    const/16 v4, 0xa

    invoke-static {v1, v5, v5, v4}, Landroid/util/Base64;->encode([BIII)[B

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    .line 46
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v8, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, v8, :cond_2

    .line 47
    :cond_1
    const-string v1, "Sdch.DictionaryUtils"

    const-string v4, "Unexpected hashes: %s, %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    aput-object v3, v5, v7

    invoke-static {v1, v4, v5}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :cond_2
    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public static D([B)I
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 92
    const/4 v1, -0x1

    .line 93
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 94
    aget-byte v2, p0, v0

    if-ne v2, v3, :cond_0

    .line 95
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_0

    add-int/lit8 v2, v0, 0x1

    aget-byte v2, p0, v2

    if-ne v2, v3, :cond_0

    .line 96
    add-int/lit8 v0, v0, -0x1

    .line 102
    :goto_1
    add-int/lit8 v0, v0, 0x1

    return v0

    .line 93
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static jn(Ljava/lang/String;)Ljava/util/Map;
    .locals 12
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 60
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v3

    .line 63
    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_4

    aget-object v5, v2, v0

    .line 64
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 65
    const/16 v6, 0x3a

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 69
    if-gtz v6, :cond_1

    .line 70
    const-string v6, "Sdch.DictionaryUtils"

    const-string v7, "Invalid dictionary header: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v5, v8, v1

    invoke-static {v11, v6, v7, v8}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 63
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 78
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 80
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 81
    :cond_2
    const-string v6, "Sdch.DictionaryUtils"

    const-string v8, "Invalid key value pair in dict header: %s, %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v7, v9, v1

    aput-object v5, v9, v10

    invoke-static {v11, v6, v8, v9}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 85
    :cond_3
    invoke-interface {v3, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 88
    :cond_4
    return-object v3
.end method
