.class public final Ljge;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field public dMm:Ljcn;

.field private ehZ:Ljava/lang/String;

.field private ejt:Ljava/lang/String;

.field private eju:Z

.field private ejv:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 50451
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50452
    iput v1, p0, Ljge;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljge;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljge;->ejt:Ljava/lang/String;

    iput-object v2, p0, Ljge;->dMm:Ljcn;

    iput-boolean v1, p0, Ljge;->eju:Z

    const-string v0, ""

    iput-object v0, p0, Ljge;->ejv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljge;->ehZ:Ljava/lang/String;

    iput-object v2, p0, Ljge;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljge;->eCz:I

    .line 50453
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 50322
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljge;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljge;->agq:Ljava/lang/String;

    iget v0, p0, Ljge;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljge;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljge;->ejt:Ljava/lang/String;

    iget v0, p0, Ljge;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljge;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljge;->dMm:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljge;->dMm:Ljcn;

    :cond_1
    iget-object v0, p0, Ljge;->dMm:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljge;->eju:Z

    iget v0, p0, Ljge;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljge;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljge;->ejv:Ljava/lang/String;

    iget v0, p0, Ljge;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljge;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljge;->ehZ:Ljava/lang/String;

    iget v0, p0, Ljge;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljge;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 50471
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 50472
    const/4 v0, 0x1

    iget-object v1, p0, Ljge;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 50474
    :cond_0
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 50475
    const/4 v0, 0x2

    iget-object v1, p0, Ljge;->ejt:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 50477
    :cond_1
    iget-object v0, p0, Ljge;->dMm:Ljcn;

    if-eqz v0, :cond_2

    .line 50478
    const/4 v0, 0x3

    iget-object v1, p0, Ljge;->dMm:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 50480
    :cond_2
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 50481
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljge;->eju:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 50483
    :cond_3
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 50484
    const/4 v0, 0x5

    iget-object v1, p0, Ljge;->ejv:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 50486
    :cond_4
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 50487
    const/4 v0, 0x6

    iget-object v1, p0, Ljge;->ehZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 50489
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 50490
    return-void
.end method

.method public final bjR()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50366
    iget-object v0, p0, Ljge;->ejt:Ljava/lang/String;

    return-object v0
.end method

.method public final bjS()Z
    .locals 1

    .prologue
    .line 50391
    iget-boolean v0, p0, Ljge;->eju:Z

    return v0
.end method

.method public final bjT()Z
    .locals 1

    .prologue
    .line 50399
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50344
    iget-object v0, p0, Ljge;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 50494
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 50495
    iget v1, p0, Ljge;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 50496
    const/4 v1, 0x1

    iget-object v2, p0, Ljge;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50499
    :cond_0
    iget v1, p0, Ljge;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 50500
    const/4 v1, 0x2

    iget-object v2, p0, Ljge;->ejt:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50503
    :cond_1
    iget-object v1, p0, Ljge;->dMm:Ljcn;

    if-eqz v1, :cond_2

    .line 50504
    const/4 v1, 0x3

    iget-object v2, p0, Ljge;->dMm:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50507
    :cond_2
    iget v1, p0, Ljge;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 50508
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljge;->eju:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 50511
    :cond_3
    iget v1, p0, Ljge;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 50512
    const/4 v1, 0x5

    iget-object v2, p0, Ljge;->ejv:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50515
    :cond_4
    iget v1, p0, Ljge;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 50516
    const/4 v1, 0x6

    iget-object v2, p0, Ljge;->ehZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50519
    :cond_5
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 50355
    iget v0, p0, Ljge;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final uv(Ljava/lang/String;)Ljge;
    .locals 1

    .prologue
    .line 50347
    if-nez p1, :cond_0

    .line 50348
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50350
    :cond_0
    iput-object p1, p0, Ljge;->agq:Ljava/lang/String;

    .line 50351
    iget v0, p0, Ljge;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljge;->aez:I

    .line 50352
    return-object p0
.end method
