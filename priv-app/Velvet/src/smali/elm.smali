.class public final Lelm;
.super Landroid/graphics/drawable/InsetDrawable;
.source "PG"


# instance fields
.field private final ceM:Landroid/content/res/ColorStateList;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 21
    iput-object p2, p0, Lelm;->ceM:Landroid/content/res/ColorStateList;

    .line 22
    invoke-virtual {p0}, Lelm;->getState()[I

    move-result-object v0

    invoke-virtual {p0, v0}, Lelm;->onStateChange([I)Z

    .line 23
    return-void
.end method


# virtual methods
.method public final isStateful()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method protected final onStateChange([I)Z
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/graphics/drawable/InsetDrawable;->onStateChange([I)Z

    move-result v0

    .line 34
    iget-object v1, p0, Lelm;->ceM:Landroid/content/res/ColorStateList;

    iget-object v2, p0, Lelm;->ceM:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    .line 36
    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {p0}, Lelm;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 38
    const/4 v0, 0x1

    .line 40
    :cond_0
    return v0
.end method
