.class public final Lfsu;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 35
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agF:Lanj;

    .line 36
    invoke-virtual {v1}, Lanj;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    const v2, 0x7f110082

    invoke-virtual {v1}, Lanj;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 40
    :cond_0
    invoke-virtual {v1}, Lanj;->pd()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41
    const v2, 0x7f1102ee

    invoke-virtual {v1}, Lanj;->pc()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 44
    :cond_1
    invoke-virtual {v1}, Lanj;->pe()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 45
    const v2, 0x7f1102f0

    invoke-virtual {v1}, Lanj;->on()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 48
    :cond_2
    invoke-virtual {v1}, Lanj;->pf()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 49
    const v2, 0x7f1100b0

    invoke-virtual {v1}, Lanj;->oo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 52
    :cond_3
    iget-object v2, v1, Lanj;->ahN:Laok;

    if-eqz v2, :cond_4

    .line 53
    iget-object v2, p0, Lfsu;->mContext:Landroid/content/Context;

    iget-object v3, v1, Lanj;->ahN:Laok;

    invoke-static {v2, v3}, Lfxt;->a(Landroid/content/Context;Laok;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 55
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 56
    const v3, 0x7f1102ef

    invoke-static {v0, v3, v2}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 60
    :cond_4
    iget-object v1, v1, Lanj;->ahM:Laoi;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lfsu;->a(Landroid/view/View;Laoi;Z)V

    .line 61
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 26
    const v0, 0x7f04010a

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
