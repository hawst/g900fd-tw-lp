.class public final Lage;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lagt;


# instance fields
.field private synthetic CJ:Lahz;

.field private synthetic WN:Lcom/android/launcher3/Workspace;

.field private synthetic Xh:Ljava/util/HashMap;

.field private synthetic Xi:Ljava/util/HashSet;

.field private synthetic Xj:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Workspace;Ljava/util/HashMap;Lahz;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 4832
    iput-object p1, p0, Lage;->WN:Lcom/android/launcher3/Workspace;

    iput-object p2, p0, Lage;->Xh:Ljava/util/HashMap;

    iput-object p3, p0, Lage;->CJ:Lahz;

    iput-object p4, p0, Lage;->Xi:Ljava/util/HashSet;

    iput-object p5, p0, Lage;->Xj:Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lwq;Landroid/view/View;Landroid/view/View;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4835
    instance-of v0, p1, Ladh;

    if-eqz v0, :cond_2

    instance-of v0, p2, Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 4836
    check-cast v0, Ladh;

    .line 4837
    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v4

    .line 4838
    iget-object v1, p0, Lage;->Xh:Ljava/util/HashMap;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrr;

    .line 4839
    iget-object v5, p0, Lage;->CJ:Lahz;

    iget-object v6, v0, Ladh;->Jl:Lahz;

    invoke-virtual {v5, v6}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    invoke-static {p1}, Lzi;->f(Lwq;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lage;->Xi:Ljava/util/HashSet;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 4844
    invoke-virtual {v0}, Ladh;->kg()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 4845
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ladh;->bH(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 4847
    iget-object v5, p0, Lage;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 4848
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x10000

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 4852
    if-nez v6, :cond_4

    .line 4854
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 4856
    if-eqz v4, :cond_0

    .line 4857
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 4858
    iget-object v5, p0, Lage;->Xh:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrr;

    .line 4861
    :cond_0
    if-eqz v4, :cond_1

    iget-object v5, p0, Lage;->Xh:Ljava/util/HashMap;

    if-nez v5, :cond_3

    .line 4863
    :cond_1
    iget-object v1, p0, Lage;->Xj:Ljava/util/HashSet;

    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4905
    :cond_2
    :goto_0
    return v3

    .line 4868
    :cond_3
    iput-object v4, v0, Ladh;->SX:Landroid/content/Intent;

    .line 4873
    :cond_4
    iget-object v4, v0, Ladh;->SX:Landroid/content/Intent;

    iput-object v4, v0, Ladh;->intent:Landroid/content/Intent;

    .line 4874
    const/4 v4, 0x0

    iput-object v4, v0, Ladh;->SX:Landroid/content/Intent;

    .line 4875
    iget v4, v0, Ladh;->status:I

    and-int/lit8 v4, v4, -0x8

    iput v4, v0, Ladh;->status:I

    .line 4881
    iget-object v4, p0, Lage;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v4}, Lcom/android/launcher3/Workspace;->s(Lcom/android/launcher3/Workspace;)Lwi;

    move-result-object v4

    invoke-virtual {v0, v4}, Ladh;->a(Lwi;)V

    .line 4882
    iget-object v4, p0, Lage;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0}, Lzi;->a(Landroid/content/Context;Lwq;)V

    move-object v4, v1

    move v5, v2

    move v1, v2

    .line 4886
    :goto_1
    if-eqz v4, :cond_5

    .line 4887
    iget-object v1, p0, Lage;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1}, Lcom/android/launcher3/Workspace;->s(Lcom/android/launcher3/Workspace;)Lwi;

    move-result-object v1

    invoke-virtual {v0, v1}, Ladh;->a(Lwi;)V

    .line 4888
    iget-object v1, v4, Lrr;->title:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ladh;->title:Ljava/lang/CharSequence;

    .line 4889
    iget-object v1, v4, Lrr;->Jk:Ljava/lang/CharSequence;

    iput-object v1, v0, Ladh;->Jk:Ljava/lang/CharSequence;

    move v1, v2

    .line 4893
    :cond_5
    if-eqz v1, :cond_2

    .line 4894
    check-cast p2, Lcom/android/launcher3/BubbleTextView;

    .line 4895
    iget-object v1, p0, Lage;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1}, Lcom/android/launcher3/Workspace;->s(Lcom/android/launcher3/Workspace;)Lwi;

    move-result-object v1

    invoke-virtual {p2, v0, v1, v2, v5}, Lcom/android/launcher3/BubbleTextView;->a(Ladh;Lwi;ZZ)V

    .line 4898
    if-eqz p3, :cond_2

    .line 4899
    invoke-virtual {p3}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :cond_6
    move-object v4, v1

    move v5, v3

    move v1, v3

    goto :goto_1
.end method
