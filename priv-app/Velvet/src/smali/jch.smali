.class public final Ljch;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eag:[Ljch;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field public aiS:Ljcn;

.field private dZU:Ljava/lang/String;

.field private eah:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 48462
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 48463
    iput v1, p0, Ljch;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljch;->agq:Ljava/lang/String;

    iput v1, p0, Ljch;->eah:I

    const-string v0, ""

    iput-object v0, p0, Ljch;->dZU:Ljava/lang/String;

    iput-object v2, p0, Ljch;->aiS:Ljcn;

    iput-object v2, p0, Ljch;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljch;->eCz:I

    .line 48464
    return-void
.end method

.method public static bgg()[Ljch;
    .locals 2

    .prologue
    .line 48383
    sget-object v0, Ljch;->eag:[Ljch;

    if-nez v0, :cond_1

    .line 48384
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 48386
    :try_start_0
    sget-object v0, Ljch;->eag:[Ljch;

    if-nez v0, :cond_0

    .line 48387
    const/4 v0, 0x0

    new-array v0, v0, [Ljch;

    sput-object v0, Ljch;->eag:[Ljch;

    .line 48389
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48391
    :cond_1
    sget-object v0, Ljch;->eag:[Ljch;

    return-object v0

    .line 48389
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 48377
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljch;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljch;->agq:Ljava/lang/String;

    iget v0, p0, Ljch;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljch;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljch;->eah:I

    iget v0, p0, Ljch;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljch;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljch;->dZU:Ljava/lang/String;

    iget v0, p0, Ljch;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljch;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljch;->aiS:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljch;->aiS:Ljcn;

    :cond_1
    iget-object v0, p0, Ljch;->aiS:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 48480
    iget v0, p0, Ljch;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 48481
    const/4 v0, 0x2

    iget-object v1, p0, Ljch;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 48483
    :cond_0
    iget v0, p0, Ljch;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 48484
    const/4 v0, 0x3

    iget v1, p0, Ljch;->eah:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 48486
    :cond_1
    iget v0, p0, Ljch;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 48487
    const/4 v0, 0x4

    iget-object v1, p0, Ljch;->dZU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 48489
    :cond_2
    iget-object v0, p0, Ljch;->aiS:Ljcn;

    if-eqz v0, :cond_3

    .line 48490
    const/4 v0, 0x5

    iget-object v1, p0, Ljch;->aiS:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 48492
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 48493
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48399
    iget-object v0, p0, Ljch;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 48497
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 48498
    iget v1, p0, Ljch;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 48499
    const/4 v1, 0x2

    iget-object v2, p0, Ljch;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48502
    :cond_0
    iget v1, p0, Ljch;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 48503
    const/4 v1, 0x3

    iget v2, p0, Ljch;->eah:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 48506
    :cond_1
    iget v1, p0, Ljch;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 48507
    const/4 v1, 0x4

    iget-object v2, p0, Ljch;->dZU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48510
    :cond_2
    iget-object v1, p0, Ljch;->aiS:Ljcn;

    if-eqz v1, :cond_3

    .line 48511
    const/4 v1, 0x5

    iget-object v2, p0, Ljch;->aiS:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48514
    :cond_3
    return v0
.end method
