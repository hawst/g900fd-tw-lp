.class public final Ljlv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ajm:Ljava/lang/String;

.field private esX:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14762
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14763
    const/4 v0, 0x0

    iput v0, p0, Ljlv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlv;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlv;->ajm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlv;->esX:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljlv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlv;->eCz:I

    .line 14764
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 14677
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlv;->agq:Ljava/lang/String;

    iget v0, p0, Ljlv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlv;->ajm:Ljava/lang/String;

    iget v0, p0, Ljlv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlv;->esX:Ljava/lang/String;

    iget v0, p0, Ljlv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlv;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 14779
    iget v0, p0, Ljlv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14780
    const/4 v0, 0x1

    iget-object v1, p0, Ljlv;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14782
    :cond_0
    iget v0, p0, Ljlv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14783
    const/4 v0, 0x2

    iget-object v1, p0, Ljlv;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14785
    :cond_1
    iget v0, p0, Ljlv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14786
    const/4 v0, 0x3

    iget-object v1, p0, Ljlv;->esX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14788
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14789
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 14793
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14794
    iget v1, p0, Ljlv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14795
    const/4 v1, 0x1

    iget-object v2, p0, Ljlv;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14798
    :cond_0
    iget v1, p0, Ljlv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14799
    const/4 v1, 0x2

    iget-object v2, p0, Ljlv;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14802
    :cond_1
    iget v1, p0, Ljlv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14803
    const/4 v1, 0x3

    iget-object v2, p0, Ljlv;->esX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14806
    :cond_2
    return v0
.end method
