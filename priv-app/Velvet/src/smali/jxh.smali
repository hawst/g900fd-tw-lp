.class public final Ljxh;
.super Ljsl;
.source "PG"


# static fields
.field public static final eKv:Ljsm;


# instance fields
.field private aez:I

.field private eGT:Ljava/lang/String;

.field private eIA:Z

.field public eIB:Ljtl;

.field private eID:Z

.field private eIE:I

.field private eII:Z

.field private eIJ:Z

.field private eIK:Z

.field private eKA:F

.field private eKB:Ljxi;

.field private eKC:Z

.field private eKD:Z

.field private eKE:Z

.field private eKF:Z

.field private eKG:Z

.field private eKH:Z

.field private eKI:Z

.field private eKJ:Z

.field private eKK:Z

.field public eKw:Ljtp;

.field private eKx:I

.field private eKy:Z

.field private eKz:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljxh;

    const v2, 0x23f322

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxh;->eKv:Ljsm;

    .line 169
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 568
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 569
    iput v1, p0, Ljxh;->aez:I

    iput-object v2, p0, Ljxh;->eKw:Ljtp;

    const/4 v0, 0x1

    iput v0, p0, Ljxh;->eKx:I

    iput-object v2, p0, Ljxh;->eIB:Ljtl;

    iput-boolean v1, p0, Ljxh;->eID:Z

    iput-boolean v1, p0, Ljxh;->eII:Z

    iput-boolean v1, p0, Ljxh;->eIJ:Z

    iput-boolean v1, p0, Ljxh;->eIK:Z

    iput-boolean v1, p0, Ljxh;->eKy:Z

    const/4 v0, 0x2

    iput v0, p0, Ljxh;->eKz:I

    const/4 v0, 0x0

    iput v0, p0, Ljxh;->eKA:F

    iput-object v2, p0, Ljxh;->eKB:Ljxi;

    const-string v0, ""

    iput-object v0, p0, Ljxh;->eGT:Ljava/lang/String;

    iput-boolean v1, p0, Ljxh;->eKC:Z

    iput-boolean v1, p0, Ljxh;->eKD:Z

    iput-boolean v1, p0, Ljxh;->eIA:Z

    iput-boolean v1, p0, Ljxh;->eKE:Z

    iput-boolean v1, p0, Ljxh;->eKF:Z

    iput-boolean v1, p0, Ljxh;->eKG:Z

    iput-boolean v1, p0, Ljxh;->eKH:Z

    iput-boolean v1, p0, Ljxh;->eKI:Z

    iput v1, p0, Ljxh;->eIE:I

    iput-boolean v1, p0, Ljxh;->eKJ:Z

    iput-boolean v1, p0, Ljxh;->eKK:Z

    iput-object v2, p0, Ljxh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxh;->eCz:I

    .line 570
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljxh;->eKw:Ljtp;

    if-nez v0, :cond_1

    new-instance v0, Ljtp;

    invoke-direct {v0}, Ljtp;-><init>()V

    iput-object v0, p0, Ljxh;->eKw:Ljtp;

    :cond_1
    iget-object v0, p0, Ljxh;->eKw:Ljtp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxh;->eKx:I

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxh;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljxh;->eIB:Ljtl;

    if-nez v0, :cond_2

    new-instance v0, Ljtl;

    invoke-direct {v0}, Ljtl;-><init>()V

    iput-object v0, p0, Ljxh;->eIB:Ljtl;

    :cond_2
    iget-object v0, p0, Ljxh;->eIB:Ljtl;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eID:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxh;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKy:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljxh;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxh;->eKz:I

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljxh;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljxh;->eKA:F

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljxh;->aez:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljxh;->eKB:Ljxi;

    if-nez v0, :cond_3

    new-instance v0, Ljxi;

    invoke-direct {v0}, Ljxi;-><init>()V

    iput-object v0, p0, Ljxh;->eKB:Ljxi;

    :cond_3
    iget-object v0, p0, Ljxh;->eKB:Ljxi;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxh;->eGT:Ljava/lang/String;

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKC:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKD:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eIA:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKE:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKF:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKG:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKH:Z

    iget v0, p0, Ljxh;->aez:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKI:Z

    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxh;->eIE:I

    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKJ:Z

    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eII:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eIK:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eIJ:Z

    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxh;->eKK:Z

    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    iput v0, p0, Ljxh;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x45 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x90 -> :sswitch_11
        0x98 -> :sswitch_12
        0xa0 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb0 -> :sswitch_15
        0xb8 -> :sswitch_16
        0xc0 -> :sswitch_17
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Ljxh;->eKw:Ljtp;

    if-eqz v0, :cond_0

    .line 606
    const/4 v0, 0x1

    iget-object v1, p0, Ljxh;->eKw:Ljtp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 608
    :cond_0
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 609
    const/4 v0, 0x3

    iget v1, p0, Ljxh;->eKx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 611
    :cond_1
    iget-object v0, p0, Ljxh;->eIB:Ljtl;

    if-eqz v0, :cond_2

    .line 612
    const/4 v0, 0x4

    iget-object v1, p0, Ljxh;->eIB:Ljtl;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 614
    :cond_2
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 615
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljxh;->eID:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 617
    :cond_3
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 618
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljxh;->eKy:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 620
    :cond_4
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 621
    const/4 v0, 0x7

    iget v1, p0, Ljxh;->eKz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 623
    :cond_5
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 624
    const/16 v0, 0x8

    iget v1, p0, Ljxh;->eKA:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 626
    :cond_6
    iget-object v0, p0, Ljxh;->eKB:Ljxi;

    if-eqz v0, :cond_7

    .line 627
    const/16 v0, 0x9

    iget-object v1, p0, Ljxh;->eKB:Ljxi;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 629
    :cond_7
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    .line 630
    const/16 v0, 0xa

    iget-object v1, p0, Ljxh;->eGT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 632
    :cond_8
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_9

    .line 633
    const/16 v0, 0xb

    iget-boolean v1, p0, Ljxh;->eKC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 635
    :cond_9
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_a

    .line 636
    const/16 v0, 0xc

    iget-boolean v1, p0, Ljxh;->eKD:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 638
    :cond_a
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_b

    .line 639
    const/16 v0, 0xd

    iget-boolean v1, p0, Ljxh;->eIA:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 641
    :cond_b
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_c

    .line 642
    const/16 v0, 0xe

    iget-boolean v1, p0, Ljxh;->eKE:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 644
    :cond_c
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_d

    .line 645
    const/16 v0, 0xf

    iget-boolean v1, p0, Ljxh;->eKF:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 647
    :cond_d
    iget v0, p0, Ljxh;->aez:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_e

    .line 648
    const/16 v0, 0x10

    iget-boolean v1, p0, Ljxh;->eKG:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 650
    :cond_e
    iget v0, p0, Ljxh;->aez:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_f

    .line 651
    const/16 v0, 0x11

    iget-boolean v1, p0, Ljxh;->eKH:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 653
    :cond_f
    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_10

    .line 654
    const/16 v0, 0x12

    iget-boolean v1, p0, Ljxh;->eKI:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 656
    :cond_10
    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_11

    .line 657
    const/16 v0, 0x13

    iget v1, p0, Ljxh;->eIE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 659
    :cond_11
    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_12

    .line 660
    const/16 v0, 0x14

    iget-boolean v1, p0, Ljxh;->eKJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 662
    :cond_12
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_13

    .line 663
    const/16 v0, 0x15

    iget-boolean v1, p0, Ljxh;->eII:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 665
    :cond_13
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_14

    .line 666
    const/16 v0, 0x16

    iget-boolean v1, p0, Ljxh;->eIK:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 668
    :cond_14
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_15

    .line 669
    const/16 v0, 0x17

    iget-boolean v1, p0, Ljxh;->eIJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 671
    :cond_15
    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_16

    .line 672
    const/16 v0, 0x18

    iget-boolean v1, p0, Ljxh;->eKK:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 674
    :cond_16
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 675
    return-void
.end method

.method public final aHQ()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Ljxh;->eKx:I

    return v0
.end method

.method public final bvH()Z
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Ljxh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bvI()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Ljxh;->eKz:I

    return v0
.end method

.method public final bvJ()Z
    .locals 1

    .prologue
    .line 419
    iget-boolean v0, p0, Ljxh;->eKE:Z

    return v0
.end method

.method public final bvK()Z
    .locals 1

    .prologue
    .line 457
    iget-boolean v0, p0, Ljxh;->eKG:Z

    return v0
.end method

.method public final bvL()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Ljxh;->eKJ:Z

    return v0
.end method

.method public final bvk()Z
    .locals 1

    .prologue
    .line 400
    iget-boolean v0, p0, Ljxh;->eIA:Z

    return v0
.end method

.method public final bvl()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Ljxh;->eID:Z

    return v0
.end method

.method public final jo(Z)Ljxh;
    .locals 1

    .prologue
    .line 207
    iput-boolean p1, p0, Ljxh;->eID:Z

    .line 208
    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxh;->aez:I

    .line 209
    return-object p0
.end method

.method public final jp(Z)Ljxh;
    .locals 1

    .prologue
    .line 403
    iput-boolean p1, p0, Ljxh;->eIA:Z

    .line 404
    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljxh;->aez:I

    .line 405
    return-object p0
.end method

.method public final jq(Z)Ljxh;
    .locals 1

    .prologue
    .line 422
    iput-boolean p1, p0, Ljxh;->eKE:Z

    .line 423
    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljxh;->aez:I

    .line 424
    return-object p0
.end method

.method public final jr(Z)Ljxh;
    .locals 1

    .prologue
    .line 460
    iput-boolean p1, p0, Ljxh;->eKG:Z

    .line 461
    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljxh;->aez:I

    .line 462
    return-object p0
.end method

.method public final js(Z)Ljxh;
    .locals 2

    .prologue
    .line 536
    iput-boolean p1, p0, Ljxh;->eKJ:Z

    .line 537
    iget v0, p0, Ljxh;->aez:I

    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    iput v0, p0, Ljxh;->aez:I

    .line 538
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 679
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 680
    iget-object v1, p0, Ljxh;->eKw:Ljtp;

    if-eqz v1, :cond_0

    .line 681
    const/4 v1, 0x1

    iget-object v2, p0, Ljxh;->eKw:Ljtp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 684
    :cond_0
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 685
    const/4 v1, 0x3

    iget v2, p0, Ljxh;->eKx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 688
    :cond_1
    iget-object v1, p0, Ljxh;->eIB:Ljtl;

    if-eqz v1, :cond_2

    .line 689
    const/4 v1, 0x4

    iget-object v2, p0, Ljxh;->eIB:Ljtl;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 692
    :cond_2
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 693
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljxh;->eID:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 696
    :cond_3
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 697
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljxh;->eKy:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 700
    :cond_4
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 701
    const/4 v1, 0x7

    iget v2, p0, Ljxh;->eKz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    :cond_5
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 705
    const/16 v1, 0x8

    iget v2, p0, Ljxh;->eKA:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 708
    :cond_6
    iget-object v1, p0, Ljxh;->eKB:Ljxi;

    if-eqz v1, :cond_7

    .line 709
    const/16 v1, 0x9

    iget-object v2, p0, Ljxh;->eKB:Ljxi;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 712
    :cond_7
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    .line 713
    const/16 v1, 0xa

    iget-object v2, p0, Ljxh;->eGT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 716
    :cond_8
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    .line 717
    const/16 v1, 0xb

    iget-boolean v2, p0, Ljxh;->eKC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 720
    :cond_9
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    .line 721
    const/16 v1, 0xc

    iget-boolean v2, p0, Ljxh;->eKD:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 724
    :cond_a
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_b

    .line 725
    const/16 v1, 0xd

    iget-boolean v2, p0, Ljxh;->eIA:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 728
    :cond_b
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_c

    .line 729
    const/16 v1, 0xe

    iget-boolean v2, p0, Ljxh;->eKE:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 732
    :cond_c
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    .line 733
    const/16 v1, 0xf

    iget-boolean v2, p0, Ljxh;->eKF:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 736
    :cond_d
    iget v1, p0, Ljxh;->aez:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_e

    .line 737
    const/16 v1, 0x10

    iget-boolean v2, p0, Ljxh;->eKG:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 740
    :cond_e
    iget v1, p0, Ljxh;->aez:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-eqz v1, :cond_f

    .line 741
    const/16 v1, 0x11

    iget-boolean v2, p0, Ljxh;->eKH:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 744
    :cond_f
    iget v1, p0, Ljxh;->aez:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    if-eqz v1, :cond_10

    .line 745
    const/16 v1, 0x12

    iget-boolean v2, p0, Ljxh;->eKI:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 748
    :cond_10
    iget v1, p0, Ljxh;->aez:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-eqz v1, :cond_11

    .line 749
    const/16 v1, 0x13

    iget v2, p0, Ljxh;->eIE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 752
    :cond_11
    iget v1, p0, Ljxh;->aez:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    if-eqz v1, :cond_12

    .line 753
    const/16 v1, 0x14

    iget-boolean v2, p0, Ljxh;->eKJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 756
    :cond_12
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_13

    .line 757
    const/16 v1, 0x15

    iget-boolean v2, p0, Ljxh;->eII:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 760
    :cond_13
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_14

    .line 761
    const/16 v1, 0x16

    iget-boolean v2, p0, Ljxh;->eIK:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 764
    :cond_14
    iget v1, p0, Ljxh;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_15

    .line 765
    const/16 v1, 0x17

    iget-boolean v2, p0, Ljxh;->eIJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 768
    :cond_15
    iget v1, p0, Ljxh;->aez:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-eqz v1, :cond_16

    .line 769
    const/16 v1, 0x18

    iget-boolean v2, p0, Ljxh;->eKK:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 772
    :cond_16
    return v0
.end method

.method public final sS(I)Ljxh;
    .locals 1

    .prologue
    .line 185
    iput p1, p0, Ljxh;->eKx:I

    .line 186
    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxh;->aez:I

    .line 187
    return-object p0
.end method

.method public final sT(I)Ljxh;
    .locals 1

    .prologue
    .line 302
    iput p1, p0, Ljxh;->eKz:I

    .line 303
    iget v0, p0, Ljxh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljxh;->aez:I

    .line 304
    return-object p0
.end method

.method public final xL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Ljxh;->eGT:Ljava/lang/String;

    return-object v0
.end method

.method public final zM(Ljava/lang/String;)Ljxh;
    .locals 1

    .prologue
    .line 343
    if-nez p1, :cond_0

    .line 344
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 346
    :cond_0
    iput-object p1, p0, Ljxh;->eGT:Ljava/lang/String;

    .line 347
    iget v0, p0, Ljxh;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljxh;->aez:I

    .line 348
    return-object p0
.end method
