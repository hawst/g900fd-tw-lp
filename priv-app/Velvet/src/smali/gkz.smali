.class public final Lgkz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglc;


# instance fields
.field private aSo:Lgmj;

.field private aSw:Lcdc;

.field private final cOx:Lenw;

.field private cOy:Lgla;

.field private final mClock:Lemp;

.field private final mS3ConnectionFactory:Lcdr;

.field private final mSpeechLibFactory:Lgdn;


# direct methods
.method public constructor <init>(Lemp;Lcdr;Lgdn;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lgkz;->mClock:Lemp;

    .line 49
    iput-object p2, p0, Lgkz;->mS3ConnectionFactory:Lcdr;

    .line 50
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lgkz;->cOx:Lenw;

    .line 51
    iput-object p3, p0, Lgkz;->mSpeechLibFactory:Lgdn;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lger;Lggg;Lgnj;)V
    .locals 8

    .prologue
    .line 60
    iget-object v0, p0, Lgkz;->cOx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 62
    iget-object v0, p0, Lgkz;->mSpeechLibFactory:Lgdn;

    invoke-interface {v0, p1, p3}, Lgdn;->a(Lger;Lgnj;)Lgmj;

    move-result-object v0

    iput-object v0, p0, Lgkz;->aSo:Lgmj;

    .line 66
    invoke-virtual {p3}, Lgnj;->getMode()I

    move-result v0

    invoke-static {v0}, Lgnj;->kr(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v7, 0x1

    .line 67
    :goto_0
    new-instance v0, Lgla;

    invoke-direct {v0, p2}, Lgla;-><init>(Lggg;)V

    iput-object v0, p0, Lgkz;->cOy:Lgla;

    .line 69
    new-instance v0, Lcdc;

    iget-object v1, p0, Lgkz;->cOy:Lgla;

    iget-object v2, p0, Lgkz;->mS3ConnectionFactory:Lcdr;

    iget-object v3, p0, Lgkz;->aSo:Lgmj;

    iget-object v4, p0, Lgkz;->mSpeechLibFactory:Lgdn;

    invoke-interface {v4, p3}, Lgdn;->b(Lgnj;)Lcdp;

    move-result-object v4

    iget-object v5, p0, Lgkz;->mSpeechLibFactory:Lgdn;

    invoke-interface {v5, p3}, Lgdn;->c(Lgnj;)Lcdz;

    move-result-object v5

    iget-object v6, p0, Lgkz;->mClock:Lemp;

    invoke-direct/range {v0 .. v7}, Lcdc;-><init>(Lcdf;Lcdr;Lgmj;Lcdp;Lcdz;Lemp;Z)V

    iput-object v0, p0, Lgkz;->aSw:Lcdc;

    .line 76
    iget-object v0, p0, Lgkz;->aSw:Lcdc;

    invoke-virtual {v0}, Lcdc;->start()V

    .line 77
    return-void

    .line 66
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lgkz;->cOx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 82
    iput-object v1, p0, Lgkz;->aSo:Lgmj;

    .line 84
    iget-object v0, p0, Lgkz;->aSw:Lcdc;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lgkz;->cOy:Lgla;

    invoke-virtual {v0}, Lgla;->invalidate()V

    .line 86
    iput-object v1, p0, Lgkz;->cOy:Lgla;

    .line 87
    iget-object v0, p0, Lgkz;->aSw:Lcdc;

    invoke-virtual {v0}, Lcdc;->close()V

    .line 88
    iput-object v1, p0, Lgkz;->aSw:Lcdc;

    .line 90
    :cond_0
    return-void
.end method
