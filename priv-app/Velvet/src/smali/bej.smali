.class public Lbej;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private axM:Ljava/lang/String;

.field protected final axN:Ljava/lang/String;

.field private axO:Ljava/lang/String;

.field private axP:Ljava/lang/String;

.field private axQ:Ljava/lang/String;

.field private axR:Ljava/lang/String;

.field private axS:Ljava/util/Map;

.field private axT:Ljava/lang/String;

.field private axU:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    if-nez p3, :cond_0

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "A URI column must be specified for table "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    iput-object p1, p0, Lbej;->axM:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lbej;->axN:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lbej;->axP:Ljava/lang/String;

    .line 55
    if-nez p4, :cond_1

    const-string p4, "0"

    :cond_1
    iput-object p4, p0, Lbej;->axQ:Ljava/lang/String;

    .line 56
    if-nez p5, :cond_2

    const-string p5, "0"

    :cond_2
    iput-object p5, p0, Lbej;->axR:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p6}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbej;->axS:Ljava/util/Map;

    .line 60
    iput-object p7, p0, Lbej;->axT:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lbej;->axN:Ljava/lang/String;

    iput-object v0, p0, Lbej;->axO:Ljava/lang/String;

    .line 62
    iput-boolean p9, p0, Lbej;->axU:Z

    .line 63
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 42
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lbej;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 44
    return-void
.end method


# virtual methods
.method public final getDataSourceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbej;->axO:Ljava/lang/String;

    return-object v0
.end method

.method public final xF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbej;->axM:Ljava/lang/String;

    return-object v0
.end method

.method public final xG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lbej;->axN:Ljava/lang/String;

    return-object v0
.end method

.method public final xH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lbej;->axP:Ljava/lang/String;

    return-object v0
.end method

.method public final xI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbej;->axQ:Ljava/lang/String;

    return-object v0
.end method

.method public final xJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lbej;->axR:Ljava/lang/String;

    return-object v0
.end method

.method public final xK()Ljava/util/Map;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbej;->axS:Ljava/util/Map;

    return-object v0
.end method

.method public final xL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lbej;->axT:Ljava/lang/String;

    return-object v0
.end method

.method public final xM()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lbej;->axU:Z

    return v0
.end method
