.class public final Lhly;
.super Lhlu;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Leoj;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 29
    iput-object p2, p0, Lhly;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public final synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v0

    iget-object v1, p0, Lhly;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Ldye;->a(Ljpe;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v1

    iget-object v0, v1, Ljpe;->exC:[Ljpd;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Ljpe;->exC:[Ljpd;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    :goto_0
    iget-object v2, p0, Lhly;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v2}, Ldye;->b(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v2

    iget-object v3, v2, Ljpe;->exC:[Ljpd;

    array-length v3, v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, v2, Ljpe;->exC:[Ljpd;

    aget-object v0, v0, v1

    iget-object v1, p0, Lhly;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Ldye;->a(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 4

    .prologue
    const/16 v1, 0xc8

    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    const/16 v0, 0x64

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lhly;->mIntentStarter:Leoj;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v1

    invoke-virtual {v1}, Ljpe;->brF()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhly;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Ledx;->b(Ljava/lang/String;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    if-eq p2, v1, :cond_1

    const/16 v0, 0x12c

    if-ne p2, v0, :cond_4

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahx()Ljpd;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    if-ne p2, v1, :cond_3

    iget-object v1, p0, Lhly;->mIntentStarter:Leoj;

    invoke-virtual {v0}, Ljpd;->brF()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lhly;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Ledx;->b(Ljava/lang/String;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lhly;->mIntentStarter:Leoj;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v2

    iget-object v3, p0, Lhly;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v3}, Ldye;->a(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahy()V

    invoke-super {p0, p1, p2}, Lhlu;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    goto :goto_0
.end method
