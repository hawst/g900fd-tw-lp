.class public final Lcpk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldvn;


# direct methods
.method public constructor <init>(Lcpd;)V
    .locals 0

    .prologue
    .line 789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x2e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x29

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x27

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x2b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x25

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v0

    invoke-virtual {v0}, Ljpe;->oY()I

    move-result v0

    invoke-static {v0}, Ldye;->gV(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x38

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x46

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 788
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PhoneCallAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aia()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x2c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x2f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x22

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x37

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/16 v0, 0x21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
