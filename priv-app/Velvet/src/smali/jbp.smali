.class public final Ljbp;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dYS:[Ljbp;


# instance fields
.field private aeI:D

.field private aeJ:D

.field private aez:I

.field private agq:Ljava/lang/String;

.field private dNN:J

.field private dYT:Ljava/lang/String;

.field private dYU:D

.field private dYV:Z

.field private dYW:I

.field private dYX:I

.field private dYY:Ljava/lang/String;

.field public dYZ:Lixu;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 28200
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 28201
    iput v4, p0, Ljbp;->aez:I

    iput-wide v2, p0, Ljbp;->aeI:D

    iput-wide v2, p0, Ljbp;->aeJ:D

    const-string v0, ""

    iput-object v0, p0, Ljbp;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbp;->dYT:Ljava/lang/String;

    iput-wide v2, p0, Ljbp;->dYU:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbp;->dNN:J

    iput-boolean v4, p0, Ljbp;->dYV:Z

    const/4 v0, 0x1

    iput v0, p0, Ljbp;->dYW:I

    iput v4, p0, Ljbp;->dYX:I

    const-string v0, ""

    iput-object v0, p0, Ljbp;->dYY:Ljava/lang/String;

    iput-object v5, p0, Ljbp;->dYZ:Lixu;

    iput-object v5, p0, Ljbp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbp;->eCz:I

    .line 28202
    return-void
.end method

.method public static beZ()[Ljbp;
    .locals 2

    .prologue
    .line 27985
    sget-object v0, Ljbp;->dYS:[Ljbp;

    if-nez v0, :cond_1

    .line 27986
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 27988
    :try_start_0
    sget-object v0, Ljbp;->dYS:[Ljbp;

    if-nez v0, :cond_0

    .line 27989
    const/4 v0, 0x0

    new-array v0, v0, [Ljbp;

    sput-object v0, Ljbp;->dYS:[Ljbp;

    .line 27991
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27993
    :cond_1
    sget-object v0, Ljbp;->dYS:[Ljbp;

    return-object v0

    .line 27991
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 27975
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljbp;->aeI:D

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljbp;->aeJ:D

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbp;->agq:Ljava/lang/String;

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbp;->dYT:Ljava/lang/String;

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljbp;->dYU:D

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljbp;->dNN:J

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbp;->dYV:Z

    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljbp;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljbp;->dYW:I

    iget v0, p0, Ljbp;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbp;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbp;->dYX:I

    iget v0, p0, Ljbp;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljbp;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbp;->dYY:Ljava/lang/String;

    iget v0, p0, Ljbp;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljbp;->aez:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljbp;->dYZ:Lixu;

    if-nez v0, :cond_1

    new-instance v0, Lixu;

    invoke-direct {v0}, Lixu;-><init>()V

    iput-object v0, p0, Ljbp;->dYZ:Lixu;

    :cond_1
    iget-object v0, p0, Ljbp;->dYZ:Lixu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x31 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 28225
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 28226
    const/4 v0, 0x1

    iget-wide v2, p0, Ljbp;->aeI:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 28228
    :cond_0
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 28229
    const/4 v0, 0x2

    iget-wide v2, p0, Ljbp;->aeJ:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 28231
    :cond_1
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 28232
    const/4 v0, 0x3

    iget-object v1, p0, Ljbp;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28234
    :cond_2
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 28235
    const/4 v0, 0x4

    iget-object v1, p0, Ljbp;->dYT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28237
    :cond_3
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 28238
    const/4 v0, 0x6

    iget-wide v2, p0, Ljbp;->dYU:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 28240
    :cond_4
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 28241
    const/4 v0, 0x7

    iget-wide v2, p0, Ljbp;->dNN:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 28243
    :cond_5
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 28244
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljbp;->dYV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 28246
    :cond_6
    iget v0, p0, Ljbp;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 28247
    const/16 v0, 0x9

    iget v1, p0, Ljbp;->dYW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 28249
    :cond_7
    iget v0, p0, Ljbp;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    .line 28250
    const/16 v0, 0xa

    iget v1, p0, Ljbp;->dYX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 28252
    :cond_8
    iget v0, p0, Ljbp;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_9

    .line 28253
    const/16 v0, 0xb

    iget-object v1, p0, Ljbp;->dYY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28255
    :cond_9
    iget-object v0, p0, Ljbp;->dYZ:Lixu;

    if-eqz v0, :cond_a

    .line 28256
    const/16 v0, 0xc

    iget-object v1, p0, Ljbp;->dYZ:Lixu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 28258
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 28259
    return-void
.end method

.method public final baq()J
    .locals 2

    .prologue
    .line 28102
    iget-wide v0, p0, Ljbp;->dNN:J

    return-wide v0
.end method

.method public final bar()Z
    .locals 1

    .prologue
    .line 28110
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfa()Ljbp;
    .locals 1

    .prologue
    .line 28053
    const-string v0, ""

    iput-object v0, p0, Ljbp;->agq:Ljava/lang/String;

    .line 28054
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Ljbp;->aez:I

    .line 28055
    return-object p0
.end method

.method public final bfb()Z
    .locals 1

    .prologue
    .line 28072
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfc()D
    .locals 2

    .prologue
    .line 28083
    iget-wide v0, p0, Ljbp;->dYU:D

    return-wide v0
.end method

.method public final bfd()Z
    .locals 1

    .prologue
    .line 28091
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfe()I
    .locals 1

    .prologue
    .line 28140
    iget v0, p0, Ljbp;->dYW:I

    return v0
.end method

.method public final bff()Z
    .locals 1

    .prologue
    .line 28148
    iget v0, p0, Ljbp;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28178
    iget-object v0, p0, Ljbp;->dYY:Ljava/lang/String;

    return-object v0
.end method

.method public final bfh()Z
    .locals 1

    .prologue
    .line 28189
    iget v0, p0, Ljbp;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cF(J)Ljbp;
    .locals 1

    .prologue
    .line 28105
    iput-wide p1, p0, Ljbp;->dNN:J

    .line 28106
    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbp;->aez:I

    .line 28107
    return-object p0
.end method

.method public final getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28061
    iget-object v0, p0, Ljbp;->dYT:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28039
    iget-object v0, p0, Ljbp;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 28263
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 28264
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 28265
    const/4 v1, 0x1

    iget-wide v2, p0, Ljbp;->aeI:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 28268
    :cond_0
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 28269
    const/4 v1, 0x2

    iget-wide v2, p0, Ljbp;->aeJ:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 28272
    :cond_1
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 28273
    const/4 v1, 0x3

    iget-object v2, p0, Ljbp;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28276
    :cond_2
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 28277
    const/4 v1, 0x4

    iget-object v2, p0, Ljbp;->dYT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28280
    :cond_3
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 28281
    const/4 v1, 0x6

    iget-wide v2, p0, Ljbp;->dYU:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 28284
    :cond_4
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 28285
    const/4 v1, 0x7

    iget-wide v2, p0, Ljbp;->dNN:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 28288
    :cond_5
    iget v1, p0, Ljbp;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 28289
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljbp;->dYV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 28292
    :cond_6
    iget v1, p0, Ljbp;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 28293
    const/16 v1, 0x9

    iget v2, p0, Ljbp;->dYW:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28296
    :cond_7
    iget v1, p0, Ljbp;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    .line 28297
    const/16 v1, 0xa

    iget v2, p0, Ljbp;->dYX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28300
    :cond_8
    iget v1, p0, Ljbp;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    .line 28301
    const/16 v1, 0xb

    iget-object v2, p0, Ljbp;->dYY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28304
    :cond_9
    iget-object v1, p0, Ljbp;->dYZ:Lixu;

    if-eqz v1, :cond_a

    .line 28305
    const/16 v1, 0xc

    iget-object v2, p0, Ljbp;->dYZ:Lixu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28308
    :cond_a
    return v0
.end method

.method public final mR()D
    .locals 2

    .prologue
    .line 28001
    iget-wide v0, p0, Ljbp;->aeI:D

    return-wide v0
.end method

.method public final mS()D
    .locals 2

    .prologue
    .line 28020
    iget-wide v0, p0, Ljbp;->aeJ:D

    return-wide v0
.end method

.method public final n(D)Ljbp;
    .locals 1

    .prologue
    .line 28004
    iput-wide p1, p0, Ljbp;->aeI:D

    .line 28005
    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbp;->aez:I

    .line 28006
    return-object p0
.end method

.method public final nH()Z
    .locals 1

    .prologue
    .line 28009
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nI()Z
    .locals 1

    .prologue
    .line 28028
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o(D)Ljbp;
    .locals 1

    .prologue
    .line 28023
    iput-wide p1, p0, Ljbp;->aeJ:D

    .line 28024
    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbp;->aez:I

    .line 28025
    return-object p0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 28050
    iget v0, p0, Ljbp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final os(I)Ljbp;
    .locals 1

    .prologue
    .line 28143
    iput p1, p0, Ljbp;->dYW:I

    .line 28144
    iget v0, p0, Ljbp;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbp;->aez:I

    .line 28145
    return-object p0
.end method

.method public final p(D)Ljbp;
    .locals 1

    .prologue
    .line 28086
    iput-wide p1, p0, Ljbp;->dYU:D

    .line 28087
    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbp;->aez:I

    .line 28088
    return-object p0
.end method

.method public final st(Ljava/lang/String;)Ljbp;
    .locals 1

    .prologue
    .line 28042
    if-nez p1, :cond_0

    .line 28043
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28045
    :cond_0
    iput-object p1, p0, Ljbp;->agq:Ljava/lang/String;

    .line 28046
    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbp;->aez:I

    .line 28047
    return-object p0
.end method

.method public final su(Ljava/lang/String;)Ljbp;
    .locals 1

    .prologue
    .line 28064
    if-nez p1, :cond_0

    .line 28065
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28067
    :cond_0
    iput-object p1, p0, Ljbp;->dYT:Ljava/lang/String;

    .line 28068
    iget v0, p0, Ljbp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbp;->aez:I

    .line 28069
    return-object p0
.end method
