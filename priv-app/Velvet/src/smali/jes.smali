.class public final Ljes;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile efX:[Ljes;


# instance fields
.field private aez:I

.field private dRS:I

.field private efY:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15462
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15463
    iput v0, p0, Ljes;->aez:I

    iput v0, p0, Ljes;->dRS:I

    iput-boolean v0, p0, Ljes;->efY:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljes;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljes;->eCz:I

    .line 15464
    return-void
.end method

.method public static bje()[Ljes;
    .locals 2

    .prologue
    .line 15411
    sget-object v0, Ljes;->efX:[Ljes;

    if-nez v0, :cond_1

    .line 15412
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 15414
    :try_start_0
    sget-object v0, Ljes;->efX:[Ljes;

    if-nez v0, :cond_0

    .line 15415
    const/4 v0, 0x0

    new-array v0, v0, [Ljes;

    sput-object v0, Ljes;->efX:[Ljes;

    .line 15417
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15419
    :cond_1
    sget-object v0, Ljes;->efX:[Ljes;

    return-object v0

    .line 15417
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 15405
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljes;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljes;->dRS:I

    iget v0, p0, Ljes;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljes;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljes;->efY:Z

    iget v0, p0, Ljes;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljes;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 15478
    iget v0, p0, Ljes;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15479
    const/4 v0, 0x1

    iget v1, p0, Ljes;->dRS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 15481
    :cond_0
    iget v0, p0, Ljes;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15482
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljes;->efY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15484
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15485
    return-void
.end method

.method public final bcO()I
    .locals 1

    .prologue
    .line 15427
    iget v0, p0, Ljes;->dRS:I

    return v0
.end method

.method public final bjf()Z
    .locals 1

    .prologue
    .line 15446
    iget-boolean v0, p0, Ljes;->efY:Z

    return v0
.end method

.method public final hU(Z)Ljes;
    .locals 1

    .prologue
    .line 15449
    iput-boolean p1, p0, Ljes;->efY:Z

    .line 15450
    iget v0, p0, Ljes;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljes;->aez:I

    .line 15451
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 15489
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 15490
    iget v1, p0, Ljes;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15491
    const/4 v1, 0x1

    iget v2, p0, Ljes;->dRS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15494
    :cond_0
    iget v1, p0, Ljes;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15495
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljes;->efY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 15498
    :cond_1
    return v0
.end method

.method public final pr(I)Ljes;
    .locals 1

    .prologue
    .line 15430
    iput p1, p0, Ljes;->dRS:I

    .line 15431
    iget v0, p0, Ljes;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljes;->aez:I

    .line 15432
    return-object p0
.end method
