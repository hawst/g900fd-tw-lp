.class public final Ldoh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field private bDY:Lcom/google/android/shared/search/Query;

.field private bDZ:Z

.field private final mContext:Landroid/content/Context;

.field private final mEventBus:Ldda;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mTtsState:Ldcw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldda;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ldda;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Ldoh;->mContext:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Ldoh;->mEventBus:Ldda;

    .line 39
    iget-object v0, p0, Ldoh;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldoh;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 40
    iget-object v0, p0, Ldoh;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    iput-object v0, p0, Ldoh;->mTtsState:Ldcw;

    .line 41
    return-void
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 107
    const/16 v0, 0x8

    return v0
.end method

.method public final a(Lddb;)V
    .locals 3

    .prologue
    .line 71
    iget-boolean v0, p0, Ldoh;->bDZ:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldoh;->mTtsState:Ldcw;

    invoke-virtual {v0}, Ldcw;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Ldoh;->bDY:Lcom/google/android/shared/search/Query;

    if-nez v0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldoh;->bDZ:Z

    .line 92
    iget-object v0, p0, Ldoh;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Ldoh;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v2, p0, Ldoh;->bDY:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    .line 99
    :cond_2
    iget-boolean v0, p0, Ldoh;->bDZ:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldoh;->mTtsState:Ldcw;

    invoke-virtual {v0}, Ldcw;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    goto :goto_0
.end method

.method public final aK(Lcom/google/android/shared/search/Query;)V
    .locals 5
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 46
    iget-object v0, p0, Ldoh;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->c(Lddc;)V

    .line 48
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apj()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Ldoh;->bDY:Lcom/google/android/shared/search/Query;

    .line 52
    iget-object v0, p0, Ldoh;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Ldoh;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-object v3, p0, Ldoh;->mContext:Landroid/content/Context;

    const v4, 0x7f0a06de

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/shared/search/Query;->az(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    .line 56
    return-void
.end method
