.class public final Ljnt;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajk:Ljava/lang/String;

.field private akf:Ljava/lang/String;

.field private eaO:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2146
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2147
    iput v1, p0, Ljnt;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnt;->ajk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnt;->akf:Ljava/lang/String;

    iput v1, p0, Ljnt;->eaO:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljnt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnt;->eCz:I

    .line 2148
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2060
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnt;->ajk:Ljava/lang/String;

    iget v0, p0, Ljnt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnt;->akf:Ljava/lang/String;

    iget v0, p0, Ljnt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnt;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljnt;->eaO:I

    iget v0, p0, Ljnt;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnt;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2163
    iget v0, p0, Ljnt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2164
    const/4 v0, 0x1

    iget-object v1, p0, Ljnt;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2166
    :cond_0
    iget v0, p0, Ljnt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2167
    const/4 v0, 0x2

    iget-object v1, p0, Ljnt;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2169
    :cond_1
    iget v0, p0, Ljnt;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2170
    const/4 v0, 0x3

    iget v1, p0, Ljnt;->eaO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2172
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2173
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2177
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2178
    iget v1, p0, Ljnt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2179
    const/4 v1, 0x1

    iget-object v2, p0, Ljnt;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2182
    :cond_0
    iget v1, p0, Ljnt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2183
    const/4 v1, 0x2

    iget-object v2, p0, Ljnt;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2186
    :cond_1
    iget v1, p0, Ljnt;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2187
    const/4 v1, 0x3

    iget v2, p0, Ljnt;->eaO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2190
    :cond_2
    return v0
.end method
