.class public final Leoa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final cgu:Ljava/lang/Integer;

.field private final cgv:Ljava/util/concurrent/atomic/AtomicLong;

.field private final cgw:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final cgx:Z

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Leoa;-><init>(Ljava/lang/String;IZ)V

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Leoa;->cgv:Ljava/util/concurrent/atomic/AtomicLong;

    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Leoa;->cgw:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 48
    if-ltz p2, :cond_0

    const/16 v0, 0x13

    if-gt p2, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Invalid priority: %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 51
    iput-object p1, p0, Leoa;->mName:Ljava/lang/String;

    .line 52
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leoa;->cgu:Ljava/lang/Integer;

    .line 53
    iput-boolean p3, p0, Leoa;->cgx:Z

    .line 54
    return-void

    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    :cond_1
    move v1, v2

    .line 50
    goto :goto_1
.end method

.method static synthetic a(Leoa;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Leoa;->cgw:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic b(Leoa;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Leoa;->cgu:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public final newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 6

    .prologue
    const/16 v5, 0x1e

    .line 72
    iget-boolean v0, p0, Leoa;->cgx:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leoa;->cgw:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gt v0, v5, :cond_1

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Leoa;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Leoa;->cgv:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 74
    new-instance v0, Leob;

    invoke-direct {v0, p0, p1, v1}, Leob;-><init>(Leoa;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 90
    :goto_0
    return-object v0

    .line 88
    :cond_1
    const-string v0, "GsaThreadFactory"

    const-string v1, "Not creating another thread for %s because there are already %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Leoa;->mName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 90
    const/4 v0, 0x0

    goto :goto_0
.end method
