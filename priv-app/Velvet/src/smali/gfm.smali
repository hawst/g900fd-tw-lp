.class public Lgfm;
.super Ljava/io/InputStream;
.source "PG"


# static fields
.field private static volatile cIG:I


# instance fields
.field protected final anB:I

.field protected final cIH:I

.field protected final cII:Lgfn;

.field protected final cIJ:Z

.field protected final cIK:Z

.field private final cIL:Ljava/lang/String;

.field protected cIM:Landroid/media/AudioRecord;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cIN:Z

.field private cq:Z

.field private dK:Ljava/lang/Object;

.field private mClosed:Z


# direct methods
.method public constructor <init>(IIZLgfn;Z)V
    .locals 3
    .param p4    # Lgfn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgfm;->dK:Ljava/lang/Object;

    .line 46
    iput-boolean v1, p0, Lgfm;->cIN:Z

    .line 47
    iput-boolean v1, p0, Lgfm;->cq:Z

    .line 107
    iput p1, p0, Lgfm;->anB:I

    .line 108
    const/16 v0, 0x10

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lgfm;->cIH:I

    .line 111
    iput-boolean p3, p0, Lgfm;->cIK:Z

    .line 112
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lgfm;->cII:Lgfn;

    .line 113
    iput-boolean p5, p0, Lgfm;->cIJ:Z

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MicrophoneInputStream_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lgfm;->cIG:I

    add-int/lit8 v2, v1, 0x1

    sput v2, Lgfm;->cIG:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgfm;->cIL:Ljava/lang/String;

    .line 115
    return-void

    .line 112
    :cond_0
    new-instance p4, Lgfn;

    invoke-direct {p4}, Lgfn;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected aFC()Landroid/media/AudioRecord;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 120
    const/4 v1, 0x6

    .line 121
    iget-boolean v0, p0, Lgfm;->cIJ:Z

    if-eqz v0, :cond_0

    .line 123
    const/16 v1, 0x7cf

    .line 125
    :cond_0
    new-instance v0, Landroid/media/AudioRecord;

    iget v2, p0, Lgfm;->anB:I

    const/16 v3, 0x10

    const/4 v4, 0x2

    iget v5, p0, Lgfm;->cIH:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 128
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 129
    const-string v1, "MicrophoneInputStream"

    const-string v2, "Failed to initialize AudioRecord"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 130
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 131
    const/4 v0, 0x0

    .line 134
    :cond_1
    return-object v0
.end method

.method protected aFE()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method protected aFF()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    .line 245
    iget-object v0, p0, Lgfm;->cII:Lgfn;

    iget-object v1, p0, Lgfm;->cIL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgfn;->mB(Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lgfm;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 248
    :try_start_0
    iget-object v0, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgfm;->mClosed:Z

    if-nez v0, :cond_0

    .line 249
    const-string v0, "MicrophoneInputStream"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mic_close "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 252
    iget-object v0, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 253
    invoke-virtual {p0}, Lgfm;->aFF()V

    .line 254
    iget-object v0, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfm;->mClosed:Z

    .line 257
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public read()I
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Single-byte read not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([B)I
    .locals 2

    .prologue
    .line 195
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lgfm;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 206
    iget-object v2, p0, Lgfm;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 207
    :try_start_0
    iget-boolean v1, p0, Lgfm;->mClosed:Z

    if-eqz v1, :cond_0

    .line 208
    monitor-exit v2

    .line 237
    :goto_0
    return v0

    .line 210
    :cond_0
    iget-object v1, p0, Lgfm;->dK:Ljava/lang/Object;

    iget-boolean v1, p0, Lgfm;->cIN:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    if-nez v1, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "AudioRecord failed to initialize."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 210
    :cond_1
    :try_start_1
    iget-boolean v1, p0, Lgfm;->cq:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    .line 211
    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    invoke-virtual {v1, p1, p2, p3}, Landroid/media/AudioRecord;->read([BII)I

    move-result v1

    .line 218
    iget-object v2, p0, Lgfm;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 219
    :try_start_2
    iget-boolean v3, p0, Lgfm;->mClosed:Z

    if-eqz v3, :cond_6

    .line 220
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 222
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 210
    :cond_2
    :try_start_3
    const-string v1, "MicrophoneInputStream"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mic_starting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iget-object v1, p0, Lgfm;->cII:Lgfn;

    iget-object v3, p0, Lgfm;->cIL:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lgfn;->mA(Ljava/lang/String;)V

    iget-boolean v1, p0, Lgfm;->cIN:Z

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lgfm;->aFC()Landroid/media/AudioRecord;

    move-result-object v1

    iput-object v1, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lgfm;->cIN:Z

    :cond_3
    iget-object v1, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    if-nez v1, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "AudioRecord failed to initialize."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-virtual {p0}, Lgfm;->aFE()V

    invoke-virtual {p0}, Lgfm;->startRecording()V

    iget-object v1, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_5

    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t start recording, state is:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lgfm;->cq:Z

    const-string v1, "MicrophoneInputStream"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mic_started "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iget-object v1, p0, Lgfm;->cIM:Landroid/media/AudioRecord;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 222
    :cond_6
    monitor-exit v2

    .line 224
    if-ge v1, v0, :cond_9

    .line 225
    const/4 v0, -0x3

    if-ne v1, v0, :cond_7

    .line 226
    new-instance v0, Ljava/io/IOException;

    const-string v1, "not open"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_7
    const/4 v0, -0x2

    if-ne v1, v0, :cond_8

    .line 230
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Bad offset/length arguments for buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_8
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected error code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move v0, v1

    .line 237
    goto/16 :goto_0
.end method

.method protected startRecording()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lgfm;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 186
    return-void
.end method
