.class public final Ljvs;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eHP:[Ljvs;


# instance fields
.field private aez:I

.field private aiC:Ljava/lang/String;

.field private dzl:F

.field private eHQ:Ljava/lang/String;

.field private eHR:Ljava/lang/String;

.field public eHS:Ljvy;

.field private eHT:Z

.field private eHU:Ljuc;

.field private eHV:Ljuc;

.field private eHW:Ljuc;

.field private eHX:[Ljuf;

.field public eHY:Ljtn;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 267
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 268
    const/4 v0, 0x0

    iput v0, p0, Ljvs;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljvs;->aiC:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljvs;->eHQ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljvs;->eHR:Ljava/lang/String;

    iput-object v1, p0, Ljvs;->eHS:Ljvy;

    const/4 v0, 0x0

    iput v0, p0, Ljvs;->dzl:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljvs;->eHT:Z

    iput-object v1, p0, Ljvs;->eHU:Ljuc;

    iput-object v1, p0, Ljvs;->eHV:Ljuc;

    iput-object v1, p0, Ljvs;->eHW:Ljuc;

    invoke-static {}, Ljuf;->buF()[Ljuf;

    move-result-object v0

    iput-object v0, p0, Ljvs;->eHX:[Ljuf;

    iput-object v1, p0, Ljvs;->eHY:Ljtn;

    iput-object v1, p0, Ljvs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvs;->eCz:I

    .line 269
    return-void
.end method

.method public static bvd()[Ljvs;
    .locals 2

    .prologue
    .line 132
    sget-object v0, Ljvs;->eHP:[Ljvs;

    if-nez v0, :cond_1

    .line 133
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    sget-object v0, Ljvs;->eHP:[Ljvs;

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    new-array v0, v0, [Ljvs;

    sput-object v0, Ljvs;->eHP:[Ljvs;

    .line 138
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_1
    sget-object v0, Ljvs;->eHP:[Ljvs;

    return-object v0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 126
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvs;->aiC:Ljava/lang/String;

    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvs;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljvs;->dzl:F

    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljvs;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljvs;->eHV:Ljuc;

    if-nez v0, :cond_1

    new-instance v0, Ljuc;

    invoke-direct {v0}, Ljuc;-><init>()V

    iput-object v0, p0, Ljvs;->eHV:Ljuc;

    :cond_1
    iget-object v0, p0, Ljvs;->eHV:Ljuc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljvs;->eHW:Ljuc;

    if-nez v0, :cond_2

    new-instance v0, Ljuc;

    invoke-direct {v0}, Ljuc;-><init>()V

    iput-object v0, p0, Ljvs;->eHW:Ljuc;

    :cond_2
    iget-object v0, p0, Ljvs;->eHW:Ljuc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvs;->eHX:[Ljuf;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljuf;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljvs;->eHX:[Ljuf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljuf;

    invoke-direct {v3}, Ljuf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljvs;->eHX:[Ljuf;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljuf;

    invoke-direct {v3}, Ljuf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvs;->eHX:[Ljuf;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ljvs;->eHY:Ljtn;

    if-nez v0, :cond_6

    new-instance v0, Ljtn;

    invoke-direct {v0}, Ljtn;-><init>()V

    iput-object v0, p0, Ljvs;->eHY:Ljtn;

    :cond_6
    iget-object v0, p0, Ljvs;->eHY:Ljtn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljvs;->eHS:Ljvy;

    if-nez v0, :cond_7

    new-instance v0, Ljvy;

    invoke-direct {v0}, Ljvy;-><init>()V

    iput-object v0, p0, Ljvs;->eHS:Ljvy;

    :cond_7
    iget-object v0, p0, Ljvs;->eHS:Ljvy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljvs;->eHU:Ljuc;

    if-nez v0, :cond_8

    new-instance v0, Ljuc;

    invoke-direct {v0}, Ljuc;-><init>()V

    iput-object v0, p0, Ljvs;->eHU:Ljuc;

    :cond_8
    iget-object v0, p0, Ljvs;->eHU:Ljuc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljvs;->eHT:Z

    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljvs;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvs;->eHQ:Ljava/lang/String;

    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvs;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvs;->eHR:Ljava/lang/String;

    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvs;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x52 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 292
    iget v0, p0, Ljvs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 293
    const/4 v0, 0x1

    iget-object v1, p0, Ljvs;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 295
    :cond_0
    iget v0, p0, Ljvs;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    .line 296
    const/4 v0, 0x2

    iget v1, p0, Ljvs;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 298
    :cond_1
    iget-object v0, p0, Ljvs;->eHV:Ljuc;

    if-eqz v0, :cond_2

    .line 299
    const/4 v0, 0x3

    iget-object v1, p0, Ljvs;->eHV:Ljuc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 301
    :cond_2
    iget-object v0, p0, Ljvs;->eHW:Ljuc;

    if-eqz v0, :cond_3

    .line 302
    const/4 v0, 0x4

    iget-object v1, p0, Ljvs;->eHW:Ljuc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 304
    :cond_3
    iget-object v0, p0, Ljvs;->eHX:[Ljuf;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljvs;->eHX:[Ljuf;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 305
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljvs;->eHX:[Ljuf;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 306
    iget-object v1, p0, Ljvs;->eHX:[Ljuf;

    aget-object v1, v1, v0

    .line 307
    if-eqz v1, :cond_4

    .line 308
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 305
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    :cond_5
    iget-object v0, p0, Ljvs;->eHY:Ljtn;

    if-eqz v0, :cond_6

    .line 313
    const/4 v0, 0x6

    iget-object v1, p0, Ljvs;->eHY:Ljtn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 315
    :cond_6
    iget-object v0, p0, Ljvs;->eHS:Ljvy;

    if-eqz v0, :cond_7

    .line 316
    const/4 v0, 0x7

    iget-object v1, p0, Ljvs;->eHS:Ljvy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 318
    :cond_7
    iget-object v0, p0, Ljvs;->eHU:Ljuc;

    if-eqz v0, :cond_8

    .line 319
    const/16 v0, 0xa

    iget-object v1, p0, Ljvs;->eHU:Ljuc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 321
    :cond_8
    iget v0, p0, Ljvs;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 322
    const/16 v0, 0xb

    iget-boolean v1, p0, Ljvs;->eHT:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 324
    :cond_9
    iget v0, p0, Ljvs;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    .line 325
    const/16 v0, 0xc

    iget-object v1, p0, Ljvs;->eHQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 327
    :cond_a
    iget v0, p0, Ljvs;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_b

    .line 328
    const/16 v0, 0xd

    iget-object v1, p0, Ljvs;->eHR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 330
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 331
    return-void
.end method

.method public final aa(F)Ljvs;
    .locals 1

    .prologue
    .line 220
    iput p1, p0, Ljvs;->dzl:F

    .line 221
    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljvs;->aez:I

    .line 222
    return-object p0
.end method

.method public final bty()F
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Ljvs;->dzl:F

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Ljvs;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method public final hasText()Z
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Ljvs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 335
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 336
    iget v1, p0, Ljvs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 337
    const/4 v1, 0x1

    iget-object v2, p0, Ljvs;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    :cond_0
    iget v1, p0, Ljvs;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    .line 341
    const/4 v1, 0x2

    iget v2, p0, Ljvs;->dzl:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 344
    :cond_1
    iget-object v1, p0, Ljvs;->eHV:Ljuc;

    if-eqz v1, :cond_2

    .line 345
    const/4 v1, 0x3

    iget-object v2, p0, Ljvs;->eHV:Ljuc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    :cond_2
    iget-object v1, p0, Ljvs;->eHW:Ljuc;

    if-eqz v1, :cond_3

    .line 349
    const/4 v1, 0x4

    iget-object v2, p0, Ljvs;->eHW:Ljuc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    :cond_3
    iget-object v1, p0, Ljvs;->eHX:[Ljuf;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljvs;->eHX:[Ljuf;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 353
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljvs;->eHX:[Ljuf;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 354
    iget-object v2, p0, Ljvs;->eHX:[Ljuf;

    aget-object v2, v2, v0

    .line 355
    if-eqz v2, :cond_4

    .line 356
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 353
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 361
    :cond_6
    iget-object v1, p0, Ljvs;->eHY:Ljtn;

    if-eqz v1, :cond_7

    .line 362
    const/4 v1, 0x6

    iget-object v2, p0, Ljvs;->eHY:Ljtn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_7
    iget-object v1, p0, Ljvs;->eHS:Ljvy;

    if-eqz v1, :cond_8

    .line 366
    const/4 v1, 0x7

    iget-object v2, p0, Ljvs;->eHS:Ljvy;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_8
    iget-object v1, p0, Ljvs;->eHU:Ljuc;

    if-eqz v1, :cond_9

    .line 370
    const/16 v1, 0xa

    iget-object v2, p0, Ljvs;->eHU:Ljuc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    :cond_9
    iget v1, p0, Ljvs;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 374
    const/16 v1, 0xb

    iget-boolean v2, p0, Ljvs;->eHT:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 377
    :cond_a
    iget v1, p0, Ljvs;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_b

    .line 378
    const/16 v1, 0xc

    iget-object v2, p0, Ljvs;->eHQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 381
    :cond_b
    iget v1, p0, Ljvs;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_c

    .line 382
    const/16 v1, 0xd

    iget-object v2, p0, Ljvs;->eHR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 385
    :cond_c
    return v0
.end method

.method public final zo(Ljava/lang/String;)Ljvs;
    .locals 1

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 152
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 154
    :cond_0
    iput-object p1, p0, Ljvs;->aiC:Ljava/lang/String;

    .line 155
    iget v0, p0, Ljvs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvs;->aez:I

    .line 156
    return-object p0
.end method
