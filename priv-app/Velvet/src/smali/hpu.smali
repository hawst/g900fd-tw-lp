.class public final Lhpu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lekh;


# instance fields
.field private synthetic dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lhpu;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hZ(I)V
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lhpu;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    if-eqz v0, :cond_0

    .line 292
    packed-switch p1, :pswitch_data_0

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 295
    :pswitch_0
    iget-object v0, p0, Lhpu;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-static {p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->lp(I)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    iget-object v1, v0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiA()Ljpd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhpn;->e(Ljpd;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, v0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiB()Ljpd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhpn;->e(Ljpd;)V

    goto :goto_0

    .line 298
    :pswitch_2
    iget-object v0, p0, Lhpu;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v0, v1, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lhpn;->dnE:Lhpq;

    invoke-interface {v0}, Lhpq;->vh()V

    iget-object v0, v1, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiz()Ljpd;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_1

    iget-object v3, v2, Ljpd;->exz:Ljpb;

    if-eqz v3, :cond_2

    iget-object v3, v2, Ljpd;->exz:Ljpb;

    invoke-virtual {v3}, Ljpb;->baV()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v2, Ljpd;->exz:Ljpb;

    invoke-virtual {v0}, Ljpb;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    iget-object v1, v1, Lhpn;->dnE:Lhpq;

    invoke-interface {v1, v0}, Lhpq;->ez(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljpd;->pb()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 292
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 295
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method
