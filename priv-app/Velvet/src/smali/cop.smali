.class public final Lcop;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbus;


# instance fields
.field private final bdW:Ljava/lang/String;

.field private final bdX:I

.field private final bdY:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcop;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcop;->bdW:Ljava/lang/String;

    .line 29
    iput p3, p0, Lcop;->bdX:I

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcop;->bdY:Z

    .line 31
    return-void
.end method


# virtual methods
.method public final AU()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public final AV()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final AW()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public final a(Ljava/lang/String;J[B)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 34
    new-instance v7, Lbur;

    iget-object v0, p0, Lcop;->mContext:Landroid/content/Context;

    iget v1, p0, Lcop;->bdX:I

    invoke-direct {v7, v0, v1, p0}, Lbur;-><init>(Landroid/content/Context;ILbus;)V

    .line 35
    iget-boolean v6, p0, Lcop;->bdY:Z

    iget-object v0, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/qt;->aGY:Z

    if-eq v6, v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/qt;

    iget-object v1, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-object v1, v1, Lcom/google/android/gms/internal/qt;->packageName:Ljava/lang/String;

    iget-object v2, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget v2, v2, Lcom/google/android/gms/internal/qt;->aGU:I

    iget-object v3, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget v3, v3, Lcom/google/android/gms/internal/qt;->aGV:I

    iget-object v4, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-object v4, v4, Lcom/google/android/gms/internal/qt;->aGW:Ljava/lang/String;

    iget-object v5, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-object v5, v5, Lcom/google/android/gms/internal/qt;->aGX:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qt;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, v7, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    .line 36
    :cond_0
    invoke-virtual {v7, p1}, Lbur;->fH(Ljava/lang/String;)Lbur;

    .line 37
    iget-object v4, p0, Lcop;->bdW:Ljava/lang/String;

    const/4 v0, 0x0

    new-array v6, v0, [Ljava/lang/String;

    move-object v1, v7

    move-wide v2, p2

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, Lbur;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    .line 38
    iget-object v0, v7, Lbur;->aGS:Lbpd;

    invoke-virtual {v0}, Lbpd;->start()V

    .line 40
    iget-object v0, p0, Lcop;->bdW:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    .line 42
    iget-object v2, p0, Lcop;->bdW:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "logEvent(): Logged "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, p4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :cond_1
    return-void
.end method
