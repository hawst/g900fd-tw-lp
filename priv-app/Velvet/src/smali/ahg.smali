.class public final Lahg;
.super Ljsr;
.source "PG"


# instance fields
.field public Yh:Ljava/lang/String;

.field public Yi:Z

.field public Yj:Lahe;

.field public Yk:Lahe;

.field public label:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 949
    invoke-direct {p0}, Ljsr;-><init>()V

    .line 950
    const-string v0, ""

    iput-object v0, p0, Lahg;->Yh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lahg;->label:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lahg;->Yi:Z

    iput-object v1, p0, Lahg;->Yj:Lahe;

    iput-object v1, p0, Lahg;->Yk:Lahe;

    const/4 v0, -0x1

    iput v0, p0, Lahg;->eCz:I

    .line 951
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 917
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Ljsu;->b(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lahg;->Yh:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lahg;->label:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lahg;->Yi:Z

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lahg;->Yj:Lahe;

    if-nez v0, :cond_1

    new-instance v0, Lahe;

    invoke-direct {v0}, Lahe;-><init>()V

    iput-object v0, p0, Lahg;->Yj:Lahe;

    :cond_1
    iget-object v0, p0, Lahg;->Yj:Lahe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lahg;->Yk:Lahe;

    if-nez v0, :cond_2

    new-instance v0, Lahe;

    invoke-direct {v0}, Lahe;-><init>()V

    iput-object v0, p0, Lahg;->Yk:Lahe;

    :cond_2
    iget-object v0, p0, Lahg;->Yk:Lahe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 966
    const/4 v0, 0x1

    iget-object v1, p0, Lahg;->Yh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 967
    iget-object v0, p0, Lahg;->label:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 968
    const/4 v0, 0x2

    iget-object v1, p0, Lahg;->label:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 970
    :cond_0
    iget-boolean v0, p0, Lahg;->Yi:Z

    if-eqz v0, :cond_1

    .line 971
    const/4 v0, 0x3

    iget-boolean v1, p0, Lahg;->Yi:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 973
    :cond_1
    iget-object v0, p0, Lahg;->Yj:Lahe;

    if-eqz v0, :cond_2

    .line 974
    const/4 v0, 0x4

    iget-object v1, p0, Lahg;->Yj:Lahe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 976
    :cond_2
    iget-object v0, p0, Lahg;->Yk:Lahe;

    if-eqz v0, :cond_3

    .line 977
    const/4 v0, 0x5

    iget-object v1, p0, Lahg;->Yk:Lahe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 979
    :cond_3
    invoke-super {p0, p1}, Ljsr;->a(Ljsj;)V

    .line 980
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 984
    invoke-super {p0}, Ljsr;->lF()I

    move-result v0

    .line 985
    const/4 v1, 0x1

    iget-object v2, p0, Lahg;->Yh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 987
    iget-object v1, p0, Lahg;->label:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 988
    const/4 v1, 0x2

    iget-object v2, p0, Lahg;->label:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 991
    :cond_0
    iget-boolean v1, p0, Lahg;->Yi:Z

    if-eqz v1, :cond_1

    .line 992
    const/4 v1, 0x3

    iget-boolean v2, p0, Lahg;->Yi:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 995
    :cond_1
    iget-object v1, p0, Lahg;->Yj:Lahe;

    if-eqz v1, :cond_2

    .line 996
    const/4 v1, 0x4

    iget-object v2, p0, Lahg;->Yj:Lahe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 999
    :cond_2
    iget-object v1, p0, Lahg;->Yk:Lahe;

    if-eqz v1, :cond_3

    .line 1000
    const/4 v1, 0x5

    iget-object v2, p0, Lahg;->Yk:Lahe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1003
    :cond_3
    return v0
.end method
