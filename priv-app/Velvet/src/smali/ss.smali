.class public Lss;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Ltz;
.implements Luo;


# instance fields
.field protected xZ:Lcom/android/launcher3/Launcher;

.field protected final zB:I

.field private zC:I

.field protected zD:Lcom/android/launcher3/SearchDropTargetBar;

.field protected zE:Z

.field protected zF:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lss;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lss;->zF:I

    .line 54
    invoke-virtual {p0}, Lss;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    const v1, 0x7f0c001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lss;->zB:I

    .line 56
    const v1, 0x7f0d007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lss;->zC:I

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/launcher3/Launcher;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lss;->xZ:Lcom/android/launcher3/Launcher;

    .line 61
    return-void
.end method

.method public final a(Lcom/android/launcher3/SearchDropTargetBar;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lss;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    .line 69
    return-void
.end method

.method public a(Lui;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public a(Luq;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public a(Luq;)Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public b(Luq;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method protected final c(IIII)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 127
    iget-object v1, p0, Lss;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v1

    .line 130
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 131
    invoke-virtual {v1, p0, v2}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 133
    invoke-virtual {p0}, Lss;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    .line 140
    iget v0, v2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lss;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 141
    sub-int v1, v0, p3

    .line 147
    :goto_1
    iget v3, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lss;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v4, p4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 148
    add-int v4, v3, p4

    .line 150
    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 153
    sub-int v0, p1, p3

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 154
    sub-int v1, p2, p4

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    .line 155
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 157
    return-object v2

    .line 133
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lss;->getPaddingLeft()I

    move-result v1

    add-int/2addr v1, v0

    .line 144
    add-int v0, v1, p3

    goto :goto_1
.end method

.method public final c(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/widget/TextView;->getHitRect(Landroid/graphics/Rect;)V

    .line 115
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lss;->zC:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 118
    iget-object v1, p0, Lss;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;[I)F

    .line 119
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 120
    return-void
.end method

.method public c(Luq;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p1, Luq;->Gd:Luj;

    iget v1, p0, Lss;->zF:I

    invoke-virtual {v0, v1}, Luj;->setColor(I)V

    .line 90
    return-void
.end method

.method public final d(Luq;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public e(Luq;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p1, Luq;->Gd:Luj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Luj;->setColor(I)V

    .line 98
    return-void
.end method

.method protected final eI()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 72
    invoke-virtual {p0}, Lss;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 73
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 74
    aget-object v2, v1, v0

    if-eqz v2, :cond_0

    .line 75
    aget-object v0, v1, v0

    .line 78
    :goto_1
    return-object v0

    .line 73
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final eJ()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lss;->zE:Z

    return v0
.end method

.method public eK()V
    .locals 0

    .prologue
    .line 110
    return-void
.end method
