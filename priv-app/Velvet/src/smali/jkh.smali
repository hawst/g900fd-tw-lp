.class public final Ljkh;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private edm:I

.field private edn:I

.field private edo:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 93
    iput v0, p0, Ljkh;->aez:I

    iput v0, p0, Ljkh;->edm:I

    iput v0, p0, Ljkh;->edn:I

    iput v0, p0, Ljkh;->edo:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljkh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkh;->eCz:I

    .line 94
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 16
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkh;->edm:I

    iget v0, p0, Ljkh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkh;->edn:I

    iget v0, p0, Ljkh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkh;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkh;->edo:I

    iget v0, p0, Ljkh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkh;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 109
    iget v0, p0, Ljkh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    iget v1, p0, Ljkh;->edm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 112
    :cond_0
    iget v0, p0, Ljkh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 113
    const/4 v0, 0x2

    iget v1, p0, Ljkh;->edn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 115
    :cond_1
    iget v0, p0, Ljkh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 116
    const/4 v0, 0x3

    iget v1, p0, Ljkh;->edo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 118
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 119
    return-void
.end method

.method public final bil()Z
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Ljkh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHour()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Ljkh;->edm:I

    return v0
.end method

.method public final getMinute()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Ljkh;->edn:I

    return v0
.end method

.method public final getSecond()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Ljkh;->edo:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 123
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 124
    iget v1, p0, Ljkh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 125
    const/4 v1, 0x1

    iget v2, p0, Ljkh;->edm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_0
    iget v1, p0, Ljkh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 129
    const/4 v1, 0x2

    iget v2, p0, Ljkh;->edn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_1
    iget v1, p0, Ljkh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 133
    const/4 v1, 0x3

    iget v2, p0, Ljkh;->edo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_2
    return v0
.end method

.method public final qt(I)Ljkh;
    .locals 1

    .prologue
    .line 41
    iput p1, p0, Ljkh;->edm:I

    .line 42
    iget v0, p0, Ljkh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkh;->aez:I

    .line 43
    return-object p0
.end method

.method public final qu(I)Ljkh;
    .locals 1

    .prologue
    .line 60
    iput p1, p0, Ljkh;->edn:I

    .line 61
    iget v0, p0, Ljkh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkh;->aez:I

    .line 62
    return-object p0
.end method

.method public final qv(I)Ljkh;
    .locals 1

    .prologue
    .line 79
    iput p1, p0, Ljkh;->edo:I

    .line 80
    iget v0, p0, Ljkh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkh;->aez:I

    .line 81
    return-object p0
.end method
