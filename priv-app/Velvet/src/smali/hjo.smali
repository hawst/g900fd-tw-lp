.class public abstract Lhjo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final TAG:Ljava/lang/String;

.field protected ac:Z

.field protected final diL:Lesk;

.field private diM:Lelb;

.field protected final mCardController:Ledr;

.field protected mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;


# direct methods
.method public constructor <init>(Ledr;)V
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhjo;-><init>(Ledr;Legs;)V

    .line 47
    return-void
.end method

.method private constructor <init>(Ledr;Legs;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AbstractCardController."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhjo;->TAG:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjo;->ac:Z

    .line 52
    iput-object p1, p0, Lhjo;->mCardController:Ledr;

    .line 53
    new-instance v0, Lhjp;

    const-string v1, "Execute action"

    invoke-direct {v0, p0, v1}, Lhjp;-><init>(Lhjo;Ljava/lang/String;)V

    iput-object v0, p0, Lhjo;->diL:Lesk;

    .line 60
    return-void
.end method


# virtual methods
.method public K(IZ)V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1, p1, p2}, Ledr;->a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V

    .line 337
    return-void
.end method

.method public a(Lelb;)V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lhjo;->TAG:Ljava/lang/String;

    const-string v1, "#attach"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iput-object p1, p0, Lhjo;->diM:Lelb;

    .line 102
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->aga()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lhjo;->uE()V

    .line 105
    :cond_0
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lhjo;->TAG:Ljava/lang/String;

    const-string v1, "MatchingAppInfo is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 109
    :cond_1
    invoke-virtual {p0}, Lhjo;->ui()V

    .line 112
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lhjo;->aAU()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lhjo;->aQa()Lelb;

    move-result-object v0

    invoke-interface {v0}, Lelb;->ux()V

    :cond_2
    invoke-virtual {p0}, Lhjo;->anO()Z

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_3
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->z(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_0
.end method

.method public final aAU()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lhjo;->diM:Lelb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aG(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 323
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0, p1}, Ledr;->aG(Ljava/lang/Object;)V

    .line 324
    return-void
.end method

.method public final aLf()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    return-object v0
.end method

.method public final aPY()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->x(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhjo;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lhjo;->tP()V

    .line 79
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->GR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ledr;->eK(Z)V

    .line 84
    :cond_0
    return-void
.end method

.method protected final aPZ()Ledr;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    return-object v0
.end method

.method protected final aQa()Lelb;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lhjo;->diM:Lelb;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelb;

    return-object v0
.end method

.method public final aQb()V
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lhjo;->aQc()V

    .line 160
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->anJ()Z

    .line 161
    return-void
.end method

.method public final aQc()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lhjo;->anO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Ledr;->anN()V

    .line 166
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->D(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 168
    :cond_0
    return-void
.end method

.method public final aQd()V
    .locals 2

    .prologue
    .line 267
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhjo;->K(IZ)V

    .line 268
    return-void
.end method

.method public final aQe()V
    .locals 2

    .prologue
    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhjo;->ac:Z

    .line 272
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->A(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 273
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->o(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 275
    invoke-virtual {p0}, Lhjo;->onUserInteraction()V

    .line 276
    return-void
.end method

.method public final aQf()V
    .locals 3

    .prologue
    .line 366
    invoke-virtual {p0}, Lhjo;->onUserInteraction()V

    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 368
    invoke-virtual {p0}, Lhjo;->wr()Leoj;

    move-result-object v1

    invoke-static {}, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->aKr()Leol;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    .line 370
    return-void
.end method

.method public final anM()Lesm;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->anM()Lesm;

    move-result-object v0

    return-object v0
.end method

.method public anO()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->anO()Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 89
    return-void
.end method

.method public b(Ldbd;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 373
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-virtual {p0}, Lhjo;->tP()V

    .line 376
    :cond_0
    return-void
.end method

.method public final b(Lelb;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lhjo;->diM:Lelb;

    .line 148
    return-void
.end method

.method public final b(Lesj;)V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0, p1}, Ledr;->a(Lesj;)V

    .line 352
    return-void
.end method

.method public cN()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public final detach()V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lhjo;->anO()Z

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lhjo;->diM:Lelb;

    .line 143
    return-void
.end method

.method public gu(Z)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1, p1}, Ledr;->a(Lcom/google/android/search/shared/actions/VoiceAction;Z)V

    .line 229
    if-eqz p1, :cond_0

    .line 234
    iget-object v0, p0, Lhjo;->diM:Lelb;

    invoke-interface {v0}, Lelb;->auj()V

    .line 237
    :cond_0
    invoke-virtual {p0}, Lhjo;->anO()Z

    .line 238
    invoke-virtual {p0}, Lhjo;->onPreExecute()V

    .line 240
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lhjo;->K(IZ)V

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_1
    invoke-virtual {p0}, Lhjo;->tQ()V

    goto :goto_0
.end method

.method public m(Lcom/google/android/shared/util/App;)Z
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/shared/util/MatchingAppInfo;->l(Lcom/google/android/shared/util/App;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->p(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 307
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->anK()V

    .line 190
    return-void
.end method

.method public start()V
    .locals 0

    .prologue
    .line 71
    invoke-virtual {p0}, Lhjo;->aPY()V

    .line 72
    return-void
.end method

.method public tP()V
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lhjo;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lhjo;->ui()V

    .line 137
    :cond_0
    return-void
.end method

.method public tQ()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lhjo;->K(IZ)V

    .line 255
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->anQ()V

    .line 256
    invoke-virtual {p0}, Lhjo;->anO()Z

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lhjo;->K(IZ)V

    goto :goto_0
.end method

.method protected uE()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    iget-object v1, p0, Lhjo;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->C(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 121
    return-void
.end method

.method protected uF()I
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->uF()I

    move-result v0

    return v0
.end method

.method protected abstract ui()V
.end method

.method public uy()V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method public final wr()Leoj;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lhjo;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->wr()Leoj;

    move-result-object v0

    return-object v0
.end method
