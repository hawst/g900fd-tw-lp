.class public final Lcfk;
.super Lcyc;
.source "PG"


# instance fields
.field private final aUh:Lcxs;

.field private aUi:Landroid/preference/Preference;

.field private final mNowOptInSettings:Lcin;


# direct methods
.method public constructor <init>(Lcxs;Lcin;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 22
    iput-object p1, p0, Lcfk;->aUh:Lcxs;

    .line 23
    iput-object p2, p0, Lcfk;->mNowOptInSettings:Lcin;

    .line 24
    return-void
.end method

.method private Dz()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcfk;->aUi:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcfk;->aUi:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcfk;->aUh:Lcxs;

    invoke-virtual {v1}, Lcxs;->TH()Lcxi;

    move-result-object v1

    invoke-virtual {v1}, Lcxi;->TC()Z

    move-result v1

    .line 48
    if-eqz v1, :cond_1

    const v1, 0x7f0a0408

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 51
    :goto_0
    iget-object v1, p0, Lcfk;->aUi:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 53
    :cond_0
    return-void

    .line 48
    :cond_1
    const v1, 0x7f0a0407

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcfk;->aUi:Landroid/preference/Preference;

    .line 34
    invoke-direct {p0}, Lcfk;->Dz()V

    .line 35
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcfk;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcfk;->Dz()V

    .line 40
    return-void
.end method
