.class public final Laoj;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private ajw:Ljava/lang/String;

.field public ajx:Lani;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5306
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5307
    const/4 v0, 0x0

    iput v0, p0, Laoj;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laoj;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laoj;->ajw:Ljava/lang/String;

    iput-object v1, p0, Laoj;->ajx:Lani;

    iput-object v1, p0, Laoj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laoj;->eCz:I

    .line 5308
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5240
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laoj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoj;->afh:Ljava/lang/String;

    iget v0, p0, Laoj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoj;->ajw:Ljava/lang/String;

    iget v0, p0, Laoj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoj;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Laoj;->ajx:Lani;

    if-nez v0, :cond_1

    new-instance v0, Lani;

    invoke-direct {v0}, Lani;-><init>()V

    iput-object v0, p0, Laoj;->ajx:Lani;

    :cond_1
    iget-object v0, p0, Laoj;->ajx:Lani;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5323
    iget v0, p0, Laoj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5324
    const/4 v0, 0x2

    iget-object v1, p0, Laoj;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5326
    :cond_0
    iget v0, p0, Laoj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5327
    const/4 v0, 0x4

    iget-object v1, p0, Laoj;->ajw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5329
    :cond_1
    iget-object v0, p0, Laoj;->ajx:Lani;

    if-eqz v0, :cond_2

    .line 5330
    const/4 v0, 0x5

    iget-object v1, p0, Laoj;->ajx:Lani;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5332
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5333
    return-void
.end method

.method public final bS(Ljava/lang/String;)Laoj;
    .locals 1

    .prologue
    .line 5265
    if-nez p1, :cond_0

    .line 5266
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5268
    :cond_0
    iput-object p1, p0, Laoj;->afh:Ljava/lang/String;

    .line 5269
    iget v0, p0, Laoj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoj;->aez:I

    .line 5270
    return-object p0
.end method

.method public final bT(Ljava/lang/String;)Laoj;
    .locals 1

    .prologue
    .line 5287
    if-nez p1, :cond_0

    .line 5288
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5290
    :cond_0
    iput-object p1, p0, Laoj;->ajw:Ljava/lang/String;

    .line 5291
    iget v0, p0, Laoj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoj;->aez:I

    .line 5292
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5262
    iget-object v0, p0, Laoj;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5337
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5338
    iget v1, p0, Laoj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5339
    const/4 v1, 0x2

    iget-object v2, p0, Laoj;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5342
    :cond_0
    iget v1, p0, Laoj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5343
    const/4 v1, 0x4

    iget-object v2, p0, Laoj;->ajw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5346
    :cond_1
    iget-object v1, p0, Laoj;->ajx:Lani;

    if-eqz v1, :cond_2

    .line 5347
    const/4 v1, 0x5

    iget-object v2, p0, Laoj;->ajx:Lani;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5350
    :cond_2
    return v0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 5273
    iget v0, p0, Laoj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5284
    iget-object v0, p0, Laoj;->ajw:Ljava/lang/String;

    return-object v0
.end method

.method public final qI()Z
    .locals 1

    .prologue
    .line 5295
    iget v0, p0, Laoj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
