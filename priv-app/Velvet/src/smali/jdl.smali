.class public final Ljdl;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ecq:[Ljdl;


# instance fields
.field private aez:I

.field private ecr:Ljava/lang/String;

.field private ecs:I

.field private ect:I

.field private ecu:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Ljdl;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljdl;->ecr:Ljava/lang/String;

    iput v1, p0, Ljdl;->ecs:I

    const/4 v0, 0x1

    iput v0, p0, Ljdl;->ect:I

    iput v1, p0, Ljdl;->ecu:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljdl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdl;->eCz:I

    return-void
.end method

.method public static bhz()[Ljdl;
    .locals 2

    sget-object v0, Ljdl;->ecq:[Ljdl;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdl;->ecq:[Ljdl;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljdl;

    sput-object v0, Ljdl;->ecq:[Ljdl;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljdl;->ecq:[Ljdl;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdl;->ecr:Ljava/lang/String;

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdl;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdl;->ecs:I

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdl;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljdl;->ect:I

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdl;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdl;->ecu:I

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdl;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljdl;->ecr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljdl;->ecs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljdl;->ect:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_2
    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Ljdl;->ecu:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhA()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdl;->ecr:Ljava/lang/String;

    return-object v0
.end method

.method public final bhB()Z
    .locals 1

    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhC()Z
    .locals 1

    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhD()I
    .locals 1

    iget v0, p0, Ljdl;->ect:I

    return v0
.end method

.method public final bhE()Z
    .locals 1

    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getIcon()I
    .locals 1

    iget v0, p0, Ljdl;->ecs:I

    return v0
.end method

.method public final getValue()I
    .locals 1

    iget v0, p0, Ljdl;->ecu:I

    return v0
.end method

.method public final hasValue()Z
    .locals 1

    iget v0, p0, Ljdl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljdl;->ecr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljdl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljdl;->ecs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljdl;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljdl;->ect:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljdl;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Ljdl;->ecu:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    return v0
.end method

.method public final oP(I)Ljdl;
    .locals 1

    iput p1, p0, Ljdl;->ecs:I

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdl;->aez:I

    return-object p0
.end method

.method public final oQ(I)Ljdl;
    .locals 1

    const/4 v0, 0x4

    iput v0, p0, Ljdl;->ect:I

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdl;->aez:I

    return-object p0
.end method

.method public final oR(I)Ljdl;
    .locals 1

    iput p1, p0, Ljdl;->ecu:I

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdl;->aez:I

    return-object p0
.end method

.method public final tH(Ljava/lang/String;)Ljdl;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdl;->ecr:Ljava/lang/String;

    iget v0, p0, Ljdl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdl;->aez:I

    return-object p0
.end method
