.class public final Lcpd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static beC:Ljava/lang/String;


# instance fields
.field public acx:Ljava/lang/String;

.field final beD:Ljava/lang/Object;

.field beE:Lcpv;

.field private beF:I

.field private beG:I

.field private beH:Ljava/lang/String;

.field private beI:I

.field public beJ:Ljava/lang/String;

.field public final mBgExecutor:Ljava/util/concurrent/Executor;

.field final mClock:Lemp;

.field private final mGsaConfig:Lchk;

.field final mHttpHelper:Ldkx;

.field private final mLocationSettings:Lcob;

.field private final mLoginHelper:Lcrh;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lchk;Ldkx;Ljava/util/concurrent/Executor;Lemp;Lcke;Lcob;Lcrh;)V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcpd;->beD:Ljava/lang/Object;

    .line 157
    iput-object p1, p0, Lcpd;->mGsaConfig:Lchk;

    .line 158
    iput-object p2, p0, Lcpd;->mHttpHelper:Ldkx;

    .line 159
    iput-object p3, p0, Lcpd;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 160
    iput-object p4, p0, Lcpd;->mClock:Lemp;

    .line 161
    iput-object p5, p0, Lcpd;->mSearchSettings:Lcke;

    .line 162
    iput-object p6, p0, Lcpd;->mLocationSettings:Lcob;

    .line 163
    iput-object p7, p0, Lcpd;->mLoginHelper:Lcrh;

    .line 164
    new-instance v0, Lcpv;

    invoke-direct {v0}, Lcpv;-><init>()V

    iput-object v0, p0, Lcpd;->beE:Lcpv;

    .line 165
    return-void
.end method

.method public static O(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 604
    const-string v0, "contacts"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    const/4 v0, 0x1

    .line 619
    :goto_0
    return v0

    .line 606
    :cond_0
    const-string v0, "applications"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607
    const/4 v0, 0x2

    goto :goto_0

    .line 608
    :cond_1
    const-string v0, "com.android.chrome"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 609
    const/4 v0, 0x3

    goto :goto_0

    .line 610
    :cond_2
    const-string v0, "com.android.browser"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 611
    const/4 v0, 0x4

    goto :goto_0

    .line 612
    :cond_3
    const-string v0, "navsuggestion"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "now-promo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 614
    const/4 v0, 0x5

    goto :goto_0

    .line 617
    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static declared-synchronized Rj()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "TrulyRandom"
        }
    .end annotation

    .prologue
    .line 1058
    const-class v1, Lcpd;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lenu;->auQ()V

    .line 1059
    sget-object v0, Lcpd;->beC:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1064
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 1065
    const/16 v2, 0x8

    new-array v2, v2, [B

    .line 1066
    invoke-virtual {v0, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 1067
    const/16 v0, 0xb

    invoke-static {v2, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcpd;->beC:Ljava/lang/String;

    .line 1071
    :cond_0
    sget-object v0, Lcpd;->beC:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1058
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Lcom/google/android/shared/search/SuggestionLogInfo;
    .locals 3

    .prologue
    .line 416
    new-instance v0, Lijk;

    invoke-direct {v0}, Lijk;-><init>()V

    .line 418
    if-eqz p0, :cond_0

    .line 419
    invoke-virtual {v0, p0}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    .line 421
    :cond_0
    if-eqz p1, :cond_1

    .line 422
    invoke-virtual {v0, p1}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    .line 424
    :cond_1
    new-instance v1, Lcom/google/android/shared/search/SuggestionLogInfo;

    invoke-static {p0}, Lcpd;->o(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lcpd;->p(Ljava/util/List;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/shared/search/SuggestionLogInfo;-><init>(Ljava/lang/String;[B)V

    return-object v1
.end method

.method public static a(Lcom/google/android/shared/search/Suggestion;Ljsy;)V
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Ljsy;->sp(I)Ljsy;

    .line 526
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->acm()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 545
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Ljsy;->sq(I)Ljsy;

    .line 549
    :goto_0
    return-void

    .line 528
    :sswitch_0
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Ljsy;->sq(I)Ljsy;

    goto :goto_0

    .line 532
    :sswitch_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Ljsy;->sq(I)Ljsy;

    goto :goto_0

    .line 536
    :sswitch_2
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Ljsy;->sq(I)Ljsy;

    goto :goto_0

    .line 540
    :sswitch_3
    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Ljsy;->sq(I)Ljsy;

    goto :goto_0

    .line 526
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x19 -> :sswitch_1
        0x23 -> :sswitch_2
        0x53 -> :sswitch_3
    .end sparse-switch
.end method

.method public static b(Landroid/util/SparseArray;)Lcom/google/android/shared/search/SuggestionLogInfo;
    .locals 3

    .prologue
    .line 386
    new-instance v2, Lijk;

    invoke-direct {v2}, Lijk;-><init>()V

    .line 388
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 389
    const/4 v1, 0x0

    .line 390
    if-eqz v0, :cond_0

    .line 391
    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v0

    .line 392
    invoke-virtual {v2, v0}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    move-object v1, v0

    .line 395
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 397
    if-eqz v0, :cond_1

    .line 398
    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    .line 401
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 403
    if-eqz v0, :cond_2

    .line 404
    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lijk;->r(Ljava/lang/Iterable;)Lijk;

    .line 407
    :cond_2
    new-instance v0, Lcom/google/android/shared/search/SuggestionLogInfo;

    invoke-static {v1}, Lcpd;->o(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v2, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v2}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v2

    invoke-static {v2}, Lcpd;->p(Ljava/util/List;)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/shared/search/SuggestionLogInfo;-><init>(Ljava/lang/String;[B)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Lcpn;)Ldlb;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 741
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcpn;->cB(Z)Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RU()Lcpo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcpo;->if(Ljava/lang/String;)Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 745
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2, v3, v3}, Lcpn;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 747
    new-instance v2, Ldlb;

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v2
.end method

.method public static b(Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 2

    .prologue
    .line 294
    const/16 v0, 0x94

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 296
    invoke-static {p0}, Lcod;->a(Lcom/google/android/shared/search/SearchBoxStats;)Lith;

    move-result-object v1

    iput-object v1, v0, Litu;->dJd:Lith;

    .line 298
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 299
    return-void
.end method

.method public static hy(Ljava/lang/String;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 586
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 587
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 588
    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v0, v5, v4

    .line 589
    const-string v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 590
    array-length v8, v7

    const/4 v9, 0x2

    if-ge v8, v9, :cond_0

    .line 591
    const-string v1, "Velvet.SearchBoxLogging"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Incorrect summon encoding"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v0, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 599
    :goto_1
    return-object v0

    .line 594
    :cond_0
    aget-object v0, v7, v2

    .line 595
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 596
    aget-object v0, v7, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 597
    :goto_2
    new-instance v7, Landroid/util/Pair;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 596
    goto :goto_2

    :cond_2
    move-object v0, v3

    .line 599
    goto :goto_1
.end method

.method private static o(Ljava/util/List;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x1

    .line 429
    new-instance v8, Lcpm;

    const-string v0, "j"

    invoke-direct {v8, v0}, Lcpm;-><init>(Ljava/lang/String;)V

    .line 431
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 432
    const/4 v0, 0x0

    .line 433
    if-eqz p0, :cond_5

    .line 434
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v3, v1

    move v5, v6

    move v1, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 435
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->acm()I

    move-result v7

    .line 436
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asl()Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 441
    :goto_1
    if-ne v7, v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 436
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asl()Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_1

    .line 445
    :cond_1
    if-lez v1, :cond_4

    .line 446
    new-instance v10, Lcpm;

    const-string v0, "l"

    invoke-direct {v10, v0}, Lcpm;-><init>(Ljava/lang/String;)V

    .line 447
    new-instance v11, Lcpm;

    const-string v0, "i"

    invoke-direct {v11, v0}, Lcpm;-><init>(Ljava/lang/String;)V

    .line 448
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 449
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 450
    invoke-virtual {v11, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    goto :goto_2

    .line 452
    :cond_2
    invoke-virtual {v11}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 453
    if-le v1, v2, :cond_3

    .line 454
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 456
    :cond_3
    invoke-virtual {v10}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    :cond_4
    move v1, v2

    move-object v3, v4

    move v5, v7

    .line 462
    goto :goto_0

    :cond_5
    move-object v3, v1

    move v5, v6

    move v1, v0

    .line 464
    :cond_6
    if-eq v5, v6, :cond_9

    if-lez v1, :cond_9

    .line 465
    new-instance v4, Lcpm;

    const-string v0, "l"

    invoke-direct {v4, v0}, Lcpm;-><init>(Ljava/lang/String;)V

    .line 466
    new-instance v6, Lcpm;

    const-string v0, "i"

    invoke-direct {v6, v0}, Lcpm;-><init>(Ljava/lang/String;)V

    .line 467
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 468
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 469
    invoke-virtual {v6, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    goto :goto_3

    .line 471
    :cond_7
    invoke-virtual {v6}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 472
    if-le v1, v2, :cond_8

    .line 473
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 475
    :cond_8
    invoke-virtual {v4}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    .line 477
    :cond_9
    invoke-virtual {v8}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static p(Ljava/util/List;)[B
    .locals 14

    .prologue
    const/16 v13, 0xc

    const/4 v12, 0x2

    const/4 v2, 0x1

    .line 482
    new-instance v3, Lith;

    invoke-direct {v3}, Lith;-><init>()V

    .line 483
    if-nez p0, :cond_0

    .line 484
    invoke-static {v3}, Lith;->m(Ljsr;)[B

    move-result-object v0

    .line 519
    :goto_0
    return-object v0

    .line 486
    :cond_0
    new-instance v4, Ljsw;

    invoke-direct {v4}, Ljsw;-><init>()V

    .line 487
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 488
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 489
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 490
    new-instance v8, Ljsy;

    invoke-direct {v8}, Ljsy;-><init>()V

    .line 491
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asE()J

    move-result-wide v10

    long-to-int v1, v10

    invoke-virtual {v8, v1}, Ljsy;->sr(I)Ljsy;

    .line 492
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 493
    invoke-virtual {v8, v2}, Ljsy;->ja(Z)Ljsy;

    .line 495
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 496
    :cond_3
    new-instance v9, Liue;

    invoke-direct {v9}, Liue;-><init>()V

    .line 497
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    move v1, v2

    :goto_2
    invoke-static {v1}, Lifv;->gY(Z)V

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v8, v13}, Ljsy;->sp(I)Ljsy;

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->arX()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Liue;->pV(Ljava/lang/String;)Liue;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Liue;->pU(Ljava/lang/String;)Liue;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asm()Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v1, 0x14

    invoke-virtual {v8, v1}, Ljsy;->sq(I)Ljsy;

    .line 499
    :cond_5
    :goto_4
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 503
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asF()I

    move-result v1

    if-ltz v1, :cond_1

    .line 505
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asF()I

    move-result v0

    invoke-virtual {v3, v0}, Lith;->mr(I)Lith;

    goto/16 :goto_1

    .line 497
    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :cond_7
    const/16 v1, 0xd

    invoke-virtual {v8, v1}, Ljsy;->sp(I)Ljsy;

    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->arX()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->arW()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lcpd;->O(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-ne v1, v2, :cond_9

    invoke-virtual {v8, v13}, Ljsy;->sq(I)Ljsy;

    goto :goto_4

    :cond_9
    if-ne v1, v12, :cond_a

    const/16 v1, 0x13

    invoke-virtual {v8, v1}, Ljsy;->sq(I)Ljsy;

    goto :goto_4

    :cond_a
    const/4 v10, 0x3

    if-ne v1, v10, :cond_b

    invoke-virtual {v8, v12}, Ljsy;->sq(I)Ljsy;

    goto :goto_4

    :cond_b
    const/4 v10, -0x1

    if-eq v1, v10, :cond_5

    const/16 v1, 0x12

    invoke-virtual {v8, v1}, Ljsy;->sq(I)Ljsy;

    goto :goto_4

    .line 509
    :cond_c
    invoke-static {v0, v8}, Lcpd;->a(Lcom/google/android/shared/search/Suggestion;Ljsy;)V

    .line 510
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 513
    :cond_d
    iget-object v0, v4, Ljsw;->eCW:[Ljsy;

    invoke-static {v0, v5}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljsy;

    iput-object v0, v4, Ljsw;->eCW:[Ljsy;

    .line 514
    iget-object v0, v3, Lith;->dHE:[Liue;

    invoke-static {v0, v6}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liue;

    iput-object v0, v3, Lith;->dHE:[Liue;

    .line 518
    iput-object v4, v3, Lith;->dHD:Ljsw;

    .line 519
    invoke-static {v3}, Lith;->m(Ljsr;)[B

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final P(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1088
    iput-object p1, p0, Lcpd;->acx:Ljava/lang/String;

    .line 1089
    iput-object p2, p0, Lcpd;->beJ:Ljava/lang/String;

    .line 1090
    return-void
.end method

.method public final Rg()V
    .locals 2

    .prologue
    .line 168
    iget-object v1, p0, Lcpd;->beD:Ljava/lang/Object;

    monitor-enter v1

    .line 169
    :try_start_0
    new-instance v0, Lcpv;

    invoke-direct {v0}, Lcpv;-><init>()V

    iput-object v0, p0, Lcpd;->beE:Lcpv;

    .line 170
    iget v0, p0, Lcpd;->beF:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcpd;->beF:I

    .line 171
    const/4 v0, 0x1

    iput v0, p0, Lcpd;->beI:I

    .line 173
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Rh()V
    .locals 4

    .prologue
    .line 177
    iget-object v1, p0, Lcpd;->beD:Ljava/lang/Object;

    monitor-enter v1

    .line 178
    :try_start_0
    iget-object v0, p0, Lcpd;->beE:Lcpv;

    iget-object v2, p0, Lcpd;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcpv;->ae(J)V

    .line 179
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ri()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1039
    iget-object v1, p0, Lcpd;->beD:Ljava/lang/Object;

    monitor-enter v1

    .line 1040
    :try_start_0
    iget-object v0, p0, Lcpd;->beH:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcpd;->beG:I

    iget v2, p0, Lcpd;->beF:I

    if-eq v0, v2, :cond_1

    .line 1041
    :cond_0
    iget v0, p0, Lcpd;->beF:I

    iput v0, p0, Lcpd;->beG:I

    .line 1042
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1043
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1044
    invoke-static {}, Lcpd;->Rj()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045
    iget v2, p0, Lcpd;->beG:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1046
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpd;->beH:Ljava/lang/String;

    .line 1048
    :cond_1
    iget-object v0, p0, Lcpd;->beH:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1049
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Rk()I
    .locals 3

    .prologue
    .line 1078
    iget-object v1, p0, Lcpd;->beD:Ljava/lang/Object;

    monitor-enter v1

    .line 1079
    :try_start_0
    iget v0, p0, Lcpd;->beI:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcpd;->beI:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 1080
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;I)Lcom/google/android/shared/search/Query;
    .locals 8

    .prologue
    .line 254
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->arU()Lehj;

    move-result-object v1

    iget-object v0, p0, Lcpd;->beE:Lcpv;

    iget v2, v0, Lcpv;->bfN:I

    iget-object v0, p0, Lcpd;->beE:Lcpv;

    iget v3, v0, Lcpv;->bfO:I

    iget-object v0, p0, Lcpd;->beE:Lcpv;

    iget v4, v0, Lcpv;->bfP:I

    iget-object v0, p0, Lcpd;->beE:Lcpv;

    iget v5, v0, Lcpv;->bfQ:I

    iget-object v0, p0, Lcpd;->beE:Lcpv;

    iget-wide v6, v0, Lcpv;->bfK:J

    invoke-virtual/range {v1 .. v7}, Lehj;->a(IIIIJ)Lehj;

    move-result-object v0

    iput p2, v0, Lehj;->bZC:I

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    .line 265
    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object p1

    .line 266
    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcpd;->acx:Ljava/lang/String;

    .line 267
    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->getSource()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpd;->beJ:Ljava/lang/String;

    .line 269
    :cond_0
    return-object p1
.end method

.method public final a(Ldex;)Ldex;
    .locals 1

    .prologue
    .line 236
    new-instance v0, Lcpf;

    invoke-direct {v0, p0, p1}, Lcpf;-><init>(Lcpd;Ldex;)V

    return-object v0
.end method

.method public final a(ILjyx;Ljava/lang/String;Ljava/lang/String;Lcpn;JI)V
    .locals 8
    .param p2    # Ljyx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcpn;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 660
    if-nez p5, :cond_1

    .line 661
    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log, urlHelper==null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 665
    :cond_1
    and-int/lit16 v0, p1, 0x1000

    if-eqz v0, :cond_2

    .line 666
    if-eqz p3, :cond_b

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-wide v4, p6

    move/from16 v6, p8

    .line 667
    invoke-virtual/range {v0 .. v6}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;JI)V

    .line 668
    and-int/lit16 p1, p1, -0x1001

    .line 673
    :cond_2
    :goto_1
    if-eqz p1, :cond_0

    .line 674
    if-eqz p2, :cond_c

    .line 675
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    .line 676
    invoke-virtual {p2}, Ljyx;->bwE()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 678
    :cond_3
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_4

    .line 679
    invoke-virtual {p2}, Ljyx;->bwG()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 681
    :cond_4
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    .line 682
    invoke-virtual {p2}, Ljyx;->bwF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 684
    :cond_5
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_6

    .line 685
    invoke-virtual {p2}, Ljyx;->bwH()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 687
    :cond_6
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_7

    .line 688
    invoke-virtual {p2}, Ljyx;->bwK()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 690
    :cond_7
    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_8

    .line 691
    invoke-virtual {p2}, Ljyx;->bwI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 693
    :cond_8
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_9

    .line 694
    invoke-virtual {p2}, Ljyx;->bwJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 696
    :cond_9
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_a

    .line 697
    invoke-virtual {p2}, Ljyx;->bwL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    .line 699
    :cond_a
    and-int/lit16 v0, p1, 0x100

    if-eqz v0, :cond_0

    .line 700
    invoke-virtual {p2}, Ljyx;->bwM()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4, p5}, Lcpd;->a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V

    goto :goto_0

    .line 670
    :cond_b
    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log card above SRP because event id is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 703
    :cond_c
    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log to GWS: all URLs missing."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcpo;)V
    .locals 11

    .prologue
    .line 281
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v4

    .line 282
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283
    if-eqz v4, :cond_0

    .line 284
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arG()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcpo;->hW(Ljava/lang/String;)Lcpo;

    .line 285
    invoke-static {v4}, Lcpd;->b(Lcom/google/android/shared/search/SearchBoxStats;)V

    .line 287
    :cond_0
    new-instance v5, Lcpm;

    const-string v0, "."

    invoke-direct {v5, v0}, Lcpm;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcpd;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->IT()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    const-string v0, ""

    const-string v2, ""

    const/4 v1, 0x0

    if-eqz v4, :cond_1e

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arB()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arF()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arE()Lcom/google/android/shared/search/SuggestionLogInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/SuggestionLogInfo;->asJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arn()I

    move-result v1

    move v10, v1

    move-object v1, v2

    move-object v2, v0

    move v0, v10

    :goto_0
    new-instance v6, Lcpl;

    const-string v3, "j"

    invoke-direct {v6, v3}, Lcpl;-><init>(Ljava/lang/String;)V

    if-nez v4, :cond_3

    const-string v3, ""

    :goto_1
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_4

    const-string v3, ""

    :goto_2
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_5

    const-string v3, ""

    :goto_3
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_6

    const-string v3, ""

    :goto_4
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_7

    const-string v3, ""

    :goto_5
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_8

    const-string v3, ""

    :goto_6
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_9

    const-string v3, ""

    :goto_7
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_a

    const-string v3, ""

    :goto_8
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_b

    const-string v3, ""

    :goto_9
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-eqz v4, :cond_c

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arC()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arv()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :goto_a
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    iget-object v3, p0, Lcpd;->mSearchSettings:Lcke;

    invoke-interface {v3}, Lcke;->NE()Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "1"

    :goto_b
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    iget-object v3, p0, Lcpd;->mLocationSettings:Lcob;

    invoke-interface {v3}, Lcob;->QM()Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "0"

    :goto_c
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    iget-object v3, p0, Lcpd;->mLocationSettings:Lcob;

    invoke-interface {v3}, Lcob;->QN()Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v3, "0"

    :goto_d
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_10

    const-string v3, ""

    :goto_e
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_11

    const-string v3, ""

    :goto_f
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_12

    const-string v3, ""

    :goto_10
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_13

    const-string v3, ""

    :goto_11
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_14

    const-string v3, ""

    :goto_12
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_15

    const-string v3, ""

    :goto_13
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    iget-object v3, p0, Lcpd;->mLocationSettings:Lcob;

    invoke-interface {v3}, Lcob;->QR()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    iget-object v3, p0, Lcpd;->mLoginHelper:Lcrh;

    invoke-virtual {v3}, Lcrh;->Su()Z

    move-result v3

    if-eqz v3, :cond_16

    const-string v3, "1"

    :goto_14
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    iget-object v3, p0, Lcpd;->mLoginHelper:Lcrh;

    invoke-virtual {v3}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v3

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_17

    const-string v3, ""

    :goto_15
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_19

    const-string v3, ""

    :goto_16
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_1a

    const-string v3, ""

    :goto_17
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_1b

    const-string v3, ""

    :goto_18
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_1c

    const-string v3, ""

    :goto_19
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    if-nez v4, :cond_1d

    const-string v3, ""

    :goto_1a
    invoke-virtual {v6, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    invoke-virtual {v6}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcpm;->ax(Ljava/lang/Object;)Lcpm;

    invoke-virtual {v5}, Lcpm;->Rl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcpo;->hN(Ljava/lang/String;)Lcpo;

    .line 289
    :cond_2
    iget-object v0, p0, Lcpd;->beJ:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcpo;->ie(Ljava/lang/String;)Lcpo;

    .line 290
    iget-object v0, p0, Lcpd;->acx:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcpo;->id(Ljava/lang/String;)Lcpo;

    .line 291
    return-void

    .line 287
    :cond_3
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arw()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->art()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->aru()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_3

    :cond_6
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arJ()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_4

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arL()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_5

    :cond_8
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arK()I

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arL()I

    move-result v7

    sub-int/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_6

    :cond_9
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arM()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_7

    :cond_a
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arN()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_8

    :cond_b
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->ary()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_9

    :cond_c
    const-string v3, ""

    goto/16 :goto_a

    :cond_d
    const-string v3, "0"

    goto/16 :goto_b

    :cond_e
    const-string v3, "1"

    goto/16 :goto_c

    :cond_f
    const-string v3, "1"

    goto/16 :goto_d

    :cond_10
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arq()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_e

    :cond_11
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arr()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_f

    :cond_12
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->ars()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_10

    :cond_13
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arz()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_11

    :cond_14
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arA()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_12

    :cond_15
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->aro()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_13

    :cond_16
    const-string v3, "0"

    goto/16 :goto_14

    :cond_17
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arO()Z

    move-result v3

    if-eqz v3, :cond_18

    const-string v3, "1"

    goto/16 :goto_15

    :cond_18
    const-string v3, "0"

    goto/16 :goto_15

    :cond_19
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arP()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_16

    :cond_1a
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arQ()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_17

    :cond_1b
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arR()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_18

    :cond_1c
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arS()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_19

    :cond_1d
    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->arT()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_1a

    :cond_1e
    move v10, v1

    move-object v1, v2

    move-object v2, v0

    move v0, v10

    goto/16 :goto_0
.end method

.method public final a(Lcpn;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 948
    iget-object v7, p0, Lcpd;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcpj;

    const-string v2, "logUrlQuery"

    const/4 v1, 0x2

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcpj;-><init>(Lcpd;Ljava/lang/String;[ILcpn;Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 964
    return-void

    .line 948
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcpn;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 717
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log event to GWS because URL is null/empty"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 735
    :goto_0
    return-void

    .line 721
    :cond_0
    iget-object v7, p0, Lcpd;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcph;

    const-string v2, "logEventToGws"

    const/4 v1, 0x2

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcph;-><init>(Lcpd;Ljava/lang/String;[ILjava/lang/String;Ljava/lang/String;Lcpn;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcpn;JI)V
    .locals 12

    .prologue
    .line 761
    iget-object v0, p0, Lcpd;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcpi;

    const-string v3, "Log card above SRP"

    const/4 v2, 0x2

    new-array v4, v2, [I

    fill-array-data v4, :array_0

    move-object v2, p0

    move-object v5, p3

    move-object v6, p1

    move-object v7, p2

    move-wide/from16 v8, p4

    move/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcpi;-><init>(Lcpd;Ljava/lang/String;[ILcpn;Ljava/lang/String;Ljava/lang/String;JI)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 780
    return-void

    .line 761
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final i(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 183
    iget-object v1, p0, Lcpd;->beD:Ljava/lang/Object;

    monitor-enter v1

    .line 184
    :try_start_0
    iget-object v0, p0, Lcpd;->beE:Lcpv;

    invoke-virtual {v0, p1}, Lcpv;->r(Lcom/google/android/shared/search/Query;)V

    .line 185
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
