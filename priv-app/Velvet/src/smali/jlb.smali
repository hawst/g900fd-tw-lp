.class public final Ljlb;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ere:[Ljlb;


# instance fields
.field private aez:I

.field private dMI:Ljava/lang/String;

.field private dYT:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2320
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2321
    const/4 v0, 0x0

    iput v0, p0, Ljlb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlb;->dYT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlb;->dMI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljlb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlb;->eCz:I

    .line 2322
    return-void
.end method

.method public static boJ()[Ljlb;
    .locals 2

    .prologue
    .line 2263
    sget-object v0, Ljlb;->ere:[Ljlb;

    if-nez v0, :cond_1

    .line 2264
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2266
    :try_start_0
    sget-object v0, Ljlb;->ere:[Ljlb;

    if-nez v0, :cond_0

    .line 2267
    const/4 v0, 0x0

    new-array v0, v0, [Ljlb;

    sput-object v0, Ljlb;->ere:[Ljlb;

    .line 2269
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2271
    :cond_1
    sget-object v0, Ljlb;->ere:[Ljlb;

    return-object v0

    .line 2269
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2257
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlb;->dYT:Ljava/lang/String;

    iget v0, p0, Ljlb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlb;->dMI:Ljava/lang/String;

    iget v0, p0, Ljlb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2336
    iget v0, p0, Ljlb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2337
    const/4 v0, 0x1

    iget-object v1, p0, Ljlb;->dYT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2339
    :cond_0
    iget v0, p0, Ljlb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2340
    const/4 v0, 0x2

    iget-object v1, p0, Ljlb;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2342
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2343
    return-void
.end method

.method public final boK()Ljlb;
    .locals 1

    .prologue
    .line 2315
    const-string v0, ""

    iput-object v0, p0, Ljlb;->dMI:Ljava/lang/String;

    .line 2316
    iget v0, p0, Ljlb;->aez:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Ljlb;->aez:I

    .line 2317
    return-object p0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2301
    iget-object v0, p0, Ljlb;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2347
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2348
    iget v1, p0, Ljlb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2349
    const/4 v1, 0x1

    iget-object v2, p0, Ljlb;->dYT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2352
    :cond_0
    iget v1, p0, Ljlb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2353
    const/4 v1, 0x2

    iget-object v2, p0, Ljlb;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2356
    :cond_1
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 2312
    iget v0, p0, Ljlb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wA(Ljava/lang/String;)Ljlb;
    .locals 1

    .prologue
    .line 2304
    if-nez p1, :cond_0

    .line 2305
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2307
    :cond_0
    iput-object p1, p0, Ljlb;->dMI:Ljava/lang/String;

    .line 2308
    iget v0, p0, Ljlb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlb;->aez:I

    .line 2309
    return-object p0
.end method
