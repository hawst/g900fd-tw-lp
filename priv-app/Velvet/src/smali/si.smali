.class public final Lsi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Labh;


# static fields
.field private static final ze:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final zf:Landroid/appwidget/AppWidgetHost;

.field private final zg:Lsn;

.field private final zh:Landroid/content/ContentValues;

.field private final zi:Landroid/content/res/Resources;

.field private final zj:I

.field private zk:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/16 v0, -0x65

    invoke-static {v0}, Labn;->bp(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsi;->ze:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetHost;Lsn;Landroid/content/res/Resources;I)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p1, p0, Lsi;->mContext:Landroid/content/Context;

    .line 130
    iput-object p2, p0, Lsi;->zf:Landroid/appwidget/AppWidgetHost;

    .line 131
    iput-object p3, p0, Lsi;->zg:Lsn;

    .line 133
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lsi;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 134
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lsi;->zh:Landroid/content/ContentValues;

    .line 136
    iput-object p4, p0, Lsi;->zi:Landroid/content/res/Resources;

    .line 137
    iput p5, p0, Lsi;->zj:I

    .line 138
    return-void
.end method

.method static synthetic a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 54
    const/4 v1, 0x0

    const-string v0, "http://schemas.android.com/apk/res-auto/com.android.launcher3"

    invoke-interface {p0, v0, p1, v1}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p0, v0, p1, v1}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :cond_0
    return v0
.end method

.method static synthetic a(Lsi;)Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsi;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method private static a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 532
    const-string v0, "http://schemas.android.com/apk/res-auto/com.android.launcher3"

    invoke-interface {p0, v0, p1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 534
    if-nez v0, :cond_0

    .line 535
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 537
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/appwidget/AppWidgetHost;Lsn;)Lsi;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 66
    const-string v0, "android.autoinstalls.config.action.PLAY_AUTO_INSTALL"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v0, v2}, Ladp;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Landroid/util/Pair;

    move-result-object v2

    .line 68
    if-nez v2, :cond_0

    move-object v0, v1

    .line 79
    :goto_0
    return-object v0

    .line 72
    :cond_0
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 73
    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Landroid/content/res/Resources;

    .line 74
    const-string v2, "default_layout"

    const-string v3, "xml"

    invoke-virtual {v4, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 75
    if-nez v5, :cond_1

    .line 76
    const-string v2, "AutoInstalls"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Layout definition not found in package: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 77
    goto :goto_0

    .line 79
    :cond_1
    new-instance v0, Lsi;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lsi;-><init>(Landroid/content/Context;Landroid/appwidget/AppWidgetHost;Lsn;Landroid/content/res/Resources;I)V

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method static synthetic b(Lsi;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsi;->zh:Landroid/content/ContentValues;

    return-object v0
.end method

.method static synthetic b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-static {p0, p1}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lsi;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsi;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lsi;)Landroid/appwidget/AppWidgetHost;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsi;->zf:Landroid/appwidget/AppWidgetHost;

    return-object v0
.end method

.method static synthetic e(Lsi;)Lsn;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsi;->zg:Lsn;

    return-object v0
.end method

.method static synthetic f(Lsi;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsi;->zk:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)I
    .locals 18

    .prologue
    .line 142
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lsi;->zk:Landroid/database/sqlite/SQLiteDatabase;

    .line 144
    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lsi;->zi:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget v2, v0, Lsi;->zj:I

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v3

    iget-object v3, v3, Lyu;->Mk:Lur;

    invoke-virtual {v3}, Lur;->gl()Ltu;

    move-result-object v3

    iget v9, v3, Ltu;->DY:I

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v10

    const-string v2, "workspace"

    :cond_0
    invoke-interface {v10}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    :cond_1
    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v3, "No start tag found"

    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 145
    :catch_0
    move-exception v2

    .line 146
    :goto_0
    const-string v3, "AutoInstalls"

    const-string v4, "Got exception parsing layout."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 147
    const/4 v4, -0x1

    :cond_2
    return v4

    .line 144
    :cond_3
    :try_start_1
    invoke-interface {v10}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected start tag: found "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v10}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", expected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 145
    :catch_1
    move-exception v2

    goto :goto_0

    .line 144
    :cond_4
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v11

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    const-string v2, "appicon"

    new-instance v3, Lsj;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lsj;-><init>(Lsi;B)V

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "autoinstall"

    new-instance v3, Lsl;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lsl;-><init>(Lsi;B)V

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "folder"

    new-instance v3, Lsm;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lsm;-><init>(Lsi;B)V

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "appwidget"

    new-instance v3, Lsk;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lsk;-><init>(Lsi;B)V

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "shortcut"

    new-instance v3, Lso;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lso;-><init>(Lsi;B)V

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x0

    :cond_5
    :goto_1
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v3

    if-le v3, v11, :cond_2

    :cond_6
    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lsi;->zh:Landroid/content/ContentValues;

    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    sget-object v2, Lsi;->ze:Ljava/lang/String;

    const-string v3, "container"

    invoke-static {v10, v3}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v5, -0x65

    const-string v2, "rank"

    invoke-static {v10, v2}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    int-to-long v6, v9

    cmp-long v6, v2, v6

    if-gez v6, :cond_7

    :goto_2
    move-wide v6, v2

    move v3, v5

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v5, "container"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v2, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v5, "screen"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v2, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsp;

    if-nez v2, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring unknown element tag: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 145
    :catch_2
    move-exception v2

    goto/16 :goto_0

    .line 144
    :cond_7
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    goto :goto_2

    :cond_8
    const/16 v5, -0x64

    const-string v2, "screen"

    invoke-static {v10, v2}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v7, "cellX"

    const-string v13, "x"

    invoke-static {v10, v13}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v7, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v7, "cellY"

    const-string v13, "y"

    invoke-static {v10, v13}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v7, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v6, v2

    move v3, v5

    goto :goto_3

    :cond_9
    invoke-interface {v2, v10, v8}, Lsp;->a(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;)J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v2, v14, v16

    if-ltz v2, :cond_b

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const/16 v2, -0x64

    if-ne v3, v2, :cond_a

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_a
    add-int/lit8 v2, v4, 0x1

    :goto_4
    move v4, v2

    goto/16 :goto_1

    :cond_b
    move v2, v4

    goto :goto_4
.end method

.method protected final a(Ljava/lang/String;Landroid/content/Intent;I)J
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 210
    iget-object v0, p0, Lsi;->zg:Lsn;

    invoke-interface {v0}, Lsn;->eE()J

    move-result-wide v0

    .line 211
    iget-object v2, p0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v3, "intent"

    const/4 v4, 0x0

    invoke-virtual {p2, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v2, p0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v3, "title"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v2, p0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v3, "itemType"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    iget-object v2, p0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v3, "spanX"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215
    iget-object v2, p0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v3, "spanY"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    iget-object v2, p0, Lsi;->zh:Landroid/content/ContentValues;

    const-string v3, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 217
    iget-object v2, p0, Lsi;->zg:Lsn;

    iget-object v3, p0, Lsi;->zk:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v4, p0, Lsi;->zh:Landroid/content/ContentValues;

    invoke-interface {v2, v3, v4}, Lsn;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 218
    const-wide/16 v0, -0x1

    .line 220
    :cond_0
    return-wide v0
.end method

.method protected final eD()Ljava/util/HashMap;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 225
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 226
    const-string v1, "appicon"

    new-instance v2, Lsj;

    invoke-direct {v2, p0, v3}, Lsj;-><init>(Lsi;B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const-string v1, "autoinstall"

    new-instance v2, Lsl;

    invoke-direct {v2, p0, v3}, Lsl;-><init>(Lsi;B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    const-string v1, "shortcut"

    new-instance v2, Lso;

    invoke-direct {v2, p0, v3}, Lso;-><init>(Lsi;B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    return-object v0
.end method
