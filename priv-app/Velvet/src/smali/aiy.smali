.class final Laiy;
.super Lajm;
.source "PG"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 858
    invoke-direct {p0}, Lajm;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 858
    invoke-direct {p0}, Laiy;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;[I[II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 888
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    .line 891
    if-lez v0, :cond_0

    .line 893
    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 894
    const/16 v2, -0x35

    const/16 v3, 0x35

    invoke-static {v1, v2, v3, v4}, Laiy;->a(Ljava/lang/String;IIZ)I

    move-result v1

    .line 895
    aput v1, p2, p3

    .line 896
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 901
    :goto_0
    invoke-static {}, Laiw;->me()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 902
    if-nez v0, :cond_1

    .line 903
    new-instance v0, Laix;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid BYDAY value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laix;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p0

    .line 899
    goto :goto_0

    .line 905
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, p1, p3

    .line 906
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Laiw;)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 864
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    .line 867
    new-array v2, v0, [I

    .line 868
    new-array v1, v0, [I

    .line 869
    invoke-static {p1, v2, v1, v3}, Laiy;->a(Ljava/lang/String;[I[II)V

    .line 880
    :cond_0
    iput-object v2, p2, Laiw;->aae:[I

    .line 881
    iput-object v1, p2, Laiw;->aaf:[I

    .line 882
    iput v0, p2, Laiw;->aag:I

    .line 883
    const/16 v0, 0x80

    return v0

    .line 871
    :cond_1
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 872
    array-length v0, v4

    .line 874
    new-array v2, v0, [I

    .line 875
    new-array v1, v0, [I

    .line 876
    :goto_0
    if-ge v3, v0, :cond_0

    .line 877
    aget-object v5, v4, v3

    invoke-static {v5, v2, v1, v3}, Laiy;->a(Ljava/lang/String;[I[II)V

    .line 876
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
