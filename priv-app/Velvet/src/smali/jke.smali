.class public final Ljke;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aix:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private epS:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1101
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1102
    const/4 v0, 0x0

    iput v0, p0, Ljke;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljke;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljke;->aix:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljke;->ajk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljke;->epS:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljke;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljke;->eCz:I

    .line 1103
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 994
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljke;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljke;->afh:Ljava/lang/String;

    iget v0, p0, Ljke;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljke;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljke;->aix:Ljava/lang/String;

    iget v0, p0, Ljke;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljke;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljke;->ajk:Ljava/lang/String;

    iget v0, p0, Ljke;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljke;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljke;->epS:Ljava/lang/String;

    iget v0, p0, Ljke;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljke;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1119
    iget v0, p0, Ljke;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1120
    const/4 v0, 0x1

    iget-object v1, p0, Ljke;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1122
    :cond_0
    iget v0, p0, Ljke;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1123
    const/4 v0, 0x2

    iget-object v1, p0, Ljke;->aix:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1125
    :cond_1
    iget v0, p0, Ljke;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1126
    const/4 v0, 0x3

    iget-object v1, p0, Ljke;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1128
    :cond_2
    iget v0, p0, Ljke;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1129
    const/4 v0, 0x4

    iget-object v1, p0, Ljke;->epS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1131
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1132
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1136
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1137
    iget v1, p0, Ljke;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1138
    const/4 v1, 0x1

    iget-object v2, p0, Ljke;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1141
    :cond_0
    iget v1, p0, Ljke;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1142
    const/4 v1, 0x2

    iget-object v2, p0, Ljke;->aix:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1145
    :cond_1
    iget v1, p0, Ljke;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1146
    const/4 v1, 0x3

    iget-object v2, p0, Ljke;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1149
    :cond_2
    iget v1, p0, Ljke;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1150
    const/4 v1, 0x4

    iget-object v2, p0, Ljke;->epS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1153
    :cond_3
    return v0
.end method
