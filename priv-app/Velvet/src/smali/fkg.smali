.class final Lfkg;
.super Lelg;
.source "PG"


# instance fields
.field private synthetic ctS:Landroid/view/LayoutInflater;


# direct methods
.method constructor <init>(Lfkf;Landroid/content/Context;I[Lelh;Lelh;Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 116
    iput-object p6, p0, Lfkg;->ctS:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, p3, p4}, Lelg;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lfkg;->ctS:Landroid/view/LayoutInflater;

    const v1, 0x7f04006f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 138
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lfkg;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lelh;

    iget-object v2, v2, Lelh;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    return-object v1
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 121
    .line 122
    if-nez p2, :cond_1

    .line 123
    iget-object v0, p0, Lfkg;->ctS:Landroid/view/LayoutInflater;

    const v1, 0x7f0400ac

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 124
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b00b0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    move-object v0, v1

    .line 125
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    array-length v5, v4

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v2, v4, v0

    .line 126
    if-eqz v2, :cond_0

    .line 127
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 125
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v1, p2

    .line 132
    :cond_2
    invoke-super {p0, p1, v1, p3}, Lelg;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
