.class public final enum Ldtj;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum bKt:Ldtj;

.field public static final enum bKu:Ldtj;

.field public static final enum bKv:Ldtj;

.field public static final enum bKw:Ldtj;

.field public static final enum bKx:Ldtj;

.field private static final synthetic bKy:[Ldtj;


# instance fields
.field private kO:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 406
    new-instance v0, Ldtj;

    const-string v1, "NOT"

    const-string v2, "unknown commit type or not committed yet"

    invoke-direct {v0, v1, v3, v2}, Ldtj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldtj;->bKt:Ldtj;

    .line 409
    new-instance v0, Ldtj;

    const-string v1, "SUGGESTION"

    const-string v2, "suggestion"

    invoke-direct {v0, v1, v4, v2}, Ldtj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldtj;->bKu:Ldtj;

    .line 412
    new-instance v0, Ldtj;

    const-string v1, "SEPARATOR"

    const-string v2, "separator"

    invoke-direct {v0, v1, v5, v2}, Ldtj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldtj;->bKv:Ldtj;

    .line 415
    new-instance v0, Ldtj;

    const-string v1, "UNFOCUSED"

    const-string v2, "removed focus"

    invoke-direct {v0, v1, v6, v2}, Ldtj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldtj;->bKw:Ldtj;

    .line 418
    new-instance v0, Ldtj;

    const-string v1, "UNSURE"

    const-string v2, "(no-op) suggestion clicked or started another gesture"

    invoke-direct {v0, v1, v7, v2}, Ldtj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldtj;->bKx:Ldtj;

    .line 404
    const/4 v0, 0x5

    new-array v0, v0, [Ldtj;

    sget-object v1, Ldtj;->bKt:Ldtj;

    aput-object v1, v0, v3

    sget-object v1, Ldtj;->bKu:Ldtj;

    aput-object v1, v0, v4

    sget-object v1, Ldtj;->bKv:Ldtj;

    aput-object v1, v0, v5

    sget-object v1, Ldtj;->bKw:Ldtj;

    aput-object v1, v0, v6

    sget-object v1, Ldtj;->bKx:Ldtj;

    aput-object v1, v0, v7

    sput-object v0, Ldtj;->bKy:[Ldtj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 423
    iput-object p3, p0, Ldtj;->kO:Ljava/lang/String;

    .line 424
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldtj;
    .locals 1

    .prologue
    .line 404
    const-class v0, Ldtj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldtj;

    return-object v0
.end method

.method public static values()[Ldtj;
    .locals 1

    .prologue
    .line 404
    sget-object v0, Ldtj;->bKy:[Ldtj;

    invoke-virtual {v0}, [Ldtj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldtj;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Ldtj;->kO:Ljava/lang/String;

    return-object v0
.end method
