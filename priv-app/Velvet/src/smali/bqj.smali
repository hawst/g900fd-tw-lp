.class public abstract Lbqj;
.super Landroid/os/Binder;

# interfaces
.implements Lbqi;


# direct methods
.method public static x(Landroid/os/IBinder;)Lbqi;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lbqi;

    if-eqz v1, :cond_1

    check-cast v0, Lbqi;

    goto :goto_0

    :cond_1
    new-instance v0, Lbqk;

    invoke-direct {v0, p0}, Lbqk;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const-string v0, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v2, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->CREATOR:Lbvm;

    invoke-static {p2}, Lbvm;->ai(Landroid/os/Parcel;)Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;

    move-result-object v0

    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbqg;->w(Landroid/os/IBinder;)Lbqf;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbqj;->a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;Lbqf;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string v2, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->CREATOR:Lbvq;

    invoke-static {p2}, Lbvq;->am(Landroid/os/Parcel;)Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;

    move-result-object v0

    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbqg;->w(Landroid/os/IBinder;)Lbqf;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbqj;->a(Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;Lbqf;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_3
    const-string v2, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;->CREATOR:Lbvi;

    invoke-static {p2}, Lbvi;->ag(Landroid/os/Parcel;)Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;

    move-result-object v0

    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbqg;->w(Landroid/os/IBinder;)Lbqf;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbqj;->a(Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;Lbqf;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_4
    const-string v2, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;->CREATOR:Lbvo;

    invoke-static {p2}, Lbvo;->ak(Landroid/os/Parcel;)Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;

    move-result-object v0

    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbqg;->w(Landroid/os/IBinder;)Lbqf;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbqj;->a(Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;Lbqf;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto/16 :goto_0

    :sswitch_5
    const-string v2, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Request;->CREATOR:Lbvs;

    invoke-static {p2}, Lbvs;->ao(Landroid/os/Parcel;)Lcom/google/android/gms/search/global/SetSignedInAccountCall$Request;

    move-result-object v0

    :cond_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbqg;->w(Landroid/os/IBinder;)Lbqf;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbqj;->a(Lcom/google/android/gms/search/global/SetSignedInAccountCall$Request;Lbqf;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
