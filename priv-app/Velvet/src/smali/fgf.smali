.class final Lfgf;
.super Lfgd;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0}, Lfgd;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Lamx;Lffx;Lemp;)V
    .locals 0

    .prologue
    .line 323
    invoke-virtual {p2, p1, p3}, Lffx;->a(Lamx;Lemp;)V

    .line 324
    return-void
.end method

.method final a(Lamx;Ljava/util/List;Ligi;)V
    .locals 12

    .prologue
    .line 370
    new-instance v0, Lbri;

    invoke-direct {v0}, Lbri;-><init>()V

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lbri;->L(J)Lbri;

    move-result-object v1

    const v0, 0xea60

    iput v0, v1, Lbri;->aGa:I

    const v0, 0x493e0

    iput v0, v1, Lbri;->aFZ:I

    const/4 v0, 0x6

    iput v0, v1, Lbri;->aFY:I

    .line 376
    iget-object v0, p1, Lamx;->entry:Lizj;

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    iget-object v0, v0, Ljcg;->dRY:Ljie;

    iget-object v8, v0, Ljie;->eaW:[Ljbp;

    array-length v9, v8

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_2

    aget-object v6, v8, v7

    .line 378
    invoke-virtual {v6}, Ljbp;->nH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Ljbp;->nI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Ljbp;->bfd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Ljbp;->bfc()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_1

    .line 380
    :cond_0
    const-string v0, "PendingNotificationAdapter"

    const-string v2, "Geofence is incomplete"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 384
    :cond_1
    invoke-interface {p3}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 385
    iget-object v0, p1, Lamx;->afN:[J

    array-length v4, v0

    add-int/lit8 v4, v4, 0x1

    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v4

    array-length v0, v0

    aput-wide v2, v4, v0

    iput-object v4, p1, Lamx;->afN:[J

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "notification_"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbri;->aFS:Ljava/lang/String;

    invoke-virtual {v6}, Ljbp;->mR()D

    move-result-wide v2

    invoke-virtual {v6}, Ljbp;->mS()D

    move-result-wide v4

    invoke-virtual {v6}, Ljbp;->bfc()D

    move-result-wide v10

    double-to-float v6, v10

    invoke-virtual/range {v1 .. v6}, Lbri;->a(DDF)Lbri;

    .line 390
    invoke-virtual {v1}, Lbri;->Ae()Lbrh;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 392
    :cond_2
    return-void
.end method

.method final a(Lamx;J[Lamy;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 337
    invoke-virtual {p0, p1, p2, p3}, Lfgf;->c(Lamx;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v0

    .line 341
    :cond_1
    iget-object v2, p1, Lamx;->afA:Ljbj;

    .line 344
    invoke-static {v2}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v3

    .line 345
    array-length v4, p4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, p4, v2

    .line 346
    iget-object v5, v5, Lamy;->afA:Ljbj;

    invoke-static {v5}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v5

    invoke-virtual {v3, v5}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v0

    .line 353
    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 345
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2
.end method

.method final a(Ljcg;Ljcg;)Z
    .locals 2

    .prologue
    .line 362
    invoke-super {p0, p1, p2}, Lfgd;->a(Ljcg;Ljcg;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Ljcg;->dRY:Ljie;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iget-object v1, p2, Ljcg;->dRY:Ljie;

    invoke-static {v1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(Lamx;)J
    .locals 2

    .prologue
    .line 313
    invoke-virtual {p1}, Lamx;->nU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p1}, Lamx;->nT()J

    move-result-wide v0

    .line 316
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method
