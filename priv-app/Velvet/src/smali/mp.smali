.class Lmp;
.super Lmo;
.source "PG"

# interfaces
.implements Llo;
.implements Llu;


# static fields
.field private static final lK:Ljava/util/ArrayList;

.field private static final lL:Ljava/util/ArrayList;


# instance fields
.field private final lM:Lmy;

.field protected final lN:Ljava/lang/Object;

.field protected final lO:Ljava/lang/Object;

.field private lP:Ljava/lang/Object;

.field private lQ:Ljava/lang/Object;

.field protected lR:I

.field protected lS:Z

.field protected lT:Z

.field protected final lU:Ljava/util/ArrayList;

.field private lV:Ljava/util/ArrayList;

.field private lW:Lls;

.field private lX:Llq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 209
    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 211
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 212
    sput-object v1, Lmp;->lK:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 218
    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 221
    sput-object v1, Lmp;->lL:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmy;)V
    .locals 3

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lmo;-><init>(Landroid/content/Context;)V

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmp;->lV:Ljava/util/ArrayList;

    .line 250
    iput-object p2, p0, Lmp;->lM:Lmy;

    .line 251
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    .line 252
    invoke-virtual {p0}, Lmp;->cf()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lmp;->lO:Ljava/lang/Object;

    .line 253
    new-instance v0, Llv;

    invoke-direct {v0, p0}, Llv;-><init>(Llu;)V

    iput-object v0, p0, Lmp;->lP:Ljava/lang/Object;

    .line 255
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 256
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    const v2, 0x7f0a00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v0, Landroid/media/MediaRouter;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;

    move-result-object v0

    iput-object v0, p0, Lmp;->lQ:Ljava/lang/Object;

    .line 259
    invoke-direct {p0}, Lmp;->cc()V

    .line 260
    return-void
.end method

.method private T(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 319
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {p0, p1}, Lmp;->U(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_4

    .line 321
    invoke-virtual {p0}, Lmp;->cg()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v4

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "DEFAULT_ROUTE"

    :goto_1
    invoke-direct {p0, v0}, Lmp;->n(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_2

    .line 322
    :goto_2
    new-instance v1, Lmr;

    invoke-direct {v1, p1, v0}, Lmr;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    invoke-direct {p0, v1}, Lmp;->a(Lmr;)V

    .line 324
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v4

    .line 327
    :goto_3
    return v0

    :cond_0
    move v0, v5

    .line 321
    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ROUTE_%08x"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lmp;->W(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_4
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s_%d"

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v3, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lmp;->n(Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_3

    move-object v0, v3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    move v0, v5

    .line 327
    goto :goto_3
.end method

.method private static V(Ljava/lang/Object;)Lms;
    .locals 2

    .prologue
    .line 564
    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 565
    instance-of v1, v0, Lms;

    if-eqz v1, :cond_0

    check-cast v0, Lms;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lkt;->mContext:Landroid/content/Context;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 583
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private a(Lmr;)V
    .locals 3

    .prologue
    .line 571
    new-instance v0, Lkr;

    iget-object v1, p1, Lmr;->lZ:Ljava/lang/String;

    iget-object v2, p1, Lmr;->lY:Ljava/lang/Object;

    invoke-direct {p0, v2}, Lmp;->W(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkr;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    invoke-virtual {p0, p1, v0}, Lmp;->a(Lmr;Lkr;)V

    .line 574
    invoke-virtual {v0}, Lkr;->bz()Lkq;

    move-result-object v0

    iput-object v0, p1, Lmr;->ma:Lkq;

    .line 575
    return-void
.end method

.method private cc()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 309
    .line 310
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 311
    invoke-direct {p0, v1}, Lmp;->T(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 312
    goto :goto_1

    .line 313
    :cond_1
    if-eqz v0, :cond_2

    .line 314
    invoke-virtual {p0}, Lmp;->cd()V

    .line 316
    :cond_2
    return-void
.end method

.method private h(Llm;)I
    .locals 3

    .prologue
    .line 554
    iget-object v0, p0, Lmp;->lV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 555
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 556
    iget-object v0, p0, Lmp;->lV:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lms;

    iget-object v0, v0, Lms;->mb:Llm;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 560
    :goto_1
    return v0

    .line 555
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 560
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private n(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 545
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 546
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    iget-object v0, v0, Lmr;->lZ:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 550
    :goto_1
    return v0

    .line 545
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 550
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final L(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    const v1, 0x800003

    invoke-static {v0, v1}, Lln;->e(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_2

    .line 400
    iget-object v0, v0, Lms;->mb:Llm;

    invoke-virtual {v0}, Llm;->select()V

    goto :goto_0

    .line 404
    :cond_2
    invoke-virtual {p0, p1}, Lmp;->U(Ljava/lang/Object;)I

    move-result v0

    .line 405
    if-ltz v0, :cond_0

    .line 406
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 407
    iget-object v1, p0, Lmp;->lM:Lmy;

    iget-object v0, v0, Lmr;->lZ:Ljava/lang/String;

    invoke-interface {v1, v0}, Lmy;->j(Ljava/lang/String;)Llm;

    move-result-object v0

    .line 409
    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {v0}, Llm;->select()V

    goto :goto_0
.end method

.method public final M(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lmp;->T(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lmp;->cd()V

    .line 306
    :cond_0
    return-void
.end method

.method public final N(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 350
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    if-nez v0, :cond_0

    .line 351
    invoke-virtual {p0, p1}, Lmp;->U(Ljava/lang/Object;)I

    move-result v0

    .line 352
    if-ltz v0, :cond_0

    .line 353
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 354
    invoke-virtual {p0}, Lmp;->cd()V

    .line 357
    :cond_0
    return-void
.end method

.method public final O(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 361
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    if-nez v0, :cond_0

    .line 362
    invoke-virtual {p0, p1}, Lmp;->U(Ljava/lang/Object;)I

    move-result v0

    .line 363
    if-ltz v0, :cond_0

    .line 364
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 365
    invoke-direct {p0, v0}, Lmp;->a(Lmr;)V

    .line 366
    invoke-virtual {p0}, Lmp;->cd()V

    .line 369
    :cond_0
    return-void
.end method

.method public final P(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 373
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    if-nez v0, :cond_0

    .line 374
    invoke-virtual {p0, p1}, Lmp;->U(Ljava/lang/Object;)I

    move-result v0

    .line 375
    if-ltz v0, :cond_0

    .line 376
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 377
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v1

    .line 378
    iget-object v2, v0, Lmr;->ma:Lkq;

    invoke-virtual {v2}, Lkq;->getVolume()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 379
    new-instance v2, Lkr;

    iget-object v3, v0, Lmr;->ma:Lkq;

    invoke-direct {v2, v3}, Lkr;-><init>(Lkq;)V

    invoke-virtual {v2, v1}, Lkr;->R(I)Lkr;

    move-result-object v1

    invoke-virtual {v1}, Lkr;->bz()Lkq;

    move-result-object v1

    iput-object v1, v0, Lmr;->ma:Lkq;

    .line 383
    invoke-virtual {p0}, Lmp;->cd()V

    .line 387
    :cond_0
    return-void
.end method

.method protected final U(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 535
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 536
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    iget-object v0, v0, Lmr;->lY:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 540
    :goto_1
    return v0

    .line 535
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 540
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected X(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const v5, 0x800003

    .line 645
    iget-object v0, p0, Lmp;->lW:Lls;

    if-nez v0, :cond_0

    .line 646
    new-instance v0, Lls;

    invoke-direct {v0}, Lls;-><init>()V

    iput-object v0, p0, Lmp;->lW:Lls;

    .line 648
    :cond_0
    iget-object v1, p0, Lmp;->lW:Lls;

    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v2

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    if-nez v2, :cond_1

    iget-object v2, v1, Lls;->kZ:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_2

    :try_start_0
    iget-object v1, v1, Lls;->kZ:Ljava/lang/reflect/Method;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x800003

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 650
    :goto_0
    return-void

    .line 648
    :catch_0
    move-exception v1

    const-string v2, "MediaRouterJellybean"

    const-string v3, "Cannot programmatically select non-user route.  Media routing may not work."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_1
    invoke-virtual {v0, v5, p1}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "MediaRouterJellybean"

    const-string v3, "Cannot programmatically select non-user route.  Media routing may not work."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    const-string v1, "MediaRouterJellybean"

    const-string v2, "Cannot programmatically select non-user route because the platform is missing the selectRouteInt() method.  Media routing may not work."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected a(Lmr;Lkr;)V
    .locals 2

    .prologue
    .line 588
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v0

    .line 590
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 591
    sget-object v1, Lmp;->lK:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Lkr;->b(Ljava/util/Collection;)Lkr;

    .line 593
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 594
    sget-object v0, Lmp;->lL:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Lkr;->b(Ljava/util/Collection;)Lkr;

    .line 597
    :cond_1
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v0

    invoke-virtual {p2, v0}, Lkr;->P(I)Lkr;

    .line 599
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackStream()I

    move-result v0

    invoke-virtual {p2, v0}, Lkr;->Q(I)Lkr;

    .line 601
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v0

    invoke-virtual {p2, v0}, Lkr;->R(I)Lkr;

    .line 603
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v0

    invoke-virtual {p2, v0}, Lkr;->S(I)Lkr;

    .line 605
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v0

    invoke-virtual {p2, v0}, Lkr;->T(I)Lkr;

    .line 607
    return-void
.end method

.method protected a(Lms;)V
    .locals 2

    .prologue
    .line 610
    iget-object v0, p1, Lms;->lY:Ljava/lang/Object;

    iget-object v1, p1, Lms;->mb:Llm;

    iget-object v1, v1, Llm;->mName:Ljava/lang/String;

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setName(Ljava/lang/CharSequence;)V

    .line 612
    iget-object v0, p1, Lms;->lY:Ljava/lang/Object;

    iget-object v1, p1, Lms;->mb:Llm;

    iget v1, v1, Llm;->kQ:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackType(I)V

    .line 614
    iget-object v0, p1, Lms;->lY:Ljava/lang/Object;

    iget-object v1, p1, Lms;->mb:Llm;

    iget v1, v1, Llm;->kR:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackStream(I)V

    .line 616
    iget-object v0, p1, Lms;->lY:Ljava/lang/Object;

    iget-object v1, p1, Lms;->mb:Llm;

    iget v1, v1, Llm;->kT:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolume(I)V

    .line 618
    iget-object v0, p1, Lms;->lY:Ljava/lang/Object;

    iget-object v1, p1, Lms;->mb:Llm;

    iget v1, v1, Llm;->kU:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeMax(I)V

    .line 620
    iget-object v0, p1, Lms;->lY:Ljava/lang/Object;

    iget-object v1, p1, Lms;->mb:Llm;

    iget v1, v1, Llm;->kS:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeHandling(I)V

    .line 622
    return-void
.end method

.method public final b(Lks;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 274
    .line 276
    if-eqz p1, :cond_5

    .line 277
    invoke-virtual {p1}, Lks;->bA()Llb;

    move-result-object v1

    .line 278
    invoke-virtual {v1}, Llb;->bH()Ljava/util/List;

    move-result-object v3

    .line 279
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    .line 280
    :goto_0
    if-ge v2, v4, :cond_2

    .line 281
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 282
    const-string v5, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 283
    or-int/lit8 v0, v1, 0x1

    .line 280
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 284
    :cond_0
    const-string v5, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    or-int/lit8 v0, v1, 0x2

    goto :goto_1

    .line 287
    :cond_1
    const/high16 v0, 0x800000

    or-int/2addr v0, v1

    goto :goto_1

    .line 290
    :cond_2
    invoke-virtual {p1}, Lks;->bC()Z

    move-result v0

    .line 293
    :goto_2
    iget v2, p0, Lmp;->lR:I

    if-ne v2, v1, :cond_3

    iget-boolean v2, p0, Lmp;->lS:Z

    if-eq v2, v0, :cond_4

    .line 294
    :cond_3
    iput v1, p0, Lmp;->lR:I

    .line 295
    iput-boolean v0, p0, Lmp;->lS:Z

    .line 296
    invoke-virtual {p0}, Lmp;->ce()V

    .line 297
    invoke-direct {p0}, Lmp;->cc()V

    .line 299
    :cond_4
    return-void

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method protected final cd()V
    .locals 4

    .prologue
    .line 523
    new-instance v2, Lkz;

    invoke-direct {v2}, Lkz;-><init>()V

    .line 525
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 526
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 527
    iget-object v0, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    iget-object v0, v0, Lmr;->ma:Lkq;

    invoke-virtual {v2, v0}, Lkz;->a(Lkq;)Lkz;

    .line 526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 530
    :cond_0
    invoke-virtual {v2}, Lkz;->bG()Lky;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmp;->a(Lky;)V

    .line 531
    return-void
.end method

.method protected ce()V
    .locals 3

    .prologue
    .line 625
    iget-boolean v0, p0, Lmp;->lT:Z

    if-eqz v0, :cond_0

    .line 626
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmp;->lT:Z

    .line 627
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    iget-object v1, p0, Lmp;->lO:Ljava/lang/Object;

    invoke-static {v0, v1}, Lln;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 630
    :cond_0
    iget v0, p0, Lmp;->lR:I

    if-eqz v0, :cond_1

    .line 631
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmp;->lT:Z

    .line 632
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    iget v2, p0, Lmp;->lR:I

    iget-object v1, p0, Lmp;->lO:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v2, v1}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    .line 634
    :cond_1
    return-void
.end method

.method protected cf()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 637
    new-instance v0, Llp;

    invoke-direct {v0, p0}, Llp;-><init>(Llo;)V

    return-object v0
.end method

.method protected cg()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Lmp;->lX:Llq;

    if-nez v0, :cond_0

    .line 654
    new-instance v0, Llq;

    invoke-direct {v0}, Llq;-><init>()V

    iput-object v0, p0, Lmp;->lX:Llq;

    .line 656
    :cond_0
    iget-object v0, p0, Lmp;->lX:Llq;

    iget-object v1, p0, Lmp;->lN:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Llq;->Q(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d(Llm;)V
    .locals 3

    .prologue
    .line 450
    invoke-virtual {p1}, Llm;->bS()Lkt;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 451
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    iget-object v1, p0, Lmp;->lQ:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;

    move-result-object v1

    .line 453
    new-instance v0, Lms;

    invoke-direct {v0, p1, v1}, Lms;-><init>(Llm;Ljava/lang/Object;)V

    .line 454
    invoke-static {v1, v0}, Llr;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 455
    iget-object v2, p0, Lmp;->lP:Ljava/lang/Object;

    invoke-static {v1, v2}, Llt;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 456
    invoke-virtual {p0, v0}, Lmp;->a(Lms;)V

    .line 457
    iget-object v2, p0, Lmp;->lV:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v0, p0, Lmp;->lN:Ljava/lang/Object;

    const v1, 0x800003

    invoke-static {v0, v1}, Lln;->e(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 465
    invoke-virtual {p0, v0}, Lmp;->U(Ljava/lang/Object;)I

    move-result v0

    .line 466
    if-ltz v0, :cond_0

    .line 467
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 468
    iget-object v0, v0, Lmr;->lZ:Ljava/lang/String;

    iget-object v1, p1, Llm;->kM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p1}, Llm;->select()V

    goto :goto_0
.end method

.method public final e(Llm;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 477
    invoke-virtual {p1}, Llm;->bS()Lkt;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 478
    invoke-direct {p0, p1}, Lmp;->h(Llm;)I

    move-result v0

    .line 479
    if-ltz v0, :cond_0

    .line 480
    iget-object v1, p0, Lmp;->lV:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lms;

    .line 481
    iget-object v1, v0, Lms;->lY:Ljava/lang/Object;

    invoke-static {v1, v2}, Llr;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 482
    iget-object v1, v0, Lms;->lY:Ljava/lang/Object;

    invoke-static {v1, v2}, Llt;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 483
    iget-object v1, p0, Lmp;->lN:Ljava/lang/Object;

    iget-object v2, v0, Lms;->lY:Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, Landroid/media/MediaRouter;

    move-object v1, v2

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    .line 486
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/Object;I)V
    .locals 4

    .prologue
    .line 434
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    .line 435
    if-eqz v0, :cond_0

    .line 436
    iget-object v0, v0, Lms;->mb:Llm;

    invoke-static {}, Lld;->bN()V

    sget-object v1, Lld;->kt:Llh;

    iget v2, v0, Llm;->kU:I

    const/4 v3, 0x0

    invoke-static {v3, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v1, Llh;->kG:Llm;

    if-ne v0, v3, :cond_0

    iget-object v0, v1, Llh;->kH:Lkx;

    if-eqz v0, :cond_0

    iget-object v0, v1, Llh;->kH:Lkx;

    invoke-virtual {v0, v2}, Lkx;->V(I)V

    .line 438
    :cond_0
    return-void
.end method

.method public final f(Llm;)V
    .locals 2

    .prologue
    .line 490
    invoke-virtual {p1}, Llm;->bS()Lkt;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 491
    invoke-direct {p0, p1}, Lmp;->h(Llm;)I

    move-result v0

    .line 492
    if-ltz v0, :cond_0

    .line 493
    iget-object v1, p0, Lmp;->lV:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lms;

    .line 494
    invoke-virtual {p0, v0}, Lmp;->a(Lms;)V

    .line 497
    :cond_0
    return-void
.end method

.method public final g(Ljava/lang/String;)Lkx;
    .locals 2

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lmp;->n(Ljava/lang/String;)I

    move-result v0

    .line 265
    if-ltz v0, :cond_0

    .line 266
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 267
    new-instance v1, Lmq;

    iget-object v0, v0, Lmr;->lY:Ljava/lang/Object;

    invoke-direct {v1, p0, v0}, Lmq;-><init>(Lmp;Ljava/lang/Object;)V

    move-object v0, v1

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 442
    invoke-static {p1}, Lmp;->V(Ljava/lang/Object;)Lms;

    move-result-object v0

    .line 443
    if-eqz v0, :cond_0

    .line 444
    iget-object v0, v0, Lms;->mb:Llm;

    invoke-static {}, Lld;->bN()V

    if-eqz p2, :cond_0

    sget-object v1, Lld;->kt:Llh;

    iget-object v2, v1, Llh;->kG:Llm;

    if-ne v0, v2, :cond_0

    iget-object v0, v1, Llh;->kH:Lkx;

    if-eqz v0, :cond_0

    iget-object v0, v1, Llh;->kH:Lkx;

    invoke-virtual {v0, p2}, Lkx;->W(I)V

    .line 446
    :cond_0
    return-void
.end method

.method public final g(Llm;)V
    .locals 2

    .prologue
    .line 501
    invoke-static {}, Lld;->bN()V

    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0}, Llh;->bM()Llm;

    move-result-object v0

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 520
    :cond_0
    :goto_1
    return-void

    .line 501
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 507
    :cond_2
    invoke-virtual {p1}, Llm;->bS()Lkt;

    move-result-object v0

    if-eq v0, p0, :cond_3

    .line 508
    invoke-direct {p0, p1}, Lmp;->h(Llm;)I

    move-result v0

    .line 509
    if-ltz v0, :cond_0

    .line 510
    iget-object v1, p0, Lmp;->lV:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lms;

    .line 511
    iget-object v0, v0, Lms;->lY:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmp;->X(Ljava/lang/Object;)V

    goto :goto_1

    .line 514
    :cond_3
    iget-object v0, p1, Llm;->kM:Ljava/lang/String;

    invoke-direct {p0, v0}, Lmp;->n(Ljava/lang/String;)I

    move-result v0

    .line 515
    if-ltz v0, :cond_0

    .line 516
    iget-object v1, p0, Lmp;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 517
    iget-object v0, v0, Lmr;->lY:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmp;->X(Ljava/lang/Object;)V

    goto :goto_1
.end method
