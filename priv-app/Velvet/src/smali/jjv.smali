.class public final Ljjv;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eps:[Ljjv;


# instance fields
.field private aez:I

.field private ept:Ljava/lang/String;

.field private epu:Ljava/lang/String;

.field private epv:Ljava/lang/String;

.field private epw:Ljava/lang/String;

.field private epx:Z

.field private epy:[I

.field private epz:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 146
    iput v1, p0, Ljjv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljjv;->ept:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljjv;->epu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljjv;->epv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljjv;->epw:Ljava/lang/String;

    iput-boolean v1, p0, Ljjv;->epx:Z

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljjv;->epy:[I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjv;->eCz:I

    .line 147
    return-void
.end method

.method public static boe()[Ljjv;
    .locals 2

    .prologue
    .line 19
    sget-object v0, Ljjv;->eps:[Ljjv;

    if-nez v0, :cond_1

    .line 20
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Ljjv;->eps:[Ljjv;

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Ljjv;

    sput-object v0, Ljjv;->eps:[Ljjv;

    .line 25
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Ljjv;->eps:[Ljjv;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjv;->ept:Ljava/lang/String;

    iget v0, p0, Ljjv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjv;->epu:Ljava/lang/String;

    iget v0, p0, Ljjv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjv;->epv:Ljava/lang/String;

    iget v0, p0, Ljjv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjv;->epw:Ljava/lang/String;

    iget v0, p0, Ljjv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjv;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljjv;->epx:Z

    iget v0, p0, Ljjv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljjv;->aez:I

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljjv;->epy:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljjv;->epy:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljjv;->epy:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljjv;->epy:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljjv;->epy:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Ljjv;->epy:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljjv;->epy:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljjv;->epy:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljjv;->epy:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljjv;->epz:[Ljava/lang/String;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_7

    :cond_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    iput-object v1, p0, Ljjv;->epz:[Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 166
    iget v0, p0, Ljjv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x1

    iget-object v2, p0, Ljjv;->ept:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 169
    :cond_0
    iget v0, p0, Ljjv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 170
    const/4 v0, 0x2

    iget-object v2, p0, Ljjv;->epu:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 172
    :cond_1
    iget v0, p0, Ljjv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 173
    const/4 v0, 0x3

    iget-object v2, p0, Ljjv;->epv:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 175
    :cond_2
    iget v0, p0, Ljjv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 176
    const/4 v0, 0x4

    iget-object v2, p0, Ljjv;->epw:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 178
    :cond_3
    iget v0, p0, Ljjv;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 179
    const/4 v0, 0x5

    iget-boolean v2, p0, Ljjv;->epx:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 181
    :cond_4
    iget-object v0, p0, Ljjv;->epy:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljjv;->epy:[I

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 182
    :goto_0
    iget-object v2, p0, Ljjv;->epy:[I

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 183
    const/4 v2, 0x6

    iget-object v3, p0, Ljjv;->epy:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_5
    iget-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 187
    :goto_1
    iget-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 188
    iget-object v0, p0, Ljjv;->epz:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 189
    if-eqz v0, :cond_6

    .line 190
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 187
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 194
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 195
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 200
    iget v1, p0, Ljjv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 201
    const/4 v1, 0x1

    iget-object v3, p0, Ljjv;->ept:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_0
    iget v1, p0, Ljjv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 205
    const/4 v1, 0x2

    iget-object v3, p0, Ljjv;->epu:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_1
    iget v1, p0, Ljjv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v3, p0, Ljjv;->epv:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_2
    iget v1, p0, Ljjv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 213
    const/4 v1, 0x4

    iget-object v3, p0, Ljjv;->epw:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_3
    iget v1, p0, Ljjv;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 217
    const/4 v1, 0x5

    iget-boolean v3, p0, Ljjv;->epx:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 220
    :cond_4
    iget-object v1, p0, Ljjv;->epy:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljjv;->epy:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    .line 222
    :goto_0
    iget-object v4, p0, Ljjv;->epy:[I

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 223
    iget-object v4, p0, Ljjv;->epy:[I

    aget v4, v4, v1

    .line 224
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    :cond_5
    add-int/2addr v0, v3

    .line 228
    iget-object v1, p0, Ljjv;->epy:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 230
    :cond_6
    iget-object v1, p0, Ljjv;->epz:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Ljjv;->epz:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    .line 233
    :goto_1
    iget-object v4, p0, Ljjv;->epz:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_8

    .line 234
    iget-object v4, p0, Ljjv;->epz:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 235
    if-eqz v4, :cond_7

    .line 236
    add-int/lit8 v3, v3, 0x1

    .line 237
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 233
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 241
    :cond_8
    add-int/2addr v0, v1

    .line 242
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 244
    :cond_9
    return v0
.end method
