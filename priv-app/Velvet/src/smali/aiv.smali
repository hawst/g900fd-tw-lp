.class final Laiv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field private synthetic ZP:Lair;

.field private ZT:Lrc;


# direct methods
.method private constructor <init>(Lair;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Laiv;->ZP:Lair;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lair;B)V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0, p1}, Laiv;-><init>(Lair;)V

    return-void
.end method


# virtual methods
.method public final onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6

    .prologue
    .line 312
    iget-object v0, p0, Laiv;->ZT:Lrc;

    invoke-static {}, Lrc;->dO()V

    .line 314
    iget-object v0, p0, Laiv;->ZP:Lair;

    iget-object v1, v0, Lair;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 315
    :try_start_0
    iget-object v0, p0, Laiv;->ZP:Lair;

    iget-object v0, v0, Lair;->ZN:Laiu;

    iget-object v0, v0, Laiu;->ZR:Ljava/lang/Runnable;

    .line 316
    iget-object v2, p0, Laiv;->ZP:Lair;

    iget-object v2, v2, Lair;->ZN:Laiu;

    iget-object v2, v2, Laiu;->ZS:Laim;

    iget-object v3, p0, Laiv;->ZP:Lair;

    iget-object v3, v3, Lair;->ZN:Laiu;

    iget-object v3, v3, Laiu;->ZQ:Laiq;

    iget-object v4, p0, Laiv;->ZP:Lair;

    iget-object v4, v4, Lair;->ZN:Laiu;

    iget v4, v4, Laiu;->rotation:I

    invoke-virtual {v2, v3, v4}, Laim;->a(Laiq;I)V

    .line 317
    iget-object v2, p0, Laiv;->ZP:Lair;

    iget-object v2, v2, Lair;->ZN:Laiu;

    iget-object v2, v2, Laiu;->ZS:Laim;

    iget-object v3, p0, Laiv;->ZP:Lair;

    iget-object v3, v3, Lair;->ZN:Laiu;

    iget v3, v3, Laiu;->centerX:I

    iget-object v4, p0, Laiv;->ZP:Lair;

    iget-object v4, v4, Lair;->ZN:Laiu;

    iget v4, v4, Laiu;->centerY:I

    iget-object v5, p0, Laiv;->ZP:Lair;

    iget-object v5, v5, Lair;->ZN:Laiu;

    iget v5, v5, Laiu;->Ix:F

    invoke-virtual {v2, v3, v4, v5}, Laim;->a(IIF)V

    .line 319
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    iget-object v1, p0, Laiv;->ZP:Lair;

    iget-object v1, v1, Lair;->ZN:Laiu;

    iget-object v1, v1, Laiu;->ZS:Laim;

    iget-object v2, p0, Laiv;->ZT:Lrc;

    invoke-virtual {v1, v2}, Laim;->d(Lrb;)Z

    move-result v1

    .line 321
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 322
    iget-object v1, p0, Laiv;->ZP:Lair;

    iget-object v1, v1, Lair;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 325
    :try_start_1
    iget-object v2, p0, Laiv;->ZP:Lair;

    iget-object v2, v2, Lair;->ZN:Laiu;

    iget-object v2, v2, Laiu;->ZR:Ljava/lang/Runnable;

    if-ne v2, v0, :cond_0

    .line 326
    iget-object v2, p0, Laiv;->ZP:Lair;

    iget-object v2, v2, Lair;->ZN:Laiu;

    const/4 v3, 0x0

    iput-object v3, v2, Laiu;->ZR:Ljava/lang/Runnable;

    .line 328
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 329
    if-eqz v0, :cond_1

    .line 330
    iget-object v1, p0, Laiv;->ZP:Lair;

    invoke-virtual {v1, v0}, Lair;->post(Ljava/lang/Runnable;)Z

    .line 333
    :cond_1
    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 328
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Laiv;->ZT:Lrc;

    invoke-virtual {v0, p2, p3}, Lrc;->setSize(II)V

    .line 307
    iget-object v0, p0, Laiv;->ZP:Lair;

    iget-object v0, v0, Lair;->ZN:Laiu;

    iget-object v0, v0, Laiu;->ZS:Laim;

    invoke-virtual {v0, p2, p3}, Laim;->U(II)V

    .line 308
    return-void
.end method

.method public final onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3

    .prologue
    .line 299
    new-instance v0, Lrc;

    invoke-direct {v0}, Lrc;-><init>()V

    iput-object v0, p0, Laiv;->ZT:Lrc;

    .line 300
    invoke-static {}, Lqz;->dL()V

    .line 301
    iget-object v0, p0, Laiv;->ZP:Lair;

    iget-object v0, v0, Lair;->ZN:Laiu;

    iget-object v0, v0, Laiu;->ZS:Laim;

    iget-object v1, p0, Laiv;->ZP:Lair;

    iget-object v1, v1, Lair;->ZN:Laiu;

    iget-object v1, v1, Laiu;->ZQ:Laiq;

    iget-object v2, p0, Laiv;->ZP:Lair;

    iget-object v2, v2, Lair;->ZN:Laiu;

    iget v2, v2, Laiu;->rotation:I

    invoke-virtual {v0, v1, v2}, Laim;->a(Laiq;I)V

    .line 302
    return-void
.end method
