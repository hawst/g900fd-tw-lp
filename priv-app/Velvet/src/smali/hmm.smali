.class public Lhmm;
.super Lhlo;
.source "PG"


# instance fields
.field private final dku:Libs;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Leoj;Libs;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lhlo;-><init>(Leoj;)V

    .line 31
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libs;

    iput-object v0, p0, Lhmm;->dku:Libs;

    .line 32
    iput-object p3, p0, Lhmm;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 8

    .prologue
    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const-wide/16 v6, 0x7

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/search/shared/actions/ShowContactInformationAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lico;->k(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->aiK()Z

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-static {v0}, Lico;->l(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lhmm;->aQK()[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;I)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 37
    packed-switch p2, :pswitch_data_0

    .line 47
    invoke-super {p0, p1, p2}, Lhlo;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    :goto_0
    return v0

    .line 39
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->aiE()Lcom/google/android/search/shared/contact/Contact;

    move-result-object v0

    iget-object v1, p0, Lhmm;->mIntentStarter:Leoj;

    new-array v2, v6, [Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lico;->pa(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 41
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->aiE()Lcom/google/android/search/shared/contact/Contact;

    move-result-object v0

    iget-object v1, p0, Lhmm;->mIntentStarter:Leoj;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lhmm;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Ledx;->c(Ljava/lang/String;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 43
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->aiE()Lcom/google/android/search/shared/contact/Contact;

    move-result-object v0

    new-instance v2, Libu;

    invoke-direct {v2}, Libu;-><init>()V

    new-array v1, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alQ()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    iput-object v1, v2, Libu;->dxs:[Ljava/lang/String;

    iget-object v1, p0, Lhmm;->dku:Libs;

    iget-object v4, p0, Lhmm;->mIntentStarter:Leoj;

    iget-object v7, v1, Libs;->dkw:Lglm;

    new-instance v0, Libt;

    invoke-direct/range {v0 .. v5}, Libt;-><init>(Libs;Libu;ZLeoj;Lefk;)V

    invoke-interface {v7, v0}, Lglm;->a(Lefk;)V

    move v0, v6

    goto :goto_0

    .line 45
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->aiE()Lcom/google/android/search/shared/contact/Contact;

    move-result-object v0

    iget-object v1, p0, Lhmm;->mIntentStarter:Leoj;

    new-array v2, v6, [Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Lico;->be(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected aQK()[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/search/shared/actions/ShowContactInformationAction;

    invoke-virtual {p0, p1, p2}, Lhmm;->a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;I)Z

    move-result v0

    return v0
.end method
