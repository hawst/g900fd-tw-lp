.class public final Lacr;
.super Landroid/widget/GridLayout;
.source "PG"

# interfaces
.implements Lacf;


# instance fields
.field private QA:I

.field private Qz:I

.field private Rw:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    .prologue
    .line 36
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput p2, p0, Lacr;->Qz:I

    .line 38
    iput p3, p0, Lacr;->QA:I

    .line 39
    return-void
.end method


# virtual methods
.method public final eb()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-virtual {p0}, Lacr;->removeAllViews()V

    .line 92
    iput-object v1, p0, Lacr;->Rw:Ljava/lang/Runnable;

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lacr;->setLayerType(ILandroid/graphics/Paint;)V

    .line 94
    return-void
.end method

.method public final ec()I
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lacr;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lacr;->Rw:Ljava/lang/Runnable;

    .line 67
    return-void
.end method

.method public final jL()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lacr;->Qz:I

    return v0
.end method

.method final jM()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lacr;->QA:I

    return v0
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/GridLayout;->onDetachedFromWindow()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lacr;->Rw:Ljava/lang/Runnable;

    .line 63
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 70
    invoke-super/range {p0 .. p5}, Landroid/widget/GridLayout;->onLayout(ZIIII)V

    .line 71
    iget-object v0, p0, Lacr;->Rw:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lacr;->Rw:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 74
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/widget/GridLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 79
    invoke-virtual {p0}, Lacr;->getChildCount()I

    move-result v1

    .line 80
    if-lez v1, :cond_1

    .line 82
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lacr;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 84
    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 86
    :cond_1
    :goto_0
    return v0

    .line 84
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
