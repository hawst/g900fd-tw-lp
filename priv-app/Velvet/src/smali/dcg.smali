.class public Ldcg;
.super Lddj;
.source "PG"


# instance fields
.field private ag:I

.field private brs:Lcom/google/android/velvet/ActionData;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mQuery:Lcom/google/android/shared/search/Query;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Ldda;Lhym;)V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x1000

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 52
    iput-object p2, p0, Ldcg;->mSettings:Lhym;

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Ldcg;->ag:I

    .line 54
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    .line 56
    return-void
.end method

.method private G(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final E(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 125
    iget-object v2, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2, p1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqe()Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Ldcg;->ag:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Leqt;->H(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Ldcg;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepb;->aD(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final F(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 150
    invoke-virtual {p0, p1}, Ldcg;->E(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Ldcg;->ag:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final H(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189
    invoke-direct {p0, p1}, Ldcg;->G(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final WS()Z
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 71
    invoke-direct {p0, v0}, Ldcg;->G(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Ldcg;->H(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final WT()V
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Ldcg;->ag:I

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Ldcg;->ag:I

    .line 100
    const/16 v0, 0xbb

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 103
    invoke-virtual {p0}, Ldcg;->notifyChanged()V

    .line 105
    :cond_0
    return-void
.end method

.method public final WU()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldcg;->E(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    return v0
.end method

.method public final WV()Lcom/google/android/shared/search/Query;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 155
    iget-object v1, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 156
    iget-object v2, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Ldcg;->F(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    iput-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    .line 158
    iput-object v0, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    .line 160
    const/16 v0, 0xbe

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 163
    iget-object v0, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    .line 170
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    .line 77
    invoke-virtual {p2}, Lcom/google/android/velvet/ActionData;->aIT()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p2, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    .line 78
    return-void

    .line 77
    :cond_0
    sget-object p2, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    goto :goto_0
.end method

.method protected final a(Lddb;)V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    .line 67
    :cond_0
    return-void
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 194
    const-string v0, "PumpkinState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 195
    const-string v0, "mState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Ldcg;->ag:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 196
    const-string v0, "mQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 197
    const-string v0, "mData"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 198
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iput-object p2, p0, Ldcg;->brs:Lcom/google/android/velvet/ActionData;

    .line 177
    const/16 v0, 0xbf

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 180
    invoke-virtual {p0}, Ldcg;->notifyChanged()V

    .line 182
    :cond_0
    return-void
.end method

.method public final dg(Z)V
    .locals 2

    .prologue
    .line 81
    iget v0, p0, Ldcg;->ag:I

    if-nez v0, :cond_0

    .line 82
    if-eqz p1, :cond_1

    .line 83
    const/16 v0, 0xb9

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Ldcg;->ag:I

    .line 93
    :goto_0
    invoke-virtual {p0}, Ldcg;->notifyChanged()V

    .line 95
    :cond_0
    return-void

    .line 88
    :cond_1
    const/16 v0, 0xba

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldcg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 91
    const/4 v0, 0x2

    iput v0, p0, Ldcg;->ag:I

    goto :goto_0
.end method
