.class public final Land;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agg:Ljava/lang/String;

.field private agh:Ljava/lang/String;

.field private agi:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14605
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14606
    const/4 v0, 0x0

    iput v0, p0, Land;->aez:I

    const-string v0, ""

    iput-object v0, p0, Land;->agg:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Land;->agh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Land;->agi:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Land;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Land;->eCz:I

    .line 14607
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 14520
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Land;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Land;->agg:Ljava/lang/String;

    iget v0, p0, Land;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Land;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Land;->agh:Ljava/lang/String;

    iget v0, p0, Land;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Land;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Land;->agi:Ljava/lang/String;

    iget v0, p0, Land;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Land;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 14622
    iget v0, p0, Land;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14623
    const/4 v0, 0x1

    iget-object v1, p0, Land;->agg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14625
    :cond_0
    iget v0, p0, Land;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14626
    const/4 v0, 0x2

    iget-object v1, p0, Land;->agh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14628
    :cond_1
    iget v0, p0, Land;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14629
    const/4 v0, 0x3

    iget-object v1, p0, Land;->agi:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14631
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14632
    return-void
.end method

.method public final ae(Ljava/lang/String;)Land;
    .locals 1

    .prologue
    .line 14545
    if-nez p1, :cond_0

    .line 14546
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14548
    :cond_0
    iput-object p1, p0, Land;->agg:Ljava/lang/String;

    .line 14549
    iget v0, p0, Land;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Land;->aez:I

    .line 14550
    return-object p0
.end method

.method public final af(Ljava/lang/String;)Land;
    .locals 1

    .prologue
    .line 14567
    if-nez p1, :cond_0

    .line 14568
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14570
    :cond_0
    iput-object p1, p0, Land;->agh:Ljava/lang/String;

    .line 14571
    iget v0, p0, Land;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Land;->aez:I

    .line 14572
    return-object p0
.end method

.method public final ag(Ljava/lang/String;)Land;
    .locals 1

    .prologue
    .line 14589
    if-nez p1, :cond_0

    .line 14590
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14592
    :cond_0
    iput-object p1, p0, Land;->agi:Ljava/lang/String;

    .line 14593
    iget v0, p0, Land;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Land;->aez:I

    .line 14594
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 14636
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14637
    iget v1, p0, Land;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14638
    const/4 v1, 0x1

    iget-object v2, p0, Land;->agg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14641
    :cond_0
    iget v1, p0, Land;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14642
    const/4 v1, 0x2

    iget-object v2, p0, Land;->agh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14645
    :cond_1
    iget v1, p0, Land;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14646
    const/4 v1, 0x3

    iget-object v2, p0, Land;->agi:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14649
    :cond_2
    return v0
.end method

.method public final ou()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14542
    iget-object v0, p0, Land;->agg:Ljava/lang/String;

    return-object v0
.end method

.method public final ov()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14564
    iget-object v0, p0, Land;->agh:Ljava/lang/String;

    return-object v0
.end method

.method public final ow()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14586
    iget-object v0, p0, Land;->agi:Ljava/lang/String;

    return-object v0
.end method
