.class public Lgjr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglc;


# instance fields
.field private final bhu:I

.field private cIb:Ljava/io/InputStream;

.field private final cLH:Lgjh;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final cMT:I

.field private final cMU:Lgjy;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private cMV:Lgju;

.field private cMW:Lgjk;

.field private final mCallbackFactory:Lgiv;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mModeSelector:Lgjp;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mSpeechSettings:Lgdo;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgjh;IILgdo;Lgiv;Lgjp;Lgjy;)V
    .locals 0
    .param p1    # Lgjh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lgdo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lgiv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lgjp;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Lgjy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lgjr;->cLH:Lgjh;

    .line 66
    iput p2, p0, Lgjr;->cMT:I

    .line 67
    iput p3, p0, Lgjr;->bhu:I

    .line 68
    iput-object p4, p0, Lgjr;->mSpeechSettings:Lgdo;

    .line 69
    iput-object p5, p0, Lgjr;->mCallbackFactory:Lgiv;

    .line 70
    iput-object p6, p0, Lgjr;->mModeSelector:Lgjp;

    .line 71
    iput-object p7, p0, Lgjr;->cMU:Lgjy;

    .line 72
    return-void
.end method

.method private a(Lgiu;Leiq;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 148
    iput-object v0, p0, Lgjr;->cMV:Lgju;

    .line 149
    iput-object v0, p0, Lgjr;->cMW:Lgjk;

    .line 150
    invoke-interface {p1, p2}, Lgiu;->l(Leiq;)V

    .line 151
    return-void
.end method


# virtual methods
.method protected a(Lgjk;)Lgju;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223
    iget v0, p0, Lgjr;->cMT:I

    iget v1, p0, Lgjr;->bhu:I

    invoke-static {p1, v0, v1}, Lgju;->a(Lgjk;II)Lgju;

    move-result-object v0

    .line 225
    if-nez v0, :cond_0

    .line 226
    const v1, 0x8a5c0e

    invoke-static {v1}, Lhwt;->lx(I)V

    .line 229
    :cond_0
    return-object v0
.end method

.method public final a(Lger;Lggg;Lgnj;)V
    .locals 15

    .prologue
    .line 79
    invoke-static/range {p2 .. p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    const/4 v1, 0x0

    iput-object v1, p0, Lgjr;->cMV:Lgju;

    .line 82
    const/4 v1, 0x0

    iput-object v1, p0, Lgjr;->cMW:Lgjk;

    .line 84
    invoke-static {}, Lgju;->aGA()V

    .line 86
    invoke-virtual/range {p3 .. p3}, Lgnj;->aHK()Lgjo;

    move-result-object v9

    .line 89
    invoke-virtual/range {p3 .. p3}, Lgnj;->aHG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHJ()Lgjl;

    move-result-object v2

    invoke-virtual {p0, v1, v9, v2}, Lgjr;->d(Ljava/lang/String;Lgjo;Lgjl;)Lgjo;

    move-result-object v10

    .line 92
    iget-object v1, p0, Lgjr;->mCallbackFactory:Lgiv;

    move-object/from16 v0, p2

    invoke-interface {v1, v0, v10}, Lgiv;->a(Lggg;Lgjo;)Lgiu;

    move-result-object v11

    .line 93
    if-nez v10, :cond_1

    .line 94
    new-instance v1, Lehz;

    const v2, 0x70003

    invoke-direct {v1, v2}, Lehz;-><init>(I)V

    invoke-direct {p0, v11, v1}, Lgjr;->a(Lgiu;Leiq;)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lger;->df()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lgjr;->cIb:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    iget-object v12, p0, Lgjr;->cLH:Lgjh;

    iget-object v13, p0, Lgjr;->cMV:Lgju;

    iget-object v14, p0, Lgjr;->cIb:Ljava/io/InputStream;

    new-instance v1, Lgms;

    iget-object v2, p0, Lgjr;->mSpeechSettings:Lgdo;

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHC()Lgmx;

    move-result-object v3

    invoke-virtual {v3}, Lgmx;->getSamplingRate()I

    move-result v3

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHO()Z

    move-result v4

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHD()Z

    move-result v5

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHP()Z

    move-result v6

    invoke-direct/range {v1 .. v6}, Lgms;-><init>(Lgdo;IZZZ)V

    invoke-virtual {v1}, Lgms;->call()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljwb;

    invoke-virtual/range {p3 .. p3}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lgjr;->cMU:Lgjy;

    invoke-interface {v1, v10}, Lgjy;->c(Lgjo;)Lgjx;

    move-result-object v7

    iget-object v1, p0, Lgjr;->cMW:Lgjk;

    iget-object v8, v1, Lgjk;->cMz:Ljzp;

    move-object v1, v12

    move-object v2, v13

    move-object v3, v14

    move-object v4, v11

    invoke-virtual/range {v1 .. v8}, Lgjh;->a(Lgju;Ljava/io/InputStream;Lgiu;Ljwb;Ljava/lang/String;Lgjx;Ljzp;)V

    .line 114
    invoke-virtual {v10}, Lgjo;->aGy()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v9}, Lgjo;->aGy()Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    new-instance v1, Leia;

    invoke-direct {v1}, Leia;-><init>()V

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lggg;->a(Leiq;)V

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    new-instance v2, Lehx;

    const v3, 0x70007

    invoke-direct {v2, v1, v3}, Lehx;-><init>(Ljava/lang/Throwable;I)V

    invoke-direct {p0, v11, v2}, Lgjr;->a(Lgiu;Leiq;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lgjr;->cMV:Lgju;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lgjr;->cLH:Lgjh;

    iget-object v1, p0, Lgjr;->cMV:Lgju;

    invoke-virtual {v0, v1}, Lgjh;->a(Lgju;)V

    .line 125
    iput-object v2, p0, Lgjr;->cMV:Lgju;

    .line 128
    :cond_0
    iget-object v0, p0, Lgjr;->cIb:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 129
    iput-object v2, p0, Lgjr;->cIb:Ljava/io/InputStream;

    .line 130
    return-void
.end method

.method public final d(Ljava/lang/String;Lgjo;Lgjl;)Lgjo;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 157
    iget-object v0, p0, Lgjr;->cLH:Lgjh;

    invoke-virtual {v0}, Lgjh;->aGu()V

    .line 159
    iget-object v0, p0, Lgjr;->mModeSelector:Lgjp;

    invoke-interface {v0, p2}, Lgjp;->a(Lgjo;)Lgjo;

    move-result-object v2

    .line 160
    iget-object v0, p0, Lgjr;->mModeSelector:Lgjp;

    invoke-interface {v0, p2}, Lgjp;->b(Lgjo;)Lgjo;

    move-result-object v1

    .line 161
    if-nez v2, :cond_0

    move-object v1, v3

    .line 216
    :goto_0
    return-object v1

    .line 170
    :cond_0
    iget-object v0, p0, Lgjr;->cLH:Lgjh;

    invoke-virtual {v0, p1, v2, p3}, Lgjh;->a(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;

    move-result-object v0

    .line 174
    if-nez v0, :cond_1

    sget-object v4, Lgjo;->cMP:Lgjo;

    if-ne v2, v4, :cond_1

    const-string v4, "en-US"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 176
    iget-object v0, p0, Lgjr;->cLH:Lgjh;

    const-string v4, "en-US"

    invoke-virtual {v0, v4, v2, p3}, Lgjh;->a(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;

    move-result-object v0

    .line 181
    :cond_1
    if-nez v0, :cond_3

    .line 184
    if-nez v1, :cond_2

    move-object v1, v3

    .line 187
    goto :goto_0

    .line 191
    :cond_2
    iget-object v0, p0, Lgjr;->cLH:Lgjh;

    invoke-virtual {v0, p1, v1, v3}, Lgjh;->a(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;

    move-result-object v0

    .line 194
    if-nez v0, :cond_4

    move-object v1, v3

    .line 196
    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 206
    :cond_4
    invoke-virtual {p0, v0}, Lgjr;->a(Lgjk;)Lgju;

    move-result-object v2

    iput-object v2, p0, Lgjr;->cMV:Lgju;

    .line 207
    iget-object v2, p0, Lgjr;->cMV:Lgju;

    if-nez v2, :cond_5

    .line 211
    iput-object v3, p0, Lgjr;->cMW:Lgjk;

    move-object v1, v3

    .line 212
    goto :goto_0

    .line 215
    :cond_5
    iput-object v0, p0, Lgjr;->cMW:Lgjk;

    goto :goto_0
.end method
