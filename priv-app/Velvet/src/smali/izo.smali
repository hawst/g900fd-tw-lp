.class public final Lizo;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dUP:[Lizo;


# instance fields
.field private aez:I

.field private dMc:I

.field public dUQ:Lizq;

.field private dUR:J

.field public dUS:[Lizp;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34051
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 34052
    const/4 v0, 0x0

    iput v0, p0, Lizo;->aez:I

    iput-object v2, p0, Lizo;->dUQ:Lizq;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lizo;->dUR:J

    const/16 v0, 0xc

    iput v0, p0, Lizo;->dMc:I

    invoke-static {}, Lizp;->bdn()[Lizp;

    move-result-object v0

    iput-object v0, p0, Lizo;->dUS:[Lizp;

    iput-object v2, p0, Lizo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizo;->eCz:I

    .line 34053
    return-void
.end method

.method public static ag([B)Lizo;
    .locals 1

    .prologue
    .line 34194
    new-instance v0, Lizo;

    invoke-direct {v0}, Lizo;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    return-object v0
.end method

.method public static bdk()[Lizo;
    .locals 2

    .prologue
    .line 33994
    sget-object v0, Lizo;->dUP:[Lizo;

    if-nez v0, :cond_1

    .line 33995
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 33997
    :try_start_0
    sget-object v0, Lizo;->dUP:[Lizo;

    if-nez v0, :cond_0

    .line 33998
    const/4 v0, 0x0

    new-array v0, v0, [Lizo;

    sput-object v0, Lizo;->dUP:[Lizo;

    .line 34000
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34002
    :cond_1
    sget-object v0, Lizo;->dUP:[Lizo;

    return-object v0

    .line 34000
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 33847
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lizo;->dUQ:Lizq;

    if-nez v0, :cond_1

    new-instance v0, Lizq;

    invoke-direct {v0}, Lizq;-><init>()V

    iput-object v0, p0, Lizo;->dUQ:Lizq;

    :cond_1
    iget-object v0, p0, Lizo;->dUQ:Lizq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizo;->dUR:J

    iget v0, p0, Lizo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lizo;->dMc:I

    iget v0, p0, Lizo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizo;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizo;->dUS:[Lizp;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lizp;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lizo;->dUS:[Lizp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lizp;

    invoke-direct {v3}, Lizp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lizo;->dUS:[Lizp;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lizp;

    invoke-direct {v3}, Lizp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lizo;->dUS:[Lizp;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 34069
    iget-object v0, p0, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_0

    .line 34070
    const/4 v0, 0x1

    iget-object v1, p0, Lizo;->dUQ:Lizq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 34072
    :cond_0
    iget v0, p0, Lizo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 34073
    const/4 v0, 0x2

    iget-wide v2, p0, Lizo;->dUR:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 34075
    :cond_1
    iget v0, p0, Lizo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 34076
    const/4 v0, 0x3

    iget v1, p0, Lizo;->dMc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 34078
    :cond_2
    iget-object v0, p0, Lizo;->dUS:[Lizp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lizo;->dUS:[Lizp;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 34079
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lizo;->dUS:[Lizp;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 34080
    iget-object v1, p0, Lizo;->dUS:[Lizp;

    aget-object v1, v1, v0

    .line 34081
    if-eqz v1, :cond_3

    .line 34082
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 34079
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34086
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 34087
    return-void
.end method

.method public final aZt()I
    .locals 1

    .prologue
    .line 34032
    iget v0, p0, Lizo;->dMc:I

    return v0
.end method

.method public final bdl()J
    .locals 2

    .prologue
    .line 34013
    iget-wide v0, p0, Lizo;->dUR:J

    return-wide v0
.end method

.method public final bdm()Z
    .locals 1

    .prologue
    .line 34021
    iget v0, p0, Lizo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ct(J)Lizo;
    .locals 1

    .prologue
    .line 34016
    iput-wide p1, p0, Lizo;->dUR:J

    .line 34017
    iget v0, p0, Lizo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizo;->aez:I

    .line 34018
    return-object p0
.end method

.method public final hasError()Z
    .locals 1

    .prologue
    .line 34040
    iget v0, p0, Lizo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 34091
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 34092
    iget-object v1, p0, Lizo;->dUQ:Lizq;

    if-eqz v1, :cond_0

    .line 34093
    const/4 v1, 0x1

    iget-object v2, p0, Lizo;->dUQ:Lizq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34096
    :cond_0
    iget v1, p0, Lizo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 34097
    const/4 v1, 0x2

    iget-wide v2, p0, Lizo;->dUR:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 34100
    :cond_1
    iget v1, p0, Lizo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 34101
    const/4 v1, 0x3

    iget v2, p0, Lizo;->dMc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 34104
    :cond_2
    iget-object v1, p0, Lizo;->dUS:[Lizp;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lizo;->dUS:[Lizp;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 34105
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lizo;->dUS:[Lizp;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 34106
    iget-object v2, p0, Lizo;->dUS:[Lizp;

    aget-object v2, v2, v0

    .line 34107
    if-eqz v2, :cond_3

    .line 34108
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 34105
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 34113
    :cond_5
    return v0
.end method
