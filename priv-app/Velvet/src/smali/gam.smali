.class public final Lgam;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cEm:J

.field public cEn:J

.field public cEo:I

.field private cEp:Ljava/lang/Boolean;

.field public cEq:Ljcu;

.field public cEr:Ljava/lang/Boolean;

.field private final ckR:Liwk;

.field public cxS:Lixx;

.field private final mEntry:Lizj;


# direct methods
.method public constructor <init>(Lizj;Liwk;J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgam;->cEn:J

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lgam;->cEo:I

    .line 28
    iput-object v2, p0, Lgam;->cEp:Ljava/lang/Boolean;

    .line 29
    iput-object v2, p0, Lgam;->cEq:Ljcu;

    .line 30
    iput-object v2, p0, Lgam;->cxS:Lixx;

    .line 31
    iput-object v2, p0, Lgam;->cEr:Ljava/lang/Boolean;

    .line 41
    iput-object p1, p0, Lgam;->mEntry:Lizj;

    .line 42
    iput-object p2, p0, Lgam;->ckR:Liwk;

    .line 43
    iput-wide p3, p0, Lgam;->cEm:J

    .line 44
    return-void
.end method

.method public static H(Lizj;)Lizj;
    .locals 5

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    iget-object v1, p0, Lizj;->dSc:Ljal;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lizj;->dSc:Ljal;

    iget-object v1, v1, Ljal;->dMF:[Liyg;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 150
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-static {p0, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 151
    iget-object v2, v0, Lizj;->dSc:Ljal;

    .line 152
    const/4 v1, 0x0

    :goto_0
    iget-object v3, v2, Ljal;->dMF:[Liyg;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 153
    iget-object v3, v2, Ljal;->dMF:[Liyg;

    aget-object v3, v3, v1

    invoke-static {}, Ljbp;->beZ()[Ljbp;

    move-result-object v4

    iput-object v4, v3, Liyg;->dPI:[Ljbp;

    .line 152
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :cond_0
    iget-object v1, p0, Lizj;->dUo:[Liwk;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 158
    if-nez v0, :cond_1

    .line 159
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-static {p0, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 161
    :cond_1
    invoke-static {}, Liwk;->aZn()[Liwk;

    move-result-object v1

    iput-object v1, v0, Lizj;->dUo:[Liwk;

    .line 164
    :cond_2
    iget-object v1, p0, Lizj;->dTp:[Ljdj;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 165
    if-nez v0, :cond_3

    .line 166
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-static {p0, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 168
    :cond_3
    invoke-static {}, Ljdj;->bhp()[Ljdj;

    move-result-object v1

    iput-object v1, v0, Lizj;->dTp:[Ljdj;

    .line 171
    :cond_4
    if-eqz v0, :cond_5

    move-object p0, v0

    :cond_5
    return-object p0
.end method


# virtual methods
.method public final aDZ()Lizv;
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 110
    new-instance v0, Lizv;

    invoke-direct {v0}, Lizv;-><init>()V

    iget-wide v2, p0, Lgam;->cEm:J

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lizv;->cw(J)Lizv;

    move-result-object v0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    iget-wide v2, p0, Lgam;->cEm:J

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    int-to-long v2, v1

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lizv;->nX(I)Lizv;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lgam;->ckR:Liwk;

    iput-object v1, v0, Lizv;->dVr:Liwk;

    .line 115
    iget-object v1, p0, Lgam;->mEntry:Lizj;

    invoke-static {v1}, Lgam;->H(Lizj;)Lizj;

    move-result-object v1

    iput-object v1, v0, Lizv;->entry:Lizj;

    .line 116
    iget-wide v2, p0, Lgam;->cEn:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 117
    iget-wide v2, p0, Lgam;->cEn:J

    invoke-virtual {v0, v2, v3}, Lizv;->cx(J)Lizv;

    .line 119
    :cond_0
    iget v1, p0, Lgam;->cEo:I

    if-lez v1, :cond_1

    .line 120
    iget v1, p0, Lgam;->cEo:I

    invoke-virtual {v0, v1}, Lizv;->nY(I)Lizv;

    .line 122
    :cond_1
    iget-object v1, p0, Lgam;->cEp:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 123
    iget-object v1, p0, Lgam;->cEp:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lizv;->hy(Z)Lizv;

    .line 125
    :cond_2
    iget-object v1, p0, Lgam;->cEq:Ljcu;

    iput-object v1, v0, Lizv;->dVG:Ljcu;

    .line 126
    iget-object v1, p0, Lgam;->cxS:Lixx;

    iput-object v1, v0, Lizv;->dVs:Lixx;

    .line 128
    iget-object v1, p0, Lgam;->cEr:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgam;->cEr:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 130
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lizv;->hz(Z)Lizv;

    .line 133
    :cond_3
    return-object v0
.end method

.method public final fB(Z)Lgam;
    .locals 1

    .prologue
    .line 75
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgam;->cEp:Ljava/lang/Boolean;

    .line 76
    return-object p0
.end method
