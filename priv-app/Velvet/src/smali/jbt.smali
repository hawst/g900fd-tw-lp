.class public final Ljbt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dZl:[Ljbt;


# instance fields
.field private aez:I

.field private dZm:I

.field private dZn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31414
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 31415
    const/4 v0, 0x0

    iput v0, p0, Ljbt;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljbt;->dZm:I

    const-string v0, ""

    iput-object v0, p0, Ljbt;->dZn:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljbt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbt;->eCz:I

    .line 31416
    return-void
.end method

.method public static bfu()[Ljbt;
    .locals 2

    .prologue
    .line 31360
    sget-object v0, Ljbt;->dZl:[Ljbt;

    if-nez v0, :cond_1

    .line 31361
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 31363
    :try_start_0
    sget-object v0, Ljbt;->dZl:[Ljbt;

    if-nez v0, :cond_0

    .line 31364
    const/4 v0, 0x0

    new-array v0, v0, [Ljbt;

    sput-object v0, Ljbt;->dZl:[Ljbt;

    .line 31366
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31368
    :cond_1
    sget-object v0, Ljbt;->dZl:[Ljbt;

    return-object v0

    .line 31366
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 31351
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljbt;->dZm:I

    iget v0, p0, Ljbt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbt;->dZn:Ljava/lang/String;

    iget v0, p0, Ljbt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbt;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 31430
    iget v0, p0, Ljbt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 31431
    const/4 v0, 0x1

    iget v1, p0, Ljbt;->dZm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 31433
    :cond_0
    iget v0, p0, Ljbt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 31434
    const/4 v0, 0x2

    iget-object v1, p0, Ljbt;->dZn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31436
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 31437
    return-void
.end method

.method public final bfv()I
    .locals 1

    .prologue
    .line 31376
    iget v0, p0, Ljbt;->dZm:I

    return v0
.end method

.method public final getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31395
    iget-object v0, p0, Ljbt;->dZn:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 31441
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 31442
    iget v1, p0, Ljbt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 31443
    const/4 v1, 0x1

    iget v2, p0, Ljbt;->dZm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 31446
    :cond_0
    iget v1, p0, Ljbt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 31447
    const/4 v1, 0x2

    iget-object v2, p0, Ljbt;->dZn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31450
    :cond_1
    return v0
.end method

.method public final oz(I)Ljbt;
    .locals 1

    .prologue
    .line 31379
    const/4 v0, 0x1

    iput v0, p0, Ljbt;->dZm:I

    .line 31380
    iget v0, p0, Ljbt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbt;->aez:I

    .line 31381
    return-object p0
.end method

.method public final sA(Ljava/lang/String;)Ljbt;
    .locals 1

    .prologue
    .line 31398
    if-nez p1, :cond_0

    .line 31399
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31401
    :cond_0
    iput-object p1, p0, Ljbt;->dZn:Ljava/lang/String;

    .line 31402
    iget v0, p0, Ljbt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbt;->aez:I

    .line 31403
    return-object p0
.end method
