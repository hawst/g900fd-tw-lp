.class public final Lgif;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcox;
.implements Leai;


# instance fields
.field private final cKY:Lgic;

.field private cKZ:Leai;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lcke;Lgic;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lgif;->mSearchSettings:Lcke;

    .line 31
    iput-object p2, p0, Lgif;->cKY:Lgic;

    .line 32
    invoke-interface {p1}, Lcke;->On()Ljtd;

    move-result-object v0

    .line 33
    invoke-direct {p0, v0}, Lgif;->b(Ljtd;)V

    .line 34
    return-void
.end method

.method private a(Ljtd;Z)V
    .locals 1

    .prologue
    .line 83
    if-nez p2, :cond_0

    .line 84
    invoke-direct {p0, p1}, Lgif;->b(Ljtd;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lgif;->mSearchSettings:Lcke;

    invoke-interface {v0, p1}, Lcke;->a(Ljtd;)V

    .line 87
    return-void
.end method

.method private b(Ljtd;)V
    .locals 1

    .prologue
    .line 90
    if-nez p1, :cond_0

    iget-object v0, p0, Lgif;->cKY:Lgic;

    :goto_0
    iput-object v0, p0, Lgif;->cKZ:Leai;

    .line 94
    return-void

    .line 90
    :cond_0
    new-instance v0, Lgie;

    invoke-direct {v0, p1}, Lgie;-><init>(Ljtd;)V

    goto :goto_0
.end method


# virtual methods
.method public final Ez()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x3

    return v0
.end method

.method public final a(Ljje;Z)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p1, Ljje;->eoD:Ljtd;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p1, Ljje;->eoD:Ljtd;

    invoke-direct {p0, v0, p2}, Lgif;->a(Ljtd;Z)V

    .line 74
    :cond_0
    return-void
.end method

.method public final aFO()Leai;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lgif;->cKZ:Leai;

    return-object v0
.end method

.method public final ci(Z)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lgif;->a(Ljtd;Z)V

    .line 79
    return-void
.end method

.method public final kH(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38
    iget-object v0, p0, Lgif;->cKZ:Leai;

    invoke-interface {v0, p1}, Leai;->kH(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final kI(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lgif;->cKZ:Leai;

    invoke-interface {v0, p1}, Leai;->kI(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final kJ(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lgif;->cKZ:Leai;

    invoke-interface {v0, p1}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lgif;->cKZ:Leai;

    invoke-interface {v0, p1}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lgif;->cKZ:Leai;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
