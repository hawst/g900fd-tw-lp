.class public final Lfff;
.super Lfeu;
.source "PG"


# instance fields
.field private cpU:Ljbp;

.field private cqc:Ljal;

.field private mClock:Lemp;

.field private mDirectionsLauncher:Lgah;

.field private final mTravelReport:Lgca;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lizj;Lemp;Ljbp;Lgah;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 37
    iput-object p2, p0, Lfff;->mClock:Lemp;

    .line 38
    iput-object p3, p0, Lfff;->cpU:Ljbp;

    .line 39
    iget-object v0, p1, Lizj;->dTg:Ljal;

    iput-object v0, p0, Lfff;->cqc:Ljal;

    .line 40
    iget-object v0, p0, Lfff;->cqc:Ljal;

    iget-object v0, v0, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 41
    new-instance v0, Lgca;

    iget-object v1, p0, Lfff;->cqc:Ljal;

    iget-object v1, v1, Ljal;->dMF:[Liyg;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, Lfff;->mClock:Lemp;

    invoke-direct {v0, v1, v2}, Lgca;-><init>(Liyg;Lemp;)V

    iput-object v0, p0, Lfff;->mTravelReport:Lgca;

    .line 45
    :goto_0
    iput-object p4, p0, Lfff;->mDirectionsLauncher:Lgah;

    .line 46
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfff;->mTravelReport:Lgca;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 61
    iget-object v0, p0, Lfff;->mTravelReport:Lgca;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lfff;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 64
    invoke-static {p1, v0, v1, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lfff;->cqc:Ljal;

    const v2, 0x7f0a01af

    invoke-static {v1, v2}, Lfvt;->a(Ljal;I)I

    move-result v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v0, ""

    aput-object v0, v2, v4

    const/4 v0, 0x2

    const-string v3, ""

    aput-object v3, v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 77
    iget-object v0, p0, Lfff;->mTravelReport:Lgca;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lfff;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v2

    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    int-to-long v0, v0

    sub-long v0, v2, v0

    .line 81
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {p1, v0, v1, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 83
    const v1, 0x7f0a01e2

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-string v0, "event"

    return-object v0
.end method

.method public final ayW()I
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lfff;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lfff;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    goto :goto_0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 90
    const v0, 0x7f0202d9

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lfgb;->cqF:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2}, Lfff;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;
    .locals 6

    .prologue
    .line 50
    invoke-static {p1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lfff;->cqc:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    .line 53
    new-instance v2, Lffj;

    iget-object v3, p0, Lfff;->mDirectionsLauncher:Lgah;

    iget-object v4, p0, Lfff;->cpU:Ljbp;

    iget-object v4, p0, Lfff;->cqc:Ljal;

    iget-object v4, v4, Ljal;->dMF:[Liyg;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-direct {v2, v0, v3, v1, v4}, Lffj;-><init>(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Lgah;Ljbp;Liyg;)V

    .line 55
    invoke-static {v2}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0
.end method
