.class public final Lzi;
.super Landroid/content/BroadcastReceiver;
.source "PG"

# interfaces
.implements Laho;


# static fields
.field private static final MO:Landroid/os/HandlerThread;

.field private static final MP:Landroid/os/Handler;

.field static final MS:Ljava/util/ArrayList;

.field static final MV:Ljava/lang/Object;

.field static final MW:Ljava/util/HashMap;

.field static final MX:Ljava/util/ArrayList;

.field static final MY:Ljava/util/ArrayList;

.field static final MZ:Ljava/util/HashMap;

.field static final Na:Ljava/util/HashMap;

.field static final Nb:Ljava/util/ArrayList;

.field static final Nc:Ljava/util/HashMap;


# instance fields
.field private final Jf:Laia;

.field private final Jg:Lahn;

.field private final MH:Z

.field private final MI:Z

.field private final MJ:Lyu;

.field private MK:Ltj;

.field private ML:Laah;

.field private MM:Z

.field private volatile MN:Z

.field private MQ:Z

.field private MR:Z

.field private MT:Ljava/lang/ref/WeakReference;

.field MU:Lrp;

.field private Nd:I

.field private final dK:Ljava/lang/Object;

.field private xn:Lwi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "launcher-loader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 122
    sput-object v0, Lzi;->MO:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 124
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lzi;->MO:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lzi;->MP:Landroid/os/Handler;

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lzi;->MS:Ljava/util/ArrayList;

    .line 147
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lzi;->MV:Ljava/lang/Object;

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lzi;->MW:Ljava/util/HashMap;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lzi;->MX:Ljava/util/ArrayList;

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lzi;->MY:Ljava/util/ArrayList;

    .line 163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lzi;->MZ:Ljava/util/HashMap;

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lzi;->Na:Ljava/util/HashMap;

    .line 169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lzi;->Nb:Ljava/util/ArrayList;

    .line 172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lzi;->Nc:Ljava/util/HashMap;

    .line 3666
    new-instance v0, Lzt;

    invoke-direct {v0}, Lzt;-><init>()V

    return-void
.end method

.method constructor <init>(Lyu;Lwi;Lrq;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 217
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 107
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lzi;->dK:Ljava/lang/Object;

    .line 108
    new-instance v1, Ltj;

    invoke-direct {v1}, Ltj;-><init>()V

    iput-object v1, p0, Lzi;->MK:Ltj;

    .line 218
    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    .line 220
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v2

    iput-boolean v2, p0, Lzi;->MH:Z

    .line 221
    const v2, 0x7f0a007e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 224
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    .line 225
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.android.launcher2.settings"

    invoke-virtual {v4, v5, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 227
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 230
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Old launcher provider: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lzi;->MI:Z

    .line 233
    iget-boolean v0, p0, Lzi;->MI:Z

    .line 234
    iput-object p1, p0, Lzi;->MJ:Lyu;

    .line 240
    new-instance v0, Lrp;

    invoke-direct {v0, p2, p3}, Lrp;-><init>(Lwi;Lrq;)V

    iput-object v0, p0, Lzi;->MU:Lrp;

    .line 241
    iput-object p2, p0, Lzi;->xn:Lwi;

    .line 243
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 245
    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    iput v0, p0, Lzi;->Nd:I

    .line 246
    invoke-static {v1}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v0

    iput-object v0, p0, Lzi;->Jg:Lahn;

    .line 247
    invoke-static {v1}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v0

    iput-object v0, p0, Lzi;->Jf:Laia;

    .line 248
    return-void
.end method

.method static synthetic a(Lzi;Laah;)Laah;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lzi;->ML:Laah;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;Landroid/content/Context;IIIII)Ladh;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 3432
    .line 3433
    new-instance v1, Ladh;

    invoke-direct {v1}, Ladh;-><init>()V

    .line 3435
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v2

    iput-object v2, v1, Ladh;->Jl:Lahz;

    .line 3436
    iput v6, v1, Ladh;->Jz:I

    .line 3440
    invoke-interface {p1, p7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3442
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 3443
    packed-switch v2, :pswitch_data_0

    .line 3481
    iget-object v0, p0, Lzi;->xn:Lwi;

    iget-object v2, v1, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v2}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3482
    iput-boolean v6, v1, Ladh;->SU:Z

    .line 3483
    iput-boolean v5, v1, Ladh;->ST:Z

    .line 3486
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Ladh;->h(Landroid/graphics/Bitmap;)V

    .line 3487
    return-object v1

    .line 3445
    :pswitch_0
    invoke-interface {p1, p4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3446
    invoke-interface {p1, p5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 3447
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 3448
    iput-boolean v5, v1, Ladh;->ST:Z

    .line 3451
    :try_start_0
    invoke-virtual {v4, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 3452
    if-eqz v2, :cond_1

    .line 3453
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 3454
    iget-object v4, p0, Lzi;->xn:Lwi;

    invoke-virtual {v4, v2, v3}, Lwi;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2, p2}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3461
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 3462
    invoke-static {p1, p6, p2}, Lzi;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3465
    :cond_2
    if-nez v0, :cond_0

    .line 3466
    iget-object v0, p0, Lzi;->xn:Lwi;

    iget-object v2, v1, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v2}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3467
    iput-boolean v6, v1, Ladh;->SU:Z

    goto :goto_0

    .line 3471
    :pswitch_1
    invoke-static {p1, p6, p2}, Lzi;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3472
    if-nez v0, :cond_3

    .line 3473
    iget-object v0, p0, Lzi;->xn:Lwi;

    iget-object v2, v1, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v2}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3474
    iput-boolean v5, v1, Ladh;->ST:Z

    .line 3475
    iput-boolean v6, v1, Ladh;->SU:Z

    goto :goto_0

    .line 3477
    :cond_3
    iput-boolean v6, v1, Ladh;->ST:Z

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1

    .line 3443
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lzi;Landroid/database/Cursor;Landroid/content/Context;IIIII)Ladh;
    .locals 1

    .prologue
    .line 85
    invoke-direct/range {p0 .. p7}, Lzi;->a(Landroid/database/Cursor;Landroid/content/Context;IIIII)Ladh;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;
    .locals 3

    .prologue
    .line 3522
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v0

    .line 3524
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 3525
    iget-object v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3529
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lzi;Landroid/database/Cursor;Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lzi;->v(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 3492
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 3499
    const/4 v1, 0x0

    :try_start_0
    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, p2}, Ladp;->a(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3502
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/util/Pair;
    .locals 22

    .prologue
    .line 312
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v12

    .line 313
    iget-object v2, v12, Lyu;->Kr:Lzi;

    .line 314
    monitor-enter v12

    .line 316
    :try_start_0
    sget-object v3, Lzi;->MO:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v3

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v4

    if-eq v3, v4, :cond_2

    .line 320
    const/4 v3, 0x1

    iput-boolean v3, v2, Lzi;->MN:Z

    new-instance v3, Laad;

    invoke-direct {v3, v2}, Laad;-><init>(Lzi;)V

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {v3}, Lzi;->e(Ljava/lang/Runnable;)V

    iget-object v4, v2, Lzi;->ML:Laah;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lzi;->ML:Laah;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, v2, Lzi;->ML:Laah;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v4

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v3

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 336
    :catchall_2
    move-exception v2

    monitor-exit v12

    throw v2

    .line 320
    :cond_1
    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 322
    :cond_2
    :try_start_7
    invoke-static/range {p0 .. p0}, Lzi;->q(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v13

    .line 326
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 327
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    move v8, v2

    .line 328
    :goto_1
    if-ge v8, v14, :cond_7

    .line 329
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 330
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v3

    iget-object v3, v3, Lyu;->Mk:Lur;

    invoke-virtual {v3}, Lur;->gl()Ltu;

    move-result-object v3

    iget v4, v3, Ltu;->Dh:F

    float-to-int v5, v4

    iget v3, v3, Ltu;->Dg:F

    float-to-int v6, v3

    filled-new-array {v5, v6}, [I

    move-result-object v3

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [[Z

    const/4 v3, 0x0

    move v11, v3

    :goto_2
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v11, v3, :cond_5

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwq;

    iget-wide v0, v3, Lwq;->JA:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x64

    cmp-long v4, v18, v20

    if-nez v4, :cond_4

    iget-wide v0, v3, Lwq;->Bd:J

    move-wide/from16 v18, v0

    cmp-long v4, v18, v16

    if-nez v4, :cond_4

    iget v10, v3, Lwq;->Bb:I

    iget v4, v3, Lwq;->Bc:I

    iget v15, v3, Lwq;->AY:I

    iget v0, v3, Lwq;->AZ:I

    move/from16 v18, v0

    move v9, v10

    :goto_3
    if-ltz v9, :cond_4

    add-int v3, v10, v15

    if-ge v9, v3, :cond_4

    if-ge v9, v5, :cond_4

    move v3, v4

    :goto_4
    if-ltz v3, :cond_3

    add-int v19, v4, v18

    move/from16 v0, v19

    if-ge v3, v0, :cond_3

    if-ge v3, v6, :cond_3

    aget-object v19, v7, v9

    const/16 v20, 0x1

    aput-boolean v20, v19, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v3, v9, 0x1

    move v9, v3

    goto :goto_3

    :cond_4
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto :goto_2

    :cond_5
    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-static/range {v2 .. v7}, Lcom/android/launcher3/CellLayout;->a([IIIII[[Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 333
    new-instance v3, Landroid/util/Pair;

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object v2, v3

    .line 337
    :goto_5
    return-object v2

    .line 328
    :cond_6
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_1

    .line 336
    :cond_7
    monitor-exit v12

    .line 337
    const/4 v2, 0x0

    goto :goto_5

    .line 320
    :catch_0
    move-exception v4

    goto/16 :goto_0
.end method

.method static synthetic a(Lzi;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Laag;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3363
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 3364
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 3365
    instance-of v1, v0, Ladh;

    if-eqz v1, :cond_1

    .line 3366
    check-cast v0, Ladh;

    .line 3367
    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v1

    .line 3368
    if-eqz v1, :cond_0

    invoke-interface {p1, v6, v0, v1}, Laag;->a(Lwq;Lwq;Landroid/content/ComponentName;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3369
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3371
    :cond_1
    instance-of v1, v0, Lvy;

    if-eqz v1, :cond_3

    .line 3372
    check-cast v0, Lvy;

    .line 3373
    iget-object v1, v0, Lvy;->IA:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ladh;

    .line 3374
    invoke-virtual {v1}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v5

    .line 3375
    if-eqz v5, :cond_2

    invoke-interface {p1, v0, v1, v5}, Laag;->a(Lwq;Lwq;Landroid/content/ComponentName;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3376
    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3379
    :cond_3
    instance-of v1, v0, Lyy;

    if-eqz v1, :cond_0

    .line 3380
    check-cast v0, Lyy;

    .line 3381
    iget-object v1, v0, Lyy;->Mp:Landroid/content/ComponentName;

    .line 3382
    if-eqz v1, :cond_0

    invoke-interface {p1, v6, v0, v1}, Laag;->a(Lwq;Lwq;Landroid/content/ComponentName;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3383
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3387
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method static synthetic a(Lzi;Landroid/content/ComponentName;Lahz;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lzr;

    invoke-direct {v0, p0, p1, p2}, Lzr;-><init>(Lzi;Landroid/content/ComponentName;Lahz;)V

    sget-object v1, Lzi;->MW:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1, v0}, Lzi;->a(Ljava/util/Collection;Laag;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/HashMap;J)Lvy;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 968
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 969
    sget-object v1, Labn;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_id=? and (itemType=? or itemType=?)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "2"

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 975
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 976
    const-string v0, "itemType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 977
    const-string v3, "title"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 978
    const-string v4, "container"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 979
    const-string v5, "screen"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 980
    const-string v6, "cellX"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 981
    const-string v7, "cellY"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 984
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 990
    :goto_0
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lvy;->title:Ljava/lang/CharSequence;

    .line 991
    iput-wide p2, v2, Lvy;->id:J

    .line 992
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v8, v0

    iput-wide v8, v2, Lvy;->JA:J

    .line 993
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, v2, Lvy;->Bd:J

    .line 994
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lvy;->Bb:I

    .line 995
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lvy;->Bc:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1000
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1003
    :goto_1
    return-object v2

    .line 986
    :pswitch_0
    :try_start_1
    invoke-static {p1, p2, p3}, Lzi;->a(Ljava/util/HashMap;J)Lvy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 1000
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 984
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/util/HashMap;J)Lvy;
    .locals 3

    .prologue
    .line 3638
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    .line 3639
    if-nez v0, :cond_0

    .line 3641
    new-instance v0, Lvy;

    invoke-direct {v0}, Lvy;-><init>()V

    .line 3642
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3644
    :cond_0
    return-object v0
.end method

.method static a(JLwq;[Ljava/lang/StackTraceElement;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 596
    sget-object v0, Lzi;->MW:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 597
    if-eqz v0, :cond_1

    if-eq p2, v0, :cond_1

    .line 599
    instance-of v1, v0, Ladh;

    if-eqz v1, :cond_2

    instance-of v1, p2, Ladh;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 600
    check-cast v1, Ladh;

    move-object v2, p2

    .line 601
    check-cast v2, Ladh;

    .line 602
    iget-object v3, v1, Ladh;->title:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Ladh;->title:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Ladh;->intent:Landroid/content/Intent;

    iget-object v4, v2, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, v1, Ladh;->id:J

    iget-wide v6, v2, Ladh;->id:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget v3, v1, Ladh;->Jz:I

    iget v4, v2, Ladh;->Jz:I

    if-ne v3, v4, :cond_2

    iget-wide v4, v1, Ladh;->JA:J

    iget-wide v6, v2, Ladh;->JA:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, v1, Ladh;->Bd:J

    iget-wide v6, v2, Ladh;->Bd:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget v3, v1, Ladh;->Bb:I

    iget v4, v2, Ladh;->Bb:I

    if-ne v3, v4, :cond_2

    iget v3, v1, Ladh;->Bc:I

    iget v4, v2, Ladh;->Bc:I

    if-ne v3, v4, :cond_2

    iget v3, v1, Ladh;->AY:I

    iget v4, v2, Ladh;->AY:I

    if-ne v3, v4, :cond_2

    iget v3, v1, Ladh;->AZ:I

    iget v4, v2, Ladh;->AZ:I

    if-ne v3, v4, :cond_2

    iget-object v3, v1, Ladh;->JE:[I

    if-nez v3, :cond_0

    iget-object v3, v2, Ladh;->JE:[I

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, v1, Ladh;->JE:[I

    if-eqz v3, :cond_2

    iget-object v3, v2, Ladh;->JE:[I

    if-eqz v3, :cond_2

    iget-object v3, v1, Ladh;->JE:[I

    aget v3, v3, v8

    iget-object v4, v2, Ladh;->JE:[I

    aget v4, v4, v8

    if-ne v3, v4, :cond_2

    iget-object v1, v1, Ladh;->JE:[I

    aget v1, v1, v9

    iget-object v2, v2, Ladh;->JE:[I

    aget v2, v2, v9

    if-ne v1, v2, :cond_2

    .line 635
    :cond_1
    return-void

    .line 625
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "item: "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lwq;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "modelItem: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lwq;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Error: ItemInfo passed to checkItemInfo doesn\'t match original"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 629
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 630
    if-eqz p3, :cond_3

    .line 631
    invoke-virtual {v1, p3}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 633
    :cond_3
    throw v1

    .line 625
    :cond_4
    const-string v1, "null"

    goto :goto_0

    :cond_5
    const-string v0, "null"

    goto :goto_1
.end method

.method static a(Laau;)V
    .locals 1

    .prologue
    .line 2923
    sget-object v0, Lzi;->MP:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2924
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/ContentValues;Lwq;)V
    .locals 9

    .prologue
    .line 652
    iget-wide v6, p2, Lwq;->id:J

    .line 653
    const/4 v0, 0x0

    invoke-static {v6, v7, v0}, Labn;->a(JZ)Landroid/net/Uri;

    move-result-object v3

    .line 654
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 656
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v8

    .line 657
    new-instance v1, Laab;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v8}, Laab;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Lwq;J[Ljava/lang/StackTraceElement;)V

    .line 663
    invoke-static {v1}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 664
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lahz;)V
    .locals 1

    .prologue
    .line 1095
    invoke-static {p1, p2}, Lzi;->c(Ljava/lang/String;Lahz;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, v0}, Lzi;->c(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 1096
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;JI)V
    .locals 8

    .prologue
    .line 800
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 801
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 803
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 804
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 805
    iput-wide p2, v0, Lwq;->JA:J

    .line 809
    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lwq;->Bd:J

    .line 817
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 818
    const-string v5, "container"

    iget-wide v6, v0, Lwq;->JA:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 819
    const-string v5, "cellX"

    iget v6, v0, Lwq;->Bb:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 820
    const-string v5, "cellY"

    iget v6, v0, Lwq;->Bc:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 821
    const-string v5, "screen"

    iget-wide v6, v0, Lwq;->Bd:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 823
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 803
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 825
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    new-instance v3, Laac;

    invoke-direct {v3, p1, v2, v1, v0}, Laac;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;[Ljava/lang/StackTraceElement;Landroid/content/ContentResolver;)V

    invoke-static {v3}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 826
    return-void
.end method

.method public static a(Landroid/content/Context;Lvy;)V
    .locals 2

    .prologue
    .line 1212
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1214
    new-instance v1, Lzo;

    invoke-direct {v1, v0, p1}, Lzo;-><init>(Landroid/content/ContentResolver;Lvy;)V

    .line 1236
    invoke-static {v1}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 1237
    return-void
.end method

.method public static a(Landroid/content/Context;Lwq;)V
    .locals 3

    .prologue
    .line 863
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 864
    invoke-virtual {p1, p0, v0}, Lwq;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 865
    iget v1, p1, Lwq;->Bb:I

    iget v2, p1, Lwq;->Bc:I

    invoke-static {v0, v1, v2}, Lwq;->a(Landroid/content/ContentValues;II)V

    .line 866
    invoke-static {p0, v0, p1}, Lzi;->a(Landroid/content/Context;Landroid/content/ContentValues;Lwq;)V

    .line 867
    return-void
.end method

.method public static a(Landroid/content/Context;Lwq;JJII)V
    .locals 10

    .prologue
    .line 585
    iget-wide v0, p1, Lwq;->JA:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 587
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lzi;->a(Landroid/content/Context;Lwq;JJIIZ)V

    .line 592
    :goto_0
    return-void

    .line 590
    :cond_0
    invoke-static/range {p0 .. p7}, Lzi;->b(Landroid/content/Context;Lwq;JJII)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lwq;JJIIII)V
    .locals 4

    .prologue
    .line 833
    iput-wide p2, p1, Lwq;->JA:J

    .line 834
    iput p6, p1, Lwq;->Bb:I

    .line 835
    iput p7, p1, Lwq;->Bc:I

    .line 836
    iput p8, p1, Lwq;->AY:I

    .line 837
    iput p9, p1, Lwq;->AZ:I

    .line 841
    instance-of v0, p0, Lcom/android/launcher3/Launcher;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gez v0, :cond_0

    const-wide/16 v0, -0x65

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 843
    check-cast v0, Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Lcom/android/launcher3/Hotseat;->K(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p1, Lwq;->Bd:J

    .line 848
    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 849
    const-string v1, "container"

    iget-wide v2, p1, Lwq;->JA:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 850
    const-string v1, "cellX"

    iget v2, p1, Lwq;->Bb:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 851
    const-string v1, "cellY"

    iget v2, p1, Lwq;->Bc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 852
    const-string v1, "spanX"

    iget v2, p1, Lwq;->AY:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 853
    const-string v1, "spanY"

    iget v2, p1, Lwq;->AZ:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 854
    const-string v1, "screen"

    iget-wide v2, p1, Lwq;->Bd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 856
    invoke-static {p0, v0, p1}, Lzi;->a(Landroid/content/Context;Landroid/content/ContentValues;Lwq;)V

    .line 857
    return-void

    .line 845
    :cond_0
    iput-wide p4, p1, Lwq;->Bd:J

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lwq;JJIIZ)V
    .locals 6

    .prologue
    .line 1012
    iput-wide p2, p1, Lwq;->JA:J

    .line 1013
    iput p6, p1, Lwq;->Bb:I

    .line 1014
    iput p7, p1, Lwq;->Bc:I

    .line 1017
    instance-of v0, p0, Lcom/android/launcher3/Launcher;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gez v0, :cond_0

    const-wide/16 v0, -0x65

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 1019
    check-cast v0, Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Lcom/android/launcher3/Hotseat;->K(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p1, Lwq;->Bd:J

    .line 1024
    :goto_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1025
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1026
    invoke-virtual {p1, p0, v3}, Lwq;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 1028
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/LauncherProvider;->eE()J

    move-result-wide v4

    iput-wide v4, p1, Lwq;->id:J

    .line 1029
    const-string v0, "_id"

    iget-wide v4, p1, Lwq;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1030
    iget v0, p1, Lwq;->Bb:I

    iget v2, p1, Lwq;->Bc:I

    invoke-static {v3, v0, v2}, Lwq;->a(Landroid/content/ContentValues;II)V

    .line 1032
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    .line 1033
    new-instance v0, Lzk;

    const/4 v2, 0x0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lzk;-><init>(Landroid/content/ContentResolver;ZLandroid/content/ContentValues;Lwq;[Ljava/lang/StackTraceElement;)V

    .line 1067
    invoke-static {v0}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 1068
    return-void

    .line 1021
    :cond_0
    iput-wide p4, p1, Lwq;->Bd:J

    goto :goto_0
.end method

.method static a(Lwq;J[Ljava/lang/StackTraceElement;)V
    .locals 9

    .prologue
    const-wide/16 v6, -0x64

    const-wide/16 v4, -0x65

    .line 698
    sget-object v1, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v1

    .line 699
    :try_start_0
    invoke-static {p1, p2, p0, p3}, Lzi;->a(JLwq;[Ljava/lang/StackTraceElement;)V

    .line 701
    iget-wide v2, p0, Lwq;->JA:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lwq;->JA:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 704
    sget-object v0, Lzi;->MZ:Ljava/util/HashMap;

    iget-wide v2, p0, Lwq;->JA:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 707
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "item: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " container being set to: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lwq;->JA:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", not in the list of folders"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 709
    const-string v2, "Launcher.Model"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    :cond_0
    sget-object v0, Lzi;->MW:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 717
    if-eqz v0, :cond_3

    iget-wide v2, v0, Lwq;->JA:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    iget-wide v2, v0, Lwq;->JA:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 720
    :cond_1
    iget v2, v0, Lwq;->Jz:I

    packed-switch v2, :pswitch_data_0

    .line 734
    :cond_2
    :goto_0
    monitor-exit v1

    return-void

    .line 724
    :pswitch_0
    sget-object v2, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 725
    sget-object v2, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 734
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 732
    :cond_3
    :try_start_1
    sget-object v2, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 720
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lzi;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lzi;->d(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lzi;Ljava/lang/Runnable;I)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lzi;->d(Ljava/lang/Runnable;)V

    return-void
.end method

.method static a(Landroid/appwidget/AppWidgetProviderInfo;)Z
    .locals 1

    .prologue
    .line 3748
    if-eqz p0, :cond_0

    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;Lahz;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3205
    if-nez p1, :cond_1

    .line 3212
    :cond_0
    :goto_0
    return v0

    .line 3208
    :cond_1
    invoke-static {p0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v1

    .line 3209
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3212
    invoke-virtual {v1, p1, p2}, Lahn;->e(Landroid/content/ComponentName;Lahz;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 874
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 877
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 880
    invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 882
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object v6, v1

    .line 892
    :goto_0
    sget-object v1, Labn;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "title"

    aput-object v3, v2, v8

    const-string v3, "intent"

    aput-object v3, v2, v9

    const-string v3, "title=? and (intent=? or intent=?)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    invoke-virtual {p2, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v9

    invoke-virtual {v6, v8}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v10

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 895
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 899
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 901
    return v1

    .line 884
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object v6, p2

    move-object p2, v1

    .line 886
    goto :goto_0

    :cond_1
    move-object v6, p2

    .line 890
    goto :goto_0

    .line 899
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method static synthetic a(Lzi;Z)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzi;->MN:Z

    return v0
.end method

.method static synthetic b(Ljava/util/HashMap;J)Lvy;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0, p1, p2}, Lzi;->a(Ljava/util/HashMap;J)Lvy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lzi;)Lwi;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->xn:Lwi;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lwq;)V
    .locals 1

    .prologue
    .line 1104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1105
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1106
    invoke-static {p0, v0}, Lzi;->c(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 1107
    return-void
.end method

.method public static b(Landroid/content/Context;Lwq;JJII)V
    .locals 4

    .prologue
    .line 771
    iput-wide p2, p1, Lwq;->JA:J

    .line 772
    iput p6, p1, Lwq;->Bb:I

    .line 773
    iput p7, p1, Lwq;->Bc:I

    .line 777
    instance-of v0, p0, Lcom/android/launcher3/Launcher;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gez v0, :cond_0

    const-wide/16 v0, -0x65

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 779
    check-cast v0, Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Lcom/android/launcher3/Hotseat;->K(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p1, Lwq;->Bd:J

    .line 784
    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 785
    const-string v1, "container"

    iget-wide v2, p1, Lwq;->JA:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 786
    const-string v1, "cellX"

    iget v2, p1, Lwq;->Bb:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 787
    const-string v1, "cellY"

    iget v2, p1, Lwq;->Bc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 788
    const-string v1, "screen"

    iget-wide v2, p1, Lwq;->Bd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 790
    invoke-static {p0, v0, p1}, Lzi;->a(Landroid/content/Context;Landroid/content/ContentValues;Lwq;)V

    .line 791
    return-void

    .line 781
    :cond_0
    iput-wide p4, p1, Lwq;->Bd:J

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Lahz;)Z
    .locals 1

    .prologue
    .line 3217
    if-nez p1, :cond_0

    .line 3218
    const/4 v0, 0x0

    .line 3221
    :goto_0
    return v0

    .line 3220
    :cond_0
    invoke-static {p0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v0

    .line 3221
    invoke-virtual {v0, p1, p2}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v0

    goto :goto_0
.end method

.method static b(Landroid/content/Intent;Lahz;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 908
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 909
    if-nez v1, :cond_1

    .line 912
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lzi;->c(Ljava/lang/String;Lahz;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lzi;Z)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lzi;->MQ:Z

    return v0
.end method

.method private static c(Ljava/lang/String;Lahz;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 1081
    new-instance v0, Lzl;

    invoke-direct {v0, p0, p1}, Lzl;-><init>(Ljava/lang/String;Lahz;)V

    .line 1087
    sget-object v1, Lzi;->MW:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1, v0}, Lzi;->a(Ljava/util/Collection;Laag;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static c(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 1115
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1117
    new-instance v1, Lzm;

    invoke-direct {v1, p1, v0}, Lzm;-><init>(Ljava/util/ArrayList;Landroid/content/ContentResolver;)V

    .line 1153
    invoke-static {v1}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 1154
    return-void
.end method

.method static synthetic c(Landroid/content/Context;Ljava/lang/String;Lahz;)Z
    .locals 1

    .prologue
    .line 85
    invoke-static {p0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lzi;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lzi;->MQ:Z

    return v0
.end method

.method static synthetic c(Lzi;Z)Z
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lzi;->MM:Z

    return p1
.end method

.method static synthetic d(Lzi;)Ltj;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->MK:Ltj;

    return-object v0
.end method

.method private d(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 256
    sget-object v0, Lzi;->MO:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 258
    iget-object v0, p0, Lzi;->MK:Ltj;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ltj;->a(Ljava/lang/Runnable;I)V

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method static synthetic d(Lzi;Z)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lzi;->MR:Z

    return v0
.end method

.method private static e(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lzi;->MO:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 268
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    sget-object v0, Lzi;->MP:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public static e(Lwq;)V
    .locals 4

    .prologue
    .line 638
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 639
    iget-wide v2, p0, Lwq;->id:J

    .line 640
    new-instance v1, Laaa;

    invoke-direct {v1, v2, v3, p0, v0}, Laaa;-><init>(JLwq;[Ljava/lang/StackTraceElement;)V

    .line 647
    invoke-static {v1}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 648
    return-void
.end method

.method static synthetic e(Lzi;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lzi;->MN:Z

    return v0
.end method

.method public static f(Lwq;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 3406
    instance-of v1, p0, Ladh;

    if-eqz v1, :cond_2

    .line 3407
    check-cast p0, Ladh;

    .line 3411
    iget-object v1, p0, Ladh;->intent:Landroid/content/Intent;

    .line 3412
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    .line 3413
    iget v3, p0, Ladh;->Jz:I

    if-nez v3, :cond_1

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 3422
    :cond_0
    :goto_0
    return v0

    .line 3418
    :cond_1
    invoke-virtual {p0}, Ladh;->kg()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3422
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lzi;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lzi;->MR:Z

    return v0
.end method

.method static synthetic g(Lzi;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->dK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic h(Lzi;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lzi;->MM:Z

    return v0
.end method

.method static synthetic i(Lzi;)Laah;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->ML:Laah;

    return-object v0
.end method

.method public static final iB()Ljava/util/Comparator;
    .locals 2

    .prologue
    .line 3648
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 3649
    new-instance v1, Lzs;

    invoke-direct {v1, v0}, Lzs;-><init>(Ljava/text/Collator;)V

    return-object v1
.end method

.method static synthetic iC()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lzi;->MP:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic iD()Landroid/os/HandlerThread;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lzi;->MO:Landroid/os/HandlerThread;

    return-object v0
.end method

.method private ix()Z
    .locals 3

    .prologue
    .line 1378
    const/4 v0, 0x0

    .line 1379
    iget-object v1, p0, Lzi;->ML:Laah;

    .line 1380
    if-eqz v1, :cond_1

    .line 1381
    invoke-virtual {v1}, Laah;->iE()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1382
    const/4 v0, 0x1

    .line 1384
    :cond_0
    invoke-virtual {v1}, Laah;->iG()V

    .line 1386
    :cond_1
    return v0
.end method

.method static synthetic j(Lzi;)Lyu;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->MJ:Lyu;

    return-object v0
.end method

.method static synthetic k(Lzi;)Laia;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->Jf:Laia;

    return-object v0
.end method

.method static synthetic l(Lzi;)Lahn;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lzi;->Jg:Lahn;

    return-object v0
.end method

.method private static q(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 920
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 921
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 922
    sget-object v1, Labn;->CONTENT_URI:Landroid/net/Uri;

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "itemType"

    aput-object v5, v2, v4

    const-string v4, "container"

    aput-object v4, v2, v7

    const/4 v4, 0x2

    const-string v5, "screen"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "cellX"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "cellY"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "spanX"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "spanY"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "profileId"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 929
    const-string v0, "itemType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 930
    const-string v2, "container"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 931
    const-string v3, "screen"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 932
    const-string v4, "cellX"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 933
    const-string v5, "cellY"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 934
    const-string v7, "spanX"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 935
    const-string v8, "spanY"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 936
    const-string v9, "profileId"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 937
    invoke-static {p0}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v10

    .line 939
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 940
    new-instance v11, Lwq;

    invoke-direct {v11}, Lwq;-><init>()V

    .line 941
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    iput v12, v11, Lwq;->Bb:I

    .line 942
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    iput v12, v11, Lwq;->Bc:I

    .line 943
    const/4 v12, 0x1

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v12

    iput v12, v11, Lwq;->AY:I

    .line 944
    const/4 v12, 0x1

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v12

    iput v12, v11, Lwq;->AZ:I

    .line 945
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    int-to-long v12, v12

    iput-wide v12, v11, Lwq;->JA:J

    .line 946
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    iput v12, v11, Lwq;->Jz:I

    .line 947
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    int-to-long v12, v12

    iput-wide v12, v11, Lwq;->Bd:J

    .line 948
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    int-to-long v12, v12

    .line 949
    invoke-virtual {v10, v12, v13}, Laia;->m(J)Lahz;

    move-result-object v12

    iput-object v12, v11, Lwq;->Jl:Lahz;

    .line 951
    iget-object v12, v11, Lwq;->Jl:Lahz;

    if-eqz v12, :cond_0

    .line 952
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 956
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 958
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 961
    :goto_1
    return-object v6

    .line 958
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static r(Landroid/content/Context;)Ljava/util/TreeMap;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1451
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1452
    sget-object v1, Labo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 1453
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1454
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 1457
    :try_start_0
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 1459
    const-string v0, "screenRank"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 1461
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1463
    :try_start_1
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1464
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1465
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1466
    :catch_0
    move-exception v0

    .line 1467
    :try_start_2
    const-string v5, "Launcher.Model"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Desktop items loading interrupted - invalid screens: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    invoke-static {v5, v0, v6}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1471
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1475
    const-string v0, "Launcher.Model"

    const-string v1, "11683562 - loadWorkspaceScreensDb()"

    invoke-static {v0, v1, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1476
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1477
    invoke-virtual {v2}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1478
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "{ "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " }"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1480
    :cond_1
    const-string v0, "Launcher.Model"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "11683562 -   screens: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ", "

    invoke-static {v4, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1482
    return-object v2
.end method

.method public static t(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 3187
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 3188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3189
    invoke-static {p0}, Lahh;->A(Landroid/content/Context;)Lahh;

    move-result-object v2

    invoke-virtual {v2}, Lahh;->getAllProviders()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3191
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3192
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3193
    new-instance v0, Laba;

    invoke-direct {v0, p0}, Laba;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3194
    return-object v1
.end method

.method static synthetic u(Landroid/content/Context;)Ljava/util/TreeMap;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0}, Lzi;->r(Landroid/content/Context;)Ljava/util/TreeMap;

    move-result-object v0

    return-object v0
.end method

.method public static v(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3266
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "market"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "details"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Ladh;
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3533
    const-string v1, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 3534
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3535
    const-string v2, "android.intent.extra.shortcut.ICON"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 3537
    if-nez v1, :cond_0

    .line 3539
    const-string v1, "Launcher.Model"

    const-string v2, "Can\'t construct ShorcutInfo with null intent"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3590
    :goto_0
    return-object v4

    .line 3544
    :cond_0
    const/4 v6, 0x0

    .line 3547
    if-eqz v2, :cond_2

    instance-of v3, v2, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    .line 3548
    new-instance v3, Lus;

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v3, v2}, Lus;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {v3, p1}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    move v3, v5

    move-object v12, v4

    move-object v4, v2

    move-object v2, v12

    .line 3568
    :goto_1
    new-instance v6, Ladh;

    invoke-direct {v6}, Ladh;-><init>()V

    .line 3572
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v8

    iput-object v8, v6, Ladh;->Jl:Lahz;

    .line 3573
    if-nez v4, :cond_1

    .line 3574
    iget-object v4, p0, Lzi;->xn:Lwi;

    iget-object v8, v6, Ladh;->Jl:Lahz;

    invoke-virtual {v4, v8}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 3578
    iput-boolean v5, v6, Ladh;->SU:Z

    .line 3581
    :cond_1
    invoke-virtual {v6, v4}, Ladh;->h(Landroid/graphics/Bitmap;)V

    .line 3583
    iput-object v7, v6, Ladh;->title:Ljava/lang/CharSequence;

    .line 3584
    iget-object v4, p0, Lzi;->Jf:Laia;

    iget-object v5, v6, Ladh;->title:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v6, Ladh;->Jl:Lahz;

    invoke-virtual {v4, v5, v7}, Laia;->a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, v6, Ladh;->Jk:Ljava/lang/CharSequence;

    .line 3586
    iput-object v1, v6, Ladh;->intent:Landroid/content/Intent;

    .line 3587
    iput-boolean v3, v6, Ladh;->ST:Z

    .line 3588
    iput-object v2, v6, Ladh;->Jr:Landroid/content/Intent$ShortcutIconResource;

    move-object v4, v6

    .line 3590
    goto :goto_0

    .line 3551
    :cond_2
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 3552
    if-eqz v3, :cond_3

    instance-of v2, v3, Landroid/content/Intent$ShortcutIconResource;

    if-eqz v2, :cond_3

    .line 3554
    :try_start_0
    move-object v0, v3

    check-cast v0, Landroid/content/Intent$ShortcutIconResource;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3555
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 3556
    iget-object v9, v2, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v8

    .line 3558
    iget-object v9, v2, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 3559
    iget-object v10, p0, Lzi;->xn:Lwi;

    invoke-virtual {v10, v8, v9}, Lwi;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-static {v8, p1}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    move v3, v6

    .line 3564
    goto :goto_1

    .line 3563
    :catch_0
    move-exception v2

    move-object v2, v4

    :goto_2
    const-string v8, "Launcher.Model"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Could not load shortcut icon: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v6

    goto :goto_1

    :catch_1
    move-exception v8

    goto :goto_2

    :cond_3
    move-object v2, v4

    move v3, v6

    goto :goto_1
.end method

.method public final a(Landroid/content/Intent;Lahz;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;Z)Ladh;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3291
    if-nez p2, :cond_0

    .line 3358
    :goto_0
    return-object v0

    .line 3296
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    .line 3297
    if-nez v2, :cond_1

    .line 3298
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Missing component found in getShortcutInfo: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3302
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 3303
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 3304
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 3305
    iget-object v3, p0, Lzi;->Jg:Lahn;

    invoke-virtual {v3, v1, p2}, Lahn;->c(Landroid/content/Intent;Lahz;)Lahk;

    move-result-object v3

    .line 3306
    if-nez v3, :cond_2

    if-nez p8, :cond_2

    .line 3307
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Missing activity found in getShortcutInfo: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3311
    :cond_2
    new-instance v1, Ladh;

    invoke-direct {v1}, Ladh;-><init>()V

    .line 3317
    iget-object v0, p0, Lzi;->xn:Lwi;

    invoke-virtual {v0, v2, v3, p7}, Lwi;->a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3320
    if-nez v0, :cond_3

    .line 3321
    if-eqz p4, :cond_3

    .line 3322
    invoke-static {p4, p5, p3}, Lzi;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3326
    :cond_3
    if-nez v0, :cond_4

    .line 3327
    iget-object v0, p0, Lzi;->xn:Lwi;

    invoke-virtual {v0, p2}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3328
    const/4 v4, 0x1

    iput-boolean v4, v1, Ladh;->SU:Z

    .line 3330
    :cond_4
    invoke-virtual {v1, v0}, Ladh;->h(Landroid/graphics/Bitmap;)V

    .line 3333
    if-eqz p7, :cond_5

    .line 3334
    invoke-virtual {p7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3338
    :cond_5
    iget-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    if-nez v0, :cond_6

    if-eqz v3, :cond_6

    .line 3339
    invoke-virtual {v3}, Lahk;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3340
    if-eqz p7, :cond_6

    .line 3341
    iget-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    invoke-virtual {p7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3345
    :cond_6
    iget-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    if-nez v0, :cond_7

    .line 3346
    if-eqz p4, :cond_7

    .line 3347
    invoke-interface {p4, p6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3351
    :cond_7
    iget-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    if-nez v0, :cond_8

    .line 3352
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3354
    :cond_8
    const/4 v0, 0x0

    iput v0, v1, Ladh;->Jz:I

    .line 3355
    iput-object p2, v1, Ladh;->Jl:Lahz;

    .line 3356
    iget-object v0, p0, Lzi;->Jf:Laia;

    iget-object v2, v1, Ladh;->title:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v2, v3}, Laia;->a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Ladh;->Jk:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 3358
    goto/16 :goto_0
.end method

.method public final a(Landroid/database/Cursor;ILandroid/content/Intent;I)Ladh;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3230
    new-instance v1, Ladh;

    invoke-direct {v1}, Ladh;-><init>()V

    .line 3231
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v0

    iput-object v0, v1, Ladh;->Jl:Lahz;

    .line 3232
    iget-object v0, p0, Lzi;->xn:Lwi;

    iget-object v2, v1, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v1, p3, v2, v4}, Lwi;->a(Ladh;Landroid/content/Intent;Lahz;Z)V

    .line 3234
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_2

    .line 3235
    if-eqz p1, :cond_1

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3236
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3237
    iput-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3239
    :cond_0
    iput v4, v1, Ladh;->status:I

    .line 3249
    :goto_1
    iget-object v0, p0, Lzi;->Jf:Laia;

    iget-object v2, v1, Ladh;->title:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v2, v3}, Laia;->a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Ladh;->Jk:Ljava/lang/CharSequence;

    .line 3251
    iput v4, v1, Ladh;->Jz:I

    .line 3252
    iput-object p3, v1, Ladh;->SX:Landroid/content/Intent;

    .line 3253
    return-object v1

    .line 3235
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 3240
    :cond_2
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_5

    .line 3241
    iget-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3242
    if-eqz p1, :cond_4

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, v1, Ladh;->title:Ljava/lang/CharSequence;

    .line 3244
    :cond_3
    const/4 v0, 0x2

    iput v0, v1, Ladh;->status:I

    goto :goto_1

    .line 3242
    :cond_4
    const-string v0, ""

    goto :goto_2

    .line 3246
    :cond_5
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid restoreType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Laaf;)V
    .locals 2

    .prologue
    .line 1243
    iget-object v1, p0, Lzi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 1244
    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    .line 1245
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Landroid/content/Context;Ladh;[B)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3612
    if-eqz p3, :cond_0

    .line 3615
    const/4 v2, 0x0

    :try_start_0
    array-length v3, p3

    invoke-static {p3, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 3616
    iget-object v3, p0, Lzi;->xn:Lwi;

    invoke-virtual {p2, v3}, Ladh;->b(Lwi;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 3617
    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_2

    .line 3624
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 3625
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "going to save icon bitmap for info="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3628
    invoke-static {p1, p2}, Lzi;->a(Landroid/content/Context;Lwq;)V

    .line 3630
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 3617
    goto :goto_0

    .line 3622
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 367
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    move-object v1, v0

    .line 369
    :goto_0
    if-nez p2, :cond_1

    .line 370
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "allAppsApps must not be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 372
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 409
    :goto_1
    return-void

    .line 376
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 377
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 378
    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 379
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 380
    invoke-virtual {v0}, Lwq;->getIntent()Landroid/content/Intent;

    move-result-object v4

    iget-object v5, v0, Lwq;->Jl:Lahz;

    invoke-static {v4, v5}, Lzi;->b(Landroid/content/Intent;Lahz;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 381
    check-cast v0, Lrr;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 386
    :cond_4
    new-instance v0, Lzv;

    invoke-direct {v0, p0, v1, v2, p2}, Lzv;-><init>(Lzi;Laaf;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 408
    invoke-static {v0}, Lzi;->e(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(ZI)V
    .locals 1

    .prologue
    .line 1394
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lzi;->a(ZII)V

    .line 1395
    return-void
.end method

.method public final a(ZII)V
    .locals 4

    .prologue
    .line 1398
    iget-object v1, p0, Lzi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 1405
    :try_start_0
    sget-object v2, Lzi;->MS:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1406
    :try_start_1
    sget-object v0, Lzi;->MS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1407
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1410
    :try_start_2
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1413
    if-nez p1, :cond_0

    invoke-direct {p0}, Lzi;->ix()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1414
    :goto_0
    new-instance v2, Laah;

    iget-object v3, p0, Lzi;->MJ:Lyu;

    sget-object v3, Lyu;->Mi:Landroid/content/Context;

    invoke-direct {v2, p0, v3, v0, p3}, Laah;-><init>(Lzi;Landroid/content/Context;ZI)V

    iput-object v2, p0, Lzi;->ML:Laah;

    .line 1415
    const/16 v0, -0x3e9

    if-eq p2, v0, :cond_3

    iget-boolean v0, p0, Lzi;->MR:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lzi;->MQ:Z

    if-eqz v0, :cond_3

    .line 1417
    iget-object v0, p0, Lzi;->ML:Laah;

    invoke-virtual {v0, p2}, Laah;->bo(I)V

    .line 1423
    :cond_1
    :goto_1
    monitor-exit v1

    return-void

    .line 1407
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1423
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1413
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1419
    :cond_3
    :try_start_3
    sget-object v0, Lzi;->MO:Landroid/os/HandlerThread;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/os/HandlerThread;->setPriority(I)V

    .line 1420
    sget-object v0, Lzi;->MP:Landroid/os/Handler;

    iget-object v2, p0, Lzi;->ML:Laah;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1
.end method

.method public final a([Ljava/lang/String;Lahz;Z)V
    .locals 2

    .prologue
    .line 1272
    if-nez p3, :cond_1

    .line 1273
    new-instance v0, Laau;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1, p2}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v0}, Lzi;->a(Laau;)V

    .line 1275
    iget-boolean v0, p0, Lzi;->MH:Z

    if-eqz v0, :cond_0

    .line 1279
    invoke-virtual {p0}, Lzi;->iw()V

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1283
    :cond_1
    new-instance v0, Laau;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1, p1, p2}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v0}, Lzi;->a(Laau;)V

    goto :goto_0
.end method

.method final a(Ljava/util/HashMap;Ladh;Landroid/database/Cursor;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3596
    iget-boolean v1, p0, Lzi;->MH:Z

    if-nez v1, :cond_1

    .line 3609
    :cond_0
    :goto_0
    return v0

    .line 3605
    :cond_1
    iget-boolean v1, p2, Ladh;->ST:Z

    if-nez v1, :cond_0

    iget-boolean v1, p2, Ladh;->SU:Z

    if-nez v1, :cond_0

    .line 3606
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3607
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    .line 415
    :goto_0
    if-nez p2, :cond_1

    .line 416
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "workspaceApps and allAppsApps must not be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 418
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 537
    :goto_1
    return-void

    .line 422
    :cond_2
    new-instance v1, Lzx;

    invoke-direct {v1, p0, p1, p2, v0}, Lzx;-><init>(Lzi;Landroid/content/Context;Ljava/util/ArrayList;Laaf;)V

    .line 536
    invoke-static {v1}, Lzi;->e(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final b(ZZ)V
    .locals 2

    .prologue
    .line 1344
    iget-object v1, p0, Lzi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 1347
    :try_start_0
    invoke-direct {p0}, Lzi;->ix()Z

    .line 1348
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lzi;->MR:Z

    .line 1349
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzi;->MQ:Z

    .line 1350
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b([Ljava/lang/String;Lahz;Z)V
    .locals 2

    .prologue
    .line 1291
    if-nez p3, :cond_0

    .line 1292
    new-instance v0, Laau;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1, p1, p2}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v0}, Lzi;->a(Laau;)V

    .line 1297
    :cond_0
    return-void
.end method

.method public final b(Laaf;)Z
    .locals 1

    .prologue
    .line 1390
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 1162
    const-string v0, "Launcher.Model"

    const-string v1, "11683562 - updateWorkspaceScreenOrder()"

    invoke-static {v0, v1, v3}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1163
    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "11683562 -   screens: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ", "

    invoke-static {v2, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1166
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1167
    sget-object v3, Labo;->CONTENT_URI:Landroid/net/Uri;

    .line 1170
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1171
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1172
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1173
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-gez v0, :cond_0

    .line 1174
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1178
    :cond_1
    new-instance v0, Lzn;

    invoke-direct {v0, p0, v3, v1, v2}, Lzn;-><init>(Lzi;Landroid/net/Uri;Ljava/util/ArrayList;Landroid/content/ContentResolver;)V

    .line 1205
    invoke-static {v0}, Lzi;->e(Ljava/lang/Runnable;)V

    .line 1206
    return-void
.end method

.method public final d(Ljava/lang/String;Lahz;)V
    .locals 4

    .prologue
    .line 1250
    new-instance v0, Laau;

    const/4 v1, 0x2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v0, p0, v1, v2, p2}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v0}, Lzi;->a(Laau;)V

    .line 1253
    return-void
.end method

.method public final e(Ljava/lang/String;Lahz;)V
    .locals 4

    .prologue
    .line 1257
    new-instance v0, Laau;

    const/4 v1, 0x3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v0, p0, v1, v2, p2}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v0}, Lzi;->a(Laau;)V

    .line 1260
    return-void
.end method

.method public final ev()V
    .locals 3

    .prologue
    .line 3753
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mCallbacks="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3754
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.data"

    iget-object v2, p0, Lzi;->MU:Lrp;

    iget-object v2, v2, Lrp;->xk:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lrr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3755
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.added"

    iget-object v2, p0, Lzi;->MU:Lrp;

    iget-object v2, v2, Lrp;->xl:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lrr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3756
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.removed"

    iget-object v2, p0, Lzi;->MU:Lrp;

    iget-object v2, v2, Lrp;->O:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lrr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3757
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.modified"

    iget-object v2, p0, Lzi;->MU:Lrp;

    iget-object v2, v2, Lrp;->xm:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lrr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 3758
    iget-object v0, p0, Lzi;->ML:Laah;

    if-eqz v0, :cond_0

    .line 3759
    iget-object v0, p0, Lzi;->ML:Laah;

    invoke-virtual {v0}, Laah;->ev()V

    .line 3761
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;Lahz;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1264
    new-instance v0, Laau;

    new-array v1, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {v0, p0, v3, v1, p2}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v0}, Lzi;->a(Laau;)V

    .line 1267
    return-void
.end method

.method public final iA()Z
    .locals 2

    .prologue
    .line 1490
    iget-object v1, p0, Lzi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 1491
    :try_start_0
    iget-object v0, p0, Lzi;->ML:Laah;

    if-eqz v0, :cond_0

    .line 1492
    iget-object v0, p0, Lzi;->ML:Laah;

    invoke-virtual {v0}, Laah;->iA()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1495
    :goto_0
    return v0

    .line 1494
    :cond_0
    monitor-exit v1

    .line 1495
    const/4 v0, 0x0

    goto :goto_0

    .line 1494
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final it()V
    .locals 2

    .prologue
    .line 540
    sget-object v0, Lzi;->MO:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 541
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected unbindLauncherItemInfos() to be called from the main thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 546
    :cond_0
    sget-object v1, Lzi;->MS:Ljava/util/ArrayList;

    monitor-enter v1

    .line 547
    :try_start_0
    sget-object v0, Lzi;->MS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 548
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550
    iget-object v0, p0, Lzi;->MK:Ltj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltj;->aY(I)V

    .line 552
    invoke-virtual {p0}, Lzi;->iu()V

    .line 553
    return-void

    .line 548
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final iu()V
    .locals 4

    .prologue
    .line 559
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 560
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 561
    sget-object v2, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v2

    .line 562
    :try_start_0
    sget-object v3, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 563
    sget-object v3, Lzi;->MY:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 564
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565
    new-instance v2, Lzz;

    invoke-direct {v2, p0, v0, v1}, Lzz;-><init>(Lzi;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 576
    invoke-direct {p0, v2}, Lzi;->d(Ljava/lang/Runnable;)V

    .line 577
    return-void

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method final iv()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1335
    invoke-virtual {p0, v0, v0}, Lzi;->b(ZZ)V

    .line 1340
    invoke-virtual {p0}, Lzi;->iw()V

    .line 1341
    return-void
.end method

.method public final iw()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1360
    .line 1361
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 1362
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    .line 1363
    if-eqz v0, :cond_1

    .line 1365
    invoke-interface {v0}, Laaf;->hO()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1366
    const/4 v0, 0x1

    .line 1370
    :goto_0
    if-eqz v0, :cond_0

    .line 1371
    const/16 v0, -0x3e9

    invoke-virtual {p0, v1, v0, v1}, Lzi;->a(ZII)V

    .line 1373
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method final iy()V
    .locals 6

    .prologue
    .line 1428
    sget-object v0, Lzi;->MS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1429
    sget-object v1, Lzi;->MS:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1431
    :try_start_0
    sget-object v0, Lzi;->MS:Ljava/util/ArrayList;

    sget-object v2, Lzi;->MS:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Runnable;

    .line 1433
    sget-object v2, Lzi;->MS:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1434
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1435
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 1436
    iget-object v4, p0, Lzi;->MK:Ltj;

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ltj;->a(Ljava/lang/Runnable;I)V

    .line 1435
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1434
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1439
    :cond_0
    return-void
.end method

.method public final iz()V
    .locals 2

    .prologue
    .line 1442
    iget-object v1, p0, Lzi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 1443
    :try_start_0
    iget-object v0, p0, Lzi;->ML:Laah;

    if-eqz v0, :cond_0

    .line 1444
    iget-object v0, p0, Lzi;->ML:Laah;

    invoke-virtual {v0}, Laah;->iG()V

    .line 1446
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1307
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1308
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1310
    invoke-virtual {p0}, Lzi;->iv()V

    .line 1332
    :cond_0
    :goto_0
    return-void

    .line 1311
    :cond_1
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1315
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1316
    iget v1, p0, Lzi;->Nd:I

    iget v2, v0, Landroid/content/res/Configuration;->mcc:I

    if-eq v1, v2, :cond_2

    .line 1317
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reload apps on config change. curr_mcc:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " prevmcc:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lzi;->Nd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1319
    invoke-virtual {p0}, Lzi;->iv()V

    .line 1322
    :cond_2
    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    iput v0, p0, Lzi;->Nd:I

    goto :goto_0

    .line 1323
    :cond_3
    const-string v1, "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.search.action.SEARCHABLES_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325
    :cond_4
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1326
    iget-object v0, p0, Lzi;->MT:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    .line 1327
    if-eqz v0, :cond_0

    .line 1328
    invoke-interface {v0}, Laaf;->hR()V

    goto :goto_0
.end method

.method public final p(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 342
    new-instance v0, Lzj;

    invoke-direct {v0, p0, p1}, Lzj;-><init>(Lzi;Ljava/util/ArrayList;)V

    .line 350
    iget-object v1, p0, Lzi;->MK:Ltj;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ltj;->a(Ljava/lang/Runnable;I)V

    .line 351
    return-void
.end method

.method public final s(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2958
    new-instance v0, Lzp;

    invoke-direct {v0, p0, p1}, Lzp;-><init>(Lzi;Landroid/content/Context;)V

    .line 3008
    sget-object v1, Lzi;->MP:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3009
    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 355
    new-instance v0, Lzu;

    invoke-direct {v0, p0, p1}, Lzu;-><init>(Lzi;Ljava/lang/String;)V

    .line 363
    iget-object v1, p0, Lzi;->MK:Ltj;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ltj;->a(Ljava/lang/Runnable;I)V

    .line 364
    return-void
.end method

.method public final u(Lcom/android/launcher3/Launcher;)Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lzi;->MI:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->hU()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
