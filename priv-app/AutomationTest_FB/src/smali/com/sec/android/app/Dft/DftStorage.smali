.class public Lcom/sec/android/app/Dft/DftStorage;
.super Lcom/sec/android/app/Dft/DftObject;
.source "DftStorage.java"


# static fields
.field private static final STORAGE_PATH:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/storage/sdcard0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/storage/extSdCard"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "/system"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/Dft/DftStorage;->STORAGE_PATH:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    const-string v0, "DftStorage"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/Dft/DftObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method private findFilesByextension(Ljava/io/File;[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 12
    .param p1, "file"    # Ljava/io/File;
    .param p2, "extension"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p3, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 93
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v6, v5

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_3

    aget-object v3, v0, v6

    .line 94
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 95
    invoke-direct {p0, v3, p2, p3}, Lcom/sec/android/app/Dft/DftStorage;->findFilesByextension(Ljava/io/File;[Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 93
    .end local v6    # "i$":I
    :cond_0
    add-int/lit8 v5, v6, 0x1

    .restart local v5    # "i$":I
    move v6, v5

    .end local v5    # "i$":I
    .restart local v6    # "i$":I
    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x2e

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "ext":Ljava/lang/String;
    move-object v1, p2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v5, 0x0

    .end local v6    # "i$":I
    .restart local v5    # "i$":I
    :goto_1
    if-ge v5, v8, :cond_0

    aget-object v4, v1, v5

    .line 100
    .local v4, "findextension":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 101
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 107
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "ext":Ljava/lang/String;
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "findextension":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v8    # "len$":I
    :cond_3
    return-void
.end method


# virtual methods
.method public findFilesByextension(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1, "findPath"    # Ljava/lang/String;
    .param p2, "extension"    # [Ljava/lang/String;

    .prologue
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v0, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, p2, v0}, Lcom/sec/android/app/Dft/DftStorage;->findFilesByextension(Ljava/io/File;[Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 87
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    return-object v1
.end method

.method public findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D
    .locals 12
    .param p1, "findPath"    # Ljava/lang/String;
    .param p2, "extension"    # [Ljava/lang/String;
    .param p3, "unit"    # I

    .prologue
    .line 123
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/Dft/DftStorage;->findFilesByextension(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "findFiles":[Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 126
    .local v6, "size":J
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 127
    .local v1, "file":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 126
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 130
    .end local v1    # "file":Ljava/lang/String;
    :cond_0
    long-to-double v8, v6

    int-to-double v10, p3

    div-double/2addr v8, v10

    return-wide v8
.end method

.method public getAvailableSize(II)J
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 144
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/android/app/Dft/DftStorage;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 145
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getPointSize(II)D
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 139
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/android/app/Dft/DftStorage;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    long-to-double v2, v2

    int-to-double v4, p2

    div-double/2addr v2, v4

    return-wide v2
.end method

.method public getSize(II)J
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 134
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/android/app/Dft/DftStorage;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getStoragePath(I)Ljava/lang/String;
    .locals 1
    .param p1, "storage"    # I

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/Dft/DftStorage;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getUseSize(II)J
    .locals 4
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 154
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/Dft/DftStorage;->getSize(II)J

    move-result-wide v0

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/Dft/DftStorage;->getAvailableSize(II)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method
