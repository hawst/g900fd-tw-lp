.class public Lcom/sec/android/app/FileCopy/FileCopy;
.super Landroid/app/Activity;
.source "FileCopy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;
    }
.end annotation


# static fields
.field private static EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

.field private static INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

.field private static SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

.field private static SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;

.field private static SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;

.field private static USER_FILE_COPY_FOLDER:Ljava/lang/String;

.field private static mButton:Landroid/widget/Button;

.field private static mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

.field private static mFullButton:Landroid/widget/Button;

.field private static mHandler:Landroid/os/Handler;

.field private static mProduct:Ljava/lang/String;

.field private static mProgressBar:Landroid/widget/ProgressBar;

.field private static mResetButton:Landroid/widget/Button;

.field private static mSnote3Enable:Z

.field private static mSnoteEnable:Z


# instance fields
.field private final COPY_PATH_NAME:Ljava/lang/String;

.field private final PATH_KEY:Ljava/lang/String;

.field private final SYSTEM:I

.field private final SYSTEM_SPACE_VALUE:Ljava/lang/String;

.field private final USER:I

.field private final USER_SPACE_VALUE:Ljava/lang/String;

.field private mCloneSize:J

.field private mContext:Landroid/content/Context;

.field private mCopyPathGroup:Landroid/widget/RadioGroup;

.field private mMp3:Landroid/widget/EditText;

.field private mMp3CopyNum:I

.field private mMp3Text:Landroid/widget/TextView;

.field private mMp3Text2:Landroid/widget/TextView;

.field private mPhoto:Landroid/widget/EditText;

.field private mPhotoCopyNum:I

.field private mPhotoText:Landroid/widget/TextView;

.field private mPhotoText2:Landroid/widget/TextView;

.field private mSnote:Landroid/widget/EditText;

.field private mSnote3:Landroid/widget/EditText;

.field private mSnote3Num:I

.field private mSnote3Text:Landroid/widget/TextView;

.field private mSnote3Text2:Landroid/widget/TextView;

.field private mSnote3Text3:Landroid/widget/TextView;

.field private mSnoteNum:I

.field private mSnoteText:Landroid/widget/TextView;

.field private mSnoteText2:Landroid/widget/TextView;

.field private mSnoteText3:Landroid/widget/TextView;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStorageVolumes:[Landroid/os/storage/StorageVolume;

.field private mVideo:Landroid/widget/EditText;

.field private mVideoCopyNum:I

.field private mVideoText:Landroid/widget/TextView;

.field private mVideoText2:Landroid/widget/TextView;

.field private mVoiceMemo:Landroid/widget/EditText;

.field private mVoiceMemoNum:I

.field private mVoiceText:Landroid/widget/TextView;

.field private mVoiceText2:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mHandler:Landroid/os/Handler;

    .line 97
    sput-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 98
    sput-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 99
    sput-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    .line 100
    sput-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    .line 101
    sput-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;

    .line 102
    sput-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;

    .line 103
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    .line 104
    sput-boolean v2, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    .line 105
    sput-boolean v2, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 92
    const-string v0, "filecopy.preferences_name"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->COPY_PATH_NAME:Ljava/lang/String;

    .line 93
    const-string v0, "user_space"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->USER_SPACE_VALUE:Ljava/lang/String;

    .line 94
    const-string v0, "system_space"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_SPACE_VALUE:Ljava/lang/String;

    .line 95
    const-string v0, "path"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->PATH_KEY:Ljava/lang/String;

    .line 107
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM:I

    .line 108
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->USER:I

    .line 695
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FileCopy/FileCopy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteNum:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FileCopy/FileCopy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Num:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FileCopy/FileCopy;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/FileCopy/FileCopy;->setPath(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FileCopy/FileCopy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FileCopy/FileCopy;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    return v0
.end method

.method static synthetic access$2000()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/FileCopy/FileCopy;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;
    .param p1, "x1"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCloneSize:J

    return-wide p1
.end method

.method static synthetic access$2100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FileCopy/FileCopy;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy;->setMaxNumber()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/FileCopy/FileCopy;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy;->defaultSet()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/FileCopy/FileCopy;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy;->threadStart()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/FileCopy/FileCopy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoCopyNum:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FileCopy/FileCopy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoCopyNum:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FileCopy/FileCopy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3CopyNum:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FileCopy/FileCopy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemoNum:I

    return v0
.end method

.method private calculateUsingSpace(Ljava/io/File;JI)V
    .locals 10
    .param p1, "file"    # Ljava/io/File;
    .param p2, "usedSize"    # J
    .param p4, "depth"    # I

    .prologue
    .line 322
    const/16 v5, 0x14

    if-lt p4, v5, :cond_1

    .line 323
    const-string v5, "FileCopy"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MemoryStatusCalculating : Stop Calculating in folder\'s depth =\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\',"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 329
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 331
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 335
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 336
    .local v1, "f":Ljava/io/File;
    add-int/lit8 v5, p4, 0x1

    invoke-direct {p0, v1, p2, p3, v5}, Lcom/sec/android/app/FileCopy/FileCopy;->calculateUsingSpace(Ljava/io/File;JI)V

    .line 335
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 347
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "files":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "mdata_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 348
    iget-wide v6, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCloneSize:J

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCloneSize:J

    goto :goto_0
.end method

.method private defaultSet()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhoto:Landroid/widget/EditText;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3:Landroid/widget/EditText;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideo:Landroid/widget/EditText;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemo:Landroid/widget/EditText;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote:Landroid/widget/EditText;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3:Landroid/widget/EditText;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 137
    return-void
.end method

.method private getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 691
    iget-object v1, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mContext:Landroid/content/Context;

    const-string v2, "filecopy.preferences_name"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 692
    .local v0, "settings":Landroid/content/SharedPreferences;
    const-string v1, "0"

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getSDCardPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 674
    const/4 v0, 0x0

    .line 675
    .local v0, "SDCARD_PATH":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 676
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 681
    :goto_0
    return-object v0

    .line 678
    :cond_0
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    goto :goto_0
.end method

.method private getSystemMemorySpace()J
    .locals 11

    .prologue
    .line 372
    sget-object v8, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 373
    const-wide/16 v4, 0x0

    .line 384
    :goto_0
    return-wide v4

    .line 376
    :cond_0
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 378
    .local v7, "storageDirectory":Ljava/lang/String;
    new-instance v6, Landroid/os/StatFs;

    invoke-direct {v6, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 379
    .local v6, "stat":Landroid/os/StatFs;
    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v2, v8

    .line 380
    .local v2, "blocksize":J
    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v0, v8

    .line 381
    .local v0, "blockcount":J
    mul-long v4, v0, v2

    .line 382
    .local v4, "remaining":J
    const-string v8, "FileCopy"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getSystemMemorySpace : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " (size "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " * count "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getUserMemorySpace()J
    .locals 11

    .prologue
    .line 357
    sget-object v8, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 358
    const-wide/16 v4, 0x0

    .line 368
    :goto_0
    return-wide v4

    .line 361
    :cond_0
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 362
    .local v7, "storageDirectory":Ljava/lang/String;
    new-instance v6, Landroid/os/StatFs;

    invoke-direct {v6, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 363
    .local v6, "stat":Landroid/os/StatFs;
    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v2, v8

    .line 364
    .local v2, "blocksize":J
    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v0, v8

    .line 365
    .local v0, "blockcount":J
    mul-long v4, v0, v2

    .line 366
    .local v4, "remaining":J
    const-string v8, "FileCopy"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getUserMemorySpace : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " (size "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " * count "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private init()V
    .locals 12

    .prologue
    const v11, 0x7f060042

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 436
    const-string v0, "GT-N8000"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GT-N8010"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GT-N8020"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GT-N8013"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GT-N8005"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SHW-M480W"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SHW-M480S"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SHW-M480K"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "GT-N710"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "SGH-I317"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCH-I605"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCH-R950"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SPH-L900"

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "SGH-T889"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "SHV-E250"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "SHV-E230"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "Sailor"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "SC-02E"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProduct:Ljava/lang/String;

    const-string v2, "GT-N51"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v9

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    .line 444
    sput-boolean v9, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    .line 445
    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v2, v2, v9

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "mounted"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    .line 470
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/FileCopy"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/FileCopy"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/S Note"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;

    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/SnoteData"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;

    .line 475
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 476
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 477
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 478
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 479
    const v0, 0x7f060043

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText:Landroid/widget/TextView;

    .line 480
    const v0, 0x7f060045

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText:Landroid/widget/TextView;

    .line 481
    const v0, 0x7f060044

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text:Landroid/widget/TextView;

    .line 482
    const v0, 0x7f060046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText:Landroid/widget/TextView;

    .line 483
    const v0, 0x7f060049

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText2:Landroid/widget/TextView;

    .line 484
    const v0, 0x7f06004b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText2:Landroid/widget/TextView;

    .line 485
    const v0, 0x7f06004a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text2:Landroid/widget/TextView;

    .line 486
    const v0, 0x7f06004c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText2:Landroid/widget/TextView;

    .line 487
    const v0, 0x7f060055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote:Landroid/widget/EditText;

    .line 488
    const v0, 0x7f060047

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText:Landroid/widget/TextView;

    .line 489
    const v0, 0x7f06004d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText2:Landroid/widget/TextView;

    .line 490
    const v0, 0x7f060054

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText3:Landroid/widget/TextView;

    .line 491
    const v0, 0x7f060057

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3:Landroid/widget/EditText;

    .line 492
    const v0, 0x7f060048

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text:Landroid/widget/TextView;

    .line 493
    const v0, 0x7f06004e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text2:Landroid/widget/TextView;

    .line 494
    const v0, 0x7f060056

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text3:Landroid/widget/TextView;

    .line 496
    sget-boolean v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText3:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 502
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    if-eqz v0, :cond_2

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text3:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 509
    :cond_2
    iput-object p0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mContext:Landroid/content/Context;

    .line 510
    const v0, 0x7f060058

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProgressBar:Landroid/widget/ProgressBar;

    .line 511
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 512
    const v0, 0x7f060050

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhoto:Landroid/widget/EditText;

    .line 513
    const v0, 0x7f060051

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3:Landroid/widget/EditText;

    .line 514
    const v0, 0x7f060052

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideo:Landroid/widget/EditText;

    .line 515
    const v0, 0x7f060053

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemo:Landroid/widget/EditText;

    .line 516
    new-instance v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    iget v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoCopyNum:I

    iget v3, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoCopyNum:I

    iget v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3CopyNum:I

    iget v5, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemoNum:I

    iget v6, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteNum:I

    iget v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Num:I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;-><init>(Landroid/content/Context;IIIIII)V

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    .line 518
    const v0, 0x7f06004f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mFullButton:Landroid/widget/Button;

    .line 519
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mFullButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/FileCopy/FileCopy$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FileCopy/FileCopy$1;-><init>(Lcom/sec/android/app/FileCopy/FileCopy;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    const v0, 0x7f060059

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mResetButton:Landroid/widget/Button;

    .line 526
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/FileCopy/FileCopy$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FileCopy/FileCopy$2;-><init>(Lcom/sec/android/app/FileCopy/FileCopy;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 531
    const v0, 0x7f06005a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mButton:Landroid/widget/Button;

    .line 532
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/FileCopy/FileCopy$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FileCopy/FileCopy$3;-><init>(Lcom/sec/android/app/FileCopy/FileCopy;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 539
    const v0, 0x7f060041

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCopyPathGroup:Landroid/widget/RadioGroup;

    .line 540
    const-string v0, "path"

    invoke-direct {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 542
    .local v8, "pathIniString":Ljava/lang/String;
    const-string v0, "0"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v9, :cond_7

    .line 543
    const-string v0, "system_space"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v9, :cond_6

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCopyPathGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f060004

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 545
    invoke-direct {p0, v9}, Lcom/sec/android/app/FileCopy/FileCopy;->setPath(I)V

    .line 555
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCopyPathGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/sec/android/app/FileCopy/FileCopy$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FileCopy/FileCopy$4;-><init>(Lcom/sec/android/app/FileCopy/FileCopy;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 574
    return-void

    .end local v8    # "pathIniString":Ljava/lang/String;
    :cond_4
    move v0, v1

    .line 436
    goto/16 :goto_0

    .line 467
    :cond_5
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    goto/16 :goto_1

    .line 546
    .restart local v8    # "pathIniString":Ljava/lang/String;
    :cond_6
    const-string v0, "user_space"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v9, :cond_3

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCopyPathGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v11}, Landroid/widget/RadioGroup;->check(I)V

    .line 548
    invoke-direct {p0, v10}, Lcom/sec/android/app/FileCopy/FileCopy;->setPath(I)V

    goto :goto_2

    .line 551
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mCopyPathGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v11}, Landroid/widget/RadioGroup;->check(I)V

    .line 552
    invoke-direct {p0, v10}, Lcom/sec/android/app/FileCopy/FileCopy;->setPath(I)V

    goto :goto_2
.end method

.method public static onFinish()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 140
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 141
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 142
    sget-object v0, Lcom/sec/android/app/FileCopy/FileCopy;->mFullButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 143
    return-void
.end method

.method private setMaxNumber()V
    .locals 40

    .prologue
    .line 153
    const-wide/16 v16, 0x0

    .line 154
    .local v16, "photoFullNumber":J
    const-wide/16 v32, 0x0

    .line 155
    .local v32, "voiceFullNumber":J
    const-wide/16 v12, 0x0

    .line 156
    .local v12, "mp3FullNumber":J
    const-wide/16 v28, 0x0

    .line 157
    .local v28, "videoFullNumber":J
    const-wide/16 v8, 0x0

    .line 158
    .local v8, "SnoteFullNumber":J
    const-wide/16 v4, 0x0

    .line 160
    .local v4, "Snote3FullNumber":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mCopyPathGroup:Landroid/widget/RadioGroup;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v36

    const v37, 0x7f060004

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_2

    .line 161
    const/16 v36, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/sec/android/app/FileCopy/FileCopy;->startFileCalculation(I)V

    .line 162
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FileCopy/FileCopy;->getSystemMemorySpace()J

    move-result-wide v26

    .line 163
    .local v26, "totalSpace":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mCloneSize:J

    move-wide/from16 v36, v0

    add-long v26, v26, v36

    .line 174
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mCloneSize:J

    move-wide/from16 v36, v0

    add-long v26, v26, v36

    .line 179
    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v36, v0

    const-wide v38, 0x3feccccccccccccdL    # 0.9

    mul-double v36, v36, v38

    move-wide/from16 v0, v36

    double-to-long v0, v0

    move-wide/from16 v26, v0

    .line 181
    sget-boolean v36, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v36, :cond_8

    .line 182
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x1

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v21

    .line 183
    .local v21, "srcFilePhoto":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x2

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v25

    .line 184
    .local v25, "srcFileVoice":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x3

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v20

    .line 185
    .local v20, "srcFileMp3":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x4

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v24

    .line 186
    .local v24, "srcFileVideo":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x5

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v22

    .line 188
    .local v22, "srcFileSnote":Ljava/io/File;
    if-eqz v21, :cond_3

    if-eqz v25, :cond_3

    if-eqz v20, :cond_3

    if-eqz v24, :cond_3

    if-eqz v22, :cond_3

    .line 190
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "totalSpace : "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const-wide/16 v36, 0x5

    div-long v10, v26, v36

    .line 192
    .local v10, "SnoteFullspace":J
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v8, v10, v36

    .line 193
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "Snote : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v8

    sub-long v36, v26, v36

    const-wide/16 v38, 0x4

    div-long v34, v36, v38

    .line 196
    .local v34, "voiceFullSpace":J
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v32, v34, v36

    .line 197
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "voice : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v32

    sub-long v36, v26, v36

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v8

    sub-long v36, v36, v38

    const-wide/16 v38, 0x3

    div-long v14, v36, v38

    .line 201
    .local v14, "mp3FullSpace":J
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v12, v14, v36

    .line 202
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "mp3 : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v32

    sub-long v36, v26, v36

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v12

    sub-long v36, v36, v38

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v8

    sub-long v36, v36, v38

    const-wide/16 v38, 0x2

    div-long v30, v36, v38

    .line 206
    .local v30, "videoFullSpace":J
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v28, v30, v36

    .line 207
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "video : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v32

    sub-long v36, v26, v36

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v12

    sub-long v36, v36, v38

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v28

    sub-long v36, v36, v38

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v8

    sub-long v18, v36, v38

    .line 212
    .local v18, "photoFullSpace":J
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v16, v18, v36

    .line 213
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "photo : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhoto:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideo:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemo:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 302
    .end local v10    # "SnoteFullspace":J
    .end local v14    # "mp3FullSpace":J
    .end local v18    # "photoFullSpace":J
    .end local v20    # "srcFileMp3":Ljava/io/File;
    .end local v21    # "srcFilePhoto":Ljava/io/File;
    .end local v22    # "srcFileSnote":Ljava/io/File;
    .end local v24    # "srcFileVideo":Ljava/io/File;
    .end local v25    # "srcFileVoice":Ljava/io/File;
    .end local v30    # "videoFullSpace":J
    .end local v34    # "voiceFullSpace":J
    :cond_1
    :goto_0
    return-void

    .line 165
    .end local v26    # "totalSpace":J
    :cond_2
    const/16 v36, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/sec/android/app/FileCopy/FileCopy;->startFileCalculation(I)V

    .line 166
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FileCopy/FileCopy;->getUserMemorySpace()J

    move-result-wide v26

    .line 167
    .restart local v26    # "totalSpace":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mCloneSize:J

    move-wide/from16 v36, v0

    add-long v26, v26, v36

    .line 169
    const-wide/16 v36, 0x0

    cmp-long v36, v26, v36

    if-nez v36, :cond_0

    goto :goto_0

    .line 221
    .restart local v20    # "srcFileMp3":Ljava/io/File;
    .restart local v21    # "srcFilePhoto":Ljava/io/File;
    .restart local v22    # "srcFileSnote":Ljava/io/File;
    .restart local v24    # "srcFileVideo":Ljava/io/File;
    .restart local v25    # "srcFileVoice":Ljava/io/File;
    :cond_3
    if-nez v21, :cond_4

    .line 222
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Photo File is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 225
    :cond_4
    if-nez v20, :cond_5

    .line 226
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Mp3 File is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 229
    :cond_5
    if-nez v24, :cond_6

    .line 230
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Video File is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 233
    :cond_6
    if-nez v25, :cond_7

    .line 234
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Voice Memo is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 237
    :cond_7
    if-nez v22, :cond_1

    .line 238
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "SNote is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    .end local v20    # "srcFileMp3":Ljava/io/File;
    .end local v21    # "srcFilePhoto":Ljava/io/File;
    .end local v22    # "srcFileSnote":Ljava/io/File;
    .end local v24    # "srcFileVideo":Ljava/io/File;
    .end local v25    # "srcFileVoice":Ljava/io/File;
    :cond_8
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x1

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v21

    .line 243
    .restart local v21    # "srcFilePhoto":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x2

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v25

    .line 244
    .restart local v25    # "srcFileVoice":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x3

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v20

    .line 245
    .restart local v20    # "srcFileMp3":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x4

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v24

    .line 246
    .restart local v24    # "srcFileVideo":Ljava/io/File;
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const/16 v37, 0x6

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;

    move-result-object v23

    .line 248
    .local v23, "srcFileSnote3":Ljava/io/File;
    if-eqz v21, :cond_9

    if-eqz v25, :cond_9

    if-eqz v20, :cond_9

    if-eqz v24, :cond_9

    if-eqz v23, :cond_9

    .line 250
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "totalSpace : "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const-wide/16 v36, 0x5

    div-long v6, v26, v36

    .line 252
    .local v6, "Snote3Fullspace":J
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v4, v6, v36

    .line 253
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "Snote 3.0 : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v4

    sub-long v36, v26, v36

    const-wide/16 v38, 0x4

    div-long v34, v36, v38

    .line 256
    .restart local v34    # "voiceFullSpace":J
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v32, v34, v36

    .line 257
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "voice : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v32

    sub-long v36, v26, v36

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v4

    sub-long v36, v36, v38

    const-wide/16 v38, 0x3

    div-long v14, v36, v38

    .line 261
    .restart local v14    # "mp3FullSpace":J
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v12, v14, v36

    .line 262
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "mp3 : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v32

    sub-long v36, v26, v36

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v12

    sub-long v36, v36, v38

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v4

    sub-long v36, v36, v38

    const-wide/16 v38, 0x2

    div-long v30, v36, v38

    .line 266
    .restart local v30    # "videoFullSpace":J
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v28, v30, v36

    .line 267
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "video : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->length()J

    move-result-wide v36

    mul-long v36, v36, v32

    sub-long v36, v26, v36

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v12

    sub-long v36, v36, v38

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v28

    sub-long v36, v36, v38

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v38

    mul-long v38, v38, v4

    sub-long v18, v36, v38

    .line 272
    .restart local v18    # "photoFullSpace":J
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v36

    div-long v16, v18, v36

    .line 273
    const-string v36, "FileCopy"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "photo : space "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ", "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " * "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhoto:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideo:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemo:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3:Landroid/widget/EditText;

    move-object/from16 v36, v0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 281
    .end local v6    # "Snote3Fullspace":J
    .end local v14    # "mp3FullSpace":J
    .end local v18    # "photoFullSpace":J
    .end local v30    # "videoFullSpace":J
    .end local v34    # "voiceFullSpace":J
    :cond_9
    if-nez v21, :cond_a

    .line 282
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Photo File is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 285
    :cond_a
    if-nez v20, :cond_b

    .line 286
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Mp3 File is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 289
    :cond_b
    if-nez v24, :cond_c

    .line 290
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Video File is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 293
    :cond_c
    if-nez v25, :cond_d

    .line 294
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "Voice Memo is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    .line 297
    :cond_d
    if-nez v23, :cond_1

    .line 298
    sget-object v36, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    const-string v37, "SNote 3.0 is not ready!!"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V
    invoke-static/range {v36 .. v37}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setPath(I)V
    .locals 10
    .param p1, "path"    # I

    .prologue
    .line 577
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setPath : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v7, :cond_4

    .line 580
    packed-switch p1, :pswitch_data_0

    .line 651
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->getSDCardPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/loaddata_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ro.product.model"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 653
    .local v2, "LOADDATA_PATH":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 654
    .local v1, "ImagePath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "voicememo"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 655
    .local v0, "AmrPath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "music"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 656
    .local v3, "Mp3Path":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "video"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 657
    .local v6, "VideoPath":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Image : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 658
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Video : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 659
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sound : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 660
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VoiceMemo : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.amr"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 662
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v7, :cond_0

    .line 663
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "snote"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 664
    .local v5, "SnotePath":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SNote : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.snb"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 667
    .end local v5    # "SnotePath":Ljava/lang/String;
    :cond_0
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    if-eqz v7, :cond_1

    .line 668
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "snote3"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 669
    .local v4, "Snote3Path":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Snote 3.0 : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.spd"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 671
    .end local v4    # "Snote3Path":Ljava/lang/String;
    :cond_1
    return-void

    .line 582
    .end local v0    # "AmrPath":Ljava/lang/String;
    .end local v1    # "ImagePath":Ljava/lang/String;
    .end local v2    # "LOADDATA_PATH":Ljava/lang/String;
    .end local v3    # "Mp3Path":Ljava/lang/String;
    .end local v6    # "VideoPath":Ljava/lang/String;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Image : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 583
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Video : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sound : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 585
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VoiceMemo : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.amr"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v7, :cond_2

    .line 588
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SNote : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.snb"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    :cond_2
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->setSystemPath()V
    invoke-static {v7}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$1400(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V

    goto/16 :goto_0

    .line 595
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Image : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Video : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sound : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VoiceMemo : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.amr"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 600
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v7, :cond_3

    .line 601
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SNote : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.snb"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    :cond_3
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->setUserPath()V
    invoke-static {v7}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$1500(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V

    goto/16 :goto_0

    .line 610
    :cond_4
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    .line 612
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Image : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Video : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 614
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sound : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VoiceMemo : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.amr"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 617
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    if-eqz v7, :cond_5

    .line 618
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SNote 3.0 : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.spd"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 622
    :cond_5
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->setSystemPath()V
    invoke-static {v7}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$1400(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V

    goto/16 :goto_0

    .line 625
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Image : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Video : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3Text:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sound : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.mp3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 628
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceText:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VoiceMemo : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.amr"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 630
    sget-boolean v7, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    if-eqz v7, :cond_6

    .line 631
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Text:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SNote 3.0 : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.spd"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    :cond_6
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy;->mFileCopyThread:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->setUserPath()V
    invoke-static {v7}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->access$1500(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V

    goto/16 :goto_0

    .line 580
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 610
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 684
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mContext:Landroid/content/Context;

    const-string v3, "filecopy.preferences_name"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 685
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 686
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 687
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 688
    return-void
.end method

.method private startFileCalculation(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 307
    packed-switch p1, :pswitch_data_0

    .line 315
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 318
    .local v0, "f1":Ljava/io/File;
    :goto_0
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/sec/android/app/FileCopy/FileCopy;->calculateUsingSpace(Ljava/io/File;JI)V

    .line 319
    return-void

    .line 309
    .end local v0    # "f1":Ljava/io/File;
    :pswitch_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 310
    .restart local v0    # "f1":Ljava/io/File;
    goto :goto_0

    .line 312
    .end local v0    # "f1":Ljava/io/File;
    :pswitch_1
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 313
    .restart local v0    # "f1":Ljava/io/File;
    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private threadStart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 388
    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 389
    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mResetButton:Landroid/widget/Button;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 390
    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mResetButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 391
    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 392
    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->mFullButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 395
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhoto:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoCopyNum:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3CopyNum:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 407
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideo:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoCopyNum:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    .line 413
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemo:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemoNum:I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    .line 418
    :goto_3
    sget-boolean v2, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z

    if-eqz v2, :cond_0

    .line 420
    :try_start_4
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteNum:I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    .line 426
    :cond_0
    :goto_4
    sget-boolean v2, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z

    if-eqz v2, :cond_1

    .line 428
    :try_start_5
    iget-object v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Num:I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_5

    .line 433
    :cond_1
    :goto_5
    return-void

    .line 396
    :catch_0
    move-exception v0

    .line 397
    .local v0, "ne":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mPhotoCopyNum:I

    goto :goto_0

    .line 402
    .end local v0    # "ne":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v1

    .line 403
    .local v1, "nfe":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mMp3CopyNum:I

    goto :goto_1

    .line 408
    .end local v1    # "nfe":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v1

    .line 409
    .restart local v1    # "nfe":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVideoCopyNum:I

    goto :goto_2

    .line 414
    .end local v1    # "nfe":Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v1

    .line 415
    .restart local v1    # "nfe":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mVoiceMemoNum:I

    goto :goto_3

    .line 421
    .end local v1    # "nfe":Ljava/lang/NumberFormatException;
    :catch_4
    move-exception v1

    .line 422
    .restart local v1    # "nfe":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteNum:I

    goto :goto_4

    .line 429
    .end local v1    # "nfe":Ljava/lang/NumberFormatException;
    :catch_5
    move-exception v1

    .line 430
    .restart local v1    # "nfe":Ljava/lang/NumberFormatException;
    iput v4, p0, Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Num:I

    goto :goto_5
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 123
    const-string v0, "FileCopy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "system path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const-string v0, "FileCopy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "user path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FileCopy/FileCopy;->setContentView(I)V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy;->init()V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy;->defaultSet()V

    .line 128
    return-void
.end method
