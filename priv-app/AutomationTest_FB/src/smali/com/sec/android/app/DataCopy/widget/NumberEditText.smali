.class public Lcom/sec/android/app/DataCopy/widget/NumberEditText;
.super Landroid/widget/EditText;
.source "NumberEditText.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->initialize()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    const v0, 0x101006e

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->initialize()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->initialize()V

    .line 26
    return-void
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInputType(I)V

    .line 62
    invoke-super {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const-string v0, "0"

    invoke-super {p0, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 79
    :cond_0
    new-instance v0, Lcom/sec/android/app/DataCopy/widget/NumberEditText$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText$1;-><init>(Lcom/sec/android/app/DataCopy/widget/NumberEditText;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 94
    return-void
.end method


# virtual methods
.method public getInt()I
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 30
    if-nez p1, :cond_0

    .line 31
    const-string v0, "0"

    invoke-super {p0, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x1060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->setTextColor(I)V

    .line 37
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 38
    invoke-super {p0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 39
    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1060012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0
.end method

.method public setInt(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 51
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 43
    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 44
    :cond_0
    const-string v0, "0"

    invoke-super {p0, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 47
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 48
    return-void
.end method
