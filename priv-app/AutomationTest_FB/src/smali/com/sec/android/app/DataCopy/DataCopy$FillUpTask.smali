.class Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
.super Landroid/os/AsyncTask;
.source "DataCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/DataCopy/DataCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FillUpTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final MSG_SHOW_TOAST:Ljava/lang/Integer;

.field private final MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

.field private mContext:Landroid/content/Context;

.field private mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

.field private mProgress:Landroid/app/ProgressDialog;

.field private mProgressCount:I

.field final synthetic this$0:Lcom/sec/android/app/DataCopy/DataCopy;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/DataCopy/DataCopy;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 386
    iput-object p1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 378
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    .line 379
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    .line 387
    iput-object p2, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    .line 389
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 392
    return-void
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)V
    .locals 6
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "destFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 858
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 859
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    .line 862
    :cond_0
    const/4 v1, 0x0

    .line 863
    .local v1, "src":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 866
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 867
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 868
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 870
    if-eqz v0, :cond_1

    .line 871
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 874
    :cond_1
    if-eqz v1, :cond_2

    .line 875
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 878
    :cond_2
    return-void

    .line 870
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 871
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 874
    :cond_3
    if-eqz v1, :cond_4

    .line 875
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    :cond_4
    throw v2
.end method

.method private fillUpCallLog(II)V
    .locals 11
    .param p1, "type"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 931
    if-eqz p2, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 969
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    const/4 v4, 0x1

    .line 936
    .local v4, "mCallLogFileExist":Z
    new-instance v1, Lcom/sec/android/app/Dft/DftCallLog;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/sec/android/app/Dft/DftCallLog;-><init>(Landroid/content/Context;)V

    .line 937
    .local v1, "dftCallLog":Lcom/sec/android/app/Dft/DftCallLog;
    const-string v5, "DataCopy"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fillUpCallLog - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1, p1}, Lcom/sec/android/app/Dft/DftCallLog;->getCallLogTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, p2, :cond_2

    .line 943
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 950
    .local v0, "callLogDatas":[Ljava/lang/String;
    array-length v5, v0

    if-lt v5, v10, :cond_3

    .line 951
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 952
    aget-object v5, v0, v9

    invoke-virtual {v1, v5, p1, v8}, Lcom/sec/android/app/Dft/DftCallLog;->insert(Ljava/lang/String;II)V

    .line 953
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Fill Up "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1, p1}, Lcom/sec/android/app/Dft/DftCallLog;->getCallLogTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 939
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 944
    .end local v0    # "callLogDatas":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 945
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 946
    const/4 v4, 0x0

    .line 962
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v4, :cond_4

    .line 963
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/sec/android/app/Dft/DftCallLog;->getCallLogTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fill up is done"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 964
    const-string v5, "DataCopy"

    const-string v6, "Call Log Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 958
    .restart local v0    # "callLogDatas":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 966
    .end local v0    # "callLogDatas":[Ljava/lang/String;
    :cond_4
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/sec/android/app/Dft/DftCallLog;->getCallLogTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fill up failed. CallLog file does not exist"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 967
    const-string v5, "DataCopy"

    const-string v6, "Call Log Fill Up failed. CallLog file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private fillUpContact(I)V
    .locals 10
    .param p1, "count"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 622
    if-eqz p1, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    const/4 v4, 0x1

    .line 627
    .local v4, "mContactFileExist":Z
    new-instance v1, Lcom/sec/android/app/Dft/DftContacts;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/sec/android/app/Dft/DftContacts;-><init>(Landroid/content/Context;)V

    .line 628
    .local v1, "dftContact":Lcom/sec/android/app/Dft/DftContacts;
    const-string v5, "DataCopy"

    const-string v6, "fillUpSchedule"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, p1, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_3

    .line 634
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 641
    .local v0, "contactDatas":[Ljava/lang/String;
    if-eqz v0, :cond_5

    array-length v5, v0

    const/16 v6, 0x1d

    if-lt v5, v6, :cond_5

    .line 642
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 643
    add-int/lit8 v5, v3, 0x1

    rem-int/lit8 v5, v5, 0xa

    if-eqz v5, :cond_2

    add-int/lit8 v5, p1, -0x1

    if-ne v3, v5, :cond_4

    .line 644
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v5, v8}, Lcom/sec/android/app/Dft/DftContacts;->insert([Ljava/lang/String;Ljava/lang/String;Z)V

    .line 649
    :goto_2
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Fill Up Contacts..."

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 630
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 635
    .end local v0    # "contactDatas":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 636
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 637
    const/4 v4, 0x0

    .line 658
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    if-eqz v4, :cond_6

    .line 659
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Contacts fill up is done"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 660
    const-string v5, "DataCopy"

    const-string v6, "Contacts Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 646
    .restart local v0    # "contactDatas":[Ljava/lang/String;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v5, v7}, Lcom/sec/android/app/Dft/DftContacts;->insert([Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 654
    :cond_5
    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    .line 662
    .end local v0    # "contactDatas":[Ljava/lang/String;
    :cond_6
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Contacts fill up failed. Contacts file does not exist"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 663
    const-string v5, "DataCopy"

    const-string v6, "Contacts Fill Up failed. Contacts file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private fillUpMMS(II)V
    .locals 11
    .param p1, "type"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 581
    if-eqz p2, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    const/4 v3, 0x1

    .line 586
    .local v3, "mMmsFileExist":Z
    new-instance v0, Lcom/sec/android/app/Dft/DftMessage;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/Dft/DftMessage;-><init>(Landroid/content/Context;)V

    .line 587
    .local v0, "dftMMS":Lcom/sec/android/app/Dft/DftMessage;
    const-string v5, "DataCopy"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fillUpMMS - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getMMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, p2, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 593
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 600
    .local v4, "mmsDatas":[Ljava/lang/String;
    if-eqz v4, :cond_3

    array-length v5, v4

    const/4 v6, 0x5

    if-lt v5, v6, :cond_3

    .line 601
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 602
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p1, v4, v5}, Lcom/sec/android/app/Dft/DftMessage;->insertMMS(I[Ljava/lang/String;Ljava/lang/String;)Z

    .line 603
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Fill Up "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getMMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 589
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 594
    .end local v4    # "mmsDatas":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 595
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 596
    const/4 v3, 0x0

    .line 612
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v3, :cond_4

    .line 613
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getMMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fill up is done"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 614
    const-string v5, "DataCopy"

    const-string v6, "MMS Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 608
    .restart local v4    # "mmsDatas":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 616
    .end local v4    # "mmsDatas":[Ljava/lang/String;
    :cond_4
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getMMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fill up failed. MMS file does not exist"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 617
    const-string v5, "DataCopy"

    const-string v6, "MMS Fill Up failed. MMS file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private fillUpMemo(I)V
    .locals 10
    .param p1, "count"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 709
    if-eqz p1, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    const/4 v3, 0x1

    .line 714
    .local v3, "mMemoFileExist":Z
    new-instance v0, Lcom/sec/android/app/Dft/DftMemo;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/Dft/DftMemo;-><init>(Landroid/content/Context;)V

    .line 715
    .local v0, "dftMemo":Lcom/sec/android/app/Dft/DftMemo;
    const-string v5, "DataCopy"

    const-string v6, "fillUpMemo"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, p1, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 721
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 728
    .local v4, "memoDatas":[Ljava/lang/String;
    if-eqz v4, :cond_3

    array-length v5, v4

    if-eqz v5, :cond_3

    .line 729
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 730
    aget-object v5, v4, v7

    invoke-virtual {v0, v5}, Lcom/sec/android/app/Dft/DftMemo;->insert(Ljava/lang/String;)V

    .line 731
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Fill Up Memo..."

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 717
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 722
    .end local v4    # "memoDatas":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 723
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 724
    const/4 v3, 0x0

    .line 740
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v3, :cond_4

    .line 741
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Memo fill up is done"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 742
    const-string v5, "DataCopy"

    const-string v6, "Memo Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 736
    .restart local v4    # "memoDatas":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 744
    .end local v4    # "memoDatas":[Ljava/lang/String;
    :cond_4
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Memo fill up failed. Memo file does not exist"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 745
    const-string v5, "DataCopy"

    const-string v6, "Memo Fill Up failed. Memo file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private fillUpNMemo(I)V
    .locals 12
    .param p1, "count"    # I

    .prologue
    .line 750
    if-eqz p1, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v8}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v8}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 818
    :cond_0
    :goto_0
    return-void

    .line 754
    :cond_1
    const/4 v3, 0x1

    .line 755
    .local v3, "mMemoFileExist":Z
    const/4 v4, 0x1

    .line 756
    .local v4, "mMemoSvcBinded":Z
    new-instance v0, Lcom/sec/android/app/Dft/DftNMemo;

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/Dft/DftNMemo;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 757
    .local v0, "dftNMemo":Lcom/sec/android/app/Dft/DftNMemo;
    const-string v8, "DataCopy"

    const-string v9, "fillUpNMemo"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    const-string v8, "DataCopy"

    const-string v9, "fillUpNMemo..bind"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftNMemo;->bind()V

    .line 762
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x1388

    add-long v6, v8, v10

    .line 763
    .local v6, "timeout":J
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v8, v8, v6

    if-gez v8, :cond_2

    .line 764
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftNMemo;->isBinded()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 772
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftNMemo;->isBinded()Z

    move-result v4

    .line 775
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, p1, :cond_4

    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v8}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v8

    if-nez v8, :cond_4

    .line 779
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v8}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v8

    const-string v9, "\t"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    .line 786
    .local v5, "memoDatas":[Ljava/lang/String;
    :try_start_2
    array-length v8, v5

    if-eqz v8, :cond_5

    .line 787
    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v8}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 788
    const/4 v8, 0x0

    aget-object v8, v5, v8

    invoke-virtual {v0, v2, v8}, Lcom/sec/android/app/Dft/DftNMemo;->insert(ILjava/lang/String;)V

    .line 789
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Fill Up NMemo ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "..."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 775
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 767
    .end local v2    # "i":I
    .end local v5    # "memoDatas":[Ljava/lang/String;
    :cond_3
    const-wide/16 v8, 0xc8

    :try_start_3
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 768
    :catch_0
    move-exception v1

    .line 769
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 780
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "i":I
    :catch_1
    move-exception v1

    .line 781
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2

    .line 782
    const/4 v3, 0x0

    .line 802
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_4
    if-eqz v3, :cond_6

    if-eqz v4, :cond_6

    .line 803
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "Memo fill up is done"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 804
    const-string v8, "DataCopy"

    const-string v9, "Memo Fill Up is done"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    :goto_5
    if-eqz v4, :cond_0

    .line 814
    const-string v8, "DataCopy"

    const-string v9, "fillUpNMemo..unbind"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftNMemo;->unbind()V

    .line 816
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 794
    .restart local v5    # "memoDatas":[Ljava/lang/String;
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    .line 797
    .end local v5    # "memoDatas":[Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 798
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 799
    const/4 v4, 0x0

    goto :goto_4

    .line 805
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_6
    if-nez v4, :cond_7

    .line 806
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "Memo fill up failed. Failed to bind Memo SVC"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 807
    const-string v8, "DataCopy"

    const-string v9, "Memo Fill Up failed. Failed to bind Memo SVC"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 809
    :cond_7
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "Memo fill up failed. Memo file does not exist"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 810
    const-string v8, "DataCopy"

    const-string v9, "Memo Fill Up failed. Memo file does not exist"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private fillUpPenMemo(I)V
    .locals 10
    .param p1, "count"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 821
    if-eqz p1, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 855
    :cond_0
    :goto_0
    return-void

    .line 825
    :cond_1
    const/4 v3, 0x1

    .line 826
    .local v3, "mPenMemoFileExist":Z
    new-instance v0, Lcom/sec/android/app/Dft/DftPenMemo;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/Dft/DftPenMemo;-><init>(Landroid/content/Context;)V

    .line 827
    .local v0, "dftPenMemo":Lcom/sec/android/app/Dft/DftPenMemo;
    const-string v5, "DataCopy"

    const-string v6, "fillUpPenMemo"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, p1, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 830
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 834
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 841
    .local v4, "memoDatas":[Ljava/lang/String;
    aget-object v5, v4, v7

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/Dft/DftPenMemo;->saveMemoData(ILjava/lang/String;)V

    .line 842
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Fill Up Pen Memo..."

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 829
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 835
    .end local v4    # "memoDatas":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 836
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 837
    const/4 v3, 0x0

    .line 848
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v3, :cond_3

    .line 849
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Pen Memo fill up is done"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 850
    const-string v5, "DataCopy"

    const-string v6, "Pen Memo Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 852
    :cond_3
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Pen Memo fill up failed. Pen Memo file does not exist"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 853
    const-string v5, "DataCopy"

    const-string v6, "Pen Memo Fill Up failed. Pen Memo file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private fillUpSMS(II)V
    .locals 11
    .param p1, "type"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 540
    if-eqz p2, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    const/4 v3, 0x1

    .line 545
    .local v3, "mSmsFileExist":Z
    new-instance v0, Lcom/sec/android/app/Dft/DftMessage;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/Dft/DftMessage;-><init>(Landroid/content/Context;)V

    .line 546
    .local v0, "dftSMS":Lcom/sec/android/app/Dft/DftMessage;
    const-string v5, "DataCopy"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fillUpSMS - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getSMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, p2, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 552
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 559
    .local v4, "smsDatas":[Ljava/lang/String;
    if-eqz v4, :cond_3

    array-length v5, v4

    const/4 v6, 0x3

    if-lt v5, v6, :cond_3

    .line 560
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 561
    invoke-virtual {v0, p1, v4}, Lcom/sec/android/app/Dft/DftMessage;->insertSMS(I[Ljava/lang/String;)V

    .line 562
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Fill Up "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getSMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 548
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 553
    .end local v4    # "smsDatas":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 554
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 555
    const/4 v3, 0x0

    .line 571
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v3, :cond_4

    .line 572
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getSMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fill up is done"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 573
    const-string v5, "DataCopy"

    const-string v6, "SMS Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 567
    .restart local v4    # "smsDatas":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 575
    .end local v4    # "smsDatas":[Ljava/lang/String;
    :cond_4
    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/Dft/DftMessage;->getSMSTypeString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fill up failed. SMS file does not exist"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 576
    const-string v5, "DataCopy"

    const-string v6, "SMS Fill Up failed. SMS file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private fillUpSMemo2(I)V
    .locals 12
    .param p1, "count"    # I

    .prologue
    .line 881
    if-eqz p1, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v6}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v6}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 926
    :cond_0
    :goto_0
    return-void

    .line 885
    :cond_1
    const-string v6, "DataCopy"

    const-string v7, "fillUpSMemo2"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "SMemo"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v7, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask$1;-><init>(Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;)V

    invoke-virtual {v6, v7}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 893
    .local v3, "src_files":[Ljava/io/File;
    if-nez v3, :cond_2

    .line 894
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "SMemo+ fill up failed. SNB file does not exist"

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 895
    const-string v6, "DataCopy"

    const-string v7, "SMemo+ Fill Up failed. SNB file does not exist"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 899
    :cond_2
    array-length v2, v3

    .line 901
    .local v2, "num_src_files":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_6

    .line 902
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v2, :cond_5

    add-int v6, v0, v1

    if-ge v6, p1, :cond_5

    .line 904
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v6}, Lcom/sec/android/app/DataCopy/DataCopy;->access$300(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/Dft/support/DftFeature;->IsSupportSNote()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 905
    aget-object v6, v3, v1

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/S Note/dft"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".snb"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 911
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0xbb8

    add-long v4, v6, v8

    .line 913
    .local v4, "till":J
    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gez v6, :cond_3

    .line 914
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 916
    .end local v4    # "till":J
    :catch_0
    move-exception v6

    .line 920
    :cond_3
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Fill Up SMemo+ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int v9, v0, v1

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 902
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 907
    :cond_4
    :try_start_1
    aget-object v6, v3, v1

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/SMemo/dft"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".snb"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 901
    :cond_5
    add-int/2addr v0, v2

    goto/16 :goto_1

    .line 924
    .end local v1    # "j":I
    :cond_6
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SMemo+ fill up is done ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 925
    const-string v6, "DataCopy"

    const-string v7, "SMemo+ fill up is done"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private fillUpSchedule(I)V
    .locals 10
    .param p1, "count"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 668
    if-eqz p1, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 672
    :cond_1
    const/4 v3, 0x1

    .line 673
    .local v3, "mScheduleFileExist":Z
    new-instance v0, Lcom/sec/android/app/Dft/DftSchedule;

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/Dft/DftSchedule;-><init>(Landroid/content/Context;)V

    .line 674
    .local v0, "dftSchedule":Lcom/sec/android/app/Dft/DftSchedule;
    const-string v5, "DataCopy"

    const-string v6, "fillUpSchedule"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, p1, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 680
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    invoke-virtual {v5}, Lcom/sec/android/app/Dft/support/DataReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 687
    .local v4, "scheduleDatas":[Ljava/lang/String;
    if-eqz v4, :cond_3

    array-length v5, v4

    const/4 v6, 0x5

    if-lt v5, v6, :cond_3

    .line 688
    iget-object v5, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v5}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 689
    invoke-virtual {v0, v4}, Lcom/sec/android/app/Dft/DftSchedule;->insert([Ljava/lang/String;)V

    .line 690
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Fill Up Schedules..."

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 676
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 681
    .end local v4    # "scheduleDatas":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 682
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 683
    const/4 v3, 0x0

    .line 699
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v3, :cond_4

    .line 700
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Schedules fill up is done"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 701
    const-string v5, "DataCopy"

    const-string v6, "Schedules Fill Up is done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 695
    .restart local v4    # "scheduleDatas":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 703
    .end local v4    # "scheduleDatas":[Ljava/lang/String;
    :cond_4
    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    aput-object v6, v5, v7

    const-string v6, "Schedules fill up failed. Schedule file does not exist"

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->publishProgress([Ljava/lang/Object;)V

    .line 704
    const-string v5, "DataCopy"

    const-string v6, "Schedules Fill Up failed. Schedule file does not exist"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 22
    .param p1, "args"    # [Ljava/lang/Integer;

    .prologue
    .line 405
    const-string v19, "DataCopy"

    const-string v20, "doInBackground"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const/16 v19, 0x0

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 407
    .local v16, "sms_inbox_count":I
    const/16 v19, 0x1

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 408
    .local v17, "sms_outbox_count":I
    const/16 v19, 0x2

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 409
    .local v18, "sms_sentbox_count":I
    const/16 v19, 0x3

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 410
    .local v15, "sms_draftbox_count":I
    const/16 v19, 0x4

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 411
    .local v9, "mms_inbox_count":I
    const/16 v19, 0x5

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 412
    .local v10, "mms_outbox_count":I
    const/16 v19, 0x6

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 413
    .local v11, "mms_sentbox_count":I
    const/16 v19, 0x7

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 414
    .local v8, "mms_draftbox_count":I
    const/16 v19, 0x8

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 415
    .local v3, "contact_count":I
    const/16 v19, 0x9

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 416
    .local v14, "schedule_count":I
    const/16 v19, 0xa

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 417
    .local v6, "memo_count":I
    const/16 v19, 0xb

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 418
    .local v13, "pen_memo_count":I
    const/16 v19, 0xc

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 419
    .local v5, "incoming_call_count":I
    const/16 v19, 0xd

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 420
    .local v12, "outgoing_call_count":I
    const/16 v19, 0xe

    aget-object v19, p1, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 421
    .local v7, "missed_call_count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    move-object/from16 v19, v0

    add-int v20, v16, v17

    add-int v20, v20, v18

    add-int v20, v20, v15

    add-int v20, v20, v9

    add-int v20, v20, v10

    add-int v20, v20, v11

    add-int v20, v20, v8

    add-int v20, v20, v3

    add-int v20, v20, v14

    add-int v20, v20, v6

    add-int v20, v20, v13

    add-int v20, v20, v5

    add-int v20, v20, v12

    add-int v20, v20, v7

    invoke-virtual/range {v19 .. v20}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 428
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "SMS.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 429
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpSMS(II)V

    .line 430
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpSMS(II)V

    .line 431
    const/16 v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpSMS(II)V

    .line 432
    const/16 v19, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v15}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpSMS(II)V

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 434
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "MMS.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 435
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v9}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpMMS(II)V

    .line 436
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v10}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpMMS(II)V

    .line 437
    const/16 v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v11}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpMMS(II)V

    .line 438
    const/16 v19, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v8}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpMMS(II)V

    .line 439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 440
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Contact.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 441
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpContact(I)V

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 443
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Schedule.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 444
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpSchedule(I)V

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 446
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Memo.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->access$300(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->access$300(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportNMemo()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 449
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpNMemo(I)V

    .line 454
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 457
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->access$300(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v19

    if-eqz v19, :cond_1

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->access$300(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DftFeature;->IsSupportSNote()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    const-string v20, "com.sec.android.app.snotebook"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 463
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpSMemo2(I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :cond_1
    :goto_2
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "calllog.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 472
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpCallLog(II)V

    .line 473
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v12}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpCallLog(II)V

    .line 474
    const/16 v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpCallLog(II)V

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 478
    :cond_2
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    return-object v19

    .line 451
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpMemo(I)V

    goto/16 :goto_0

    .line 461
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/DataCopy/DataCopy;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    const-string v20, "com.sec.android.provider.snote"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 465
    :catch_0
    move-exception v4

    .line 466
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v19, Lcom/sec/android/app/Dft/support/DataReader;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/DataCopy/DataCopy;->access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Memo.txt"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/Dft/support/DataReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    .line 467
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->fillUpPenMemo(I)V

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mDataReader:Lcom/sec/android/app/Dft/support/DataReader;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    goto/16 :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 377
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 520
    const-string v1, "DataCopy"

    const-string v2, "onCancelled"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 523
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 525
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 526
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/DataCopy/DataCopy;->setWriteMessageMode(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/DataCopy/DataCopy;->access$400(Lcom/sec/android/app/DataCopy/DataCopy;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    .line 536
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    const-string v2, "  Stop DataCopy!!! "

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 537
    return-void

    .line 529
    :catch_0
    move-exception v0

    .line 530
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 532
    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    throw v1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 493
    const-string v1, "DataCopy"

    const-string v2, "onPostExcute"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 497
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 498
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/DataCopy/DataCopy;->setWriteMessageMode(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/DataCopy/DataCopy;->access$400(Lcom/sec/android/app/DataCopy/DataCopy;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    .line 508
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v1}, Lcom/sec/android/app/DataCopy/DataCopy;->access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 509
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    const-string v2, "Memory is in low space"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 512
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTask:Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;
    invoke-static {v1}, Lcom/sec/android/app/DataCopy/DataCopy;->access$500(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    # getter for: Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTask:Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;
    invoke-static {v1}, Lcom/sec/android/app/DataCopy/DataCopy;->access$500(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;->cancel()Z

    .line 514
    const-string v1, "DataCopy"

    const-string v2, "Response DataCopy End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.intent.DataCopy.END"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/DataCopy/DataCopy;->sendBroadcast(Landroid/content/Intent;)V

    .line 517
    :cond_3
    return-void

    .line 501
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    throw v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 377
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 396
    const-string v0, "DataCopy"

    const-string v1, "onPreExcute"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    const-string v1, "Waiting..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 401
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 483
    aget-object v0, p1, v2

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_SHOW_TOAST:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mContext:Landroid/content/Context;

    aget-object v1, p1, v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    aget-object v0, p1, v2

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->MSG_UPDATE_PROGRESS:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    aget-object v1, p1, v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgress:Landroid/app/ProgressDialog;

    iget v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgressCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->mProgressCount:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0
.end method
