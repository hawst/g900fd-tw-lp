.class public Lcom/sec/android/app/Dft/support/DataReader;
.super Ljava/lang/Object;
.source "DataReader.java"


# instance fields
.field private mExceptionFlag:Z

.field private mFilename:Ljava/lang/String;

.field private mReader:Ljava/io/BufferedReader;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-boolean v5, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    .line 16
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mFilename:Ljava/lang/String;

    .line 17
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v4, "UTF-16"

    invoke-direct {v2, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mReader:Ljava/io/BufferedReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    iget-boolean v1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    if-eqz v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 25
    iput-boolean v5, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    .line 28
    :cond_0
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    .line 21
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23
    iget-boolean v1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    if-eqz v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 25
    iput-boolean v5, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    goto :goto_0

    .line 23
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iget-boolean v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    if-eqz v2, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 25
    iput-boolean v5, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    :cond_1
    throw v1
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 60
    iget-object v1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mReader:Ljava/io/BufferedReader;

    if-eqz v1, :cond_0

    .line 62
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/Dft/support/DataReader;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public readLine()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 31
    const/4 v1, 0x0

    .line 34
    .local v1, "s":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 36
    if-nez v1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 38
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lcom/sec/android/app/Dft/support/DataReader;->mFilename:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string v5, "UTF-16"

    invoke-direct {v3, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mReader:Ljava/io/BufferedReader;

    .line 40
    iget-object v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 46
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    if-eqz v2, :cond_1

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 48
    iput-boolean v6, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    .line 52
    :cond_1
    :goto_0
    if-nez v1, :cond_2

    .line 53
    const-string v1, ""

    .line 56
    :cond_2
    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    .line 44
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    iget-boolean v2, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    if-eqz v2, :cond_1

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 48
    iput-boolean v6, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    goto :goto_0

    .line 46
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget-boolean v3, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    if-eqz v3, :cond_3

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/support/DataReader;->close()V

    .line 48
    iput-boolean v6, p0, Lcom/sec/android/app/Dft/support/DataReader;->mExceptionFlag:Z

    :cond_3
    throw v2
.end method
