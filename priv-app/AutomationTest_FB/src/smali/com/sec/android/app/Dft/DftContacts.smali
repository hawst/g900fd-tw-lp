.class public Lcom/sec/android/app/Dft/DftContacts;
.super Lcom/sec/android/app/Dft/DftObject;
.source "DftContacts.java"


# instance fields
.field private final CONTENT_URI_CONTACTS:Landroid/net/Uri;

.field private mBuilder:Landroid/content/ContentProviderOperation$Builder;

.field private mOperationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const-string v0, "DftContacts"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/Dft/DftObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftContacts;->CONTENT_URI_CONTACTS:Landroid/net/Uri;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    .line 40
    return-void
.end method


# virtual methods
.method public getCount(I)I
    .locals 11
    .param p1, "type"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 503
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 504
    .local v6, "contacs":[I
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftContacts;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/Dft/DftContacts;->CONTENT_URI_CONTACTS:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "account_type"

    aput-object v3, v2, v9

    const-string v3, "contact_id IS NOT NULL"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 508
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 512
    :cond_0
    const-string v0, "account_type"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 515
    .local v7, "contactsType":Ljava/lang/String;
    const-string v0, "vnd.sec.contact.sim"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 516
    aget v0, v6, v9

    add-int/lit8 v0, v0, 0x1

    aput v0, v6, v9

    .line 520
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 523
    .end local v7    # "contactsType":Ljava/lang/String;
    :cond_1
    if-eqz v8, :cond_2

    .line 524
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 527
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftContacts;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "count(type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, v6, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    aget v0, v6, p1

    return v0

    .line 518
    .restart local v7    # "contactsType":Ljava/lang/String;
    :cond_3
    aget v0, v6, v10

    add-int/lit8 v0, v0, 0x1

    aput v0, v6, v10

    goto :goto_0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "firstName"    # Ljava/lang/String;
    .param p2, "lastName"    # Ljava/lang/String;
    .param p3, "pictureFilePath"    # Ljava/lang/String;
    .param p4, "mobilePhone"    # Ljava/lang/String;
    .param p5, "homePhone"    # Ljava/lang/String;
    .param p6, "workPhone"    # Ljava/lang/String;
    .param p7, "other1Phone"    # Ljava/lang/String;
    .param p8, "other2Phone"    # Ljava/lang/String;
    .param p9, "homeEmail"    # Ljava/lang/String;
    .param p10, "workEmail"    # Ljava/lang/String;
    .param p11, "mobileEmail"    # Ljava/lang/String;
    .param p12, "otherEmail"    # Ljava/lang/String;
    .param p13, "chatGoogle"    # Ljava/lang/String;
    .param p14, "group"    # I
    .param p15, "customRingtone"    # Ljava/lang/String;
    .param p16, "postalAddress"    # Ljava/lang/String;
    .param p17, "postalPO"    # Ljava/lang/String;
    .param p18, "postalNeighbor"    # Ljava/lang/String;
    .param p19, "postalCity"    # Ljava/lang/String;
    .param p20, "postalState"    # Ljava/lang/String;
    .param p21, "postalZip"    # Ljava/lang/String;
    .param p22, "postalCountry"    # Ljava/lang/String;
    .param p23, "companyName"    # Ljava/lang/String;
    .param p24, "jobtitleName"    # Ljava/lang/String;
    .param p25, "notes"    # Ljava/lang/String;
    .param p26, "nickname"    # Ljava/lang/String;
    .param p27, "webAddress"    # Ljava/lang/String;
    .param p28, "flag"    # Z

    .prologue
    .line 320
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 321
    .local v2, "rawContactIdIndex":I
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inser contacts - firstName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lastName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "custom_ringtone"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://media/internal/audio/media/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p15

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 326
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 330
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/name"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data3"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 333
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    if-eqz p3, :cond_1

    .line 337
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 339
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data15"

    invoke-static {p3}, Lcom/sec/android/app/Dft/support/DftUtil;->makeBitmapStream(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 340
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "is_super_primary"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 342
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    :goto_0
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 352
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    invoke-virtual {v3, v4, p4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 353
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "is_primary"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 354
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 359
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 360
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    invoke-virtual {v3, v4, p5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 361
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 365
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 367
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    invoke-virtual {v3, v4, p6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 371
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 373
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 374
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    invoke-virtual {v3, v4, p7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 375
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 378
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 380
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 381
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    invoke-virtual {v3, v4, p8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 382
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 385
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 386
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 387
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 388
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p9

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 389
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 392
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 393
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 394
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 395
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p10

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 396
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 399
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 400
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 401
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 402
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p11

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 406
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 407
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 408
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 409
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p12

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 410
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 413
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 414
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/im"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 415
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 416
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data5"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 417
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p13

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 418
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 421
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 422
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 423
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 424
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 427
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 428
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 429
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data2"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 430
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data4"

    move-object/from16 v0, p16

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 431
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data5"

    move-object/from16 v0, p17

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 432
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data6"

    move-object/from16 v0, p18

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 433
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data7"

    move-object/from16 v0, p19

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 434
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data8"

    move-object/from16 v0, p20

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 435
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data9"

    move-object/from16 v0, p21

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 436
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data10"

    move-object/from16 v0, p22

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 437
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 440
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 441
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/note"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 442
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p25

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 443
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 446
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 447
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/nickname"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 448
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p26

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 449
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 452
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 453
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/website"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 454
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p27

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 455
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    .line 458
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "raw_contact_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 459
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/organization"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 460
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data1"

    move-object/from16 v0, p23

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 461
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    const-string v4, "data4"

    move-object/from16 v0, p24

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 462
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftContacts;->mBuilder:Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    const-string v3, "JUST DB TEST"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "applyBatch rawContactIdIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    if-eqz p28, :cond_0

    .line 484
    const-string v3, "JUST DB TEST"

    const-string v4, "applyBatch(ContactsContract.AUTHORITY, mOperationList) D2"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.contacts"

    iget-object v5, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 488
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 495
    :cond_0
    :goto_1
    return-void

    .line 344
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftContacts;->TAG:Ljava/lang/String;

    const-string v4, "DFT Contact Image is NULL."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 489
    :catch_0
    move-exception v1

    .line 490
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 491
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 492
    .local v1, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v1}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_1
.end method

.method public insert([Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 30
    .param p1, "contactsData"    # [Ljava/lang/String;
    .param p2, "loaddataPath"    # Ljava/lang/String;
    .param p3, "flag"    # Z

    .prologue
    .line 272
    const/4 v1, 0x0

    aget-object v2, p1, v1

    const/4 v1, 0x1

    aget-object v3, p1, v1

    const/4 v1, 0x2

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/Dft/support/DftUtil;->makeImageFilePath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x3

    aget-object v5, p1, v1

    const/4 v1, 0x4

    aget-object v6, p1, v1

    const/4 v1, 0x5

    aget-object v7, p1, v1

    const/4 v1, 0x6

    aget-object v8, p1, v1

    const/4 v1, 0x7

    aget-object v9, p1, v1

    const/16 v1, 0x8

    aget-object v10, p1, v1

    const/16 v1, 0x9

    aget-object v11, p1, v1

    const/16 v1, 0xa

    aget-object v12, p1, v1

    const/16 v1, 0xb

    aget-object v13, p1, v1

    const/16 v1, 0xc

    aget-object v14, p1, v1

    const/16 v1, 0xd

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    const/16 v1, 0xe

    aget-object v16, p1, v1

    const/16 v1, 0xf

    aget-object v17, p1, v1

    const/16 v1, 0x10

    aget-object v18, p1, v1

    const/16 v1, 0x11

    aget-object v19, p1, v1

    const/16 v1, 0x12

    aget-object v20, p1, v1

    const/16 v1, 0x13

    aget-object v21, p1, v1

    const/16 v1, 0x14

    aget-object v22, p1, v1

    const/16 v1, 0x15

    aget-object v23, p1, v1

    const/16 v1, 0x16

    aget-object v24, p1, v1

    const/16 v1, 0x17

    aget-object v25, p1, v1

    const/16 v1, 0x18

    aget-object v26, p1, v1

    const/16 v1, 0x19

    aget-object v27, p1, v1

    const/16 v1, 0x1a

    aget-object v28, p1, v1

    move-object/from16 v1, p0

    move/from16 v29, p3

    invoke-virtual/range {v1 .. v29}, Lcom/sec/android/app/Dft/DftContacts;->insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 302
    return-void
.end method
