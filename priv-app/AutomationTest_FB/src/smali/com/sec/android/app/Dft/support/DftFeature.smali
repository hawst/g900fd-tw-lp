.class public Lcom/sec/android/app/Dft/support/DftFeature;
.super Ljava/lang/Object;
.source "DftFeature.java"


# instance fields
.field private final PACKAGE_CONTACTS:Ljava/lang/String;

.field private final PACKAGE_JCONTACTS:Ljava/lang/String;

.field private final PACKAGE_MEMO:Ljava/lang/String;

.field private final PACKAGE_MESSAGE:Ljava/lang/String;

.field private final PACKAGE_NMEMO:Ljava/lang/String;

.field private final PACKAGE_PEN_MEMO:Ljava/lang/String;

.field private final PACKAGE_SCHEDULE:Ljava/lang/String;

.field private final PACKAGE_SMEMO:Ljava/lang/String;

.field private final PACKAGE_SNOTE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIsSupportCall:Z

.field private mIsSupportContacts:Z

.field private mIsSupportMemo:Z

.field private mIsSupportMessage:Z

.field private mIsSupportNMemo:Z

.field private mIsSupportPenMemo:Z

.field private mIsSupportSMemo:Z

.field private mIsSupportSNote:Z

.field private mIsSupportSchedule:Z

.field private mIsSupportUSBStorage:Z

.field private mIsSupportUserStorage1:Z

.field private mIsSupportUserStorage2:Z

.field private mPathOfUSBStorage:Ljava/lang/String;

.field private mPathOfUserStorage1:Ljava/lang/String;

.field private mPathOfUserStorage2:Ljava/lang/String;

.field private mPm:Landroid/os/PersonaManager;

.field private mStorageManager:Landroid/os/storage/StorageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "com.android.mms"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_MESSAGE:Ljava/lang/String;

    .line 25
    const-string v0, "com.android.contacts"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_CONTACTS:Ljava/lang/String;

    .line 26
    const-string v0, "com.android.jcontacts"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_JCONTACTS:Ljava/lang/String;

    .line 27
    const-string v0, "com.sec.android.app.memo"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_MEMO:Ljava/lang/String;

    .line 28
    const-string v0, "com.samsung.android.app.memo"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_NMEMO:Ljava/lang/String;

    .line 29
    const-string v0, "com.sec.android.widgetapp.diotek.smemo"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_SMEMO:Ljava/lang/String;

    .line 30
    const-string v0, "com.sec.android.app.snotebook"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_SNOTE:Ljava/lang/String;

    .line 31
    const-string v0, "com.diotek.penmemo"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_PEN_MEMO:Ljava/lang/String;

    .line 32
    const-string v0, "com.android.calendar"

    iput-object v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->PACKAGE_SCHEDULE:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mPm:Landroid/os/PersonaManager;

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/Dft/support/DftFeature;->initialize(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/Dft/support/DftFeature;->mContext:Landroid/content/Context;

    .line 66
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mContext:Landroid/content/Context;

    const-string v15, "phone"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/telephony/TelephonyManager;

    .line 68
    .local v10, "tm":Landroid/telephony/TelephonyManager;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mContext:Landroid/content/Context;

    const-string v15, "storage"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/os/storage/StorageManager;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 69
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v14

    if-eqz v14, :cond_2

    const/4 v14, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportCall:Z

    .line 73
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v11

    .line 74
    .local v11, "uid":I
    const-string v14, "DftFeature"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "@@ uid"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mContext:Landroid/content/Context;

    const-string v15, "persona"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/os/PersonaManager;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mPm:Landroid/os/PersonaManager;

    .line 77
    const/4 v7, 0x0

    .line 79
    .local v7, "isPersona":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mPm:Landroid/os/PersonaManager;

    if-eqz v14, :cond_0

    .line 80
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v14, v11}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v7

    .line 81
    const-string v14, "DftFeature"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "@@ isPersona"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    if-nez v7, :cond_b

    .line 86
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 87
    .local v9, "pm":Landroid/content/pm/PackageManager;
    const/16 v14, 0x2000

    invoke-virtual {v9, v14}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v5

    .line 90
    .local v5, "installedPackageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_15

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    .line 91
    .local v4, "info":Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMessage:Z

    if-nez v14, :cond_3

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.mms"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 92
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMessage:Z

    goto :goto_1

    .line 69
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "installedPackageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v7    # "isPersona":Z
    .end local v9    # "pm":Landroid/content/pm/PackageManager;
    .end local v11    # "uid":I
    :cond_2
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 93
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "info":Landroid/content/pm/ApplicationInfo;
    .restart local v5    # "installedPackageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .restart local v7    # "isPersona":Z
    .restart local v9    # "pm":Landroid/content/pm/PackageManager;
    .restart local v11    # "uid":I
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportContacts:Z

    if-nez v14, :cond_5

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.contacts"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_4

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.jcontacts"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 94
    :cond_4
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportContacts:Z

    goto :goto_1

    .line 95
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    if-nez v14, :cond_6

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.sec.android.app.memo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 96
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    goto :goto_1

    .line 97
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    if-nez v14, :cond_7

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.samsung.android.app.memo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 98
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    .line 99
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportNMemo:Z

    goto :goto_1

    .line 100
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    if-nez v14, :cond_8

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.sec.android.widgetapp.diotek.smemo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 101
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    goto/16 :goto_1

    .line 102
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    if-nez v14, :cond_9

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.sec.android.app.snotebook"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 103
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    .line 104
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSNote:Z

    goto/16 :goto_1

    .line 105
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportPenMemo:Z

    if-nez v14, :cond_a

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.diotek.penmemo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 106
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportPenMemo:Z

    goto/16 :goto_1

    .line 107
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSchedule:Z

    if-nez v14, :cond_1

    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.calendar"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 108
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSchedule:Z

    goto/16 :goto_1

    .line 113
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "installedPackageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v9    # "pm":Landroid/content/pm/PackageManager;
    :cond_b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 114
    .restart local v9    # "pm":Landroid/content/pm/PackageManager;
    const/4 v14, 0x1

    invoke-virtual {v9, v14, v11}, Landroid/content/pm/PackageManager;->getInstalledPackages(II)Ljava/util/List;

    move-result-object v6

    .line 115
    .local v6, "installedPackageList2":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const-string v14, "DftFeature"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "@@ installedPackageList2"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const-string v14, "DftFeature"

    const-string v15, "@@ isPersona1"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_c
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_15

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInfo;

    .line 118
    .local v4, "info":Landroid/content/pm/PackageInfo;
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMessage:Z

    if-nez v14, :cond_d

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.mms"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 119
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMessage:Z

    goto :goto_2

    .line 120
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportContacts:Z

    if-nez v14, :cond_f

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.contacts"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_e

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.jcontacts"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 121
    :cond_e
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportContacts:Z

    goto :goto_2

    .line 122
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    if-nez v14, :cond_10

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.sec.android.app.memo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 123
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    goto :goto_2

    .line 124
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    if-nez v14, :cond_11

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.samsung.android.app.memo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 125
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    .line 126
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportNMemo:Z

    goto :goto_2

    .line 127
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    if-nez v14, :cond_12

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.sec.android.widgetapp.diotek.smemo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_12

    .line 128
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    goto/16 :goto_2

    .line 129
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    if-nez v14, :cond_13

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.sec.android.app.snotebook"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_13

    .line 130
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    .line 131
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSNote:Z

    goto/16 :goto_2

    .line 132
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportPenMemo:Z

    if-nez v14, :cond_14

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.diotek.penmemo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_14

    .line 133
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportPenMemo:Z

    goto/16 :goto_2

    .line 134
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSchedule:Z

    if-nez v14, :cond_c

    iget-object v14, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.calendar"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 135
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSchedule:Z

    goto/16 :goto_2

    .line 140
    .end local v4    # "info":Landroid/content/pm/PackageInfo;
    .end local v6    # "installedPackageList2":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v14}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v13

    .line 142
    .local v13, "volumes":[Landroid/os/storage/StorageVolume;
    move-object v2, v13

    .local v2, "arr$":[Landroid/os/storage/StorageVolume;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_3
    if-ge v3, v8, :cond_19

    aget-object v12, v2, v3

    .line 143
    .local v12, "volume":Landroid/os/storage/StorageVolume;
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage1:Z

    if-nez v14, :cond_17

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/Dft/support/DftFeature;->isMountedStorage(I)Z

    move-result v14

    if-eqz v14, :cond_17

    .line 144
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage1:Z

    .line 145
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mPathOfUserStorage1:Ljava/lang/String;

    .line 142
    :cond_16
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 146
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage2:Z

    if-nez v14, :cond_18

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/Dft/support/DftFeature;->isMountedStorage(I)Z

    move-result v14

    if-eqz v14, :cond_18

    .line 147
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage2:Z

    .line 148
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mPathOfUserStorage2:Ljava/lang/String;

    goto :goto_4

    .line 149
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUSBStorage:Z

    if-nez v14, :cond_16

    const/4 v14, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/Dft/support/DftFeature;->isMountedStorage(I)Z

    move-result v14

    if-eqz v14, :cond_16

    .line 150
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUSBStorage:Z

    .line 151
    invoke-virtual {v12}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mPathOfUSBStorage:Ljava/lang/String;

    goto :goto_4

    .line 155
    .end local v12    # "volume":Landroid/os/storage/StorageVolume;
    :cond_19
    const-string v14, "DftFeature"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "call="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportCall:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", msg="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMessage:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", contacts="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportContacts:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", memo="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", s-memo="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", pen-memo="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportPenMemo:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", Schedule="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSchedule:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", User1="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage1:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", User2="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage2:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", Usb="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUSBStorage:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void
.end method

.method private isMountedStorage(I)Z
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v3, 0x0

    .line 163
    iget-object v4, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v2

    .line 165
    .local v2, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v4, v2

    if-ge p1, v4, :cond_0

    aget-object v4, v2, p1

    if-nez v4, :cond_1

    .line 166
    :cond_0
    const-string v4, "DftFeature"

    const-string v5, "isMountedStorage: StorageVolumes[type] is null"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :goto_0
    return v3

    .line 170
    :cond_1
    aget-object v4, v2, p1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "path":Ljava/lang/String;
    const-string v4, "DftFeature"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isMountedStorage: path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    if-eqz v0, :cond_2

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v3, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "state":Ljava/lang/String;
    const-string v3, "DftFeature"

    const-string v4, "isMountedStorage: Environment.MEDIA_MOUNTED : mounted"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const-string v3, "DftFeature"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isMountedStorage: state : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const-string v3, "mounted"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    .line 180
    .end local v1    # "state":Ljava/lang/String;
    :cond_2
    const-string v4, "DftFeature"

    const-string v5, "isMountedStorage: another error"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public IsSupportSNote()Z
    .locals 1

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSNote:Z

    return v0
.end method

.method public isSupportCall()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportCall:Z

    return v0
.end method

.method public isSupportContacts()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportContacts:Z

    return v0
.end method

.method public isSupportMemo()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMemo:Z

    return v0
.end method

.method public isSupportMessage()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportMessage:Z

    return v0
.end method

.method public isSupportNMemo()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportNMemo:Z

    return v0
.end method

.method public isSupportPenMemo()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportPenMemo:Z

    return v0
.end method

.method public isSupportSchedule()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSchedule:Z

    return v0
.end method

.method public isSupportSmemo()Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportSMemo:Z

    return v0
.end method

.method public isSupportUserStorage1()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage1:Z

    return v0
.end method

.method public isSupportUserStorage2()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/sec/android/app/Dft/support/DftFeature;->mIsSupportUserStorage2:Z

    return v0
.end method
