.class public Lcom/sec/android/app/Dft/DftObject;
.super Ljava/lang/Object;
.source "DftObject.java"


# instance fields
.field protected TAG:Ljava/lang/String;

.field protected feature:Lcom/sec/android/app/Dft/support/DftFeature;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, "DftObject"

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftObject;->TAG:Ljava/lang/String;

    .line 16
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftObject;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Create "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/Dft/DftObject;->mContext:Landroid/content/Context;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/Dft/DftObject;->TAG:Ljava/lang/String;

    .line 19
    new-instance v0, Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-direct {v0, p1}, Lcom/sec/android/app/Dft/support/DftFeature;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftObject;->feature:Lcom/sec/android/app/Dft/support/DftFeature;

    .line 20
    return-void
.end method
