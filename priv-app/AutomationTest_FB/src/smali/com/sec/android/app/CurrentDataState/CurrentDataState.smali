.class public Lcom/sec/android/app/CurrentDataState/CurrentDataState;
.super Landroid/app/Activity;
.source "CurrentDataState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;
    }
.end annotation


# instance fields
.field private final CALL_LOG_VIEW_INDEXS:[I

.field private final IMEI_ESN_VIEW_INDEXS:[I

.field private final MEMO_VIEW_INDEXS:[I

.field private final MESSAGE_VIEW_INDEXS:[I

.field private final RINGTONE_VIEW_INDEXS:[I

.field private final SCHEDULE_VIEW_INDEXS:[I

.field private final TAG:Ljava/lang/String;

.field private final USER_SPACE2_VIEW_INDEXS:[I

.field private final mDoubleTypeFormat:Ljava/text/DecimalFormat;

.field private mErrorLog:Landroid/widget/TextView;

.field private mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

.field private mIMEI_ESN:Landroid/widget/TextView;

.field private mIncomingCall:Landroid/widget/TextView;

.field private mMMSDrafts:Landroid/widget/TextView;

.field private mMMSInbox:Landroid/widget/TextView;

.field private mMMSOutbox:Landroid/widget/TextView;

.field private mMMSSentbox:Landroid/widget/TextView;

.field private mMP3User1:Landroid/widget/TextView;

.field private mMP3User2:Landroid/widget/TextView;

.field private mMemo:Landroid/widget/TextView;

.field private mMissedCall:Landroid/widget/TextView;

.field private mOutgoingCall:Landroid/widget/TextView;

.field private mPhoneBook:Landroid/widget/TextView;

.field private mPhoneBookSIM:Landroid/widget/TextView;

.field private mPhotoUser1:Landroid/widget/TextView;

.field private mPhotoUser2:Landroid/widget/TextView;

.field private mRingtone:Landroid/widget/TextView;

.field private mRootView:Landroid/widget/TableLayout;

.field private mSMSDrafts:Landroid/widget/TextView;

.field private mSMSFailed:Landroid/widget/TextView;

.field private mSMSInbox:Landroid/widget/TextView;

.field private mSMSOutbox:Landroid/widget/TextView;

.field private mSMSQueued:Landroid/widget/TextView;

.field private mSMSSentbox:Landroid/widget/TextView;

.field private mSchedule:Landroid/widget/TextView;

.field private mScreen:Landroid/widget/TextView;

.field private mSmemo:Landroid/widget/TextView;

.field private mSnoteUser1:Landroid/widget/TextView;

.field private mSnoteUser2:Landroid/widget/TextView;

.field private mSystemSpace:Landroid/widget/TextView;

.field private mTask:Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;

.field private mVideoUser1:Landroid/widget/TextView;

.field private mVideoUser2:Landroid/widget/TextView;

.field private mVoiceUser1:Landroid/widget/TextView;

.field private mVoiceUser2:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    const-string v0, "CurrentDataState"

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->TAG:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;

    .line 40
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->CALL_LOG_VIEW_INDEXS:[I

    .line 52
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->USER_SPACE2_VIEW_INDEXS:[I

    .line 60
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->MEMO_VIEW_INDEXS:[I

    .line 66
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->SCHEDULE_VIEW_INDEXS:[I

    .line 71
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->MESSAGE_VIEW_INDEXS:[I

    .line 77
    new-array v0, v2, [I

    fill-array-data v0, :array_5

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->RINGTONE_VIEW_INDEXS:[I

    .line 85
    new-array v0, v2, [I

    fill-array-data v0, :array_6

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->IMEI_ESN_VIEW_INDEXS:[I

    .line 199
    return-void

    .line 40
    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data

    .line 52
    :array_1
    .array-data 4
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
    .end array-data

    .line 60
    :array_2
    .array-data 4
        0x18
        0x19
        0x1a
        0x1b
    .end array-data

    .line 66
    :array_3
    .array-data 4
        0x1c
        0x1d
    .end array-data

    .line 71
    :array_4
    .array-data 4
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x23
        0x24
        0x25
        0x26
        0x27
        0x28
        0x29
    .end array-data

    .line 77
    :array_5
    .array-data 4
        0x2a
        0x2b
    .end array-data

    .line 85
    :array_6
    .array-data 4
        0x2e
        0x2f
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mIncomingCall:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhotoUser2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVideoUser2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMP3User2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVoiceUser2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBook:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBookSIM:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMemo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSmemo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mOutgoingCall:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSchedule:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSInbox:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSOutbox:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSSentbox:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSDrafts:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSFailed:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSQueued:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSInbox:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSOutbox:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSSentbox:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMissedCall:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSDrafts:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mRingtone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mScreen:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mIMEI_ESN:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mErrorLog:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSystemSpace:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhotoUser1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVideoUser1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMP3User1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVoiceUser1:Landroid/widget/TextView;

    return-object v0
.end method

.method private initialize()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 103
    const-string v0, "CurrentDataState"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v0, Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-direct {v0, p0}, Lcom/sec/android/app/Dft/support/DftFeature;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    .line 105
    new-instance v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;-><init>(Lcom/sec/android/app/CurrentDataState/CurrentDataState;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mTask:Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;

    .line 106
    const/high16 v0, 0x7f060000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mRootView:Landroid/widget/TableLayout;

    .line 108
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mIncomingCall:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f060002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mOutgoingCall:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f060003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMissedCall:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f060004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSystemSpace:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f060006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhotoUser1:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f060007

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVideoUser1:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f060008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMP3User1:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f060009

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVoiceUser1:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f06000a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser1:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f06000c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhotoUser2:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f06000d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVideoUser2:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f06000e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMP3User2:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f06000f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVoiceUser2:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f060010

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser2:Landroid/widget/TextView;

    .line 126
    const v0, 0x7f060011

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBook:Landroid/widget/TextView;

    .line 127
    const v0, 0x7f060012

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBookSIM:Landroid/widget/TextView;

    .line 129
    const v0, 0x7f060015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSchedule:Landroid/widget/TextView;

    .line 130
    const v0, 0x7f060013

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMemo:Landroid/widget/TextView;

    .line 131
    const v0, 0x7f060014

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSmemo:Landroid/widget/TextView;

    .line 133
    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSInbox:Landroid/widget/TextView;

    .line 134
    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSOutbox:Landroid/widget/TextView;

    .line 135
    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSSentbox:Landroid/widget/TextView;

    .line 136
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSDrafts:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSFailed:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f06001b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSQueued:Landroid/widget/TextView;

    .line 139
    const v0, 0x7f06001c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSInbox:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f06001d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSOutbox:Landroid/widget/TextView;

    .line 141
    const v0, 0x7f06001e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSSentbox:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f06001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSDrafts:Landroid/widget/TextView;

    .line 144
    const v0, 0x7f060020

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mRingtone:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f060021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mScreen:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f060022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mIMEI_ESN:Landroid/widget/TextView;

    .line 150
    const v0, 0x7f060023

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mErrorLog:Landroid/widget/TextView;

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportCall()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->CALL_LOG_VIEW_INDEXS:[I

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBookSIM:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportUserStorage2()Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->USER_SPACE2_VIEW_INDEXS:[I

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 161
    const v0, 0x7f060005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f050021

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMemo()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSmemo()Z

    move-result v0

    if-nez v0, :cond_6

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->MEMO_VIEW_INDEXS:[I

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 181
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSchedule()Z

    move-result v0

    if-nez v0, :cond_3

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->SCHEDULE_VIEW_INDEXS:[I

    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSchedule()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 186
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->MESSAGE_VIEW_INDEXS:[I

    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMessage()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->RINGTONE_VIEW_INDEXS:[I

    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportCall()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v2

    :goto_3
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->IMEI_ESN_VIEW_INDEXS:[I

    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v1}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportCall()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v1}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMessage()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    move v3, v2

    :cond_5
    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setVisibleViews([II)V

    .line 191
    return-void

    .line 171
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMemo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v1}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMemo()Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSmemo()Z

    move-result v0

    if-nez v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSmemo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 171
    goto :goto_4

    :cond_8
    move v0, v3

    .line 182
    goto :goto_1

    :cond_9
    move v0, v3

    .line 186
    goto :goto_2

    :cond_a
    move v0, v3

    .line 188
    goto :goto_3
.end method

.method private setVisibleViews([II)V
    .locals 5
    .param p1, "viewIndexs"    # [I
    .param p2, "visibility"    # I

    .prologue
    .line 194
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget v2, v0, v1

    .line 195
    .local v2, "index":I
    iget-object v4, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mRootView:Landroid/widget/TableLayout;

    invoke-virtual {v4, v2}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/view/View;->setVisibility(I)V

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    .end local v2    # "index":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    const-string v0, "CurrentDataState"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->setContentView(I)V

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->initialize()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mTask:Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 100
    return-void
.end method
