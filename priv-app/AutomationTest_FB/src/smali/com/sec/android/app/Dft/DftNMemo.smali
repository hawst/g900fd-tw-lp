.class public Lcom/sec/android/app/Dft/DftNMemo;
.super Lcom/sec/android/app/Dft/DftObject;
.source "DftNMemo.java"


# static fields
.field public static final MEMO_URI:Landroid/net/Uri;

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mImgUris:[Landroid/net/Uri;

.field private mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

.field private mRandom:Ljava/util/Random;

.field private mSvcConnection:Landroid/content/ServiceConnection;

.field private mVRUris:[Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/Dft/DftNMemo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/Dft/DftNMemo;->TAG:Ljava/lang/String;

    .line 25
    const-string v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/Dft/DftNMemo;->MEMO_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaddata_path"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 48
    sget-object v5, Lcom/sec/android/app/Dft/DftNMemo;->TAG:Ljava/lang/String;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/app/Dft/DftObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    iput-object v6, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 30
    new-instance v5, Lcom/sec/android/app/Dft/DftNMemo$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/Dft/DftNMemo$1;-><init>(Lcom/sec/android/app/Dft/DftNMemo;)V

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mSvcConnection:Landroid/content/ServiceConnection;

    .line 50
    new-instance v5, Ljava/util/Random;

    const-wide/16 v8, 0x3

    invoke-direct {v5, v8, v9}, Ljava/util/Random;-><init>(J)V

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mRandom:Ljava/util/Random;

    .line 52
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    .local v1, "fLD":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v5, "image"

    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 55
    .local v0, "fImg":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v5, v6

    .line 56
    check-cast v5, Ljava/io/FilenameFilter;

    invoke-virtual {v0, v5}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 57
    .local v3, "files":[Ljava/io/File;
    array-length v5, v3

    new-array v5, v5, [Landroid/net/Uri;

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    .line 58
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v3

    if-ge v4, v5, :cond_1

    .line 59
    iget-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    aget-object v7, v3, v4

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    aput-object v7, v5, v4

    .line 58
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 61
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "i":I
    :cond_0
    new-array v5, v10, [Landroid/net/Uri;

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    .line 64
    :cond_1
    new-instance v2, Ljava/io/File;

    const-string v5, "voicememo"

    invoke-direct {v2, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 65
    .local v2, "fVR":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 66
    check-cast v6, Ljava/io/FilenameFilter;

    invoke-virtual {v2, v6}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 67
    .restart local v3    # "files":[Ljava/io/File;
    array-length v5, v3

    new-array v5, v5, [Landroid/net/Uri;

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    .line 68
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    array-length v5, v3

    if-ge v4, v5, :cond_3

    .line 69
    iget-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    aget-object v6, v3, v4

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v5, v4

    .line 68
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 71
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "i":I
    :cond_2
    new-array v5, v10, [Landroid/net/Uri;

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    .line 74
    :cond_3
    sget-object v5, Lcom/sec/android/app/Dft/DftNMemo;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "images: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    sget-object v5, Lcom/sec/android/app/Dft/DftNMemo;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "vrfiles: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/Dft/DftNMemo;Lcom/samsung/android/app/memo/MemoServiceIF;)Lcom/samsung/android/app/memo/MemoServiceIF;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/Dft/DftNMemo;
    .param p1, "x1"    # Lcom/samsung/android/app/memo/MemoServiceIF;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    return-object p1
.end method


# virtual methods
.method public bind()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 79
    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.samsung.android.intent.action.MEMO_SERVICE"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 80
    .local v2, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/Dft/DftNMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 81
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    .line 82
    .local v4, "mResolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v3, 0x0

    .line 83
    .local v3, "mResolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-virtual {v7, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 84
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 85
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "mResolveInfo":Landroid/content/pm/ResolveInfo;
    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 87
    .restart local v3    # "mResolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    iget-object v8, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v6, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 88
    .local v6, "pkg":Ljava/lang/String;
    iget-object v8, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v8, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 89
    .local v0, "cls":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 90
    .local v5, "newintent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v6, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .local v1, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v5, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 92
    iget-object v8, p0, Lcom/sec/android/app/Dft/DftNMemo;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/app/Dft/DftNMemo;->mSvcConnection:Landroid/content/ServiceConnection;

    const/4 v10, 0x1

    invoke-virtual {v8, v5, v9, v10}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 93
    return-void
.end method

.method public getCount()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 138
    const/4 v7, 0x0

    .line 139
    .local v7, "memoCount":I
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftNMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/Dft/DftNMemo;->MEMO_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 141
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 143
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 146
    :cond_0
    return v7
.end method

.method public insert(ILjava/lang/String;)V
    .locals 8
    .param p1, "n"    # I
    .param p2, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    if-nez v4, :cond_0

    .line 135
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v4}, Lcom/samsung/android/app/memo/MemoServiceIF;->createNew()Ljava/lang/String;

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v4, p2}, Lcom/samsung/android/app/memo/MemoServiceIF;->appendText(Ljava/lang/String;)V

    .line 115
    rem-int/lit8 v4, p1, 0x3

    if-nez v4, :cond_1

    .line 116
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mRandom:Ljava/util/Random;

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v1, v4, 0x1

    .line 117
    .local v1, "last":I
    if-lez v1, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 118
    const/4 v4, 0x0

    invoke-virtual {p2, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v4, v3}, Lcom/samsung/android/app/memo/MemoServiceIF;->setTitle(Ljava/lang/String;)V

    .line 123
    .end local v1    # "last":I
    .end local v3    # "title":Ljava/lang/String;
    :cond_1
    rem-int/lit8 v4, p1, 0x5

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    array-length v4, v4

    if-lez v4, :cond_2

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mRandom:Ljava/util/Random;

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    .line 125
    .local v2, "max":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_2

    .line 126
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    iget-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    iget-object v6, p0, Lcom/sec/android/app/Dft/DftNMemo;->mRandom:Ljava/util/Random;

    iget-object v7, p0, Lcom/sec/android/app/Dft/DftNMemo;->mImgUris:[Landroid/net/Uri;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/samsung/android/app/memo/MemoServiceIF;->appendImageByUri(Landroid/net/Uri;)V

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129
    .end local v0    # "i":I
    .end local v2    # "max":I
    :cond_2
    rem-int/lit8 v4, p1, 0x8

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    array-length v4, v4

    if-lez v4, :cond_3

    .line 130
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    iget-object v5, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    iget-object v6, p0, Lcom/sec/android/app/Dft/DftNMemo;->mRandom:Ljava/util/Random;

    iget-object v7, p0, Lcom/sec/android/app/Dft/DftNMemo;->mVRUris:[Landroid/net/Uri;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/samsung/android/app/memo/MemoServiceIF;->setVRByUri(Landroid/net/Uri;)V

    .line 133
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v4}, Lcom/samsung/android/app/memo/MemoServiceIF;->saveCurrent()J

    goto :goto_0
.end method

.method public isBinded()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unbind()V
    .locals 3

    .prologue
    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/Dft/DftNMemo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/Dft/DftNMemo;->mSvcConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 98
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/Dft/DftNMemo;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/Dft/DftNMemo;->TAG:Ljava/lang/String;

    const-string v2, "MemoService has been unbound already"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
