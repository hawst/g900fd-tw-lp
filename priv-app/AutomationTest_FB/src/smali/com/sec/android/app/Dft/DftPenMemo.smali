.class public Lcom/sec/android/app/Dft/DftPenMemo;
.super Ljava/lang/Object;
.source "DftPenMemo.java"


# static fields
.field public static AUTHORITY:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;


# instance fields
.field public filesDir:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field public memoXML:Ljava/lang/String;

.field public switcherImage:Landroid/graphics/Bitmap;

.field public thumbnailImage:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "com.sec.android.widgetapp.q1_penmemo"

    sput-object v0, Lcom/sec/android/app/Dft/DftPenMemo;->AUTHORITY:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/Dft/DftPenMemo;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "PenMemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/Dft/DftPenMemo;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "/mnt/sdcard/smemo"

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->filesDir:Ljava/lang/String;

    .line 87
    iput-object p1, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020002

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->thumbnailImage:Landroid/graphics/Bitmap;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020001

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->switcherImage:Landroid/graphics/Bitmap;

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/Dft/DftPenMemo;->prepareMemoData()V

    .line 92
    return-void
.end method

.method private convertBytesToIS([B)Ljava/io/InputStream;
    .locals 1
    .param p1, "bytes"    # [B

    .prologue
    .line 339
    const/4 v0, 0x0

    .line 340
    .local v0, "byteis":Ljava/io/ByteArrayInputStream;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .end local v0    # "byteis":Ljava/io/ByteArrayInputStream;
    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 341
    .restart local v0    # "byteis":Ljava/io/ByteArrayInputStream;
    return-object v0
.end method

.method private convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "xmlFile"    # Ljava/lang/String;

    .prologue
    .line 309
    const/4 v2, 0x0

    .line 310
    .local v2, "inputstream":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 311
    .local v0, "buffreader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 312
    .local v4, "stringbuilder":Ljava/lang/StringBuilder;
    const-string v3, ""

    .line 315
    .local v3, "line":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, p2, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 323
    :goto_0
    new-instance v0, Ljava/io/BufferedReader;

    .end local v0    # "buffreader":Ljava/io/BufferedReader;
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 324
    .restart local v0    # "buffreader":Ljava/io/BufferedReader;
    new-instance v4, Ljava/lang/StringBuilder;

    .end local v4    # "stringbuilder":Ljava/lang/StringBuilder;
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    .restart local v4    # "stringbuilder":Ljava/lang/StringBuilder;
    :goto_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 328
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 330
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 335
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 317
    :catch_1
    move-exception v1

    .line 319
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getUniqueFilename(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "folder"    # Ljava/io/File;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 214
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 215
    :cond_0
    const/4 v1, 0x0

    .line 233
    :cond_1
    return-object v1

    .line 221
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x14

    if-le v4, v5, :cond_3

    .line 222
    const/16 v4, 0x13

    invoke-virtual {p1, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 225
    :cond_3
    invoke-static {p1}, Lcom/sec/android/app/Dft/DftPenMemo;->stringCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 226
    const/4 v2, 0x1

    .line 229
    .local v2, "i":I
    :goto_0
    const-string v4, "%s_%02d.%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v8

    const/4 v6, 0x1

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object p2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, "curFileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 231
    .local v0, "curFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0
.end method

.method private static stringCheck(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    .local v2, "strbuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 199
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 201
    .local v0, "curChar":C
    const/16 v3, 0x5c

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x2a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x22

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3c

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3e

    if-eq v0, v3, :cond_0

    const/16 v3, 0x7c

    if-ne v0, v3, :cond_1

    .line 204
    :cond_0
    const/16 v3, 0x5f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 206
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 210
    .end local v0    # "curChar":C
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method protected buildSwitcher(Landroid/content/ContentValues;)V
    .locals 9
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 170
    const-string v3, "/"

    .line 171
    .local v3, "folderName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/android/app/Dft/DftPenMemo;->filesDir:Ljava/lang/String;

    invoke-direct {v2, v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 174
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 177
    :cond_0
    const-string v1, "jpg"

    .line 178
    .local v1, "ext":Ljava/lang/String;
    const-string v6, "smemo"

    invoke-static {v2, v6, v1}, Lcom/sec/android/app/Dft/DftPenMemo;->getUniqueFilename(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 179
    .local v4, "sendFilName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 183
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 184
    .local v5, "switcherStream":Ljava/io/FileOutputStream;
    iget-object v6, p0, Lcom/sec/android/app/Dft/DftPenMemo;->switcherImage:Landroid/graphics/Bitmap;

    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 185
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 192
    .end local v5    # "switcherStream":Ljava/io/FileOutputStream;
    :goto_0
    const-string v6, "SwitcherImage"

    invoke-virtual {p1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 188
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected buildThumbnail(Landroid/content/ContentValues;Ljava/lang/String;I)V
    .locals 24
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "id"    # I

    .prologue
    .line 238
    const/16 v3, 0x310

    const/16 v6, 0x4ba

    :try_start_0
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 240
    .local v17, "offscreenBitmap":Landroid/graphics/Bitmap;
    new-instance v18, Landroid/graphics/Canvas;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 241
    .local v18, "offscreenCanvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/text/TextPaint;

    const/4 v3, 0x1

    invoke-direct {v4, v3}, Landroid/text/TextPaint;-><init>(I)V

    .line 242
    .local v4, "textPaint":Landroid/text/TextPaint;
    const/high16 v3, 0x42180000    # 38.0f

    invoke-virtual {v4, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 243
    const/high16 v3, -0x1000000

    invoke-virtual {v4, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 244
    new-instance v2, Landroid/text/DynamicLayout;

    const-string v3, "lib"

    const/16 v5, 0x64

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 245
    .local v2, "layout":Landroid/text/DynamicLayout;
    const/high16 v3, 0x423c0000    # 47.0f

    invoke-virtual {v2}, Landroid/text/DynamicLayout;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v3, v6

    float-to-int v0, v3

    move/from16 v20, v0

    .line 246
    .local v20, "spacing":I
    new-instance v5, Landroid/text/DynamicLayout;

    const/16 v8, 0x310

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v10, 0x3f800000    # 1.0f

    move/from16 v0, v20

    int-to-float v11, v0

    const/4 v12, 0x1

    move-object/from16 v6, p2

    move-object v7, v4

    invoke-direct/range {v5 .. v12}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 248
    .local v5, "textLayout":Landroid/text/DynamicLayout;
    invoke-virtual {v5}, Landroid/text/DynamicLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    const/16 v6, 0x1a

    iput v6, v3, Landroid/text/TextPaint;->baselineShift:I

    .line 249
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Canvas;->save()I

    .line 250
    const/high16 v3, 0x41d00000    # 26.0f

    const/high16 v6, 0x42c80000    # 100.0f

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 251
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/text/DynamicLayout;->draw(Landroid/graphics/Canvas;)V

    .line 252
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Canvas;->restore()V

    .line 253
    const/16 v3, 0xe2

    const/16 v6, 0x12a

    const/4 v7, 0x1

    move-object/from16 v0, v17

    invoke-static {v0, v3, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 255
    .local v23, "thumbnailImage":Landroid/graphics/Bitmap;
    const-string v14, "/mnt/sdcard/Application/SMemo"

    .line 256
    .local v14, "DEFAULT_APP_DIRECTORY":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/cache"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 257
    .local v13, "DEFAULT_APP_CACHE_DIRECTORY":Ljava/lang/String;
    move-object/from16 v21, v13

    .line 258
    .local v21, "strFilePath":Ljava/lang/String;
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .local v15, "cacheDirectory":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->mkdirs()Z

    .line 260
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/thumb"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".png"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 261
    .local v22, "strThumb":Ljava/lang/String;
    const-string v3, "Thumb"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 262
    new-instance v19, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 264
    .local v19, "out":Ljava/io/FileOutputStream;
    if-eqz v19, :cond_0

    if-eqz v23, :cond_0

    .line 265
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 266
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->recycle()V

    .line 269
    :cond_0
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .end local v2    # "layout":Landroid/text/DynamicLayout;
    .end local v4    # "textPaint":Landroid/text/TextPaint;
    .end local v5    # "textLayout":Landroid/text/DynamicLayout;
    .end local v13    # "DEFAULT_APP_CACHE_DIRECTORY":Ljava/lang/String;
    .end local v14    # "DEFAULT_APP_DIRECTORY":Ljava/lang/String;
    .end local v15    # "cacheDirectory":Ljava/io/File;
    .end local v17    # "offscreenBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "offscreenCanvas":Landroid/graphics/Canvas;
    .end local v19    # "out":Ljava/io/FileOutputStream;
    .end local v20    # "spacing":I
    .end local v21    # "strFilePath":Ljava/lang/String;
    .end local v22    # "strThumb":Ljava/lang/String;
    .end local v23    # "thumbnailImage":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 270
    :catch_0
    move-exception v16

    .line 271
    .local v16, "e1":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 277
    const/4 v6, 0x0

    .line 278
    .local v6, "count":I
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 281
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/Dft/DftPenMemo;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 284
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 285
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 286
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 289
    :cond_0
    return v6
.end method

.method public getCount2()I
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 294
    const/4 v7, 0x0

    .line 295
    .local v7, "ret":I
    const-string v3, "IsFolder=\'0\' AND deleted == 0"

    .line 296
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.infraware.provider.SNoteProvider/fileMgr"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "_id"

    aput-object v8, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 299
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 300
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 301
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 304
    :cond_0
    return v7
.end method

.method protected prepareMemoData()V
    .locals 7

    .prologue
    .line 102
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 106
    .local v4, "output":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    const-string v6, "memo.dat"

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/Dft/DftPenMemo;->convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 107
    .local v1, "dataBytes":[B
    invoke-direct {p0, v1}, Lcom/sec/android/app/Dft/DftPenMemo;->convertBytesToIS([B)Ljava/io/InputStream;

    move-result-object v3

    .line 108
    .local v3, "input":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 110
    .local v0, "ch":I
    :goto_0
    const/4 v5, -0x1

    if-eq v0, v5, :cond_0

    .line 111
    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 112
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0

    .line 115
    :cond_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v0    # "ch":I
    .end local v1    # "dataBytes":[B
    .end local v3    # "input":Ljava/io/InputStream;
    :goto_1
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/Dft/DftPenMemo;->memoXML:Ljava/lang/String;

    .line 121
    return-void

    .line 116
    :catch_0
    move-exception v2

    .line 117
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public saveMemoData(ILjava/lang/String;)V
    .locals 8
    .param p1, "index"    # I
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/16 v6, 0x172

    const/4 v5, 0x0

    .line 143
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 144
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "Title"

    const-string v2, "Memo %03d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v1, "Date"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 147
    const-string v1, "IsFavorite"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 148
    const-string v1, "IsLock"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 149
    const-string v1, "Drawing"

    iget-object v2, p0, Lcom/sec/android/app/Dft/DftPenMemo;->memoXML:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 150
    const-string v2, "Text"

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v6, :cond_1

    invoke-virtual {p2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v1, "Tehme"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    const-string v1, "Tag"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v1, "Content"

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v6, :cond_0

    invoke-virtual {p2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .end local p2    # "content":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v1, "LastMode"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    const-string v1, "HasVoice"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 157
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/Dft/DftPenMemo;->buildThumbnail(Landroid/content/ContentValues;Ljava/lang/String;I)V

    .line 158
    invoke-virtual {p0, v0}, Lcom/sec/android/app/Dft/DftPenMemo;->buildSwitcher(Landroid/content/ContentValues;)V

    .line 159
    const-string v1, "CreateDate"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 160
    const-string v1, "isTemp"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/Dft/DftPenMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/Dft/DftPenMemo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 167
    return-void

    .restart local p2    # "content":Ljava/lang/String;
    :cond_1
    move-object v1, p2

    .line 150
    goto :goto_0
.end method
