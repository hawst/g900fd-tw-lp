.class public Lcom/sec/android/app/DataCopy/DataCopy;
.super Landroid/app/Activity;
.source "DataCopy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;,
        Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    }
.end annotation


# instance fields
.field private final CALLLOG_FILE:Ljava/lang/String;

.field private final CONTACT_FILE:Ljava/lang/String;

.field private final DISABLE:Z

.field private final ENABLE:Z

.field private FILL_UP_MAX_CALLLOG_INCOMING_CALL:I

.field private FILL_UP_MAX_CALLLOG_MISSED_CALL:I

.field private FILL_UP_MAX_CALLLOG_OUTGOING_CALL:I

.field private FILL_UP_MAX_CONTACT:I

.field private FILL_UP_MAX_MEMO:I

.field private FILL_UP_MAX_MMS_DRAFTBOX:I

.field private FILL_UP_MAX_MMS_INBOX:I

.field private FILL_UP_MAX_MMS_OUTBOX:I

.field private FILL_UP_MAX_MMS_SENTBOX:I

.field private FILL_UP_MAX_PEN_MEMO:I

.field private FILL_UP_MAX_SCHEDULE:I

.field private FILL_UP_MAX_SMS_DRAFTBOX:I

.field private FILL_UP_MAX_SMS_INBOX:I

.field private FILL_UP_MAX_SMS_OUTBOX:I

.field private FILL_UP_MAX_SMS_SENTBOX:I

.field private LOADDATA_PATH:Ljava/lang/String;

.field private final MEMO_FILE:Ljava/lang/String;

.field private final MMS_FILE:Ljava/lang/String;

.field private final SCHEDULE_FILE:Ljava/lang/String;

.field private final SMS_FILE:Ljava/lang/String;

.field private final SNB_PATH:Ljava/lang/String;

.field private final SPLIT_CHARACTER:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private mAppOps:Landroid/app/AppOpsManager;

.field private mContact:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

.field private mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

.field private mIncomingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mMissedCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mMmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mMmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mMmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mMmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mOutgoingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mPenMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResponseTask:Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

.field private mResponseTimer:Ljava/util/Timer;

.field private mSchedule:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mSmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mSmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mSmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mSmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

.field private mStorageIsLow:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0xa6

    const/16 v3, 0x3e8

    const/16 v2, 0x32

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    const-string v0, "DataCopy"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->TAG:Ljava/lang/String;

    .line 46
    const-string v0, "calllog.txt"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->CALLLOG_FILE:Ljava/lang/String;

    .line 47
    const-string v0, "SMS.txt"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->SMS_FILE:Ljava/lang/String;

    .line 48
    const-string v0, "MMS.txt"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->MMS_FILE:Ljava/lang/String;

    .line 49
    const-string v0, "Memo.txt"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->MEMO_FILE:Ljava/lang/String;

    .line 50
    const-string v0, "Contact.txt"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->CONTACT_FILE:Ljava/lang/String;

    .line 51
    const-string v0, "Schedule.txt"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->SCHEDULE_FILE:Ljava/lang/String;

    .line 52
    const-string v0, "SMemo"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->SNB_PATH:Ljava/lang/String;

    .line 55
    const-string v0, "\t"

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->SPLIT_CHARACTER:Ljava/lang/String;

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->ENABLE:Z

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->DISABLE:Z

    .line 60
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_INBOX:I

    .line 61
    iput v1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_OUTBOX:I

    .line 62
    iput v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_SENTBOX:I

    .line 63
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_DRAFTBOX:I

    .line 64
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_INBOX:I

    .line 65
    iput v1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_OUTBOX:I

    .line 66
    const/16 v0, 0x96

    iput v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_SENTBOX:I

    .line 67
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_DRAFTBOX:I

    .line 68
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CONTACT:I

    .line 69
    iput v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SCHEDULE:I

    .line 70
    iput v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MEMO:I

    .line 71
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_PEN_MEMO:I

    .line 72
    iput v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_INCOMING_CALL:I

    .line 73
    const/16 v0, 0xa8

    iput v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_OUTGOING_CALL:I

    .line 74
    iput v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_MISSED_CALL:I

    .line 93
    iput-boolean v1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z

    .line 94
    new-instance v0, Lcom/sec/android/app/DataCopy/DataCopy$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/DataCopy/DataCopy$1;-><init>(Lcom/sec/android/app/DataCopy/DataCopy;)V

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 972
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/DataCopy/DataCopy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/DataCopy/DataCopy;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/DataCopy/DataCopy;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/Dft/support/DftFeature;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/DataCopy/DataCopy;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/DataCopy/DataCopy;->setWriteMessageMode(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTask:Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/DataCopy/DataCopy;)Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/DataCopy/DataCopy;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    return-object v0
.end method

.method private autoStart(I)V
    .locals 6
    .param p1, "interval"    # I

    .prologue
    .line 371
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTimer:Ljava/util/Timer;

    .line 372
    new-instance v0, Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;-><init>(Lcom/sec/android/app/DataCopy/DataCopy;Lcom/sec/android/app/DataCopy/DataCopy$1;)V

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTask:Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mResponseTask:Lcom/sec/android/app/DataCopy/DataCopy$ResponseTask;

    const-wide/16 v2, 0x0

    int-to-long v4, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 374
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->processFillUpMax()V

    .line 375
    return-void
.end method

.method private fillUp(IIIIIIIIIIIIIII)V
    .locals 4
    .param p1, "smsInCount"    # I
    .param p2, "smsOutCount"    # I
    .param p3, "smsSentCount"    # I
    .param p4, "smsDraftCount"    # I
    .param p5, "mmsInCount"    # I
    .param p6, "mmsOutcount"    # I
    .param p7, "mmsSentCount"    # I
    .param p8, "mmsDraftCount"    # I
    .param p9, "contactCount"    # I
    .param p10, "scheduleCount"    # I
    .param p11, "memoCount"    # I
    .param p12, "penMemoCount"    # I
    .param p13, "incomingCount"    # I
    .param p14, "outgoingCount"    # I
    .param p15, "missedCount"    # I

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->isExistLoaddata()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    new-instance v0, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;-><init>(Lcom/sec/android/app/DataCopy/DataCopy;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    invoke-static {p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xd

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xe

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 363
    :goto_0
    return-void

    .line 361
    :cond_0
    const-string v0, "Load data does not exist"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private getApplicationOperationMode()I
    .locals 5

    .prologue
    .line 205
    const-string v2, "appops"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/DataCopy/DataCopy;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 206
    .local v0, "appOps":Landroid/app/AppOpsManager;
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AppOpsManager;->checkOp(IILjava/lang/String;)I

    move-result v1

    .line 208
    .local v1, "isAllowAppOps":I
    return v1
.end method

.method private initialize()V
    .locals 5

    .prologue
    .line 212
    const-string v3, "DataCopy"

    const-string v4, "initilize"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 215
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 216
    const-string v3, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 217
    const-string v3, "TEST_INTENT_AAA"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 220
    new-instance v3, Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-direct {v3, p0}, Lcom/sec/android/app/Dft/support/DftFeature;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    .line 222
    const v3, 0x7f060025

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 223
    const v3, 0x7f060026

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 224
    const v3, 0x7f060027

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 225
    const v3, 0x7f060028

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 226
    const v3, 0x7f06002a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 227
    const v3, 0x7f06002b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 228
    const v3, 0x7f06002c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 229
    const v3, 0x7f06002d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 231
    const v3, 0x7f06002f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mContact:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 232
    const v3, 0x7f060031

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSchedule:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 233
    const v3, 0x7f060034

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 234
    const v3, 0x7f060036

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mPenMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 236
    const v3, 0x7f060038

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mIncomingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 237
    const v3, 0x7f060039

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mOutgoingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 238
    const v3, 0x7f06003a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMissedCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    .line 240
    const v3, 0x7f06003c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    const v3, 0x7f06003d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    const v3, 0x7f06003e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_INBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 244
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_OUTBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 245
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_SENTBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 246
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_DRAFTBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 247
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_INBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 248
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_OUTBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 249
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_SENTBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 250
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_DRAFTBOX:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mContact:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CONTACT:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSchedule:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SCHEDULE:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 253
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MEMO:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mPenMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_PEN_MEMO:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mIncomingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_INCOMING_CALL:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mOutgoingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_OUTGOING_CALL:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMissedCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    iget v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_MISSED_CALL:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setInt(I)V

    .line 259
    const-string v3, "ro.product.model"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 260
    .local v1, "product":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/Dft/DftStorage;

    invoke-direct {v2, p0}, Lcom/sec/android/app/Dft/DftStorage;-><init>(Landroid/content/Context;)V

    .line 262
    .local v2, "storage":Lcom/sec/android/app/Dft/DftStorage;
    iget-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportUserStorage2()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 263
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/sec/android/app/Dft/DftStorage;->getStoragePath(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/loaddata_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;

    .line 272
    :goto_0
    const v3, 0x7f06003b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    return-void

    .line 267
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/sec/android/app/Dft/DftStorage;->getStoragePath(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/loaddata_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;

    goto :goto_0
.end method

.method private isExistLoaddata()Z
    .locals 2

    .prologue
    .line 366
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/DataCopy/DataCopy;->LOADDATA_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 367
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private processFillUp()V
    .locals 17

    .prologue
    .line 327
    const-string v1, "DataCopy"

    const-string v2, "processFillUp"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsInbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsSentbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsDraftbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mContact:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mSchedule:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mPenMemo:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mIncomingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mOutgoingCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/DataCopy/DataCopy;->mMissedCall:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->getInt()I

    move-result v16

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v16}, Lcom/sec/android/app/DataCopy/DataCopy;->fillUp(IIIIIIIIIIIIIII)V

    .line 333
    return-void
.end method

.method private processFillUpMax()V
    .locals 17

    .prologue
    .line 336
    const-string v1, "DataCopy"

    const-string v2, "processFillUpMax"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_INBOX:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_OUTBOX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_SENTBOX:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_DRAFTBOX:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_INBOX:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_OUTBOX:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_SENTBOX:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_DRAFTBOX:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CONTACT:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SCHEDULE:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MEMO:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_PEN_MEMO:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_INCOMING_CALL:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_OUTGOING_CALL:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_MISSED_CALL:I

    move/from16 v16, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v16}, Lcom/sec/android/app/DataCopy/DataCopy;->fillUp(IIIIIIIIIIIIIII)V

    .line 343
    return-void
.end method

.method private setFeature()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 276
    const-string v0, "DataCopy"

    const-string v1, "setFeature"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mSmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setEnabled(Z)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mMmsOutbox:Lcom/sec/android/app/DataCopy/widget/NumberEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/DataCopy/widget/NumberEditText;->setEnabled(Z)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMessage()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    const v0, 0x7f060024

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 282
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_DRAFTBOX:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_SENTBOX:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_OUTBOX:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SMS_INBOX:I

    .line 283
    const v0, 0x7f060029

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 284
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_DRAFTBOX:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_SENTBOX:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_OUTBOX:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MMS_INBOX:I

    .line 285
    const v0, 0x7f060037

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 286
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_OUTGOING_CALL:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_MISSED_CALL:I

    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CALLLOG_INCOMING_CALL:I

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportContacts()Z

    move-result v0

    if-nez v0, :cond_1

    .line 290
    const v0, 0x7f06002e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 291
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_CONTACT:I

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMemo()Z

    move-result v0

    if-nez v0, :cond_2

    .line 295
    const v0, 0x7f060033

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 296
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_MEMO:I

    .line 299
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSmemo()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportPenMemo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 304
    :cond_3
    const v0, 0x7f060035

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 305
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_PEN_MEMO:I

    .line 308
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    invoke-virtual {v0}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSchedule()Z

    move-result v0

    if-nez v0, :cond_5

    .line 309
    const v0, 0x7f060030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 310
    iput v2, p0, Lcom/sec/android/app/DataCopy/DataCopy;->FILL_UP_MAX_SCHEDULE:I

    .line 312
    :cond_5
    return-void
.end method

.method private setVisibilityGroup(Landroid/view/ViewGroup;I)V
    .locals 3
    .param p1, "group"    # Landroid/view/ViewGroup;
    .param p2, "visibility"    # I

    .prologue
    .line 315
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 316
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 318
    .local v1, "view":Landroid/view/View;
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 319
    check-cast v2, Landroid/view/ViewGroup;

    invoke-direct {p0, v2, p2}, Lcom/sec/android/app/DataCopy/DataCopy;->setVisibilityGroup(Landroid/view/ViewGroup;I)V

    .line 322
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 324
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private setWriteMessageMode(Z)V
    .locals 5
    .param p1, "setEnable"    # Z

    .prologue
    const/16 v4, 0xf

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mAppOps:Landroid/app/AppOpsManager;

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 193
    :cond_0
    if-eqz p1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mAppOps:Landroid/app/AppOpsManager;

    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    goto :goto_0

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mAppOps:Landroid/app/AppOpsManager;

    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 172
    :goto_0
    return-void

    .line 152
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationOperationMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    invoke-direct {p0, v2}, Lcom/sec/android/app/DataCopy/DataCopy;->setWriteMessageMode(Z)V

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->processFillUp()V

    goto :goto_0

    .line 158
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationOperationMode()I

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    invoke-direct {p0, v2}, Lcom/sec/android/app/DataCopy/DataCopy;->setWriteMessageMode(Z)V

    .line 161
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->processFillUpMax()V

    goto :goto_0

    .line 164
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/FileCopy/FileCopy;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 166
    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->startActivity(Landroid/content/Intent;)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->finish()V

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x7f06003c
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 110
    const-string v0, "DataCopy"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 112
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->setContentView(I)V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->initialize()V

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->setFeature()V

    .line 117
    const-string v0, "appops"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mAppOps:Landroid/app/AppOpsManager;

    .line 118
    const-string v0, "DataCopy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() : Application operation Mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationOperationMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 121
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 176
    const-string v0, "DataCopy"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;

    .line 181
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/DataCopy/DataCopy;->setWriteMessageMode(Z)V

    .line 182
    const-string v0, "DataCopy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() : Application operation Mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getApplicationOperationMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 185
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 145
    const-string v0, "DataCopy"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 147
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 125
    const-string v2, "DataCopy"

    const-string v3, "onResume"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "auto_start"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 129
    .local v0, "autoStart":Z
    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/DataCopy/DataCopy;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "response_interval"

    const v4, 0x493e0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 131
    .local v1, "interval":I
    invoke-direct {p0, v1}, Lcom/sec/android/app/DataCopy/DataCopy;->autoStart(I)V

    .line 133
    .end local v1    # "interval":I
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 136
    const-string v0, "DataCopy"

    const-string v1, "onUserLeaveHint"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy;->mFillUpTask:Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/DataCopy/DataCopy$FillUpTask;->cancel(Z)Z

    .line 142
    :cond_0
    return-void
.end method
