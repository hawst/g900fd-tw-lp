.class Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;
.super Landroid/os/AsyncTask;
.source "CurrentDataState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/CurrentDataState/CurrentDataState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/CurrentDataState/CurrentDataState;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 203
    iput-object p2, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    .line 204
    return-void
.end method

.method private processCallLog()V
    .locals 3

    .prologue
    .line 231
    const-string v1, "CurrentDataState"

    const-string v2, "processCallLog"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportCall()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    new-instance v0, Lcom/sec/android/app/Dft/DftCallLog;

    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/Dft/DftCallLog;-><init>(Landroid/content/Context;)V

    .line 235
    .local v0, "dftCallLog":Lcom/sec/android/app/Dft/DftCallLog;
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mIncomingCall:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/Dft/DftCallLog;->getCount(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mOutgoingCall:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/Dft/DftCallLog;->getCount(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMissedCall:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/Dft/DftCallLog;->getCount(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 241
    .end local v0    # "dftCallLog":Lcom/sec/android/app/Dft/DftCallLog;
    :cond_0
    return-void
.end method

.method private processEtc()V
    .locals 7

    .prologue
    .line 416
    const-string v5, "CurrentDataState"

    const-string v6, "processEtc"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    new-instance v0, Lcom/sec/android/app/Dft/DftEtc;

    iget-object v5, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/app/Dft/DftEtc;-><init>(Landroid/content/Context;)V

    .line 418
    .local v0, "dftEtc":Lcom/sec/android/app/Dft/DftEtc;
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftEtc;->getSelectedRingtoneName()Ljava/lang/String;

    move-result-object v3

    .line 419
    .local v3, "ringtone":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftEtc;->getSelectedWallpaperName()Ljava/lang/String;

    move-result-object v4

    .line 420
    .local v4, "wallpaper":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/app/Dft/DftEtc;->getIMEI()Ljava/lang/String;

    move-result-object v2

    .line 421
    .local v2, "imei":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    invoke-virtual {v5}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050020

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 422
    .local v1, "errorLog":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mRingtone:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$3100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 423
    iget-object v5, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mScreen:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$3200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 424
    iget-object v5, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mIMEI_ESN:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$3300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v5

    invoke-direct {p0, v5, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 425
    iget-object v5, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mErrorLog:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$3400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v5

    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 426
    return-void
.end method

.method private processMemo()V
    .locals 6

    .prologue
    .line 342
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMemo()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 343
    const-string v3, "CurrentDataState"

    const-string v4, "processMemo"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportNMemo()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 347
    new-instance v3, Lcom/sec/android/app/Dft/DftNMemo;

    iget-object v4, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    const-string v5, "/"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/Dft/DftNMemo;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/DftNMemo;->getCount()I

    move-result v1

    .line 359
    .local v1, "memoCount":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMemo:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1800(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 362
    .end local v1    # "memoCount":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSmemo()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 363
    const-string v3, "CurrentDataState"

    const-string v4, "processSmemo"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/support/DftFeature;->IsSupportSNote()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    invoke-virtual {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.snotebook"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 373
    :goto_1
    new-instance v3, Lcom/sec/android/app/Dft/DftPenMemo;

    iget-object v4, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/Dft/DftPenMemo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/DftPenMemo;->getCount2()I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 378
    .local v2, "penMemoCount":I
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSmemo:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1900(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 380
    .end local v2    # "penMemoCount":I
    :cond_1
    return-void

    .line 352
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    invoke-virtual {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.provider.snote"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 353
    new-instance v3, Lcom/sec/android/app/Dft/DftMemo;

    iget-object v4, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/Dft/DftMemo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/DftMemo;->getCount2()I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    .restart local v1    # "memoCount":I
    goto :goto_0

    .line 354
    .end local v1    # "memoCount":I
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Lcom/sec/android/app/Dft/DftMemo;

    iget-object v4, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/Dft/DftMemo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/DftMemo;->getCount()I

    move-result v1

    .restart local v1    # "memoCount":I
    goto :goto_0

    .line 370
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v1    # "memoCount":I
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    invoke-virtual {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.provider.snote"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 374
    :catch_1
    move-exception v0

    .line 375
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Lcom/sec/android/app/Dft/DftPenMemo;

    iget-object v4, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/Dft/DftPenMemo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/sec/android/app/Dft/DftPenMemo;->getCount()I

    move-result v2

    .restart local v2    # "penMemoCount":I
    goto :goto_2
.end method

.method private processMessage()V
    .locals 13

    .prologue
    .line 389
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportMessage()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 390
    const-string v11, "CurrentDataState"

    const-string v12, "processMessage"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    new-instance v0, Lcom/sec/android/app/Dft/DftMessage;

    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;-><init>(Landroid/content/Context;)V

    .line 392
    .local v0, "dftMessage":Lcom/sec/android/app/Dft/DftMessage;
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getSMSCount(I)I

    move-result v7

    .line 393
    .local v7, "smsIn":I
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getSMSCount(I)I

    move-result v8

    .line 394
    .local v8, "smsOut":I
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getSMSCount(I)I

    move-result v10

    .line 395
    .local v10, "smsSent":I
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getSMSCount(I)I

    move-result v5

    .line 396
    .local v5, "smsDraft":I
    const/4 v11, 0x4

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getSMSCount(I)I

    move-result v6

    .line 397
    .local v6, "smsFailed":I
    const/4 v11, 0x5

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getSMSCount(I)I

    move-result v9

    .line 398
    .local v9, "smsQueued":I
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getMMSCount(I)I

    move-result v2

    .line 399
    .local v2, "mmsIn":I
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getMMSCount(I)I

    move-result v3

    .line 400
    .local v3, "mmsOut":I
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getMMSCount(I)I

    move-result v4

    .line 401
    .local v4, "mmsSent":I
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, Lcom/sec/android/app/Dft/DftMessage;->getMMSCount(I)I

    move-result v1

    .line 402
    .local v1, "mmsDraft":I
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSInbox:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 403
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSOutbox:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 404
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSSentbox:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 405
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSDrafts:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 406
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSFailed:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2500(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 407
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSMSQueued:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 408
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSInbox:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2700(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 409
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSOutbox:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2800(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 410
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSSentbox:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2900(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 411
    iget-object v11, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMMSDrafts:Landroid/widget/TextView;
    invoke-static {v11}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$3000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v11

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 413
    .end local v0    # "dftMessage":Lcom/sec/android/app/Dft/DftMessage;
    .end local v1    # "mmsDraft":I
    .end local v2    # "mmsIn":I
    .end local v3    # "mmsOut":I
    .end local v4    # "mmsSent":I
    .end local v5    # "smsDraft":I
    .end local v6    # "smsFailed":I
    .end local v7    # "smsIn":I
    .end local v8    # "smsOut":I
    .end local v9    # "smsQueued":I
    .end local v10    # "smsSent":I
    :cond_0
    return-void
.end method

.method private processPhoneBook()V
    .locals 5

    .prologue
    .line 333
    const-string v3, "CurrentDataState"

    const-string v4, "processPhoneBook"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    new-instance v2, Lcom/sec/android/app/Dft/DftContacts;

    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/Dft/DftContacts;-><init>(Landroid/content/Context;)V

    .line 335
    .local v2, "dftContacts":Lcom/sec/android/app/Dft/DftContacts;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/Dft/DftContacts;->getCount(I)I

    move-result v0

    .line 336
    .local v0, "contactsCount":I
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/Dft/DftContacts;->getCount(I)I

    move-result v1

    .line 337
    .local v1, "contactsSIMCount":I
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBook:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhoneBookSIM:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1700(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method private processSchedule()V
    .locals 3

    .prologue
    .line 382
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportSchedule()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    const-string v1, "CurrentDataState"

    const-string v2, "processSchedule"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    new-instance v1, Lcom/sec/android/app/Dft/DftSchedule;

    iget-object v2, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/Dft/DftSchedule;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/app/Dft/DftSchedule;->getCount()I

    move-result v0

    .line 385
    .local v0, "scheduleCount":I
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSchedule:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$2000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 387
    .end local v0    # "scheduleCount":I
    :cond_0
    return-void
.end method

.method private processSystemSpace()V
    .locals 9

    .prologue
    const/high16 v8, 0x100000

    const/4 v7, 0x2

    .line 244
    const-string v1, "CurrentDataState"

    const-string v6, "processSystemSpace"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    new-instance v0, Lcom/sec/android/app/Dft/DftStorage;

    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/Dft/DftStorage;-><init>(Landroid/content/Context;)V

    .line 246
    .local v0, "dftStorage":Lcom/sec/android/app/Dft/DftStorage;
    invoke-virtual {v0, v7, v8}, Lcom/sec/android/app/Dft/DftStorage;->getSize(II)J

    move-result-wide v2

    .line 247
    .local v2, "totalSpace":J
    invoke-virtual {v0, v7, v8}, Lcom/sec/android/app/Dft/DftStorage;->getUseSize(II)J

    move-result-wide v4

    .line 248
    .local v4, "useSpace":J
    iget-object v1, p0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSystemSpace:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "M / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "M"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method private processUserSpace1()V
    .locals 21

    .prologue
    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportUserStorage1()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 254
    const-string v18, "CurrentDataState"

    const-string v19, "processUserSpace1"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    new-instance v6, Lcom/sec/android/app/Dft/DftStorage;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Lcom/sec/android/app/Dft/DftStorage;-><init>(Landroid/content/Context;)V

    .line 256
    .local v6, "dftStorage":Lcom/sec/android/app/Dft/DftStorage;
    const/16 v18, 0x0

    const/high16 v19, 0x100000

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->getPointSize(II)D

    move-result-wide v12

    .line 258
    .local v12, "storageSize":D
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/sec/android/app/Dft/DftStorage;->getStoragePath(I)Ljava/lang/String;

    move-result-object v7

    .line 259
    .local v7, "path":Ljava/lang/String;
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "jpeg"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "jpg"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "gif"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "bmp"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "png"

    aput-object v20, v18, v19

    const/16 v19, 0x5

    const-string v20, "wbmp"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v8

    .line 263
    .local v8, "photoSize":D
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "mp3"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "wav"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "aac"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "smaf"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "wma"

    aput-object v20, v18, v19

    const/16 v19, 0x5

    const-string v20, "imy"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v4

    .line 267
    .local v4, "audioSize":D
    const/16 v18, 0x5

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "mpeg4"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3gp"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "wmv"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "avi"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "mp4"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v14

    .line 271
    .local v14, "videoSize":D
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "amr"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3ga"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v16

    .line 275
    .local v16, "voiceSize":D
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "snb"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v10

    .line 279
    .local v10, "snoteSize":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhotoUser1:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$500(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVideoUser1:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$700(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMP3User1:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$800(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVoiceUser1:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$900(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser1:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 290
    .end local v4    # "audioSize":D
    .end local v6    # "dftStorage":Lcom/sec/android/app/Dft/DftStorage;
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "photoSize":D
    .end local v10    # "snoteSize":D
    .end local v12    # "storageSize":D
    .end local v14    # "videoSize":D
    .end local v16    # "voiceSize":D
    :cond_0
    return-void
.end method

.method private processUserSpace2()V
    .locals 21

    .prologue
    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mFeature:Lcom/sec/android/app/Dft/support/DftFeature;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$000(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Lcom/sec/android/app/Dft/support/DftFeature;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/Dft/support/DftFeature;->isSupportUserStorage2()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 294
    const-string v18, "CurrentDataState"

    const-string v19, "processUserSpace2"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    new-instance v6, Lcom/sec/android/app/Dft/DftStorage;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Lcom/sec/android/app/Dft/DftStorage;-><init>(Landroid/content/Context;)V

    .line 296
    .local v6, "dftStorage":Lcom/sec/android/app/Dft/DftStorage;
    const/16 v18, 0x1

    const/high16 v19, 0x100000

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->getPointSize(II)D

    move-result-wide v12

    .line 298
    .local v12, "storageSize":D
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/sec/android/app/Dft/DftStorage;->getStoragePath(I)Ljava/lang/String;

    move-result-object v7

    .line 299
    .local v7, "path":Ljava/lang/String;
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "jpeg"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "jpg"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "gif"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "bmp"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "png"

    aput-object v20, v18, v19

    const/16 v19, 0x5

    const-string v20, "wbmp"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v8

    .line 303
    .local v8, "photoSize":D
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "mp3"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "wav"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "aac"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "smaf"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "wma"

    aput-object v20, v18, v19

    const/16 v19, 0x5

    const-string v20, "imy"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v4

    .line 307
    .local v4, "audioSize":D
    const/16 v18, 0x5

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "mpeg4"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3gp"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "wmv"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "avi"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "mp4"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v14

    .line 311
    .local v14, "videoSize":D
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "amr"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3ga"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v16

    .line 315
    .local v16, "voiceSize":D
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "snb"

    aput-object v20, v18, v19

    const/high16 v19, 0x100000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/Dft/DftStorage;->findFilesPointSizeByExtension(Ljava/lang/String;[Ljava/lang/String;I)D

    move-result-wide v10

    .line 319
    .local v10, "snoteSize":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mPhotoUser2:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1100(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVideoUser2:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1200(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mMP3User2:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1300(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mVoiceUser2:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1400(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mSnoteUser2:Landroid/widget/TextView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$1500(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Landroid/widget/TextView;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->this$0:Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/CurrentDataState/CurrentDataState;->mDoubleTypeFormat:Ljava/text/DecimalFormat;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/CurrentDataState/CurrentDataState;->access$600(Lcom/sec/android/app/CurrentDataState/CurrentDataState;)Ljava/text/DecimalFormat;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "M"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->updateView(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 330
    .end local v4    # "audioSize":D
    .end local v6    # "dftStorage":Lcom/sec/android/app/Dft/DftStorage;
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "photoSize":D
    .end local v10    # "snoteSize":D
    .end local v12    # "storageSize":D
    .end local v14    # "videoSize":D
    .end local v16    # "voiceSize":D
    :cond_0
    return-void
.end method

.method private updateView(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 429
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->publishProgress([Ljava/lang/Object;)V

    .line 430
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 2
    .param p1, "args"    # [Ljava/lang/Integer;

    .prologue
    .line 208
    const-string v0, "CurrentDataState"

    const-string v1, "doInBackground"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processCallLog()V

    .line 210
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processSystemSpace()V

    .line 211
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processPhoneBook()V

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processMemo()V

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processSchedule()V

    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processMessage()V

    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processEtc()V

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processUserSpace1()V

    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->processUserSpace2()V

    .line 220
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 199
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/CurrentDataState/CurrentDataState$BackgroundTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 225
    if-eqz p1, :cond_0

    aget-object v0, p1, v1

    if-eqz v0, :cond_0

    aget-object v0, p1, v2

    if-eqz v0, :cond_0

    .line 226
    aget-object v0, p1, v1

    check-cast v0, Landroid/widget/TextView;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :cond_0
    return-void
.end method
