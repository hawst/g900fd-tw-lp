.class public Lcom/sec/android/app/Dft/DftMemo;
.super Lcom/sec/android/app/Dft/DftObject;
.source "DftMemo.java"


# instance fields
.field private final CONTENT_URI_MEMO_READ:[Landroid/net/Uri;

.field private final CONTENT_URI_MEMO_WRITE:[Landroid/net/Uri;

.field private mMemoUriIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 25
    const-string v0, "DftMemo"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/Dft/DftObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 14
    new-array v0, v3, [Landroid/net/Uri;

    const-string v1, "content://com.samsung.sec.android/memo"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v7

    const-string v1, "content://com.sec.android.app/memo"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v8

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftMemo;->CONTENT_URI_MEMO_WRITE:[Landroid/net/Uri;

    .line 18
    new-array v0, v3, [Landroid/net/Uri;

    const-string v1, "content://com.samsung.sec.android/memo/all"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v7

    const-string v1, "content://com.sec.android.app/memo"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v0, v8

    iput-object v0, p0, Lcom/sec/android/app/Dft/DftMemo;->CONTENT_URI_MEMO_READ:[Landroid/net/Uri;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/Dft/DftMemo;->CONTENT_URI_MEMO_READ:[Landroid/net/Uri;

    iget v3, p0, Lcom/sec/android/app/Dft/DftMemo;->mMemoUriIndex:I

    aget-object v1, v1, v3

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 29
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    move v0, v7

    :goto_0
    iput v0, p0, Lcom/sec/android/app/Dft/DftMemo;->mMemoUriIndex:I

    .line 31
    if-eqz v6, :cond_0

    .line 32
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 33
    :cond_0
    return-void

    :cond_1
    move v0, v8

    .line 29
    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 80
    const/4 v7, 0x0

    .line 81
    .local v7, "memoCount":I
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/Dft/DftMemo;->CONTENT_URI_MEMO_READ:[Landroid/net/Uri;

    iget v3, p0, Lcom/sec/android/app/Dft/DftMemo;->mMemoUriIndex:I

    aget-object v1, v1, v3

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 84
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 85
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 86
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 89
    :cond_0
    return v7
.end method

.method public getCount2()I
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 93
    const/4 v7, 0x0

    .line 95
    .local v7, "ret":I
    const-string v3, "IsFolder=\'0\' AND deleted == 0"

    .line 96
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.infraware.provider.SNoteProvider/fileMgr"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "_id"

    aput-object v8, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 99
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 100
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 101
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 104
    :cond_0
    return v7
.end method

.method public insert(Ljava/lang/String;)V
    .locals 4
    .param p1, "memo"    # Ljava/lang/String;

    .prologue
    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/Dft/DftMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.provider.snote"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/Dft/DftMemo;->insert2(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/Dft/DftMemo;->insert1(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public insert1(Ljava/lang/String;)V
    .locals 6
    .param p1, "memo"    # Ljava/lang/String;

    .prologue
    .line 36
    iget-object v2, p0, Lcom/sec/android/app/Dft/DftMemo;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inser Memo - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 38
    .local v1, "format":Ljava/text/SimpleDateFormat;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 39
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "title"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v2, "content"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v2, "color"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 42
    const-string v2, "modify_t"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 43
    const-string v2, "create_t"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 44
    const-string v2, "delete_flag"

    const-string v3, "false"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v2, "synch_t"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 46
    const-string v2, "locked"

    const-string v3, "false"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-object v2, p0, Lcom/sec/android/app/Dft/DftMemo;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Memo Uri Index= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/Dft/DftMemo;->mMemoUriIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/Dft/DftMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/Dft/DftMemo;->CONTENT_URI_MEMO_WRITE:[Landroid/net/Uri;

    iget v4, p0, Lcom/sec/android/app/Dft/DftMemo;->mMemoUriIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 49
    return-void
.end method

.method public insert2(Ljava/lang/String;)V
    .locals 8
    .param p1, "memo"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CLIPBOARD_TO_MEMO_INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 54
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "title"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DFT_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v1, "content"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/Dft/DftMemo;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 60
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x258

    add-long v2, v4, v6

    .line 62
    .local v2, "till":J
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-gez v1, :cond_0

    .line 63
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    .end local v2    # "till":J
    :catch_0
    move-exception v1

    .line 68
    :cond_0
    return-void
.end method
