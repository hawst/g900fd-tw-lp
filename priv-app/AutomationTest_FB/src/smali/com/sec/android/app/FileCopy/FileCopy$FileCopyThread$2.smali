.class Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$2;
.super Ljava/lang/Object;
.source "FileCopy.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V
    .locals 0

    .prologue
    .line 858
    iput-object p1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$2;->this$0:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 860
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->onFinish()V

    .line 861
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$2;->this$0:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 863
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$2;->this$0:Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    iget-object v0, v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1700()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 865
    const-string v0, "FileCopy"

    const-string v1, "Broadcasted intent for try media scanning"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    return-void
.end method
