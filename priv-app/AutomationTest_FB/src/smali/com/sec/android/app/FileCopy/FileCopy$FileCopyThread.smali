.class Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;
.super Ljava/lang/Thread;
.source "FileCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FileCopy/FileCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FileCopyThread"
.end annotation


# static fields
.field private static LOADDATA_PATH:Ljava/lang/String;

.field private static LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

.field private static LOADDATA_PATH_INTERNAL:Ljava/lang/String;

.field private static mDstAmrPath:Ljava/lang/String;

.field private static mDstImagePath:Ljava/lang/String;

.field private static mDstMp3Path:Ljava/lang/String;

.field private static mDstSnote3Path:Ljava/lang/String;

.field private static mDstSnotePath:Ljava/lang/String;

.field private static mDstVideoPath:Ljava/lang/String;

.field private static mKNOXSrcAmrPath:Ljava/lang/String;

.field private static mKNOXSrcImagepath:Ljava/lang/String;

.field private static mKNOXSrcMp3Path:Ljava/lang/String;

.field private static mKNOXSrcSnote3Path:Ljava/lang/String;

.field private static mKNOXSrcSnotePath:Ljava/lang/String;

.field private static mKNOXSrcVideoPath:Ljava/lang/String;

.field private static mModelName:Ljava/lang/String;

.field private static mSecondSrcAmrPath:Ljava/lang/String;

.field private static mSecondSrcImagePath:Ljava/lang/String;

.field private static mSecondSrcMp3Path:Ljava/lang/String;

.field private static mSecondSrcSnote3Path:Ljava/lang/String;

.field private static mSecondSrcSnotePath:Ljava/lang/String;

.field private static mSecondSrcVideoPath:Ljava/lang/String;

.field private static mSrcAmrPath:Ljava/lang/String;

.field private static mSrcImagePath:Ljava/lang/String;

.field private static mSrcMp3Path:Ljava/lang/String;

.field private static mSrcSnote3Path:Ljava/lang/String;

.field private static mSrcSnotePath:Ljava/lang/String;

.field private static mSrcVideoPath:Ljava/lang/String;

.field private static mThirdSrcAmrPath:Ljava/lang/String;

.field private static mThirdSrcImagePath:Ljava/lang/String;

.field private static mThirdSrcMp3Path:Ljava/lang/String;

.field private static mThirdSrcSnote3Path:Ljava/lang/String;

.field private static mThirdSrcSnotePath:Ljava/lang/String;

.field private static mThirdSrcVideoPath:Ljava/lang/String;


# instance fields
.field private final MP3TYPE:Ljava/lang/String;

.field private final PHOTOTYPE:Ljava/lang/String;

.field private final SNOTE3TYPE:Ljava/lang/String;

.field private final SNOTETYPE:Ljava/lang/String;

.field private final VIDEOTYPE:Ljava/lang/String;

.field private final VOICETYPE:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field private mPm:Landroid/os/PersonaManager;

.field mp3:I

.field mp3Prefix:Ljava/lang/String;

.field photo:I

.field photoPrefix:Ljava/lang/String;

.field snote:I

.field snote3:I

.field snote3Prefix:Ljava/lang/String;

.field snotePrefix:Ljava/lang/String;

.field video:I

.field videoPrefix:Ljava/lang/String;

.field voicePrefix:Ljava/lang/String;

.field voicememo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 704
    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstImagePath:Ljava/lang/String;

    .line 706
    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstAmrPath:Ljava/lang/String;

    .line 707
    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstMp3Path:Ljava/lang/String;

    .line 708
    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstVideoPath:Ljava/lang/String;

    .line 711
    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnotePath:Ljava/lang/String;

    .line 712
    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnote3Path:Ljava/lang/String;

    .line 714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/loaddata_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    .line 716
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcImagePath:Ljava/lang/String;

    .line 717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "voicememo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcAmrPath:Ljava/lang/String;

    .line 718
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcMp3Path:Ljava/lang/String;

    .line 719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcVideoPath:Ljava/lang/String;

    .line 720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcSnotePath:Ljava/lang/String;

    .line 721
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_FOR_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcSnote3Path:Ljava/lang/String;

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1700()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/loaddata_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    .line 725
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcImagePath:Ljava/lang/String;

    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "voicememo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcAmrPath:Ljava/lang/String;

    .line 727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcMp3Path:Ljava/lang/String;

    .line 728
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcVideoPath:Ljava/lang/String;

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcSnotePath:Ljava/lang/String;

    .line 730
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcSnote3Path:Ljava/lang/String;

    .line 732
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/loaddata_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    .line 734
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mThirdSrcImagePath:Ljava/lang/String;

    .line 735
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "voicememo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mThirdSrcAmrPath:Ljava/lang/String;

    .line 736
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mThirdSrcMp3Path:Ljava/lang/String;

    .line 737
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mThirdSrcVideoPath:Ljava/lang/String;

    .line 738
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mThirdSrcSnotePath:Ljava/lang/String;

    .line 739
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->LOADDATA_PATH_INTERNAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mThirdSrcSnote3Path:Ljava/lang/String;

    .line 741
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/loaddata_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcImagepath:Ljava/lang/String;

    .line 743
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "voicememo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcAmrPath:Ljava/lang/String;

    .line 744
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcMp3Path:Ljava/lang/String;

    .line 745
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcVideoPath:Ljava/lang/String;

    .line 746
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcSnotePath:Ljava/lang/String;

    .line 747
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->INTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mModelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snote3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcSnote3Path:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIIIII)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "photo"    # I
    .param p3, "video"    # I
    .param p4, "mp3"    # I
    .param p5, "voicememo"    # I
    .param p6, "snote"    # I
    .param p7, "snote3"    # I

    .prologue
    .line 834
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 749
    const-string v0, ".jpg"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->PHOTOTYPE:Ljava/lang/String;

    .line 750
    const-string v0, ".mp4"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->VIDEOTYPE:Ljava/lang/String;

    .line 751
    const-string v0, ".mp3"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->MP3TYPE:Ljava/lang/String;

    .line 752
    const-string v0, ".amr"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->VOICETYPE:Ljava/lang/String;

    .line 753
    const-string v0, ".snb"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->SNOTETYPE:Ljava/lang/String;

    .line 754
    const-string v0, ".spd"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->SNOTE3TYPE:Ljava/lang/String;

    .line 756
    const-string v0, "mdata_vmemo"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->voicePrefix:Ljava/lang/String;

    .line 757
    const-string v0, "mdata_mp"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mp3Prefix:Ljava/lang/String;

    .line 758
    const-string v0, "mdata_pic"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->photoPrefix:Ljava/lang/String;

    .line 759
    const-string v0, "mdata_vid"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->videoPrefix:Ljava/lang/String;

    .line 760
    const-string v0, "mdata_snote"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snotePrefix:Ljava/lang/String;

    .line 761
    const-string v0, "mdata_snote3"

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snote3Prefix:Ljava/lang/String;

    .line 771
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mPm:Landroid/os/PersonaManager;

    .line 835
    iput-object p1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mContext:Landroid/content/Context;

    .line 836
    iput p2, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->photo:I

    .line 837
    iput p3, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->video:I

    .line 838
    iput p4, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mp3:I

    .line 839
    iput p5, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->voicememo:I

    .line 840
    iput p6, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snote:I

    .line 841
    iput p7, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snote3:I

    .line 842
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;I)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;
    .param p1, "x1"    # I

    .prologue
    .line 695
    invoke-direct {p0, p1}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 695
    invoke-direct {p0, p1}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->setSystemPath()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->setUserPath()V

    return-void
.end method

.method private copyFile(Ljava/lang/String;IILjava/lang/String;)V
    .locals 18
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "num"    # I
    .param p3, "fileType"    # I
    .param p4, "dst"    # Ljava/lang/String;

    .prologue
    .line 871
    if-nez p2, :cond_0

    .line 931
    :goto_0
    return-void

    .line 875
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getSrcFile(I)Ljava/io/File;

    move-result-object v10

    .line 877
    .local v10, "srcFile":Ljava/io/File;
    if-nez v10, :cond_1

    .line 878
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getName(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'s source file is not ready!!"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 884
    :cond_1
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    move/from16 v0, p2

    if-ge v9, v0, :cond_8

    .line 885
    new-instance v4, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 891
    .local v4, "dstFile":Ljava/io/File;
    const/4 v7, 0x0

    .line 892
    .local v7, "from":Ljava/io/FileInputStream;
    const/4 v11, 0x0

    .line 895
    .local v11, "to":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 896
    .end local v7    # "from":Ljava/io/FileInputStream;
    .local v8, "from":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 897
    .end local v11    # "to":Ljava/io/FileOutputStream;
    .local v12, "to":Ljava/io/FileOutputStream;
    const/16 v13, 0x2000

    :try_start_2
    new-array v2, v13, [B

    .line 900
    .local v2, "buffer":[B
    :goto_2
    invoke-virtual {v8, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    .local v3, "bytesRead":I
    const/4 v13, -0x1

    if-eq v3, v13, :cond_4

    .line 901
    const/4 v13, 0x0

    invoke-virtual {v12, v2, v13, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 909
    .end local v2    # "buffer":[B
    .end local v3    # "bytesRead":I
    :catch_0
    move-exception v5

    move-object v11, v12

    .end local v12    # "to":Ljava/io/FileOutputStream;
    .restart local v11    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 910
    .end local v8    # "from":Ljava/io/FileInputStream;
    .local v5, "e":Ljava/lang/Exception;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    :goto_3
    :try_start_3
    const-string v13, "File Operation"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error occured while copying file"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 912
    if-eqz v7, :cond_2

    .line 914
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 920
    :cond_2
    :goto_4
    if-eqz v11, :cond_3

    .line 922
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 884
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 904
    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v11    # "to":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "bytesRead":I
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v12    # "to":Ljava/io/FileOutputStream;
    :cond_4
    :try_start_6
    const-string v13, "FileCopy"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "File ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is cloned"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 912
    if-eqz v8, :cond_5

    .line 914
    :try_start_7
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 920
    :cond_5
    :goto_6
    if-eqz v12, :cond_9

    .line 922
    :try_start_8
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    move-object v11, v12

    .end local v12    # "to":Ljava/io/FileOutputStream;
    .restart local v11    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 925
    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto :goto_5

    .line 915
    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v11    # "to":Ljava/io/FileOutputStream;
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v12    # "to":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v6

    .line 916
    .local v6, "ex":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 923
    .end local v6    # "ex":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 924
    .restart local v6    # "ex":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    move-object v11, v12

    .end local v12    # "to":Ljava/io/FileOutputStream;
    .restart local v11    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 925
    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto :goto_5

    .line 915
    .end local v2    # "buffer":[B
    .end local v3    # "bytesRead":I
    .end local v6    # "ex":Ljava/io/IOException;
    .restart local v5    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v6

    .line 916
    .restart local v6    # "ex":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 923
    .end local v6    # "ex":Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 924
    .restart local v6    # "ex":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 912
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    :goto_7
    if-eqz v7, :cond_6

    .line 914
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 920
    :cond_6
    :goto_8
    if-eqz v11, :cond_7

    .line 922
    :try_start_a
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 925
    :cond_7
    :goto_9
    throw v13

    .line 915
    :catch_5
    move-exception v6

    .line 916
    .restart local v6    # "ex":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 923
    .end local v6    # "ex":Ljava/io/IOException;
    :catch_6
    move-exception v6

    .line 924
    .restart local v6    # "ex":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 930
    .end local v4    # "dstFile":Ljava/io/File;
    .end local v6    # "ex":Ljava/io/IOException;
    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v11    # "to":Ljava/io/FileOutputStream;
    :cond_8
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->getName(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " copy is done!!"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->showToast(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 912
    .restart local v4    # "dstFile":Ljava/io/File;
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v11    # "to":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v13

    move-object v7, v8

    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto :goto_7

    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v11    # "to":Ljava/io/FileOutputStream;
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v12    # "to":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v13

    move-object v11, v12

    .end local v12    # "to":Ljava/io/FileOutputStream;
    .restart local v11    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto :goto_7

    .line 909
    :catch_7
    move-exception v5

    goto/16 :goto_3

    .end local v7    # "from":Ljava/io/FileInputStream;
    .restart local v8    # "from":Ljava/io/FileInputStream;
    :catch_8
    move-exception v5

    move-object v7, v8

    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .end local v7    # "from":Ljava/io/FileInputStream;
    .end local v11    # "to":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "bytesRead":I
    .restart local v8    # "from":Ljava/io/FileInputStream;
    .restart local v12    # "to":Ljava/io/FileOutputStream;
    :cond_9
    move-object v11, v12

    .end local v12    # "to":Ljava/io/FileOutputStream;
    .restart local v11    # "to":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "from":Ljava/io/FileInputStream;
    .restart local v7    # "from":Ljava/io/FileInputStream;
    goto/16 :goto_5
.end method

.method private getName(I)Ljava/lang/String;
    .locals 1
    .param p1, "fileType"    # I

    .prologue
    .line 1227
    packed-switch p1, :pswitch_data_0

    .line 1244
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1229
    :pswitch_0
    const-string v0, "Photo"

    goto :goto_0

    .line 1231
    :pswitch_1
    const-string v0, "Voice Memo"

    goto :goto_0

    .line 1233
    :pswitch_2
    const-string v0, "MP3"

    goto :goto_0

    .line 1235
    :pswitch_3
    const-string v0, "Video"

    goto :goto_0

    .line 1237
    :pswitch_4
    const-string v0, "Snote"

    goto :goto_0

    .line 1239
    :pswitch_5
    const-string v0, "Snote3"

    goto :goto_0

    .line 1227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getSrcFile(I)Ljava/io/File;
    .locals 10
    .param p1, "fileType"    # I

    .prologue
    .line 934
    const/4 v2, 0x0

    .line 959
    .local v2, "folder":Ljava/io/File;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    .line 960
    .local v6, "uid":I
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "@@ uid"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mContext:Landroid/content/Context;

    const-string v8, "persona"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PersonaManager;

    iput-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mPm:Landroid/os/PersonaManager;

    .line 963
    const/4 v4, 0x0

    .line 965
    .local v4, "isPersona":Z
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mPm:Landroid/os/PersonaManager;

    if-eqz v7, :cond_0

    .line 966
    iget-object v7, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v7, v6}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v4

    .line 967
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "@@ isPersona"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    :cond_0
    if-eqz v4, :cond_b

    .line 970
    packed-switch p1, :pswitch_data_0

    .line 994
    :goto_0
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 995
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_a

    aget-object v1, v0, v3

    .line 996
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 997
    const/4 v7, 0x1

    if-ne p1, v7, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1223
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :cond_1
    :goto_2
    return-object v1

    .line 972
    :pswitch_0
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcImagepath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 973
    .restart local v2    # "folder":Ljava/io/File;
    goto :goto_0

    .line 975
    :pswitch_1
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 976
    .restart local v2    # "folder":Ljava/io/File;
    goto :goto_0

    .line 978
    :pswitch_2
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 979
    .restart local v2    # "folder":Ljava/io/File;
    goto :goto_0

    .line 981
    :pswitch_3
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 982
    .restart local v2    # "folder":Ljava/io/File;
    goto :goto_0

    .line 984
    :pswitch_4
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcSnotePath:Ljava/lang/String;

    :goto_3
    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 985
    .restart local v2    # "folder":Ljava/io/File;
    goto :goto_0

    .line 984
    .end local v2    # "folder":Ljava/io/File;
    :cond_2
    const/4 v7, 0x0

    goto :goto_3

    .line 987
    .restart local v2    # "folder":Ljava/io/File;
    :pswitch_5
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v7

    if-eqz v7, :cond_3

    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mKNOXSrcSnote3Path:Ljava/lang/String;

    :goto_4
    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 988
    .restart local v2    # "folder":Ljava/io/File;
    goto :goto_0

    .line 987
    .end local v2    # "folder":Ljava/io/File;
    :cond_3
    const/4 v7, 0x0

    goto :goto_4

    .line 999
    .restart local v0    # "arr$":[Ljava/io/File;
    .restart local v1    # "f":Ljava/io/File;
    .restart local v2    # "folder":Ljava/io/File;
    .restart local v3    # "i$":I
    .restart local v5    # "len$":I
    :cond_4
    const/4 v7, 0x2

    if-ne p1, v7, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".amr"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1001
    :cond_5
    const/4 v7, 0x3

    if-ne p1, v7, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1003
    :cond_6
    const/4 v7, 0x4

    if-ne p1, v7, :cond_7

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp4"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1005
    :cond_7
    const/4 v7, 0x5

    if-ne p1, v7, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".snb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1008
    :cond_8
    const/4 v7, 0x6

    if-ne p1, v7, :cond_9

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".spd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v7

    if-nez v7, :cond_1

    .line 995
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 1016
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :cond_a
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getting file fail KNOX ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1020
    :cond_b
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getting second path ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->EXTERNAL_SDCARD_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1700()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_13

    .line 1024
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-eqz v7, :cond_12

    .line 1025
    packed-switch p1, :pswitch_data_1

    .line 1066
    :goto_5
    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 1067
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .restart local v0    # "arr$":[Ljava/io/File;
    array-length v5, v0

    .restart local v5    # "len$":I
    const/4 v3, 0x0

    .restart local v3    # "i$":I
    :goto_6
    if-ge v3, v5, :cond_1b

    aget-object v1, v0, v3

    .line 1068
    .restart local v1    # "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_11

    .line 1069
    const/4 v7, 0x1

    if-ne p1, v7, :cond_c

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1071
    :cond_c
    const/4 v7, 0x2

    if-ne p1, v7, :cond_d

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".amr"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1073
    :cond_d
    const/4 v7, 0x3

    if-ne p1, v7, :cond_e

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1075
    :cond_e
    const/4 v7, 0x4

    if-ne p1, v7, :cond_f

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp4"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1077
    :cond_f
    const/4 v7, 0x5

    if-ne p1, v7, :cond_10

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".snb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_10

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1080
    :cond_10
    const/4 v7, 0x6

    if-ne p1, v7, :cond_11

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".spd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_11

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1067
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 1027
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :pswitch_6
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcImagePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1028
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1030
    :pswitch_7
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1031
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1033
    :pswitch_8
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1034
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1036
    :pswitch_9
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1037
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1039
    :pswitch_a
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcSnotePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1040
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1045
    :cond_12
    packed-switch p1, :pswitch_data_2

    :pswitch_b
    goto/16 :goto_5

    .line 1047
    :pswitch_c
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcImagePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1048
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1050
    :pswitch_d
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1051
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1053
    :pswitch_e
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1054
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1056
    :pswitch_f
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1057
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1059
    :pswitch_10
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSecondSrcSnote3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1060
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_5

    .line 1088
    :cond_13
    const-string v7, "FileCopy"

    const-string v8, "External SD card is not mounted"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getting first path ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 1092
    packed-switch p1, :pswitch_data_3

    .line 1133
    :goto_7
    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 1134
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .restart local v0    # "arr$":[Ljava/io/File;
    array-length v5, v0

    .restart local v5    # "len$":I
    const/4 v3, 0x0

    .restart local v3    # "i$":I
    :goto_8
    if-ge v3, v5, :cond_1b

    aget-object v1, v0, v3

    .line 1135
    .restart local v1    # "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_19

    .line 1136
    const/4 v7, 0x1

    if-ne p1, v7, :cond_14

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1138
    :cond_14
    const/4 v7, 0x2

    if-ne p1, v7, :cond_15

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".amr"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1140
    :cond_15
    const/4 v7, 0x3

    if-ne p1, v7, :cond_16

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1142
    :cond_16
    const/4 v7, 0x4

    if-ne p1, v7, :cond_17

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp4"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1144
    :cond_17
    const/4 v7, 0x5

    if-ne p1, v7, :cond_18

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".snb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_18

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1147
    :cond_18
    const/4 v7, 0x6

    if-ne p1, v7, :cond_19

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".spd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_19

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1134
    :cond_19
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 1094
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :pswitch_11
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcImagePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1095
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1097
    :pswitch_12
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1098
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1100
    :pswitch_13
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1101
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1103
    :pswitch_14
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1104
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1106
    :pswitch_15
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcSnotePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1107
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1112
    :cond_1a
    packed-switch p1, :pswitch_data_4

    :pswitch_16
    goto/16 :goto_7

    .line 1114
    :pswitch_17
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcImagePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1115
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1117
    :pswitch_18
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1118
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1120
    :pswitch_19
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1121
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1123
    :pswitch_1a
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1124
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1126
    :pswitch_1b
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcSnote3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1127
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_7

    .line 1156
    :cond_1b
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getting third path ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-eqz v7, :cond_22

    .line 1159
    packed-switch p1, :pswitch_data_5

    .line 1200
    :goto_9
    if-eqz v2, :cond_23

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_23

    .line 1201
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .restart local v0    # "arr$":[Ljava/io/File;
    array-length v5, v0

    .restart local v5    # "len$":I
    const/4 v3, 0x0

    .restart local v3    # "i$":I
    :goto_a
    if-ge v3, v5, :cond_23

    aget-object v1, v0, v3

    .line 1202
    .restart local v1    # "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_21

    .line 1203
    const/4 v7, 0x1

    if-ne p1, v7, :cond_1c

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1205
    :cond_1c
    const/4 v7, 0x2

    if-ne p1, v7, :cond_1d

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".amr"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1207
    :cond_1d
    const/4 v7, 0x3

    if-ne p1, v7, :cond_1e

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1209
    :cond_1e
    const/4 v7, 0x4

    if-ne p1, v7, :cond_1f

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".mp4"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1211
    :cond_1f
    const/4 v7, 0x5

    if-ne p1, v7, :cond_20

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".snb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_20

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1214
    :cond_20
    const/4 v7, 0x6

    if-ne p1, v7, :cond_21

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".spd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_21

    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1201
    :cond_21
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 1161
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :pswitch_1c
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcImagePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1162
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1164
    :pswitch_1d
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1165
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1167
    :pswitch_1e
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1168
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1170
    :pswitch_1f
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1171
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1173
    :pswitch_20
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcSnotePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1174
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1179
    :cond_22
    packed-switch p1, :pswitch_data_6

    :pswitch_21
    goto/16 :goto_9

    .line 1181
    :pswitch_22
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcImagePath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1182
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1184
    :pswitch_23
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcAmrPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1185
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1187
    :pswitch_24
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcMp3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1188
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1190
    :pswitch_25
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcVideoPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1191
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1193
    :pswitch_26
    new-instance v2, Ljava/io/File;

    .end local v2    # "folder":Ljava/io/File;
    sget-object v7, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mSrcSnote3Path:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1194
    .restart local v2    # "folder":Ljava/io/File;
    goto/16 :goto_9

    .line 1222
    :cond_23
    const-string v7, "FileCopy"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getting file fail ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 970
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1025
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 1045
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_b
        :pswitch_10
    .end packed-switch

    .line 1092
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 1112
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_16
        :pswitch_1b
    .end packed-switch

    .line 1159
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
    .end packed-switch

    .line 1179
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_21
        :pswitch_26
    .end packed-switch
.end method

.method private setSystemPath()V
    .locals 1

    .prologue
    .line 800
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2100()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstImagePath:Ljava/lang/String;

    .line 801
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2100()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstAmrPath:Ljava/lang/String;

    .line 802
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2100()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstMp3Path:Ljava/lang/String;

    .line 803
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2100()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstVideoPath:Ljava/lang/String;

    .line 805
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 806
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2200()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnotePath:Ljava/lang/String;

    .line 809
    :cond_0
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 810
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->SYSTEM_FILE_COPY_FOLDER_FOR_SNOTE3:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2300()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnote3Path:Ljava/lang/String;

    .line 823
    :cond_1
    return-void
.end method

.method private setUserPath()V
    .locals 1

    .prologue
    .line 774
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1800()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstImagePath:Ljava/lang/String;

    .line 775
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1800()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstAmrPath:Ljava/lang/String;

    .line 776
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1800()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstMp3Path:Ljava/lang/String;

    .line 777
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1800()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstVideoPath:Ljava/lang/String;

    .line 779
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 780
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1800()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnotePath:Ljava/lang/String;

    .line 783
    :cond_0
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 784
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->USER_FILE_COPY_FOLDER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1800()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnote3Path:Ljava/lang/String;

    .line 797
    :cond_1
    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 826
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2400()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$1;-><init>(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 831
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 845
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->photoPrefix:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->photo:I

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstImagePath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->copyFile(Ljava/lang/String;IILjava/lang/String;)V

    .line 846
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mp3Prefix:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mp3:I

    const/4 v2, 0x3

    sget-object v3, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstMp3Path:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->copyFile(Ljava/lang/String;IILjava/lang/String;)V

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->videoPrefix:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->video:I

    const/4 v2, 0x4

    sget-object v3, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstVideoPath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->copyFile(Ljava/lang/String;IILjava/lang/String;)V

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->voicePrefix:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->voicememo:I

    const/4 v2, 0x2

    sget-object v3, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstAmrPath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->copyFile(Ljava/lang/String;IILjava/lang/String;)V

    .line 850
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnoteEnable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1900()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snotePrefix:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snote:I

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnotePath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->copyFile(Ljava/lang/String;IILjava/lang/String;)V

    .line 854
    :cond_0
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mSnote3Enable:Z
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2000()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snote3Prefix:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->snote3:I

    const/4 v2, 0x6

    sget-object v3, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->mDstSnote3Path:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;->copyFile(Ljava/lang/String;IILjava/lang/String;)V

    .line 858
    :cond_1
    # getter for: Lcom/sec/android/app/FileCopy/FileCopy;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/FileCopy/FileCopy;->access$2400()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread$2;-><init>(Lcom/sec/android/app/FileCopy/FileCopy$FileCopyThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 868
    return-void
.end method
