.class public Lcom/sec/android/app/Dft/DftEtc;
.super Lcom/sec/android/app/Dft/DftObject;
.source "DftEtc.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const-string v0, "DftEtc"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/Dft/DftObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public getIMEI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/Dft/DftEtc;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedRingtoneName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 22
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    .line 23
    .local v0, "RINGTONE_URI":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 25
    .local v2, "ringtoneName":Ljava/lang/String;
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/Dft/DftEtc;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 26
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftEtc;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/Dft/DftEtc;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 28
    const-string v3, "\\([a-zA-Z!@#$%^&*()-_=+,<.>/`~? ]+\\)"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 31
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 33
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 39
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    :goto_0
    return-object v2

    .line 36
    :cond_1
    const-string v2, "Not Set"

    goto :goto_0
.end method

.method public getSelectedWallpaperName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 43
    const/4 v1, 0x0

    .line 44
    .local v1, "wallpaperName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftEtc;->mContext:Landroid/content/Context;

    const-string v4, "wallpaper"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/WallpaperManager;

    .line 46
    .local v2, "wm":Landroid/app/WallpaperManager;
    invoke-virtual {v2}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    .line 48
    .local v0, "info":Landroid/app/WallpaperInfo;
    if-eqz v0, :cond_0

    .line 49
    iget-object v3, p0, Lcom/sec/android/app/Dft/DftEtc;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/WallpaperInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    :goto_0
    return-object v1

    .line 51
    :cond_0
    const-string v1, "Static image\nwallpaper"

    goto :goto_0
.end method
