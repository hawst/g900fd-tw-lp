.class public Lcom/sec/android/app/DataCreate/Receiver;
.super Landroid/content/BroadcastReceiver;
.source "Receiver.java"


# static fields
.field public static mWl:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method public makeFile()V
    .locals 9

    .prologue
    .line 133
    const/4 v5, 0x0

    .line 134
    .local v5, "fileCopyOutputStream":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 137
    .local v4, "fileCopyInputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    const-string v7, "/sdcard/1kHz.mp3"

    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    .end local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .local v6, "fileCopyOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/DataCreate/Receiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v8, 0x7f040000

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 139
    const-string v7, "FileOutputStream:"

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v7, "FileInputStream:"

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const/16 v7, 0x7d0

    new-array v0, v7, [B

    .line 143
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-lez v7, :cond_2

    .line 144
    invoke-virtual {v6, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 151
    .end local v0    # "buffer":[B
    :catch_0
    move-exception v1

    move-object v5, v6

    .line 152
    .end local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_2
    const-string v7, "FileCopyError"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    if-eqz v5, :cond_0

    .line 156
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 169
    :cond_0
    :goto_2
    if-eqz v4, :cond_1

    .line 170
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    .line 176
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_3
    return-void

    .line 147
    .end local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    :cond_2
    :try_start_5
    const-string v7, "FileOutputStream:"

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const-string v7, "FileInputStream:"

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 155
    if-eqz v6, :cond_3

    .line 156
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 169
    :cond_3
    :goto_4
    if-eqz v4, :cond_4

    .line 170
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_4
    move-object v5, v6

    .line 174
    .end local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 158
    .end local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 159
    .local v2, "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 162
    :try_start_8
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_4

    .line 163
    :catch_2
    move-exception v3

    .line 164
    .local v3, "ex2":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 172
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v3    # "ex2":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 173
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 175
    .end local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 158
    .end local v0    # "buffer":[B
    .end local v2    # "ex":Ljava/io/IOException;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 159
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 162
    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_2

    .line 163
    :catch_5
    move-exception v3

    .line 164
    .restart local v3    # "ex2":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 172
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v3    # "ex2":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 173
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 154
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 155
    :goto_5
    if-eqz v5, :cond_5

    .line 156
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 169
    :cond_5
    :goto_6
    if-eqz v4, :cond_6

    .line 170
    :try_start_b
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 174
    :cond_6
    :goto_7
    throw v7

    .line 158
    :catch_7
    move-exception v2

    .line 159
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 162
    :try_start_c
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    goto :goto_6

    .line 163
    :catch_8
    move-exception v3

    .line 164
    .restart local v3    # "ex2":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 172
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v3    # "ex2":Ljava/io/IOException;
    :catch_9
    move-exception v2

    .line 173
    .restart local v2    # "ex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 154
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileCopyOutputStream":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 151
    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method public mp3Copy()V
    .locals 4

    .prologue
    .line 119
    new-instance v0, Ljava/io/File;

    const-string v2, "/sdcard/1kHz.mp3"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v0, "localFile":Ljava/io/File;
    const-string v2, "DataCopyRilSupport"

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/DataCreate/Receiver;->makeFile()V

    .line 127
    :cond_0
    const-string v2, "DataCopyRilSupport"

    const-string v3, "sendSuccess :com.android.samsungtest.Mp3_COPY_SUCCESS"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.samsungtest.Mp3_COPY_SUCCESS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 129
    .local v1, "makeMp3FileInSdCardActionSuccess":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/DataCreate/Receiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 130
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v6, 0x10000000

    const/4 v5, 0x0

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/DataCreate/Receiver;->mContext:Landroid/content/Context;

    .line 54
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 55
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 56
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "host":Ljava/lang/String;
    const-string v3, "3282*727336*"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 62
    const-string v3, "Receiver"

    const-string v4, "3282*727336*"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const-class v3, Lcom/sec/android/app/CurrentDataState/CurrentDataState;

    invoke-virtual {v1, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 101
    :cond_0
    :goto_0
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 102
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 116
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 64
    .restart local v0    # "host":Ljava/lang/String;
    .restart local v1    # "i":Landroid/content/Intent;
    :cond_2
    const-string v3, "273283*255*3282*"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 65
    const-string v3, "Receiver"

    const-string v4, "273283*255*3282*"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-class v3, Lcom/sec/android/app/DataCopy/DataCopy;

    invoke-virtual {v1, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 67
    :cond_3
    const-string v3, "273283*255*663282*"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 68
    const-string v3, "Receiver"

    const-string v4, "273283*255*663282*"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-class v3, Lcom/sec/android/app/FileCopy/FileCopy;

    invoke-virtual {v1, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 70
    :cond_4
    const-string v3, "250"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 71
    sget-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_5

    .line 72
    sget-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 73
    sget-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 74
    const/4 v3, 0x0

    sput-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 75
    const-string v3, "DFTReceiver"

    const-string v4, "release WakeLock by KeyString"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const-string v3, "WAKELOCK OFF!"

    invoke-static {p1, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 79
    :cond_5
    const-string v3, "DFTReceiver"

    const-string v4, "mWl is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 83
    :cond_6
    const-string v3, "251"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    sget-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    if-nez v3, :cond_7

    .line 85
    const-string v3, "power"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 86
    .local v2, "pm":Landroid/os/PowerManager;
    const/16 v3, 0x1a

    const-string v4, "by KeyString"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 87
    const/4 v2, 0x0

    .line 88
    const-string v3, "WAKELOCK ON!"

    invoke-static {p1, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 91
    .end local v2    # "pm":Landroid/os/PowerManager;
    :cond_7
    sget-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-nez v3, :cond_8

    .line 92
    sget-object v3, Lcom/sec/android/app/DataCreate/Receiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 93
    const-string v3, "DFTReceiver"

    const-string v4, "acquire WakeLock by KeyString"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 95
    :cond_8
    const-string v3, "DFTReceiver"

    const-string v4, "already acquired WakeLock by KeyString"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 105
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_9
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.samsungtest.Mp3_COPY"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/DataCreate/Receiver;->mp3Copy()V

    goto/16 :goto_1

    .line 107
    :cond_a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.intent.DataCopy.START"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 109
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 110
    .restart local v1    # "i":Landroid/content/Intent;
    const-class v3, Lcom/sec/android/app/DataCopy/DataCopy;

    invoke-virtual {v1, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 111
    const-string v3, "auto_start"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 112
    const-string v3, "response_interval"

    const v4, 0x46cd0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 113
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method
