.class Lcom/sec/android/app/FileCopy/FileCopy$4;
.super Ljava/lang/Object;
.source "FileCopy.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FileCopy/FileCopy;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FileCopy/FileCopy;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FileCopy/FileCopy;)V
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, Lcom/sec/android/app/FileCopy/FileCopy$4;->this$0:Lcom/sec/android/app/FileCopy/FileCopy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 557
    const v1, 0x7f060004

    if-ne p2, v1, :cond_1

    .line 559
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$4;->this$0:Lcom/sec/android/app/FileCopy/FileCopy;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy;->setPath(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1200(Lcom/sec/android/app/FileCopy/FileCopy;I)V

    .line 560
    iget-object v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$4;->this$0:Lcom/sec/android/app/FileCopy/FileCopy;

    const-string v2, "path"

    const-string v3, "system_space"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy;->setProperty(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1300(Lcom/sec/android/app/FileCopy/FileCopy;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 561
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 564
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const v1, 0x7f060042

    if-ne p2, v1, :cond_0

    .line 566
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$4;->this$0:Lcom/sec/android/app/FileCopy/FileCopy;

    const/4 v2, 0x2

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy;->setPath(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1200(Lcom/sec/android/app/FileCopy/FileCopy;I)V

    .line 567
    iget-object v1, p0, Lcom/sec/android/app/FileCopy/FileCopy$4;->this$0:Lcom/sec/android/app/FileCopy/FileCopy;

    const-string v2, "path"

    const-string v3, "user_space"

    # invokes: Lcom/sec/android/app/FileCopy/FileCopy;->setProperty(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/FileCopy/FileCopy;->access$1300(Lcom/sec/android/app/FileCopy/FileCopy;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 568
    :catch_1
    move-exception v0

    .line 569
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
