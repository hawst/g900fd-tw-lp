.class Lcom/sec/android/app/DataCopy/DataCopy$1;
.super Landroid/content/BroadcastReceiver;
.source "DataCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/DataCopy/DataCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/DataCopy/DataCopy;


# direct methods
.method constructor <init>(Lcom/sec/android/app/DataCopy/DataCopy;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/DataCopy/DataCopy$1;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 98
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    const-string v0, "DataCopy"

    const-string v1, "ACTION_DEVICE_STORAGE_LOW"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$1;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/DataCopy/DataCopy;->access$002(Lcom/sec/android/app/DataCopy/DataCopy;Z)Z

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "DataCopy"

    const-string v1, "ACTION_DEVICE_STORAGE_OK"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/DataCopy/DataCopy$1;->this$0:Lcom/sec/android/app/DataCopy/DataCopy;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/DataCopy/DataCopy;->mStorageIsLow:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/DataCopy/DataCopy;->access$002(Lcom/sec/android/app/DataCopy/DataCopy;Z)Z

    goto :goto_0
.end method
