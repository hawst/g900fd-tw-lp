.class public interface abstract Lcom/lifevibes/videoeditor/VideoEditor;
.super Ljava/lang/Object;
.source "VideoEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    }
.end annotation


# static fields
.field public static final DURATION_OF_STORYBOARD:I = -0x1

.field public static final MAX_SUPPORTED_FILE_SIZE:J = 0x80000000L


# virtual methods
.method public abstract addMediaAudioItem(Lcom/lifevibes/videoeditor/MediaAudioItem;)V
.end method

.method public abstract addMediaItem(Lcom/lifevibes/videoeditor/MediaItem;)V
.end method

.method public abstract cancelExport(Ljava/lang/String;)V
.end method

.method public abstract cancelFit2Share(Ljava/lang/String;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract export(Ljava/lang/String;JLcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract export(Ljava/lang/String;IIIILcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract export(Ljava/lang/String;IIJIILcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract export(Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract exportAs2D(Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract fit2Share(Ljava/lang/String;Ljava/lang/String;JLcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract fit2ShareMMS(Ljava/lang/String;Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getAspectRatio()I
.end method

.method public abstract getDuration()J
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract release()V
.end method

.method public abstract removeMediaAudioItem(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaAudioItem;
.end method

.method public abstract removeMediaItem(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaItem;
.end method

.method public abstract setAspectRatio(I)V
.end method
