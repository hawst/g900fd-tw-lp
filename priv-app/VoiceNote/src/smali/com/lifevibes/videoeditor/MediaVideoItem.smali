.class public Lcom/lifevibes/videoeditor/MediaVideoItem;
.super Lcom/lifevibes/videoeditor/MediaItem;
.source "MediaVideoItem.java"


# instance fields
.field private final mAspectRatio:I

.field private final mAudioBitrate:I

.field private final mAudioChannels:I

.field private final mAudioSamplingFrequency:I

.field private final mAudioType:I

.field private mAudioWaveformFilename:Ljava/lang/String;

.field private mBeginBoundaryTimeMs:J

.field private final mDurationMs:J

.field private mEndBoundaryTimeMs:J

.field private mExclude:Z

.field private mExcludeBeginTime:J

.field private mExcludeEndTime:J

.field private final mFileType:I

.field private final mFps:I

.field private final mHeight:I

.field private mIsVideo3D:Z

.field private mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

.field private mMuted:Z

.field private mRotation:I

.field private mThumbnailLock:Ljava/util/concurrent/Semaphore;

.field private final mVideoBitrate:I

.field private mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditorImpl;

.field private final mVideoProfile:I

.field private final mVideoType:I

.field private mVolumePercentage:I

.field private mWaveformData:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/lifevibes/videoeditor/WaveformData;",
            ">;"
        }
    .end annotation
.end field

.field private final mWidth:I

.field private originalVolume:I


# direct methods
.method private constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v1, v0}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 13
    .param p1, "editor"    # Lcom/lifevibes/videoeditor/VideoEditor;
    .param p2, "mediaItemId"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "renderingMode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    const-wide/16 v6, 0x0

    const-wide/16 v8, -0x1

    const/16 v10, 0x64

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    invoke-direct/range {v1 .. v12}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;IJJIZLjava/lang/String;)V

    .line 125
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mThumbnailLock:Ljava/util/concurrent/Semaphore;

    .line 126
    return-void
.end method

.method constructor <init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;IJJIZLjava/lang/String;)V
    .locals 9
    .param p1, "editor"    # Lcom/lifevibes/videoeditor/VideoEditor;
    .param p2, "mediaItemId"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "renderingMode"    # I
    .param p5, "beginMs"    # J
    .param p7, "endMs"    # J
    .param p9, "volumePercent"    # I
    .param p10, "muted"    # Z
    .param p11, "audioWaveformFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/lifevibes/videoeditor/MediaItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    const/4 v4, 0x0

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mRotation:I

    .line 151
    instance-of v4, p1, Lcom/lifevibes/videoeditor/VideoEditorImpl;

    if-eqz v4, :cond_0

    move-object v4, p1

    .line 152
    check-cast v4, Lcom/lifevibes/videoeditor/VideoEditorImpl;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->getNativeContext()Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-result-object v4

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    .line 153
    check-cast p1, Lcom/lifevibes/videoeditor/VideoEditorImpl;

    .end local p1    # "editor":Lcom/lifevibes/videoeditor/VideoEditor;
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditorImpl;

    .line 155
    :cond_0
    new-instance v4, Ljava/util/concurrent/Semaphore;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mThumbnailLock:Ljava/util/concurrent/Semaphore;

    .line 158
    :try_start_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v4, p3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 163
    .local v3, "properties":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 172
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Input File Type (%d)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 159
    .end local v3    # "properties":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    :catch_0
    move-exception v2

    .line 160
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {p3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 175
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "properties":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    :sswitch_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getVideoCodecType(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 184
    :pswitch_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Video Codec Format in Input File (%d)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 187
    :pswitch_1
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    if-eqz v4, :cond_1

    .line 188
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 197
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Audio Codec Format in Input File (%d)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 200
    :cond_1
    :pswitch_2
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->averageFrameRate:F

    float-to-int v4, v4

    if-eqz v4, :cond_2

    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->averageFrameRate:F

    float-to-int v4, v4

    const/16 v5, 0x3c

    if-le v4, v5, :cond_3

    .line 202
    :cond_2
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Input Video Frame Rate (%d)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->averageFrameRate:F

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 206
    :cond_3
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->profileAndLevel:I

    const/16 v5, 0xff

    if-ne v4, v5, :cond_4

    .line 207
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Video Codec Profile in Input File (%d)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->profileAndLevel:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 210
    :cond_4
    iget-boolean v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->bIsVideo3D:Z

    iput-boolean v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mIsVideo3D:Z

    .line 211
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWidth:I

    .line 212
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mHeight:I

    .line 213
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    iget v6, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    invoke-virtual {v4, v5, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAspectRatio:I

    .line 215
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mFileType:I

    .line 216
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getVideoCodecType(I)I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoType:I

    .line 217
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->profileAndLevel:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoProfile:I

    .line 218
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    .line 219
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoBitrate:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoBitrate:I

    .line 220
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioBitrate:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioBitrate:I

    .line 221
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->averageFrameRate:F

    float-to-int v4, v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mFps:I

    .line 222
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    .line 223
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioChannels:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioChannels:I

    .line 224
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioSamplingFrequency:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    .line 225
    iput-wide p5, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    .line 226
    const-wide/16 v4, -0x1

    cmp-long v4, p7, v4

    if-nez v4, :cond_5

    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    move-wide/from16 p7, v0

    .end local p7    # "endMs":J
    :cond_5
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    .line 227
    move/from16 v0, p9

    iput v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVolumePercentage:I

    move/from16 v0, p9

    iput v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->originalVolume:I

    .line 228
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMuted:Z

    .line 229
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 230
    if-eqz p11, :cond_6

    .line 231
    new-instance v4, Ljava/lang/ref/SoftReference;

    new-instance v5, Lcom/lifevibes/videoeditor/WaveformData;

    move-object/from16 v0, p11

    invoke-direct {v5, v0}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    .line 236
    :goto_0
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeBeginTime:J

    .line 237
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeEndTime:J

    .line 239
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDisplayAngle:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mRotation:I

    .line 241
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    .line 242
    return-void

    .line 234
    :cond_6
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    goto :goto_0

    .line 163
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
    .end sparse-switch

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 188
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private thumbnailLock()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mThumbnailLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 1183
    return-void
.end method

.method private thumbnailUnlock()V
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mThumbnailLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1190
    return-void
.end method


# virtual methods
.method public cancelAudioWaveformExtraction()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stopAudioWaveformExtraction()V

    .line 411
    :cond_0
    return-void
.end method

.method public cancelThumbnailList()V
    .locals 1

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stopThumbnailList()V

    .line 1024
    return-void
.end method

.method public extractAudioWaveform(Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;)V
    .locals 13
    .param p1, "listener"    # Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 340
    const/4 v5, 0x0

    .line 341
    .local v5, "frameDuration":I
    const/4 v7, 0x0

    .line 342
    .local v7, "sampleCount":I
    const/4 v12, 0x0

    .line 343
    .local v12, "result":I
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, "projectPath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 348
    .local v4, "audioWaveFileName":Ljava/lang/String;
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 349
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "audioWaveformFile-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 358
    :goto_0
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 360
    const/4 v5, 0x5

    .line 362
    const/16 v7, 0xa0

    .line 385
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mFilename:Ljava/lang/String;

    iget-wide v10, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    move-object v8, p1

    move v9, v6

    invoke-virtual/range {v1 .. v11}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;ZJ)I

    move-result v12

    .line 396
    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 397
    const/4 v1, -0x1

    if-eq v12, v1, :cond_6

    .line 398
    new-instance v1, Ljava/lang/ref/SoftReference;

    new-instance v2, Lcom/lifevibes/videoeditor/WaveformData;

    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    .line 405
    :goto_2
    return-void

    .line 352
    :cond_1
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto :goto_0

    .line 363
    :cond_2
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 365
    const/16 v5, 0xa

    .line 367
    const/16 v7, 0x140

    goto :goto_1

    .line 368
    :cond_3
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 370
    const/16 v5, 0x20

    .line 372
    const/16 v7, 0x400

    goto :goto_1

    .line 373
    :cond_4
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 375
    const/16 v5, 0x40

    .line 377
    const/16 v7, 0x800

    goto :goto_1

    .line 378
    :cond_5
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 380
    const/16 v5, 0x40

    .line 382
    const/16 v7, 0x800

    goto :goto_1

    .line 401
    :cond_6
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 402
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 403
    :cond_7
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto :goto_2
.end method

.method public extractAudioWaveform(Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;J)V
    .locals 24
    .param p1, "listener"    # Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;
    .param p2, "numSampNeeded"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    const/4 v7, 0x0

    .line 429
    .local v7, "frameDuration":I
    const/4 v9, 0x0

    .line 430
    .local v9, "sampleCount":I
    const/16 v21, 0x0

    .line 431
    .local v21, "result":I
    const/4 v6, 0x0

    .line 432
    .local v6, "audioWaveFileName":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 433
    .local v16, "dur":D
    const/4 v2, 0x0

    .line 434
    .local v2, "inDuration":I
    const/16 v22, 0x0

    .line 435
    .local v22, "roundedDuration":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    move-result-object v20

    .line 439
    .local v20, "projectPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 440
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "audioWaveformFile-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 449
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 451
    const v3, 0x27100

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 453
    const/16 v9, 0xa0

    .line 476
    :cond_0
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v3, p2, v4

    if-lez v3, :cond_2

    .line 481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    .line 483
    const-wide v4, 0x4103880000000000L    # 160000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    .line 511
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    div-long v4, v4, p2

    long-to-int v2, v4

    .line 512
    move-wide/from16 v0, v16

    double-to-int v3, v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v2

    move-wide/from16 v0, v16

    double-to-int v4, v0

    div-int v22, v3, v4

    .line 516
    move/from16 v0, v22

    int-to-double v4, v0

    mul-double v16, v16, v4

    .line 517
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double v4, v4, v16

    double-to-int v7, v4

    .line 520
    :cond_2
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v4, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    int-to-long v10, v3

    mul-long/2addr v4, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioChannels:I

    int-to-long v10, v3

    mul-long/2addr v4, v10

    long-to-double v0, v4

    move-wide/from16 v18, v0

    .line 521
    .local v18, "pcmFileSize":D
    const-wide v14, 0x41efffffffe00000L    # 4.294967295E9

    .line 523
    .local v14, "MAX_PCM_FILESIZE":D
    const-wide v4, 0x41efffffffe00000L    # 4.294967295E9

    cmpl-double v3, v18, v4

    if-lez v3, :cond_c

    .line 524
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "file too longfor PCM generation"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 443
    .end local v14    # "MAX_PCM_FILESIZE":D
    .end local v18    # "pcmFileSize":D
    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto/16 :goto_0

    .line 454
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_5

    .line 456
    const v3, 0x4e200

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 458
    const/16 v9, 0x140

    goto/16 :goto_1

    .line 459
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 461
    const v3, 0xfa000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 463
    const/16 v9, 0x400

    goto/16 :goto_1

    .line 464
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_7

    .line 466
    const/16 v7, 0x40

    .line 468
    const/16 v9, 0x800

    goto/16 :goto_1

    .line 469
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 471
    const/16 v7, 0x40

    .line 473
    const/16 v9, 0x800

    goto/16 :goto_1

    .line 486
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_9

    .line 488
    const-wide v4, 0x4113880000000000L    # 320000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    goto/16 :goto_2

    .line 491
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_a

    .line 493
    const-wide v4, 0x412f400000000000L    # 1024000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    goto/16 :goto_2

    .line 496
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_b

    .line 498
    const-wide v4, 0x413f400000000000L    # 2048000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    goto/16 :goto_2

    .line 501
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 503
    const-wide v4, 0x413f400000000000L    # 2048000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    goto/16 :goto_2

    .line 527
    .restart local v14    # "MAX_PCM_FILESIZE":D
    .restart local v18    # "pcmFileSize":D
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mFilename:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    move-object/from16 v10, p1

    invoke-virtual/range {v3 .. v13}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;ZJ)I

    move-result v21

    .line 538
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 539
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_d

    .line 540
    new-instance v3, Ljava/lang/ref/SoftReference;

    new-instance v4, Lcom/lifevibes/videoeditor/WaveformData;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    .line 547
    :goto_3
    return-void

    .line 543
    :cond_d
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 544
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 545
    :cond_e
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto :goto_3
.end method

.method public getAspectRatio()I
    .locals 1

    .prologue
    .line 1066
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAspectRatio:I

    return v0
.end method

.method public getAudioBitrate()I
    .locals 1

    .prologue
    .line 1145
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioBitrate:I

    return v0
.end method

.method public getAudioChannels()I
    .locals 1

    .prologue
    .line 1166
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioChannels:I

    return v0
.end method

.method public getAudioSamplingFrequency()I
    .locals 1

    .prologue
    .line 1173
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    return v0
.end method

.method public getAudioType()I
    .locals 1

    .prologue
    .line 1159
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioType:I

    return v0
.end method

.method getAudioWaveformFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getBoundaryBeginTime()J
    .locals 2

    .prologue
    .line 316
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    return-wide v0
.end method

.method public getBoundaryEndTime()J
    .locals 2

    .prologue
    .line 323
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    return-wide v0
.end method

.method public getDisplayAngle()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mRotation:I

    return v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 1098
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    return-wide v0
.end method

.method getExcludeBeginTime()J
    .locals 2

    .prologue
    .line 1245
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeBeginTime:J

    return-wide v0
.end method

.method getExcludeEndTime()J
    .locals 2

    .prologue
    .line 1248
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeEndTime:J

    return-wide v0
.end method

.method public getFileType()I
    .locals 1

    .prologue
    .line 1074
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mFileType:I

    return v0
.end method

.method public getFps()I
    .locals 1

    .prologue
    .line 1152
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mFps:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 1090
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mHeight:I

    return v0
.end method

.method public getThumbnail(IIJ)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "timeMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 607
    const/4 v7, 0x0

    .line 609
    .local v7, "outputThumbnail":Landroid/graphics/Bitmap;
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 610
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Time Exceeds duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 613
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_1

    .line 614
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Time duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 617
    :cond_1
    if-lez p1, :cond_2

    if-gtz p2, :cond_3

    .line 618
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Dimensions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621
    :cond_3
    const/4 v8, 0x0

    .line 623
    .local v8, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 624
    const/4 v8, 0x1

    .line 626
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super {p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixels(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 632
    if-eqz v8, :cond_4

    .line 633
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 636
    :cond_4
    :goto_0
    return-object v7

    .line 629
    :catch_0
    move-exception v6

    .line 630
    .local v6, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v0, "MediaVideoItem"

    const-string v1, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 632
    if-eqz v8, :cond_4

    .line 633
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_0

    .line 632
    .end local v6    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_5

    .line 633
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_5
    throw v0
.end method

.method public getThumbnail(IIJZ)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "timeMs"    # J
    .param p5, "mode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 667
    const/4 v7, 0x0

    .line 669
    .local v7, "outputThumbnail":Landroid/graphics/Bitmap;
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 670
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Time Exceeds duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_1

    .line 674
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Time duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_1
    if-lez p1, :cond_2

    if-gtz p2, :cond_3

    .line 678
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Dimensions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 681
    :cond_3
    const/4 v8, 0x0

    .line 683
    .local v8, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 684
    const/4 v8, 0x1

    .line 687
    if-nez p5, :cond_5

    .line 688
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super {p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixels(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 699
    :goto_0
    if-eqz v8, :cond_4

    .line 700
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 703
    :cond_4
    :goto_1
    return-object v7

    .line 691
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super {p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    long-to-int v2, p3

    int-to-long v4, v2

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsFast(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto :goto_0

    .line 694
    :catch_0
    move-exception v6

    .line 695
    .local v6, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v0, "MediaVideoItem"

    const-string v1, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 699
    if-eqz v8, :cond_4

    .line 700
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_1

    .line 696
    .end local v6    # "ex":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v6

    .line 697
    .local v6, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "MediaVideoItem"

    const-string v1, "Run-time exception occured in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 699
    if-eqz v8, :cond_4

    .line 700
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_1

    .line 699
    .end local v6    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_6

    .line 700
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_6
    throw v0
.end method

.method public getThumbnailList(IIJJILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .locals 13
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "startMs"    # J
    .param p5, "endMs"    # J
    .param p7, "thumbnailCount"    # I
    .param p8, "callback"    # Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 935
    cmp-long v0, p3, p5

    if-lez v0, :cond_0

    .line 936
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Start time is greater than end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 939
    :cond_0
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p5, v0

    if-lez v0, :cond_1

    .line 940
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "End time is greater than file duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 943
    :cond_1
    if-lez p2, :cond_2

    if-gtz p1, :cond_3

    .line 944
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid dimension"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 946
    :cond_3
    if-nez p8, :cond_4

    .line 947
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Call Back Can not be Null, Please use Other API"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 949
    :cond_4
    const/4 v11, 0x0

    .line 951
    .local v11, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 952
    const/4 v11, 0x1

    .line 954
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 960
    if-eqz v11, :cond_5

    .line 961
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 964
    :cond_5
    :goto_0
    return-void

    .line 957
    :catch_0
    move-exception v10

    .line 958
    .local v10, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v0, "MediaVideoItem"

    const-string v1, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 960
    if-eqz v11, :cond_5

    .line 961
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_0

    .line 960
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v0

    if-eqz v11, :cond_6

    .line 961
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_6
    throw v0
.end method

.method public getThumbnailList(IIJJIZLcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .locals 13
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "startMs"    # J
    .param p5, "endMs"    # J
    .param p7, "thumbnailCount"    # I
    .param p8, "mode"    # Z
    .param p9, "callback"    # Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 976
    cmp-long v0, p3, p5

    if-lez v0, :cond_0

    .line 977
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Start time is greater than end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 980
    :cond_0
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p5, v0

    if-lez v0, :cond_1

    .line 981
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "End time is greater than file duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 984
    :cond_1
    if-lez p2, :cond_2

    if-gtz p1, :cond_3

    .line 985
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid dimension"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 987
    :cond_3
    if-nez p9, :cond_4

    .line 988
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Call Back Can not be Null, Please use Other API"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 990
    :cond_4
    const/4 v11, 0x0

    .line 992
    .local v11, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 993
    const/4 v11, 0x1

    .line 996
    if-nez p8, :cond_6

    .line 997
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1006
    :goto_0
    if-eqz v11, :cond_5

    .line 1007
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 1010
    :cond_5
    :goto_1
    return-void

    .line 1000
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsListFast(Ljava/lang/String;IIJJILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1003
    :catch_0
    move-exception v10

    .line 1004
    .local v10, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v0, "MediaVideoItem"

    const-string v1, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1006
    if-eqz v11, :cond_5

    .line 1007
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_1

    .line 1006
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v0

    if-eqz v11, :cond_7

    .line 1007
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_7
    throw v0
.end method

.method public getThumbnailList(IIJJI)[Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "startMs"    # J
    .param p5, "endMs"    # J
    .param p7, "thumbnailCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 727
    const/4 v10, 0x0

    .line 728
    .local v10, "bitmaps":[Landroid/graphics/Bitmap;
    const/4 v12, 0x0

    .line 730
    .local v12, "semAcquireDone":Z
    cmp-long v0, p3, p5

    if-lez v0, :cond_0

    .line 731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Start time is greater than end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 734
    :cond_0
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p5, v0

    if-lez v0, :cond_1

    .line 735
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "End time is greater than file duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 738
    :cond_1
    if-lez p2, :cond_2

    if-gtz p1, :cond_3

    .line 739
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid dimension"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 742
    :cond_3
    cmp-long v0, p3, p5

    if-nez v0, :cond_6

    .line 743
    const/4 v0, 0x1

    new-array v9, v0, [Landroid/graphics/Bitmap;

    .line 745
    .local v9, "bitmap":[Landroid/graphics/Bitmap;
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 746
    const/4 v12, 0x1

    .line 748
    const/4 v6, 0x0

    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super {p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide/from16 v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixels(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;

    move-result-object v0

    aput-object v0, v9, v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 755
    if-eqz v12, :cond_4

    .line 756
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 777
    .end local v9    # "bitmap":[Landroid/graphics/Bitmap;
    :cond_4
    :goto_0
    return-object v9

    .line 750
    .restart local v9    # "bitmap":[Landroid/graphics/Bitmap;
    :catch_0
    move-exception v11

    .line 751
    .local v11, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v0, "MediaVideoItem"

    const-string v1, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 755
    if-eqz v12, :cond_4

    .line 756
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_0

    .line 752
    .end local v11    # "ex":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v11

    .line 753
    .local v11, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "MediaVideoItem"

    const-string v1, "Run-time exception occured in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 755
    if-eqz v12, :cond_4

    .line 756
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_0

    .line 755
    .end local v11    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v12, :cond_5

    .line 756
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_5
    throw v0

    .line 762
    .end local v9    # "bitmap":[Landroid/graphics/Bitmap;
    :cond_6
    :try_start_3
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 763
    const/4 v12, 0x1

    .line 765
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super {p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v1

    move v2, p1

    move v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJI)[Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v10

    .line 773
    if-eqz v12, :cond_7

    .line 774
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_7
    :goto_1
    move-object v9, v10

    .line 777
    goto :goto_0

    .line 768
    :catch_2
    move-exception v11

    .line 769
    .local v11, "ex":Ljava/lang/InterruptedException;
    :try_start_4
    const-string v0, "MediaVideoItem"

    const-string v1, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 773
    if-eqz v12, :cond_7

    .line 774
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_1

    .line 770
    .end local v11    # "ex":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v11

    .line 771
    .local v11, "ex":Ljava/lang/Exception;
    :try_start_5
    const-string v0, "MediaVideoItem"

    const-string v1, "Run-time exception occured in getThumbnail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 773
    if-eqz v12, :cond_7

    .line 774
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_1

    .line 773
    .end local v11    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    if-eqz v12, :cond_8

    .line 774
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_8
    throw v0
.end method

.method public getThumbnailList(IIJJIZ)[Landroid/util/Pair;
    .locals 19
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "startMs"    # J
    .param p5, "endMs"    # J
    .param p7, "thumbnailCount"    # I
    .param p8, "mode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJJIZ)[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 809
    const/16 v16, 0x0

    .line 811
    .local v16, "semAcquireDone":Z
    cmp-long v2, p3, p5

    if-lez v2, :cond_0

    .line 812
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Start time is greater than end time"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 815
    :cond_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v2, p5, v2

    if-lez v2, :cond_1

    .line 816
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "End time is greater than file duration"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 819
    :cond_1
    if-lez p2, :cond_2

    if-gtz p1, :cond_3

    .line 820
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid dimension"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 824
    :cond_3
    if-nez p8, :cond_a

    .line 825
    cmp-long v2, p3, p5

    if-nez v2, :cond_7

    .line 826
    const/4 v2, 0x1

    new-array v12, v2, [Landroid/graphics/Bitmap;

    .line 827
    .local v12, "bitmap":[Landroid/graphics/Bitmap;
    const/4 v2, 0x1

    new-array v11, v2, [Landroid/util/Pair;

    .line 829
    .local v11, "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 830
    const/16 v16, 0x1

    .line 832
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v3

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixels(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v12, v8
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 839
    if-eqz v16, :cond_4

    .line 840
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 843
    :cond_4
    :goto_0
    const/4 v2, 0x0

    new-instance v3, Landroid/util/Pair;

    move-wide/from16 v0, p3

    long-to-int v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v12, v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v11, v2

    .line 922
    .end local v12    # "bitmap":[Landroid/graphics/Bitmap;
    :cond_5
    :goto_1
    return-object v11

    .line 834
    .restart local v12    # "bitmap":[Landroid/graphics/Bitmap;
    :catch_0
    move-exception v14

    .line 835
    .local v14, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v2, "MediaVideoItem"

    const-string v3, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 839
    if-eqz v16, :cond_4

    .line 840
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_0

    .line 836
    .end local v14    # "ex":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v14

    .line 837
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "MediaVideoItem"

    const-string v3, "Run-time exception occured in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 839
    if-eqz v16, :cond_4

    .line 840
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_0

    .line 839
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v16, :cond_6

    .line 840
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_6
    throw v2

    .line 848
    .end local v11    # "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    .end local v12    # "bitmap":[Landroid/graphics/Bitmap;
    :cond_7
    const/4 v12, 0x0

    .line 849
    .restart local v12    # "bitmap":[Landroid/graphics/Bitmap;
    sub-long v2, p5, p3

    long-to-int v2, v2

    div-int v13, v2, p7

    .line 850
    .local v13, "deltaTime":I
    move-wide/from16 v0, p3

    long-to-int v0, v0

    move/from16 v17, v0

    .line 853
    .local v17, "tnTime":I
    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 854
    const/16 v16, 0x1

    .line 856
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v3

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move/from16 v10, p7

    invoke-virtual/range {v2 .. v10}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJI)[Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v12

    .line 864
    if-eqz v16, :cond_8

    .line 865
    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 873
    :cond_8
    :goto_2
    move/from16 v0, p7

    new-array v11, v0, [Landroid/util/Pair;

    .line 874
    .restart local v11    # "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_3
    move/from16 v0, p7

    if-ge v15, v0, :cond_5

    .line 875
    new-instance v2, Landroid/util/Pair;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget-object v3, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailTime:[I

    aget v3, v3, v15

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aget-object v4, v12, v15

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v11, v15

    .line 874
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 859
    .end local v11    # "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    .end local v15    # "i":I
    :catch_2
    move-exception v14

    .line 860
    .local v14, "ex":Ljava/lang/InterruptedException;
    :try_start_5
    const-string v2, "MediaVideoItem"

    const-string v3, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 864
    if-eqz v16, :cond_8

    .line 865
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 868
    .end local v14    # "ex":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v14

    .line 869
    .local v14, "ex":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Memory allocation fails, thumbnail count too large: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 861
    .end local v14    # "ex":Ljava/lang/Throwable;
    :catch_4
    move-exception v14

    .line 862
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_7
    const-string v2, "MediaVideoItem"

    const-string v3, "Run-time exception occured in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 864
    if-eqz v16, :cond_8

    .line 865
    :try_start_8
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_2

    .line 864
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    if-eqz v16, :cond_9

    .line 865
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_9
    throw v2
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    .line 882
    .end local v12    # "bitmap":[Landroid/graphics/Bitmap;
    .end local v13    # "deltaTime":I
    .end local v17    # "tnTime":I
    :cond_a
    cmp-long v2, p3, p5

    if-nez v2, :cond_d

    .line 883
    const/4 v2, 0x1

    new-array v12, v2, [Landroid/graphics/Bitmap;

    .line 884
    .restart local v12    # "bitmap":[Landroid/graphics/Bitmap;
    const/4 v2, 0x1

    new-array v11, v2, [Landroid/util/Pair;

    .line 886
    .restart local v11    # "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    :try_start_9
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 887
    const/16 v16, 0x1

    .line 889
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v3

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsFast(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v12, v8
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 897
    if-eqz v16, :cond_b

    .line 898
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    .line 901
    :cond_b
    :goto_4
    const/4 v2, 0x0

    new-instance v3, Landroid/util/Pair;

    move-wide/from16 v0, p3

    long-to-int v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v12, v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v11, v2

    goto/16 :goto_1

    .line 892
    :catch_5
    move-exception v14

    .line 893
    .local v14, "ex":Ljava/lang/InterruptedException;
    :try_start_a
    const-string v2, "MediaVideoItem"

    const-string v3, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 897
    if-eqz v16, :cond_b

    .line 898
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_4

    .line 894
    .end local v14    # "ex":Ljava/lang/InterruptedException;
    :catch_6
    move-exception v14

    .line 895
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_b
    const-string v2, "MediaVideoItem"

    const-string v3, "Run-time exception occured in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 897
    if-eqz v16, :cond_b

    .line 898
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto :goto_4

    .line 897
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_2
    move-exception v2

    if-eqz v16, :cond_c

    .line 898
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_c
    throw v2

    .line 905
    .end local v11    # "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    .end local v12    # "bitmap":[Landroid/graphics/Bitmap;
    :cond_d
    const/4 v11, 0x0

    .line 907
    .restart local v11    # "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    :try_start_c
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailLock()V

    .line 908
    const/16 v16, 0x1

    .line 910
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v3

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move/from16 v10, p7

    invoke-virtual/range {v2 .. v10}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getPixelsListFast(Ljava/lang/String;IIJJI)[Landroid/util/Pair;
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_7
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    move-result-object v11

    .line 918
    if-eqz v16, :cond_5

    .line 919
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto/16 :goto_1

    .line 913
    :catch_7
    move-exception v14

    .line 914
    .local v14, "ex":Ljava/lang/InterruptedException;
    :try_start_d
    const-string v2, "MediaVideoItem"

    const-string v3, "Sem acquire NOT successful in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 918
    if-eqz v16, :cond_5

    .line 919
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto/16 :goto_1

    .line 915
    .end local v14    # "ex":Ljava/lang/InterruptedException;
    :catch_8
    move-exception v14

    .line 916
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_e
    const-string v2, "MediaVideoItem"

    const-string v3, "Run-time exception occured in getThumbnail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 918
    if-eqz v16, :cond_5

    .line 919
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    goto/16 :goto_1

    .line 918
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_3
    move-exception v2

    if-eqz v16, :cond_e

    .line 919
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->thumbnailUnlock()V

    :cond_e
    throw v2
.end method

.method public getTimelineDuration()J
    .locals 6

    .prologue
    .line 1106
    iget-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    if-nez v0, :cond_0

    .line 1107
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    iget-wide v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    sub-long/2addr v0, v2

    .line 1109
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeBeginTime:J

    const-wide/16 v2, 0x0

    sub-long/2addr v0, v2

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeEndTime:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public getVideoBitrate()I
    .locals 1

    .prologue
    .line 1138
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoBitrate:I

    return v0
.end method

.method public getVideoProfile()I
    .locals 1

    .prologue
    .line 1131
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoProfile:I

    return v0
.end method

.method public getVideoType()I
    .locals 1

    .prologue
    .line 1124
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoType:I

    return v0
.end method

.method public getWaveformData()Lcom/lifevibes/videoeditor/WaveformData;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 554
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_1

    move-object v1, v2

    .line 570
    :cond_0
    :goto_0
    return-object v1

    .line 558
    :cond_1
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lifevibes/videoeditor/WaveformData;

    .line 559
    .local v1, "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    if-nez v1, :cond_0

    .line 561
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 563
    :try_start_0
    new-instance v1, Lcom/lifevibes/videoeditor/WaveformData;

    .end local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    .restart local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    goto :goto_0

    .line 564
    .end local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    :catch_0
    move-exception v0

    .line 565
    .local v0, "e":Ljava/io/IOException;
    throw v0

    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    :cond_2
    move-object v1, v2

    .line 570
    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 1082
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mWidth:I

    return v0
.end method

.method isExcludeCalled()Z
    .locals 1

    .prologue
    .line 1242
    iget-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    return v0
.end method

.method public isMuted()Z
    .locals 1

    .prologue
    .line 1117
    iget-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMuted:Z

    return v0
.end method

.method public isThumbnailSpeedPreferedOverAccuracy(IJJ)Z
    .locals 8
    .param p1, "thumbnailCount"    # I
    .param p2, "startMs"    # J
    .param p4, "endMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 1043
    if-gtz p1, :cond_0

    .line 1044
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Negative Thumbnail count"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046
    :cond_0
    cmp-long v0, p2, v2

    if-gez v0, :cond_1

    .line 1047
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Begin Time is less than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049
    :cond_1
    cmp-long v0, p4, v2

    if-gez v0, :cond_2

    .line 1050
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "End Time is less than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052
    :cond_2
    cmp-long v0, p2, p4

    if-lez v0, :cond_3

    .line 1053
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Begin Time is more than End time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055
    :cond_3
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-super {p0}, Lcom/lifevibes/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v2

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findThumbnailMode(Ljava/lang/String;IJJ)Z

    move-result v0

    return v0
.end method

.method public setDisplayAngle(I)V
    .locals 0
    .param p1, "displayAngle"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mRotation:I

    .line 259
    return-void
.end method

.method public setExcludeBoundaries(JJ)V
    .locals 7
    .param p1, "beginMs"    # J
    .param p3, "endMs"    # J

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, -0x1

    const-wide/16 v2, 0x0

    .line 1211
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 1212
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExcludeBoundaries: Invalid start time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1214
    :cond_0
    cmp-long v0, p3, v4

    if-eqz v0, :cond_1

    cmp-long v0, p1, p3

    if-ltz v0, :cond_1

    .line 1215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExcludeBoundaries: Start time is greater than end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1217
    :cond_1
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v0

    cmp-long v0, p3, v0

    if-lez v0, :cond_2

    .line 1218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExcludeBoundaries: Invalid end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1221
    :cond_2
    cmp-long v0, p1, v2

    if-gez v0, :cond_3

    .line 1222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExcludeBoundaries: Invalid begin time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1224
    :cond_3
    cmp-long v0, p1, v2

    if-nez v0, :cond_5

    cmp-long v0, p3, v4

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v0

    cmp-long v0, p3, v0

    if-nez v0, :cond_5

    .line 1225
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExcludeBoundaries: Invalid boundaries. Can not exclude full duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1227
    :cond_5
    iput-wide p1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeBeginTime:J

    .line 1228
    iput-wide p3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExcludeEndTime:J

    .line 1229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    .line 1231
    cmp-long v0, p1, v2

    if-nez v0, :cond_7

    .line 1232
    iput-boolean v6, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    .line 1233
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v0

    invoke-virtual {p0, p3, p4, v0, v1}, Lcom/lifevibes/videoeditor/MediaVideoItem;->setExtractBoundaries(JJ)V

    .line 1238
    :cond_6
    :goto_0
    return-void

    .line 1234
    :cond_7
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v0

    cmp-long v0, p3, v0

    if-eqz v0, :cond_8

    cmp-long v0, p3, v4

    if-nez v0, :cond_6

    .line 1235
    :cond_8
    iput-boolean v6, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    .line 1236
    invoke-virtual {p0, v2, v3, p1, p2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->setExtractBoundaries(JJ)V

    goto :goto_0
.end method

.method public setExtractBoundaries(JJ)V
    .locals 7
    .param p1, "beginMs"    # J
    .param p3, "endMs"    # J

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, -0x1

    .line 283
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 284
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Invalid start time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_0
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mDurationMs:J

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    .line 288
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Invalid end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_1
    cmp-long v0, p3, v2

    if-eqz v0, :cond_2

    cmp-long v0, p1, p3

    if-ltz v0, :cond_2

    .line 292
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Start time is greater than end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_2
    cmp-long v0, p1, v4

    if-ltz v0, :cond_3

    cmp-long v0, p3, v2

    if-eqz v0, :cond_4

    cmp-long v0, p3, v4

    if-gez v0, :cond_4

    .line 296
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Start time or end time is negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_4
    iput-wide p1, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    .line 301
    iput-wide p3, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    .line 302
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditorImpl;

    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->updateTimelineDuration()V

    .line 304
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaVideoItem;->mExclude:Z

    .line 310
    return-void
.end method
