.class public Lcom/lifevibes/videoeditor/WaveformData;
.super Ljava/lang/Object;
.source "WaveformData.java"


# instance fields
.field private final mFrameDurationMs:I

.field private final mFramesCount:I

.field private final mGains:[S

.field private mNumSampNeeded:J


# direct methods
.method private constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFrameDurationMs:I

    .line 71
    iput v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFramesCount:I

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mGains:[S

    .line 73
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mNumSampNeeded:J

    .line 74
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 11
    .param p1, "audioWaveformFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    if-nez p1, :cond_0

    .line 96
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "WaveformData : filename is null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 99
    :cond_0
    const/4 v1, 0x0

    .line 102
    .local v1, "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "audioGraphFileContext":Ljava/io/File;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    .end local v1    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    .local v2, "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    const/4 v8, 0x4

    :try_start_1
    new-array v4, v8, [B

    .line 110
    .local v4, "tempFrameDuration":[B
    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {v2, v4, v8, v9}, Ljava/io/FileInputStream;->read([BII)I

    .line 112
    const/4 v5, 0x0

    .line 113
    .local v5, "tempFrameDurationMs":I
    const/4 v7, 0x0

    .line 114
    .local v7, "tempFramesCounter":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v10, :cond_1

    .line 115
    shl-int/lit8 v5, v5, 0x8

    .line 116
    aget-byte v8, v4, v3

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v5, v8

    .line 114
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 118
    :cond_1
    iput v5, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFrameDurationMs:I

    .line 123
    const/4 v8, 0x4

    new-array v6, v8, [B

    .line 125
    .local v6, "tempFramesCount":[B
    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {v2, v6, v8, v9}, Ljava/io/FileInputStream;->read([BII)I

    .line 126
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v10, :cond_2

    .line 127
    shl-int/lit8 v7, v7, 0x8

    .line 128
    aget-byte v8, v6, v3

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v7, v8

    .line 126
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 130
    :cond_2
    iput v7, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFramesCount:I

    .line 135
    iget v8, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFramesCount:I

    new-array v8, v8, [S

    iput-object v8, p0, Lcom/lifevibes/videoeditor/WaveformData;->mGains:[S

    .line 137
    const/4 v3, 0x0

    :goto_2
    iget v8, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFramesCount:I

    if-ge v3, v8, :cond_3

    .line 138
    iget-object v8, p0, Lcom/lifevibes/videoeditor/WaveformData;->mGains:[S

    invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I

    move-result v9

    int-to-short v9, v9

    aput-short v9, v8, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 137
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 141
    :cond_3
    if-eqz v2, :cond_4

    .line 142
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 145
    :cond_4
    return-void

    .line 141
    .end local v0    # "audioGraphFileContext":Ljava/io/File;
    .end local v2    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    .end local v3    # "i":I
    .end local v4    # "tempFrameDuration":[B
    .end local v5    # "tempFrameDurationMs":I
    .end local v6    # "tempFramesCount":[B
    .end local v7    # "tempFramesCounter":I
    .restart local v1    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v8

    :goto_3
    if-eqz v1, :cond_5

    .line 142
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_5
    throw v8

    .line 141
    .end local v1    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    .restart local v0    # "audioGraphFileContext":Ljava/io/File;
    .restart local v2    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    .restart local v1    # "audioGraphFileReadHandle":Ljava/io/FileInputStream;
    goto :goto_3
.end method


# virtual methods
.method public getFrameDuration()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFrameDurationMs:I

    return v0
.end method

.method public getFrameGains()[S
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mGains:[S

    return-object v0
.end method

.method public getFramesCount()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mFramesCount:I

    return v0
.end method

.method getNumSampNeeded()J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/WaveformData;->mNumSampNeeded:J

    return-wide v0
.end method

.method setNumSampNeeded(J)V
    .locals 1
    .param p1, "numSampleNeeded"    # J

    .prologue
    .line 173
    iput-wide p1, p0, Lcom/lifevibes/videoeditor/WaveformData;->mNumSampNeeded:J

    .line 174
    return-void
.end method
