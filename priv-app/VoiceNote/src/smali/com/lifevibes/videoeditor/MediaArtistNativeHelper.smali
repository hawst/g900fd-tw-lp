.class Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;
.super Ljava/lang/Object;
.source "MediaArtistNativeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClips;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EffectSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioEffect;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionBehaviour;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioTransition;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$SlideDirection;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoTransition;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoRendering;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoEffect;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoFrameRate;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoFrameSize;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoProfile;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$VideoFormat;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Result;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$MediaRendering;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$FileType;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Bitrate;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioSamplingFrequency;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioFormat;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$OnProgressUpdateListener;,
        Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$CodecConfig;
    }
.end annotation


# static fields
.field private static final AUDIO_TRACK_PCM_FILE:Ljava/lang/String; = "AudioPcm.pcm"

.field private static final MediaShareJarVersion:Ljava/lang/String; = "MediaShare_J140"

.field public static final PROCESSING_AUDIO_PCM:I = 0x1

.field public static final PROCESSING_EXPORT:I = 0x14

.field public static final PROCESSING_INTERMEDIATE1:I = 0xb

.field public static final PROCESSING_INTERMEDIATE2:I = 0xc

.field public static final PROCESSING_INTERMEDIATE3:I = 0xd

.field public static final PROCESSING_KENBURNS:I = 0x3

.field public static final PROCESSING_NONE:I = 0x0

.field public static final PROCESSING_TRANSITION:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MediaArtistNativeHelper"

.field public static final TASK_ENCODING:I = 0x2

.field public static final TASK_LOADING_SETTINGS:I = 0x1

.field public static mExportDecConfig:I

.field public static mExportEncConfig:I

.field public static mTNDecConfigAccurateMode:I

.field public static mTNDecConfigFastMode:I

.field private static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private final MAX_THUMBNAIL_PERMITTED:I

.field private mAudioSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioSettings;

.field private mAudioTrackPCMFilePath:Ljava/lang/String;

.field private mClipList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

.field mEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

.field private mErrorFlagSet:Z

.field private mExportFilename:Ljava/lang/String;

.field private mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

.field private mExtractAudioWaveformProgressListener:Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;

.field public mInvalidatePreviewArray:Z

.field private mIsAbortGeneratePreviewInBGCalled:Z

.field private mIsFirstProgress:Z

.field private final mLock:Ljava/util/concurrent/Semaphore;

.field private mManualEditContext:I

.field private mOutputFilename:Ljava/lang/String;

.field private mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

.field mPreviewEffectsSize:I

.field private mPreviewProgress:J

.field private mProcessingObject:Ljava/lang/Object;

.field private mProcessingState:I

.field private mProgressToApp:I

.field private final mProjectPath:Ljava/lang/String;

.field private mRegenerateAudio:Z

.field private mRenderPreviewOverlayFile:Ljava/lang/String;

.field private mRenderPreviewRenderingMode:I

.field public mStoryBoardSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

.field private mThumbnailListListener:Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

.field mThumbnailTime:[I

.field private mTotalClips:I

.field mTotalKenBurnClips:I

.field mTotalTransitions:I

.field private final mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 83
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->sResizePaint:Landroid/graphics/Paint;

    .line 175
    const/4 v0, 0x0

    sput v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    .line 176
    sput v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    .line 186
    sput v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportDecConfig:I

    .line 187
    const/4 v0, 0x4

    sput v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportEncConfig:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Lcom/lifevibes/videoeditor/VideoEditor;)V
    .locals 6
    .param p1, "projectPath"    # Ljava/lang/String;
    .param p2, "lock"    # Ljava/util/concurrent/Semaphore;
    .param p3, "veObj"    # Lcom/lifevibes/videoeditor/VideoEditor;

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipList:Ljava/util/List;

    .line 77
    const/16 v1, 0x8

    iput v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->MAX_THUMBNAIL_PERMITTED:I

    .line 95
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    .line 97
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    .line 101
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioSettings;

    .line 104
    iput-boolean v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    .line 106
    iput-boolean v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mRegenerateAudio:Z

    .line 108
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    .line 138
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewProgress:J

    .line 140
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    .line 142
    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    .line 144
    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEffectsSize:I

    .line 145
    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalTransitions:I

    .line 146
    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalKenBurnClips:I

    .line 148
    iput-boolean v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mErrorFlagSet:Z

    .line 150
    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mManualEditContext:I

    .line 153
    iput-boolean v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mIsAbortGeneratePreviewInBGCalled:Z

    .line 2002
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    .line 2003
    if-eqz p3, :cond_1

    .line 2004
    iput-object p3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    .line 2009
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mStoryBoardSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    if-nez v1, :cond_0

    .line 2010
    new-instance v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-direct {v1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mStoryBoardSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    .line 2013
    :cond_0
    iput-object p2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mLock:Ljava/util/concurrent/Semaphore;

    .line 2015
    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    const-string v2, "null"

    invoke-direct {p0, v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->_init(Ljava/lang/String;Ljava/lang/String;)V

    .line 2016
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    .line 2017
    const-string v1, "MediaShareVerion"

    const-string v2, "MediaShare_J140"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2018
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getVersion()Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;

    move-result-object v0

    .line 2019
    .local v0, "version":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;
    const-string v1, "MediaArtistNativeHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoEditor Version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;->major:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;->minor:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;->revision:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2021
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2022
    invoke-direct {p0, v4, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->onProgressUpdate(II)V

    .line 2023
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;

    .line 2024
    invoke-direct {p0, v4, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->onAudioGraphExtractProgressUpdate(IZ)V

    .line 2025
    return-void

    .line 2006
    .end local v0    # "version":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;
    :cond_1
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    .line 2007
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "video editor object is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private native _init(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private exportAsAudioFile(Lcom/lifevibes/videoeditor/MediaAudioItem;Ljava/lang/String;)V
    .locals 3
    .param p1, "mAI"    # Lcom/lifevibes/videoeditor/MediaAudioItem;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0xfe

    const/4 v2, 0x0

    .line 2859
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2860
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoRenderingType:I

    .line 2861
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput-object p2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    iput-object p2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    .line 2862
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2863
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2864
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2865
    invoke-virtual {p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2882
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument Audio codec NOT Supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2867
    :pswitch_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2884
    :goto_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2885
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioBitrate()I

    move-result v1

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2886
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioSamplingFrequency()I

    move-result v1

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2887
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioChannels()I

    move-result v1

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    .line 2888
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v2, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    .line 2889
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    .line 2890
    return-void

    .line 2870
    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    goto :goto_0

    .line 2873
    :pswitch_2
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    goto :goto_0

    .line 2876
    :pswitch_3
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    goto :goto_0

    .line 2879
    :pswitch_4
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v1, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    goto :goto_0

    .line 2865
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private findVideoBitrate(I)I
    .locals 3
    .param p1, "frameSize"    # I

    .prologue
    .line 3552
    const/4 v0, 0x0

    .line 3554
    .local v0, "retValue":I
    packed-switch p1, :pswitch_data_0

    .line 3580
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "bitrate for frame size not found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3582
    :goto_0
    return v0

    .line 3559
    :pswitch_0
    const v0, 0x7d000

    .line 3560
    goto :goto_0

    .line 3562
    :pswitch_1
    const v0, 0xc3500

    .line 3563
    goto :goto_0

    .line 3566
    :pswitch_2
    const v0, 0x1e8480

    .line 3567
    goto :goto_0

    .line 3571
    :pswitch_3
    const v0, 0x4c4b40

    .line 3572
    goto :goto_0

    .line 3577
    :pswitch_4
    const v0, 0x7a1200

    .line 3578
    goto :goto_0

    .line 3554
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private findVideoFrameDimensions(I)Landroid/util/Pair;
    .locals 6
    .param p1, "videoResolution"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x438

    const/16 v1, 0x280

    const/4 v4, 0x0

    const/16 v3, 0x2d0

    const/16 v2, 0x1e0

    .line 3495
    packed-switch p1, :pswitch_data_0

    .line 3539
    new-instance v0, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3542
    .local v0, "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    return-object v0

    .line 3497
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_0
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x60

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3498
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3500
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_1
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0xa0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x78

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3501
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3503
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_2
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0xb0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x90

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3504
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3506
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_3
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x140

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3507
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3509
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_4
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x160

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3510
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3512
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_5
    new-instance v0, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3513
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3515
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_6
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x320

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3516
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 3518
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_7
    new-instance v0, Landroid/util/Pair;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3519
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3521
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_8
    new-instance v0, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x168

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3522
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3524
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_9
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x356

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3525
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3527
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_a
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x500

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3528
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3530
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_b
    new-instance v0, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3531
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3533
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_c
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x3c0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3534
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3536
    .end local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_d
    new-instance v0, Landroid/util/Pair;

    const/16 v1, 0x780

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3537
    .restart local v0    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 3495
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private findVideoResolution(II)I
    .locals 6
    .param p1, "aspectRatio"    # I
    .param p2, "height"    # I

    .prologue
    const/16 v5, 0x2d0

    const/16 v4, 0x1e0

    .line 3434
    const/4 v2, -0x1

    .line 3435
    .local v2, "retValue":I
    packed-switch p1, :pswitch_data_0

    .line 3475
    :cond_0
    :goto_0
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 3476
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v3}, Lcom/lifevibes/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v3

    invoke-static {v3}, Lcom/lifevibes/videoeditor/MediaProperties;->getSupportedResolutions(I)[Landroid/util/Pair;

    move-result-object v1

    .line 3478
    .local v1, "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v1, v3

    .line 3479
    .local v0, "maxResolution":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v3}, Lcom/lifevibes/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v4

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v4, v3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    move-result v2

    .line 3482
    .end local v0    # "maxResolution":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v1    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_1
    return v2

    .line 3437
    :pswitch_0
    if-ne p2, v4, :cond_2

    .line 3438
    const/4 v2, 0x7

    goto :goto_0

    .line 3439
    :cond_2
    if-ne p2, v5, :cond_0

    .line 3440
    const/16 v2, 0xb

    goto :goto_0

    .line 3443
    :pswitch_1
    if-ne p2, v4, :cond_3

    .line 3444
    const/16 v2, 0x9

    goto :goto_0

    .line 3445
    :cond_3
    if-ne p2, v5, :cond_4

    .line 3446
    const/16 v2, 0xa

    goto :goto_0

    .line 3447
    :cond_4
    const/16 v3, 0x438

    if-ne p2, v3, :cond_5

    .line 3448
    const/16 v2, 0xd

    goto :goto_0

    .line 3449
    :cond_5
    const/16 v3, 0x168

    if-ne p2, v3, :cond_0

    .line 3450
    const/16 v2, 0x8

    goto :goto_0

    .line 3453
    :pswitch_2
    const/16 v3, 0x60

    if-ne p2, v3, :cond_6

    .line 3454
    const/4 v2, 0x0

    goto :goto_0

    .line 3455
    :cond_6
    const/16 v3, 0x78

    if-ne p2, v3, :cond_7

    .line 3456
    const/4 v2, 0x1

    goto :goto_0

    .line 3457
    :cond_7
    const/16 v3, 0xf0

    if-ne p2, v3, :cond_8

    .line 3458
    const/4 v2, 0x3

    goto :goto_0

    .line 3459
    :cond_8
    if-ne p2, v4, :cond_9

    .line 3460
    const/4 v2, 0x5

    goto :goto_0

    .line 3461
    :cond_9
    if-ne p2, v5, :cond_0

    .line 3462
    const/16 v2, 0xc

    goto :goto_0

    .line 3465
    :pswitch_3
    if-ne p2, v4, :cond_0

    .line 3466
    const/4 v2, 0x6

    goto :goto_0

    .line 3469
    :pswitch_4
    const/16 v3, 0x90

    if-ne p2, v3, :cond_a

    .line 3470
    const/4 v2, 0x2

    goto :goto_0

    .line 3471
    :cond_a
    const/16 v3, 0x120

    if-ne p2, v3, :cond_0

    .line 3472
    const/4 v2, 0x4

    goto :goto_0

    .line 3435
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getAudioMediaItemProperties(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaAudioItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "mediaAudioItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaAudioItem;>;"
    const/16 v10, 0x64

    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2283
    iput v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    .line 2284
    new-instance v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-direct {v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    .line 2285
    new-instance v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    invoke-direct {v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;-><init>()V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    .line 2286
    new-array v1, v7, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    .line 2287
    .local v1, "clipSettingsArray":[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;
    new-instance v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    invoke-direct {v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    .line 2288
    .local v0, "clipSettings":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lifevibes/videoeditor/MediaAudioItem;

    .line 2289
    .local v3, "lMediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    .line 2290
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    new-array v5, v5, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    .line 2291
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    new-array v5, v8, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EffectSettings;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EffectSettings;

    .line 2292
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    add-int/lit8 v5, v5, -0x1

    new-array v5, v5, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    .line 2293
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    .line 2294
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v10, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->primaryTrackVolume:I

    .line 2295
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    new-array v5, v5, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    .line 2296
    invoke-virtual {p0, v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->initClipSettings(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    .line 2297
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFilename()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    .line 2298
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemFileType(I)I

    move-result v4

    iput v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    .line 2300
    :try_start_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    const/4 v5, 0x0

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    move-result-object v6

    aput-object v6, v4, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2305
    new-array v4, v7, [I

    iput-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    .line 2306
    new-array v4, v7, [I

    iput-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    .line 2307
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "lMediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    check-cast v3, Lcom/lifevibes/videoeditor/MediaAudioItem;

    .line 2308
    .restart local v3    # "lMediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getBoundaryBeginTime()J

    move-result-wide v6

    long-to-int v5, v6

    aput v5, v4, v8

    .line 2309
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    aput v8, v4, v9

    .line 2310
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getBoundaryEndTime()J

    move-result-wide v6

    long-to-int v5, v6

    aput v5, v4, v8

    .line 2311
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    aput v8, v4, v9

    .line 2312
    iput v9, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->mNumCuts:I

    .line 2313
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v8

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    aget v5, v5, v8

    iget-object v6, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    aget v6, v6, v8

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    .line 2314
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v8

    iget v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    if-eqz v4, :cond_0

    .line 2315
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v8

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v5, v5, v8

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    .line 2317
    :cond_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v8

    iget v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    if-eqz v4, :cond_1

    .line 2318
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v8

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v5, v5, v8

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    .line 2320
    :cond_1
    iput v10, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipVolumePercentage:I

    .line 2321
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    aput-object v0, v4, v8

    .line 2322
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v8, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2323
    return-void

    .line 2302
    :catch_0
    move-exception v2

    .line 2303
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported file or file not found"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private getMediaItemProperties(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    const/16 v13, 0x64

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2204
    iput v10, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    .line 2206
    new-instance v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-direct {v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    .line 2207
    new-instance v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    invoke-direct {v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;-><init>()V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    .line 2208
    new-array v1, v12, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    .line 2209
    .local v1, "clipSettingsArray":[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;
    new-instance v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    invoke-direct {v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    .line 2210
    .local v0, "clipSettings":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;
    const/4 v3, 0x0

    .line 2212
    .local v3, "lMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-nez v4, :cond_0

    .line 2213
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Not a media video item"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2216
    :cond_0
    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "lMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    check-cast v3, Lcom/lifevibes/videoeditor/MediaVideoItem;

    .line 2217
    .restart local v3    # "lMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    .line 2219
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    new-array v5, v5, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    .line 2220
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    new-array v5, v10, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EffectSettings;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EffectSettings;

    .line 2221
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    add-int/lit8 v5, v5, -0x1

    new-array v5, v5, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    .line 2222
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    .line 2223
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v13, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->primaryTrackVolume:I

    .line 2224
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    new-array v5, v5, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    iput-object v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    .line 2226
    invoke-virtual {p0, v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->initClipSettings(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    .line 2227
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    .line 2228
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFileType()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemFileType(I)I

    move-result v4

    iput v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    .line 2231
    :try_start_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    const/4 v5, 0x0

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    move-result-object v6

    aput-object v6, v4, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2236
    new-array v4, v12, [I

    iput-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    .line 2237
    new-array v4, v12, [I

    iput-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    .line 2239
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->isExcludeCalled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2240
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getExcludeEndTime()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getExcludeBeginTime()J

    move-result-wide v8

    long-to-int v7, v8

    sub-int/2addr v6, v7

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    .line 2243
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    aput v10, v4, v10

    .line 2244
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getExcludeBeginTime()J

    move-result-wide v6

    long-to-int v5, v6

    add-int/lit8 v5, v5, -0x1

    aput v5, v4, v10

    .line 2245
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getExcludeEndTime()J

    move-result-wide v6

    long-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v11

    .line 2246
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v6

    long-to-int v5, v6

    aput v5, v4, v11

    .line 2247
    iput v12, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->mNumCuts:I

    .line 2249
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    if-eqz v4, :cond_1

    .line 2250
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v5, v5, v10

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    .line 2253
    :cond_1
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    if-eqz v4, :cond_2

    .line 2254
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v5, v5, v10

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    .line 2272
    :cond_2
    :goto_0
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getRenderingMode()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    move-result v4

    iput v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    .line 2273
    iput v13, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipVolumePercentage:I

    .line 2275
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    aput-object v0, v4, v10

    .line 2277
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v5}, Lcom/lifevibes/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v5

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    move-result v5

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2280
    return-void

    .line 2233
    :catch_0
    move-exception v2

    .line 2234
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported file or file not found"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2258
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    move-result-wide v6

    long-to-int v5, v6

    aput v5, v4, v10

    .line 2259
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    aput v10, v4, v11

    .line 2260
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    move-result-wide v6

    long-to-int v5, v6

    aput v5, v4, v10

    .line 2261
    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    aput v10, v4, v11

    .line 2262
    iput v11, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->mNumCuts:I

    .line 2263
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayEndCutTime:[I

    aget v5, v5, v10

    iget-object v6, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->arrayBeginCutTime:[I

    aget v6, v6, v10

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    .line 2264
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    if-eqz v4, :cond_4

    .line 2265
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v5, v5, v10

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    .line 2268
    :cond_4
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    if-eqz v4, :cond_2

    .line 2269
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v4, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v4, v4, v10

    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    aget-object v5, v5, v10

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    iput v5, v4, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    goto/16 :goto_0
.end method

.method private native getVersion()Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private lock()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 3923
    const-string v0, "MediaArtistNativeHelper"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3924
    const-string v0, "MediaArtistNativeHelper"

    const-string v1, "lock: grabbing semaphore"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3926
    :cond_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 3927
    const-string v0, "MediaArtistNativeHelper"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3928
    const-string v0, "MediaArtistNativeHelper"

    const-string v1, "lock: grabbed semaphore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3930
    :cond_1
    return-void
.end method

.method private native nativeClearSurface(Landroid/view/Surface;)V
.end method

.method private native nativeGenerateAudioGraph(Ljava/lang/String;Ljava/lang/String;III)I
.end method

.method private native nativeGenerateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeGenerateRawAudio(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native nativeGenerateRawAudioMedia(Ljava/lang/String;Ljava/lang/String;JJ)I
.end method

.method private native nativeGetPixels(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;
.end method

.method private native nativeGetPixelsList(Ljava/lang/String;IIIIII[IILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)I
.end method

.method private native nativeGetPixelsList(Ljava/lang/String;IIIIII[II)[Landroid/graphics/Bitmap;
.end method

.method private native nativePopulateSettings(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$AudioSettings;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeRenderMediaItemPreviewFrame(Landroid/view/Surface;Ljava/lang/String;IIIIJ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeRenderPreviewFrame(Landroid/view/Surface;JII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeStartPreview(Landroid/view/Surface;JJIZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeStopPreview()I
.end method

.method private onAudioGraphExtractProgressUpdate(IZ)V
    .locals 1
    .param p1, "progress"    # I
    .param p2, "isVideo"    # Z

    .prologue
    .line 2078
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;

    if-eqz v0, :cond_0

    .line 2079
    if-lez p1, :cond_0

    .line 2080
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;

    invoke-interface {v0, p1}, Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;->onProgress(I)V

    .line 2083
    :cond_0
    return-void
.end method

.method private onProgressUpdate(II)V
    .locals 4
    .param p1, "taskId"    # I
    .param p2, "progress"    # I

    .prologue
    .line 2054
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 2055
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    if-eqz v0, :cond_0

    .line 2056
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    const/16 v1, 0x64

    if-gt v0, v1, :cond_0

    .line 2057
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    if-gt v0, p2, :cond_1

    .line 2058
    iput p2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    .line 2066
    :goto_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    invoke-interface {v0, v1, v2, v3}, Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;->onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V

    .line 2071
    :cond_0
    return-void

    .line 2060
    :cond_1
    rem-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_0

    .line 2061
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    goto :goto_0
.end method

.method private native release()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native stopEncoding()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native stopExtraction()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native stopThumbnail()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private unlock()V
    .locals 2

    .prologue
    .line 3936
    const-string v0, "MediaArtistNativeHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3937
    const-string v0, "MediaArtistNativeHelper"

    const-string v1, "unlock: releasing semaphore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3939
    :cond_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 3940
    return-void
.end method


# virtual methods
.method GetClosestVideoFrameRate(I)I
    .locals 6
    .param p1, "averageFrameRate"    # I

    .prologue
    const/4 v0, 0x5

    .line 3068
    const/16 v1, 0x19

    if-le p1, v1, :cond_1

    .line 3069
    const/4 v0, 0x7

    .line 3083
    :cond_0
    :goto_0
    return v0

    .line 3070
    :cond_1
    const/16 v1, 0x14

    if-le p1, v1, :cond_2

    .line 3071
    const/4 v0, 0x6

    goto :goto_0

    .line 3072
    :cond_2
    const/16 v1, 0xf

    if-gt p1, v1, :cond_0

    .line 3074
    int-to-double v2, p1

    const-wide/high16 v4, 0x4029000000000000L    # 12.5

    cmpl-double v1, v2, v4

    if-lez v1, :cond_3

    .line 3075
    const/4 v0, 0x4

    goto :goto_0

    .line 3076
    :cond_3
    const/16 v1, 0xa

    if-le p1, v1, :cond_4

    .line 3077
    const/4 v0, 0x3

    goto :goto_0

    .line 3078
    :cond_4
    int-to-double v2, p1

    const-wide/high16 v4, 0x401e000000000000L    # 7.5

    cmpl-double v1, v2, v4

    if-lez v1, :cond_5

    .line 3079
    const/4 v0, 0x2

    goto :goto_0

    .line 3080
    :cond_5
    if-le p1, v0, :cond_6

    .line 3081
    const/4 v0, 0x1

    goto :goto_0

    .line 3083
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public export(Ljava/lang/String;Ljava/lang/String;IIIIJLjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 21
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "projectDir"    # Ljava/lang/String;
    .param p3, "height"    # I
    .param p4, "bitrate"    # I
    .param p5, "audioCodec"    # I
    .param p6, "videoCodec"    # I
    .param p7, "outputFileSize"    # J
    .param p10, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIIIJ",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaItem;",
            ">;",
            "Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2535
    .local p9, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    const/4 v12, 0x0

    .line 2536
    .local v12, "outBitrate":I
    move-object/from16 v11, p9

    .line 2537
    .local v11, "newListMediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    .line 2538
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    .line 2539
    if-eqz p10, :cond_0

    .line 2540
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2542
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemProperties(Ljava/util/List;)V

    .line 2544
    sparse-switch p4, :sswitch_data_0

    .line 2596
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "Argument Bitrate incorrect"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 2546
    :sswitch_0
    const/16 v12, 0x7d00

    .line 2598
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x7

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2599
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    .line 2600
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoRenderingType:I

    .line 2602
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v15}, Lcom/lifevibes/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v3

    .line 2603
    .local v3, "aspectRatio":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2605
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    const/16 v16, 0x5

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2606
    const v15, 0xc3500

    if-ne v12, v15, :cond_1

    .line 2607
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x5

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2609
    :cond_1
    const v15, 0x7d000

    if-ne v12, v15, :cond_2

    .line 2610
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2614
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    const/16 v16, 0x4

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 2615
    const v15, 0x5dc00

    if-ne v12, v15, :cond_3

    .line 2616
    const/4 v15, 0x1

    move/from16 v0, p6

    if-eq v0, v15, :cond_6

    .line 2617
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x6

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2623
    :cond_3
    :goto_1
    const v15, 0x3e800

    if-ne v12, v15, :cond_4

    .line 2624
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2628
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 2629
    const v15, 0x2ee00

    if-ne v12, v15, :cond_8

    .line 2630
    const/4 v15, 0x1

    move/from16 v0, p6

    if-eq v0, v15, :cond_7

    .line 2631
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x5

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2648
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoBitrate(I)I

    move-result v13

    .line 2649
    .local v13, "videoBitratePreset":I
    if-ge v13, v12, :cond_b

    .line 2650
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "Argument Bitrate NOT Supported for supplied resolution"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 2549
    .end local v3    # "aspectRatio":I
    .end local v13    # "videoBitratePreset":I
    :sswitch_1
    const v12, 0xbb80

    .line 2550
    goto/16 :goto_0

    .line 2552
    :sswitch_2
    const v12, 0xfa00

    .line 2553
    goto/16 :goto_0

    .line 2555
    :sswitch_3
    const v12, 0x17700

    .line 2556
    goto/16 :goto_0

    .line 2558
    :sswitch_4
    const v12, 0x1f400

    .line 2559
    goto/16 :goto_0

    .line 2561
    :sswitch_5
    const v12, 0x2ee00

    .line 2562
    goto/16 :goto_0

    .line 2564
    :sswitch_6
    const v12, 0x3e800

    .line 2565
    goto/16 :goto_0

    .line 2567
    :sswitch_7
    const v12, 0x5dc00

    .line 2568
    goto/16 :goto_0

    .line 2570
    :sswitch_8
    const v12, 0x7d000

    .line 2571
    goto/16 :goto_0

    .line 2573
    :sswitch_9
    const v12, 0xc3500

    .line 2574
    goto/16 :goto_0

    .line 2576
    :sswitch_a
    const v12, 0xf4240

    .line 2577
    goto/16 :goto_0

    .line 2579
    :sswitch_b
    const v12, 0x124f80

    .line 2580
    goto/16 :goto_0

    .line 2582
    :sswitch_c
    const v12, 0x16e360

    .line 2583
    goto/16 :goto_0

    .line 2585
    :sswitch_d
    const v12, 0x1e8480

    .line 2586
    goto/16 :goto_0

    .line 2589
    :sswitch_e
    const v12, 0x4c4b40

    .line 2590
    goto/16 :goto_0

    .line 2593
    :sswitch_f
    const v12, 0x7a1200

    .line 2594
    goto/16 :goto_0

    .line 2620
    .restart local v3    # "aspectRatio":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    goto/16 :goto_1

    .line 2634
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    goto :goto_2

    .line 2637
    :cond_8
    const v15, 0x1f400

    if-ne v12, v15, :cond_9

    .line 2638
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    goto/16 :goto_2

    .line 2640
    :cond_9
    const v15, 0x17700

    if-ne v12, v15, :cond_a

    .line 2641
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    goto/16 :goto_2

    .line 2643
    :cond_a
    const v15, 0xfa00

    if-ne v12, v15, :cond_5

    .line 2644
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    goto/16 :goto_2

    .line 2653
    .restart local v13    # "videoBitratePreset":I
    :cond_b
    packed-switch p5, :pswitch_data_0

    .line 2668
    :goto_3
    packed-switch p6, :pswitch_data_1

    .line 2685
    :pswitch_0
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "Argument Video codec NOT Supported"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 2655
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2656
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const v16, 0x17700

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2657
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x7d00

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2658
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    goto :goto_3

    .line 2661
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2662
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x2fa8

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2663
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x1f40

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2664
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    goto :goto_3

    .line 2670
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2689
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v16, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeAccurateMode:I

    .line 2690
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v16, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeFastMode:I

    .line 2691
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v16, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportDecConfig:I

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportDecType:I

    .line 2692
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v16, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportEncConfig:I

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportEncType:I

    .line 2694
    const-wide/16 v16, 0x0

    cmp-long v15, p7, v16

    if-eqz v15, :cond_10

    .line 2695
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    move-wide/from16 v0, p7

    long-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    .line 2701
    :goto_5
    move-object/from16 v0, p0

    iget v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 2703
    const/4 v15, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/lifevibes/videoeditor/MediaItem;

    .line 2704
    .local v10, "lMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    invoke-virtual {v10}, Lcom/lifevibes/videoeditor/MediaItem;->getTimelineDuration()J

    move-result-wide v4

    .line 2705
    .local v4, "duration":J
    const-wide v16, 0x3ff0a3d70a3d70a4L    # 1.04

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    move-object/from16 v0, v18

    iget v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoBitrate:I

    move/from16 v18, v0

    add-int v15, v15, v18

    int-to-double v0, v15

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    long-to-double v0, v4

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    const-wide v18, 0x40bf400000000000L    # 8000.0

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-long v8, v0

    .line 2706
    .local v8, "fileSizeForNullEncoding":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoFrameDimensions(I)Landroid/util/Pair;

    move-result-object v14

    .line 2707
    .local v14, "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 2709
    .local v2, "NullEncodingOK":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    int-to-long v0, v15

    move-wide/from16 v16, v0

    cmp-long v15, v8, v16

    if-lez v15, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    if-nez v15, :cond_e

    .line 2712
    :cond_d
    const/4 v2, 0x1

    .line 2714
    :cond_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v15, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v16, v16, v17

    move-object/from16 v0, v16

    iget v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    iget-object v15, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v16, v16, v17

    move-object/from16 v0, v16

    iget v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    iget-object v15, v14, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v16, v16, v17

    move-object/from16 v0, v16

    iget v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/4 v15, 0x1

    if-ne v2, v15, :cond_f

    .line 2719
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0xfe

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2723
    .end local v2    # "NullEncodingOK":Z
    .end local v4    # "duration":J
    .end local v8    # "fileSizeForNullEncoding":J
    .end local v10    # "lMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    .end local v14    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v12, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2724
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    new-array v0, v0, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    .line 2727
    const/4 v6, 0x0

    .line 2729
    .local v6, "err":I
    const/16 v15, 0x14

    :try_start_0
    move-object/from16 v0, p0

    iput v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    .line 2730
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    .line 2731
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    move-result v6

    .line 2732
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2744
    if-eqz v6, :cond_11

    .line 2745
    const-string v15, "MediaArtistNativeHelper"

    const-string v16, "RuntimeException for generateClip"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2746
    new-instance v15, Ljava/lang/RuntimeException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "generateClip failed with error="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 2673
    .end local v6    # "err":I
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_4

    .line 2676
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x5

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_4

    .line 2679
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x6

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_4

    .line 2682
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_4

    .line 2697
    :cond_10
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v15, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    goto/16 :goto_5

    .line 2733
    .restart local v6    # "err":I
    :catch_0
    move-exception v7

    .line 2734
    .local v7, "ex":Ljava/lang/IllegalArgumentException;
    const-string v15, "MediaArtistNativeHelper"

    const-string v16, "IllegalArgument for generateClip"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2735
    throw v7

    .line 2736
    .end local v7    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v7

    .line 2737
    .local v7, "ex":Ljava/lang/IllegalStateException;
    const-string v15, "MediaArtistNativeHelper"

    const-string v16, "IllegalStateExceptiont for generateClip"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2738
    throw v7

    .line 2739
    .end local v7    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v7

    .line 2740
    .local v7, "ex":Ljava/lang/RuntimeException;
    const-string v15, "MediaArtistNativeHelper"

    const-string v16, "RuntimeException for generateClip"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2741
    throw v7

    .line 2749
    .end local v7    # "ex":Ljava/lang/RuntimeException;
    :cond_11
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2750
    return-void

    .line 2544
    :sswitch_data_0
    .sparse-switch
        0x6d60 -> :sswitch_0
        0x9c40 -> :sswitch_1
        0xfa00 -> :sswitch_2
        0x17700 -> :sswitch_3
        0x1f400 -> :sswitch_4
        0x2ee00 -> :sswitch_5
        0x3e800 -> :sswitch_6
        0x5dc00 -> :sswitch_7
        0x7d000 -> :sswitch_8
        0xc3500 -> :sswitch_9
        0xf4240 -> :sswitch_a
        0x124f80 -> :sswitch_b
        0x16e360 -> :sswitch_c
        0x1e8480 -> :sswitch_d
        0x4c4b40 -> :sswitch_e
        0x7a1200 -> :sswitch_f
    .end sparse-switch

    .line 2653
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 2668
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method export(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "projectDir"    # Ljava/lang/String;
    .param p3, "height"    # I
    .param p4, "bitrate"    # I
    .param p6, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaItem;",
            ">;",
            "Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2340
    .local p5, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    const/4 v8, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-nez v8, :cond_0

    .line 2341
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Not a media video item"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2343
    :cond_0
    const/4 v5, 0x0

    .line 2344
    .local v5, "outBitrate":I
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    .line 2345
    const/4 v8, 0x0

    iput v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    .line 2346
    if-eqz p6, :cond_1

    .line 2347
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2349
    :cond_1
    move-object/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemProperties(Ljava/util/List;)V

    .line 2351
    sparse-switch p4, :sswitch_data_0

    .line 2403
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Argument Bitrate incorrect"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2353
    :sswitch_0
    const/16 v5, 0x7d00

    .line 2405
    :goto_0
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x7

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2406
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    iput-object p1, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    .line 2407
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x0

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoRenderingType:I

    .line 2409
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v8}, Lcom/lifevibes/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v1

    .line 2410
    .local v1, "aspectRatio":I
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-direct {p0, v1, p3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    move-result v9

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2412
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v8, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    invoke-direct {p0, v8}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoBitrate(I)I

    move-result v6

    .line 2413
    .local v6, "videoBitratePreset":I
    if-ge v6, v5, :cond_2

    .line 2414
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Argument Bitrate NOT Supported for supplied resolution"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2356
    .end local v1    # "aspectRatio":I
    .end local v6    # "videoBitratePreset":I
    :sswitch_1
    const v5, 0xbb80

    .line 2357
    goto :goto_0

    .line 2359
    :sswitch_2
    const v5, 0xfa00

    .line 2360
    goto :goto_0

    .line 2362
    :sswitch_3
    const v5, 0x17700

    .line 2363
    goto :goto_0

    .line 2365
    :sswitch_4
    const v5, 0x1f400

    .line 2366
    goto :goto_0

    .line 2368
    :sswitch_5
    const v5, 0x2ee00

    .line 2369
    goto :goto_0

    .line 2371
    :sswitch_6
    const v5, 0x3e800

    .line 2372
    goto :goto_0

    .line 2374
    :sswitch_7
    const v5, 0x5dc00

    .line 2375
    goto :goto_0

    .line 2377
    :sswitch_8
    const v5, 0x7d000

    .line 2378
    goto :goto_0

    .line 2380
    :sswitch_9
    const v5, 0xc3500

    .line 2381
    goto :goto_0

    .line 2383
    :sswitch_a
    const v5, 0xf4240

    .line 2384
    goto :goto_0

    .line 2386
    :sswitch_b
    const v5, 0x124f80

    .line 2387
    goto :goto_0

    .line 2389
    :sswitch_c
    const v5, 0x16e360

    .line 2390
    goto :goto_0

    .line 2392
    :sswitch_d
    const v5, 0x1e8480

    .line 2393
    goto :goto_0

    .line 2396
    :sswitch_e
    const v5, 0x4c4b40

    .line 2397
    goto :goto_0

    .line 2399
    :sswitch_f
    const v5, 0x7a1200

    .line 2400
    goto :goto_0

    .line 2417
    .restart local v1    # "aspectRatio":I
    .restart local v6    # "videoBitratePreset":I
    :cond_2
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x4

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2418
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x2

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2419
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const v9, 0x17700

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2420
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v9, 0x7d00

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2421
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x2

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    .line 2422
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x0

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    .line 2423
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v5, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2425
    const/4 v8, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lifevibes/videoeditor/MediaVideoItem;

    .line 2427
    .local v4, "m1":Lcom/lifevibes/videoeditor/MediaVideoItem;
    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v8, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    const/4 v9, 0x4

    if-gt v8, v9, :cond_5

    .line 2431
    :cond_3
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v9, 0xfe

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2433
    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_4

    .line 2434
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v9

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2436
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v9

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2438
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v9

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    .line 2442
    :cond_4
    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 2459
    :pswitch_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Argument Video codec NOT Supported"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2444
    :pswitch_1
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x1

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2463
    :goto_1
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeAccurateMode:I

    .line 2464
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeFastMode:I

    .line 2465
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportDecConfig:I

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportDecType:I

    .line 2466
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportEncConfig:I

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportEncType:I

    .line 2469
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v8, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    .line 2470
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x4

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2477
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v9, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    add-int/lit8 v9, v9, -0x1

    new-array v9, v9, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    iput-object v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    .line 2480
    iget v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 2482
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v8, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    invoke-direct {p0, v8}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoFrameDimensions(I)Landroid/util/Pair;

    move-result-object v7

    .line 2483
    .local v7, "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v8, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    iget-object v9, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v9, v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget v9, v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    if-ne v8, v9, :cond_6

    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v9, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v9, v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget v9, v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    if-ne v8, v9, :cond_6

    iget-object v8, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v9, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mClipProperties:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    iget-object v9, v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget v9, v9, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    if-ne v8, v9, :cond_6

    .line 2487
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v9, 0xfe

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2491
    .end local v7    # "videoFrameDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_6
    const/4 v2, 0x0

    .line 2493
    .local v2, "err":I
    const/16 v8, 0x14

    :try_start_0
    iput v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    .line 2494
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    .line 2495
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {p0, v8}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    move-result v2

    .line 2496
    const/4 v8, 0x0

    iput v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2508
    if-eqz v2, :cond_8

    .line 2509
    const-string v8, "MediaArtistNativeHelper"

    const-string v9, "RuntimeException for generateClip"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2510
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "generateClip failed with error="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2447
    .end local v2    # "err":I
    :pswitch_2
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x4

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_1

    .line 2450
    :pswitch_3
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x5

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_1

    .line 2453
    :pswitch_4
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x6

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_1

    .line 2456
    :pswitch_5
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v9, 0x2

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_1

    .line 2472
    :cond_7
    iget-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v9

    invoke-virtual {p0, v9}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->GetClosestVideoFrameRate(I)I

    move-result v9

    iput v9, v8, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    goto/16 :goto_2

    .line 2497
    .restart local v2    # "err":I
    :catch_0
    move-exception v3

    .line 2498
    .local v3, "ex":Ljava/lang/IllegalArgumentException;
    const-string v8, "MediaArtistNativeHelper"

    const-string v9, "IllegalArgument for generateClip"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2499
    throw v3

    .line 2500
    .end local v3    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v3

    .line 2501
    .local v3, "ex":Ljava/lang/IllegalStateException;
    const-string v8, "MediaArtistNativeHelper"

    const-string v9, "IllegalStateExceptiont for generateClip"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2502
    throw v3

    .line 2503
    .end local v3    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v3

    .line 2504
    .local v3, "ex":Ljava/lang/RuntimeException;
    const-string v8, "MediaArtistNativeHelper"

    const-string v9, "RuntimeException for generateClip"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    throw v3

    .line 2513
    .end local v3    # "ex":Ljava/lang/RuntimeException;
    :cond_8
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2514
    return-void

    .line 2351
    nop

    :sswitch_data_0
    .sparse-switch
        0x6d60 -> :sswitch_0
        0x9c40 -> :sswitch_1
        0xfa00 -> :sswitch_2
        0x17700 -> :sswitch_3
        0x1f400 -> :sswitch_4
        0x2ee00 -> :sswitch_5
        0x3e800 -> :sswitch_6
        0x5dc00 -> :sswitch_7
        0x7d000 -> :sswitch_8
        0xc3500 -> :sswitch_9
        0xf4240 -> :sswitch_a
        0x124f80 -> :sswitch_b
        0x16e360 -> :sswitch_c
        0x1e8480 -> :sswitch_d
        0x4c4b40 -> :sswitch_e
        0x7a1200 -> :sswitch_f
    .end sparse-switch

    .line 2442
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method export(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "projectDir"    # Ljava/lang/String;
    .param p5, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaAudioItem;",
            ">;",
            "Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2905
    .local p3, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    .local p4, "mediaAudioItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaAudioItem;>;"
    const/4 v5, 0x0

    iput v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    .line 2906
    if-eqz p5, :cond_0

    .line 2907
    iput-object p5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2909
    :cond_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_c

    .line 2911
    const/16 v2, 0xff

    .line 2912
    .local v2, "fileType":I
    const-string v5, ".3gp"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ".3GP"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ".3ga"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ".3GA"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ".m4a"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ".M4A"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2914
    :cond_1
    const/4 v2, 0x0

    .line 2926
    :cond_2
    :goto_0
    const/4 v5, 0x0

    invoke-interface {p4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lifevibes/videoeditor/MediaAudioItem;

    invoke-virtual {v5}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v5

    if-eq v5, v2, :cond_b

    .line 2928
    const/4 v5, 0x0

    invoke-interface {p4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lifevibes/videoeditor/MediaAudioItem;

    invoke-virtual {v5}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v3

    .line 2929
    .local v3, "infileType":I
    const/16 v5, 0xb

    if-eq v3, v5, :cond_b

    const/16 v5, 0xc

    if-eq v3, v5, :cond_b

    .line 2931
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string v6, "This Export kind is not supported for MediaAudioItems."

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2915
    .end local v3    # "infileType":I
    :cond_3
    const-string v5, ".mp4"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, ".MP4"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2916
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 2917
    :cond_5
    const-string v5, ".AMR"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, ".amr"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2918
    :cond_6
    const/4 v2, 0x2

    goto :goto_0

    .line 2920
    :cond_7
    const-string v5, ".AVI"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    const-string v5, ".avi"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2921
    :cond_8
    const/16 v2, 0xb

    goto :goto_0

    .line 2922
    :cond_9
    const-string v5, ".MKV"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    const-string v5, ".mkv"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2923
    :cond_a
    const/16 v2, 0xc

    goto :goto_0

    .line 2934
    :cond_b
    invoke-direct {p0, p4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioMediaItemProperties(Ljava/util/List;)V

    .line 2935
    const/4 v5, 0x0

    invoke-interface {p4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/lifevibes/videoeditor/MediaAudioItem;

    invoke-direct {p0, v5, p1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->exportAsAudioFile(Lcom/lifevibes/videoeditor/MediaAudioItem;Ljava/lang/String;)V

    .line 2990
    .end local v2    # "fileType":I
    :goto_1
    const/4 v0, 0x0

    .line 2992
    .local v0, "err":I
    const/16 v5, 0x14

    :try_start_0
    iput v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    .line 2993
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    .line 2994
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {p0, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    move-result v0

    .line 2995
    const/4 v5, 0x0

    iput v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 3007
    if-eqz v0, :cond_e

    .line 3008
    const-string v5, "MediaArtistNativeHelper"

    const-string v6, "RuntimeException for generateClip"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3009
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "generateClip failed with error="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2937
    .end local v0    # "err":I
    :cond_c
    const/4 v5, 0x0

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/lifevibes/videoeditor/MediaVideoItem;

    .line 2938
    .local v4, "mVI":Lcom/lifevibes/videoeditor/MediaVideoItem;
    invoke-direct {p0, p3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemProperties(Ljava/util/List;)V

    .line 2939
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    .line 2940
    const/4 v5, 0x0

    iput v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    .line 2941
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->GetClosestVideoFrameRate(I)I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2942
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x0

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoRenderingType:I

    .line 2943
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    iput-object p1, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    .line 2944
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v7

    invoke-virtual {p0, v6, v7}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    move-result v6

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v7

    invoke-direct {p0, v6, v7}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2945
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getClosestVideoBitrate(I)I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2947
    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 2964
    :pswitch_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Argument Video codec NOT Supported"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2949
    :pswitch_1
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x1

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2967
    :goto_2
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeAccurateMode:I

    .line 2968
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeFastMode:I

    .line 2969
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportDecConfig:I

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportDecType:I

    .line 2970
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportEncConfig:I

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportEncType:I

    .line 2973
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_d

    .line 2974
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x4

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2975
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v5, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    const v6, 0x5dc00

    if-le v5, v6, :cond_d

    .line 2976
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const v6, 0x5dc00

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2979
    :cond_d
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v6, 0xfe

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2980
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v6, 0xfe

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2981
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2982
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2983
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    .line 2985
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDisplayAngle()I

    move-result v6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mVideoRotation:I

    .line 2987
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x0

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    .line 2988
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    goto/16 :goto_1

    .line 2952
    :pswitch_2
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x4

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto :goto_2

    .line 2955
    :pswitch_3
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x5

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto :goto_2

    .line 2958
    :pswitch_4
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x6

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto :goto_2

    .line 2961
    :pswitch_5
    iget-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v6, 0x2

    iput v6, v5, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_2

    .line 2996
    .end local v4    # "mVI":Lcom/lifevibes/videoeditor/MediaVideoItem;
    .restart local v0    # "err":I
    :catch_0
    move-exception v1

    .line 2997
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v5, "MediaArtistNativeHelper"

    const-string v6, "IllegalArgument for generateClip"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2998
    throw v1

    .line 2999
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 3000
    .local v1, "ex":Ljava/lang/IllegalStateException;
    const-string v5, "MediaArtistNativeHelper"

    const-string v6, "IllegalStateExceptiont for generateClip"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3001
    throw v1

    .line 3002
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 3003
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v5, "MediaArtistNativeHelper"

    const-string v6, "RuntimeException for generateClip"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3004
    throw v1

    .line 3012
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :cond_e
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 3013
    return-void

    .line 2947
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method exportAs2D(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "projectDir"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaItem;",
            ">;",
            "Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    const v9, 0x5dc00

    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2756
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-nez v2, :cond_0

    .line 2757
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Not a media video item"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2760
    :cond_0
    invoke-direct {p0, p3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaItemProperties(Ljava/util/List;)V

    .line 2761
    if-eqz p4, :cond_1

    .line 2762
    iput-object p4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2763
    :cond_1
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    .line 2764
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->GetClosestVideoFrameRate(I)I

    move-result v2

    iput v2, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2765
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    iput-object p1, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    .line 2766
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaItem;->getWidth()I

    move-result v4

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaItem;->getHeight()I

    move-result v2

    invoke-virtual {p0, v4, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    move-result v4

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaItem;->getHeight()I

    move-result v2

    invoke-direct {p0, v4, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    move-result v2

    iput v2, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2767
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getClosestVideoBitrate(I)I

    move-result v2

    iput v2, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2768
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v6, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoRenderingType:I

    .line 2771
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2788
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument Video codec NOT Supported"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2773
    :pswitch_1
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v6, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    .line 2791
    :goto_0
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeAccurateMode:I

    .line 2792
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mTNDecTypeFastMode:I

    .line 2793
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportDecConfig:I

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportDecType:I

    .line 2794
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    sget v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportEncConfig:I

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->mExportEncType:I

    .line 2797
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v2, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    if-ne v2, v6, :cond_2

    .line 2798
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v7, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2799
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v2, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    if-le v2, v9, :cond_2

    .line 2800
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v9, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2804
    :cond_2
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 2806
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 2823
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v5, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    .line 2824
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    add-int/lit8 v3, v3, -0x1

    new-array v3, v3, [Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    iput-object v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    .line 2826
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v2, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    if-ne v2, v6, :cond_4

    .line 2827
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v7, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    .line 2828
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const v3, 0x3e800

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    .line 2829
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iget v2, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    if-le v2, v7, :cond_4

    .line 2830
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v7, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    .line 2833
    :cond_4
    const/4 v0, 0x0

    .line 2835
    .local v0, "err":I
    const/16 v2, 0x14

    :try_start_0
    iput v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    .line 2836
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    .line 2837
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-virtual {p0, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    move-result v0

    .line 2838
    const/4 v2, 0x0

    iput v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProcessingState:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2850
    if-eqz v0, :cond_5

    .line 2851
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "RuntimeException for generateClip"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2852
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "generateClip failed with error="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2776
    .end local v0    # "err":I
    :pswitch_2
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v7, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_0

    .line 2779
    :pswitch_3
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v3, 0x5

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_0

    .line 2782
    :pswitch_4
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/4 v3, 0x6

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_0

    .line 2785
    :pswitch_5
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v8, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    goto/16 :goto_0

    .line 2808
    :pswitch_6
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v8, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2809
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getClosestAudioBitrate(I)I

    move-result v2

    iput v2, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2810
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v2

    iput v2, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2811
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v2

    iput v2, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    goto/16 :goto_1

    .line 2814
    :pswitch_7
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v6, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    .line 2815
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v3, 0x2fa8

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    .line 2816
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    const/16 v3, 0x1f40

    iput v3, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    .line 2817
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    iput v6, v2, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    goto/16 :goto_1

    .line 2839
    .restart local v0    # "err":I
    :catch_0
    move-exception v1

    .line 2840
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "IllegalArgument for generateClip"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2841
    throw v1

    .line 2842
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 2843
    .local v1, "ex":Ljava/lang/IllegalStateException;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "IllegalStateExceptiont for generateClip"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2844
    throw v1

    .line 2845
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 2846
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "RuntimeException for generateClip"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2847
    throw v1

    .line 2855
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :cond_5
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;

    .line 2856
    return-void

    .line 2771
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 2806
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method findThumbnailMode(Ljava/lang/String;IJJ)Z
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "thumbnailCount"    # I
    .param p3, "startMs"    # J
    .param p5, "endMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 3854
    const/4 v1, 0x1

    .line 3858
    .local v1, "result":Z
    :try_start_0
    invoke-virtual/range {p0 .. p6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeFindThumbnailMode(Ljava/lang/String;IJJ)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3864
    :goto_0
    return v1

    .line 3859
    :catch_0
    move-exception v0

    .line 3860
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "Runtime exception in nativeFindThumbnailMode"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3862
    const/4 v1, 0x0

    goto :goto_0
.end method

.method generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;ZJ)I
    .locals 11
    .param p1, "uniqueId"    # Ljava/lang/String;
    .param p2, "inFileName"    # Ljava/lang/String;
    .param p3, "OutAudiGraphFileName"    # Ljava/lang/String;
    .param p4, "frameDuration"    # I
    .param p5, "audioChannels"    # I
    .param p6, "samplesCount"    # I
    .param p7, "listener"    # Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;
    .param p8, "isVideo"    # Z
    .param p9, "duration"    # J

    .prologue
    .line 2105
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;

    .line 2106
    const/4 v2, -0x1

    .line 2116
    .local v2, "result":I
    if-eqz p8, :cond_2

    .line 2117
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".pcm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2118
    .local v5, "tempPCMFileName":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 2119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "AudioPcm.pcm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2126
    :cond_0
    :goto_0
    int-to-long v8, p4

    move-object v3, p0

    move-object v4, p2

    move-wide/from16 v6, p9

    invoke-direct/range {v3 .. v9}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGenerateRawAudioMedia(Ljava/lang/String;Ljava/lang/String;JJ)I

    move-result v2

    .line 2127
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move-object v4, p0

    move-object v6, p3

    move v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    .line 2128
    invoke-direct/range {v4 .. v9}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGenerateAudioGraph(Ljava/lang/String;Ljava/lang/String;III)I

    move-result v2

    .line 2134
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2135
    return v2

    .line 2121
    .end local v5    # "tempPCMFileName":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "AudioPcm.pcm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "tempPCMFileName":Ljava/lang/String;
    iput-object v5, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    goto :goto_0
.end method

.method public generateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I
    .locals 5
    .param p1, "editSettings"    # Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;

    .prologue
    const/4 v2, -0x1

    .line 3096
    const/4 v0, 0x0

    .line 3099
    .local v0, "err":I
    :try_start_0
    invoke-direct {p0, p1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGenerateClip(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$EditSettings;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    move v2, v0

    .line 3122
    :cond_0
    :goto_0
    return v2

    .line 3100
    :catch_0
    move-exception v1

    .line 3101
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v3, "MediaArtistNativeHelper"

    const-string v4, "Illegal Argument exception in load settings"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3102
    if-eqz v0, :cond_0

    move v2, v0

    .line 3105
    goto :goto_0

    .line 3107
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 3108
    .local v1, "ex":Ljava/lang/IllegalStateException;
    const-string v3, "MediaArtistNativeHelper"

    const-string v4, "Illegal state exception in load settings"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3109
    if-eqz v0, :cond_0

    move v2, v0

    .line 3112
    goto :goto_0

    .line 3114
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 3115
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v3, "MediaArtistNativeHelper"

    const-string v4, "Runtime exception in load settings"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3116
    if-eqz v0, :cond_0

    move v2, v0

    .line 3119
    goto :goto_0
.end method

.method getAspectRatio(II)I
    .locals 8
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 3172
    int-to-double v4, p1

    int-to-double v6, p2

    div-double v0, v4, v6

    .line 3173
    .local v0, "apRatio":D
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v0, v1}, Ljava/math/BigDecimal;-><init>(D)V

    .line 3174
    .local v2, "bd":Ljava/math/BigDecimal;
    const/4 v4, 0x3

    const/4 v5, 0x4

    invoke-virtual {v2, v4, v5}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v2

    .line 3175
    invoke-virtual {v2}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    .line 3176
    const/4 v3, 0x2

    .line 3177
    .local v3, "var":I
    const-wide v4, 0x3ffb333333333333L    # 1.7

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_1

    .line 3178
    const/4 v3, 0x2

    .line 3188
    :cond_0
    :goto_0
    return v3

    .line 3179
    :cond_1
    const-wide v4, 0x3ff999999999999aL    # 1.6

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_2

    .line 3180
    const/4 v3, 0x4

    goto :goto_0

    .line 3181
    :cond_2
    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_3

    .line 3182
    const/4 v3, 0x1

    goto :goto_0

    .line 3183
    :cond_3
    const-wide v4, 0x3ff4cccccccccccdL    # 1.3

    cmpl-double v4, v0, v4

    if-lez v4, :cond_4

    .line 3184
    const/4 v3, 0x3

    goto :goto_0

    .line 3185
    :cond_4
    const-wide v4, 0x3ff3333333333333L    # 1.2

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_0

    .line 3186
    const/4 v3, 0x5

    goto :goto_0
.end method

.method getAudioCodecType(I)I
    .locals 1
    .param p1, "codecType"    # I

    .prologue
    .line 3284
    const/4 v0, -0x1

    .line 3285
    .local v0, "retValue":I
    packed-switch p1, :pswitch_data_0

    .line 3303
    const/4 v0, -0x1

    .line 3305
    :goto_0
    return v0

    .line 3287
    :pswitch_0
    const/4 v0, 0x1

    .line 3288
    goto :goto_0

    .line 3290
    :pswitch_1
    const/4 v0, 0x2

    .line 3291
    goto :goto_0

    .line 3293
    :pswitch_2
    const/4 v0, 0x3

    .line 3294
    goto :goto_0

    .line 3296
    :pswitch_3
    const/4 v0, 0x4

    .line 3297
    goto :goto_0

    .line 3299
    :pswitch_4
    const/4 v0, 0x5

    .line 3300
    goto :goto_0

    .line 3285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method getAudioflag()Z
    .locals 1

    .prologue
    .line 3057
    iget-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mRegenerateAudio:Z

    return v0
.end method

.method getClosestAudioBitrate(I)I
    .locals 5
    .param p1, "bitrateValue"    # I

    .prologue
    const v4, 0xfa00

    const v3, 0xbb80

    const/16 v2, 0x7d00

    const/16 v1, 0x5dc0

    const/16 v0, 0x3e80

    .line 2180
    if-lez p1, :cond_0

    if-gt p1, v0, :cond_0

    .line 2199
    :goto_0
    return v0

    .line 2182
    :cond_0
    if-le p1, v0, :cond_1

    if-gt p1, v1, :cond_1

    move v0, v1

    .line 2183
    goto :goto_0

    .line 2184
    :cond_1
    if-le p1, v1, :cond_2

    if-gt p1, v2, :cond_2

    move v0, v2

    .line 2185
    goto :goto_0

    .line 2186
    :cond_2
    if-le p1, v2, :cond_3

    if-gt p1, v3, :cond_3

    move v0, v3

    .line 2187
    goto :goto_0

    .line 2188
    :cond_3
    if-le p1, v3, :cond_4

    if-gt p1, v4, :cond_4

    move v0, v4

    .line 2189
    goto :goto_0

    .line 2190
    :cond_4
    if-le p1, v4, :cond_5

    const v0, 0x17700

    if-gt p1, v0, :cond_5

    .line 2191
    const v0, 0x17700

    goto :goto_0

    .line 2192
    :cond_5
    const v0, 0x17700

    if-le p1, v0, :cond_6

    const v0, 0x1f400

    if-gt p1, v0, :cond_6

    .line 2193
    const v0, 0x1f400

    goto :goto_0

    .line 2194
    :cond_6
    const v0, 0x1f400

    if-le p1, v0, :cond_7

    const v0, 0x2ee00

    if-gt p1, v0, :cond_7

    .line 2195
    const v0, 0x2ee00

    goto :goto_0

    .line 2196
    :cond_7
    const v0, 0x2ee00

    if-le p1, v0, :cond_8

    const v0, 0x3e800

    if-gt p1, v0, :cond_8

    .line 2197
    const v0, 0x3e800

    goto :goto_0

    .line 2199
    :cond_8
    const/4 v0, -0x1

    goto :goto_0
.end method

.method getClosestVideoBitrate(I)I
    .locals 5
    .param p1, "bitrateValue"    # I

    .prologue
    const v3, 0x2ee00

    const v2, 0x1f400

    const v1, 0x17700

    const v0, 0xfa00

    const v4, 0x7a1200

    .line 2140
    if-lez p1, :cond_0

    if-gt p1, v0, :cond_0

    .line 2173
    :goto_0
    return v0

    .line 2142
    :cond_0
    if-le p1, v0, :cond_1

    if-gt p1, v1, :cond_1

    move v0, v1

    .line 2143
    goto :goto_0

    .line 2144
    :cond_1
    if-le p1, v1, :cond_2

    if-gt p1, v2, :cond_2

    move v0, v2

    .line 2145
    goto :goto_0

    .line 2146
    :cond_2
    if-le p1, v2, :cond_3

    if-gt p1, v3, :cond_3

    move v0, v3

    .line 2147
    goto :goto_0

    .line 2148
    :cond_3
    if-le p1, v3, :cond_4

    const v0, 0x3e800

    if-gt p1, v0, :cond_4

    .line 2149
    const v0, 0x3e800

    goto :goto_0

    .line 2150
    :cond_4
    const v0, 0x3e800

    if-le p1, v0, :cond_5

    const v0, 0x46500

    if-gt p1, v0, :cond_5

    .line 2151
    const v0, 0x46500

    goto :goto_0

    .line 2152
    :cond_5
    const v0, 0x46500

    if-le p1, v0, :cond_6

    const v0, 0x5dc00

    if-gt p1, v0, :cond_6

    .line 2153
    const v0, 0x5dc00

    goto :goto_0

    .line 2154
    :cond_6
    const v0, 0x5dc00

    if-le p1, v0, :cond_7

    const v0, 0x7d000

    if-gt p1, v0, :cond_7

    .line 2155
    const v0, 0x7d000

    goto :goto_0

    .line 2156
    :cond_7
    const v0, 0x7d000

    if-le p1, v0, :cond_8

    const v0, 0xc3500

    if-gt p1, v0, :cond_8

    .line 2157
    const v0, 0xc3500

    goto :goto_0

    .line 2158
    :cond_8
    const v0, 0xc3500

    if-le p1, v0, :cond_9

    const v0, 0xf4240

    if-gt p1, v0, :cond_9

    .line 2159
    const v0, 0xf4240

    goto :goto_0

    .line 2160
    :cond_9
    const v0, 0xf4240

    if-le p1, v0, :cond_a

    const v0, 0x124f80

    if-gt p1, v0, :cond_a

    .line 2161
    const v0, 0x124f80

    goto :goto_0

    .line 2162
    :cond_a
    const v0, 0x124f80

    if-le p1, v0, :cond_b

    const v0, 0x16e360

    if-gt p1, v0, :cond_b

    .line 2163
    const v0, 0x16e360

    goto :goto_0

    .line 2164
    :cond_b
    const v0, 0x16e360

    if-le p1, v0, :cond_c

    const v0, 0x1e8480

    if-gt p1, v0, :cond_c

    .line 2165
    const v0, 0x1e8480

    goto/16 :goto_0

    .line 2166
    :cond_c
    const v0, 0x1e8480

    if-le p1, v0, :cond_d

    const v0, 0x4c4b40

    if-gt p1, v0, :cond_d

    .line 2167
    const v0, 0x4c4b40

    goto/16 :goto_0

    .line 2168
    :cond_d
    const v0, 0x4c4b40

    if-le p1, v0, :cond_e

    if-gt p1, v4, :cond_e

    move v0, v4

    .line 2169
    goto/16 :goto_0

    .line 2170
    :cond_e
    if-le p1, v4, :cond_f

    move v0, v4

    .line 2171
    goto/16 :goto_0

    .line 2173
    :cond_f
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method getFileType(I)I
    .locals 1
    .param p1, "fileType"    # I

    .prologue
    .line 3200
    const/4 v0, -0x1

    .line 3201
    .local v0, "retValue":I
    sparse-switch p1, :sswitch_data_0

    .line 3233
    const/4 v0, -0x1

    .line 3235
    :goto_0
    return v0

    .line 3203
    :sswitch_0
    const/16 v0, 0xff

    .line 3204
    goto :goto_0

    .line 3206
    :sswitch_1
    const/4 v0, 0x0

    .line 3207
    goto :goto_0

    .line 3209
    :sswitch_2
    const/4 v0, 0x1

    .line 3210
    goto :goto_0

    .line 3212
    :sswitch_3
    const/4 v0, 0x5

    .line 3213
    goto :goto_0

    .line 3215
    :sswitch_4
    const/16 v0, 0x8

    .line 3216
    goto :goto_0

    .line 3218
    :sswitch_5
    const/4 v0, 0x3

    .line 3219
    goto :goto_0

    .line 3221
    :sswitch_6
    const/16 v0, 0xa

    .line 3222
    goto :goto_0

    .line 3224
    :sswitch_7
    const/4 v0, 0x2

    .line 3225
    goto :goto_0

    .line 3227
    :sswitch_8
    const/16 v0, 0xb

    .line 3228
    goto :goto_0

    .line 3230
    :sswitch_9
    const/16 v0, 0xc

    .line 3231
    goto :goto_0

    .line 3201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_7
        0x3 -> :sswitch_5
        0x5 -> :sswitch_3
        0x8 -> :sswitch_4
        0xa -> :sswitch_6
        0xb -> :sswitch_8
        0xc -> :sswitch_9
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method getFrameRate(I)I
    .locals 1
    .param p1, "fps"    # I

    .prologue
    .line 3316
    const/4 v0, -0x1

    .line 3317
    .local v0, "retValue":I
    packed-switch p1, :pswitch_data_0

    .line 3344
    const/4 v0, -0x1

    .line 3346
    :goto_0
    return v0

    .line 3319
    :pswitch_0
    const/4 v0, 0x5

    .line 3320
    goto :goto_0

    .line 3322
    :pswitch_1
    const/16 v0, 0x8

    .line 3323
    goto :goto_0

    .line 3325
    :pswitch_2
    const/16 v0, 0xa

    .line 3326
    goto :goto_0

    .line 3328
    :pswitch_3
    const/16 v0, 0xd

    .line 3329
    goto :goto_0

    .line 3331
    :pswitch_4
    const/16 v0, 0xf

    .line 3332
    goto :goto_0

    .line 3334
    :pswitch_5
    const/16 v0, 0x14

    .line 3335
    goto :goto_0

    .line 3337
    :pswitch_6
    const/16 v0, 0x19

    .line 3338
    goto :goto_0

    .line 3340
    :pswitch_7
    const/16 v0, 0x1e

    .line 3341
    goto :goto_0

    .line 3317
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method getGeneratePreview()Z
    .locals 1

    .prologue
    .line 3160
    iget-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    return v0
.end method

.method getMediaItemFileType(I)I
    .locals 1
    .param p1, "fileType"    # I

    .prologue
    .line 3358
    const/4 v0, -0x1

    .line 3360
    .local v0, "retValue":I
    sparse-switch p1, :sswitch_data_0

    .line 3389
    const/4 v0, -0x1

    .line 3391
    :goto_0
    return v0

    .line 3362
    :sswitch_0
    const/16 v0, 0xff

    .line 3363
    goto :goto_0

    .line 3365
    :sswitch_1
    const/4 v0, 0x0

    .line 3366
    goto :goto_0

    .line 3368
    :sswitch_2
    const/4 v0, 0x1

    .line 3369
    goto :goto_0

    .line 3371
    :sswitch_3
    const/4 v0, 0x5

    .line 3372
    goto :goto_0

    .line 3374
    :sswitch_4
    const/16 v0, 0x8

    .line 3375
    goto :goto_0

    .line 3377
    :sswitch_5
    const/16 v0, 0xa

    .line 3378
    goto :goto_0

    .line 3380
    :sswitch_6
    const/4 v0, 0x2

    .line 3381
    goto :goto_0

    .line 3383
    :sswitch_7
    const/16 v0, 0xb

    .line 3384
    goto :goto_0

    .line 3386
    :sswitch_8
    const/16 v0, 0xc

    .line 3387
    goto :goto_0

    .line 3360
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_6
        0x5 -> :sswitch_3
        0x8 -> :sswitch_4
        0xa -> :sswitch_5
        0xb -> :sswitch_7
        0xc -> :sswitch_8
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method getMediaItemRenderingMode(I)I
    .locals 1
    .param p1, "renderingMode"    # I

    .prologue
    .line 3404
    const/4 v0, -0x1

    .line 3405
    .local v0, "retValue":I
    packed-switch p1, :pswitch_data_0

    .line 3417
    const/4 v0, -0x1

    .line 3419
    :goto_0
    return v0

    .line 3407
    :pswitch_0
    const/4 v0, 0x2

    .line 3408
    goto :goto_0

    .line 3410
    :pswitch_1
    const/4 v0, 0x0

    .line 3411
    goto :goto_0

    .line 3413
    :pswitch_2
    const/4 v0, 0x1

    .line 3414
    goto :goto_0

    .line 3405
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method native getMediaProperties(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;,
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method getPixels(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "inputFile"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "timeMS"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 3597
    if-nez p1, :cond_0

    .line 3598
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid input file"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3600
    :cond_0
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNAccurateCodecConfig()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixels(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method getPixelsFast(Ljava/lang/String;IIJ)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "inputFile"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "timeMS"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 3620
    const/4 v7, 0x0

    .line 3622
    .local v7, "TNFastConfigFailed":Z
    if-nez p1, :cond_0

    .line 3623
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 3628
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNFastCodecConfig()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsFast(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3647
    :goto_0
    return-object v0

    .line 3630
    :catch_0
    move-exception v8

    .line 3631
    .local v8, "x":Ljava/lang/Exception;
    const-string v0, "MediaArtistNativeHelper"

    const-string v1, "Runtime exception in getPixelsFast call "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3632
    const/4 v7, 0x1

    .line 3636
    const/4 v0, 0x1

    if-ne v7, v0, :cond_1

    .line 3639
    :try_start_1
    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNAccurateCodecConfig()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsFast(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 3641
    :catch_1
    move-exception v8

    .line 3642
    .local v8, "x":Ljava/lang/RuntimeException;
    const-string v0, "MediaArtistNativeHelper"

    const-string v1, "Runtime exception in getPixelsFast call with fallback"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3643
    throw v8

    .line 3647
    .local v8, "x":Ljava/lang/Exception;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getPixelsList(Ljava/lang/String;IIJJILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .locals 14
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "startMs"    # J
    .param p6, "endMs"    # J
    .param p8, "thumbnailCount"    # I
    .param p9, "callback"    # Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    .prologue
    .line 3691
    move/from16 v0, p8

    new-array v10, v0, [I

    .line 3692
    .local v10, "tnTimeList":[I
    sub-long v2, p6, p4

    long-to-int v2, v2

    div-int v6, v2, p8

    .line 3693
    .local v6, "deltaTime":I
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailListListener:Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    .line 3694
    move-wide/from16 v0, p4

    long-to-int v8, v0

    move-wide/from16 v0, p6

    long-to-int v9, v0

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNAccurateCodecConfig()I

    move-result v11

    iget-object v12, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailListListener:Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    move-object v2, p0

    move-object v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v7, p8

    invoke-direct/range {v2 .. v12}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsList(Ljava/lang/String;IIIIII[IILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)I

    .line 3697
    return-void
.end method

.method getPixelsList(Ljava/lang/String;IIJJI)[Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "startMs"    # J
    .param p6, "endMs"    # J
    .param p8, "thumbnailCount"    # I

    .prologue
    .line 3666
    move/from16 v0, p8

    new-array v10, v0, [I

    .line 3667
    .local v10, "tnTimeList":[I
    sub-long v2, p6, p4

    long-to-int v2, v2

    div-int v6, v2, p8

    .line 3668
    .local v6, "deltaTime":I
    const/4 v12, 0x0

    .line 3669
    .local v12, "bitmaps":[Landroid/graphics/Bitmap;
    move-wide/from16 v0, p4

    long-to-int v8, v0

    move-wide/from16 v0, p6

    long-to-int v9, v0

    invoke-virtual {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNAccurateCodecConfig()I

    move-result v11

    move-object v2, p0

    move-object v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v7, p8

    invoke-direct/range {v2 .. v11}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsList(Ljava/lang/String;IIIIII[II)[Landroid/graphics/Bitmap;

    move-result-object v12

    .line 3672
    iput-object v10, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailTime:[I

    .line 3673
    return-object v12
.end method

.method getPixelsListFast(Ljava/lang/String;IIJJILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .locals 18
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "startMs"    # J
    .param p6, "endMs"    # J
    .param p8, "thumbnailCount"    # I
    .param p9, "callback"    # Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    .prologue
    .line 3793
    const/4 v13, 0x0

    .line 3796
    .local v13, "TNFastConfigFailed":Z
    add-int/lit8 v2, p2, 0x1

    and-int/lit8 v15, v2, -0x2

    .line 3797
    .local v15, "newWidth":I
    add-int/lit8 v2, p3, 0x1

    and-int/lit8 v14, v2, -0x2

    .line 3802
    .local v14, "newHeight":I
    move/from16 v0, p8

    new-array v10, v0, [I

    .line 3803
    .local v10, "tnTimeList":[I
    sub-long v2, p6, p4

    long-to-int v2, v2

    div-int v6, v2, p8

    .line 3804
    .local v6, "deltaTime":I
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailListListener:Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    .line 3807
    move-wide/from16 v0, p4

    long-to-int v8, v0

    move-wide/from16 v0, p6

    long-to-int v9, v0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNFastCodecConfig()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailListListener:Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v7, p8

    invoke-virtual/range {v2 .. v12}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsListFastCB(Ljava/lang/String;IIIIII[IILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3818
    :goto_0
    const/4 v2, 0x1

    if-ne v13, v2, :cond_0

    .line 3821
    move-wide/from16 v0, p4

    long-to-int v8, v0

    move-wide/from16 v0, p6

    long-to-int v9, v0

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNAccurateCodecConfig()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mThumbnailListListener:Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v7, p8

    invoke-virtual/range {v2 .. v12}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsListFastCB(Ljava/lang/String;IIIIII[IILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3832
    :cond_0
    return-void

    .line 3812
    :catch_0
    move-exception v16

    .line 3813
    .local v16, "x":Ljava/lang/Exception;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "Runtime exception in getPixelsListFast call "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3814
    const/4 v13, 0x1

    goto :goto_0

    .line 3826
    .end local v16    # "x":Ljava/lang/Exception;
    :catch_1
    move-exception v16

    .line 3827
    .local v16, "x":Ljava/lang/RuntimeException;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "Runtime exception in getPixelsListFast call with fallback"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3828
    throw v16
.end method

.method getPixelsListFast(Ljava/lang/String;IIJJI)[Landroid/util/Pair;
    .locals 22
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "startMs"    # J
    .param p6, "endMs"    # J
    .param p8, "thumbnailCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIJJI)[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3718
    const/16 v18, 0x0

    .line 3719
    .local v18, "rgb888":[I
    const/16 v20, 0x0

    .line 3720
    .local v20, "thumbnailSize":I
    const/16 v19, 0x0

    .line 3721
    .local v19, "tempBitmap":Landroid/graphics/Bitmap;
    const/4 v13, 0x0

    .line 3724
    .local v13, "TNFastConfigFailed":Z
    add-int/lit8 v2, p2, 0x1

    and-int/lit8 v17, v2, -0x2

    .line 3725
    .local v17, "newWidth":I
    add-int/lit8 v2, p3, 0x1

    and-int/lit8 v16, v2, -0x2

    .line 3726
    .local v16, "newHeight":I
    mul-int v2, v17, v16

    mul-int/lit8 v20, v2, 0x4

    .line 3728
    const/4 v15, 0x0

    .line 3730
    .local v15, "i":I
    const/4 v14, 0x0

    .line 3731
    .local v14, "bitmaps":[Landroid/graphics/Bitmap;
    move/from16 v0, p8

    new-array v10, v0, [I

    .line 3732
    .local v10, "tnTimeList":[I
    move/from16 v0, p8

    new-array v12, v0, [Landroid/util/Pair;

    .line 3733
    .local v12, "PixelList":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Landroid/graphics/Bitmap;>;"
    sub-long v2, p6, p4

    long-to-int v2, v2

    div-int v6, v2, p8

    .line 3736
    .local v6, "deltaTime":I
    move-wide/from16 v0, p4

    long-to-int v8, v0

    move-wide/from16 v0, p6

    long-to-int v9, v0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNFastCodecConfig()I

    move-result v11

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v7, p8

    invoke-virtual/range {v2 .. v11}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsListFast(Ljava/lang/String;IIIIII[II)[Landroid/graphics/Bitmap;

    move-result-object v14

    .line 3739
    :goto_0
    move/from16 v0, p8

    if-ge v15, v0, :cond_0

    .line 3740
    new-instance v2, Landroid/util/Pair;

    aget v3, v10, v15

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aget-object v4, v14, v15

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v12, v15
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3739
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 3744
    :catch_0
    move-exception v21

    .line 3745
    .local v21, "x":Ljava/lang/Exception;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "Runtime exception in getPixelsListFast call "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3746
    const/4 v13, 0x1

    .line 3750
    .end local v21    # "x":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x1

    if-ne v13, v2, :cond_1

    .line 3753
    move-wide/from16 v0, p4

    long-to-int v8, v0

    move-wide/from16 v0, p6

    long-to-int v9, v0

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getTNAccurateCodecConfig()I

    move-result v11

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v7, p8

    invoke-virtual/range {v2 .. v11}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsListFast(Ljava/lang/String;IIIIII[II)[Landroid/graphics/Bitmap;

    move-result-object v14

    .line 3756
    :goto_1
    move/from16 v0, p8

    if-ge v15, v0, :cond_1

    .line 3757
    new-instance v2, Landroid/util/Pair;

    aget v3, v10, v15

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aget-object v4, v14, v15

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v12, v15
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3756
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 3761
    :catch_1
    move-exception v21

    .line 3762
    .local v21, "x":Ljava/lang/RuntimeException;
    const-string v2, "MediaArtistNativeHelper"

    const-string v3, "Runtime exception in getPixelsListFast call with fallback"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3763
    throw v21

    .line 3767
    .end local v21    # "x":Ljava/lang/RuntimeException;
    :cond_1
    return-object v12
.end method

.method getProjectAudioTrackPCMFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2038
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    return-object v0
.end method

.method getProjectPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    return-object v0
.end method

.method public getTNAccurateCodecConfig()I
    .locals 1

    .prologue
    .line 3947
    sget v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    return v0
.end method

.method public getTNFastCodecConfig()I
    .locals 1

    .prologue
    .line 3955
    sget v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    return v0
.end method

.method public getVideoCodecType(I)I
    .locals 1
    .param p1, "codecType"    # I

    .prologue
    .line 3247
    const/4 v0, -0x1

    .line 3248
    .local v0, "retValue":I
    packed-switch p1, :pswitch_data_0

    .line 3270
    :pswitch_0
    const/4 v0, -0x1

    .line 3272
    :goto_0
    return v0

    .line 3250
    :pswitch_1
    const/4 v0, 0x1

    .line 3251
    goto :goto_0

    .line 3253
    :pswitch_2
    const/4 v0, 0x4

    .line 3254
    goto :goto_0

    .line 3256
    :pswitch_3
    const/4 v0, 0x5

    .line 3257
    goto :goto_0

    .line 3259
    :pswitch_4
    const/4 v0, 0x6

    .line 3260
    goto :goto_0

    .line 3262
    :pswitch_5
    const/4 v0, 0x2

    .line 3263
    goto :goto_0

    .line 3265
    :pswitch_6
    const/4 v0, 0x0

    .line 3266
    goto :goto_0

    .line 3248
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method initClipSettings(Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;)V
    .locals 2
    .param p1, "lclipSettings"    # Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 3132
    iput-object v1, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    .line 3133
    iput-object v1, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipDecodedPath:Ljava/lang/String;

    .line 3134
    iput-object v1, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipOriginalPath:Ljava/lang/String;

    .line 3135
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    .line 3136
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    .line 3137
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    .line 3138
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutPercent:I

    .line 3139
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutPercent:I

    .line 3140
    iput-boolean v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomEnabled:Z

    .line 3141
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentStart:I

    .line 3142
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXStart:I

    .line 3143
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYStart:I

    .line 3144
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentEnd:I

    .line 3145
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXEnd:I

    .line 3146
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYEnd:I

    .line 3147
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    .line 3148
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    .line 3149
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    .line 3150
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipVolumePercentage:I

    .line 3151
    iput v0, p1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$ClipSettings;->mNumCuts:I

    .line 3152
    return-void
.end method

.method invalidatePcmFile()V
    .locals 2

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2046
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2047
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    .line 2049
    :cond_0
    return-void
.end method

.method public native nativeFindThumbnailMode(Ljava/lang/String;IJJ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method public native nativeGetPixelsFast(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;
.end method

.method public native nativeGetPixelsListFast(Ljava/lang/String;IIIIII[II)[Landroid/graphics/Bitmap;
.end method

.method public native nativeGetPixelsListFastCB(Ljava/lang/String;IIIIII[IILcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)I
.end method

.method nativeHelperGetAspectRatio()I
    .locals 1

    .prologue
    .line 3033
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v0}, Lcom/lifevibes/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v0

    return v0
.end method

.method public releaseNativeHelper()V
    .locals 3

    .prologue
    .line 3020
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3029
    return-void

    .line 3021
    :catch_0
    move-exception v0

    .line 3022
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Illegal State exeption caught in releaseNativeHelper"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3024
    throw v0

    .line 3025
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 3026
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Runtime exeption caught in releaseNativeHelper"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3027
    throw v0
.end method

.method setAudioflag(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 3044
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "AudioPcm.pcm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3045
    const/4 p1, 0x1

    .line 3047
    :cond_0
    iput-boolean p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mRegenerateAudio:Z

    .line 3048
    return-void
.end method

.method setFit2ShareFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 3901
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    .line 3902
    return-void
.end method

.method stop(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 3874
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stopEncoding()V

    .line 3875
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3883
    return-void

    .line 3876
    :catch_0
    move-exception v0

    .line 3877
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Illegal state exception in unload settings"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3878
    throw v0

    .line 3879
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 3880
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Runtime exception in unload settings"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3881
    throw v0
.end method

.method stopAudioWaveformExtraction()V
    .locals 3

    .prologue
    .line 3887
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stopExtraction()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3895
    return-void

    .line 3888
    :catch_0
    move-exception v0

    .line 3889
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Illegal state exception in unload settings"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3890
    throw v0

    .line 3891
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 3892
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Runtime exception in unload settings"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3893
    throw v0
.end method

.method stopThumbnailList()V
    .locals 3

    .prologue
    .line 3910
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stopThumbnail()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3915
    return-void

    .line 3911
    :catch_0
    move-exception v0

    .line 3912
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "MediaArtistNativeHelper"

    const-string v2, "Illegal state exception in stopThumbnail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3913
    throw v0
.end method
