.class public Lcom/lifevibes/videoeditor/MediaAudioItem;
.super Lcom/lifevibes/videoeditor/BaseMediaItem;
.source "MediaAudioItem.java"


# instance fields
.field private final mAudioBitrate:I

.field private final mAudioChannels:I

.field private final mAudioSamplingFrequency:I

.field private final mAudioType:I

.field private mAudioWaveformFilename:Ljava/lang/String;

.field private mBeginBoundaryTimeMs:J

.field private final mDurationMs:J

.field private mEndBoundaryTimeMs:J

.field private final mFileType:I

.field private mMuted:Z

.field private mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditorImpl;

.field private mVolumePercentage:I

.field private mWaveformData:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/lifevibes/videoeditor/WaveformData;",
            ">;"
        }
    .end annotation
.end field

.field private originalVolume:I


# direct methods
.method private constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0, v0, v0, v0}, Lcom/lifevibes/videoeditor/MediaAudioItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "editor"    # Lcom/lifevibes/videoeditor/VideoEditor;
    .param p2, "mediaItemId"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    const-wide/16 v4, 0x0

    const-wide/16 v6, -0x1

    const/16 v8, 0x64

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v10}, Lcom/lifevibes/videoeditor/MediaAudioItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJIZLjava/lang/String;)V

    .line 105
    return-void
.end method

.method constructor <init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJIZLjava/lang/String;)V
    .locals 8
    .param p1, "editor"    # Lcom/lifevibes/videoeditor/VideoEditor;
    .param p2, "mediaItemId"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "beginMs"    # J
    .param p6, "endMs"    # J
    .param p8, "volumePercent"    # I
    .param p9, "muted"    # Z
    .param p10, "audioWaveformFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2, p3}, Lcom/lifevibes/videoeditor/BaseMediaItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    instance-of v4, p1, Lcom/lifevibes/videoeditor/VideoEditorImpl;

    if-eqz v4, :cond_0

    .line 130
    check-cast p1, Lcom/lifevibes/videoeditor/VideoEditorImpl;

    .end local p1    # "editor":Lcom/lifevibes/videoeditor/VideoEditor;
    iput-object p1, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditorImpl;

    .line 135
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v4, p3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 140
    .local v3, "properties":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 150
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Input File Type"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 136
    .end local v3    # "properties":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    :catch_0
    move-exception v2

    .line 137
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 153
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "properties":Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;
    :sswitch_0
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getVideoCodecType(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 163
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Video in input file for MediaAudioItem is not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 166
    :pswitch_0
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    if-eqz v4, :cond_1

    .line 167
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 177
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unsupported Audio Codec Format in Input File"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 181
    :cond_1
    :pswitch_1
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mFileType:I

    .line 182
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    .line 183
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioBitrate:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioBitrate:I

    .line 184
    iget-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget v5, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    invoke-virtual {v4, v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v4

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    .line 185
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioChannels:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioChannels:I

    .line 186
    iget v4, v3, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper$Properties;->audioSamplingFrequency:I

    iput v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    .line 187
    iput-wide p4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mBeginBoundaryTimeMs:J

    .line 188
    const-wide/16 v4, -0x1

    cmp-long v4, p6, v4

    if-nez v4, :cond_2

    iget-wide p6, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    .end local p6    # "endMs":J
    :cond_2
    iput-wide p6, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mEndBoundaryTimeMs:J

    .line 189
    move/from16 v0, p8

    iput v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mVolumePercentage:I

    move/from16 v0, p8

    iput v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->originalVolume:I

    .line 190
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMuted:Z

    .line 191
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 192
    if-eqz p10, :cond_3

    .line 193
    new-instance v4, Ljava/lang/ref/SoftReference;

    new-instance v5, Lcom/lifevibes/videoeditor/WaveformData;

    move-object/from16 v0, p10

    invoke-direct {v5, v0}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_3
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    goto :goto_0

    .line 140
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
    .end sparse-switch

    .line 153
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 167
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public cancelAudioWaveformExtraction()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stopAudioWaveformExtraction()V

    .line 258
    :cond_0
    return-void
.end method

.method public extractAudioWaveform(Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;)V
    .locals 19
    .param p1, "listener"    # Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 410
    const/4 v7, 0x0

    .line 411
    .local v7, "frameDuration":I
    const/4 v9, 0x0

    .line 412
    .local v9, "sampleCount":I
    const/16 v18, 0x0

    .line 413
    .local v18, "result":I
    const/4 v6, 0x0

    .line 415
    .local v6, "audioWaveFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    move-result-object v2

    .line 420
    .local v2, "projectPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 421
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "audioWaveformFile-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 431
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 433
    const v3, 0x27100

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 435
    const/16 v9, 0xa0

    .line 448
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v4, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    int-to-long v10, v3

    mul-long/2addr v4, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioChannels:I

    int-to-long v10, v3

    mul-long/2addr v4, v10

    long-to-double v0, v4

    move-wide/from16 v16, v0

    .line 449
    .local v16, "pcmFileSize":D
    const-wide v14, 0x41efffffffe00000L    # 4.294967295E9

    .line 451
    .local v14, "MAX_PCM_FILESIZE":D
    const-wide v4, 0x41efffffffe00000L    # 4.294967295E9

    cmpl-double v3, v16, v4

    if-lez v3, :cond_4

    .line 452
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "file too longfor PCM generation"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 424
    .end local v14    # "MAX_PCM_FILESIZE":D
    .end local v16    # "pcmFileSize":D
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto :goto_0

    .line 436
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 438
    const v3, 0x4e200

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 440
    const/16 v9, 0x140

    goto :goto_1

    .line 441
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 443
    const v3, 0xfa000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 445
    const/16 v9, 0x400

    goto :goto_1

    .line 455
    .restart local v14    # "MAX_PCM_FILESIZE":D
    .restart local v16    # "pcmFileSize":D
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mFilename:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    move-object/from16 v10, p1

    invoke-virtual/range {v3 .. v13}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;ZJ)I

    move-result v18

    .line 466
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 468
    const/4 v3, -0x1

    move/from16 v0, v18

    if-eq v0, v3, :cond_5

    .line 469
    new-instance v3, Ljava/lang/ref/SoftReference;

    new-instance v4, Lcom/lifevibes/videoeditor/WaveformData;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    .line 476
    :goto_2
    return-void

    .line 472
    :cond_5
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 473
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 474
    :cond_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto :goto_2
.end method

.method public extractAudioWaveform(Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;J)V
    .locals 24
    .param p1, "listener"    # Lcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;
    .param p2, "numSampNeeded"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    const/4 v7, 0x0

    .line 283
    .local v7, "frameDuration":I
    const/4 v9, 0x0

    .line 284
    .local v9, "sampleCount":I
    const/16 v21, 0x0

    .line 285
    .local v21, "result":I
    const/4 v2, 0x0

    .line 286
    .local v2, "inDuration":I
    const/16 v22, 0x0

    .line 287
    .local v22, "roundedDuration":I
    const-wide/16 v16, 0x0

    .line 288
    .local v16, "dur":D
    const/4 v6, 0x0

    .line 289
    .local v6, "audioWaveFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    move-result-object v20

    .line 291
    .local v20, "projectPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    if-eqz v3, :cond_0

    .line 292
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lifevibes/videoeditor/WaveformData;

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/WaveformData;->getNumSampNeeded()J

    move-result-wide v4

    cmp-long v3, v4, p2

    if-nez v3, :cond_0

    .line 395
    :goto_0
    return-void

    .line 301
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 302
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "audioWaveformFile-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 312
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 314
    const v3, 0x27100

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 316
    const/16 v9, 0xa0

    .line 329
    :cond_1
    :goto_2
    const-wide/16 v4, 0x0

    cmp-long v3, p2, v4

    if-lez v3, :cond_3

    .line 334
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 336
    const-wide v4, 0x4103880000000000L    # 160000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    .line 353
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    div-long v4, v4, p2

    long-to-int v2, v4

    .line 354
    move-wide/from16 v0, v16

    double-to-int v3, v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v2

    move-wide/from16 v0, v16

    double-to-int v4, v0

    div-int v22, v3, v4

    .line 358
    move/from16 v0, v22

    int-to-double v4, v0

    mul-double v16, v16, v4

    .line 359
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double v4, v4, v16

    double-to-int v7, v4

    .line 362
    :cond_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v4, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    int-to-long v10, v3

    mul-long/2addr v4, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioChannels:I

    int-to-long v10, v3

    mul-long/2addr v4, v10

    long-to-double v0, v4

    move-wide/from16 v18, v0

    .line 363
    .local v18, "pcmFileSize":D
    const-wide v14, 0x41efffffffe00000L    # 4.294967295E9

    .line 365
    .local v14, "MAX_PCM_FILESIZE":D
    const-wide v4, 0x41efffffffe00000L    # 4.294967295E9

    cmpl-double v3, v18, v4

    if-lez v3, :cond_9

    .line 366
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "file too longfor PCM generation"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 305
    .end local v14    # "MAX_PCM_FILESIZE":D
    .end local v18    # "pcmFileSize":D
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto/16 :goto_1

    .line 317
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_6

    .line 319
    const v3, 0x4e200

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 321
    const/16 v9, 0x140

    goto/16 :goto_2

    .line 322
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 324
    const v3, 0xfa000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    div-int v7, v3, v4

    .line 326
    const/16 v9, 0x400

    goto/16 :goto_2

    .line 339
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_8

    .line 341
    const-wide v4, 0x4113880000000000L    # 320000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    goto/16 :goto_3

    .line 344
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    invoke-virtual {v3, v4}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 346
    const-wide v4, 0x412f400000000000L    # 1024000.0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    int-to-double v10, v3

    div-double v16, v4, v10

    goto/16 :goto_3

    .line 369
    .restart local v14    # "MAX_PCM_FILESIZE":D
    .restart local v18    # "pcmFileSize":D
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mFilename:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    move-object/from16 v10, p1

    invoke-virtual/range {v3 .. v13}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/lifevibes/videoeditor/ExtractAudioWaveformProgressListener;ZJ)I

    move-result v21

    .line 380
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 382
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_a

    .line 383
    new-instance v3, Ljava/lang/ref/SoftReference;

    new-instance v4, Lcom/lifevibes/videoeditor/WaveformData;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    .line 388
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lifevibes/videoeditor/WaveformData;

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Lcom/lifevibes/videoeditor/WaveformData;->setNumSampNeeded(J)V

    goto/16 :goto_0

    .line 390
    :cond_a
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 391
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 392
    :cond_b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public getAudioBitrate()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioBitrate:I

    return v0
.end method

.method public getAudioChannels()I
    .locals 1

    .prologue
    .line 569
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioChannels:I

    return v0
.end method

.method public getAudioSamplingFrequency()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioSamplingFrequency:I

    return v0
.end method

.method public getAudioType()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioType:I

    return v0
.end method

.method getAudioWaveformFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getBoundaryBeginTime()J
    .locals 2

    .prologue
    .line 244
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mBeginBoundaryTimeMs:J

    return-wide v0
.end method

.method public getBoundaryEndTime()J
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mEndBoundaryTimeMs:J

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 540
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    return-wide v0
.end method

.method public getFileType()I
    .locals 1

    .prologue
    .line 533
    iget v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mFileType:I

    return v0
.end method

.method public getTimelineDuration()J
    .locals 4

    .prologue
    .line 547
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mEndBoundaryTimeMs:J

    iget-wide v2, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mBeginBoundaryTimeMs:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getWaveformData()Lcom/lifevibes/videoeditor/WaveformData;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 507
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_1

    move-object v1, v2

    .line 523
    :cond_0
    :goto_0
    return-object v1

    .line 511
    :cond_1
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lifevibes/videoeditor/WaveformData;

    .line 512
    .local v1, "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    if-nez v1, :cond_0

    .line 514
    iget-object v3, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 516
    :try_start_0
    new-instance v1, Lcom/lifevibes/videoeditor/WaveformData;

    .end local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    iget-object v2, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/lifevibes/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 520
    .restart local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    goto :goto_0

    .line 517
    .end local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Ljava/io/IOException;
    throw v0

    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "waveformData":Lcom/lifevibes/videoeditor/WaveformData;
    :cond_2
    move-object v1, v2

    .line 523
    goto :goto_0
.end method

.method invalidate()V
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 498
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mAudioWaveformFilename:Ljava/lang/String;

    .line 501
    :cond_0
    return-void
.end method

.method public setExtractBoundaries(JJ)V
    .locals 7
    .param p1, "beginMs"    # J
    .param p3, "endMs"    # J

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, -0x1

    .line 213
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Invalid start time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mDurationMs:J

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    .line 218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Invalid end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_1
    cmp-long v0, p3, v2

    if-eqz v0, :cond_2

    cmp-long v0, p1, p3

    if-ltz v0, :cond_2

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Start time is greater than end time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_2
    cmp-long v0, p1, v4

    if-ltz v0, :cond_3

    cmp-long v0, p3, v2

    if-eqz v0, :cond_4

    cmp-long v0, p3, v4

    if-gez v0, :cond_4

    .line 226
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setExtractBoundaries: Start time or end time is negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_4
    iput-wide p1, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mBeginBoundaryTimeMs:J

    .line 230
    iput-wide p3, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mEndBoundaryTimeMs:J

    .line 231
    iget-object v0, p0, Lcom/lifevibes/videoeditor/MediaAudioItem;->mVideoEditor:Lcom/lifevibes/videoeditor/VideoEditorImpl;

    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->updateTimelineDuration()V

    .line 238
    return-void
.end method
