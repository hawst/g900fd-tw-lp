.class Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;
.super Ljava/lang/Thread;
.source "FileShareOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileSharedThread"
.end annotation


# instance fields
.field private mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

.field private mFragment:Landroid/app/Fragment;

.field private mHandler:Landroid/os/Handler;

.field private mShareArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;Ljava/util/ArrayList;Landroid/os/Handler;Landroid/app/Fragment;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 1
    .param p3, "progressHandler"    # Landroid/os/Handler;
    .param p4, "fragment"    # Landroid/app/Fragment;
    .param p5, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/app/Fragment;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 211
    .local p2, "checkedIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    .line 212
    invoke-virtual {p2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mShareArrayList:Ljava/util/ArrayList;

    .line 213
    iput-object p3, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mHandler:Landroid/os/Handler;

    .line 214
    iput-object p4, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    .line 215
    iput-object p5, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    .line 216
    return-void
.end method

.method private ShareFiles()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 229
    const-string v6, "FileShareOperation"

    const-string v7, "SharedFiles : Start"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mShareArrayList:Ljava/util/ArrayList;

    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->getSelectedFile(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "file":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 233
    .local v5, "sttfilename":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mShareArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v3, v6, [J

    .line 235
    .local v3, "ids":[J
    array-length v6, v3

    if-le v6, v8, :cond_2

    .line 236
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mShareArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 238
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .line 239
    .local v1, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 240
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 243
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->multipleSend([J)V

    .line 266
    .end local v1    # "i":I
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_1
    :goto_1
    return-void

    .line 244
    :cond_2
    array-length v6, v3

    if-ne v6, v8, :cond_1

    .line 245
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mShareArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v10

    .line 246
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mShareMode:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$000(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 261
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    aget-wide v8, v3, v10

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSend(Landroid/content/Context;J)V

    goto :goto_1

    .line 248
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    aget-wide v8, v3, v10

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 249
    if-eqz v5, :cond_1

    .line 250
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSendText_File(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 253
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    aget-wide v8, v3, v10

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 254
    if-eqz v5, :cond_3

    .line 255
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    aget-wide v8, v3, v10

    invoke-static {v6, v8, v9, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSendVT(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_1

    .line 257
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    aget-wide v8, v3, v10

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSend(Landroid/content/Context;J)V

    goto :goto_1

    .line 246
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSelectedFile(Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .local v1, "file_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 272
    .local v3, "listsize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 273
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v5}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 274
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 277
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Ljava/lang/String;

    .line 279
    .local v0, "file":[Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 280
    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->ShareFiles()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mShareArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    invoke-static {v0, v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 225
    const-string v0, "FileShareOperation"

    const-string v1, "SharedFiles : Finish"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    return-void
.end method
