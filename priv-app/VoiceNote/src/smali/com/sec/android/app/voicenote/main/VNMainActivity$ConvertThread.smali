.class public Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;
.super Ljava/lang/Thread;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConvertThread"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 4503
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 4504
    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;->mContext:Landroid/content/Context;

    .line 4505
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 4510
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 4512
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 4513
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;->mContext:Landroid/content/Context;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 4515
    .local v1, "srcFilePath":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;-><init>()V

    .line 4516
    .local v0, "decoder":Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;
    new-instance v2, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread$1;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->setOnDecodeFinishedListener(Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;)V

    .line 4537
    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->startDecode(Ljava/lang/String;)V

    .line 4538
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 4539
    return-void
.end method
