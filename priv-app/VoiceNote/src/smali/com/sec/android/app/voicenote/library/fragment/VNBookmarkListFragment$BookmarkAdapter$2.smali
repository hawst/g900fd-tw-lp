.class Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;
.super Ljava/lang/Object;
.source "VNBookmarkListFragment.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->startCheckBoxAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;)V
    .locals 0

    .prologue
    .line 736
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 11
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 741
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 743
    .local v6, "value":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    monitor-enter v8

    .line 744
    :try_start_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 745
    .local v5, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_5

    .line 746
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 747
    .local v0, "checkbox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_4

    .line 748
    const/4 v3, 0x0

    .line 749
    .local v3, "marginLeft":I
    const/4 v4, 0x0

    .line 751
    .local v4, "marginRight":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsSelectionMode:Z
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 752
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v0, v7, v9}, Landroid/widget/CheckBox;->measure(II)V

    .line 755
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v9

    sub-int v4, v7, v9

    .line 761
    :goto_1
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 763
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    if-nez v7, :cond_2

    .line 764
    :cond_0
    monitor-exit v8

    .line 775
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "marginLeft":I
    .end local v4    # "marginRight":I
    :goto_2
    return-void

    .line 757
    .restart local v0    # "checkbox":Landroid/widget/CheckBox;
    .restart local v3    # "marginLeft":I
    .restart local v4    # "marginRight":I
    :cond_1
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    mul-int/lit8 v4, v7, -0x1

    .line 758
    const-string v7, "VNBookmarkListFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onAnimationUpdate() : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 774
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    .end local v1    # "i":I
    .end local v3    # "marginLeft":I
    .end local v4    # "marginRight":I
    .end local v5    # "size":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 766
    .restart local v0    # "checkbox":Landroid/widget/CheckBox;
    .restart local v1    # "i":I
    .restart local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v3    # "marginLeft":I
    .restart local v4    # "marginRight":I
    .restart local v5    # "size":I
    :cond_2
    if-nez v4, :cond_3

    .line 767
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v9, 0x41800000    # 16.0f

    const/4 v10, 0x0

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v3

    .line 768
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;->this$1:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v9, 0x40800000    # 4.0f

    const/4 v10, 0x0

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v4

    .line 770
    :cond_3
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v3, v7, v4, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 771
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 745
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "marginLeft":I
    .end local v4    # "marginRight":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 774
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    :cond_5
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
