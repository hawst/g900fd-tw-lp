.class Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;
.super Ljava/lang/Object;
.source "VNFileNameDialogFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

.field final synthetic val$dialog:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;->val$dialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;->val$dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;->val$dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->refreshPositiveButton()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :cond_0
    monitor-exit p0

    return-void

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 236
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 240
    return-void
.end method
