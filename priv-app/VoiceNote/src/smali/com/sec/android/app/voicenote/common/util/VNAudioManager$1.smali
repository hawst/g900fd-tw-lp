.class Lcom/sec/android/app/voicenote/common/util/VNAudioManager$1;
.super Landroid/os/Handler;
.source "VNAudioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNAudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNAudioManager;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 110
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_1

    .line 111
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->access$000()Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 112
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->access$000()Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->access$000()Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 116
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->access$000()Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    goto :goto_0
.end method
