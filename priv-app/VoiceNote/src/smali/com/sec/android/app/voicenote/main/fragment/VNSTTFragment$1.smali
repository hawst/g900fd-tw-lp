.class Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;
.super Ljava/lang/Object;
.source "VNSTTFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 141
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    .line 142
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "view":Landroid/view/View;
    # invokes: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    invoke-static {v4, p1, p2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$100(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Landroid/widget/TextView;Landroid/view/MotionEvent;)I

    move-result v4

    # setter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mIsTouching:I
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$002(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;I)I

    .line 172
    :cond_0
    :goto_0
    return v6

    .line 143
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "view":Landroid/view/View;
    # invokes: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    invoke-static {v3, p1, p2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$100(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Landroid/widget/TextView;Landroid/view/MotionEvent;)I

    move-result v2

    .line 145
    .local v2, "offset":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mIsTouching:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$000(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v3

    sub-int/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-gt v3, v7, :cond_0

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getSelectStringIndex(Ljava/util/ArrayList;I)I
    invoke-static {v4, v5, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$400(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Ljava/util/ArrayList;I)I

    move-result v4

    # setter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$202(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;I)I

    .line 150
    const/4 v3, -0x1

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 151
    const-string v3, "VNSTTFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "can not find word at offset : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    new-instance v4, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-direct {v4}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;-><init>()V

    # setter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$502(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v3, v3, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setNBestStrings([Ljava/lang/String;)V

    .line 158
    new-array v1, v7, [I

    .line 159
    .local v1, "loc":[I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f0e0017

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 161
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 162
    invoke-virtual {v0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 164
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v3

    aget v4, v1, v6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setYLocation(I)V

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;)V

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG_SELECT_BEST"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V

    goto/16 :goto_0
.end method
