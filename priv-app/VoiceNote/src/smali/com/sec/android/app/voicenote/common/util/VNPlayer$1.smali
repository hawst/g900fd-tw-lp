.class Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;
.super Landroid/os/Handler;
.source "VNPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCurrentVolume:F

.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNPlayer;)V
    .locals 1

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 111
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/16 v4, 0x1f

    .line 114
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 130
    :goto_0
    return-void

    .line 116
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    const v1, 0x3c23d70a    # 0.01f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    .line 117
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->access$000(Lcom/sec/android/app/voicenote/common/util/VNPlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->access$000(Lcom/sec/android/app/voicenote/common/util/VNPlayer;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 123
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->volumeCtrl(F)V

    goto :goto_0

    .line 121
    :cond_0
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    goto :goto_1

    .line 126
    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->mCurrentVolume:F

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->access$000(Lcom/sec/android/app/voicenote/common/util/VNPlayer;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1f
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
