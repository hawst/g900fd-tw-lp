.class public Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNFileNameDialogFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final ADD_REQUEST:I = 0x0

.field private static final INVALID_CHARS:[Ljava/lang/String;

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field public static final MAX_LENGTH:I = 0x32

.field public static final RENAME_FILENAME:I = 0x4

.field public static RENAME_MAX_LENGTH:I = 0x0

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final TAG:Ljava/lang/String; = "VNFileNameDialogFragment"

.field public static mAddCategoryChecked:Z


# instance fields
.field private bShowKeyboard:Z

.field private categorychecked:I

.field private editText:Landroid/widget/EditText;

.field private mActivity:Landroid/app/Activity;

.field private mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

.field private mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

.field private mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

.field private mColor:I

.field private mCursor:Landroid/database/Cursor;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mEditTextChanged:Z

.field private mId:I

.field private mPosition:I

.field private mReceiveTime:J

.field private mRenameMode:I

.field private mTitle:Ljava/lang/String;

.field private medit:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    const/16 v0, 0x32

    sput v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->RENAME_MAX_LENGTH:I

    .line 75
    sput-boolean v2, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mAddCategoryChecked:Z

    .line 79
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\\"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 107
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 76
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mReceiveTime:J

    .line 77
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mEditTextChanged:Z

    .line 83
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 86
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    .line 88
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    .line 89
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    .line 90
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->bShowKeyboard:Z

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    .line 98
    iput v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
    .param p1, "x1"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mReceiveTime:J

    return-wide p1
.end method

.method static synthetic access$1202(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->bShowKeyboard:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->refreshPositiveButton()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mId:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mColor:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mPosition:I

    return v0
.end method

.method static synthetic access$900()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;

    return-object v0
.end method

.method public static getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "isIncludeCount"    # Z
    .param p2, "fixedMaxLength"    # I

    .prologue
    .line 293
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 294
    .local v0, "filterArray":[Landroid/text/InputFilter;
    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$7;-><init>(Landroid/app/Activity;)V

    aput-object v3, v0, v2

    .line 370
    const/4 v1, 0x0

    .line 371
    .local v1, "max_length":I
    const/16 v1, 0x32

    .line 372
    sput v1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->RENAME_MAX_LENGTH:I

    .line 373
    if-eqz p1, :cond_0

    .line 374
    sget v2, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->RENAME_MAX_LENGTH:I

    if-ge v2, p2, :cond_1

    .line 375
    move v1, p2

    .line 380
    :cond_0
    :goto_0
    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$8;

    invoke-direct {v3, v1, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$8;-><init>(ILandroid/app/Activity;)V

    aput-object v3, v0, v2

    .line 393
    return-object v0

    .line 377
    :cond_1
    sget v1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->RENAME_MAX_LENGTH:I

    goto :goto_0
.end method

.method public static getPattern()Ljava/util/regex/Pattern;
    .locals 8

    .prologue
    .line 411
    const/4 v0, 0x0

    .line 412
    .local v0, "EMOJI_PATTERN":Ljava/util/regex/Pattern;
    const/4 v3, 0x0

    .line 413
    .local v3, "patternMade":Z
    if-nez v3, :cond_1

    .line 414
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 416
    .local v1, "emojiPattern":Ljava/lang/StringBuilder;
    const/16 v5, 0x28

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 418
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 419
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 420
    .local v4, "str":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    const/16 v5, 0x7c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 423
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    const-string v7, ")"

    invoke-virtual {v1, v5, v6, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 426
    const/4 v3, 0x1

    .line 428
    .end local v1    # "emojiPattern":Ljava/lang/StringBuilder;
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    return-object v0
.end method

.method private getPositionById(I)I
    .locals 13
    .param p1, "id"    # I

    .prologue
    .line 741
    const/4 v2, 0x0

    .line 743
    .local v2, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x3

    new-array v9, v10, [I

    .line 744
    .local v9, "to":[I
    const/4 v10, 0x0

    const v11, 0x7f0e0059

    aput v11, v9, v10

    .line 745
    const/4 v10, 0x1

    const v11, 0x7f0e005a

    aput v11, v9, v10

    .line 748
    const/4 v7, 0x0

    .line 749
    .local v7, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 750
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 753
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v7, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 768
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 769
    const-string v10, "VNFileNameDialogFragment"

    const-string v11, "listBinding() : cursor null"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const/4 v6, -0x1

    .line 790
    :cond_1
    :goto_1
    return v6

    .line 754
    :catch_0
    move-exception v3

    .line 755
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    const-string v10, "VNFileNameDialogFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "listBinding - SQLiteException :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_0

    .line 757
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 758
    const/4 v2, 0x0

    goto :goto_0

    .line 760
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v4

    .line 761
    .local v4, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v10, "VNFileNameDialogFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_0

    .line 763
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 764
    const/4 v2, 0x0

    goto :goto_0

    .line 773
    .end local v4    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_2
    const/4 v6, -0x1

    .line 774
    .local v6, "result":I
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 775
    .local v8, "size":I
    if-lez v8, :cond_3

    .line 776
    const/4 v0, -0x1

    .line 777
    .local v0, "_id":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v8, :cond_3

    .line 778
    invoke-interface {v2, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 779
    const-string v10, "_id"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 780
    if-ne v0, p1, :cond_4

    .line 781
    move v6, v5

    .line 786
    .end local v0    # "_id":I
    .end local v5    # "i":I
    :cond_3
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_1

    .line 787
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 788
    const/4 v2, 0x0

    goto :goto_1

    .line 777
    .restart local v0    # "_id":I
    .restart local v5    # "i":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public static hasEmojiString(Ljava/lang/CharSequence;)Z
    .locals 4
    .param p0, "chars"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 398
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v2

    .line 399
    .local v2, "ptn":Ljava/util/regex/Pattern;
    if-eqz v2, :cond_0

    .line 400
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 401
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 406
    .end local v1    # "match":Ljava/util/regex/Matcher;
    .end local v2    # "ptn":Ljava/util/regex/Pattern;
    :cond_0
    :goto_0
    return v3

    .line 405
    :catch_0
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method private listBinding()Z
    .locals 13

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x0

    .line 539
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 540
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 541
    iput-object v12, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 543
    :cond_0
    new-array v4, v11, [Ljava/lang/String;

    const-string v2, "COLOR"

    aput-object v2, v4, v1

    const-string v2, "TITLE"

    aput-object v2, v4, v10

    const/4 v2, 0x2

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 547
    .local v4, "cols":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 549
    .local v5, "to":[I
    new-array v5, v11, [I

    .line 550
    const v2, 0x7f0e0059

    aput v2, v5, v1

    .line 551
    const v2, 0x7f0e005a

    aput v2, v5, v10

    .line 554
    const/4 v9, 0x0

    .line 555
    .local v9, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v6

    .line 556
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 559
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v9, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 574
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_2

    .line 575
    const-string v2, "VNFileNameDialogFragment"

    const-string v3, "listBinding() : cursor null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :goto_1
    return v1

    .line 560
    :catch_0
    move-exception v7

    .line 561
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "VNFileNameDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "listBinding - SQLiteException :"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 563
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 564
    iput-object v12, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 566
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 567
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "VNFileNameDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 569
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 570
    iput-object v12, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 579
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    if-nez v1, :cond_3

    .line 580
    const-string v1, "VNFileNameDialogFragment"

    const-string v2, "[HIYA]VNFileNameDialogFragment - listBinding()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move v1, v10

    .line 587
    goto :goto_1

    .line 582
    :cond_3
    new-instance v0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f03002d

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    .line 584
    .local v0, "mListAdapter":Landroid/widget/SpinnerAdapter;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 585
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->setCallback(Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;)V

    goto :goto_2
.end method

.method private refreshPositiveButton()V
    .locals 4

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    if-nez v2, :cond_1

    .line 645
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 636
    .local v0, "btn":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 637
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 638
    .local v1, "trimmedStr":Ljava/lang/String;
    const v2, 0x7f0b0105

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 639
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->categorychecked:I

    iget v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mPosition:I

    if-ne v2, v3, :cond_3

    .line 640
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 642
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public static setArguments(ILjava/lang/String;I)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
    .locals 3
    .param p0, "renameMode"    # I
    .param p1, "origin_text"    # Ljava/lang/String;
    .param p2, "category_id"    # I

    .prologue
    .line 111
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;-><init>()V

    .line 112
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 113
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "origintext"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v2, "categoryid"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string v2, "mode"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 116
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 117
    return-object v1
.end method

.method public static unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 432
    const-string v0, ""

    .line 434
    .local v0, "convertedUTF16Str":Ljava/lang/String;
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 436
    .local v9, "unicodeInteger":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x2

    const/4 v13, 0x4

    if-le v12, v13, :cond_0

    .line 437
    const/high16 v12, 0x1f0000

    and-int/2addr v12, v9

    shr-int/lit8 v11, v12, 0x10

    .line 438
    .local v11, "zzzzz":I
    add-int/lit8 v10, v11, -0x1

    .line 440
    .local v10, "yyyy":I
    const/4 v5, 0x0

    .line 441
    .local v5, "rear2byte":I
    const v6, 0xdc00

    .line 442
    .local v6, "rear2byteFixed":I
    and-int/lit16 v12, v9, 0x3ff

    or-int v5, v6, v12

    .line 443
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 445
    .local v8, "rear2byteString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 446
    .local v1, "former2byte":I
    const v2, 0xd800

    .line 447
    .local v2, "former2byteFixed":I
    const v12, 0xfc00

    and-int/2addr v12, v9

    shr-int/lit8 v12, v12, 0xa

    or-int v1, v2, v12

    .line 448
    shl-int/lit8 v12, v10, 0x6

    or-int/2addr v1, v12

    .line 449
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 451
    .local v4, "former2byteString":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 452
    .local v7, "rear2byteInt":I
    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 454
    .local v3, "former2byteInt":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v7

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 460
    .end local v1    # "former2byte":I
    .end local v2    # "former2byteFixed":I
    .end local v3    # "former2byteInt":I
    .end local v4    # "former2byteString":Ljava/lang/String;
    .end local v5    # "rear2byte":I
    .end local v6    # "rear2byteFixed":I
    .end local v7    # "rear2byteInt":I
    .end local v8    # "rear2byteString":Ljava/lang/String;
    .end local v10    # "yyyy":I
    .end local v11    # "zzzzz":I
    :goto_0
    return-object v0

    .line 457
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v9

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 703
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 705
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 733
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 712
    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 713
    const-string v2, "color"

    invoke-virtual {p3, v2, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 714
    .local v0, "color":I
    const-string v2, "title"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 715
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 716
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 717
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->listBinding()Z

    .line 718
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 720
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 721
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->setSelection(I)V

    .line 725
    .end local v0    # "color":I
    .end local v1    # "title":Ljava/lang/String;
    :cond_2
    if-nez p2, :cond_0

    .line 726
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    const-string v3, "category_label_position"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->setSelection(I)V

    goto :goto_0

    .line 709
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 122
    instance-of v0, p1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 123
    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    .line 125
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 126
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 533
    const-string v0, "VNFileNameDialogFragment"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 535
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 536
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const v8, 0x7f0e0031

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "origintext"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "prevFileName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "categoryid"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 137
    .local v1, "categoryId":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "mode"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I

    .line 139
    if-eqz p1, :cond_0

    .line 140
    const-string v6, "editText_changed"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mEditTextChanged:Z

    .line 141
    const-string v6, "id"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mId:I

    .line 144
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 145
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v6, 0x7f0b0105

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 150
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030013

    invoke-virtual {v6, v7, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 152
    .local v3, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    .line 170
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    .line 171
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v7, v9, v8}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 172
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 175
    const-string v6, "contextual_filename"

    invoke-static {v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    .line 176
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    new-instance v7, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 185
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v10}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 188
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v10}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 198
    :goto_1
    iput v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->categorychecked:I

    .line 199
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->selectAll()V

    .line 200
    const-string v6, "VNFileNameDialogFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[HIYA]categorychecked = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->categorychecked:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 203
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    .line 204
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 205
    const v6, 0x104000a

    new-instance v7, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 212
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 213
    const/high16 v6, 0x1040000

    new-instance v7, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 226
    .local v2, "dialog":Landroid/app/AlertDialog;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    new-instance v7, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;

    invoke-direct {v7, p0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$4;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 243
    new-instance v6, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    invoke-direct {v6, p0, v2, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;Landroid/app/AlertDialog;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 277
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    new-instance v7, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$6;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$6;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 287
    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->bShowKeyboard:Z

    .line 288
    return-object v2

    .line 155
    .end local v2    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "layout":Landroid/widget/LinearLayout;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030012

    invoke-virtual {v6, v7, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 157
    .restart local v3    # "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    .line 159
    const v6, 0x7f0e0032

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    .line 161
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->listBinding()Z

    .line 163
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getPositionById(I)I

    move-result v5

    .line 164
    .local v5, "selectPosition":I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 165
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCategory:Lcom/sec/android/app/voicenote/common/util/VNSpinner;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->setSelection(I)V

    .line 167
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto/16 :goto_0

    .line 190
    .end local v5    # "selectPosition":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setSelected(Z)V

    .line 191
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 192
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    const-string v7, "disableVoiceInput=true;inputType=filename;disableEmoticonInput=true"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 193
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    const v7, 0x84001

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setInputType(I)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 594
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 597
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 598
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;I)V
    .locals 8
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x0

    .line 666
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne p2, v5, :cond_3

    .line 667
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 668
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_1

    .line 699
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 671
    .restart local v0    # "context":Landroid/content/Context;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    if-eqz v5, :cond_2

    .line 672
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;->selectAddCategory()V

    .line 674
    :cond_2
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mAddCategoryChecked:Z

    .line 675
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;

    const-string v6, "input_method"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 677
    .local v3, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v3, v5, v7}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 678
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    invoke-direct {v5, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v7}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 680
    .end local v0    # "context":Landroid/content/Context;
    .end local v3    # "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 683
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "_id"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 684
    .local v2, "mIdindex":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "COLOR"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 685
    .local v1, "mColorIndex":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "TITLE"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 687
    .local v4, "mTitleindex":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 688
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 692
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mId:I

    .line 693
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mTitle:Ljava/lang/String;

    .line 694
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mColor:I

    .line 695
    iput p2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mPosition:I

    .line 697
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->refreshPositiveButton()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 513
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->registerBroadcastReceiverSip(Z)V

    .line 514
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mEditTextChanged:Z

    .line 521
    :goto_0
    return-void

    .line 519
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mEditTextChanged:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 471
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mEditTextChanged:Z

    if-nez v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "origintext"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 474
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I

    if-eq v4, v0, :cond_3

    .line 475
    const-string v0, "contextual_filename"

    invoke-static {v0, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 483
    :cond_1
    :goto_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->registerBroadcastReceiverSip(Z)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->bShowKeyboard:Z

    if-nez v0, :cond_4

    .line 486
    :cond_2
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 508
    :goto_1
    return-void

    .line 479
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0

    .line 490
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 507
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 525
    const-string v0, "editText_changed"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mEditTextChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 526
    const-string v0, "id"

    iget v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 527
    const-string v0, "edit_text"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 529
    return-void
.end method

.method public onVNItemSelected(Landroid/widget/AdapterView;I)V
    .locals 0
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 737
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->onItemSelected(Landroid/widget/AdapterView;I)V

    .line 738
    return-void
.end method

.method public registerBroadcastReceiverSip(Z)V
    .locals 6
    .param p1, "register"    # Z

    .prologue
    .line 601
    if-eqz p1, :cond_2

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 628
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$10;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    .line 615
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 616
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 620
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mReceiveTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x190

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 621
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->bShowKeyboard:Z

    .line 623
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 625
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method public resetOriginalName(Ljava/lang/String;)V
    .locals 2
    .param p1, "origin_text"    # Ljava/lang/String;

    .prologue
    .line 648
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I

    if-eq v0, v1, :cond_0

    .line 649
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "origintext"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 652
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    .line 130
    return-void
.end method
