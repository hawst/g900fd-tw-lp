.class Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;
.super Ljava/lang/Object;
.source "VNSTTFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearScreen()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$700(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$700(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    sget-object v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converting:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    # invokes: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$800(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    .line 221
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public onError(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;)V
    .locals 3
    .param p1, "error"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 205
    :cond_0
    return-void
.end method

.method public onResultSentence(Lcom/sec/android/app/voicenote/main/common/VNSTTData;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    .prologue
    .line 196
    return-void
.end method

.method public declared-synchronized onResultWord(Lcom/sec/android/app/voicenote/main/common/VNSTTData;)V
    .locals 2
    .param p1, "result"    # Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    const-string v0, "VNSTTFragment"

    const-string v1, "onResultWord"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$600(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$600(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2$1;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    :cond_0
    monitor-exit p0

    return-void

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStartOfSpeech()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method
