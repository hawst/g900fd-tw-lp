.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;
.super Landroid/content/BroadcastReceiver;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMediaButtonProcessListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 2080
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v10, 0x28

    const/4 v7, 0x4

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const/4 v6, 0x0

    .line 2084
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Media_Button_Recieved"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2086
    const-string v4, "button"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2087
    .local v2, "mediaButton":I
    const-string v4, "repeat"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 2088
    .local v3, "repeatecount":I
    const-string v4, "iskeydown"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2090
    .local v1, "isKeyDown":Z
    if-eqz v1, :cond_8

    .line 2091
    const-string v4, "VoiceNoteService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "registerMediaButtonProcessListener() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093
    packed-switch v2, :pswitch_data_0

    .line 2179
    .end local v1    # "isKeyDown":Z
    .end local v2    # "mediaButton":I
    .end local v3    # "repeatecount":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2097
    .restart local v1    # "isKeyDown":Z
    .restart local v2    # "mediaButton":I
    .restart local v3    # "repeatecount":I
    :pswitch_1
    if-nez v3, :cond_0

    .line 2098
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v4

    const/16 v5, 0x3eb

    if-ne v4, v5, :cond_1

    .line 2099
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pauseRecording()Z

    .line 2100
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 2101
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v4

    const/16 v5, 0x3ec

    if-ne v4, v5, :cond_2

    .line 2102
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumeRecording()Z

    .line 2103
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 2104
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2105
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 2106
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2107
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 2108
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2110
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 2111
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2112
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 2121
    :pswitch_2
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v4

    if-le v4, v10, :cond_5

    .line 2122
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v4, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    .line 2126
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 2127
    .local v0, "cpos":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v4

    int-to-double v4, v4

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int/lit16 v4, v4, 0x3e8

    add-int/2addr v0, v4

    .line 2128
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v4

    rem-int/lit8 v4, v4, 0xa

    if-nez v4, :cond_4

    .line 2129
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 2130
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2132
    :cond_4
    # operator++ for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2208()I

    goto/16 :goto_0

    .line 2124
    .end local v0    # "cpos":I
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v5

    div-int/lit8 v5, v5, 0xa

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    goto :goto_1

    .line 2137
    :pswitch_3
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v4

    if-le v4, v10, :cond_7

    .line 2138
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v4, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    .line 2142
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 2143
    .restart local v0    # "cpos":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v4

    int-to-double v4, v4

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int/lit16 v4, v4, 0x3e8

    sub-int/2addr v0, v4

    .line 2144
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v4

    rem-int/lit8 v4, v4, 0xa

    if-nez v4, :cond_6

    .line 2145
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 2146
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2149
    :cond_6
    # operator++ for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2208()I

    goto/16 :goto_0

    .line 2140
    .end local v0    # "cpos":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v5

    div-int/lit8 v5, v5, 0xa

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    goto :goto_2

    .line 2154
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.voicenote.VOICE_INTENT_PREV"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2158
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.voicenote.VOICE_INTENT_NEXT"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2165
    :cond_8
    packed-switch v2, :pswitch_data_1

    :pswitch_6
    goto/16 :goto_0

    .line 2167
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    goto/16 :goto_0

    .line 2172
    :pswitch_8
    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2202(I)I

    goto/16 :goto_0

    .line 2093
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    .line 2165
    :pswitch_data_1
    .packed-switch 0x56
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method
