.class public final Lcom/sec/android/app/voicenote/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar_select_mode:I = 0x7f030000

.field public static final actionmode_selectview:I = 0x7f030001

.field public static final activity_no_sd_card:I = 0x7f030002

.field public static final activity_voice_list_nfc_writing:I = 0x7f030003

.field public static final activity_voice_note_editlabel:I = 0x7f030004

.field public static final activity_voice_note_main:I = 0x7f030005

.field public static final activity_voice_note_managelabel:I = 0x7f030006

.field public static final activity_voice_note_nfc_select:I = 0x7f030007

.field public static final activity_voice_note_opensource_license:I = 0x7f030008

.field public static final activity_voice_note_policy:I = 0x7f030009

.field public static final activity_voice_note_setting:I = 0x7f03000a

.field public static final bookmark_list:I = 0x7f03000b

.field public static final custom_action_mode_view:I = 0x7f03000c

.field public static final details_list_item:I = 0x7f03000d

.field public static final dialog_common_text:I = 0x7f03000e

.field public static final dialog_rename_text:I = 0x7f03000f

.field public static final edit_label_color_select:I = 0x7f030010

.field public static final editstt_list_popup:I = 0x7f030011

.field public static final fragment_dialog_filename:I = 0x7f030012

.field public static final fragment_dialog_filename_easy:I = 0x7f030013

.field public static final fragment_dialog_selectlabel:I = 0x7f030014

.field public static final fragment_dialog_selectsort:I = 0x7f030015

.field public static final fragment_dialog_selectstt_sharevia:I = 0x7f030016

.field public static final fragment_library_editbookmark:I = 0x7f030017

.field public static final fragment_library_expandable_listview:I = 0x7f030018

.field public static final fragment_library_listview:I = 0x7f030019

.field public static final fragment_setting_logo:I = 0x7f03001a

.field public static final fragment_setting_logo_text:I = 0x7f03001b

.field public static final fragment_setting_skipinterval:I = 0x7f03001c

.field public static final fragment_stt_edit_view:I = 0x7f03001d

.field public static final fragment_stt_view:I = 0x7f03001e

.field public static final hover_track_list_full_text_popup:I = 0x7f03001f

.field public static final hover_window_layout:I = 0x7f030020

.field public static final layout_toolbar_help:I = 0x7f030021

.field public static final library_search_view:I = 0x7f030022

.field public static final list_selectlogo_item:I = 0x7f030023

.field public static final list_sortby_item:I = 0x7f030024

.field public static final listrow_label_category_item:I = 0x7f030025

.field public static final listrow_label_manage_item:I = 0x7f030026

.field public static final listrow_logotext_spinner:I = 0x7f030027

.field public static final listrow_logotext_spinner_item:I = 0x7f030028

.field public static final listrow_selectall_item:I = 0x7f030029

.field public static final listrow_spinner_actionmode_item:I = 0x7f03002a

.field public static final listrow_spinner_add_category_item:I = 0x7f03002b

.field public static final listrow_spinner_dropdown_actionmode_item:I = 0x7f03002c

.field public static final listrow_spinner_list_category_item:I = 0x7f03002d

.field public static final listrow_voiceclip_playstate_child:I = 0x7f03002e

.field public static final listrow_voiceclip_playstate_item:I = 0x7f03002f

.field public static final listrow_voiceclip_playstate_item_easy:I = 0x7f030030

.field public static final listrow_voiceclip_radiobutton_item:I = 0x7f030031

.field public static final lockscreen_widget:I = 0x7f030032

.field public static final logo_color_select:I = 0x7f030033

.field public static final logo_text:I = 0x7f030034

.field public static final main_animator_conversation_fragment:I = 0x7f030035

.field public static final main_animator_fragment_easy:I = 0x7f030036

.field public static final main_animator_interview_fragment:I = 0x7f030037

.field public static final main_animator_normal_fragment:I = 0x7f030038

.field public static final main_animator_voicememo_fragment:I = 0x7f030039

.field public static final opensource_license_list_item:I = 0x7f03003a

.field public static final operation_progress_dialog:I = 0x7f03003b

.field public static final player_control_fragment:I = 0x7f03003c

.field public static final player_control_idle_fragment:I = 0x7f03003d

.field public static final player_control_idle_fragment_easy:I = 0x7f03003e

.field public static final player_toolbar_fragment:I = 0x7f03003f

.field public static final player_toolbar_fragment_easy:I = 0x7f030040

.field public static final player_trim_fragment:I = 0x7f030041

.field public static final policy_dialog_layout:I = 0x7f030042

.field public static final popup_menu_item_layout:I = 0x7f030043

.field public static final recorder_control_idle_fragment:I = 0x7f030044

.field public static final recorder_control_idle_fragment_easy:I = 0x7f030045

.field public static final recorder_control_pause_fragment:I = 0x7f030046

.field public static final recorder_control_pause_fragment_easy:I = 0x7f030047

.field public static final recorder_control_recording_fragment:I = 0x7f030048

.field public static final recorder_control_recording_fragment_easy:I = 0x7f030049

.field public static final recorder_mode_conversation_fragment:I = 0x7f03004a

.field public static final recorder_mode_interview_fragment:I = 0x7f03004b

.field public static final recorder_mode_light_conversation_fragment:I = 0x7f03004c

.field public static final recorder_mode_light_interview_fragment:I = 0x7f03004d

.field public static final recorder_mode_light_normal_fragment:I = 0x7f03004e

.field public static final recorder_mode_logo:I = 0x7f03004f

.field public static final recorder_mode_normal_fragment:I = 0x7f030050

.field public static final recorder_panel:I = 0x7f030051

.field public static final recorder_panel_text:I = 0x7f030052

.field public static final recorder_panel_text_easy:I = 0x7f030053

.field public static final recorder_panel_text_stt:I = 0x7f030054

.field public static final remoteview_play_paused:I = 0x7f030055

.field public static final remoteview_play_playing:I = 0x7f030056

.field public static final remoteview_record_paused:I = 0x7f030057

.field public static final remoteview_record_recording:I = 0x7f030058

.field public static final remoteview_standby:I = 0x7f030059

.field public static final reslove_list_item:I = 0x7f03005a

.field public static final resolver_grid:I = 0x7f03005b

.field public static final round_more_icon:I = 0x7f03005c

.field public static final simple_list_item_1:I = 0x7f03005d

.field public static final skipinterval_list_item:I = 0x7f03005e

.field public static final warning_dialog_layout:I = 0x7f03005f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
