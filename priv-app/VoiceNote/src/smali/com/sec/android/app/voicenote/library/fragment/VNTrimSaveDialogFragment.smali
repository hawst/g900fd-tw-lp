.class public Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNTrimSaveDialogFragment.java"


# static fields
.field private static final KEY_CODE:Ljava/lang/String; = "code"

.field private static final KEY_ITEMSELECT:Ljava/lang/String; = "item"

.field private static final KEY_TAG:Ljava/lang/String; = "tag"

.field private static final TAG:Ljava/lang/String; = "VNTrimSaveDialogFragment"

.field public static final TRIM_WARNING_DIALOG_OK:I = 0x1863d

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static mTrimSaveWitch:I


# instance fields
.field private isFinished:Z

.field private mExpectedFileSize:J

.field private mtargetFragmentRequestCode:I

.field private mtargetFragmentTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mExpectedFileSize:J

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->isFinished:Z

    .line 59
    return-void
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 43
    sput p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    return p0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->doPositiveAction()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->isFinished:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    return v0
.end method

.method private doPositiveAction()V
    .locals 13

    .prologue
    .line 128
    const-string v7, "VNTrimSaveDialogFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doPositiveAction : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v2, 0x0

    .line 131
    .local v2, "frag":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 132
    .local v3, "fragmentManager":Landroid/app/FragmentManager;
    if-eqz v3, :cond_0

    .line 133
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 135
    :cond_0
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 137
    .local v6, "intent":Landroid/content/Intent;
    sget v7, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    if-nez v7, :cond_5

    .line 138
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAvailableStorage(Z)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableStorage(Z)J

    move-result-wide v8

    iget-wide v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mExpectedFileSize:J

    cmp-long v7, v8, v10

    if-gez v7, :cond_4

    .line 140
    :cond_1
    const-string v7, "VNTrimSaveDialogFragment"

    const-string v8, "doPositiveAction : checkAvailableStorage fail"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableStorage(Z)J

    move-result-wide v0

    .line 142
    .local v0, "enoughtFileSize":J
    const-wide/16 v8, 0x400

    div-long v8, v0, v8

    const-wide/16 v10, 0x400

    div-long v4, v8, v10

    .line 143
    .local v4, "freeSize":J
    const-string v7, "VNTrimSaveDialogFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "freeSize = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const v7, 0x7f0b015c

    const v8, 0x7f0b00d0

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Mb"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->newInstance(ILjava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 155
    .end local v0    # "enoughtFileSize":J
    .end local v4    # "freeSize":J
    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 156
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    const/4 v8, -0x1

    invoke-virtual {v2, v7, v8, v6}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 158
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->dismissAllowingStateLoss()V

    .line 159
    return-void

    .line 150
    :cond_4
    const-string v7, "what"

    const/16 v8, 0x3d

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 152
    :cond_5
    sget v7, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 153
    const-string v7, "what"

    const/16 v8, 0x3c

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 2
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 186
    const-string v0, "VNTrimSaveDialogFragment"

    const-string v1, "initService()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 188
    return-void
.end method

.method private static isServiceValid()Z
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 197
    const/4 v0, 0x0

    .line 200
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static releaseService()V
    .locals 2

    .prologue
    .line 191
    const-string v0, "VNTrimSaveDialogFragment"

    const-string v1, "releaseService()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 193
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 98
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v0, "tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    .line 105
    const-string v0, "code"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    .line 106
    const-string v0, "item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    .line 108
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 109
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const-string v2, "VNTrimSaveDialogFragment"

    const-string v3, "onCreateDialog : "

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0155

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0156

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 69
    .local v1, "items":[Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 70
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0157

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 71
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 79
    const v2, 0x7f0b0021

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 92
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 113
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->isFinished:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 115
    .local v0, "frag":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 116
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 119
    .end local v0    # "frag":Landroid/app/Fragment;
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 120
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 178
    const-string v0, "tag"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v0, "code"

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    const-string v0, "item"

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mTrimSaveWitch:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 182
    return-void
.end method

.method public setExpectedFileSize(Ljava/lang/String;II)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "duration"    # I
    .param p3, "trimTime"    # I

    .prologue
    .line 162
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    :cond_0
    const-string v1, "VNTrimSaveDialogFragment"

    const-string v2, "setExpectedFileSize path is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_0
    return-void

    .line 167
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 169
    int-to-float v1, p3

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-long v2, v1

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mExpectedFileSize:J

    .line 170
    const-string v1, "VNTrimSaveDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setExpectedFileSize : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mExpectedFileSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_2
    const-string v1, "VNTrimSaveDialogFragment"

    const-string v2, "setExpectedFileSize file doesn\'t exist"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTargetFragmentTag(Ljava/lang/String;I)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "code"    # I

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;

    .line 124
    iput p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I

    .line 125
    return-void
.end method
