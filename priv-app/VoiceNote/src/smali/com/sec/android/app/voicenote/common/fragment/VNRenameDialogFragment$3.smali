.class Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;
.super Ljava/lang/Object;
.source "VNRenameDialogFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

.field final synthetic val$dialog:Landroid/app/AlertDialog;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Landroid/app/AlertDialog;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->val$dialog:Landroid/app/AlertDialog;

    iput-object p3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->val$editText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->val$dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->val$dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->refreshPositiveButton()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$300(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->changeFileName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$402(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_0
    monitor-exit p0

    return-void

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 267
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 271
    return-void
.end method
