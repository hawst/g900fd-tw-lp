.class final Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$2;
.super Landroid/os/Handler;
.source "SamsungSTT.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 504
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 507
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 526
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 527
    return-void

    .line 509
    :pswitch_0
    const-string v1, "SamsungSTT"

    const-string v2, "BUFFER_READ_START"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$800()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->startRecording(J)V

    .line 514
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v2

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getFilepath(I)Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1500(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1302(Ljava/io/FileInputStream;)Ljava/io/FileInputStream;

    .line 515
    new-instance v1, Ljava/io/BufferedInputStream;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1300()Ljava/io/FileInputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1102(Ljava/io/BufferedInputStream;)Ljava/io/BufferedInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 520
    :goto_1
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v1

    if-nez v1, :cond_0

    .line 521
    const-string v1, "SamsungSTT"

    const-string v2, "BUFFER_READ_START mBIStream is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    :cond_0
    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->restartBufferReadThread()V
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1800()V

    goto :goto_0

    .line 516
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e1":Ljava/io/FileNotFoundException;
    const-string v1, "SamsungSTT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BUFFER_READ_START error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 507
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
