.class public Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "VNMultiShareActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;,
        Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNMultiShareActivity"


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mExtraStream:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private mGrid:Landroid/widget/GridView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mGrid:Landroid/widget/GridView;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mAdapter:Landroid/widget/BaseAdapter;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mExtraStream:Ljava/util/ArrayList;

    .line 187
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const-string v5, "VNMultiShareActivity"

    const-string v6, "onCreate()"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 68
    .local v1, "data":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 69
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mExtraStream:Ljava/util/ArrayList;

    .line 71
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;

    .line 72
    const-string v5, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 74
    .local v4, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    .line 75
    .local v3, "intent":Landroid/os/Parcelable;
    instance-of v5, v3, Landroid/content/Intent;

    if-eqz v5, :cond_0

    .line 76
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;

    check-cast v3, Landroid/content/Intent;

    .end local v3    # "intent":Landroid/os/Parcelable;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    :cond_1
    const-string v5, "layout_inflater"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    .line 84
    .local v0, "ap":Lcom/android/internal/app/AlertController$AlertParams;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b011f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03005b

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    .line 86
    iget-object v5, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    const v6, 0x7f0e00eb

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/GridView;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mGrid:Landroid/widget/GridView;

    .line 87
    new-instance v5, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)V

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mAdapter:Landroid/widget/BaseAdapter;

    .line 88
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mGrid:Landroid/widget/GridView;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 89
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v5, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 90
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v5, p0}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->setupAlert()V

    .line 93
    const-string v5, "VNMultiShareActivity"

    const-string v6, "onCreate() END"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 128
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.STREAM"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mExtraStream:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 129
    const-string v1, "application/txt,audio/mp4"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->startActivity(Landroid/content/Intent;)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->finish()V

    .line 132
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 195
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 198
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "package"

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 200
    .local v0, "in":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 201
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->startActivity(Landroid/content/Intent;)V

    .line 204
    .end local v0    # "in":Landroid/content/Intent;
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 98
    const-string v0, "VNMultiShareActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 101
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onResume()V

    .line 102
    const-string v0, "VNMultiShareActivity"

    const-string v1, "onResume() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 107
    const-string v0, "VNMultiShareActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    sparse-switch p1, :sswitch_data_0

    .line 121
    :sswitch_0
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onTrimMemory(I)V

    .line 122
    return-void

    .line 108
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
