.class public Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;
.super Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;
.source "BufferingPipe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AudioChunkType:",
        "Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;",
        ">",
        "Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe",
        "<TAudioChunkType;TAudioChunkType;>;"
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x8

.field private static final SEND_BUFFER_SIZE:I = 0x3

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field private final mBuffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe",
            "<TAudioChunkType;>.CustomAudioChunk;>;"
        }
    .end annotation
.end field

.field private mRecentChunk:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe",
            "<TAudioChunkType;>.CustomAudioChunk;"
        }
    .end annotation
.end field

.field private final mTempBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TAudioChunkType;>;"
        }
    .end annotation
.end field

.field private mTimeStamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "startTimeStamp"    # J

    .prologue
    .line 24
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mRecentChunk:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    .line 25
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTempBuffer:Ljava/util/ArrayList;

    .line 27
    sget-object v0, Lcom/nuance/dragon/toolkit/audio/AudioType;->UNKNOWN:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 28
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTimeStamp:J

    .line 29
    return-void
.end method


# virtual methods
.method protected chunksAvailable(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getChunksAvailableForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 48
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {p1, p2, v3}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAllAudioChunksForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;Ljava/util/List;)V

    .line 49
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    .line 50
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    new-instance v2, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iget-object v4, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioBytes:[B

    iget v5, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioDuration:I

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;-><init>(Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;Lcom/nuance/dragon/toolkit/audio/AudioType;[BI)V

    .line 51
    .local v2, "newChunk":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>.CustomAudioChunk;"
    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTimeStamp:J

    iget v3, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioDuration:I

    invoke-virtual {v2, v4, v5, v3}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->setTimeStamp(JI)V

    .line 55
    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTimeStamp:J

    iget v3, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioDuration:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTimeStamp:J

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x8

    if-le v3, v4, :cond_0

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 61
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    .end local v2    # "newChunk":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>.CustomAudioChunk;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->notifyChunksAvailable()V

    .line 64
    return-void
.end method

.method public connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    .local p1, "source":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 90
    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 91
    invoke-super {p0, p1}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 92
    return-void
.end method

.method protected framesDropped(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->notifyFramesDropped()V

    .line 97
    return-void
.end method

.method protected getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TAudioChunkType;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 35
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    move-object v1, v0

    .line 37
    check-cast v1, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mRecentChunk:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    .line 42
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;
    .locals 1

    .prologue
    .line 106
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-object v0
.end method

.method public getChunksAvailable()I
    .locals 2

    .prologue
    .line 116
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x2

    .line 117
    .local v0, "nCount":I
    if-gez v0, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 120
    :cond_0
    return v0
.end method

.method public getChunksDuration()J
    .locals 6

    .prologue
    .line 67
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    const-wide/16 v2, 0x0

    .line 68
    .local v2, "lDuration":J
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    .line 69
    .local v0, "chunk":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>.CustomAudioChunk;"
    iget v4, v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->audioDuration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 70
    goto :goto_0

    .line 72
    .end local v0    # "chunk":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>.CustomAudioChunk;"
    :cond_0
    return-wide v2
.end method

.method public getFirstChunkTimeStamp()J
    .locals 2

    .prologue
    .line 76
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 77
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->TAG:Ljava/lang/String;

    const-string v1, "= buffer size is 0 in getFirstChunkTimeStamp, use _timeStamp"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mTimeStamp:J

    .line 81
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->mTimeStamp:J

    goto :goto_0
.end method

.method public getRecentTimeStamp()J
    .locals 2

    .prologue
    .line 124
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mRecentChunk:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    if-nez v0, :cond_0

    .line 125
    const-wide/16 v0, -0x1

    .line 127
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mRecentChunk:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->mTimeStamp:J

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 111
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->isSourceActive()Z

    move-result v0

    return v0
.end method

.method protected sourceClosed(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->notifySourceClosed()V

    .line 102
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>;"
    const/4 v1, 0x3

    .line 132
    const-string v2, "========================== BufferingPipe ======================"

    .line 133
    .local v2, "strString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, v1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 134
    .local v1, "nSize":I
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n========================== BufferingPipe ======================"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    return-object v2
.end method
