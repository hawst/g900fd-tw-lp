.class public final Lcom/sec/android/app/voicenote/common/util/VNConsts$BroadcastMessage;
.super Ljava/lang/Object;
.source "VNConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BroadcastMessage"
.end annotation


# static fields
.field public static final ACTIVITY_DESTORYED:I = 0x1194

.field public static final ALARM:I = 0xfaa

.field public static final CALL_STATE_IDLE:I = 0xfdc

.field public static final CALL_STATE_OFFHOOK:I = 0xfde

.field public static final CALL_STATE_RINGING:I = 0xfdd

.field public static final COVER_ON_OFF:I = 0x1324

.field public static final EMERGENCYSTATECHANGED:I = 0x12c0

.field public static final FILE_DELETE:I = 0xfff

.field public static final FINISH:I = 0xfe6

.field public static final GPS_CHANGED:I = 0x10d6

.field public static final HEADSET_PLUGIN:I = 0x1130

.field public static final HOME_KEY_PRESSED:I = 0x1450

.field public static final KNOX_MOVE_COMPLETE:I = 0x11fb

.field public static final KNOX_MOVE_DONE:I = 0x11f8

.field public static final KNOX_MOVE_FAIL:I = 0x11fa

.field public static final KNOX_MOVE_SAME:I = 0x11f9

.field public static final KNOX_USER_SWITCHED:I = 0x11fc

.field public static final LIST_ACTIVITY_REC_ORDER:I = 0x1068

.field public static final LOCALE_CHANGED:I = 0xfb4

.field public static final LOW_BATTERY:I = 0xff0

.field public static final MEDIA_SCANNING:I = 0xfd4

.field public static final MUSIC_COMMAND:I = 0xfbe

.field public static final NEXTBUTTON_PLAYER:I = 0x125d

.field public static final NEXTLONGBUTTON_PLAYER:I = 0x125f

.field public static final NOTIFICATION:I = 0xfc8

.field public static final PALM_TOUCH_DOWN:I = 0x1004

.field public static final PALM_TOUCH_UP:I = 0x1009

.field public static final PREVBUTTON_PLAYER:I = 0x125e

.field public static final PREVLONGBUTTON_PLAYER:I = 0x1260

.field public static final REFRESH_PLAYER:I = 0x125c

.field public static final SCREEN_ON_OFF:I = 0x10cc

.field public static final SD_BAD_REMOVAL:I = 0xfd7

.field public static final SD_EJECT:I = 0xfd6

.field public static final SD_MOUNT:I = 0xfd2

.field public static final SD_SCAN_FINISH:I = 0xfd5

.field public static final SD_UNMOUNT:I = 0xfd3

.field public static final STATUSBAR_STATE:I = 0x132e

.field public static final VOLUME_CHANGED:I = 0xffa


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
