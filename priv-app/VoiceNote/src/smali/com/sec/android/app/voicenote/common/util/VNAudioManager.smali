.class public Lcom/sec/android/app/voicenote/common/util/VNAudioManager;
.super Ljava/lang/Object;
.source "VNAudioManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VNAudioManager"

.field private static mAudioManager:Landroid/media/AudioManager;


# instance fields
.field private mMute:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager$1;-><init>(Lcom/sec/android/app/voicenote/common/util/VNAudioManager;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mMute:Landroid/os/Handler;

    .line 36
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 37
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    .line 39
    :cond_0
    return-void
.end method

.method static synthetic access$000()Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method


# virtual methods
.method public abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    .locals 1
    .param p1, "listener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    return v0
.end method

.method public getAudioManager()Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method public isBluetoothA2dpOn()Z
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    return v0
.end method

.method public isRecordActive()Z
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v0

    return v0
.end method

.method public isWiredHeadsetOn()Z
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method public requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Z
    .locals 3
    .param p1, "listener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    const/4 v0, 0x1

    .line 59
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-nez v1, :cond_0

    .line 60
    const-string v0, "VNAudioManager"

    const-string v1, "requestAudioFocus is failed"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v0, 0x0

    .line 63
    :cond_0
    return v0
.end method

.method public setBeamforming(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 85
    packed-switch p1, :pswitch_data_0

    .line 93
    :goto_0
    return-void

    .line 87
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "beamforming_mode=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "beamforming_mode=1"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setNoiseReduction(Z)V
    .locals 2
    .param p1, "on"    # Z

    .prologue
    .line 77
    if-eqz p1, :cond_0

    const-string v0, "noise_reduction"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "samsungrecord_ns=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "samsungrecord_ns=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setStreamMute(Z)V
    .locals 6
    .param p1, "mute"    # Z

    .prologue
    const-wide/16 v4, 0x14

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 96
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1, p1}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 98
    if-eqz p1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mMute:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mMute:Landroid/os/Handler;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 105
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mMute:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->mMute:Landroid/os/Handler;

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
