.class public Lcom/sec/android/app/voicenote/library/stt/M4aConsts;
.super Ljava/lang/Object;
.source "M4aConsts.java"


# static fields
.field public static final BKMK_LENGTH:I = 0x264

.field public static final BOOK:Ljava/lang/String; = "book"

.field public static final BOOKMARK:Ljava/lang/String; = "bkmk"

.field public static final BOOKMARKS_NUMBER:Ljava/lang/String; = "bnum"

.field public static final DESCR_LENGTH:I = 0x1f4

.field public static final FTYP:Ljava/lang/String; = "ftyp"

.field public static final IMAG:Ljava/lang/String; = "imag"

.field public static final MAX_BOOKMARKS_NUMBER:I = 0x32

.field public static final MDAT:Ljava/lang/String; = "mdat"

.field public static final MEMO:Ljava/lang/String; = "memo"

.field public static final MEMO_HEIGHT:I = 0x190

.field public static final MEMO_WIDTH:I = 0x190

.field public static final MOOV:Ljava/lang/String; = "moov"

.field public static final MULTIP:I = 0x3e8

.field public static final NAME_PLUS_LENGTH:I = 0x8

.field public static final SMALL_SEG:I = 0x4

.field public static final STTD:Ljava/lang/String; = "sttd"

.field public static final TEXT:Ljava/lang/String; = "text"

.field public static final THUM:Ljava/lang/String; = "thum"

.field public static final TITLE_LENGTH:I = 0x64

.field public static final UDTA:Ljava/lang/String; = "udta"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
