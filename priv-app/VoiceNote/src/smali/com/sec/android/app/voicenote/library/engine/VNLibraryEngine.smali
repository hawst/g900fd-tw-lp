.class public Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
.super Ljava/lang/Object;
.source "VNLibraryEngine.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;
    }
.end annotation


# static fields
.field private static final MSG_CHECK_NEXTITEM:I = 0x2

.field private static final MSG_CHECK_PREVIOUSITEM:I = 0x3

.field private static final MSG_DISPLAY_TOOLTIP:I = 0x4

.field private static final MSG_SKIP_SILENCE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VNLibraryEngine"

.field private static final VALUE_IGNORE_MSEC:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private mBookmarks_origin:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mEventHandler:Landroid/os/Handler;

.field private mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

.field private mIsPlayTrimNewFile:Z

.field private mSttData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    .line 69
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mIsPlayTrimNewFile:Z

    .line 879
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;-><init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mEventHandler:Landroid/os/Handler;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    .line 73
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 876
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    goto :goto_0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 86
    sput-object p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 87
    return-void
.end method

.method private isEqualBookmarkContent()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 969
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    if-nez v7, :cond_2

    .line 970
    :cond_0
    const-string v5, "VNLibraryEngine"

    const-string v7, "isEqualBookmarkContent - bookmark is null"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 992
    :cond_1
    :goto_0
    return v5

    .line 974
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 975
    .local v0, "arraySize1":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 978
    .local v1, "arraySize2":I
    if-ne v0, v1, :cond_1

    .line 982
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_4

    .line 983
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 984
    .local v3, "tempBookmark1":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 985
    .local v4, "tempBookmark2":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 986
    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v8

    if-ne v7, v8, :cond_1

    .line 982
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v3    # "tempBookmark1":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v4    # "tempBookmark2":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    :cond_4
    move v5, v6

    .line 992
    goto :goto_0
.end method

.method private organizeBookmarks()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 692
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 693
    .local v0, "bookmarkIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 695
    .local v3, "count":I
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 696
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 697
    .local v2, "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getNamed()Z

    move-result v6

    if-nez v6, :cond_0

    .line 698
    const/4 v1, 0x1

    .line 699
    .local v1, "bookmarkTitleNumber":I
    :goto_0
    add-int/lit8 v6, v1, -0x1

    if-ge v6, v3, :cond_0

    .line 700
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    const v9, 0x7f0b001a

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%d"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 701
    .local v5, "newBookmarkTitle":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v3, :cond_1

    .line 702
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 710
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 711
    goto :goto_0

    .line 704
    :cond_2
    add-int/lit8 v6, v3, -0x1

    if-ne v4, v6, :cond_3

    .line 705
    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setTitle(Ljava/lang/String;)V

    .line 706
    invoke-virtual {v2, v11}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setNamed(Z)V

    .line 707
    add-int/lit8 v1, v3, 0x1

    .line 701
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 714
    .end local v1    # "bookmarkTitleNumber":I
    .end local v2    # "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v4    # "j":I
    .end local v5    # "newBookmarkTitle":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 91
    return-void
.end method


# virtual methods
.method public addBookmarkList()V
    .locals 4

    .prologue
    .line 654
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->addBookmarkList(JLjava/util/ArrayList;)V

    .line 655
    return-void
.end method

.method public addPlayerforBackgroundPlay()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v0, v1, :cond_1

    .line 343
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setOnCompletionListener(Ljava/lang/Object;)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->addPlayer()V

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 349
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setOnCompletionListener(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public changeSkipSilenceButton()V
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->changeSkipSilenceButton()V

    .line 766
    return-void
.end method

.method public doNextPlay()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 266
    const-string v6, "VNLibraryEngine"

    const-string v7, "doNextPlay E"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v6, "skip_interval"

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 268
    const-string v6, "VNLibraryEngine"

    const-string v7, "doNextPlay : KEY_SKIP_INTERVAL"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    sget-object v6, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    const-string v7, "skip_interval_value"

    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v7

    aget v4, v6, v7

    .line 270
    .local v4, "skipinterval":I
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v6, :cond_0

    .line 303
    .end local v4    # "skipinterval":I
    :goto_0
    return-void

    .line 273
    .restart local v4    # "skipinterval":I
    :cond_0
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v6

    mul-int/lit16 v7, v4, 0x3e8

    add-int v2, v6, v7

    .line 274
    .local v2, "nextpoint":I
    const/4 v5, 0x0

    .line 276
    .local v5, "startTime":I
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v6

    if-ge v2, v6, :cond_4

    .line 277
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v3

    .line 278
    .local v3, "repeatTime":[I
    aget v6, v3, v8

    if-ltz v6, :cond_3

    aget v6, v3, v9

    if-ltz v6, :cond_3

    .line 279
    aget v6, v3, v8

    aget v7, v3, v9

    if-le v6, v7, :cond_1

    aget v0, v3, v8

    .line 280
    .local v0, "endTime":I
    :goto_1
    aget v6, v3, v8

    aget v7, v3, v9

    if-ge v6, v7, :cond_2

    aget v5, v3, v8

    .line 281
    :goto_2
    if-le v2, v0, :cond_3

    .line 282
    const-string v6, "VNLibraryEngine"

    const-string v7, "doNextPlay : out of repeat range"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 279
    .end local v0    # "endTime":I
    :cond_1
    aget v0, v3, v9

    goto :goto_1

    .line 280
    .restart local v0    # "endTime":I
    :cond_2
    aget v5, v3, v9

    goto :goto_2

    .line 286
    .end local v0    # "endTime":I
    :cond_3
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 287
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    sub-int v7, v2, v5

    invoke-virtual {v6, v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->setProgress(I)V

    .line 302
    .end local v2    # "nextpoint":I
    .end local v3    # "repeatTime":[I
    .end local v4    # "skipinterval":I
    .end local v5    # "startTime":I
    :cond_4
    :goto_3
    const-string v6, "VNLibraryEngine"

    const-string v7, "doNextPlay X"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 290
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isSTTMode()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 291
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->switchSTT(Ljava/util/ArrayList;)V

    .line 292
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->clickNextItemAsync()V

    .line 293
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 294
    .local v1, "msg":Landroid/os/Message;
    iput v10, v1, Landroid/os/Message;->what:I

    .line 295
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v1, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 299
    .end local v1    # "msg":Landroid/os/Message;
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_3

    .line 297
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->clickNextItem()V

    goto :goto_4
.end method

.method public doPause()V
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 120
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->stopProgress()V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_0
.end method

.method public doPauseForTrim()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->doPauseForTrim()V

    .line 127
    return-void
.end method

.method public doPlay(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 94
    const-string v0, "VNLibraryEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doPlay : id - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSkipSilenceMode(Z)V

    .line 97
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPlay(JLjava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    const-string v0, "VNLibraryEngine"

    const-string v1, "doPlay fail"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 114
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->addPlayer()V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 113
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mainActivityInvalidateOptionsMenu()V

    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateSttFragmentData()V

    goto :goto_1

    .line 110
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    goto :goto_1
.end method

.method public doPlayTrimNewFile()V
    .locals 2

    .prologue
    .line 769
    const-string v0, "VNLibraryEngine"

    const-string v1, "doPlayTrimNewFile"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mIsPlayTrimNewFile:Z

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->doPlayTrimNewFile()V

    .line 772
    return-void
.end method

.method public doPlay_PlayerOn(JJ)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "msec"    # J

    .prologue
    const/4 v6, 0x0

    .line 207
    const-string v2, "VNLibraryEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doPlay_PlayerOn : id - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isEqualBookmarkContent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 210
    :cond_0
    const-string v2, "VNLibraryEngine"

    const-string v3, "Bookmark is not changed. No Save"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 216
    .local v0, "fm":Landroid/app/FragmentManager;
    const-string v2, "FRAGMENT_DIALOG"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 217
    const-string v2, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 218
    const-string v2, "FRAGMENT_MULTI_CATEGORY"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 219
    const-string v2, "FRAGMENT_FILE"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 220
    const-string v2, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 221
    const-string v2, "FRAGMENT_PRIVATE_SELECT"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 222
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 223
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 224
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSkipSilenceMode(Z)V

    .line 226
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->postDoPlay_PlayerOn(JJ)V

    .line 227
    return-void

    .line 211
    .end local v0    # "fm":Landroid/app/FragmentManager;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, "path":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;-><init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;Ljava/lang/String;)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public doPreviousPlay()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 306
    const-string v5, "VNLibraryEngine"

    const-string v7, "doPreviousPlay E"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v5, "skip_interval"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 308
    const-string v5, "VNLibraryEngine"

    const-string v7, "doPreviousPlay : KEY_SKIP_INTERVAL"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    const-string v7, "skip_interval_value"

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v7

    aget v3, v5, v7

    .line 310
    .local v3, "skipinterval":I
    sget-object v5, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v5, :cond_0

    .line 339
    .end local v3    # "skipinterval":I
    :goto_0
    return-void

    .line 313
    .restart local v3    # "skipinterval":I
    :cond_0
    sget-object v5, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v5

    mul-int/lit16 v7, v3, 0x3e8

    sub-int v1, v5, v7

    .line 314
    .local v1, "prevpoint":I
    sget-object v5, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v2

    .line 315
    .local v2, "repeatTime":[I
    const/4 v4, 0x0

    .line 316
    .local v4, "startTime":I
    aget v5, v2, v6

    if-ltz v5, :cond_2

    aget v5, v2, v9

    if-ltz v5, :cond_2

    .line 317
    aget v5, v2, v6

    aget v7, v2, v9

    if-ge v5, v7, :cond_1

    aget v4, v2, v6

    .line 318
    :goto_1
    if-ge v1, v4, :cond_2

    .line 319
    const-string v5, "VNLibraryEngine"

    const-string v6, "doPreviousPlay : out of repeat range"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_1
    aget v4, v2, v9

    goto :goto_1

    .line 323
    :cond_2
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-gez v1, :cond_3

    move v5, v6

    :goto_2
    invoke-virtual {v7, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 324
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-gez v1, :cond_4

    :goto_3
    invoke-virtual {v5, v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->setProgress(I)V

    .line 338
    .end local v1    # "prevpoint":I
    .end local v2    # "repeatTime":[I
    .end local v3    # "skipinterval":I
    .end local v4    # "startTime":I
    :goto_4
    const-string v5, "VNLibraryEngine"

    const-string v6, "doPreviousPlay X"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .restart local v1    # "prevpoint":I
    .restart local v2    # "repeatTime":[I
    .restart local v3    # "skipinterval":I
    .restart local v4    # "startTime":I
    :cond_3
    move v5, v1

    .line 323
    goto :goto_2

    .line 324
    :cond_4
    sub-int v6, v1, v4

    goto :goto_3

    .line 327
    .end local v1    # "prevpoint":I
    .end local v2    # "repeatTime":[I
    .end local v3    # "skipinterval":I
    .end local v4    # "startTime":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isSTTMode()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 328
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->switchSTT(Ljava/util/ArrayList;)V

    .line 329
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->clickPreviousItemAsync()V

    .line 330
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 331
    .local v0, "msg":Landroid/os/Message;
    const/4 v5, 0x3

    iput v5, v0, Landroid/os/Message;->what:I

    .line 332
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x12c

    invoke-virtual {v5, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 336
    .end local v0    # "msg":Landroid/os/Message;
    :goto_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_4

    .line 334
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->clickPreviousItem()V

    goto :goto_5
.end method

.method public doResume()V
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 133
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->startProgess()V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_0
.end method

.method public doResumeForTrim()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->doResumeForTrim()V

    .line 140
    return-void
.end method

.method public doSearch(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 685
    if-eqz p1, :cond_0

    .line 686
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 688
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->searchLibrary(Ljava/lang/String;)V

    .line 689
    return-void
.end method

.method public doStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 146
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isEqualBookmarkContent()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 150
    :cond_2
    const-string v1, "VNLibraryEngine"

    const-string v2, "Bookmark is not changed. No Save"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_3
    :goto_1
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFragmentControllerState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v1, v2, :cond_4

    .line 158
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    .line 160
    :cond_4
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 162
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    .line 169
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mainActivityInvalidateOptionsMenu()V

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    const/4 v1, 0x1

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setSearchMode(ZZ)V

    goto :goto_0

    .line 151
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 154
    new-instance v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;-><init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;Ljava/lang/String;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 164
    .end local v0    # "path":Ljava/lang/String;
    :cond_8
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-nez v1, :cond_6

    goto/16 :goto_0
.end method

.method public doUpdateTrimView()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->doUpdateTrimView()V

    .line 143
    return-void
.end method

.method public endTrim(Z)V
    .locals 1
    .param p1, "bCancel"    # Z

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeTrim(Z)V

    .line 408
    return-void
.end method

.method public finishActionMode()V
    .locals 1

    .prologue
    .line 951
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 953
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    .line 955
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setActionMode(Z)V

    .line 956
    return-void
.end method

.method public getActionMode()Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method public getIdxNearByCurrentPosition(I)I
    .locals 12
    .param p1, "currentPos"    # I

    .prologue
    .line 912
    const/4 v4, -0x1

    .line 914
    .local v4, "idxNearByCurPos":I
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    if-nez v9, :cond_0

    move v5, v4

    .line 933
    .end local v4    # "idxNearByCurPos":I
    .local v5, "idxNearByCurPos":I
    :goto_0
    return v5

    .line 918
    .end local v5    # "idxNearByCurPos":I
    .restart local v4    # "idxNearByCurPos":I
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 919
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 920
    .local v0, "bookmarkssize":I
    const-wide v6, 0x7fffffffffffffffL

    .line 921
    .local v6, "lastestLen":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_3

    .line 922
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 923
    .local v8, "tmp":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v9

    int-to-long v2, v9

    .line 924
    .local v2, "first":J
    const-wide v10, 0x7fffffffffffffffL

    cmp-long v9, v6, v10

    if-nez v9, :cond_2

    .line 925
    move-wide v6, v2

    .line 926
    move v4, v1

    .line 921
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 927
    :cond_2
    int-to-long v10, p1

    cmp-long v9, v10, v2

    if-ltz v9, :cond_1

    .line 928
    move-wide v6, v2

    .line 929
    move v4, v1

    goto :goto_2

    .end local v2    # "first":J
    .end local v8    # "tmp":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    :cond_3
    move v5, v4

    .line 933
    .end local v4    # "idxNearByCurPos":I
    .restart local v5    # "idxNearByCurPos":I
    goto :goto_0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 354
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 355
    const/16 v0, 0x15

    .line 357
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    goto :goto_0
.end method

.method public getSttData()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 815
    const-string v4, "VNLibraryEngine"

    const-string v6, "getSttData"

    invoke-static {v4, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v4, v5

    .line 831
    :goto_0
    return v4

    .line 819
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 820
    .local v3, "path":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 821
    sget-object v6, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 822
    :try_start_0
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    invoke-direct {v1, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 823
    .local v1, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v2

    .line 824
    .local v2, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 825
    .local v0, "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->readSttd()Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    .line 826
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 828
    const/4 v4, 0x1

    goto :goto_0

    .line 826
    .end local v0    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v1    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_2
    move v4, v5

    .line 831
    goto :goto_0
.end method

.method public init()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->init()V

    .line 362
    return-void
.end method

.method public insetSttToBookmark(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V
    .locals 12
    .param p1, "bookmark"    # Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .prologue
    .line 775
    const-string v7, "VNLibraryEngine"

    const-string v8, "insetSttToBookmark"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    if-nez v7, :cond_1

    .line 778
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v7

    if-nez v7, :cond_1

    .line 779
    const-string v7, "VNLibraryEngine"

    const-string v8, "insetSttToBookmark : stt data is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v7

    int-to-long v0, v7

    .line 785
    .local v0, "currentTime":J
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 786
    .local v5, "size":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 787
    .local v4, "sb":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 789
    .local v6, "title":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_2

    .line 790
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v7, :cond_4

    .line 791
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v8, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v10, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->duration:J

    add-long/2addr v8, v10

    cmp-long v7, v0, v8

    if-gez v7, :cond_4

    .line 792
    move v3, v2

    .local v3, "j":I
    :goto_2
    if-ge v3, v5, :cond_2

    .line 793
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v7, :cond_3

    .line 794
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    const/16 v8, 0x32

    if-le v7, v8, :cond_3

    .line 805
    .end local v3    # "j":I
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 806
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 807
    const-string v7, "VNLibraryEngine"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insetSttToBookmark : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    invoke-virtual {p1, v6}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setTitle(Ljava/lang/String;)V

    .line 809
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setNamed(Z)V

    goto/16 :goto_0

    .line 792
    .restart local v3    # "j":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 789
    .end local v3    # "j":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public isBookmarkList()Z
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isBookmarkList()Z

    move-result v0

    return v0
.end method

.method public isBookmarkListSelectionMode()Z
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isBookmarkListSelectionMode()Z

    move-result v0

    return v0
.end method

.method public isPlayerActive()Z
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRepeatMode()Z
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isRepeateMode()Z

    move-result v0

    return v0
.end method

.method public isSTTMode()Z
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isSTTMode()Z

    move-result v0

    return v0
.end method

.method public isSkipSilenceMode()Z
    .locals 1

    .prologue
    .line 761
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isSkipSilenceMode()Z

    move-result v0

    return v0
.end method

.method public isTrimActive()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-nez v1, :cond_1

    .line 414
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBookmarkChildPressed(JI)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "miliSec"    # I

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getState()I

    move-result v0

    .line 484
    .local v0, "playState":I
    const/16 v1, 0x15

    if-ne v1, v0, :cond_1

    .line 485
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlay(J)V

    .line 486
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 487
    :cond_1
    const/16 v1, 0x17

    if-eq v1, v0, :cond_2

    const/16 v1, 0x18

    if-ne v1, v0, :cond_0

    .line 489
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_3

    .line 490
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshToolBar(Z)V

    goto :goto_0

    .line 493
    :cond_3
    int-to-long v2, p3

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlay_PlayerOn(JJ)V

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v2, 0x0

    .line 366
    const-string v0, "VNLibraryEngine"

    const-string v1, "onCompletion"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    :cond_0
    const-string v0, "VNLibraryEngine"

    const-string v1, "onCompletion - activity is isFinishing"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 370
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 395
    :goto_0
    return-void

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->closeOptionsMenu()V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mainActivityInvalidateOptionsMenu()V

    .line 377
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 378
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setPlaySpeedIndex(I)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->stopTrimPlay()V

    .line 394
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    goto :goto_0

    .line 382
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->restartPlay()V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshToolBar(Z)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_1

    .line 387
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 390
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->removeToolbarHelp()V

    goto :goto_1
.end method

.method public onPressBookmarkListView(I)V
    .locals 6
    .param p1, "miliSec"    # I

    .prologue
    const/4 v5, 0x1

    .line 459
    const-string v2, "VNLibraryEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPressBookmarkListView - milisec : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 461
    :cond_0
    const-string v2, "VNLibraryEngine"

    const-string v3, "onPressBookmarkListView - return!!"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_1
    :goto_0
    return-void

    .line 465
    :cond_2
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v1

    .line 466
    .local v1, "repeatTime":[I
    aget v2, v1, v5

    if-ltz v2, :cond_4

    const/4 v2, 0x0

    aget v2, v1, v2

    if-gt v2, p1, :cond_3

    aget v2, v1, v5

    if-ge v2, p1, :cond_4

    .line 467
    :cond_3
    const-string v2, "VNLibraryEngine"

    const-string v3, "onPressBookmarkListView - bookmark position is exceed of repeat bound"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getState()I

    move-result v0

    .line 472
    .local v0, "playState":I
    const/16 v2, 0x17

    if-ne v2, v0, :cond_5

    .line 473
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_0

    .line 474
    :cond_5
    const/16 v2, 0x18

    if-ne v2, v0, :cond_1

    .line 475
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 476
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-eqz v2, :cond_1

    .line 477
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->setProgress(I)V

    goto :goto_0
.end method

.method public onPressListView(J)Z
    .locals 7
    .param p1, "id"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 422
    const-string v1, "VNLibraryEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPressListView : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 424
    :cond_0
    const-string v1, "VNLibraryEngine"

    const-string v2, "onPressListView - return!!"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const/4 v1, 0x0

    .line 455
    :goto_0
    return v1

    .line 427
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getState()I

    move-result v0

    .line 428
    .local v0, "playState":I
    packed-switch v0, :pswitch_data_0

    .line 455
    :goto_1
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 430
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_2

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPause()V

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_1

    .line 434
    :cond_2
    invoke-virtual {p0, p1, p2, v4, v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlay_PlayerOn(JJ)V

    goto :goto_1

    .line 439
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_3

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doResume()V

    .line 441
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    goto :goto_1

    .line 443
    :cond_3
    invoke-virtual {p0, p1, p2, v4, v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlay_PlayerOn(JJ)V

    goto :goto_1

    .line 448
    :pswitch_3
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlay(J)V

    .line 449
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mEventHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 428
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public postDoPlay_PlayerOn(JJ)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "miliSec"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 230
    const-string v0, "VNLibraryEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postDoPlay_PlayerOn : id - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 232
    const-string v0, "VNLibraryEngine"

    const-string v1, "postDoPlay_PlayerOn() mService is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-nez v0, :cond_1

    .line 236
    const-string v0, "VNLibraryEngine"

    const-string v1, "postDoPlay_PlayerOn() fragment controller is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mIsPlayTrimNewFile:Z

    if-eqz v0, :cond_6

    .line 241
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p1, p2, p0, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPlay(JLjava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    .line 245
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mIsPlayTrimNewFile:Z

    .line 253
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshToolBar(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    .line 256
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-eqz v0, :cond_4

    .line 257
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    long-to-int v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 259
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v0

    if-nez v0, :cond_5

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 262
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mainActivityInvalidateOptionsMenu()V

    goto :goto_0

    .line 247
    :cond_6
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p1, p2, p0, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPlay(JLjava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    goto :goto_1
.end method

.method public refreashAnim(Z)V
    .locals 1
    .param p1, "needAnim"    # Z

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreashAnim(Z)V

    .line 507
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 82
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    .line 83
    return-void
.end method

.method public removeBookmarkList()V
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeBookmarkList()V

    .line 659
    return-void
.end method

.method public removePlayer()V
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    .line 529
    :cond_0
    return-void
.end method

.method public resetRepeatMode()V
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->resetRepeatMode()V

    .line 997
    return-void
.end method

.method public restartPlay()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 176
    const-string v2, "VNLibraryEngine"

    const-string v3, "restartPlay"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v2, :cond_1

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    .line 183
    .local v0, "id":J
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 184
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSkipSilenceMode(Z)V

    .line 185
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, v0, v1, p0, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPlay(JLjava/lang/Object;Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    .line 190
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v2

    if-nez v2, :cond_4

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 197
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mainActivityInvalidateOptionsMenu()V

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 200
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 201
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 194
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateSttFragment()V

    goto :goto_1
.end method

.method public saveBookmarks()V
    .locals 6

    .prologue
    .line 854
    const-string v2, "VNLibraryEngine"

    const-string v3, "saveBookmarks : Bookmark is not changed. No Save"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isEqualBookmarkContent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 856
    :cond_0
    const-string v2, "VNLibraryEngine"

    const-string v3, "Bookmark is not changed. No Save"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :cond_1
    :goto_0
    return-void

    .line 857
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_1

    .line 858
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 859
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 860
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateBookmarkList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 861
    new-instance v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;-><init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;Ljava/lang/String;)V

    .line 862
    .local v1, "task":Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;
    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->setClearTiming(J)V

    .line 863
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public selectBookmark(II)V
    .locals 2
    .param p1, "currentPosition"    # I
    .param p2, "optionId"    # I

    .prologue
    .line 937
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getIdxNearByCurrentPosition(I)I

    move-result v0

    .line 938
    .local v0, "idxNearByCurPos":I
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->selectBookmark(II)V

    .line 939
    return-void
.end method

.method public setActionMode(Landroid/view/ActionMode;)V
    .locals 1
    .param p1, "actionMode"    # Landroid/view/ActionMode;

    .prologue
    .line 946
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setActionMode(Z)V

    .line 948
    return-void

    .line 947
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionMode(Z)V
    .locals 2
    .param p1, "ActionModeOn"    # Z

    .prologue
    .line 959
    const-string v0, "VNLibraryEngine"

    const-string v1, "setActionMode()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    if-eqz p1, :cond_0

    .line 961
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->hidePlayer()V

    .line 965
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->setActionModehOn(Z)V

    .line 966
    return-void

    .line 963
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->showPlayer()V

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->setProgress(I)V

    .line 671
    return-void
.end method

.method public setSearchMode(ZZ)V
    .locals 1
    .param p1, "searchOn"    # Z
    .param p2, "updateUI"    # Z

    .prologue
    .line 674
    if-eqz p2, :cond_0

    .line 675
    if-eqz p1, :cond_1

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeIdleControl()V

    .line 681
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->setSearchOn(Z)V

    .line 682
    return-void

    .line 678
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->addIdleContorl()V

    goto :goto_0
.end method

.method public startRepeatModeTrim()V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->startRepeatModeTrim()V

    .line 399
    return-void
.end method

.method public startTrim()V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->addTrim()V

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->saveBookmarks()V

    .line 404
    return-void
.end method

.method public switchSTT()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->switchSTT(Ljava/util/ArrayList;)V

    .line 647
    return-void
.end method

.method public switchSkipSilenceMode(ZLandroid/os/Bundle;)V
    .locals 11
    .param p1, "isPrepared"    # Z
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 717
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    if-nez v7, :cond_1

    .line 718
    :cond_0
    const-string v7, "VNLibraryEngine"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "switchSkipSilenceMode E : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    :goto_0
    return-void

    .line 721
    :cond_1
    const-string v7, "VNLibraryEngine"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "switchSkipSilenceMode E : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    if-nez p1, :cond_3

    .line 723
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    .line 724
    .local v2, "id":J
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 725
    .local v0, "currentPos":I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v5

    .line 726
    .local v5, "playspeed":I
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v6

    .line 727
    .local v6, "state":I
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 728
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 729
    .local v1, "msg":Landroid/os/Message;
    iput v10, v1, Landroid/os/Message;->what:I

    .line 730
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 731
    .local v4, "ori_data":Landroid/os/Bundle;
    const-string v7, "id"

    invoke-virtual {v4, v7, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 732
    const-string v7, "currentPos"

    invoke-virtual {v4, v7, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 733
    const-string v7, "playspeed"

    invoke-virtual {v4, v7, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 734
    const-string v7, "state"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 735
    invoke-virtual {v1, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 736
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mEventHandler:Landroid/os/Handler;

    if-eqz v7, :cond_2

    .line 737
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x190

    invoke-virtual {v7, v1, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 739
    :cond_2
    const-string v7, "VNLibraryEngine"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "switchSkipSilenceMode : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    .end local v0    # "currentPos":I
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "id":J
    .end local v4    # "ori_data":Landroid/os/Bundle;
    .end local v5    # "playspeed":I
    .end local v6    # "state":I
    :goto_1
    const-string v7, "VNLibraryEngine"

    const-string v8, "switchSkipSilenceMode X"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 741
    :cond_3
    if-nez p2, :cond_4

    .line 742
    const-string v7, "VNLibraryEngine"

    const-string v8, "switchSkipSilenceMode : bundle is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 745
    :cond_4
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->switchSkipSilenceMode()V

    .line 746
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-string v8, "id"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9, p0, v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPlay(JLjava/lang/Object;Z)Z

    .line 747
    const-string v7, "playspeed"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setPlaySpeedIndex(I)V

    .line 748
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-string v8, "currentPos"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 749
    const-string v7, "state"

    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x17

    if-ne v7, v8, :cond_5

    .line 750
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 752
    :cond_5
    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-string v8, "playspeed"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setPlaySpeed(I)V

    .line 753
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshToolBar(Z)V

    .line 754
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    .line 755
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mainActivityInvalidateOptionsMenu()V

    goto :goto_1
.end method

.method public updateBookmarkList(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 6
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 618
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-nez p1, :cond_1

    .line 619
    const-string v4, "VNLibraryEngine"

    const-string v5, "Bookmark is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_0
    return-void

    .line 622
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    .line 624
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->organizeBookmarks()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 630
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 631
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    .line 634
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 635
    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_0

    .line 636
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 637
    .local v0, "copiedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks_origin:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-direct {v5, v0}, Lcom/sec/android/app/voicenote/common/util/Bookmark;-><init>(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 625
    .end local v0    # "copiedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v2    # "i":I
    .end local v3    # "size":I
    :catch_0
    move-exception v1

    .line 626
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v4, "VNLibraryEngine"

    const-string v5, "Check Bookmark is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 627
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 628
    .local v1, "e":Ljava/util/ConcurrentModificationException;
    const-string v4, "VNLibraryEngine"

    const-string v5, "updateBookmarkList Bookmarks have ConcurrentModificationException"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 606
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    .line 608
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->organizeBookmarks()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 614
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 615
    return-void

    .line 609
    :catch_0
    move-exception v0

    .line 610
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VNLibraryEngine"

    const-string v2, "Check Bookmark is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 611
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 612
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    const-string v1, "VNLibraryEngine"

    const-string v2, "updateBookmarks Bookmarks have ConcurrentModificationException"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateBookmarksPlayer(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 593
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;

    .line 595
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->organizeBookmarks()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 602
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarksPlayer(Ljava/util/ArrayList;)V

    .line 603
    return-void

    .line 596
    :catch_0
    move-exception v0

    .line 597
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VNLibraryEngine"

    const-string v2, "Check Bookmark is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 598
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 599
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    const-string v1, "VNLibraryEngine"

    const-string v2, "updateBookmarksPlayer Bookmarks have ConcurrentModificationException"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateControlButtons()V
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshControlBar()V

    .line 513
    :cond_0
    return-void
.end method

.method public updateLibrary(Z)V
    .locals 1
    .param p1, "bRequeryCursor"    # Z

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateLibrary(Z)V

    .line 501
    :cond_0
    return-void
.end method

.method public updatePlayer()V
    .locals 4

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    if-nez v0, :cond_1

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 518
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    goto :goto_0

    .line 520
    :cond_2
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->refreshToolBar(Z)V

    goto :goto_0
.end method

.method public updateSTTString(ILjava/lang/String;Z)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "newLine"    # Z

    .prologue
    const/4 v3, 0x0

    .line 835
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 836
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v0

    if-nez v0, :cond_1

    .line 851
    :cond_0
    :goto_0
    return-void

    .line 841
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 842
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 847
    :goto_1
    if-eqz p3, :cond_0

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v1, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    goto :goto_0

    .line 844
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aput-object p2, v0, v3

    goto :goto_1
.end method

.method public updateSttFragmentData()V
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isSTTMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateSttFragment()V

    .line 872
    :cond_0
    return-void
.end method
