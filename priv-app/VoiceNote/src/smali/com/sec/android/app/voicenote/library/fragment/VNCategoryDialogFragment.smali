.class public Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNCategoryDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final SAVE_LISTVIEW:Ljava/lang/String; = "listview"

.field private static final TAG:Ljava/lang/String; = "VNCategoryDialogFragment"


# instance fields
.field private mAllItem:Landroid/widget/LinearLayout;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mCursor:Landroid/database/Cursor;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    .line 62
    return-void
.end method

.method private initView(Landroid/view/LayoutInflater;)V
    .locals 14
    .param p1, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const-wide/16 v12, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 115
    const-string v5, "VNCategoryDialogFragment"

    const-string v6, "initView E"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mViewChild:Landroid/view/View;

    const v6, 0x7f0e0033

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    .line 117
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mViewChild:Landroid/view/View;

    const v7, 0x7f0e0034

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 120
    const v5, 0x7f030025

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p1, v5, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 123
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v6, 0x7f0e005a

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 124
    .local v4, "title":Landroid/widget/TextView;
    const v5, 0x7f0b0013

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 125
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v6, 0x7f0e005b

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 126
    .local v0, "allcount":Landroid/widget/TextView;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "(%d)"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountInLibrary(Landroid/content/Context;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v6, 0x7f0e005c

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 130
    .local v1, "radio":Landroid/widget/RadioButton;
    const-string v5, "category_text"

    invoke-static {v5, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v2

    .line 131
    .local v2, "id":J
    cmp-long v5, v12, v2

    if-nez v5, :cond_0

    .line 132
    invoke-virtual {v1, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 136
    :cond_0
    return-void
.end method

.method private listBinding()Z
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v2, 0x2

    const/4 v10, 0x1

    const/4 v3, 0x0

    const/4 v11, 0x0

    .line 139
    const-string v0, "VNCategoryDialogFragment"

    const-string v1, "listBinding E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 143
    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 145
    :cond_0
    new-array v4, v12, [Ljava/lang/String;

    const-string v0, "COLOR"

    aput-object v0, v4, v11

    const-string v0, "TITLE"

    aput-object v0, v4, v10

    const-string v0, "_id"

    aput-object v0, v4, v2

    .line 150
    .local v4, "cols":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 152
    .local v5, "to":[I
    const/4 v0, 0x4

    new-array v5, v0, [I

    .line 153
    const v0, 0x7f0e0059

    aput v0, v5, v11

    .line 154
    const v0, 0x7f0e005a

    aput v0, v5, v10

    .line 155
    const v0, 0x7f0e005b

    aput v0, v5, v2

    .line 156
    const v0, 0x7f0e005c

    aput v0, v5, v12

    .line 159
    const/4 v9, 0x0

    .line 160
    .local v9, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v6

    .line 161
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 179
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    :cond_2
    const-string v0, "VNCategoryDialogFragment"

    const-string v1, "listBinding() : cursor null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v11

    .line 187
    :goto_1
    return v0

    .line 165
    :catch_0
    move-exception v7

    .line 166
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VNCategoryDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listBinding - SQLiteException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 169
    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 171
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 172
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VNCategoryDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 175
    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 184
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_3
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f030025

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v11}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectMode(Z)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move v0, v10

    .line 187
    goto :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 91
    const-string v1, "VNCategoryDialogFragment"

    const-string v2, "onActivityCreated E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->listBinding()Z

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const-string v1, "listview"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 99
    .local v0, "state":Landroid/os/Parcelable;
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 103
    .end local v0    # "state":Landroid/os/Parcelable;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const-string v0, "VNCategoryDialogFragment"

    const-string v1, "onCreate E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 74
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    const-string v0, "VNCategoryDialogFragment"

    const-string v1, "onCreateView E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 82
    const v0, 0x7f030014

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mViewChild:Landroid/view/View;

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0b0117

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 85
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->initView(Landroid/view/LayoutInflater;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mViewChild:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 192
    const-string v0, "VNCategoryDialogFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 196
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 201
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_2

    .line 204
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 207
    :cond_2
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 208
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "clickedView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v4, 0x7f0e005c

    const/4 v3, 0x1

    .line 222
    const-string v2, "category_text"

    invoke-static {v2, p4, p5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 224
    .local v1, "radio":Landroid/widget/RadioButton;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 225
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 226
    .local v0, "clickedRadio":Landroid/widget/RadioButton;
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->dismissAllowingStateLoss()V

    .line 230
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 212
    const-string v0, "VNCategoryDialogFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 218
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 109
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNCategoryDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 110
    .local v0, "state":Landroid/os/Parcelable;
    const-string v1, "listview"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 111
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    return-void
.end method
