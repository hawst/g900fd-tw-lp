.class Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;
.super Ljava/lang/Object;
.source "VNTrimSaveDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "position"    # I

    .prologue
    .line 82
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-nez v1, :cond_0

    .line 90
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentTag:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 85
    .local v0, "frag":Landroid/app/Fragment;
    if-eqz v0, :cond_1

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->mtargetFragmentRequestCode:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->dismissAllowingStateLoss()V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->isFinished:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;Z)Z

    goto :goto_0
.end method
