.class Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;
.super Ljava/lang/Object;
.source "SamsungAudioSource.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->handleSourceClosed(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->val$success:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$400(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$402(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)Z

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$500(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$502(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)Z

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->notifySourceClosed()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$600(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$700(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/nuance/dragon/toolkit/util/WorkerThread;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$700(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/nuance/dragon/toolkit/util/WorkerThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->stop()V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$702(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Lcom/nuance/dragon/toolkit/util/WorkerThread;)Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$000(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 332
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->val$success:Z

    if-eqz v0, :cond_3

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$000(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;->onStopped(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V

    .line 337
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$002(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;)Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    .line 339
    :cond_2
    return-void

    .line 335
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->access$000(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;->onError(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V

    goto :goto_0
.end method
