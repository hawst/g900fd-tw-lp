.class public final enum Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;
.super Ljava/lang/Enum;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/stt/Const;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FinishType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

.field public static final enum CONVERT_TRY_COUNT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

.field public static final enum DECODE_TRY_COUNT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

.field public static final enum NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

.field public static final enum NORMAL:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

.field public static final enum SOURCE_FILE_NOT_FOUND:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

.field public static final enum TEXT_CONVERT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NORMAL:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 47
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    const-string v1, "SOURCE_FILE_NOT_FOUND"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->SOURCE_FILE_NOT_FOUND:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 48
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    const-string v1, "NETWORK_NOT_AVAILABLE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 49
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    const-string v1, "DECODE_TRY_COUNT_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->DECODE_TRY_COUNT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 50
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    const-string v1, "CONVERT_TRY_COUNT_ERROR"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->CONVERT_TRY_COUNT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 51
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    const-string v1, "TEXT_CONVERT_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->TEXT_CONVERT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NORMAL:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->SOURCE_FILE_NOT_FOUND:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->DECODE_TRY_COUNT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->CONVERT_TRY_COUNT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->TEXT_CONVERT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->$VALUES:[Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->$VALUES:[Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    invoke-virtual {v0}, [Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    return-object v0
.end method
