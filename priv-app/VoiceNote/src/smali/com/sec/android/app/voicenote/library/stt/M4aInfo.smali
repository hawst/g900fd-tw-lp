.class public Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
.super Ljava/lang/Object;
.source "M4aInfo.java"


# instance fields
.field public bnumPos:J

.field public bookPos:J

.field public fileBnumLength:I

.field public fileBookLength:I

.field public fileImagLength:I

.field public fileMemoLength:I

.field public fileMoovLength:I

.field public filePos:J

.field public fileSttdLength:I

.field public fileTextLength:I

.field public fileThumLength:I

.field public fileUdtaLength:I

.field public hasBookmarks:Z

.field public hasMemo:Z

.field public hasSttd:Z

.field public memoPos:J

.field public moovPos:J

.field public path:Ljava/lang/String;

.field public sttdPos:J

.field public textPos:J

.field public thumPos:J

.field public udtaPos:J

.field public usedToWrite:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileMoovLength:I

    .line 5
    iput v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileUdtaLength:I

    .line 7
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileBookLength:I

    .line 8
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileBnumLength:I

    .line 9
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileMemoLength:I

    .line 10
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileImagLength:I

    .line 11
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileThumLength:I

    .line 12
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileTextLength:I

    .line 13
    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileSttdLength:I

    .line 15
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->moovPos:J

    .line 16
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    .line 17
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->bookPos:J

    .line 18
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->bnumPos:J

    .line 19
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->memoPos:J

    .line 20
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->filePos:J

    .line 21
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->thumPos:J

    .line 22
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->textPos:J

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    .line 25
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasBookmarks:Z

    .line 26
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasMemo:Z

    .line 27
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    .line 28
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->usedToWrite:Z

    return-void
.end method
