.class public Lcom/sec/android/app/voicenote/library/stt/DB;
.super Ljava/lang/Object;
.source "DB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final AUDIO_CONVERTED:Ljava/lang/String; = "audio_converted"

.field private static final CONVERTING_QUEUE_TABLE:Ljava/lang/String; = "converting_queue_table"

.field private static final CONVERTING_QUEUE_TABLE_SCHEMA:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS converting_queue_table(_id INTEGER PRIMARY KEY, _data TEXT, pcm_decoded INTEGER, decoding_try_count INTEGER, audio_converted INTEGER, converting_try_count INTEGER, insertion_time LONG);"

.field private static final CONVERTING_TRY_COUNT:Ljava/lang/String; = "converting_try_count"

.field private static final CREATE_TABLE_IF_NOT_EXISTS:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS "

.field private static final DATABASE_NAME:Ljava/lang/String; = "voice_converter.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final DATA_PATH:Ljava/lang/String; = "_data"

.field private static final DECODING_TRY_COUNT:Ljava/lang/String; = "decoding_try_count"

.field private static final DROP_TABLE_IF_EXISTS:Ljava/lang/String; = "DROP TABLE IF EXISTS "

.field private static final FALSE:I = 0x0

.field private static final ID:Ljava/lang/String; = "_id"

.field private static final INSERTION_TIME:Ljava/lang/String; = "insertion_time"

.field private static final MAX_TRY_COUNT:I = 0x5

.field private static final PCM_DECODED:Ljava/lang/String; = "pcm_decoded"

.field private static final TAG:Ljava/lang/String;

.field private static final TEXT_QUEUE_TABLE_SCHEMA:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS text_sending_queue_table(_id INTEGER PRIMARY KEY, _data TEXT, insertion_time LONG);"

.field private static final TEXT_SENDING_QUEUE_TABLE:Ljava/lang/String; = "text_sending_queue_table"

.field private static final TRUE:I = 0x1

.field private static mUniqueInstance:Lcom/sec/android/app/voicenote/library/stt/DB;


# instance fields
.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mOpenHelper:Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/app/voicenote/library/stt/DB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/DB;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mOpenHelper:Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mOpenHelper:Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/stt/DB$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 58
    return-void
.end method

.method private fetchInt(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "columns"    # [Ljava/lang/String;

    .prologue
    .line 123
    if-nez p2, :cond_1

    .line 124
    const/4 v10, -0x1

    .line 143
    :cond_0
    :goto_0
    return v10

    .line 127
    :cond_1
    const/4 v10, -0x1

    .line 128
    .local v10, "index":I
    const/4 v8, 0x0

    .line 131
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p2}, Lcom/sec/android/app/voicenote/library/stt/DB;->getSelectionByPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 132
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 138
    :cond_2
    if-eqz v8, :cond_0

    .line 139
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 135
    :catch_0
    move-exception v9

    .line 136
    .local v9, "e":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_1
    invoke-virtual {v9}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    if-eqz v8, :cond_0

    .line 139
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 138
    .end local v9    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 139
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/android/app/voicenote/library/stt/DB;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const-class v1, Lcom/sec/android/app/voicenote/library/stt/DB;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/DB;->mUniqueInstance:Lcom/sec/android/app/voicenote/library/stt/DB;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/DB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/stt/DB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/DB;->mUniqueInstance:Lcom/sec/android/app/voicenote/library/stt/DB;

    .line 65
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/DB;->mUniqueInstance:Lcom/sec/android/app/voicenote/library/stt/DB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getConvertingTryCount(Ljava/lang/String;)I
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 177
    if-eqz p1, :cond_0

    .line 178
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "converting_try_count"

    aput-object v2, v0, v1

    .line 179
    .local v0, "column":[Ljava/lang/String;
    const-string v1, "converting_queue_table"

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->fetchInt(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 181
    .end local v0    # "column":[Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getDecodingTryCount(Ljava/lang/String;)I
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 169
    if-eqz p1, :cond_0

    .line 170
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "decoding_try_count"

    aput-object v2, v0, v1

    .line 171
    .local v0, "column":[Ljava/lang/String;
    const-string v1, "converting_queue_table"

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->fetchInt(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 173
    .end local v0    # "column":[Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private declared-synchronized getOldestPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 200
    monitor-enter p0

    const/4 v10, 0x0

    .line 201
    .local v10, "path":Ljava/lang/String;
    const/4 v8, 0x0

    .line 202
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    .local v2, "columns":[Ljava/lang/String;
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "insertion_time ASC LIMIT 1"

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 205
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 211
    :cond_0
    if-eqz v8, :cond_1

    .line 212
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 216
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v10

    .line 208
    :catch_0
    move-exception v9

    .line 209
    .local v9, "e":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_3
    invoke-virtual {v9}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 211
    if-eqz v8, :cond_1

    .line 212
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 200
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v9    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 211
    .restart local v2    # "columns":[Ljava/lang/String;
    :catchall_1
    move-exception v0

    if-eqz v8, :cond_2

    .line 212
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private getSelectionByPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data = \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private insert(Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 91
    if-nez p2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v2

    .line 95
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 97
    .local v0, "rowId":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 98
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isPathExist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 147
    if-nez p2, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v1

    .line 151
    :cond_1
    new-array v0, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v1

    .line 153
    .local v0, "columns":[Ljava/lang/String;
    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->fetchInt(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exist path:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;)V

    move v1, v2

    .line 155
    goto :goto_0
.end method

.method private isTrue(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 3
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "column"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 162
    if-nez p2, :cond_0

    .line 165
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/stt/DB;->fetchInt(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized removePath(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 185
    monitor-enter p0

    if-nez p2, :cond_1

    .line 196
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 189
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p2}, Lcom/sec/android/app/voicenote/library/stt/DB;->getSelectionByPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 191
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "table :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has removed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    const/4 v1, 0x1

    goto :goto_0

    .line 185
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 5
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 105
    if-nez p2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v1

    .line 109
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/stt/DB;->getSelectionByPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, p1, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 111
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 112
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getNextConvPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "currentPath"    # Ljava/lang/String;

    .prologue
    .line 270
    monitor-enter p0

    const/4 v11, 0x0

    .line 271
    .local v11, "path":Ljava/lang/String;
    const/4 v8, 0x0

    .line 273
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    .line 274
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 276
    .local v2, "columns":[Ljava/lang/String;
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "converting_queue_table"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "insertion_time ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 277
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v12

    .line 278
    .local v12, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    add-int/lit8 v0, v12, -0x1

    if-ge v10, v0, :cond_1

    .line 279
    invoke-interface {v8, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 280
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    .line 282
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 278
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 290
    :cond_1
    if-eqz v8, :cond_2

    .line 291
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292
    const/4 v8, 0x0

    .line 297
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v10    # "i":I
    .end local v12    # "size":I
    :cond_2
    :goto_1
    monitor-exit p0

    return-object v11

    .line 287
    .restart local v2    # "columns":[Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 288
    .local v9, "e":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_3
    invoke-virtual {v9}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 290
    if-eqz v8, :cond_2

    .line 291
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 292
    const/4 v8, 0x0

    goto :goto_1

    .line 290
    .end local v9    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 291
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 292
    const/4 v8, 0x0

    :cond_3
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 270
    .end local v2    # "columns":[Ljava/lang/String;
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getOldestConvPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    const-string v0, "converting_queue_table"

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->getOldestPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTextQueueCursor()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 321
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 323
    .local v2, "columns":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "text_sending_queue_table"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "insertion_time ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 324
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 328
    .end local v8    # "cursor":Landroid/database/Cursor;
    :goto_0
    monitor-exit p0

    return-object v8

    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_0
    move-object v8, v9

    goto :goto_0

    .line 321
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized increaseConvertingTryCount(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 377
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 378
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->getConvertingTryCount(Ljava/lang/String;)I

    move-result v0

    .line 379
    .local v0, "readCount":I
    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/DB;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "convertingTryCount :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 381
    .local v1, "val":Landroid/content/ContentValues;
    const-string v2, "converting_try_count"

    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 382
    const-string v2, "converting_queue_table"

    invoke-direct {p0, v2, v1, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 384
    .end local v0    # "readCount":I
    .end local v1    # "val":Landroid/content/ContentValues;
    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 377
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized increaseDecodingTryCount(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 366
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 367
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->getDecodingTryCount(Ljava/lang/String;)I

    move-result v0

    .line 368
    .local v0, "readCount":I
    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/DB;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "decodingTryCount :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 370
    .local v1, "val":Landroid/content/ContentValues;
    const-string v2, "decoding_try_count"

    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    const-string v2, "converting_queue_table"

    invoke-direct {p0, v2, v1, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 373
    .end local v0    # "readCount":I
    .end local v1    # "val":Landroid/content/ContentValues;
    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized isAudioConverted(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 340
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 341
    const/4 v1, 0x1

    :try_start_0
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "audio_converted"

    aput-object v2, v0, v1

    .line 342
    .local v0, "column":[Ljava/lang/String;
    const-string v1, "converting_queue_table"

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->isTrue(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 344
    .end local v0    # "column":[Ljava/lang/String;
    :cond_0
    monitor-exit p0

    return v1

    .line 340
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isConvQueueEmpty()Z
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 220
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "count(*) AS count"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 221
    .local v2, "columns":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 222
    .local v8, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 224
    .local v9, "count":I
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/DB;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "converting_queue_table"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 225
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    .line 231
    :cond_0
    if-eqz v8, :cond_1

    .line 232
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 233
    const/4 v8, 0x0

    .line 237
    :cond_1
    :goto_0
    if-ge v9, v11, :cond_3

    move v0, v11

    :goto_1
    monitor-exit p0

    return v0

    .line 228
    :catch_0
    move-exception v10

    .line 229
    .local v10, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_3
    invoke-virtual {v10}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 231
    if-eqz v8, :cond_1

    .line 232
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 233
    const/4 v8, 0x0

    goto :goto_0

    .line 231
    .end local v10    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    .line 232
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 233
    const/4 v8, 0x0

    :cond_2
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 220
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "count":I
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .restart local v2    # "columns":[Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "count":I
    :cond_3
    move v0, v12

    .line 237
    goto :goto_1
.end method

.method public declared-synchronized isDecoded(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 332
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 333
    const/4 v1, 0x1

    :try_start_0
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pcm_decoded"

    aput-object v2, v0, v1

    .line 334
    .local v0, "column":[Ljava/lang/String;
    const-string v1, "converting_queue_table"

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->isTrue(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 336
    .end local v0    # "column":[Ljava/lang/String;
    :cond_0
    monitor-exit p0

    return v1

    .line 332
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isOverMaxTryOfConverting(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->getConvertingTryCount(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 394
    .local v0, "count":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    monitor-exit p0

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 393
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isOverMaxTryOfDecoding(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 388
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->getDecodingTryCount(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 389
    .local v0, "count":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    monitor-exit p0

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 388
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized markAudioConverted(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 357
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 358
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 359
    .local v0, "val":Landroid/content/ContentValues;
    const-string v1, "audio_converted"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 360
    const-string v1, "converting_queue_table"

    invoke-direct {p0, v1, v0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 362
    .end local v0    # "val":Landroid/content/ContentValues;
    :goto_0
    monitor-exit p0

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 357
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized markDecoded(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 348
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 349
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 350
    .local v0, "val":Landroid/content/ContentValues;
    const-string v1, "pcm_decoded"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 351
    const-string v1, "converting_queue_table"

    invoke-direct {p0, v1, v0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 353
    .end local v0    # "val":Landroid/content/ContentValues;
    :goto_0
    monitor-exit p0

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized putConvPath(Ljava/lang/String;)Z
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 241
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    const-string v3, "converting_queue_table"

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->isPathExist(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    .line 257
    :goto_0
    monitor-exit p0

    return v1

    .line 245
    :cond_1
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 246
    .local v0, "val":Landroid/content/ContentValues;
    const-string v3, "_data"

    invoke-virtual {v0, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v3, "pcm_decoded"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248
    const-string v3, "decoding_try_count"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 249
    const-string v3, "audio_converted"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 250
    const-string v3, "converting_try_count"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 251
    const-string v3, "insertion_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 253
    const-string v3, "converting_queue_table"

    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->insert(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 254
    const-string v2, "putConvPath - put succeed"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 241
    .end local v0    # "val":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .restart local v0    # "val":Landroid/content/ContentValues;
    :cond_2
    move v1, v2

    .line 257
    goto :goto_0
.end method

.method public declared-synchronized putTxtSendPath(Ljava/lang/String;)Z
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 301
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    const-string v2, "text_sending_queue_table"

    invoke-direct {p0, v2, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->isPathExist(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 305
    :cond_1
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 306
    .local v0, "val":Landroid/content/ContentValues;
    const-string v2, "_data"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v2, "insertion_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 309
    const-string v2, "text_sending_queue_table"

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/voicenote/library/stt/DB;->insert(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    const-string v1, "putTxtSendPath - put succeed"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    const/4 v1, 0x1

    goto :goto_0

    .line 301
    .end local v0    # "val":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized removeConvPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    const-string v0, "converting_queue_table"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->removePath(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeTxtSendPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    const-string v0, "text_sending_queue_table"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/voicenote/library/stt/DB;->removePath(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
