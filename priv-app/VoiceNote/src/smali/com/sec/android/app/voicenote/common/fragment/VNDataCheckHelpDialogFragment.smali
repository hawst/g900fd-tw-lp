.class public Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNDataCheckHelpDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

.field private mCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;
    .locals 3
    .param p0, "needDataCheck"    # Ljava/lang/String;

    .prologue
    .line 25
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;-><init>()V

    .line 26
    .local v1, "frag":Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "needDataCheck"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 41
    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    .line 44
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 45
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 49
    const/4 v4, -0x1

    .line 50
    .local v4, "stringBodyId":I
    const/4 v5, -0x1

    .line 52
    .local v5, "stringTitleId":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 53
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "needDataCheck"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "needDataCheck":Ljava/lang/String;
    const-string v8, "need_wifi_check"

    if-ne v3, v8, :cond_1

    .line 56
    const v4, 0x7f0b0057

    .line 57
    const v5, 0x7f0b0058

    .line 65
    :goto_0
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0b0021

    new-instance v10, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0b00e0

    new-instance v10, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;

    invoke-direct {v10, p0, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;Ljava/lang/String;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 88
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f03005f

    invoke-virtual {v8, v9, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 89
    .local v2, "layout":Landroid/view/View;
    const v7, 0x7f0e00c3

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 90
    .local v6, "warningText":Landroid/widget/TextView;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 91
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    const v7, 0x7f0e00ef

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 94
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 95
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "layout":Landroid/view/View;
    .end local v6    # "warningText":Landroid/widget/TextView;
    :cond_0
    return-object v7

    .line 58
    :cond_1
    const-string v8, "need_mobile_check"

    if-ne v3, v8, :cond_0

    .line 59
    const v4, 0x7f0b0055

    .line 60
    const v5, 0x7f0b0056

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 110
    :cond_0
    return-void
.end method
