.class public Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;
.super Landroid/app/ListFragment;
.source "VNSettingSkipIntervalFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "VNSettingSkipIntervalFragment"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mIsAttach:Z

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mListView:Landroid/widget/ListView;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mIsAttach:Z

    .line 54
    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 143
    const v1, 0x102000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mListView:Landroid/widget/ListView;

    .line 144
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 145
    .local v0, "v":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 146
    return-void
.end method

.method private listBinding()Z
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const v8, 0x7f0b006c

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 149
    const-string v5, "VNSettingSkipIntervalFragment"

    const-string v6, "listBinding "

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mIsAttach:Z

    if-nez v5, :cond_0

    .line 190
    :goto_0
    return v3

    .line 155
    :cond_0
    const/4 v5, 0x4

    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    sget-object v7, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    aget v7, v7, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    sget-object v7, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    aget v7, v7, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    sget-object v7, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    aget v7, v7, v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    sget-object v7, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    aget v7, v7, v10

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v10

    .line 161
    .local v1, "items":[Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment$1;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v5, 0x7f03005e

    invoke-direct {v0, p0, v3, v5, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment$1;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;Landroid/content/Context;I[Ljava/lang/String;)V

    .line 185
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 187
    const-string v3, "skip_interval_value"

    invoke-static {v3, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v2

    .line 188
    .local v2, "sel":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    move v3, v4

    .line 190
    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mIsAttach:Z

    .line 92
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 93
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    .line 138
    const-string v0, "skip_interval"

    invoke-static {v0, p2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 139
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->listBinding()Z

    .line 140
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x10

    const/4 v4, 0x0

    const/4 v5, -0x2

    .line 58
    const-string v1, "VNSettingSkipIntervalFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v1, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v4, v4, v2, v4}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_1

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0121

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 68
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ur"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x13

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 80
    :goto_0
    const-string v1, "skip_interval"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 81
    .local v0, "setval":Ljava/lang/Boolean;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->setHasOptionsMenu(Z)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 86
    .end local v0    # "setval":Ljava/lang/Boolean;
    :cond_1
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    return-void

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    const v1, 0x7f03001c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 121
    .local v0, "v":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->initView(Landroid/view/View;)V

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->listBinding()Z

    .line 123
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 199
    :cond_0
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 200
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mIsAttach:Z

    .line 98
    invoke-super {p0}, Landroid/app/ListFragment;->onDetach()V

    .line 99
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    const-string v1, "skip_interval"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    const-string v1, "skip_interval_value"

    invoke-static {v1, p3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 107
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const-string v1, "skip_interval_value"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v0

    .line 112
    .local v0, "sel":I
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 128
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 133
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 130
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
