.class public final Lcom/sec/android/app/voicenote/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_switch_padding:I = 0x7f090000

.field public static final action_bar_tab_max_width:I = 0x7f090001

.field public static final action_bar_tab_text_max_width:I = 0x7f090002

.field public static final action_mode_dropdown_anchor_left_margin:I = 0x7f090003

.field public static final action_mode_dropdown_height:I = 0x7f090004

.field public static final action_mode_dropdown_margin_left:I = 0x7f090005

.field public static final action_mode_dropdown_menu_x_offset:I = 0x7f090006

.field public static final action_mode_dropdown_menu_y_offset:I = 0x7f090007

.field public static final action_mode_dropdown_padding_right:I = 0x7f090008

.field public static final action_mode_min_dropdown_width:I = 0x7f090009

.field public static final activity_horizontal_margin:I = 0x7f09000a

.field public static final activity_vertical_margin:I = 0x7f09000b

.field public static final activity_width:I = 0x7f09000c

.field public static final bookmark_listview_selectall_top:I = 0x7f090042

.field public static final canvas_height:I = 0x7f09000d

.field public static final canvas_width:I = 0x7f09000e

.field public static final category_toast_description_left_offset:I = 0x7f09000f

.field public static final category_toast_description_top_offset:I = 0x7f090010

.field public static final colorpicker_offset_min_height:I = 0x7f090011

.field public static final colorpicker_offset_min_width:I = 0x7f090012

.field public static final dialog_rename_layout_margin:I = 0x7f090013

.field public static final drawer_margin_right:I = 0x7f090014

.field public static final drawer_width:I = 0x7f090015

.field public static final dropdown_item_min_width:I = 0x7f090016

.field public static final dropdown_item_side_padding:I = 0x7f090017

.field public static final dropdown_list_item_height:I = 0x7f090018

.field public static final dropdown_list_item_start_padding:I = 0x7f090019

.field public static final dropdown_list_item_text_size:I = 0x7f09001a

.field public static final easy_recorder_controller_btn_height:I = 0x7f09001b

.field public static final easy_recorder_controller_btn_margin:I = 0x7f09001c

.field public static final easy_recorder_controller_height:I = 0x7f09001d

.field public static final easy_recorder_controller_list_btn_width:I = 0x7f09001e

.field public static final easy_recorder_controller_recorder_btn_width:I = 0x7f09001f

.field public static final library_listview_offsetY:I = 0x7f090040

.field public static final list_header_select_all_padding_side:I = 0x7f090020

.field public static final listrow_height:I = 0x7f090021

.field public static final listrow_height_easy:I = 0x7f090022

.field public static final logo_text_padding_rect:I = 0x7f090023

.field public static final logo_text_top_margin:I = 0x7f090024

.field public static final logo_text_width:I = 0x7f090025

.field public static final mic_mask_max_value:I = 0x7f090026

.field public static final mic_width:I = 0x7f090027

.field public static final nbest_play_dilog_position_y_offset:I = 0x7f090028

.field public static final nbest_recording_dilog_position_y_offset:I = 0x7f090029

.field public static final no_item_bg_top_margin:I = 0x7f09002a

.field public static final no_item_bg_top_margin_no_search:I = 0x7f09002b

.field public static final panel_category_height:I = 0x7f09002c

.field public static final panel_category_width:I = 0x7f09002d

.field public static final player_easy_progress_layout:I = 0x7f09002e

.field public static final player_easy_progress_layout_margin_top:I = 0x7f09002f

.field public static final player_easy_toolbar:I = 0x7f090030

.field public static final player_progress_layout:I = 0x7f090031

.field public static final player_toolbar:I = 0x7f090032

.field public static final quick_panel_control_icon_height:I = 0x7f090033

.field public static final quick_panel_control_icon_width:I = 0x7f090034

.field public static final quick_panel_launcher_icon_height:I = 0x7f090035

.field public static final quick_panel_launcher_icon_width:I = 0x7f090036

.field public static final searchview_width:I = 0x7f090037

.field public static final seekbar_atob_icon_diameter:I = 0x7f090038

.field public static final showdow_y:I = 0x7f090041

.field public static final speaker_base_height:I = 0x7f090039

.field public static final speaker_point_height:I = 0x7f09003a

.field public static final sttview_line_margine:I = 0x7f09003b

.field public static final trim_empty_handler_offset:I = 0x7f09003c

.field public static final widget_app_provider_minhight:I = 0x7f09003d

.field public static final widget_app_provider_minwidth:I = 0x7f09003e

.field public static final widget_app_provider_timer_textsize:I = 0x7f09003f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
