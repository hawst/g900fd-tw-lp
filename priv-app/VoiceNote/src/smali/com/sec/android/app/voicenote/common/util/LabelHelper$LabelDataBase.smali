.class public Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "LabelHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/LabelHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LabelDataBase"
.end annotation


# static fields
.field public static final COLOR:Ljava/lang/String; = "COLOR"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final LabelDBName:Ljava/lang/String; = "label.db"

.field public static final TITLE:Ljava/lang/String; = "TITLE"

.field public static final _ID:Ljava/lang/String; = "_id"

.field public static final _TABLENAME:Ljava/lang/String; = "labels"


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/LabelHelper;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/common/util/LabelHelper;Landroid/content/Context;)V
    .locals 3
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 248
    const-string v0, "label.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 250
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 255
    const-string v1, "LabelHelper"

    const-string v2, "Create DB"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v1, "CREATE TABLE labels (_id INTEGER PRIMARY KEY AUTOINCREMENT, COLOR INTEGER,TITLE TEXT);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    .local v0, "builder1":Ljava/lang/StringBuilder;
    const-string v1, "INSERT INTO labels VALUES(0, \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    # getter for: Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->access$000(Lcom/sec/android/app/voicenote/common/util/LabelHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    # getter for: Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->access$000(Lcom/sec/android/app/voicenote/common/util/LabelHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0029

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 266
    const-string v1, "category_label_id"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 268
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 272
    const-string v0, "DROP TABLE IF EXISTS labels"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 274
    return-void
.end method
