.class Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;
.super Ljava/lang/Thread;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EQThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2773
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/main/VNMainActivity$1;

    .prologue
    .line 2773
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2777
    const/4 v3, 0x1

    .line 2779
    .local v3, "run":Z
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->isInterrupted()Z

    move-result v5

    if-nez v5, :cond_0

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-nez v5, :cond_1

    .line 2780
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2902(Lcom/sec/android/app/voicenote/main/VNMainActivity;I)I

    .line 2807
    :goto_1
    return-void

    .line 2784
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getMaxAmplitude()I

    move-result v4

    .line 2785
    .local v4, "tmpMaxAmplitude":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    int-to-long v6, v4

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setMaxAmplitude(J)J

    move-result-wide v6

    long-to-int v6, v6

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUVThreshold:I
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3002(Lcom/sec/android/app/voicenote/main/VNMainActivity;I)I

    .line 2786
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I
    invoke-static {v5, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2902(Lcom/sec/android/app/voicenote/main/VNMainActivity;I)I

    .line 2788
    sget-object v6, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2789
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2790
    const/4 v3, 0x0

    .line 2794
    :goto_2
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2795
    if-eqz v3, :cond_2

    .line 2796
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v6, 0x1

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateEQ(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3600(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)V

    .line 2798
    :cond_2
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 2800
    .end local v4    # "tmpMaxAmplitude":I
    :catch_0
    move-exception v1

    .line 2801
    .local v1, "ne":Ljava/lang/NullPointerException;
    const-string v5, "VoiceNoteMainActivity"

    const-string v6, "ignore EQ update message after destroyed : "

    invoke-static {v5, v6, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2792
    .end local v1    # "ne":Ljava/lang/NullPointerException;
    .restart local v4    # "tmpMaxAmplitude":I
    :cond_3
    const/4 v3, 0x1

    goto :goto_2

    .line 2794
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    .line 2802
    .end local v4    # "tmpMaxAmplitude":I
    :catch_1
    move-exception v2

    .line 2803
    .local v2, "re":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 2804
    .end local v2    # "re":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 2805
    .local v0, "ie":Ljava/lang/InterruptedException;
    const-string v5, "VoiceNoteMainActivity"

    const-string v6, "EQThread InterruptedException"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
