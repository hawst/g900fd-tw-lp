.class public Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNPolicyInfoDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNPolicyInfoDialogFragment"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 146
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 147
    const v6, 0x7f0e00c3

    invoke-virtual {v0, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 149
    .local v5, "warningText":Landroid/widget/TextView;
    const v6, 0x7f0b0095

    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 150
    .local v4, "voicememo_msg":Ljava/lang/String;
    const v6, 0x7f0b0170

    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "voicememo_link1":Ljava/lang/String;
    const v6, 0x7f0b0173

    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 152
    .local v2, "voicememo_link2":Ljava/lang/String;
    const v6, 0x7f0b0174

    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 154
    .local v3, "voicememo_link3":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<a href=\"sherif-activity://nuanceinfo\">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</a>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v1, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 155
    const-string v6, "ro.csc.country_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "KOREA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ko_KR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 156
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<a href=\"sherif-activity://samsunginfo\">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</a>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 160
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<a href=http://www.vlingo.com/wap/samsung-asr-privacy-addendum/en>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</a>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 161
    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 164
    .end local v1    # "voicememo_link1":Ljava/lang/String;
    .end local v2    # "voicememo_link2":Ljava/lang/String;
    .end local v3    # "voicememo_link3":Ljava/lang/String;
    .end local v4    # "voicememo_msg":Ljava/lang/String;
    .end local v5    # "warningText":Landroid/widget/TextView;
    :cond_0
    return-void

    .line 158
    .restart local v1    # "voicememo_link1":Ljava/lang/String;
    .restart local v2    # "voicememo_link2":Ljava/lang/String;
    .restart local v3    # "voicememo_link3":Ljava/lang/String;
    .restart local v4    # "voicememo_msg":Ljava/lang/String;
    .restart local v5    # "warningText":Landroid/widget/TextView;
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<a href= http://account.samsung.com/legal/pp>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</a>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;

    .line 62
    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 63
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    .line 65
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 66
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    .line 71
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030042

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 73
    .local v1, "layout":Landroid/view/View;
    const v7, 0x7f0e00c3

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 75
    .local v6, "warningText":Landroid/widget/TextView;
    const v7, 0x7f0b0094

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 78
    const v7, 0x7f0b0095

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 79
    .local v5, "voicememo_msg":Ljava/lang/String;
    const v7, 0x7f0b0170

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "voicememo_link1":Ljava/lang/String;
    const v7, 0x7f0b0173

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 81
    .local v3, "voicememo_link2":Ljava/lang/String;
    const v7, 0x7f0b0174

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "voicememo_link3":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<a href=\"sherif-activity://nuanceinfo\">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</a>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v2, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 84
    const-string v7, "ro.csc.country_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "KOREA"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ko_KR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 85
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<a href=\"sherif-activity://samsunginfo\">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</a>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 89
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<a href=http://www.vlingo.com/wap/samsung-asr-privacy-addendum/en>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</a>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v4, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 90
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 93
    const v7, 0x7f0b0011

    new-instance v8, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 127
    const v7, 0x7f0b005b

    invoke-virtual {v0, v7, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7

    .line 87
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<a href= http://account.samsung.com/legal/pp>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</a>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x100

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 136
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
