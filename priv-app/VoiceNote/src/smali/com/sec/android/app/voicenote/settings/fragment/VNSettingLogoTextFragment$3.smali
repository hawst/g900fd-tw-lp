.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;
.super Ljava/lang/Object;
.source "VNSettingLogoTextFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onColorChanged(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I
    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$302(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;I)I

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$400(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)[Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0xf

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$500(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$600(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)[I

    move-result-object v1

    array-length v1, v1

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$700(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;I)V

    .line 316
    return-void
.end method
