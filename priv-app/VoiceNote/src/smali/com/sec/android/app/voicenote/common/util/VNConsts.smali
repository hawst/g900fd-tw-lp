.class public final Lcom/sec/android/app/voicenote/common/util/VNConsts;
.super Ljava/lang/Object;
.source "VNConsts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/VNConsts$ButtonType;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$ActivityState;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$AddBookmarkResult;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$KNOX_ERROR;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$STT;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$WakeLock;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$SECRETFILE;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$VNFROM;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$MemoThumbnail;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$PlaySpeed;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$Logo_Select;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$SortMode;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$RecordVolume;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$RecordMode;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$LibraryMode;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$UIUpdate;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$RecorderCommand;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$MediaRecorderState;,
        Lcom/sec/android/app/voicenote/common/util/VNConsts$BroadcastMessage;
    }
.end annotation


# static fields
.field public static final ACTION_MEDIA_SCAN:Ljava/lang/String; = "com.samsung.MEDIA_SCAN"

.field public static final BUTTON_STATE:Ljava/lang/String; = "button"

.field public static final CATEGORY_ALL:J = -0x1L

.field public static final DEFAULT_ID:J = -0x1L

.field public static final DESCRIPTION_KEY:Ljava/lang/String; = "description"

.field public static final DEST_FRAGMENT_ID:Ljava/lang/String; = "desrt_fragment_id"

.field public static final FEATURE_INSERT_LOG_CHANGECATEGORY:Ljava/lang/String; = "CHAN"

.field public static final FEATURE_INSERT_LOG_CONVERSATION:Ljava/lang/String; = "CONV"

.field public static final FEATURE_INSERT_LOG_INTERVIEW:Ljava/lang/String; = "INTE"

.field public static final FEATURE_INSERT_LOG_MANAGECATEGORIES:Ljava/lang/String; = "MANA"

.field public static final FEATURE_INSERT_LOG_NORMAL:Ljava/lang/String; = "NORM"

.field public static final FEATURE_INSERT_LOG_PRIVATEMODE:Ljava/lang/String; = "MOVP"

.field public static final FEATURE_INSERT_LOG_REC:Ljava/lang/String; = "RECO"

.field public static final FEATURE_INSERT_LOG_SAVE:Ljava/lang/String; = "SAVE"

.field public static final FEATURE_INSERT_LOG_TRIM:Ljava/lang/String; = "TRIM"

.field public static final FEATURE_INSERT_LOG_VOICEMEMO:Ljava/lang/String; = "MEMO"

.field public static final FILED_ADDR:Ljava/lang/String; = "addr"

.field public static final FILED_LABEL:Ljava/lang/String; = "label_id"

.field public static final FILED_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final FILED_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final FILED_RECORDINGTYPE:Ljava/lang/String; = "recordingtype"

.field public static final FILED_SECRETBOX:Ljava/lang/String; = "is_secretbox"

.field public static final FILE_OPERATONI_COPY:I = 0x0

.field public static final FILE_OPERATONI_DELETE:I = 0x2

.field public static final FILE_OPERATONI_MOVE:I = 0x1

.field public static final FILE_TO_PLAY:Ljava/lang/String; = "file_to_play"

.field public static final FRAGMENT_CHANGEMULTICATEGORY:Ljava/lang/String; = "FRAGMENT_MULTI_CATEGORY"

.field public static final FRAGMENT_DIALOG:Ljava/lang/String; = "FRAGMENT_DIALOG"

.field public static final FRAGMENT_DIALOG_DO_NOT_BOOKMARK:Ljava/lang/String; = "FRAGMENT_DO_NOT_BOOKMARK"

.field public static final FRAGMENT_DIALOG_FILEOPERATION:Ljava/lang/String; = "FRAGMENT_FILE"

.field public static final FRAGMENT_DIALOG_FILEORENAME:Ljava/lang/String; = "FRAGMENT_DIALOG_FILEORENAME"

.field public static final FRAGMENT_DIALOG_SECOND:Ljava/lang/String; = "FRAGMENT_DIALOG_2"

.field public static final FRAGMENT_DIALOG_SELECT_BEST:Ljava/lang/String; = "FRAGMENT_DIALOG_SELECT_BEST"

.field public static final FRAGMENT_DIALOG_SETAS:Ljava/lang/String; = "FRAGMENT_DIALOG_SETAS"

.field public static final FRAGMENT_LOGO:Ljava/lang/String; = "FRAGMENT_LOGO"

.field public static final FRAGMENT_PRIVATE_SELECT:Ljava/lang/String; = "FRAGMENT_PRIVATE_SELECT"

.field public static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field public static final ITEM_POSITION_KEY:Ljava/lang/String; = "position"

.field public static final LAST_SAVED_FILE_ID:Ljava/lang/String; = "last_saved_file_id"

.field public static final LOGO_FILE_NAME:Ljava/lang/String; = "logo_image.jpg"

.field public static final MAX_DESCRIPTION_WORDS:I = 0x8c

.field public static final MAX_LENGTH:I = 0x2e

.field public static final MAX_PATH_LENGTH:I = 0x100

.field public static final MEMO_FILE_NAME:Ljava/lang/String; = "voice_memo.png"

.field public static final MIMETYPE_3GPP:Ljava/lang/String; = "audio/3gpp"

.field public static final MIMETYPE_AMR:Ljava/lang/String; = "audio/amr"

.field public static final MIMETYPE_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIN_RECORD_LENGTH:I = 0x2800

.field public static final NEED_MOBILE_CHECK:Ljava/lang/String; = "need_mobile_check"

.field public static final NEED_WIFI_CHECK:Ljava/lang/String; = "need_wifi_check"

.field public static final NFC_DBKEY:Ljava/lang/String; = "year_name"

.field public static final NFC_DIFFERENT_DEVICE:I = -0x2

.field public static final NFC_FILE_DOES_NOT_EXIST:I = -0x1

.field public static final NFC_TAGED:Ljava/lang/String; = "NFC"

.field public static final PERSONAL_PAGE_MOVE_TO_PERSONAL:I = 0x0

.field public static final PERSONAL_PAGE_REMOVE_FROM_PERSONAL:I = 0x1

.field public static final PERSONAL_SELECT_DUPILCATION_CANCEL:I = 0x0

.field public static final PERSONAL_SELECT_DUPILCATION_INIT:I = -0x1

.field public static final PERSONAL_SELECT_DUPILCATION_RENAME:I = 0x2

.field public static final PERSONAL_SELECT_DUPILCATION_REPLACE:I = 0x1

.field public static final PERSONAL_SELECT_TYPE_OF_ITEM_BOTH:I = 0x2

.field public static final PERSONAL_SELECT_TYPE_OF_ITEM_NONE:I = -0x1

.field public static final PERSONAL_SELECT_TYPE_OF_ITEM_NORMAL:I = 0x0

.field public static final PERSONAL_SELECT_TYPE_OF_ITEM_PERSONAL:I = 0x1

.field public static final PERSONAL_SELECT_TYPE_OF_ITEM_READY:I = 0x3

.field public static final PLAY_TRACK_FROM_MAP:I = 0x42e

.field public static final RENAME_MAX_LENGTH:I = 0x32

.field public static final REQUEST_BOKMARK_DESCRIPTION:I = 0xc1d

.field public static final REQUEST_CODE_MANAGE_CATEGORY:I = 0x186a1

.field public static final REQUEST_CODE_PLAY_RECORDING:I = 0x186a2

.field public static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field public static final RESULT_END:I = 0xbb8

.field public static final RESULT_RECORD:I = 0xbb9

.field public static final SD_SLOT_AVAILABLE:I = 0x1

.field public static final SD_SLOT_UNAVAILABLE:I = 0x2

.field public static final SD_SLOT_UNKNOWN:I = 0x0

.field public static final SEARCH_KEYPAD:Ljava/lang/String; = "keypad"

.field public static final SEARCH_STATE:Ljava/lang/String; = "searchOn"

.field public static final SEARCH_TEXT:Ljava/lang/String; = "searchText"

.field public static final SRC_FRAGMENT_ID:Ljava/lang/String; = "src_fragment_id"

.field public static final TRANSITION_DURATION:I = 0x1f4

.field public static final TRIM_TURN_OFF_THRESHOLD:I = 0x64

.field public static final VOICEMEMO_LENGTH:I = 0x494a8

.field public static final WARNING_SIZE:I = 0x3e80


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    return-void
.end method
