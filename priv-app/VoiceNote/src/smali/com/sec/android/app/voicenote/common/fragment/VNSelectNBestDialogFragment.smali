.class public Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNSelectNBestDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
    }
.end annotation


# static fields
.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final KEY_EDITTEXT_SELECTION:Ljava/lang/String; = "key_edittext_selection"

.field public static final MAX_LENGTH:I = 0x32

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final TAG:Ljava/lang/String; = "VNSelectNBestDialogFragment"

.field private static mCurrentWord:Ljava/lang/String;


# instance fields
.field private bShowKeyboard:Z

.field private mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

.field private mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

.field private mDeleteButton:Landroid/widget/ImageButton;

.field private mEditTextSelection:[I

.field private mEditTextView:Landroid/widget/EditText;

.field private mIsSaveEditText:Z

.field private mListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mReceiveTime:J

.field private mTextList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mViewChild:Landroid/view/View;

.field private yloc:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCurrentWord:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 98
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 71
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    .line 72
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;

    .line 73
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    .line 74
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mTextList:Ljava/util/List;

    .line 75
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    .line 76
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mIsSaveEditText:Z

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 79
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mReceiveTime:J

    .line 81
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextSelection:[I

    .line 83
    iput v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->yloc:I

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    .param p1, "x1"    # J

    .prologue
    .line 65
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mReceiveTime:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->yloc:I

    return v0
.end method

.method public static getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "isIncludeCount"    # Z
    .param p2, "fixedMaxLength"    # I

    .prologue
    .line 357
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 359
    .local v0, "filterArray":[Landroid/text/InputFilter;
    const/16 v1, 0x32

    .line 360
    .local v1, "max_length":I
    if-eqz p1, :cond_0

    .line 361
    const/16 v2, 0x32

    if-ge v2, p2, :cond_1

    .line 362
    move v1, p2

    .line 367
    :cond_0
    :goto_0
    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$3;

    invoke-direct {v3, v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$3;-><init>(ILandroid/app/Activity;)V

    aput-object v3, v0, v2

    .line 380
    return-object v0

    .line 364
    :cond_1
    const/16 v1, 0x32

    goto :goto_0
.end method

.method private initView()V
    .locals 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e0030

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    const-string v1, "disableVoiceInput=true"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 190
    sget-object v0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCurrentWord:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    sget-object v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCurrentWord:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCurrentWord:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    :cond_3
    return-void
.end method

.method private listBinding()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 229
    const-string v0, "VNSelectNBestDialogFragment"

    const-string v1, "listBinding E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mTextList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f03005d

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mTextList:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 237
    :goto_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->registerBroadcastReceiverSip(Z)V

    .line 238
    return v4

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method private saveeditTextSelection()V
    .locals 3

    .prologue
    .line 303
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 305
    const/4 v1, 0x2

    new-array v0, v1, [I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    aput v2, v0, v1

    .line 308
    .local v0, "selection":[I
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextSelection:[I

    .line 310
    .end local v0    # "selection":[I
    :cond_0
    return-void
.end method


# virtual methods
.method public getCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 169
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 170
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 324
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 325
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 333
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    if-nez v2, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 336
    .local v1, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0030
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 161
    const-string v0, "VNSelectNBestDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    const v1, 0x7f0c0001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setStyle(II)V

    .line 164
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 165
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v11, 0x50

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 117
    const v7, 0x7f030011

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mViewChild:Landroid/view/View;

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/view/Window;->setGravity(I)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v7, v8}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->initView()V

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->listBinding()Z

    .line 126
    if-eqz p3, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    move-result-object v7

    if-nez v7, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "VNEditSTTFragment"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 129
    .local v0, "f":Landroid/app/Fragment;
    if-eqz v0, :cond_2

    move-object v2, v0

    .line 130
    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .line 131
    .local v2, "mEditSttFragment":Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;)V

    .line 140
    .end local v0    # "f":Landroid/app/Fragment;
    .end local v2    # "mEditSttFragment":Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    :cond_0
    :goto_0
    const-string v7, "yloc"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 141
    .local v6, "y":I
    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setYLocation(I)V

    .line 144
    .end local v6    # "y":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "input_method"

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 146
    .local v3, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v7

    if-nez v7, :cond_3

    .line 147
    iput-boolean v10, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    .line 152
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/view/Window;->setGravity(I)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 154
    .local v5, "params":Landroid/view/WindowManager$LayoutParams;
    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 156
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mViewChild:Landroid/view/View;

    return-object v7

    .line 133
    .end local v3    # "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    .end local v5    # "params":Landroid/view/WindowManager$LayoutParams;
    .restart local v0    # "f":Landroid/app/Fragment;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const v8, 0x7f0e000e

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 134
    .local v1, "f2":Landroid/app/Fragment;
    if-eqz v1, :cond_0

    instance-of v7, v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    if-eqz v7, :cond_0

    move-object v4, v1

    .line 135
    check-cast v4, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 136
    .local v4, "mSTTFragment":Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;)V

    goto :goto_0

    .line 149
    .end local v0    # "f":Landroid/app/Fragment;
    .end local v1    # "f2":Landroid/app/Fragment;
    .end local v4    # "mSTTFragment":Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .restart local v3    # "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_3
    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 243
    const-string v0, "VNSelectNBestDialogFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mIsSaveEditText:Z

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;->onResult(Ljava/lang/String;Z)V

    .line 249
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mIsSaveEditText:Z

    .line 252
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mTextList:Ljava/util/List;

    .line 253
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;

    .line 254
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    .line 255
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mViewChild:Landroid/view/View;

    .line 256
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-eqz v0, :cond_2

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;->onFinish()V

    .line 261
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->registerBroadcastReceiverSip(Z)V

    .line 262
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 263
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 437
    const-string v0, "VNSelectNBestDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEditorAction: done key pressed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/4 v0, 0x6

    if-eq v0, p2, :cond_0

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-ne v0, v1, :cond_1

    .line 439
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mIsSaveEditText:Z

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->dismiss()V

    .line 443
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "clickedView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 314
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mIsSaveEditText:Z

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-eqz v0, :cond_0

    .line 316
    const-string v0, "VNSelectNBestDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onItemClick:pos is"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    invoke-interface {v0, p3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;->onResult(I)V

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->dismiss()V

    .line 320
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->saveeditTextSelection()V

    .line 299
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 300
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 267
    const-string v0, "VNSelectNBestDialogFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextSelection:[I

    if-eqz v0, :cond_0

    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextSelection:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextSelection:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 290
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_2

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 294
    :cond_2
    return-void

    .line 274
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 174
    const-string v0, "yloc"

    iget v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->yloc:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 176
    return-void
.end method

.method public registerBroadcastReceiverSip(Z)V
    .locals 6
    .param p1, "register"    # Z

    .prologue
    .line 384
    if-eqz p1, :cond_2

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    .line 420
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 421
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 423
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 425
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mReceiveTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x190

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 426
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z

    .line 428
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 430
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method public setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 347
    return-void
.end method

.method public final setNBestStrings([Ljava/lang/String;)V
    .locals 4
    .param p1, "NBestStrings"    # [Ljava/lang/String;

    .prologue
    .line 102
    const/4 v1, 0x0

    .line 103
    .local v1, "size":I
    const/4 v2, 0x0

    aget-object v2, p1, v2

    sput-object v2, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mCurrentWord:Ljava/lang/String;

    .line 104
    :goto_0
    aget-object v2, p1, v1

    if-eqz v2, :cond_0

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    :cond_0
    const/4 v2, 0x1

    if-ge v2, v1, :cond_1

    .line 108
    new-instance v2, Ljava/util/ArrayList;

    add-int/lit8 v3, v1, -0x1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mTextList:Ljava/util/List;

    .line 109
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mTextList:Ljava/util/List;

    aget-object v3, p1, v0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public setYLocation(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->yloc:I

    .line 91
    return-void
.end method
