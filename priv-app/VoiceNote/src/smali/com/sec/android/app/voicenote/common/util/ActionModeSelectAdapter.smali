.class public Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ActionModeSelectAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ActionModeSelectAdapter"

.field private static final mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final mDropDownLayoutId:I = 0x7f03002c

.field private static final mLayoutId:I = 0x7f03002a


# instance fields
.field private mSelected:I

.field private mTotalItems:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->initSelect()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "total"    # I

    .prologue
    const/4 v2, 0x0

    .line 34
    const v0, 0x7f03002a

    const v1, 0x7f0e005a

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 21
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    .line 22
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mTotalItems:I

    .line 35
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->setDropDownViewResource(I)V

    .line 36
    iput p2, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mTotalItems:I

    .line 37
    return-void
.end method

.method private static initSelect()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 27
    const v1, 0x7f0b011b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    const v1, 0x7f0b0116

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    const v1, 0x7f0b015f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    return-object v0
.end method


# virtual methods
.method public getCheckItemCount()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mTotalItems:I

    if-ne v0, v1, :cond_1

    .line 87
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    .line 89
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 57
    if-nez p2, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 59
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03002c

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 62
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const/4 v1, 0x0

    .line 63
    .local v1, "resId":I
    iget v3, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    if-nez v3, :cond_1

    .line 64
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 70
    :goto_0
    const v3, 0x7f0e0063

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 71
    .local v2, "tv":Landroid/widget/TextView;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    return-object p2

    .line 65
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_1
    iget v3, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    iget v4, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mTotalItems:I

    if-ne v3, v4, :cond_2

    .line 66
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 68
    :cond_2
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 41
    if-nez p2, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 44
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03002a

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 47
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    sget-object v4, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 48
    .local v1, "resId":I
    const v4, 0x7f0e005a

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 49
    .local v3, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setSelected(Z)V

    .line 51
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    return-object p2
.end method

.method public setCheckItemCount(II)V
    .locals 1
    .param p1, "selected"    # I
    .param p2, "total"    # I

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    if-eq v0, p1, :cond_0

    .line 78
    iput p1, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mSelected:I

    .line 79
    iput p2, p0, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->mTotalItems:I

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ActionModeSelectAdapter;->notifyDataSetChanged()V

    .line 82
    :cond_0
    return-void
.end method
