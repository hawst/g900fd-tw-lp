.class Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1$1;
.super Ljava/lang/Object;
.source "FileDeleteOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x3

    .line 228
    const-string v0, "FileDeleteOperation"

    const-string v1, "DeleteFiles : Cancel interrupt!!!"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    # getter for: Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->access$300()Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->interrupt()V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->access$400(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->access$400(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 232
    return-void
.end method
