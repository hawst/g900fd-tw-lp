.class public Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNMultiShareViaSTTSelectDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment$ShareViaType;
    }
.end annotation


# static fields
.field public static final MODE_STTFILE_ONLY:I = 0x1

.field public static final MODE_STT_VOICE_FILE:I = 0x3

.field public static final MODE_VOICEFILE_ONLY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VNShareViaSTT"


# instance fields
.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 57
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e0036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 86
    return-void
.end method

.method private listBinding()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 90
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    .line 92
    .local v1, "items":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b016c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 94
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b016f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 96
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030023

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 98
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 99
    return v5
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->listBinding()Z

    .line 80
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b011a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030016

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->initView()V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 72
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 107
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 109
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "VNMultiShareViaSTTSelectDialogFragment"

    long-to-int v3, p4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 110
    invoke-virtual {v0, v4, v4, v1}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 114
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;->dismissAllowingStateLoss()V

    .line 115
    return-void
.end method
