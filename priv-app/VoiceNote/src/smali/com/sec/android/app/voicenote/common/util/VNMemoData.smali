.class public Lcom/sec/android/app/voicenote/common/util/VNMemoData;
.super Ljava/lang/Object;
.source "VNMemoData.java"


# static fields
.field private static final VNMEMO_END:I = 0x1

.field private static final VNMEMO_START:I

.field private static mHasSTTData:Z

.field private static mState:I

.field private static mSttData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    sput v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mState:I

    .line 34
    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mHasSTTData:Z

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mContext:Landroid/content/Context;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mContext:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static addSttData(Lcom/sec/android/app/voicenote/main/common/VNSTTData;Z)V
    .locals 9
    .param p0, "sd"    # Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .param p1, "errorlog"    # Z

    .prologue
    const/4 v8, 0x1

    .line 173
    if-eqz p0, :cond_0

    sget v5, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mState:I

    if-eqz v5, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    array-length v0, v5

    .line 178
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_4

    .line 179
    new-instance v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    invoke-direct {v4}, Lcom/sec/android/app/voicenote/common/util/TextData;-><init>()V

    .line 180
    .local v4, "temp":Lcom/sec/android/app/voicenote/common/util/TextData;
    if-eqz p1, :cond_2

    .line 181
    iput v8, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    .line 186
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v1

    iget-wide v6, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->dConfidenceScore:D

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->ConfidenceScore:D

    .line 187
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v1

    iget-wide v6, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    .line 188
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v1

    iget-wide v6, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 189
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v1

    iget-wide v6, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->duration:J

    .line 191
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 192
    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    if-ge v2, v3, :cond_3

    .line 193
    iget-object v6, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v2

    .line 192
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 183
    .end local v2    # "j":I
    .end local v3    # "size":I
    :cond_2
    const/4 v5, 0x0

    iput v5, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    .line 184
    sput-boolean v8, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mHasSTTData:Z

    goto :goto_2

    .line 195
    .restart local v2    # "j":I
    .restart local v3    # "size":I
    :cond_3
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 197
    .end local v2    # "j":I
    .end local v3    # "size":I
    .end local v4    # "temp":Lcom/sec/android/app/voicenote/common/util/TextData;
    :cond_4
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static getSttData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static setSttData(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "sttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    .line 166
    return-void
.end method


# virtual methods
.method public addBookmark(Lcom/sec/android/app/voicenote/common/util/Bookmark;)I
    .locals 4
    .param p1, "bookmark"    # Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .prologue
    .line 84
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_0

    .line 85
    const/4 v2, 0x1

    .line 93
    :goto_0
    return v2

    .line 87
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 88
    .local v0, "bk":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 89
    const/4 v2, 0x2

    goto :goto_0

    .line 92
    .end local v0    # "bk":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public cancelMemoData()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mState:I

    .line 77
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mHasSTTData:Z

    .line 78
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 81
    :cond_0
    return-void
.end method

.method public getBookmarkSize()I
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public hasSTTdata()Z
    .locals 1

    .prologue
    .line 110
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mHasSTTData:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initBookmark()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 52
    :cond_0
    return-void
.end method

.method public initMemoData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    sput v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mState:I

    .line 65
    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mHasSTTData:Z

    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 73
    :cond_0
    return-void
.end method

.method public saveMemoData(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mState:I

    .line 121
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;-><init>(Lcom/sec/android/app/voicenote/common/util/VNMemoData;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 162
    return-void
.end method
