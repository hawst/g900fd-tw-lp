.class Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;
.super Landroid/os/AsyncTask;
.source "VNKNOXOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KNOX2OperationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;

    .prologue
    .line 387
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 387
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 390
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const-string v9, "rcp"

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/RCPManager;

    .line 391
    .local v2, "mRCP":Landroid/os/RCPManager;
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "com.sec.knox.container.FileRelayExist"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 392
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXUserId:I
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I

    move-result v7

    .line 393
    .local v7, "userId":I
    const/4 v5, 0x0

    .line 394
    .local v5, "successCnt":I
    const/4 v6, 0x0

    .line 396
    .local v6, "targetcount":I
    if-eqz v2, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v8, v8, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v8, :cond_2

    .line 397
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v8, v8, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 398
    .local v3, "path":Ljava/lang/String;
    move-object v0, v3

    .line 399
    .local v0, "destFilePath":Ljava/lang/String;
    const/4 v4, -0x1

    .line 401
    .local v4, "result":I
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "com.sec.knox.container.FileRelayStart"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 408
    if-eqz v4, :cond_0

    .line 409
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "com.sec.knox.container.FileRelayFail2"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, ""

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 416
    :goto_1
    const-wide/16 v8, 0xc8

    invoke-static {v8, v9}, Landroid/os/SystemClock;->sleep(J)V

    .line 422
    add-int/lit8 v6, v6, 0x1

    .line 423
    goto :goto_0

    .line 411
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v8, v8, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetIDs:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v9, v10, v11, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->updateDBinKNOX(Landroid/content/Context;JLjava/lang/String;)Z

    .line 413
    add-int/lit8 v5, v5, 0x1

    .line 414
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "com.sec.knox.container.FileRelayDone"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_1

    .line 424
    .end local v0    # "destFilePath":Ljava/lang/String;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "result":I
    :cond_1
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "com.sec.knox.container.FileRelayComplete"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 427
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v8, 0x0

    return-object v8
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 17
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 432
    invoke-super/range {p0 .. p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 434
    const/4 v2, 0x0

    .line 435
    .local v2, "action":Ljava/lang/String;
    const/4 v11, 0x0

    .line 436
    .local v11, "strValue":Ljava/lang/String;
    const/4 v8, 0x0

    .line 438
    .local v8, "intValue":I
    if-eqz p1, :cond_1

    .line 439
    move-object/from16 v0, p1

    array-length v13, v0

    packed-switch v13, :pswitch_data_0

    .line 457
    :cond_0
    :goto_0
    :pswitch_0
    if-nez v2, :cond_4

    .line 510
    :cond_1
    :goto_1
    return-void

    .line 443
    :pswitch_1
    const/4 v13, 0x1

    aget-object v13, p1, v13

    instance-of v13, v13, Ljava/lang/String;

    if-eqz v13, :cond_3

    .line 444
    const/4 v13, 0x1

    aget-object v11, p1, v13

    .end local v11    # "strValue":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 449
    .restart local v11    # "strValue":Ljava/lang/String;
    :cond_2
    :goto_2
    :pswitch_2
    const/4 v13, 0x0

    aget-object v13, p1, v13

    instance-of v13, v13, Ljava/lang/String;

    if-eqz v13, :cond_0

    .line 450
    const/4 v13, 0x0

    aget-object v2, p1, v13

    .end local v2    # "action":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .restart local v2    # "action":Ljava/lang/String;
    goto :goto_0

    .line 445
    :cond_3
    const/4 v13, 0x1

    aget-object v13, p1, v13

    instance-of v13, v13, Ljava/lang/Integer;

    if-eqz v13, :cond_2

    .line 446
    const/4 v13, 0x1

    aget-object v13, p1, v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_2

    .line 461
    :cond_4
    const-string v13, "com.sec.knox.container.FileRelayExist"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 463
    const-string v13, "com.sec.knox.container.FileRelayStart"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 464
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v13, v13, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-le v13, v14, :cond_1

    .line 465
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    const/4 v14, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I
    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)I

    .line 466
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    goto :goto_1

    .line 468
    :cond_5
    const-string v13, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 469
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v13, v13, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-le v13, v14, :cond_1

    .line 470
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # operator++ for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mMovedCount:I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$008(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I

    .line 471
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    const/16 v14, 0x64

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I
    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)I

    .line 472
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    goto/16 :goto_1

    .line 474
    :cond_6
    const-string v13, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 475
    move v5, v8

    .line 476
    .local v5, "errorcode":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->errorDisplay(I)V
    invoke-static {v13, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)V

    goto/16 :goto_1

    .line 477
    .end local v5    # "errorcode":I
    :cond_7
    const-string v13, "com.sec.knox.container.FileRelayFail2"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 478
    move-object v4, v11

    .line 479
    .local v4, "error":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->errorDisplay20(Ljava/lang/String;)V
    invoke-static {v13, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 480
    .end local v4    # "error":Ljava/lang/String;
    :cond_8
    const-string v13, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 481
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I
    invoke-static {v13, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)I

    .line 482
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    goto/16 :goto_1

    .line 483
    :cond_9
    const-string v13, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 484
    move v3, v8

    .line 486
    .local v3, "cnt":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const-string v14, "persona"

    invoke-virtual {v13, v14}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PersonaManager;

    .line 487
    .local v9, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v9}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v10

    .line 488
    .local v10, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    const/4 v1, 0x0

    .line 490
    .local v1, "KNOXTitle":Ljava/lang/String;
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PersonaInfo;

    .line 491
    .local v7, "info":Landroid/content/pm/PersonaInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXTargetId:I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I

    move-result v13

    invoke-virtual {v7}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v14

    if-ne v13, v14, :cond_a

    goto :goto_3

    .line 496
    .end local v7    # "info":Landroid/content/pm/PersonaInfo;
    :cond_b
    const/4 v12, 0x0

    .line 497
    .local v12, "toastText":Ljava/lang/String;
    const/4 v13, 0x1

    if-ne v3, v13, :cond_d

    .line 498
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const v14, 0x7f0b0153

    invoke-virtual {v13, v14}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v1, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 500
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v13, v12, v14}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 507
    :cond_c
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    goto/16 :goto_1

    .line 501
    :cond_d
    const/4 v13, 0x1

    if-le v3, v13, :cond_c

    .line 502
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const v14, 0x7f0b0152

    invoke-virtual {v13, v14}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v1, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 504
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v13, v12, v14}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_4

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
