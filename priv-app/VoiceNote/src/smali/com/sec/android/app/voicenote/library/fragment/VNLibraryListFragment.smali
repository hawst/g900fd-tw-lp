.class public Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;
.super Landroid/app/ListFragment;
.source "VNLibraryListFragment.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;


# static fields
.field private static final LISTVIEW:Ljava/lang/String; = "listview"

.field private static final LISTVIEW_ALPHA:Ljava/lang/String; = "alpha"

.field private static final LISTVIEW_SEARCHTEXT:Ljava/lang/String; = "searchtext"

.field private static final LISTVIEW_TRIMMODE:Ljava/lang/String; = "trim"

.field private static final MENU_CHANGELABEL:I = 0x9

.field private static final MENU_DELETE:I = 0x2

.field private static final MENU_DETAILS:I = 0x7

.field private static final MENU_MOVETOSECRET:I = 0xa

.field private static final MENU_REMOVEFROMSECRET:I = 0xb

.field private static final MENU_RENAME:I = 0x3

.field private static final MENU_SETAS:I = 0x6

.field private static final MENU_SHAREVIA:I = 0x1

.field private static final MSG_FIND_POSITION:I = 0x65

.field private static final TAG:Ljava/lang/String; = "VNLibraryListFragment"

.field private static mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private static mIsTrimMode:Z

.field private static mSelectedPosition:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static msearchText:Ljava/lang/String;


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mEnableList:Z

.field private mHandler:Landroid/os/Handler;

.field private mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mSelectedID:J

.field private mbScrollList:Z

.field private mmainView:Landroid/view/View;

.field private msearchEmptyview:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 78
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 79
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    .line 81
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    .line 89
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 73
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    .line 74
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    .line 75
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    .line 76
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .line 80
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedID:J

    .line 83
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mEnableList:Z

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mbScrollList:Z

    .line 624
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mHandler:Landroid/os/Handler;

    .line 106
    return-void
.end method

.method public static final getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 868
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    return-object v0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 3
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 564
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initService : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 566
    return-void
.end method

.method private initViews()V
    .locals 3

    .prologue
    .line 294
    const-string v0, "VNLibraryListFragment"

    const-string v1, "initViews E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    const v1, 0x7f0e003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 299
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mEnableList:Z

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 303
    const-string v0, "VNLibraryListFragment"

    const-string v1, "initViews X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-void
.end method

.method private listBinding()Z
    .locals 23

    .prologue
    .line 307
    const-string v2, "VNLibraryListFragment"

    const-string v3, "listBinding E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v2, "category_text"

    const-wide/16 v8, -0x1

    invoke-static {v2, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v18

    .line 309
    .local v18, "categotyID":J
    const/16 v16, 0x0

    .line 310
    .local v16, "builder":Ljava/lang/StringBuilder;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v22, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v22, v2

    .line 312
    .local v22, "temp_args":[Ljava/lang/String;
    const/4 v14, 0x0

    .line 313
    .local v14, "arg_index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 315
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    .line 319
    :cond_0
    const/4 v5, 0x0

    .line 320
    .local v5, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 321
    const-wide/16 v2, -0x1

    cmp-long v2, v18, v2

    if-lez v2, :cond_1

    .line 322
    const-string v2, " and (label_id == ?)"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "arg_index":I
    .local v15, "arg_index":I
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v22, v14

    move v14, v15

    .line 325
    .end local v15    # "arg_index":I
    .restart local v14    # "arg_index":I
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 326
    const-string v2, " and (title LIKE ?"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "arg_index":I
    .restart local v15    # "arg_index":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v22, v14

    .line 328
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 330
    :cond_2
    const-string v2, " or is_memo == \'2\'"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    :cond_3
    const-string v2, ")"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v14, v15

    .line 334
    .end local v15    # "arg_index":I
    .restart local v14    # "arg_index":I
    :cond_4
    const/4 v6, 0x0

    .line 335
    .local v6, "args":[Ljava/lang/String;
    if-lez v14, :cond_5

    .line 336
    new-array v6, v14, [Ljava/lang/String;

    .line 337
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_0
    move/from16 v0, v21

    if-ge v0, v14, :cond_5

    .line 338
    aget-object v2, v22, v21

    aput-object v2, v6, v21

    .line 337
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 341
    .end local v21    # "i":I
    :cond_5
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 343
    const/4 v2, 0x5

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v12, v2

    const/4 v2, 0x1

    const-string v3, "title"

    aput-object v3, v12, v2

    const/4 v2, 0x2

    const-string v3, "date_modified"

    aput-object v3, v12, v2

    const/4 v2, 0x3

    const-string v3, "duration"

    aput-object v3, v12, v2

    const/4 v2, 0x4

    const-string v3, "_data"

    aput-object v3, v12, v2

    .line 349
    .local v12, "cols":[Ljava/lang/String;
    const/4 v2, 0x3

    new-array v13, v2, [I

    fill-array-data v13, :array_0

    .line 353
    .local v13, "to":[I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getSortQuery()Ljava/lang/String;

    move-result-object v7

    .line 356
    .local v7, "orderBy":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 372
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_7

    .line 373
    const-string v2, "VNLibraryListFragment"

    const-string v3, "listBinding() : cursor null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    new-instance v21, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;

    move-object/from16 v0, v21

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 375
    .local v21, "i":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 377
    const/4 v2, 0x0

    .line 396
    .end local v21    # "i":Landroid/content/Intent;
    :goto_2
    return v2

    .line 358
    :catch_0
    move-exception v17

    .line 359
    .local v17, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "VNLibraryListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listBinding - SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 361
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 362
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_1

    .line 364
    .end local v17    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v20

    .line 365
    .local v20, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "VNLibraryListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 367
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 368
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    goto/16 :goto_1

    .line 380
    .end local v20    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    if-eqz v2, :cond_8

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 388
    :cond_8
    :goto_3
    new-instance v8, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v10, 0x7f03002f

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-direct/range {v8 .. v13}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .line 391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->setLabelCallback(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;)V

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->setSearchText(Ljava/lang/String;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 395
    const-string v2, "VNLibraryListFragment"

    const-string v3, "listBinding X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const/4 v2, 0x1

    goto/16 :goto_2

    .line 384
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 349
    :array_0
    .array-data 4
        0x7f0e0067
        0x7f0e0068
        0x7f0e0069
    .end array-data
.end method

.method public static newInstance(Z)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;
    .locals 3
    .param p0, "enableList"    # Z

    .prologue
    .line 109
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;-><init>()V

    .line 110
    .local v1, "f":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "enableList"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 112
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 113
    return-object v1
.end method

.method public static releaseService()V
    .locals 3

    .prologue
    .line 570
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseService : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 574
    return-void
.end method

.method private scrolltoHiddenPlayItem()V
    .locals 3

    .prologue
    .line 279
    const-string v0, "VNLibraryListFragment"

    const-string v1, "scrolltoHiddenPlayItem E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getCurrentPlayState()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_1

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    if-lt v0, v1, :cond_2

    .line 282
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scrolltoHiddenPlayItem1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    if-gt v0, v1, :cond_3

    .line 287
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scrolltoHiddenPlayItem2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPosition(II)V

    .line 290
    :cond_3
    const-string v0, "VNLibraryListFragment"

    const-string v1, "scrolltoHiddenPlayItem X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setEmptyView()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 786
    const/4 v2, 0x0

    .line 787
    .local v2, "textview":Landroid/widget/TextView;
    const/4 v3, 0x0

    .line 788
    .local v3, "textview_top":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 790
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    const v5, 0x7f0e001a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "textview_top":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 791
    .restart local v3    # "textview_top":Landroid/widget/TextView;
    if-nez v3, :cond_1

    .line 792
    const-string v4, "VNLibraryListFragment"

    const-string v5, "setEmptyView X - textView is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    :goto_0
    return-void

    .line 796
    :cond_0
    const-string v4, "VNLibraryListFragment"

    const-string v5, "setEmptyView X - MainView is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 800
    :cond_1
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 802
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 803
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 810
    :goto_1
    const-string v4, "category_text"

    invoke-static {v4, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v0

    .line 812
    .local v0, "category":J
    const-string v4, "VNLibraryListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setEmptyView E : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    cmp-long v4, v0, v8

    if-eqz v4, :cond_2

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 814
    :cond_2
    const v4, 0x7f0b00d1

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 818
    :goto_2
    const-string v4, "VNLibraryListFragment"

    const-string v5, "setEmptyView X"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 806
    .end local v0    # "category":J
    :cond_3
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 807
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchEmptyview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 816
    .restart local v0    # "category":J
    :cond_4
    const v4, 0x7f0b00d2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method public static setTrimMode(Z)V
    .locals 3
    .param p0, "isTrimMode"    # Z

    .prologue
    .line 760
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTrimMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    sput-boolean p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    .line 762
    return-void
.end method


# virtual methods
.method public clickNextItem()V
    .locals 6

    .prologue
    .line 661
    const-string v0, "VNLibraryListFragment"

    const-string v1, "clickNextItem E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->isNextItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    sget v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 668
    :cond_0
    :goto_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "clickNextItem X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    return-void

    .line 665
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 666
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoResume()V

    goto :goto_0
.end method

.method public clickPreviousItem()V
    .locals 6

    .prologue
    .line 694
    const-string v0, "VNLibraryListFragment"

    const-string v1, "clickPreviousItem E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->isPreviousItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    sget v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 702
    :cond_0
    :goto_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "clickPreviousItem X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    return-void

    .line 698
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 699
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoResume()V

    goto :goto_0
.end method

.method public doPlayTrimNewFile()V
    .locals 7

    .prologue
    .line 852
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->getLastSavedFileUri()Landroid/net/Uri;

    move-result-object v3

    .line 853
    .local v3, "uri":Landroid/net/Uri;
    const-string v4, "VNLibraryListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doPlayTrimNewFile E : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    if-eqz v3, :cond_1

    .line 855
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 856
    .local v0, "id":J
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->findPosition(J)I

    move-result v2

    .line 857
    .local v2, "newPosition":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 858
    sput v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    .line 859
    const-string v4, "VNLibraryListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doPlayTrimNewFile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getView()Landroid/view/View;

    move-result-object v5

    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 862
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    sget v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 864
    .end local v0    # "id":J
    .end local v2    # "newPosition":I
    :cond_1
    const-string v4, "VNLibraryListFragment"

    const-string v5, "doPlayTrimNewFile X"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    return-void
.end method

.method public executeTrimMode()V
    .locals 3

    .prologue
    .line 765
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "executeTrimMode E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    sget-boolean v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->setTrimMode(Z)V

    .line 769
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 770
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    if-eqz v0, :cond_2

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 776
    :cond_1
    :goto_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "executeTrimMode X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    return-void

    .line 773
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    goto :goto_0
.end method

.method findPosition()V
    .locals 4

    .prologue
    .line 604
    const-string v1, "VNLibraryListFragment"

    const-string v2, "findPosition : E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-nez v1, :cond_0

    .line 606
    const-string v1, "VNLibraryListFragment"

    const-string v2, "findPosition : adapter is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :goto_0
    return-void

    .line 608
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 609
    const-string v1, "VNLibraryListFragment"

    const-string v2, "findPosition : fragment is being removed from its activity"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 613
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedID:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->findPosition(J)I

    move-result v0

    .line 614
    .local v0, "result":I
    const-string v1, "VNLibraryListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selected pos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    if-eq v0, v1, :cond_2

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 617
    const-string v1, "VNLibraryListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    .line 619
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDataChanged()V

    .line 621
    :cond_2
    const-string v1, "VNLibraryListFragment"

    const-string v2, "findPosition : X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCurrentPlayID()J
    .locals 2

    .prologue
    .line 548
    const-string v0, "VNLibraryListFragment"

    const-string v1, "getCurrentPlayID E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 550
    const-wide/16 v0, -0x1

    .line 552
    :goto_0
    return-wide v0

    .line 551
    :cond_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "getCurrentPlayID X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getCurrentPlayState()I
    .locals 2

    .prologue
    .line 557
    const-string v0, "VNLibraryListFragment"

    const-string v1, "getCurrentPlayState"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 559
    const/16 v0, 0x15

    .line 560
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    goto :goto_0
.end method

.method public isNextItem()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 672
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isNextItem E"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-nez v3, :cond_0

    .line 675
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isNextItem : adapter is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    :goto_0
    return v2

    .line 679
    :cond_0
    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    add-int/lit8 v1, v3, 0x1

    .line 681
    .local v1, "nextPosition":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getItemId(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 682
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isNextItem : false"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 685
    :catch_0
    move-exception v0

    .line 686
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isNextItem : exception false"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 689
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    const-string v2, "VNLibraryListFragment"

    const-string v3, "isNextItem X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isPreviousItem()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 706
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isPreviousItem E"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-nez v3, :cond_0

    .line 709
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isPreviousItem : adapter is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :goto_0
    return v2

    .line 713
    :cond_0
    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    add-int/lit8 v1, v3, -0x1

    .line 715
    .local v1, "nextPosition":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getItemId(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 716
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isPreviousItem : false"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 719
    :catch_0
    move-exception v0

    .line 720
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v3, "VNLibraryListFragment"

    const-string v4, "isPreviousItem : exception false"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 723
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    const-string v2, "VNLibraryListFragment"

    const-string v3, "isPreviousItem X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected moveSecretBox(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 822
    const-string v3, "VNLibraryListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moveSecretBox E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 824
    .local v1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 826
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    move-result-object v2

    .line 827
    .local v2, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 828
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v3, "target_datas"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 829
    const-string v3, "movetosecret"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 830
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_FILE"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 831
    const-string v3, "VNLibraryListFragment"

    const-string v4, "moveSecretBox X"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 228
    const-string v2, "VNLibraryListFragment"

    const-string v3, "onActivityCreated E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v2, :cond_0

    .line 230
    const-string v2, "listview"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 231
    .local v1, "state":Landroid/os/Parcelable;
    const-string v2, "alpha"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    .line 232
    .local v0, "alpha":F
    const-string v2, "trim"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    sget-boolean v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->setTrimMode(Z)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAlpha(F)V

    .line 235
    const-string v2, "VNLibraryListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityCreated : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    if-eqz v1, :cond_0

    .line 237
    const-string v2, "VNLibraryListFragment"

    const-string v3, "onActivityCreated : restore"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 241
    .end local v0    # "alpha":F
    .end local v1    # "state":Landroid/os/Parcelable;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 242
    const-string v2, "VNLibraryListFragment"

    const-string v3, "onActivityCreated X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 247
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    packed-switch p1, :pswitch_data_0

    .line 255
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 256
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onActivityResult X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    return-void

    .line 250
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->updateState(Z)V

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v10, 0x15

    const/4 v12, 0x1

    .line 450
    const-string v7, "VNLibraryListFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onContextItemSelected E : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v5

    check-cast v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 455
    .local v5, "info":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 529
    :goto_0
    :pswitch_0
    const-string v7, "VNLibraryListFragment"

    const-string v8, "onContextItemSelected X"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    return v12

    .line 458
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 460
    .local v3, "filename":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 461
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;-><init>()V

    .local v2, "dialog":Landroid/app/DialogFragment;
    move-object v7, v2

    .line 462
    check-cast v7, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-virtual {v7, v8, v9, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 465
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSend(Landroid/content/Context;J)V

    goto :goto_0

    .line 472
    .end local v3    # "filename":Ljava/lang/String;
    :pswitch_2
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;-><init>()V

    .line 473
    .local v1, "deletedialog":Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;
    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_1

    .line 474
    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-virtual {v1, v8, v9, v12}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->setArguments(JZ)V

    .line 478
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 476
    :cond_1
    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    const/4 v7, 0x0

    invoke-virtual {v1, v8, v9, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->setArguments(JZ)V

    goto :goto_1

    .line 482
    .end local v1    # "deletedialog":Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;
    :pswitch_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 483
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasTagData(Landroid/content/Context;Ljava/util/ArrayList;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 485
    .local v4, "hasNFC":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 486
    const v7, 0x7f0b0105

    const v8, 0x7f0b013d

    invoke-static {v7, v8, v12}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 490
    :cond_2
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showEditDialoginPlay(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/content/Context;Landroid/app/FragmentManager;)J

    goto/16 :goto_0

    .line 495
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v4    # "hasNFC":Ljava/lang/Boolean;
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setArguments(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    move-result-object v2

    .line 496
    .restart local v2    # "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG_SETAS"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 500
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    :pswitch_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 501
    .local v6, "title":Ljava/lang/String;
    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    const v7, 0x7f0b006d

    invoke-static {v8, v9, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->setArguments(JI)Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;

    move-result-object v2

    .line 502
    .restart local v2    # "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 506
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    .end local v6    # "title":Ljava/lang/String;
    :pswitch_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 507
    .restart local v6    # "title":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;-><init>()V

    .restart local v2    # "dialog":Landroid/app/DialogFragment;
    move-object v7, v2

    .line 508
    check-cast v7, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-virtual {v7, v8, v9, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 513
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    .end local v6    # "title":Ljava/lang/String;
    :pswitch_7
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getCurrentPlayState()I

    move-result v7

    if-eq v7, v10, :cond_3

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_3

    .line 514
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoStop()V

    .line 516
    :cond_3
    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->moveSecretBox(J)V

    goto/16 :goto_0

    .line 520
    :pswitch_8
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getCurrentPlayState()I

    move-result v7

    if-eq v7, v10, :cond_4

    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_4

    .line 521
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoStop()V

    .line 523
    :cond_4
    iget-wide v8, v5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->restoreSecretBox(J)V

    goto/16 :goto_0

    .line 455
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 183
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 189
    :cond_0
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() callback : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 191
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onCreate() X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 12
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    const/16 v11, 0x15

    const/4 v10, 0x0

    .line 401
    const-string v6, "VNLibraryListFragment"

    const-string v7, "onCreateContextMenu E"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    move-object v3, p3

    .line 404
    check-cast v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 405
    .local v3, "mi":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    if-eqz v3, :cond_0

    sget-object v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v6, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    const/4 v4, 0x0

    .line 409
    .local v4, "name":Ljava/lang/String;
    iget-object v6, v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    const v7, 0x7f0e0067

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 410
    .local v5, "textview":Landroid/widget/TextView;
    iget-object v6, v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    const v7, 0x7f0e006a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 411
    .local v2, "label":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-wide v8, v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isSecretBoxFile(Landroid/content/Context;J)I

    move-result v0

    .line 413
    .local v0, "bSecretboxFile":I
    if-eqz v5, :cond_2

    .line 414
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 415
    const-string v6, "VNLibraryListFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreateContextMenu : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_2
    if-nez v4, :cond_3

    .line 419
    const-string v6, "VNLibraryListFragment"

    const-string v7, "onCreateContextMenu: Can`t load file."

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 423
    :cond_3
    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 424
    const/4 v6, 0x1

    const v7, 0x7f0b011f

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 425
    const/4 v6, 0x2

    const v7, 0x7f0b005d

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getCurrentPlayState()I

    move-result v6

    if-ne v6, v11, :cond_4

    .line 428
    const/4 v6, 0x3

    const v7, 0x7f0b0105

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 430
    :cond_4
    if-eqz v0, :cond_5

    .line 431
    const/4 v6, 0x6

    const v7, 0x7f0b011c

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 433
    :cond_5
    if-eqz v2, :cond_6

    .line 434
    const-string v6, "VNLibraryListFragment"

    const-string v7, "onCreateContextMenu : label is contained"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const/16 v6, 0x9

    const v7, 0x7f0b002d

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 436
    .local v1, "item":Landroid/view/MenuItem;
    if-nez v0, :cond_6

    invoke-interface {v1, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 439
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_6
    sget-object v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getCurrentPlayState()I

    move-result v6

    if-ne v6, v11, :cond_7

    .line 440
    const-string v6, "VNLibraryListFragment"

    const-string v7, "onCreateContextMenu : SecretMode"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const/16 v6, 0xa

    const v7, 0x7f0b00c4

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 444
    :cond_7
    const/4 v6, 0x7

    const v7, 0x7f0b006d

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 445
    const-string v6, "VNLibraryListFragment"

    const-string v7, "onCreateContextMenu X"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    const-string v1, "VNLibraryListFragment"

    const-string v2, "onCreateView() E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 121
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 122
    const-string v1, "enableList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mEnableList:Z

    .line 124
    :cond_0
    const v1, 0x7f030019

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 127
    if-eqz p3, :cond_1

    .line 128
    const-string v1, "searchtext"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    .line 130
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->initViews()V

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->listBinding()Z

    .line 133
    if-eqz p3, :cond_2

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->setEmptyView()V

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-eqz v1, :cond_3

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    sget-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->setTrimMode(Z)V

    .line 139
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getCurrentPlayState()I

    move-result v1

    const/16 v2, 0x15

    if-eq v1, v2, :cond_4

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_4

    .line 140
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedID:J

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->findPosition()V

    .line 142
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mbScrollList:Z

    .line 145
    :cond_4
    const-string v1, "VNLibraryListFragment"

    const-string v2, "onCreateView() X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 170
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onDestroy() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCursor:Landroid/database/Cursor;

    .line 176
    :cond_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onDestroy() X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 179
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onDestroyView() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 153
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onDestroyView() listview is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 155
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 160
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mmainView:Landroid/view/View;

    .line 161
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .line 162
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    .line 163
    sput-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    .line 164
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onDestroyView() X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    .line 166
    return-void
.end method

.method public onLabelButtonClicked(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 535
    const-string v2, "VNLibraryListFragment"

    const-string v3, "onLabelButtonClicked E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->isShown()Z

    move-result v2

    if-nez v2, :cond_0

    .line 537
    const v2, 0x7f0b002d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 538
    .local v1, "title":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;-><init>()V

    .line 539
    .local v0, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 540
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 542
    .end local v0    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    const-string v2, "VNLibraryListFragment"

    const-string v3, "onLabelButtonClicked X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 751
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onLayoutChange E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    if-eq p5, p9, :cond_0

    if-ne p4, p8, :cond_0

    .line 753
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onLayoutChange : scrolltoHiddenPlayItem"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->scrolltoHiddenPlayItem()V

    .line 756
    :cond_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onLayoutChange X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 261
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onListItemClick E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 263
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/ListView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    :cond_0
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onListItemClick return"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :goto_0
    return-void

    .line 268
    :cond_1
    sput p3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    .line 269
    iput-wide p4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedID:J

    .line 271
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_2

    .line 272
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getId()I

    move-result v1

    invoke-interface {v0, v1, p4, p5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    .line 274
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->scrolltoHiddenPlayItem()V

    .line 275
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onListItemClick X : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedID:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 196
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onResume() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-eqz v0, :cond_0

    .line 199
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onResume : notifyDataSetChanged()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->notifyDataSetChanged()V

    .line 203
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mbScrollList:Z

    if-eqz v0, :cond_1

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->scrolltoHiddenPlayItem()V

    .line 205
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mbScrollList:Z

    .line 207
    :cond_1
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 208
    const-string v0, "VNLibraryListFragment"

    const-string v1, "onResume() X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 213
    const-string v1, "VNLibraryListFragment"

    const-string v2, "onSaveInstanceState E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 216
    .local v0, "state":Landroid/os/Parcelable;
    const-string v1, "listview"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 217
    const-string v1, "alpha"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAlpha()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 218
    const-string v1, "trim"

    sget-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mIsTrimMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 219
    const-string v1, "searchtext"

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .end local v0    # "state":Landroid/os/Parcelable;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 222
    const-string v1, "VNLibraryListFragment"

    const-string v2, "onSaveInstanceState X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method protected restoreSecretBox(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 835
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreSecretBox E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSecretBox(Landroid/content/Context;)V

    .line 848
    const-string v0, "VNLibraryListFragment"

    const-string v1, "restoreSecretBox X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    return-void
.end method

.method public setFocusListView(Z)V
    .locals 1
    .param p1, "bfocus"    # Z

    .prologue
    .line 872
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 873
    :cond_0
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 3
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 781
    const-string v0, "VNLibraryListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSearchText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->msearchText:Ljava/lang/String;

    .line 783
    return-void
.end method

.method public updateState(Z)V
    .locals 7
    .param p1, "bRequeryCursor"    # Z

    .prologue
    const/16 v6, 0x65

    .line 577
    const-string v1, "VNLibraryListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateState E : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListView:Landroid/widget/ListView;

    if-nez v1, :cond_0

    .line 601
    :goto_0
    return-void

    .line 580
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 581
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .line 582
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->setEmptyView()V

    .line 583
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->listBinding()Z

    .line 591
    :cond_1
    :goto_1
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mSelectedID:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 592
    const-string v1, "VNLibraryListFragment"

    const-string v2, "updateState : mSelectedID is invalid"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 585
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    if-eqz v1, :cond_1

    .line 586
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->notifyDataSetChanged()V

    .line 587
    const-string v1, "VNLibraryListFragment"

    const-string v2, "updateState : notifyDataSetChanged()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 596
    :cond_3
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 597
    .local v0, "msg":Landroid/os/Message;
    iput v6, v0, Landroid/os/Message;->what:I

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryListFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 600
    const-string v1, "VNLibraryListFragment"

    const-string v2, "updateState X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
