.class public Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
.super Landroid/app/Activity;
.source "VNSttForThirdActivity.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;
    }
.end annotation


# static fields
.field private static final DORECORD:I = 0x30d4b

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final RESULT_SAVED:I = 0x30d4c

.field private static final TAG:Ljava/lang/String; = "VNSttForThirdActivity"

.field public static mPreviousRecordingMode:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

.field private bShowKeyboard:Z

.field private final mEventHandler:Landroid/os/Handler;

.field private mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

.field private final mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

.field private final mIServiceConnection:Landroid/content/ServiceConnection;

.field private mIsDialogDone:Z

.field private mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

.field private mMainFragmentWrapperView:Landroid/view/View;

.field private mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

.field private mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

.field private mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

.field private mRegistedIService:Z

.field private mResumed:Z

.field private mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

.field private mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

.field private final mUIUpdateHandler:Landroid/os/Handler;

.field private mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 70
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPreviousRecordingMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mMainFragmentWrapperView:Landroid/view/View;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 52
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mResumed:Z

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    .line 61
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z

    .line 62
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->bShowKeyboard:Z

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIsDialogDone:Z

    .line 232
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mEventHandler:Landroid/os/Handler;

    .line 261
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceConnection:Landroid/content/ServiceConnection;

    .line 541
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    .line 633
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$4;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getMediaRecorderState()I

    move-result v0

    return v0
.end method

.method static synthetic access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateTime()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addBookmark()V
    .locals 4

    .prologue
    .line 682
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v1, :cond_1

    .line 702
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 686
    .local v0, "tempfragment":Landroid/app/Fragment;
    sget-object v1, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->addBookmark()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 688
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b001e

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 693
    :pswitch_1
    if-nez v0, :cond_0

    .line 694
    const v1, 0x7f0b001d

    const v2, 0x7f0b001c

    const/16 v3, 0x32

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(III)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 686
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private drawControlButton(Landroid/app/FragmentTransaction;)V
    .locals 4
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    const/4 v3, 0x0

    .line 509
    const-string v1, "VNSttForThirdActivity"

    const-string v2, "drawControlButton"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    const-string v1, "VNSttForThirdActivity"

    const-string v2, "skip drawControlButton after the Activity is destroyed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :goto_0
    return-void

    .line 516
    :cond_0
    const/4 v0, 0x0

    .line 518
    .local v0, "currentResource":I
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getMediaRecorderState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 528
    const v0, 0x7f030044

    .line 532
    :goto_1
    invoke-static {v0, v3, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->newInstance(IZZ)Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    .line 533
    const v1, 0x7f0e0017

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    invoke-virtual {p1, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 520
    :pswitch_0
    const v0, 0x7f030048

    .line 521
    goto :goto_1

    .line 524
    :pswitch_1
    const v0, 0x7f030046

    .line 525
    goto :goto_1

    .line 518
    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getMediaRecorderState()I
    .locals 4

    .prologue
    .line 297
    const/16 v1, 0x3e8

    .line 299
    .local v1, "state":I
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v2, :cond_0

    .line 301
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getMediaRecorderState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 312
    :goto_0
    return v1

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "getMediaRecorderState error"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 306
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sget-object v2, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_1

    .line 307
    sget-object v2, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    goto :goto_0

    .line 309
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getRecorderState()I

    move-result v1

    goto :goto_0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 225
    sput-object p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 226
    return-void
.end method

.method private interruptThread(Ljava/lang/Thread;)V
    .locals 1
    .param p1, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 628
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 631
    :cond_0
    return-void
.end method

.method private isAliveThread(Ljava/lang/Thread;)Z
    .locals 1
    .param p1, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 619
    if-eqz p1, :cond_0

    .line 620
    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const/4 v0, 0x1

    .line 624
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 230
    return-void
.end method

.method private restartTimeBlinkThread()V
    .locals 2

    .prologue
    .line 610
    const-string v0, "VNSttForThirdActivity"

    const-string v1, "restartTimeBlinkThread"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 613
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;->start()V

    .line 616
    :cond_0
    return-void
.end method

.method private updateTime()V
    .locals 2

    .prologue
    const/16 v1, 0x835

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 539
    return-void
.end method

.method private updateUIThread()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x80

    .line 484
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getMediaRecorderState()I

    move-result v0

    .line 486
    .local v0, "state":I
    const-string v1, "VNSttForThirdActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateUIThread() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    packed-switch v0, :pswitch_data_0

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 503
    invoke-static {p0, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    .line 506
    :goto_0
    return-void

    .line 490
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 491
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 495
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->restartTimeBlinkThread()V

    .line 496
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 497
    invoke-static {p0, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    goto :goto_0

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected doCancel()V
    .locals 5

    .prologue
    .line 466
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "doCancel"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 469
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v2, :cond_0

    .line 470
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->cancelRecording()Z

    .line 471
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 473
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 474
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setResultWithFinish(I)V

    .line 481
    return-void

    .line 476
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VNSttForThirdActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doCancel RemoteException "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doPause()V
    .locals 5

    .prologue
    .line 403
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "doPause"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v2, :cond_0

    .line 418
    :goto_0
    return-void

    .line 409
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->pauseRecording()Z

    .line 410
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 412
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 413
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 414
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VNSttForThirdActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doPause error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doRecord(Z)V
    .locals 8
    .param p1, "startVoiceLabelAfterRecord"    # Z

    .prologue
    .line 362
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "doRecord"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 366
    sget-boolean v3, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v3, :cond_1

    const v1, 0x7f0b00d5

    .line 368
    .local v1, "messageId":I
    :goto_0
    const v3, 0x7f0b00d3

    invoke-static {v3, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 400
    .end local v1    # "messageId":I
    :cond_0
    :goto_1
    return-void

    .line 366
    :cond_1
    const v1, 0x7f0b00d4

    goto :goto_0

    .line 375
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_0

    .line 378
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getMediaRecorderState()I

    move-result v2

    .line 379
    .local v2, "state":I
    const/16 v3, 0x3eb

    if-eq v2, v3, :cond_3

    const/16 v3, 0x3ec

    if-ne v2, v3, :cond_4

    .line 380
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z

    .line 381
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mEventHandler:Landroid/os/Handler;

    const v4, 0x30d4b

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 384
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->initRecording(Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 385
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->startRecording()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 386
    sget-object v3, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setStartVoiceLabelAfterRecord(Z)V

    .line 387
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V

    .line 388
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->changVisibleBtn()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 396
    .end local v2    # "state":I
    :catch_0
    move-exception v0

    .line 397
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "VNSttForThirdActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doRecord error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 390
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v2    # "state":I
    :cond_5
    :try_start_1
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "StartRecording failed"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 394
    :cond_6
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "initRecording failed"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected doResume()V
    .locals 5

    .prologue
    .line 421
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "doResume"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v2, :cond_0

    .line 436
    :goto_0
    return-void

    .line 427
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->resumeRecording()Z

    .line 428
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 430
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 431
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 432
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 433
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VNSttForThirdActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doResume error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doSave()V
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doSave(Z)V

    .line 463
    return-void
.end method

.method protected doSave(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 439
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "doSave"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 442
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_0

    .line 443
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z

    .line 445
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 447
    .local v2, "ft":Landroid/app/FragmentTransaction;
    const/4 v0, 0x0

    .line 448
    .local v0, "convertedText":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    if-eqz v3, :cond_1

    .line 449
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getConvertedText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 451
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 452
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 453
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateTime()V

    .line 454
    const v3, 0x30d4c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setResultWithFinish(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    .end local v0    # "convertedText":Ljava/lang/CharSequence;
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    :goto_0
    return-void

    .line 455
    :catch_0
    move-exception v1

    .line 456
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "VNSttForThirdActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSave error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public is_Resumed()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mResumed:Z

    return v0
.end method

.method public onAlertDialogPositiveBtn(Z)V
    .locals 0
    .param p1, "positive"    # Z

    .prologue
    .line 676
    if-eqz p1, :cond_0

    .line 677
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doCancel()V

    .line 679
    :cond_0
    return-void
.end method

.method public onControlButtonSelected(I)Z
    .locals 8
    .param p1, "button"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->is_Resumed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 319
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "onControlButtonSelected - hidden state"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    :goto_0
    return v1

    .line 322
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_0

    .line 325
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 327
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doRecord(Z)V

    move v1, v2

    .line 328
    goto :goto_0

    .line 330
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doPause()V

    move v1, v2

    .line 331
    goto :goto_0

    .line 333
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doResume()V

    move v1, v2

    .line 334
    goto :goto_0

    .line 337
    :pswitch_4
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getRecDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doCancel()V

    :goto_1
    move v1, v2

    .line 346
    goto :goto_0

    .line 340
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doSave(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e1":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 348
    .end local v0    # "e1":Landroid/os/RemoteException;
    :pswitch_5
    const v1, 0x7f0b0023

    const v3, 0x7f0b0022

    invoke-static {v1, v3, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    move v1, v2

    .line 351
    goto :goto_0

    .line 353
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->addBookmark()V

    move v1, v2

    .line 354
    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x7f0e00c7
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 74
    const-string v3, "VNSttForThirdActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 76
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWindowStatusBarFlag(Landroid/app/Activity;)V

    .line 77
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 78
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030005

    invoke-virtual {v1, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 79
    .local v2, "mainView":Landroid/view/View;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setContentView(Landroid/view/View;)V

    .line 81
    const v3, 0x7f0e000d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mMainFragmentWrapperView:Landroid/view/View;

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->initContext(Landroid/content/Context;)V

    .line 83
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPreviousRecordingMode:I

    .line 84
    const-string v3, "record_mode"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 86
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 89
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/VNIntent;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNIntent;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 93
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const/16 v3, 0x3e8

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->newInstance(ILjava/lang/CharSequence;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 94
    const v3, 0x7f0e000e

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 96
    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 98
    sget v3, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->newInstance(I)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .line 99
    const v3, 0x7f0e000f

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v0, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 101
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 103
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "VNSttForThirdActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 113
    :cond_0
    const-string v0, "record_mode"

    sget v1, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPreviousRecordingMode:I

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 116
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 206
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mResumed:Z

    .line 209
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_0

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->unregisterCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 211
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 218
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 190
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mResumed:Z

    .line 193
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z

    if-nez v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->registerCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 195
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 202
    return-void

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 6

    .prologue
    .line 120
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "onStart"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    sget v4, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateTime(J)V

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "category_label_color"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateFilename(Ljava/lang/String;I)V

    .line 125
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 128
    .local v0, "checkDialog1":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG_2"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    .line 130
    .local v1, "checkDialog2":Landroid/app/DialogFragment;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 135
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIsDialogDone:Z

    if-eqz v3, :cond_2

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    .line 160
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->drawControlButtonforVoiceMemo()V

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->disableSettingBtnForVoiceMemo()V

    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 164
    return-void

    .line 132
    :cond_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    goto :goto_0

    .line 138
    :cond_2
    const-string v3, "show_policy_info"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v3

    if-nez v3, :cond_3

    .line 140
    new-instance v2, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;-><init>()V

    .line 141
    .local v2, "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 143
    .end local v2    # "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->finish()V

    .line 144
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "OnStart - finish1"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 146
    :cond_4
    const-string v3, "show_stt_recc_info"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v3

    if-nez v3, :cond_5

    .line 148
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 150
    const-string v3, "MEMO"

    invoke-static {p0, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 152
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->finish()V

    .line 153
    const-string v3, "VNSttForThirdActivity"

    const-string v4, "OnStart - finish2"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 156
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    goto :goto_1
.end method

.method protected onStop()V
    .locals 6

    .prologue
    .line 168
    const-string v1, "VNSttForThirdActivity"

    const-string v2, "onStop"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_0

    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getRecDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doCancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x835

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x836

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x83d

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 183
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 184
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->unbindFromService(Landroid/content/Context;)V

    .line 185
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 186
    return-void

    .line 174
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doSave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setResultWithFinish(I)V
    .locals 5
    .param p1, "resultCode"    # I

    .prologue
    .line 653
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 654
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "isFinishing()"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :goto_0
    return-void

    .line 658
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getConvertedText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 659
    .local v1, "str":Ljava/lang/CharSequence;
    const-string v2, "VNSttForThirdActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setResultWithFinish : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " text : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 661
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "stt_text"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 663
    const v2, 0x30d4c

    if-ne v2, p1, :cond_2

    .line 664
    const/4 p1, -0x1

    .line 669
    :cond_1
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setResult(ILandroid/content/Intent;)V

    .line 670
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->finish()V

    goto :goto_0

    .line 665
    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 666
    :cond_3
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public startBind()V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    .line 707
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIsDialogDone:Z

    .line 709
    :cond_0
    return-void
.end method
