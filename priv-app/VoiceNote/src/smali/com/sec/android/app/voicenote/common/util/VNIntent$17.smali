.class Lcom/sec/android/app/voicenote/common/util/VNIntent$17;
.super Landroid/content/BroadcastReceiver;
.source "VNIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverHeadSet(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V
    .locals 0

    .prologue
    .line 763
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v7, 0x1130

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 766
    const-string v3, "VNIntent"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 769
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 770
    const-string v3, "state"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 771
    .local v1, "earjackConnected":I
    const-string v3, "microphone"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 773
    .local v2, "microphone":I
    if-ne v1, v5, :cond_5

    .line 774
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$300()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->isInitialStickyBroadcast()Z

    move-result v3

    if-nez v3, :cond_1

    .line 776
    :cond_0
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$302(Z)Z

    .line 777
    instance-of v3, p1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    if-eqz v3, :cond_1

    .line 778
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 779
    if-ne v2, v5, :cond_1

    .line 780
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getRecorderState()I

    move-result v3

    const/16 v4, 0x3e8

    if-eq v3, v4, :cond_3

    .line 781
    const v3, 0x7f0b0102

    invoke-static {p1, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 792
    :cond_1
    :goto_0
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$302(Z)Z

    .line 793
    if-ne v2, v5, :cond_4

    .line 794
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isMicrophoneHeadsetConnected:Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$502(Z)Z

    .line 803
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->isInitialStickyBroadcast()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 804
    const-string v3, "VNIntent"

    const-string v4, "is Initial Sticky Broadcast"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    .end local v1    # "earjackConnected":I
    .end local v2    # "microphone":I
    :cond_2
    :goto_2
    return-void

    .line 783
    .restart local v1    # "earjackConnected":I
    .restart local v2    # "microphone":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mVNController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$400(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isPlayerActive()Z

    move-result v3

    if-nez v3, :cond_1

    .line 784
    const v3, 0x7f0b0073

    invoke-static {p1, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 796
    :cond_4
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isMicrophoneHeadsetConnected:Z
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$502(Z)Z

    goto :goto_1

    .line 799
    :cond_5
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$302(Z)Z

    .line 800
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->isMicrophoneHeadsetConnected:Z
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$502(Z)Z

    goto :goto_1

    .line 808
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 809
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 810
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2
.end method
