.class Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;
.super Ljava/lang/Object;
.source "VNFileNameDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$1000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$1000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 498
    .local v0, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$1000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 500
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_2

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$1000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 503
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$9;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$1000(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method
