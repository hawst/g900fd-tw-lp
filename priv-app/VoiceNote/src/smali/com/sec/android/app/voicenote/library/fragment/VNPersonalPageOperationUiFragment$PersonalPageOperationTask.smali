.class Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;
.super Landroid/os/AsyncTask;
.source "VNPersonalPageOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonalPageOperationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field completeToastText:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V
    .locals 1

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 493
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$1;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    return-void
.end method

.method private getTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "destFilePath"    # Ljava/lang/String;

    .prologue
    .line 599
    const/4 v2, 0x0

    .line 600
    .local v2, "rtn":Ljava/lang/String;
    move-object v0, p1

    .line 602
    .local v0, "checkFilePath":Ljava/lang/String;
    const/16 v3, 0x2f

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 603
    .local v1, "fileName":Ljava/lang/String;
    const/4 v3, 0x0

    const/16 v4, 0x2e

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 605
    return-object v2
.end method

.method private updatePrivate(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "srcFile"    # Ljava/io/File;
    .param p3, "dstFile"    # Ljava/io/File;
    .param p4, "srcPath"    # Ljava/lang/String;
    .param p5, "destFolderPath"    # Ljava/lang/String;
    .param p6, "destPath"    # Ljava/lang/String;
    .param p7, "update"    # I

    .prologue
    .line 354
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static/range {p5 .. p5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableSpace(Ljava/lang/String;)J

    move-result-wide v26

    cmp-long v2, v2, v26

    if-lez v2, :cond_0

    .line 355
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "There is no available space"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 357
    const/4 v2, 0x1

    .line 491
    :goto_0
    return v2

    .line 360
    :cond_0
    const/4 v14, 0x0

    .line 362
    .local v14, "from":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v14    # "from":Ljava/io/FileInputStream;
    .local v15, "from":Ljava/io/FileInputStream;
    move-object v14, v15

    .line 367
    .end local v15    # "from":Ljava/io/FileInputStream;
    .restart local v14    # "from":Ljava/io/FileInputStream;
    :goto_1
    const/16 v23, 0x0

    .line 369
    .local v23, "to":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v24, Ljava/io/FileOutputStream;

    move-object/from16 v0, v24

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v23    # "to":Ljava/io/FileOutputStream;
    .local v24, "to":Ljava/io/FileOutputStream;
    move-object/from16 v23, v24

    .line 375
    .end local v24    # "to":Ljava/io/FileOutputStream;
    .restart local v23    # "to":Ljava/io/FileOutputStream;
    :goto_2
    if-eqz v14, :cond_1

    if-nez v23, :cond_2

    .line 376
    :cond_1
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "stream \"from\" or \"to\" is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const/4 v2, 0x1

    goto :goto_0

    .line 363
    .end local v23    # "to":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v13

    .line 365
    .local v13, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v13}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 370
    .end local v13    # "e1":Ljava/io/FileNotFoundException;
    .restart local v23    # "to":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v13

    .line 372
    .restart local v13    # "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v13}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 381
    .end local v13    # "e1":Ljava/io/FileNotFoundException;
    :cond_2
    const/16 v2, 0x2000

    :try_start_2
    new-array v8, v2, [B

    .line 383
    .local v8, "buffer":[B
    const-wide/16 v20, 0x0

    .line 384
    .local v20, "readLength":J
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v18

    .line 385
    .local v18, "length":J
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 386
    :goto_3
    const/4 v2, 0x0

    const/16 v3, 0x2000

    invoke-virtual {v14, v8, v2, v3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v9

    .local v9, "bytesRead":I
    const/4 v2, -0x1

    if-eq v9, v2, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_9

    sget-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->EXTERNAL_STOP:Z

    if-eqz v2, :cond_9

    .line 387
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v8, v2, v9}, Ljava/io/FileOutputStream;->write([BII)V

    .line 388
    int-to-long v2, v9

    add-long v20, v20, v2

    .line 389
    cmp-long v2, v20, v18

    if-nez v2, :cond_6

    .line 390
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # operator++ for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$908(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    .line 391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 392
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 401
    .end local v8    # "buffer":[B
    .end local v9    # "bytesRead":I
    .end local v18    # "length":J
    .end local v20    # "readLength":J
    :catch_2
    move-exception v12

    .line 402
    .local v12, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 403
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 405
    :cond_3
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 406
    const/16 v26, 0x1

    .line 412
    if-eqz v14, :cond_4

    .line 413
    :try_start_4
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    .line 420
    :cond_4
    :goto_4
    if-eqz v23, :cond_5

    .line 421
    :try_start_5
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    .line 427
    :cond_5
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 428
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 429
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v28

    cmp-long v2, v2, v28

    if-eqz v2, :cond_15

    .line 430
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 431
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "srcFile and dstFile size are not same"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 394
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v8    # "buffer":[B
    .restart local v9    # "bytesRead":I
    .restart local v18    # "length":J
    .restart local v20    # "readLength":J
    :cond_6
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const-wide/16 v26, 0x64

    mul-long v26, v26, v20

    div-long v26, v26, v18

    move-wide/from16 v0, v26

    long-to-int v3, v0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 395
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    .line 407
    .end local v8    # "buffer":[B
    .end local v9    # "bytesRead":I
    .end local v18    # "length":J
    .end local v20    # "readLength":J
    :catch_3
    move-exception v12

    .line 408
    .local v12, "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 409
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 412
    if-eqz v14, :cond_7

    .line 413
    :try_start_8
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    .line 420
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_6
    if-eqz v23, :cond_8

    .line 421
    :try_start_9
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    .line 427
    :cond_8
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 428
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 429
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v26

    cmp-long v2, v2, v26

    if-eqz v2, :cond_1d

    .line 430
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 431
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "srcFile and dstFile size are not same"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 398
    .restart local v8    # "buffer":[B
    .restart local v9    # "bytesRead":I
    .restart local v18    # "length":J
    .restart local v20    # "readLength":J
    :cond_9
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_a

    .line 399
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->flush()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 412
    :cond_a
    if-eqz v14, :cond_b

    .line 413
    :try_start_b
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 420
    :cond_b
    :goto_8
    if-eqz v23, :cond_c

    .line 421
    :try_start_c
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    .line 427
    :cond_c
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 428
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 429
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v26

    cmp-long v2, v2, v26

    if-eqz v2, :cond_d

    .line 430
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 431
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "srcFile and dstFile size are not same"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 415
    :catch_4
    move-exception v12

    .line 416
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 417
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_8

    .line 423
    .end local v12    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v12

    .line 424
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 425
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_9

    .line 435
    .end local v12    # "e":Ljava/io/IOException;
    :cond_d
    const/16 v16, 0x0

    .line 436
    .local v16, "isDeleted":Z
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v26

    cmp-long v2, v2, v26

    if-nez v2, :cond_e

    .line 437
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v16

    .line 439
    :cond_e
    if-nez p3, :cond_f

    .line 440
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "dstFile is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_f
    if-eqz v16, :cond_14

    .line 443
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 444
    .local v22, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 445
    const-string v2, "_data"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 448
    .local v25, "where":Ljava/lang/String;
    const-string v5, "_data = ?"

    .line 449
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p4, v6, v2

    .line 451
    .local v6, "deleteSelectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 452
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 453
    .local v10, "contentValues":Landroid/content/ContentValues;
    if-eqz v11, :cond_11

    .line 454
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 455
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 456
    .local v17, "size":I
    if-lez v17, :cond_10

    .line 457
    new-instance v10, Landroid/content/ContentValues;

    .end local v10    # "contentValues":Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 458
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    const-string v2, "title"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v2, "mime_type"

    const-string v3, "mime_type"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v2, "_data"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v2, "duration"

    const-string v3, "duration"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v2, "_size"

    const-string v3, "_size"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v2, "date_modified"

    new-instance v3, Ljava/io/File;

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v26

    const-wide/16 v28, 0x3e8

    div-long v26, v26, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 464
    const-string v2, "recordingtype"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 465
    const-string v2, "latitude"

    const-string v3, "latitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 466
    const-string v2, "longitude"

    const-string v3, "longitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 467
    const-string v2, "addr"

    const-string v3, "addr"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v2, "track"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string v2, "is_ringtone"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 470
    const-string v2, "is_alarm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const-string v2, "is_notification"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 472
    const-string v2, "label_id"

    const-string v3, "label_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v2, "is_drm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    const-string v2, "album"

    const-string v3, "Sounds"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v2, "is_memo"

    const-string v3, "is_memo"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_10
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 479
    .end local v17    # "size":I
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 480
    if-eqz v10, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 481
    const/4 v2, 0x1

    move/from16 v0, p7

    if-ne v0, v2, :cond_13

    .line 482
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v10, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 491
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v8    # "buffer":[B
    .end local v9    # "bytesRead":I
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v18    # "length":J
    .end local v20    # "readLength":J
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_12
    :goto_a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 484
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .restart local v8    # "buffer":[B
    .restart local v9    # "bytesRead":I
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v18    # "length":J
    .restart local v20    # "readLength":J
    .restart local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .restart local v25    # "where":Ljava/lang/String;
    :cond_13
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_a

    .line 488
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_14
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "delete failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 415
    .end local v8    # "buffer":[B
    .end local v9    # "bytesRead":I
    .end local v16    # "isDeleted":Z
    .end local v18    # "length":J
    .end local v20    # "readLength":J
    .restart local v12    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v12

    .line 416
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 417
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 423
    :catch_7
    move-exception v12

    .line 424
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 425
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 435
    :cond_15
    const/16 v16, 0x0

    .line 436
    .restart local v16    # "isDeleted":Z
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v28

    cmp-long v2, v2, v28

    if-nez v2, :cond_16

    .line 437
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v16

    .line 439
    :cond_16
    if-nez p3, :cond_17

    .line 440
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "dstFile is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_17
    if-eqz v16, :cond_1c

    .line 443
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 444
    .restart local v22    # "strBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 445
    const-string v2, "_data"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 448
    .restart local v25    # "where":Ljava/lang/String;
    const-string v5, "_data = ?"

    .line 449
    .restart local v5    # "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p4, v6, v2

    .line 451
    .restart local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 452
    .restart local v11    # "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 453
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    if-eqz v11, :cond_19

    .line 454
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 455
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 456
    .restart local v17    # "size":I
    if-lez v17, :cond_18

    .line 457
    new-instance v10, Landroid/content/ContentValues;

    .end local v10    # "contentValues":Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 458
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    const-string v2, "title"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v2, "mime_type"

    const-string v3, "mime_type"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v2, "_data"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v2, "duration"

    const-string v3, "duration"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v2, "_size"

    const-string v3, "_size"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v2, "date_modified"

    new-instance v3, Ljava/io/File;

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    const-wide/16 v30, 0x3e8

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 464
    const-string v2, "recordingtype"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 465
    const-string v2, "latitude"

    const-string v3, "latitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 466
    const-string v2, "longitude"

    const-string v3, "longitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 467
    const-string v2, "addr"

    const-string v3, "addr"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v2, "track"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string v2, "is_ringtone"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 470
    const-string v2, "is_alarm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const-string v2, "is_notification"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 472
    const-string v2, "label_id"

    const-string v3, "label_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v2, "is_drm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    const-string v2, "album"

    const-string v3, "Sounds"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v2, "is_memo"

    const-string v3, "is_memo"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_18
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 479
    .end local v17    # "size":I
    :cond_19
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 480
    if-eqz v10, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 481
    const/4 v2, 0x1

    move/from16 v0, p7

    if-ne v0, v2, :cond_1b

    .line 482
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v10, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_1a
    :goto_b
    move/from16 v2, v26

    .line 490
    goto/16 :goto_0

    .line 484
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .restart local v25    # "where":Ljava/lang/String;
    :cond_1b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_b

    .line 488
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_1c
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "delete failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    .line 415
    .end local v16    # "isDeleted":Z
    .local v12, "e":Ljava/lang/Exception;
    :catch_8
    move-exception v12

    .line 416
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 417
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 423
    .end local v12    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v12

    .line 424
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 425
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto/16 :goto_7

    .line 435
    .end local v12    # "e":Ljava/io/IOException;
    :cond_1d
    const/16 v16, 0x0

    .line 436
    .restart local v16    # "isDeleted":Z
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v26

    cmp-long v2, v2, v26

    if-nez v2, :cond_1e

    .line 437
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v16

    .line 439
    :cond_1e
    if-nez p3, :cond_1f

    .line 440
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "dstFile is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_1f
    if-eqz v16, :cond_23

    .line 443
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 444
    .restart local v22    # "strBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 445
    const-string v2, "_data"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 448
    .restart local v25    # "where":Ljava/lang/String;
    const-string v5, "_data = ?"

    .line 449
    .restart local v5    # "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p4, v6, v2

    .line 451
    .restart local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 452
    .restart local v11    # "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 453
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    if-eqz v11, :cond_21

    .line 454
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 455
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 456
    .restart local v17    # "size":I
    if-lez v17, :cond_20

    .line 457
    new-instance v10, Landroid/content/ContentValues;

    .end local v10    # "contentValues":Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 458
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    const-string v2, "title"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v2, "mime_type"

    const-string v3, "mime_type"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v2, "_data"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v2, "duration"

    const-string v3, "duration"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v2, "_size"

    const-string v3, "_size"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v2, "date_modified"

    new-instance v3, Ljava/io/File;

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v26

    const-wide/16 v28, 0x3e8

    div-long v26, v26, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 464
    const-string v2, "recordingtype"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 465
    const-string v2, "latitude"

    const-string v3, "latitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 466
    const-string v2, "longitude"

    const-string v3, "longitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 467
    const-string v2, "addr"

    const-string v3, "addr"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v2, "track"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string v2, "is_ringtone"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 470
    const-string v2, "is_alarm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const-string v2, "is_notification"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 472
    const-string v2, "label_id"

    const-string v3, "label_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v2, "is_drm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    const-string v2, "album"

    const-string v3, "Sounds"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v2, "is_memo"

    const-string v3, "is_memo"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_20
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 479
    .end local v17    # "size":I
    :cond_21
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 480
    if-eqz v10, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 481
    const/4 v2, 0x1

    move/from16 v0, p7

    if-ne v0, v2, :cond_22

    .line 482
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v10, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_a

    .line 484
    :cond_22
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_a

    .line 488
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_23
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "delete failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 411
    .end local v16    # "isDeleted":Z
    :catchall_0
    move-exception v2

    move-object/from16 v26, v2

    .line 412
    if-eqz v14, :cond_24

    .line 413
    :try_start_d
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 420
    :cond_24
    :goto_c
    if-eqz v23, :cond_25

    .line 421
    :try_start_e
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 427
    :cond_25
    :goto_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_26

    .line 428
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 429
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v28

    cmp-long v2, v2, v28

    if-eqz v2, :cond_26

    .line 430
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 431
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "srcFile and dstFile size are not same"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 415
    :catch_a
    move-exception v12

    .line 416
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 417
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_c

    .line 423
    .end local v12    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v12

    .line 424
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 425
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_d

    .line 435
    .end local v12    # "e":Ljava/io/IOException;
    :cond_26
    const/16 v16, 0x0

    .line 436
    .restart local v16    # "isDeleted":Z
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_27

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v28

    cmp-long v2, v2, v28

    if-nez v2, :cond_27

    .line 437
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z

    move-result v16

    .line 439
    :cond_27
    if-nez p3, :cond_28

    .line 440
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "dstFile is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_28
    if-eqz v16, :cond_2d

    .line 443
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 444
    .restart local v22    # "strBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 445
    const-string v2, "_data"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 448
    .restart local v25    # "where":Ljava/lang/String;
    const-string v5, "_data = ?"

    .line 449
    .restart local v5    # "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p4, v6, v2

    .line 451
    .restart local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 452
    .restart local v11    # "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 453
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    if-eqz v11, :cond_2a

    .line 454
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 455
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 456
    .restart local v17    # "size":I
    if-lez v17, :cond_29

    .line 457
    new-instance v10, Landroid/content/ContentValues;

    .end local v10    # "contentValues":Landroid/content/ContentValues;
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 458
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    const-string v2, "title"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v2, "mime_type"

    const-string v3, "mime_type"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v2, "_data"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v2, "duration"

    const-string v3, "duration"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v2, "_size"

    const-string v3, "_size"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v2, "date_modified"

    new-instance v3, Ljava/io/File;

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v28

    const-wide/16 v30, 0x3e8

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 464
    const-string v2, "recordingtype"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 465
    const-string v2, "latitude"

    const-string v3, "latitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 466
    const-string v2, "longitude"

    const-string v3, "longitude"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 467
    const-string v2, "addr"

    const-string v3, "addr"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v2, "track"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string v2, "is_ringtone"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 470
    const-string v2, "is_alarm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const-string v2, "is_notification"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 472
    const-string v2, "label_id"

    const-string v3, "label_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v2, "is_drm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    const-string v2, "album"

    const-string v3, "Sounds"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v2, "is_memo"

    const-string v3, "is_memo"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_29
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 479
    .end local v17    # "size":I
    :cond_2a
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 480
    if-eqz v10, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 481
    const/4 v2, 0x1

    move/from16 v0, p7

    if-ne v0, v2, :cond_2c

    .line 482
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v10, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 490
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_2b
    :goto_e
    throw v26

    .line 484
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .restart local v10    # "contentValues":Landroid/content/ContentValues;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .restart local v25    # "where":Ljava/lang/String;
    :cond_2c
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_e

    .line 488
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "deleteSelectionArgs":[Ljava/lang/String;
    .end local v10    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v22    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "where":Ljava/lang/String;
    :cond_2d
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v3, "delete failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_e
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 219
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 22
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    .line 262
    .local v10, "activity":Landroid/app/Activity;
    if-nez v10, :cond_0

    .line 263
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v5, "activity is null"

    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const/4 v2, 0x0

    .line 350
    :goto_0
    return-object v2

    .line 266
    :cond_0
    invoke-virtual {v10}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 267
    .local v3, "context":Landroid/content/Context;
    if-nez v3, :cond_1

    .line 268
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v5, "context is null"

    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const/4 v2, 0x0

    goto :goto_0

    .line 271
    :cond_1
    const/4 v4, 0x0

    .line 273
    .local v4, "srcFile":Ljava/io/File;
    const-wide/16 v18, 0x0

    .line 274
    .local v18, "srcId":J
    const/4 v6, 0x0

    .line 275
    .local v6, "srcPath":Ljava/lang/String;
    const/4 v8, 0x0

    .line 276
    .local v8, "destPath":Ljava/lang/String;
    const/4 v7, 0x0

    .line 277
    .local v7, "destFolderPath":Ljava/lang/String;
    const/4 v12, 0x0

    .line 278
    .local v12, "fileName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 280
    .local v13, "fileNameIdx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getDestFolder(Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$902(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 283
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Long;

    .line 285
    .local v16, "id":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-nez v2, :cond_4

    .line 349
    .end local v16    # "id":Ljava/lang/Long;
    :cond_3
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v9, 0x5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 350
    const/4 v2, 0x0

    goto :goto_0

    .line 287
    .restart local v16    # "id":Ljava/lang/Long;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 288
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v9, 0x4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 289
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 290
    move-wide/from16 v0, v18

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 291
    if-nez v6, :cond_5

    .line 292
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v5, "srcPath is null"

    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 295
    :cond_5
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 296
    const-string v2, "VNPersonalPageOperationUiFragment"

    const-string v5, "srcPath is already in personal page"

    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # operator++ for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$908(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 299
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_1

    .line 302
    :cond_6
    const/16 v2, 0x2f

    invoke-virtual {v6, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v13, v2, 0x1

    .line 303
    invoke-virtual {v6, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 304
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 306
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSecretBox(Landroid/content/Context;)V

    .line 308
    new-instance v4, Ljava/io/File;

    .end local v4    # "srcFile":Ljava/io/File;
    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v4    # "srcFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v5, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    .line 310
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v5, v5, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->fileNameString:Ljava/lang/String;

    .line 311
    const/4 v14, 0x0

    .line 312
    .local v14, "flag":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->showPrivateDialog()V
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 316
    :cond_7
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 318
    const-wide/16 v20, 0x1f4

    :try_start_0
    invoke-static/range {v20 .. v21}, Ljava/lang/Thread;->sleep(J)V

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    move-result-object v2

    if-nez v2, :cond_8

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 329
    :catch_0
    move-exception v11

    .line 330
    .local v11, "e":Ljava/lang/InterruptedException;
    const-string v2, "VNPersonalPageOperationUiFragment"

    invoke-virtual {v11}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 323
    .end local v11    # "e":Ljava/lang/InterruptedException;
    :cond_8
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->isshowing()Z

    move-result v2

    if-nez v2, :cond_7

    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 333
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v2

    if-nez v2, :cond_a

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, -0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    goto/16 :goto_1

    .line 336
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_b

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, -0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v5, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    const/4 v9, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->updatePrivate(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v14

    .line 347
    :goto_3
    if-eqz v14, :cond_2

    const/4 v2, 0x0

    goto/16 :goto_0

    .line 340
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v5, -0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v5, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->updatePrivate(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v14

    goto :goto_3

    .line 345
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v5, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->updatePrivate(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v14

    goto :goto_3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 219
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 592
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissAllowingStateLoss()V

    .line 595
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 10

    .prologue
    .line 224
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v7

    if-nez v7, :cond_3

    .line 225
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 226
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 227
    const-string v7, "VNPersonalPageOperationUiFragment"

    const-string v8, "activity is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    .end local v0    # "activity":Landroid/app/Activity;
    :goto_0
    return-void

    .line 230
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 231
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_1

    .line 232
    const-string v7, "VNPersonalPageOperationUiFragment"

    const-string v8, "context is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 235
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I
    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$302(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 236
    const/4 v6, 0x0

    .line 237
    .local v6, "srcPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 238
    .local v4, "personalRoot":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 239
    .local v3, "originSize":I
    if-lez v3, :cond_3

    .line 240
    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_3

    .line 241
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 242
    if-eqz v6, :cond_2

    invoke-virtual {v6, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 243
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 244
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # operator++ for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$308(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    .line 240
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 249
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "i":I
    .end local v3    # "originSize":I
    .end local v4    # "personalRoot":Ljava/lang/String;
    .end local v6    # "srcPath":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 250
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 251
    .local v5, "size":I
    if-eqz v5, :cond_4

    .line 252
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Landroid/widget/ProgressBar;

    move-result-object v7

    mul-int/lit8 v8, v5, 0x64

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 253
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->UpdateDialog()V
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 256
    :cond_4
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 9
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 496
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 498
    aget-object v4, p1, v7

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 499
    .local v2, "intValue":I
    packed-switch v2, :pswitch_data_0

    .line 587
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 504
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->UpdateDialog()V
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    goto :goto_0

    .line 508
    :pswitch_2
    const-string v4, "VNPersonalPageOperationUiFragment"

    const-string v5, "isCancelled"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1500()Landroid/os/IBinder;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 510
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1600()Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1500()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto :goto_0

    .line 515
    :pswitch_3
    const-string v4, "VNPersonalPageOperationUiFragment"

    const-string v5, "File operation exception"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1500()Landroid/os/IBinder;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 517
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1600()Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1500()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto :goto_0

    .line 522
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->UpdateDialog()V
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    goto :goto_0

    .line 526
    :pswitch_5
    const v4, 0x7f0b015c

    const v5, 0x7f0b00dd

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-lez v4, :cond_0

    .line 532
    const-string v4, "VNPersonalPageOperationUiFragment"

    const-string v5, "isComplete"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 534
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_2

    .line 535
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 537
    .local v3, "resources":Landroid/content/res/Resources;
    if-eqz v3, :cond_1

    .line 538
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-nez v4, :cond_8

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-nez v4, :cond_4

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-ne v4, v8, :cond_3

    .line 541
    const v4, 0x7f0b014a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    .line 569
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 570
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask$1;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;Landroid/app/Activity;)V

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 578
    .end local v3    # "resources":Landroid/content/res/Resources;
    :cond_2
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1500()Landroid/os/IBinder;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 579
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1600()Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1500()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v4, v5, v8}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto/16 :goto_0

    .line 543
    .restart local v3    # "resources":Landroid/content/res/Resources;
    :cond_3
    const v4, 0x7f0b014b

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto :goto_1

    .line 546
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-ne v4, v8, :cond_6

    .line 547
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-ne v4, v8, :cond_5

    .line 548
    const v4, 0x7f0b014f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto :goto_1

    .line 550
    :cond_5
    const v4, 0x7f0b014e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto :goto_1

    .line 553
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-ne v4, v8, :cond_7

    .line 554
    const v4, 0x7f0b014d

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto/16 :goto_1

    .line 556
    :cond_7
    const v4, 0x7f0b014c

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto/16 :goto_1

    .line 561
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Sounds"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 562
    .local v1, "folderPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v4

    if-ne v4, v8, :cond_9

    .line 563
    const v4, 0x7f0b0150

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto/16 :goto_1

    .line 565
    :cond_9
    const v4, 0x7f0b0151

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v1, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->completeToastText:Ljava/lang/String;

    goto/16 :goto_1

    .line 499
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method
