.class Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;
.super Ljava/lang/Object;
.source "VNScrollView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->onGlobalLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;

.field final synthetic val$scrollView:Landroid/widget/HorizontalScrollView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;Landroid/widget/HorizontalScrollView;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->this$1:Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->val$scrollView:Landroid/widget/HorizontalScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->this$1:Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->interview:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->this$1:Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->conversation:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    const-string v0, "record_mode"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->val$scrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->val$scrollView:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;->val$scrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0
.end method
