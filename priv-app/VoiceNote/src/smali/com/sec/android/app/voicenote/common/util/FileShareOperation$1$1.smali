.class Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;
.super Ljava/lang/Object;
.source "FileShareOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x3

    .line 304
    const-string v0, "FileShareOperation"

    const-string v1, "ShareFiles : Cancel interrupt!!!"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$300(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->interrupt()V

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$400(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;->this$1:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$400(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 308
    return-void
.end method
