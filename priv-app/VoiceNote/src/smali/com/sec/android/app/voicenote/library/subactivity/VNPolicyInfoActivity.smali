.class public Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;
.super Landroid/app/Activity;
.source "VNPolicyInfoActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VNPolicyInfoActivity"


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 34
    const-string v0, "VNPolicyInfoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->setContentView(I)V

    .line 37
    const v0, 0x7f0e001d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setClickable(Z)V

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 45
    const-string v0, "VNPolicyInfoActivity"

    const-string v2, "onResume()"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "nuanceinfo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    const v2, 0x7f0b0171

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/text"

    const-string v4, "UTF-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 56
    return-void

    .line 51
    :cond_0
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->setTitle(I)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/text/policy_korea.txt"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNPolicyInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 60
    const-string v0, "VNPolicyInfoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    sparse-switch p1, :sswitch_data_0

    .line 74
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 75
    return-void

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
