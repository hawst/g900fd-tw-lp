.class public Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
.super Ljava/lang/Object;
.source "VNLEDNotiManager.java"


# static fields
.field private static LED_NOTIF_ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VNLEDNotiManager"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0x1427

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->LED_NOTIF_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->mContext:Landroid/content/Context;

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private getNextTimeFromNow(III)J
    .locals 10
    .param p1, "destHour"    # I
    .param p2, "destMin"    # I
    .param p3, "destMinAdder"    # I

    .prologue
    const/16 v1, 0xc

    const/16 v7, 0xb

    .line 48
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 49
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 51
    .local v2, "current":J
    invoke-virtual {v0, v7, p1}, Ljava/util/Calendar;->set(II)V

    .line 52
    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 53
    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->add(II)V

    .line 54
    const/16 v1, 0xd

    const/4 v6, 0x1

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 55
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 56
    .local v4, "dest":J
    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 57
    const-string v1, "VNLEDNotiManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getNextTimeFromNow()1 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v4, v2

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    sub-long v6, v4, v2

    .line 63
    :goto_0
    return-wide v6

    .line 60
    :cond_0
    const/16 v1, 0x18

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->add(II)V

    .line 61
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 62
    const-string v1, "VNLEDNotiManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getNextTimeFromNow()2 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v4, v2

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    sub-long v6, v4, v2

    goto :goto_0
.end method

.method private registerAlarm(Landroid/content/Context;J)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "timeInMillis"    # J

    .prologue
    const/4 v5, 0x0

    .line 68
    const-string v3, "VNLEDNotiManager"

    const-string v4, "registerAlarm()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VRLedAlarm"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 73
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 74
    .local v1, "mAlarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 75
    const/4 v3, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, p2

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 77
    return-void
.end method


# virtual methods
.method public isEnabledLED(Landroid/content/Context;Z)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "registerAlarm"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 123
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "led_indicator_voice_recording"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "led_indicator_missed_event"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-nez v6, :cond_2

    .line 127
    :cond_0
    const-string v5, "VNLEDNotiManager"

    const-string v6, "isEnabledLED() LED indicator voice recording false"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_1
    :goto_0
    return v4

    .line 132
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_switch_onoff"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v5, :cond_6

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_disable_led_indicator"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v5, :cond_6

    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_always"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v5, :cond_3

    .line 138
    const-string v5, "VNLEDNotiManager"

    const-string v6, "isEnabledLED() dormant always false"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_start_hour"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 143
    .local v2, "startHour":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_start_min"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 145
    .local v3, "startMin":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_end_hour"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 147
    .local v0, "endHour":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "dormant_end_min"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 150
    .local v1, "endMin":I
    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->isInnterTime(IIII)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 151
    const-string v6, "VNLEDNotiManager"

    const-string v7, "isEnabledLED() current time is inner limit area"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    if-eqz p2, :cond_1

    .line 154
    invoke-direct {p0, v0, v1, v5}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->getNextTimeFromNow(III)J

    move-result-wide v6

    invoke-direct {p0, p1, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->registerAlarm(Landroid/content/Context;J)V

    goto :goto_0

    .line 158
    :cond_4
    const-string v6, "VNLEDNotiManager"

    const-string v7, "isEnabledLED() current time is out of limit area"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    if-eqz p2, :cond_5

    .line 161
    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->getNextTimeFromNow(III)J

    move-result-wide v6

    invoke-direct {p0, p1, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->registerAlarm(Landroid/content/Context;J)V

    :cond_5
    move v4, v5

    .line 163
    goto :goto_0

    .end local v0    # "endHour":I
    .end local v1    # "endMin":I
    .end local v2    # "startHour":I
    .end local v3    # "startMin":I
    :cond_6
    move v4, v5

    .line 170
    goto :goto_0
.end method

.method public isInnterTime(IIII)Z
    .locals 15
    .param p1, "startHour"    # I
    .param p2, "startMin"    # I
    .param p3, "endHour"    # I
    .param p4, "endMin"    # I

    .prologue
    .line 90
    const/4 v5, 0x0

    .line 92
    .local v5, "rst":Z
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 93
    .local v2, "c":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 95
    .local v8, "now":J
    const/16 v12, 0xb

    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 96
    .local v3, "curHour":I
    const/16 v12, 0xc

    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 97
    .local v4, "curMin":I
    const-string v12, "VNLEDNotiManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "isInnterTime : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/16 v12, 0xb

    move/from16 v0, p1

    invoke-virtual {v2, v12, v0}, Ljava/util/Calendar;->set(II)V

    .line 101
    const/16 v12, 0xc

    move/from16 v0, p2

    invoke-virtual {v2, v12, v0}, Ljava/util/Calendar;->set(II)V

    .line 102
    const/16 v12, 0xd

    const/4 v13, 0x1

    invoke-virtual {v2, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 103
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 105
    .local v10, "start":J
    const/16 v12, 0xb

    move/from16 v0, p3

    invoke-virtual {v2, v12, v0}, Ljava/util/Calendar;->set(II)V

    .line 106
    const/16 v12, 0xc

    add-int/lit8 v13, p4, 0x1

    invoke-virtual {v2, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 107
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 109
    .local v6, "end":J
    cmp-long v12, v10, v8

    if-gtz v12, :cond_1

    cmp-long v12, v8, v6

    if-gez v12, :cond_1

    .line 110
    const/4 v5, 0x1

    .line 119
    :cond_0
    :goto_0
    return v5

    .line 111
    :cond_1
    cmp-long v12, v8, v6

    if-gez v12, :cond_2

    cmp-long v12, v6, v10

    if-gez v12, :cond_2

    .line 112
    const/4 v5, 0x1

    goto :goto_0

    .line 113
    :cond_2
    cmp-long v12, v6, v10

    if-gez v12, :cond_3

    cmp-long v12, v10, v8

    if-gtz v12, :cond_3

    .line 114
    const/4 v5, 0x1

    goto :goto_0

    .line 115
    :cond_3
    cmp-long v12, v6, v10

    if-nez v12, :cond_0

    .line 116
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public setLEDNotification(Z)V
    .locals 8
    .param p1, "ledOn"    # Z

    .prologue
    const/16 v7, 0x1f4

    const/16 v6, 0xff

    const/4 v5, 0x0

    .line 174
    const-string v2, "VNLEDNotiManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLEDNotification() LED ON : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->mContext:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 177
    .local v1, "notifMgr":Landroid/app/NotificationManager;
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->isEnabledLED(Landroid/content/Context;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 178
    const-string v2, "VNLEDNotiManager"

    const-string v3, "setLEDNotification() flag change off"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const/4 p1, 0x0

    .line 184
    :goto_0
    if-eqz p1, :cond_1

    .line 185
    const-string v2, "VNLEDNotiManager"

    const-string v3, "setLEDNotification() LED ON"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    .line 187
    .local v0, "notif":Landroid/app/Notification;
    invoke-static {v6, v5, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    iput v2, v0, Landroid/app/Notification;->ledARGB:I

    .line 188
    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/app/Notification;->flags:I

    .line 189
    iput v7, v0, Landroid/app/Notification;->ledOnMS:I

    .line 190
    iput v7, v0, Landroid/app/Notification;->ledOffMS:I

    .line 191
    sget v2, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->LED_NOTIF_ID:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 196
    .end local v0    # "notif":Landroid/app/Notification;
    :goto_1
    return-void

    .line 181
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->unregisterAlarm(Landroid/content/Context;)V

    goto :goto_0

    .line 193
    :cond_1
    const-string v2, "VNLEDNotiManager"

    const-string v3, "setLEDNotification() LED OFF"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    sget v2, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->LED_NOTIF_ID:I

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1
.end method

.method public unregisterAlarm(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 80
    const-string v3, "VNLEDNotiManager"

    const-string v4, "unregisterAlarm()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 83
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 85
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 86
    .local v1, "manager":Landroid/app/AlarmManager;
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 87
    return-void
.end method
