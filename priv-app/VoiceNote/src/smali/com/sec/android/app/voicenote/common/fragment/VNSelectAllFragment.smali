.class public Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;
.super Landroid/app/Fragment;
.source "VNSelectAllFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final SELECTALL_FRAGMENT_TAG:Ljava/lang/String; = "selectall_fragment"

.field private static final TAG:Ljava/lang/String; = "VNSelectAllFragment"


# instance fields
.field mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

.field mSelectAllCheckBox:Landroid/widget/CheckBox;

.field mSelectAllTextView:Landroid/widget/TextView;

.field mSelectAllView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllTextView:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 32
    return-void
.end method

.method private constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V
    .locals 1
    .param p1, "actionmodeListener"    # Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllTextView:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .line 37
    return-void
.end method

.method public static attachSelectAllFragment(Landroid/app/FragmentManager;ILcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V
    .locals 5
    .param p0, "fm"    # Landroid/app/FragmentManager;
    .param p1, "targetId"    # I
    .param p2, "actionmodeListener"    # Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .prologue
    .line 45
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->findFragment(Landroid/app/FragmentManager;)Landroid/app/Fragment;

    move-result-object v1

    .line 46
    .local v1, "fragment":Landroid/app/Fragment;
    instance-of v3, v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;

    if-nez v3, :cond_0

    .line 48
    :try_start_0
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 49
    .local v2, "ft":Landroid/app/FragmentTransaction;
    invoke-static {p2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->newInstance(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;

    move-result-object v3

    const-string v4, "selectall_fragment"

    invoke-virtual {v2, p1, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 51
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    :goto_0
    return-void

    .line 52
    .restart local v1    # "fragment":Landroid/app/Fragment;
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 56
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    check-cast v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;

    .end local v1    # "fragment":Landroid/app/Fragment;
    invoke-virtual {v1, p2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->setActionModeListener(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V

    goto :goto_0
.end method

.method public static findFragment(Landroid/app/FragmentManager;)Landroid/app/Fragment;
    .locals 1
    .param p0, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 75
    if-eqz p0, :cond_0

    .line 76
    const-string v0, "selectall_fragment"

    invoke-virtual {p0, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;
    .locals 1
    .param p0, "actionmodeListener"    # Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .prologue
    .line 40
    new-instance v0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;-><init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V

    return-object v0
.end method

.method public static removeSelectAllFragment(Landroid/app/FragmentManager;)V
    .locals 3
    .param p0, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 61
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->findFragment(Landroid/app/FragmentManager;)Landroid/app/Fragment;

    move-result-object v0

    .line 62
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;

    if-eqz v2, :cond_0

    .line 63
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 64
    .local v1, "ft":Landroid/app/FragmentTransaction;
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 68
    .restart local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_1
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 69
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 110
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 111
    const v1, 0x7f020126

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    if-eqz v0, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->selectAll(Z)V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->refreshTitle()V

    .line 180
    :cond_0
    return-void

    .line 177
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const v1, 0x7f030029

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    const v2, 0x7f0e0062

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllTextView:Landroid/widget/TextView;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    const v2, 0x7f0e0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->refreshTitle()V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllView:Landroid/view/View;

    return-object v1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 121
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 122
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 124
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 125
    return-void
.end method

.method public refreshTitle()V
    .locals 3

    .prologue
    .line 133
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    .line 135
    .local v1, "listview":Landroid/widget/ListView;
    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    .line 136
    .local v0, "cnt":I
    const v2, 0x7f0b0116

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->setTitle(I)V

    .line 137
    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 138
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->setCheckBox(Z)V

    .line 144
    .end local v0    # "cnt":I
    .end local v1    # "listview":Landroid/widget/ListView;
    :cond_0
    :goto_0
    return-void

    .line 141
    .restart local v0    # "cnt":I
    .restart local v1    # "listview":Landroid/widget/ListView;
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->setCheckBox(Z)V

    goto :goto_0
.end method

.method public setActionModeListener(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V
    .locals 0
    .param p1, "actionmodeListener"    # Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mActionmodeListener:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->refreshTitle()V

    .line 130
    return-void
.end method

.method public setCheckBox(Z)V
    .locals 2
    .param p1, "checked"    # Z

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->post(Ljava/lang/Runnable;)Z

    .line 172
    :cond_0
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "strId"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->mSelectAllTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 150
    :cond_0
    return-void
.end method
