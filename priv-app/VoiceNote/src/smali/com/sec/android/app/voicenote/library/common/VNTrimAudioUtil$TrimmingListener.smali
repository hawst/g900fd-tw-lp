.class public Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;
.super Ljava/lang/Object;
.source "VNTrimAudioUtil.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TrimmingListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private pushMediaDataToDB(Ljava/io/File;)Z
    .locals 20
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 207
    const/4 v13, 0x0

    .line 210
    .local v13, "value":Ljava/lang/String;
    :try_start_0
    new-instance v11, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v11}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 211
    .local v11, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 213
    const/16 v14, 0x9

    invoke-virtual {v11, v14}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v13

    .line 214
    invoke-virtual {v11}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 215
    const/4 v11, 0x0

    .line 216
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    int-to-long v6, v14

    .line 218
    .local v6, "fileDuration":J
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 219
    .local v5, "intDirIndex":I
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    .line 221
    .local v8, "intExtIndex":I
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    add-int/lit8 v15, v5, 0x1

    invoke-virtual {v14, v15, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 222
    .local v12, "title":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 223
    .local v4, "ext":Ljava/lang/String;
    const/4 v9, 0x0

    .line 224
    .local v9, "mimeType":Ljava/lang/String;
    const-string v14, ".amr"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 225
    const-string v9, "audio/amr"

    .line 230
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 231
    .local v2, "contentValues":Landroid/content/ContentValues;
    const-string v14, "title"

    invoke-virtual {v2, v14, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v14, "mime_type"

    invoke-virtual {v2, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v14, "_data"

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v14, "duration"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 235
    const-string v14, "_size"

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 236
    const-string v14, "date_modified"

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 237
    const-string v14, "recordingtype"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 238
    const-string v14, "label_id"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    const-string v14, "album"

    const-string v15, "Sounds"

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 242
    const-string v14, "recorded_number"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getRecordedNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v14

    const-string v15, "CscFeature_VoiceRecorder_SaveAsSoundFile"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 247
    const-string v14, "is_sound"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 252
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 253
    .local v10, "personalPageRoot":Ljava/lang/String;
    if-eqz v10, :cond_2

    .line 254
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 255
    const-string v14, "is_secretbox"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 258
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14, v15, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v14

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastSavedFileUri:Landroid/net/Uri;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$902(Landroid/net/Uri;)Landroid/net/Uri;

    .line 261
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastSavedFileUri:Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$900()Landroid/net/Uri;

    move-result-object v14

    if-nez v14, :cond_3

    .line 262
    const-string v14, "VNTrimAudioUtil"

    const-string v15, "Content Resolver insert failed"

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f0b0021

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 275
    :cond_3
    const/4 v14, 0x1

    .end local v2    # "contentValues":Landroid/content/ContentValues;
    .end local v4    # "ext":Ljava/lang/String;
    .end local v5    # "intDirIndex":I
    .end local v6    # "fileDuration":J
    .end local v8    # "intExtIndex":I
    .end local v9    # "mimeType":Ljava/lang/String;
    .end local v10    # "personalPageRoot":Ljava/lang/String;
    .end local v11    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v12    # "title":Ljava/lang/String;
    :goto_2
    return v14

    .line 226
    .restart local v4    # "ext":Ljava/lang/String;
    .restart local v5    # "intDirIndex":I
    .restart local v6    # "fileDuration":J
    .restart local v8    # "intExtIndex":I
    .restart local v9    # "mimeType":Ljava/lang/String;
    .restart local v11    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v12    # "title":Ljava/lang/String;
    :cond_4
    const-string v14, ".m4a"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 227
    const-string v9, "audio/mp4"

    goto/16 :goto_0

    .line 249
    .restart local v2    # "contentValues":Landroid/content/ContentValues;
    :cond_5
    const-string v14, "is_music"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 265
    .end local v2    # "contentValues":Landroid/content/ContentValues;
    .end local v4    # "ext":Ljava/lang/String;
    .end local v5    # "intDirIndex":I
    .end local v6    # "fileDuration":J
    .end local v8    # "intExtIndex":I
    .end local v9    # "mimeType":Ljava/lang/String;
    .end local v11    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v12    # "title":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 266
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 267
    const-string v14, "VNTrimAudioUtil"

    const-string v15, "error occurred while extractMetadata"

    invoke-static {v14, v15, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 268
    const/4 v14, 0x0

    goto :goto_2

    .line 269
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v3

    .line 270
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 271
    const-string v14, "VNTrimAudioUtil"

    const-string v15, "error occurred while input data to MediaStore"

    invoke-static {v14, v15, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 272
    const/4 v14, 0x0

    goto :goto_2
.end method

.method private updateMediaDataToDB(Ljava/io/File;)Z
    .locals 14
    .param p1, "file"    # Ljava/io/File;

    .prologue
    const/4 v8, 0x0

    .line 280
    const/4 v6, 0x0

    .line 283
    .local v6, "value":Ljava/lang/String;
    :try_start_0
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 284
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 286
    const/16 v9, 0x9

    invoke-virtual {v1, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    .line 287
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 288
    const/4 v1, 0x0

    .line 289
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 291
    .local v2, "fileDuration":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 292
    .local v4, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 293
    const-string v9, "_data"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " = \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 296
    .local v7, "where":Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 297
    .local v5, "v":Landroid/content/ContentValues;
    const-string v9, "duration"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 298
    const-string v9, "_size"

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 299
    const-string v9, "date_modified"

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 301
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v5, v7, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 315
    const/4 v8, 0x1

    .end local v1    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v2    # "fileDuration":J
    .end local v4    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v5    # "v":Landroid/content/ContentValues;
    .end local v7    # "where":Ljava/lang/String;
    :goto_0
    return v8

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 305
    const-string v9, "VNTrimAudioUtil"

    const-string v10, "error occurred while extractMetadata"

    invoke-static {v9, v10, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 307
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 308
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0

    .line 310
    .end local v0    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    :catch_2
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 312
    const-string v9, "VNTrimAudioUtil"

    const-string v10, "error occurred while input data to MediaStore"

    invoke-static {v9, v10, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onCompletion(I)V
    .locals 14
    .param p1, "task"    # I

    .prologue
    .line 129
    const-string v11, "VNTrimAudioUtil"

    const-string v12, "**** Trimming onCompletion() in."

    invoke-static {v11, v12}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$100(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->releaseTrimming()Z

    .line 132
    const/4 v7, 0x0

    .line 133
    .local v7, "renamed":Ljava/io/File;
    const/4 v9, 0x1

    .line 136
    .local v9, "retval":Z
    :try_start_0
    new-instance v10, Ljava/io/File;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mTrimmedFile:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$200(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    .local v10, "trimmedFile":Ljava/io/File;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mIsOverride:Z
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 139
    new-instance v8, Ljava/io/File;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 140
    .end local v7    # "renamed":Ljava/io/File;
    .local v8, "renamed":Ljava/io/File;
    :try_start_1
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 141
    invoke-virtual {v10, v8}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 142
    sget-object v12, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v12
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 143
    :try_start_2
    new-instance v6, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 144
    .local v6, "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v4

    .line 146
    .local v4, "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    if-eqz v4, :cond_0

    .line 147
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;

    invoke-direct {v1, v4}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 148
    .local v1, "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimStartTime:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$500(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v11

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimEndTime:I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v13

    invoke-virtual {v1, v11, v13}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->trimBookmarks(II)V

    .line 151
    .end local v1    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    :cond_0
    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v4

    .line 152
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v11

    invoke-direct {v0, v4, v11}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 153
    .local v0, "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimStartTime:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$500(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v11

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimEndTime:I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v13

    invoke-virtual {v0, v11, v13}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->trimSttData(II)V

    .line 154
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    :try_start_3
    invoke-direct {p0, v8}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->updateMediaDataToDB(Ljava/io/File;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result v9

    move-object v7, v8

    .line 180
    .end local v0    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v4    # "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v6    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v8    # "renamed":Ljava/io/File;
    .end local v10    # "trimmedFile":Ljava/io/File;
    .restart local v7    # "renamed":Ljava/io/File;
    :goto_0
    if-nez v9, :cond_3

    .line 181
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x48

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 182
    .local v5, "msg":Landroid/os/Message;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v11

    const-wide/16 v12, 0xc8

    invoke-virtual {v11, v5, v12, v13}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 188
    :goto_1
    return-void

    .line 154
    .end local v5    # "msg":Landroid/os/Message;
    .end local v7    # "renamed":Ljava/io/File;
    .restart local v8    # "renamed":Ljava/io/File;
    .restart local v10    # "trimmedFile":Ljava/io/File;
    :catchall_0
    move-exception v11

    :try_start_4
    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v11
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 175
    :catch_0
    move-exception v3

    move-object v7, v8

    .line 176
    .end local v8    # "renamed":Ljava/io/File;
    .end local v10    # "trimmedFile":Ljava/io/File;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v7    # "renamed":Ljava/io/File;
    :goto_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 177
    const/4 v9, 0x0

    goto :goto_0

    .line 157
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v10    # "trimmedFile":Ljava/io/File;
    :cond_1
    :try_start_6
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;
    invoke-static {v12}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;

    move-result-object v12

    # invokes: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->getDestPath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v11, v12}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$800(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "dest":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 159
    .end local v7    # "renamed":Ljava/io/File;
    .restart local v8    # "renamed":Ljava/io/File;
    :try_start_7
    invoke-virtual {v10, v8}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 160
    sget-object v12, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v12
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 161
    :try_start_8
    new-instance v6, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    invoke-direct {v6, v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 162
    .restart local v6    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v4

    .line 164
    .restart local v4    # "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    if-eqz v4, :cond_2

    .line 165
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;

    invoke-direct {v1, v4}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 166
    .restart local v1    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimStartTime:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$500(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v11

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimEndTime:I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v13

    invoke-virtual {v1, v11, v13}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->trimBookmarks(II)V

    .line 169
    .end local v1    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    :cond_2
    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v4

    .line 170
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v11

    invoke-direct {v0, v4, v11}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 171
    .restart local v0    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimStartTime:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$500(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v11

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimEndTime:I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I

    move-result v13

    invoke-virtual {v0, v11, v13}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->trimSttData(II)V

    .line 172
    monitor-exit v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 173
    :try_start_9
    invoke-direct {p0, v8}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->pushMediaDataToDB(Ljava/io/File;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    move-result v9

    move-object v7, v8

    .end local v8    # "renamed":Ljava/io/File;
    .restart local v7    # "renamed":Ljava/io/File;
    goto :goto_0

    .line 172
    .end local v0    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v4    # "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v6    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v7    # "renamed":Ljava/io/File;
    .restart local v8    # "renamed":Ljava/io/File;
    :catchall_1
    move-exception v11

    :try_start_a
    monitor-exit v12
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v11
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    .line 186
    .end local v2    # "dest":Ljava/lang/String;
    .end local v8    # "renamed":Ljava/io/File;
    .end local v10    # "trimmedFile":Ljava/io/File;
    .restart local v7    # "renamed":Ljava/io/File;
    :cond_3
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x46

    invoke-static {v11, v12, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 187
    .restart local v5    # "msg":Landroid/os/Message;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v11

    const-wide/16 v12, 0xc8

    invoke-virtual {v11, v5, v12, v13}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 175
    .end local v5    # "msg":Landroid/os/Message;
    :catch_1
    move-exception v3

    goto :goto_2
.end method

.method public onError(II)V
    .locals 5
    .param p1, "task"    # I
    .param p2, "error"    # I

    .prologue
    const/16 v4, 0x65

    .line 199
    const-string v1, "VNTrimAudioUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trimming onError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v4, p2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 202
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 203
    return-void
.end method

.method public onProgress(II)V
    .locals 4
    .param p1, "task"    # I
    .param p2, "progress"    # I

    .prologue
    const/16 v3, 0x3e

    .line 121
    const-string v1, "VNTrimAudioUtil"

    const-string v2, "**** Trimming onProgress() in."

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v3, p2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 124
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 125
    return-void
.end method

.method public onWarning(II)V
    .locals 4
    .param p1, "task"    # I
    .param p2, "warning"    # I

    .prologue
    const/16 v3, 0x67

    .line 192
    const-string v0, "VNTrimAudioUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trimming onWarning: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 195
    return-void
.end method
