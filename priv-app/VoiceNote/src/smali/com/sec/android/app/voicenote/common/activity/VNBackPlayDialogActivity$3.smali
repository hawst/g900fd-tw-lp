.class Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$3;
.super Ljava/lang/Object;
.source "VNBackPlayDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$3;->this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v3, 0x1

    .line 132
    # getter for: Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 133
    # getter for: Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    .line 135
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$3;->this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "isFromQuick"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 137
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 138
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 139
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$3;->this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 141
    return-void
.end method
