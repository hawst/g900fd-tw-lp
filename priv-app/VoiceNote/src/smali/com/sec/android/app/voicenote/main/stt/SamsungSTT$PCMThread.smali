.class Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;
.super Ljava/lang/Thread;
.source "SamsungSTT.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PCMThread"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 340
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 343
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 344
    # operator++ for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$908()I

    .line 346
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$900()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 347
    const-string v0, "SamsungSTT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PCM_WRITE_START : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$900()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1000()Landroid/media/AudioManager;

    move-result-object v0

    const-string v1, "voice_note_recording=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 353
    :goto_1
    const-wide/16 v0, 0x4e20

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 350
    :cond_0
    const-string v0, "SamsungSTT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PCM_WRITE_CONTINUE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$900()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1000()Landroid/media/AudioManager;

    move-result-object v0

    const-string v1, "voice_note_recording=conti"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_1

    .line 355
    :cond_1
    return-void
.end method
