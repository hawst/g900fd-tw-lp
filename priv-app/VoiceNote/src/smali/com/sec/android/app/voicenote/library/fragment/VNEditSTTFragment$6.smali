.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;
.super Landroid/os/Handler;
.source "VNEditSTTFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 765
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 784
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 785
    return-void

    .line 767
    :pswitch_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 768
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isActivated()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 769
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setKeepScreenOn(Z)V

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsKeepScreenOn:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1902(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    goto :goto_0

    .line 773
    :cond_2
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 774
    .local v0, "msg1":Landroid/os/Message;
    const v1, 0xf4241

    iput v1, v0, Landroid/os/Message;->what:I

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v1, v1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 776
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v1, v1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 765
    nop

    :pswitch_data_0
    .packed-switch 0xf4241
        :pswitch_0
    .end packed-switch
.end method
