.class public Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;
.super Landroid/app/Fragment;
.source "ExpandableListFragment.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupCollapseListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# static fields
.field static final INTERNAL_EMPTY_ID:I = 0xff0001


# instance fields
.field mAdapter:Landroid/widget/ExpandableListAdapter;

.field mEmptyView:Landroid/view/View;

.field mFinishedStart:Z

.field private final mHandler:Landroid/os/Handler;

.field mList:Landroid/widget/ExpandableListView;

.field mListContainer:Landroid/view/View;

.field mListShown:Z

.field private final mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mRequestFocus:Ljava/lang/Runnable;

.field mSetEmptyText:Z

.field mStandardEmptyView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 31
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mHandler:Landroid/os/Handler;

    .line 33
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mRequestFocus:Ljava/lang/Runnable;

    .line 39
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mFinishedStart:Z

    .line 56
    return-void
.end method

.method private ensureList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 256
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    if-eqz v2, :cond_0

    .line 297
    :goto_0
    return-void

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 260
    .local v1, "root":Landroid/view/View;
    if-nez v1, :cond_1

    .line 261
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Content view not yet created"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 263
    :cond_1
    instance-of v2, v1, Landroid/widget/ExpandableListView;

    if-eqz v2, :cond_3

    .line 264
    check-cast v1, Landroid/widget/ExpandableListView;

    .end local v1    # "root":Landroid/view/View;
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    .line 287
    :cond_2
    :goto_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListShown:Z

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mAdapter:Landroid/widget/ExpandableListAdapter;

    if-eqz v2, :cond_7

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->setListAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 296
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 266
    .restart local v1    # "root":Landroid/view/View;
    :cond_3
    const v2, 0xff0001

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    if-nez v2, :cond_4

    .line 268
    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mEmptyView:Landroid/view/View;

    .line 270
    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListContainer:Landroid/view/View;

    .line 271
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 272
    .local v0, "rawListView":Landroid/view/View;
    instance-of v2, v0, Landroid/widget/ExpandableListView;

    if-nez v2, :cond_6

    .line 273
    if-nez v0, :cond_5

    .line 274
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Your content must have a ExpandableListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 278
    :cond_5
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Content has view with id attribute \'android.R.id.list\' that is not a ExpandableListView class"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 282
    :cond_6
    check-cast v0, Landroid/widget/ExpandableListView;

    .end local v0    # "rawListView":Landroid/view/View;
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_1

    .line 294
    .end local v1    # "root":Landroid/view/View;
    :cond_7
    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->setListShown(ZZ)V

    goto :goto_2
.end method

.method private setListShown(ZZ)V
    .locals 3
    .param p1, "shown"    # Z
    .param p2, "animate"    # Z

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 226
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListShown:Z

    if-ne v0, p1, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 232
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListShown:Z

    .line 233
    if-eqz p1, :cond_3

    .line 234
    if-eqz p2, :cond_2

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 240
    :cond_3
    if-eqz p2, :cond_4

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x10a0001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 244
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mAdapter:Landroid/widget/ExpandableListAdapter;

    return-object v0
.end method

.method public getExpandableListView()Landroid/widget/ExpandableListView;
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method public getSelectedId()J
    .locals 2

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getSelectedId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedPosition()J
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getSelectedPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 103
    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 1
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 320
    const/4 v0, 0x0

    return v0
.end method

.method public onContentChanged()V
    .locals 3

    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 330
    .local v0, "emptyView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ExpandableListView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    if-nez v1, :cond_0

    .line 332
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Your content must have a ExpandableListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 336
    :cond_0
    if-eqz v0, :cond_1

    .line 337
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 339
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 344
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mFinishedStart:Z

    if-eqz v1, :cond_2

    .line 345
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->setListAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 347
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mFinishedStart:Z

    .line 348
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 325
    const-string v0, "ExpandableListFragment"

    const-string v1, "onCreateContextMenu"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    .line 75
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 77
    .local v2, "root":Landroid/widget/FrameLayout;
    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 78
    .local v3, "tv":Landroid/widget/TextView;
    const v4, 0xff0001

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setId(I)V

    .line 79
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 80
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    new-instance v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 84
    .local v1, "lv":Landroid/widget/ListView;
    const v4, 0x102000a

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setId(I)V

    .line 85
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setDrawSelectorOnTop(Z)V

    .line 86
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 91
    .local v0, "lp":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    return-object v2
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    .line 112
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 113
    return-void
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 1
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method

.method public onGroupCollapse(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 309
    return-void
.end method

.method public onGroupExpand(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 303
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 127
    return-void
.end method

.method public setEmptyText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mSetEmptyText:Z

    if-nez v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mSetEmptyText:Z

    .line 186
    :cond_1
    return-void
.end method

.method public setListAdapter(Landroid/widget/ExpandableListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ExpandableListAdapter;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mAdapter:Landroid/widget/ExpandableListAdapter;

    if-eqz v3, :cond_2

    move v0, v1

    .line 134
    .local v0, "hadAdapter":Z
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mAdapter:Landroid/widget/ExpandableListAdapter;

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    if-eqz v3, :cond_1

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, p1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 137
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mListShown:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->setListShown(ZZ)V

    .line 143
    :cond_1
    return-void

    .end local v0    # "hadAdapter":Z
    :cond_2
    move v0, v2

    .line 133
    goto :goto_0
.end method

.method public setListShown(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 203
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->setListShown(ZZ)V

    .line 204
    return-void
.end method

.method public setListShownNoAnimation(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->setListShown(ZZ)V

    .line 212
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->ensureList()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->setSelection(I)V

    .line 154
    return-void
.end method
