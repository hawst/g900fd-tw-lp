.class public Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "VNSetAsDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SetAsDialogAdapter"
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    .line 424
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 426
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 427
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 436
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    mul-int/lit8 v2, p1, 0x2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 441
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 446
    const/4 v0, 0x0

    .line 448
    .local v0, "name":Landroid/widget/TextView;
    if-nez p2, :cond_0

    .line 449
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030023

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 450
    const v1, 0x1020014

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "name":Landroid/widget/TextView;
    check-cast v0, Landroid/widget/TextView;

    .line 451
    .restart local v0    # "name":Landroid/widget/TextView;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 455
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 456
    return-object p2

    .line 453
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "name":Landroid/widget/TextView;
    check-cast v0, Landroid/widget/TextView;

    .restart local v0    # "name":Landroid/widget/TextView;
    goto :goto_0
.end method
