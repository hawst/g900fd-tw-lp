.class public Lcom/sec/android/app/voicenote/main/VNMainActivity;
.super Landroid/app/Activity;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
.implements Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;,
        Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;,
        Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;,
        Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;,
        Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    }
.end annotation


# static fields
.field private static final ACTIONMODE_STATE:Ljava/lang/String; = "is_actionmode"

.field public static final ACTIVITY_MODE:Ljava/lang/String; = "activitymode"

.field public static final ANIM_SYNC_KEY:Ljava/lang/Object;

.field private static final CURRENT_CHECK_ITEM_ID:Ljava/lang/String; = "current_check_item_id"

.field private static final FILE_NAME:Ljava/lang/String; = "filename"

.field public static final FILE_TO_PLAY:Ljava/lang/String; = "file_to_play"

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field public static final MAINCONTROLBAR:Ljava/lang/String; = "VNMainActivityControlButtonFragment"

.field private static final MENU_DEL:I = 0x2

.field private static final MENU_MODE:I = 0x3

.field private static final MENU_SCAN:I = 0x1

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final SEARCH_KEYPAD:Ljava/lang/String; = "keypad"

.field private static final SEARCH_MODE:Ljava/lang/String; = "searchMode"

.field private static final SEARCH_TEXT:Ljava/lang/String; = "searchText"

.field private static final TAG:Ljava/lang/String; = "VoiceNoteMainActivity"

.field public static isAttachMode:Z

.field private static isAttachModeRecording:Z

.field public static isKeepListActivity:Z

.field private static isbtnAnim:Z

.field protected static mDvfsHelper:Landroid/os/DVFSHelper;

.field public static mFilename:Ljava/lang/String;

.field public static mPreviousActivity:Landroid/app/Activity;

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field public static needToFinish:Z


# instance fields
.field private actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

.field private bShowKeyboard:Z

.field private isActionMode:Z

.field private isActionbarUpdate:Z

.field private isAnimating:Z

.field private isAttachModeActivityCreate:Z

.field private isPressEditBtn:Z

.field private isResultRecord:Z

.field private isShownPopupMenu:Z

.field private mATTVVMEnabled:Z

.field private mActivityMode:I

.field private mAmplitude:I

.field private mBackkeyPress:Z

.field private mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

.field private mCancelButton:Landroid/widget/Button;

.field mCurrentCheckedItemID:J

.field private mDeleteItem:Landroid/view/MenuItem;

.field private mDoneButton:Landroid/widget/Button;

.field private mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

.field private mEasyModeReceiver:Landroid/content/BroadcastReceiver;

.field private final mEventHandler:Landroid/os/Handler;

.field private mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

.field mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

.field private mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

.field private final mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

.field private final mIServiceConnection:Landroid/content/ServiceConnection;

.field private mIntentFilter:[Landroid/content/IntentFilter;

.field private mIsEasymode:Z

.field private mIsEnterAddCategory:Z

.field public mIsFromAttachMode:Z

.field private mIsNeedResume:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLastPressedMenuKey:J

.field private mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

.field private mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

.field private mMainFragmentWrapperView:Landroid/view/View;

.field private mMimetype:Ljava/lang/String;

.field private mModeItem:Landroid/view/MenuItem;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mPlayIdFromFindo:J

.field private mPopupMenu:Landroid/widget/PopupMenu;

.field private mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

.field private mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

.field private mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

.field private mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

.field private mRegistedIService:Z

.field private mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

.field private mSaveMmsModeState:Z

.field private mSavedActivityMode:I

.field private mSearchItem:Landroid/view/MenuItem;

.field private mSearchLayout:Landroid/view/View;

.field private mSearchProgressbar:Landroid/widget/ProgressBar;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

.field private mSizeMMS:J

.field private mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

.field private final mUIUpdateHandler:Landroid/os/Handler;

.field private mUVThreshold:I

.field private mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

.field private mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

.field private msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

.field private queryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private techListsArray:[[Ljava/lang/String;

.field private toolbarHelp:Landroid/view/View;

.field private windowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 158
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    .line 169
    sput-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 177
    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    .line 187
    sput-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    .line 202
    sput-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 219
    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 221
    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeRecording:Z

    .line 223
    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isbtnAnim:Z

    .line 254
    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isKeepListActivity:Z

    .line 255
    sput-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 148
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 159
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 160
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRegistedIService:Z

    .line 161
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    .line 162
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    .line 163
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    .line 164
    iput v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I

    .line 165
    const/high16 v0, 0x10000

    iput v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUVThreshold:I

    .line 166
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 167
    iput-wide v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    .line 168
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    .line 170
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z

    .line 171
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    .line 172
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResultRecord:Z

    .line 173
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPlayIdFromFindo:J

    .line 174
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 175
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    .line 176
    iput-wide v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLastPressedMenuKey:J

    .line 178
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 179
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPendingIntent:Landroid/app/PendingIntent;

    .line 180
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    move-object v0, v1

    .line 181
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->techListsArray:[[Ljava/lang/String;

    .line 185
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    .line 186
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    .line 199
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .line 200
    iput v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 201
    iget v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    iput v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    .line 203
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    .line 204
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    .line 205
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    .line 206
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 208
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsNeedResume:Z

    .line 212
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .line 213
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    .line 214
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    .line 215
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isPressEditBtn:Z

    .line 216
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionbarUpdate:Z

    .line 220
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z

    .line 226
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    .line 227
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .line 228
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    .line 229
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    .line 230
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 231
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 232
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 233
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .line 235
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    .line 237
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    .line 239
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->windowManager:Landroid/view/WindowManager;

    .line 243
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    .line 244
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEnterAddCategory:Z

    .line 249
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .line 252
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEasymode:Z

    .line 253
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSaveMmsModeState:Z

    .line 258
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPowerManager:Landroid/os/PowerManager;

    .line 259
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 927
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    .line 928
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    .line 929
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;

    .line 2064
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceConnection:Landroid/content/ServiceConnection;

    .line 2171
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    .line 2400
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    .line 2578
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    .line 4210
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    .line 4566
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    .line 4745
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->queryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    .line 4941
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$19;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    return-void
.end method

.method private MakeLibraryMenuPopup(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v0, 0x0

    .line 4895
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4903
    :goto_0
    return v0

    .line 4899
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->preparePopupMenu(Landroid/view/Menu;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4900
    const/4 v0, 0x1

    goto :goto_0

    .line 4902
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    goto :goto_0
.end method

.method private MakeMainMenuPopup(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 4842
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v0, :cond_1

    .line 4843
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 4844
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4845
    const v0, 0x7f0e0109

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 4847
    :cond_0
    const/4 v0, 0x1

    .line 4850
    :goto_0
    return v0

    .line 4849
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 4850
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawLibraryList(Landroid/app/FragmentTransaction;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRegistedIService:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromFindo()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 148
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPlayIdFromFindo:J

    return-wide v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResultRecord:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResultRecord:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromRecordedNumber()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Intent;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->hasValidNFCfile(Landroid/content/Intent;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->checkNFCRead(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 148
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/CharSequence;

    .prologue
    .line 148
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getLimitedRecordingTime()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I

    return v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/voicenote/main/VNMainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/SearchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUVThreshold:I

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/voicenote/main/VNMainActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUVThreshold:I

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateEQ(Z)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAdvancedAndHeadsetMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/voicenote/main/VNMainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startSearchTask(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    return v0
.end method

.method static synthetic access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$4302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 148
    sput-boolean p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isbtnAnim:Z

    return p0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEasymode:Z

    return v0
.end method

.method static synthetic access$4502(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEasymode:Z

    return p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;)Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    return-object p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    return v0
.end method

.method private addBookmark()V
    .locals 4

    .prologue
    .line 3247
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v1, :cond_1

    .line 3267
    :cond_0
    :goto_0
    return-void

    .line 3250
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 3251
    .local v0, "tempfragment":Landroid/app/Fragment;
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->addBookmark()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3253
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b001e

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 3258
    :pswitch_1
    if-nez v0, :cond_0

    .line 3259
    const v1, 0x7f0b001d

    const v2, 0x7f0b001c

    const/16 v3, 0x32

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(III)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 3251
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkNFCRead(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2191
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "checkNFCRead"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2192
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    .line 2193
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2194
    .local v0, "action":Ljava/lang/String;
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "android.nfc.action.TECH_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2197
    :cond_0
    const-string v7, "VoiceNoteMainActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "action : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2199
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->hasValidNFCfile(Landroid/content/Intent;)J

    move-result-wide v4

    .line 2200
    .local v4, "nfcId":J
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_d

    .line 2201
    const-string v7, "audio"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 2202
    .local v1, "audiomanager":Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2203
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "isRecordActive"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->overridePendingTransition(II)V

    .line 2205
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b015a

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 2207
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2208
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "NFC has to finish isRecordActive"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    .line 2295
    .end local v1    # "audiomanager":Landroid/media/AudioManager;
    .end local v4    # "nfcId":J
    :cond_1
    :goto_0
    return-void

    .line 2211
    .restart local v1    # "audiomanager":Landroid/media/AudioManager;
    .restart local v4    # "nfcId":J
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2212
    :cond_3
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->overridePendingTransition(II)V

    .line 2213
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b00d6

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2215
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2216
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "NFC has to finish isCall"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    goto :goto_0

    .line 2219
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v7

    const/16 v8, 0x3eb

    if-eq v7, v8, :cond_5

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v7

    const/16 v8, 0x3ec

    if-ne v7, v8, :cond_6

    .line 2221
    :cond_5
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->overridePendingTransition(II)V

    .line 2222
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2223
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "NFC has to finish isRecording"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2224
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    goto :goto_0

    .line 2227
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2228
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    .line 2231
    :cond_7
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2233
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 2234
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_8

    .line 2235
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 2238
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V

    .line 2243
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7, v4, v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onPressListView(J)Z

    move-result v6

    .line 2244
    .local v6, "success":Z
    if-eqz v6, :cond_9

    .line 2245
    const-string v7, "android.nfc.extra.NDEF_MESSAGES"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2248
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 2249
    .local v2, "ft":Landroid/app/FragmentTransaction;
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {p0, v2, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 2250
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_0

    .line 2252
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    .end local v6    # "success":Z
    :cond_a
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 2253
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_b

    .line 2254
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 2257
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V

    .line 2262
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7, v4, v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onPressListView(J)Z

    move-result v6

    .line 2263
    .restart local v6    # "success":Z
    if-eqz v6, :cond_c

    .line 2264
    const-string v7, "android.nfc.extra.NDEF_MESSAGES"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2267
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 2268
    .restart local v2    # "ft":Landroid/app/FragmentTransaction;
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {p0, v2, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 2269
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_0

    .line 2273
    .end local v1    # "audiomanager":Landroid/media/AudioManager;
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    .end local v6    # "success":Z
    :cond_d
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_e

    const-string v7, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2274
    :cond_e
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "NFC has to finish no playing file"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2275
    const-wide/16 v8, -0x2

    cmp-long v7, v4, v8

    if-nez v7, :cond_f

    .line 2276
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b0163

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 2280
    :goto_1
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2281
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    goto/16 :goto_0

    .line 2278
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b0084

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_1

    .line 2284
    :cond_10
    const-wide/16 v8, -0x2

    cmp-long v7, v4, v8

    if-nez v7, :cond_11

    .line 2285
    const v7, 0x7f0b0163

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2286
    .local v3, "msg":Ljava/lang/String;
    const v7, 0x7f0b015b

    invoke-static {v7, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(ILjava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2289
    .end local v3    # "msg":Ljava/lang/String;
    :cond_11
    const v7, 0x7f0b015b

    const v8, 0x7f0b0084

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private clickHomeAtActionBar()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4949
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    if-nez v0, :cond_0

    .line 4975
    :goto_0
    return-void

    .line 4952
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    const-string v1, ""

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 4953
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 4954
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 4955
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setFocusable(Z)V

    .line 4956
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 4957
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 4959
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 4961
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    .line 4962
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4964
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    .line 4965
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    if-eqz v0, :cond_3

    .line 4966
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->cancel(Z)Z

    .line 4968
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    const/16 v1, 0x848

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4969
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doSearch(Ljava/lang/String;)V

    .line 4970
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_4

    .line 4971
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setSearchMode(ZZ)V

    .line 4974
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method private displayToolbarHelp()V
    .locals 18

    .prologue
    .line 5038
    const-string v15, "initial_player_access"

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v5

    .line 5039
    .local v5, "isInitialPlayerAccess":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 5040
    const-string v15, "VoiceNoteMainActivity"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "isExploreByTouchEnabled : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 5209
    :cond_0
    :goto_0
    return-void

    .line 5044
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    invoke-static {v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEnablePenHoverInforPreview(Landroid/content/ContentResolver;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 5045
    const-string v15, "VoiceNoteMainActivity"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "isEnablePenHoverInforPreview : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEnablePenHoverInforPreview(Landroid/content/ContentResolver;)Z

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5049
    :cond_2
    sget-object v15, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v15, :cond_0

    .line 5050
    if-eqz v5, :cond_0

    sget-object v15, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v15}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v15

    const/16 v16, 0x15

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 5051
    sget-object v15, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v15}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unRegisterMediaButtonProcessListener()V

    .line 5052
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->windowManager:Landroid/view/WindowManager;

    .line 5053
    new-instance v6, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v6}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 5054
    .local v6, "layoutParamsWindow":Landroid/view/WindowManager$LayoutParams;
    const/4 v15, 0x0

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 5055
    const/4 v15, 0x0

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 5056
    const/16 v15, 0x33

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 5057
    const/4 v15, -0x1

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 5058
    const/4 v15, -0x1

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 5059
    const/16 v15, 0x480

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 5060
    const/4 v15, -0x3

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 5061
    const/4 v15, 0x0

    iput v15, v6, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 5063
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 5064
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v15, 0x7f030021

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    .line 5065
    new-instance v7, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 5090
    .local v7, "mClickListener":Landroid/view/View$OnClickListener;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    const v16, 0x7f0e0050

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 5091
    .local v11, "toolbarHelpCancelBtn":Landroid/widget/ImageButton;
    invoke-virtual {v11, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5092
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    const v16, 0x7f0e0055

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 5093
    .local v12, "tooltipFfBtn":Landroid/widget/ImageButton;
    invoke-virtual {v12, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5094
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    const v16, 0x7f0e0054

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    .line 5095
    .local v13, "tooltipPlayBtn":Landroid/widget/ImageButton;
    invoke-virtual {v13, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5096
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    const v16, 0x7f0e0053

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageButton;

    .line 5097
    .local v14, "tooltipRewBtn":Landroid/widget/ImageButton;
    invoke-virtual {v14, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5099
    new-instance v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/widget/ImageButton;)V

    .line 5121
    .local v1, "OnKeyListener":Landroid/view/View$OnKeyListener;
    const/4 v15, 0x1

    invoke-virtual {v11, v15}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 5122
    invoke-virtual {v11, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 5123
    invoke-virtual {v12, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 5124
    invoke-virtual {v13, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 5125
    invoke-virtual {v14, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 5127
    new-instance v9, Lcom/sec/android/app/voicenote/main/VNMainActivity$22;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$22;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 5139
    .local v9, "onHoverListener":Landroid/view/View$OnHoverListener;
    new-instance v2, Lcom/sec/android/app/voicenote/main/VNMainActivity$23;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$23;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 5172
    .local v2, "hoverPopupListener":Landroid/widget/HoverPopupWindow$HoverPopupListener;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 5173
    if-eqz v12, :cond_3

    .line 5174
    invoke-virtual {v12}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    .line 5175
    .local v3, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v3, :cond_3

    .line 5176
    const/16 v15, 0x3031

    invoke-virtual {v3, v15}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 5180
    .end local v3    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_3
    if-eqz v14, :cond_4

    .line 5181
    invoke-virtual {v14}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    .line 5182
    .restart local v3    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v3, :cond_4

    .line 5183
    const/16 v15, 0x3031

    invoke-virtual {v3, v15}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 5188
    .end local v3    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_4
    invoke-virtual {v12}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v8

    .line 5189
    .local v8, "nextHpw":Landroid/widget/HoverPopupWindow;
    if-eqz v8, :cond_5

    .line 5190
    invoke-virtual {v12, v9}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 5191
    const/4 v15, 0x1

    invoke-virtual {v8, v15}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 5192
    const/4 v15, 0x1

    invoke-virtual {v8, v15}, Landroid/widget/HoverPopupWindow;->setInfoPickerMoveEabled(Z)V

    .line 5193
    invoke-virtual {v8, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 5196
    :cond_5
    invoke-virtual {v14}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v10

    .line 5197
    .local v10, "preHpw":Landroid/widget/HoverPopupWindow;
    if-eqz v10, :cond_6

    .line 5198
    invoke-virtual {v14, v9}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 5199
    const/4 v15, 0x1

    invoke-virtual {v10, v15}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 5200
    const/4 v15, 0x1

    invoke-virtual {v10, v15}, Landroid/widget/HoverPopupWindow;->setInfoPickerMoveEabled(Z)V

    .line 5201
    invoke-virtual {v10, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 5205
    .end local v8    # "nextHpw":Landroid/widget/HoverPopupWindow;
    .end local v10    # "preHpw":Landroid/widget/HoverPopupWindow;
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->windowManager:Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v15, v0, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5206
    const-string v15, "VoiceNoteMainActivity"

    const-string v16, "Display Toolbar Tooltip"

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private drawControlButton(Landroid/app/FragmentTransaction;)V
    .locals 1
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 3227
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;Z)V

    .line 3228
    return-void
.end method

.method private drawControlButton(Landroid/app/FragmentTransaction;Z)V
    .locals 9
    .param p1, "ft"    # Landroid/app/FragmentTransaction;
    .param p2, "enable"    # Z

    .prologue
    const/4 v4, 0x1

    const v8, 0x7f0e0017

    .line 3120
    sget-object v5, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v5

    .line 3121
    :try_start_0
    iget-boolean v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v6, :cond_1

    .line 3122
    monitor-exit v5

    .line 3224
    :cond_0
    :goto_0
    return-void

    .line 3124
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3125
    const-string v5, "VoiceNoteMainActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "drawControlButton : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3127
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3128
    const-string v4, "VoiceNoteMainActivity"

    const-string v5, "skip drawControlButton after the Activity is destroyed"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3124
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 3132
    :cond_2
    const/4 v1, 0x0

    .line 3133
    .local v1, "playerControlFrag":Landroid/app/Fragment;
    const/4 v2, 0x0

    .line 3135
    .local v2, "playerIdleFrag":Landroid/app/Fragment;
    iget v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v5, :cond_d

    .line 3137
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v5

    if-nez v5, :cond_3

    .line 3138
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    .line 3141
    :cond_3
    const/4 v0, 0x0

    .line 3143
    .local v0, "currentResource":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 3144
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 3154
    const v0, 0x7f030045

    .line 3173
    :goto_1
    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    iget-boolean v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-nez v6, :cond_c

    if-eqz p2, :cond_c

    :goto_2
    invoke-static {v0, v5, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->newInstance(IZZ)Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    .line 3175
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    const-string v5, "VNMainActivityControlButtonFragment"

    invoke-virtual {p1, v8, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 3176
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    if-eqz v4, :cond_4

    .line 3177
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    invoke-virtual {p1, v4}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3179
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateMicState()V

    .line 3207
    .end local v0    # "currentResource":I
    :cond_5
    :goto_3
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3208
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    if-eqz v4, :cond_7

    .line 3209
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    invoke-virtual {p1, v4}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3211
    :cond_7
    sget-object v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_a

    .line 3212
    sget-object v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v3

    .line 3213
    .local v3, "state":I
    const/16 v4, 0x17

    if-eq v4, v3, :cond_8

    const/16 v4, 0x18

    if-ne v4, v3, :cond_9

    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkListSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 3215
    :cond_9
    if-eqz v1, :cond_a

    .line 3216
    invoke-virtual {p1, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3220
    .end local v3    # "state":I
    :cond_a
    if-eqz v2, :cond_0

    .line 3221
    invoke-virtual {p1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_0

    .line 3146
    .restart local v0    # "currentResource":I
    :pswitch_0
    const v0, 0x7f030049

    .line 3147
    goto :goto_1

    .line 3150
    :pswitch_1
    const v0, 0x7f030047

    .line 3151
    goto :goto_1

    .line 3158
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 3168
    const v0, 0x7f030044

    goto :goto_1

    .line 3160
    :pswitch_2
    const v0, 0x7f030048

    .line 3161
    goto :goto_1

    .line 3164
    :pswitch_3
    const v0, 0x7f030046

    .line 3165
    goto :goto_1

    .line 3173
    :cond_c
    const/4 v4, 0x0

    goto :goto_2

    .line 3180
    .end local v0    # "currentResource":I
    :cond_d
    iget v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v5, v4, :cond_f

    sget-object v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_f

    sget-object v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v4

    const/16 v5, 0x15

    if-eq v4, v5, :cond_f

    .line 3182
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v4

    if-nez v4, :cond_5

    .line 3183
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 3187
    :goto_4
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    .end local v1    # "playerControlFrag":Landroid/app/Fragment;
    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;-><init>()V

    .line 3188
    .restart local v1    # "playerControlFrag":Landroid/app/Fragment;
    const-string v4, "VNPlayerControlButtonFragment"

    invoke-virtual {p1, v8, v1, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto/16 :goto_3

    .line 3185
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    goto :goto_4

    .line 3195
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 3199
    :goto_5
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;

    .end local v2    # "playerIdleFrag":Landroid/app/Fragment;
    invoke-direct {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;-><init>()V

    .line 3200
    .restart local v2    # "playerIdleFrag":Landroid/app/Fragment;
    const-string v4, "VNPlayerControlIdleFragment"

    invoke-virtual {p1, v8, v2, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 3202
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v2, :cond_5

    .line 3203
    invoke-virtual {p1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_3

    .line 3197
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    goto :goto_5

    .line 3144
    nop

    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 3158
    :pswitch_data_1
    .packed-switch 0x3eb
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawLibraryList(Landroid/app/FragmentTransaction;)V
    .locals 6
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    const v5, 0x7f0e0013

    const/4 v4, 0x1

    .line 2924
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "drawLibraryList"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2926
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 2927
    .local v0, "libraryFt":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 2929
    .local v1, "orgFrag":Landroid/app/Fragment;
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v2, :cond_2

    .line 2930
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    if-nez v2, :cond_1

    .line 2931
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->newInstance(Z)Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .line 2932
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v0, v5, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2937
    .end local v1    # "orgFrag":Landroid/app/Fragment;
    :goto_0
    iget v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v2, :cond_0

    .line 2938
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2952
    :cond_0
    :goto_1
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2953
    return-void

    .line 2935
    .restart local v1    # "orgFrag":Landroid/app/Fragment;
    :cond_1
    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .end local v1    # "orgFrag":Landroid/app/Fragment;
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    goto :goto_0

    .line 2940
    .restart local v1    # "orgFrag":Landroid/app/Fragment;
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v2, :cond_0

    .line 2941
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-nez v2, :cond_3

    .line 2942
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 2943
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->newInstance(Z)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 2944
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v0, v5, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2948
    .end local v1    # "orgFrag":Landroid/app/Fragment;
    :goto_2
    iget v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v2, :cond_0

    .line 2949
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    .line 2946
    .restart local v1    # "orgFrag":Landroid/app/Fragment;
    :cond_3
    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .end local v1    # "orgFrag":Landroid/app/Fragment;
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    goto :goto_2
.end method

.method private drawLightMicView(Landroid/app/FragmentTransaction;)V
    .locals 5
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    const v4, 0x7f03004e

    const v3, 0x7f0e000e

    .line 3025
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "drawLightMicView"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3027
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v0

    .line 3029
    .local v0, "recordState":I
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3030
    :cond_0
    invoke-static {v4, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    .line 3032
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    invoke-virtual {p1, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3056
    :goto_0
    return-void

    .line 3034
    :cond_1
    const-string v1, "record_mode"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3036
    :pswitch_0
    invoke-static {v4, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    .line 3038
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    invoke-virtual {p1, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3041
    :pswitch_1
    const v1, 0x7f03004d

    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    .line 3043
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    invoke-virtual {p1, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3046
    :pswitch_2
    const v1, 0x7f03004c

    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    .line 3048
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    invoke-virtual {p1, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3051
    :pswitch_3
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->newInstance(ILjava/lang/CharSequence;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 3052
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {p1, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3034
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawLogoView(Landroid/app/FragmentTransaction;)V
    .locals 8
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    const v7, 0x7f0e0012

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 3059
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "drawLogoView"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3060
    const-string v3, "logo_type"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 3061
    const-string v3, "logo"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 3064
    :cond_0
    const-string v3, "logo"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_1

    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_6

    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3068
    :cond_1
    const-string v3, "logo_type_temp"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 3069
    invoke-static {}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempFile()Ljava/io/File;

    move-result-object v0

    .line 3070
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3071
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 3073
    :cond_2
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "logo_image.jpg"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3074
    .local v2, "prevLogoFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4

    .line 3075
    const-string v3, "logo"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 3076
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    if-eqz v3, :cond_3

    .line 3077
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    invoke-virtual {p1, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3097
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "prevLogoFile":Ljava/io/File;
    :cond_3
    :goto_0
    return-void

    .line 3080
    .restart local v0    # "file":Ljava/io/File;
    .restart local v2    # "prevLogoFile":Ljava/io/File;
    :cond_4
    new-instance v3, Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    .line 3081
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    const-string v4, "FRAGMENT_LOGO"

    invoke-virtual {p1, v7, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3084
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "prevLogoFile":Ljava/io/File;
    :cond_5
    new-instance v3, Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    .line 3085
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    const-string v4, "FRAGMENT_LOGO"

    invoke-virtual {p1, v7, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3088
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    if-eqz v3, :cond_7

    .line 3089
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    invoke-virtual {p1, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 3091
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_LOGO"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 3092
    .local v1, "freg":Landroid/app/Fragment;
    if-eqz v1, :cond_3

    .line 3093
    invoke-virtual {p1, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method private drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V
    .locals 2
    .param p1, "ft"    # Landroid/app/FragmentTransaction;
    .param p2, "lightmode"    # Z
    .param p3, "convertedText"    # Ljava/lang/CharSequence;

    .prologue
    .line 3231
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "drawMainview"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3233
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3234
    :cond_0
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "drawMainview - it is destroyed"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3244
    :goto_0
    return-void

    .line 3238
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawModeView(Landroid/app/FragmentTransaction;ZZLjava/lang/CharSequence;)V

    .line 3240
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 3243
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V

    goto :goto_0
.end method

.method private drawMicView(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V
    .locals 8
    .param p1, "ft"    # Landroid/app/FragmentTransaction;
    .param p2, "bRecordStart"    # Z
    .param p3, "convertedText"    # Ljava/lang/CharSequence;

    .prologue
    const v7, 0x7f030050

    const v6, 0x7f0e000e

    .line 2966
    const-string v4, "VoiceNoteMainActivity"

    const-string v5, "drawMicView"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2968
    if-eqz p2, :cond_2

    const/16 v3, 0x3e8

    .line 2969
    .local v3, "recordState":I
    :goto_0
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2970
    :cond_0
    invoke-static {v7, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 2972
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {p1, v6, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3021
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateMicState()V

    .line 3022
    return-void

    .line 2968
    .end local v3    # "recordState":I
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v3

    goto :goto_0

    .line 2974
    .restart local v3    # "recordState":I
    :cond_3
    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 3008
    :cond_4
    :goto_2
    const-string v4, "logo"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_1

    .line 3010
    const-string v4, "logo_type_temp"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 3011
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "logo_image.jpg"

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3012
    .local v2, "prevLogoFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3013
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    if-eqz v4, :cond_1

    .line 3014
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {p1, v6, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    .line 2976
    .end local v2    # "prevLogoFile":Ljava/io/File;
    :pswitch_0
    invoke-static {v7, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 2978
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {p1, v6, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2

    .line 2982
    :pswitch_1
    const v4, 0x7f03004b

    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 2984
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {p1, v6, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2

    .line 2988
    :pswitch_2
    const v4, 0x7f03004a

    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 2990
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {p1, v6, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2

    .line 2994
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 2995
    .local v0, "f":Landroid/app/Fragment;
    instance-of v4, v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    if-eqz v4, :cond_5

    .line 2996
    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .end local v0    # "f":Landroid/app/Fragment;
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 2998
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v1

    .line 2999
    .local v1, "mMediaRecorderState":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    if-eqz v4, :cond_6

    const/16 v4, 0x3eb

    if-eq v1, v4, :cond_7

    const/16 v4, 0x3ea

    if-eq v1, v4, :cond_7

    const/16 v4, 0x3ec

    if-eq v1, v4, :cond_7

    .line 3000
    :cond_6
    invoke-static {v3, p3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->newInstance(ILjava/lang/CharSequence;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 3001
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {p1, v6, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_2

    .line 3002
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    if-eqz v4, :cond_4

    .line 3003
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v4, v3, p3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setRecordStateNText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2974
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawModeView(Landroid/app/FragmentTransaction;ZZLjava/lang/CharSequence;)V
    .locals 0
    .param p1, "ft"    # Landroid/app/FragmentTransaction;
    .param p2, "lightmode"    # Z
    .param p3, "bRecordStart"    # Z
    .param p4, "convertedText"    # Ljava/lang/CharSequence;

    .prologue
    .line 2957
    if-eqz p2, :cond_0

    .line 2958
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawLightMicView(Landroid/app/FragmentTransaction;)V

    .line 2962
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawLogoView(Landroid/app/FragmentTransaction;)V

    .line 2963
    return-void

    .line 2960
    :cond_0
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMicView(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private drawPanelText(Landroid/app/FragmentTransaction;)V
    .locals 8
    .param p1, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 2889
    const-string v6, "VoiceNoteMainActivity"

    const-string v7, "drawPanelText"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v2

    .line 2894
    .local v2, "mainPanelTextId":I
    const/4 v4, 0x0

    .line 2895
    .local v4, "removeResId1":I
    const/4 v5, 0x0

    .line 2897
    .local v5, "removeResId2":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2898
    const v4, 0x7f0e0010

    .line 2899
    const v5, 0x7f0e000f

    .line 2908
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 2909
    .local v0, "fragment1":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 2910
    .local v1, "fragment2":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    .line 2911
    .local v3, "newFragment":Landroid/app/Fragment;
    if-nez v3, :cond_2

    .line 2912
    if-eqz v0, :cond_0

    .line 2913
    invoke-virtual {p1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2915
    :cond_0
    if-eqz v1, :cond_1

    .line 2916
    invoke-virtual {p1, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2918
    :cond_1
    sget v6, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    invoke-static {v6}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->newInstance(I)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .line 2919
    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {p1, v2, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2921
    :cond_2
    return-void

    .line 2900
    .end local v0    # "fragment1":Landroid/app/Fragment;
    .end local v1    # "fragment2":Landroid/app/Fragment;
    .end local v3    # "newFragment":Landroid/app/Fragment;
    :cond_3
    const/4 v6, 0x3

    const-string v7, "record_mode"

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v7

    if-ne v6, v7, :cond_4

    .line 2901
    const v4, 0x7f0e0010

    .line 2902
    const v5, 0x7f0e0011

    goto :goto_0

    .line 2904
    :cond_4
    const v4, 0x7f0e000f

    .line 2905
    const v5, 0x7f0e0011

    goto :goto_0
.end method

.method private fromFindo()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 3305
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 3306
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 3319
    :cond_0
    :goto_0
    return v3

    .line 3309
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 3310
    .local v1, "intentAction":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 3311
    const-string v4, "voicenote.intent.action.suggest"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3312
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "intent_extra_data_key"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3313
    .local v2, "tempstr":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 3314
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPlayIdFromFindo:J

    .line 3315
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private fromQuickPanel()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3323
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 3325
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 3326
    const-string v2, "isFromQuick"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 3330
    :cond_0
    return v0
.end method

.method private fromRecordedNumber()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 3334
    const-string v4, "VoiceNoteMainActivity"

    const-string v5, "fromRecordedNumber"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3335
    sget-boolean v4, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_RECORDED_CALLS:Z

    if-nez v4, :cond_0

    .line 3354
    :goto_0
    return v3

    .line 3339
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 3340
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 3341
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setRecordedNumber(Ljava/lang/String;)V

    goto :goto_0

    .line 3345
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 3346
    .local v1, "intentAction":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 3347
    const-string v4, "com.sec.android.app.voicenote.recorded_calls"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3348
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "recorded_number"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3349
    .local v2, "number":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setRecordedNumber(Ljava/lang/String;)V

    .line 3350
    const/4 v3, 0x1

    goto :goto_0

    .line 3353
    .end local v2    # "number":Ljava/lang/String;
    :cond_2
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setRecordedNumber(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getLimitedRecordingTime()J
    .locals 14

    .prologue
    .line 2344
    const-wide/16 v6, 0x0

    .line 2346
    .local v6, "time":J
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v1, :cond_5

    .line 2347
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v1, :cond_3

    .line 2348
    const-string v1, "audio/amr"

    iget-object v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2349
    iget-wide v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    const/4 v1, 0x1

    invoke-static {v8, v9, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v6

    .line 2378
    :cond_1
    :goto_0
    return-wide v6

    .line 2351
    :cond_2
    iget-wide v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    const/4 v1, 0x0

    invoke-static {v8, v9, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v6

    goto :goto_0

    .line 2353
    :cond_3
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2354
    const-wide/32 v4, 0x3200000

    .line 2355
    .local v4, "realFreeSize":J
    const/16 v1, 0x3e80

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getRemainingMmsSize(JI)J

    move-result-wide v2

    .line 2357
    .local v2, "limitedFileSize":J
    const/4 v1, 0x1

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v6

    .line 2358
    goto :goto_0

    .end local v2    # "limitedFileSize":J
    .end local v4    # "realFreeSize":J
    :cond_4
    const-string v1, "record_mode"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    const/4 v8, 0x3

    if-ne v1, v8, :cond_1

    .line 2359
    const-wide/32 v6, 0x494a8

    goto :goto_0

    .line 2362
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v1, :cond_7

    .line 2363
    const-string v1, "audio/amr"

    iget-object v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2364
    iget-wide v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    const/4 v1, 0x1

    invoke-static {v8, v9, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v8

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getRecDuration()J

    move-result-wide v10

    sub-long v6, v8, v10

    goto :goto_0

    .line 2368
    :cond_6
    iget-wide v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    const/4 v1, 0x0

    invoke-static {v8, v9, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v8

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getRecDuration()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    sub-long v6, v8, v10

    goto :goto_0

    .line 2372
    :cond_7
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecordingTime()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    goto :goto_0

    .line 2375
    :catch_0
    move-exception v0

    .line 2376
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getMediaRecorderState()I
    .locals 4

    .prologue
    .line 3270
    const/16 v1, 0x3e8

    .line 3272
    .local v1, "state":I
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v2, :cond_0

    .line 3274
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getMediaRecorderState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3285
    :goto_0
    return v1

    .line 3275
    :catch_0
    move-exception v0

    .line 3276
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "getMediaRecorderState error"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3277
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 3279
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_1

    .line 3280
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    goto :goto_0

    .line 3282
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getRecorderState()I

    move-result v1

    goto :goto_0
.end method

.method private gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    .locals 18
    .param p1, "ft"    # Landroid/app/FragmentTransaction;
    .param p2, "isMMSList"    # Z
    .param p3, "needAnim"    # Z

    .prologue
    .line 3385
    const-string v14, "VoiceNoteMainActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "gotoListwithVIAnimation() : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3386
    sget-object v15, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v15

    .line 3387
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v14, :cond_0

    .line 3388
    monitor-exit v15

    .line 3603
    :goto_0
    return-void

    .line 3390
    :cond_0
    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3391
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v14

    if-nez v14, :cond_1

    .line 3392
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v14

    const-string v15, "FRAGMENT_DIALOG"

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 3393
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v14

    const-string v15, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 3394
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v14

    const-string v15, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 3397
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_2

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v14

    if-nez v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v14}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v14

    if-nez v14, :cond_2

    .line 3399
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 3400
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    .line 3403
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v14}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v14

    if-nez v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getSearchText()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 3404
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->clickHomeAtActionBar()V

    .line 3407
    :cond_3
    const/4 v7, 0x0

    .line 3408
    .local v7, "list":Landroid/app/Fragment;
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawLibraryList(Landroid/app/FragmentTransaction;)V

    .line 3409
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 3410
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .line 3414
    :goto_1
    if-eqz v7, :cond_4

    .line 3415
    const v14, 0x7f0e0013

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 3416
    .local v8, "listview":Landroid/view/View;
    const/4 v14, 0x0

    invoke-virtual {v8, v14}, Landroid/view/View;->setVisibility(I)V

    .line 3417
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v6

    .line 3418
    .local v6, "libraryFt":Landroid/app/FragmentTransaction;
    if-eqz p3, :cond_8

    .line 3419
    const v14, 0x7f050005

    const/4 v15, 0x0

    invoke-virtual {v6, v14, v15}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 3425
    :goto_2
    invoke-virtual {v6, v7}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3426
    invoke-virtual {v6}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3461
    .end local v6    # "libraryFt":Landroid/app/FragmentTransaction;
    .end local v8    # "listview":Landroid/view/View;
    :cond_4
    if-nez p3, :cond_5

    .line 3462
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v14, :cond_9

    .line 3463
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v14, :cond_5

    .line 3464
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 3465
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const/high16 v14, 0x7f030000

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 3466
    .local v2, "actionbar_v":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v14, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_selectMode(Landroid/view/View;)V

    .line 3468
    const v14, 0x7f0e0001

    invoke-virtual {v2, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    .line 3469
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setFocusable(Z)V

    .line 3470
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 3471
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v15, Lcom/sec/android/app/voicenote/main/VNMainActivity$9;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$9;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3477
    const v14, 0x7f0e0002

    invoke-virtual {v2, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    .line 3478
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setFocusable(Z)V

    .line 3479
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 3480
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    new-instance v15, Lcom/sec/android/app/voicenote/main/VNMainActivity$10;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$10;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v14, v15}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3489
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setEnableButton(Landroid/widget/Button;Z)V

    .line 3502
    .end local v2    # "actionbar_v":Landroid/view/View;
    .end local v5    # "inflater":Landroid/view/LayoutInflater;
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    if-eqz v14, :cond_6

    .line 3503
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 3507
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 3508
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_b

    .line 3509
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3514
    :goto_4
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 3515
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->closeOptionsMenu()V

    goto/16 :goto_0

    .line 3390
    .end local v7    # "list":Landroid/app/Fragment;
    :catchall_0
    move-exception v14

    :try_start_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v14

    .line 3412
    .restart local v7    # "list":Landroid/app/Fragment;
    :cond_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    goto/16 :goto_1

    .line 3422
    .restart local v6    # "libraryFt":Landroid/app/FragmentTransaction;
    .restart local v8    # "listview":Landroid/view/View;
    :cond_8
    const v14, 0x7f050006

    const/4 v15, 0x0

    invoke-virtual {v6, v14, v15}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto/16 :goto_2

    .line 3492
    .end local v6    # "libraryFt":Landroid/app/FragmentTransaction;
    .end local v8    # "listview":Landroid/view/View;
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v14, :cond_5

    .line 3493
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v14}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 3494
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v14}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_bookmarks()V

    goto :goto_3

    .line 3495
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v14}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v14

    if-nez v14, :cond_5

    .line 3496
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto :goto_3

    .line 3511
    :cond_b
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto :goto_4

    .line 3519
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v14

    const-string v15, "VNMainActivityControlButtonFragment"

    invoke-virtual {v14, v15}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v12

    .line 3520
    .local v12, "tempmainbtnFragment":Landroid/app/Fragment;
    if-nez v12, :cond_e

    .line 3521
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_d

    .line 3522
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3527
    :goto_5
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    goto/16 :goto_0

    .line 3524
    :cond_d
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto :goto_5

    .line 3530
    :cond_e
    invoke-virtual {v12}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v10

    .line 3531
    .local v10, "mainbtnFragment":Landroid/view/View;
    if-nez v10, :cond_10

    .line 3532
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_f

    .line 3533
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3538
    :goto_6
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    goto/16 :goto_0

    .line 3535
    :cond_f
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto :goto_6

    .line 3541
    :cond_10
    const v14, 0x7f0e00c5

    invoke-virtual {v10, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 3542
    .local v9, "mListControlBtn":Landroid/view/View;
    if-nez v9, :cond_12

    .line 3543
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_11

    .line 3544
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3549
    :goto_7
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    goto/16 :goto_0

    .line 3546
    :cond_11
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto :goto_7

    .line 3552
    :cond_12
    const v14, 0x7f0e00c6

    invoke-virtual {v10, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 3553
    .local v13, "tmpView":Landroid/view/View;
    instance-of v14, v13, Landroid/widget/ImageButton;

    if-nez v14, :cond_14

    .line 3554
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_13

    .line 3555
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3560
    :goto_8
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    goto/16 :goto_0

    .line 3557
    :cond_13
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto :goto_8

    :cond_14
    move-object v11, v13

    .line 3564
    check-cast v11, Landroid/widget/ImageButton;

    .line 3565
    .local v11, "mtempListBtn":Landroid/widget/ImageButton;
    const v14, 0x7f050002

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v3

    .line 3566
    .local v3, "animgolistBtn":Landroid/animation/Animator;
    invoke-virtual {v3, v9}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 3568
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_15

    .line 3569
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3571
    :cond_15
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 3572
    const v14, 0x7f050003

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v4

    .line 3573
    .local v4, "animtempListControlBtn":Landroid/animation/Animator;
    invoke-virtual {v4, v11}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 3575
    if-nez p3, :cond_16

    .line 3576
    const-wide/16 v14, 0x0

    invoke-virtual {v3, v14, v15}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 3577
    const-wide/16 v14, 0x0

    invoke-virtual {v4, v14, v15}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 3578
    const/4 v14, 0x0

    sput-boolean v14, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isbtnAnim:Z

    .line 3582
    :goto_9
    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    .line 3583
    invoke-virtual {v4}, Landroid/animation/Animator;->start()V

    .line 3584
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v14, :cond_17

    .line 3585
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3590
    :goto_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    new-instance v15, Lcom/sec/android/app/voicenote/main/VNMainActivity$11;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$11;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    const-wide/16 v16, 0x3e8

    invoke-virtual/range {v14 .. v17}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3602
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->closeOptionsMenu()V

    goto/16 :goto_0

    .line 3580
    :cond_16
    const/4 v14, 0x1

    sput-boolean v14, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isbtnAnim:Z

    goto :goto_9

    .line 3587
    :cond_17
    const/4 v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto :goto_a
.end method

.method private gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    .locals 22
    .param p1, "ft"    # Landroid/app/FragmentTransaction;
    .param p2, "isMMSList"    # Z
    .param p3, "needAnim"    # Z

    .prologue
    .line 3606
    const-string v17, "VoiceNoteMainActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "gotoMainwithVIAnimation() : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3607
    sget-object v18, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v18

    .line 3608
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 3609
    monitor-exit v18

    .line 3769
    :cond_0
    :goto_0
    return-void

    .line 3611
    :cond_1
    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3613
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    move/from16 v17, v0

    if-nez v17, :cond_b

    .line 3614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 3615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->onReset()V

    .line 3621
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    const-string v18, "FRAGMENT_DIALOG"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 3622
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    const-string v18, "FRAGMENT_MULTI_CATEGORY"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 3623
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    const-string v18, "FRAGMENT_FILE"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 3625
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_3

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v17

    if-nez v17, :cond_3

    const-string v17, "record_mode"

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v17

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 3628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 3631
    :cond_3
    if-nez p3, :cond_4

    .line 3632
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_c

    .line 3633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 3645
    :cond_4
    :goto_1
    const-string v17, "audio"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    .line 3646
    .local v6, "audiomanager":Landroid/media/AudioManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    invoke-virtual {v6}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v17

    if-nez v17, :cond_5

    .line 3647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->resetConvertedText()V

    .line 3649
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v15

    .line 3650
    .local v15, "tmpFragment":Landroid/app/Fragment;
    instance-of v0, v15, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object v11, v15

    .line 3651
    check-cast v11, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .line 3653
    .local v11, "mainPanelText":Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    .line 3654
    .local v13, "newFileName":Ljava/lang/String;
    const-string v17, "category_label_color"

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v8

    .line 3656
    .local v8, "labelColor":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getLimitedRecordingTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v11, v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateTime(J)V

    .line 3657
    invoke-virtual {v11, v13, v8}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateFilename(Ljava/lang/String;I)V

    .line 3660
    .end local v8    # "labelColor":I
    .end local v11    # "mainPanelText":Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    .end local v13    # "newFileName":Ljava/lang/String;
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawLibraryList(Landroid/app/FragmentTransaction;)V

    .line 3661
    const/4 v9, 0x0

    .line 3662
    .local v9, "listFragment":Landroid/app/Fragment;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v17

    if-eqz v17, :cond_e

    .line 3663
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .line 3667
    :goto_2
    if-eqz v9, :cond_8

    .line 3668
    if-eqz p3, :cond_7

    .line 3669
    const/16 v17, 0x0

    const v18, 0x7f050004

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 3671
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3692
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_9

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v17

    if-nez v17, :cond_9

    const-string v17, "record_mode"

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v17

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 3695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 3697
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    move-object/from16 v17, v0

    if-eqz v17, :cond_a

    .line 3698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    .line 3702
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 3703
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3705
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 3706
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->closeOptionsMenu()V

    goto/16 :goto_0

    .line 3611
    .end local v6    # "audiomanager":Landroid/media/AudioManager;
    .end local v9    # "listFragment":Landroid/app/Fragment;
    .end local v15    # "tmpFragment":Landroid/app/Fragment;
    :catchall_0
    move-exception v17

    :try_start_1
    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v17

    .line 3617
    :cond_b
    sget-object v17, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v17, :cond_2

    sget-object v17, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v17

    if-nez v17, :cond_0

    sget-object v17, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v17

    if-eqz v17, :cond_2

    goto/16 :goto_0

    .line 3635
    :cond_c
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v17

    if-nez v17, :cond_d

    const-string v17, "record_mode"

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v17

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_d

    .line 3637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    goto/16 :goto_1

    .line 3639
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 3640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_main()V

    goto/16 :goto_1

    .line 3665
    .restart local v6    # "audiomanager":Landroid/media/AudioManager;
    .restart local v9    # "listFragment":Landroid/app/Fragment;
    .restart local v15    # "tmpFragment":Landroid/app/Fragment;
    :cond_e
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    goto/16 :goto_2

    .line 3710
    :cond_f
    const/4 v10, 0x0

    .line 3711
    .local v10, "mMainControlButton":Landroid/widget/ImageButton;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    if-nez v17, :cond_10

    .line 3712
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto/16 :goto_0

    .line 3716
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    const-string v18, "VNPlayerControlIdleFragment"

    invoke-virtual/range {v17 .. v18}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v14

    .line 3717
    .local v14, "tempbackbtnFragment":Landroid/app/Fragment;
    if-nez v14, :cond_11

    .line 3718
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto/16 :goto_0

    .line 3722
    :cond_11
    invoke-virtual {v14}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v7

    .line 3723
    .local v7, "backbtnFragment":Landroid/view/View;
    if-nez v7, :cond_12

    .line 3724
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto/16 :goto_0

    .line 3728
    :cond_12
    const v17, 0x7f0e0099

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 3729
    .local v16, "tmpView":Landroid/view/View;
    move-object/from16 v0, v16

    instance-of v0, v0, Landroid/widget/ImageButton;

    move/from16 v17, v0

    if-nez v17, :cond_13

    .line 3730
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto/16 :goto_0

    :cond_13
    move-object/from16 v10, v16

    .line 3734
    check-cast v10, Landroid/widget/ImageButton;

    .line 3735
    const v17, 0x7f050002

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v4

    .line 3736
    .local v4, "animbackBtn":Landroid/animation/Animator;
    invoke-virtual {v4, v10}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 3737
    invoke-virtual {v4}, Landroid/animation/Animator;->start()V

    .line 3739
    const v17, 0x7f0e009a

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 3740
    .local v12, "mtempListControlBtn":Landroid/widget/ImageButton;
    if-nez v12, :cond_14

    .line 3741
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    goto/16 :goto_0

    .line 3745
    :cond_14
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3746
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 3747
    const v17, 0x7f050003

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v5

    .line 3749
    .local v5, "animtempMainControlBtn":Landroid/animation/Animator;
    invoke-virtual {v5, v12}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 3750
    if-nez p3, :cond_15

    .line 3751
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 3752
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 3754
    :cond_15
    invoke-virtual {v5}, Landroid/animation/Animator;->start()V

    .line 3755
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 3757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/android/app/voicenote/main/VNMainActivity$12;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$12;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    const-wide/16 v20, 0x3e8

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3768
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->closeOptionsMenu()V

    goto/16 :goto_0
.end method

.method private hasValidNFCfile(Landroid/content/Intent;)J
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2298
    const-string v14, "VoiceNoteMainActivity"

    const-string v15, "hasValidNFCfile"

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2300
    const-wide/16 v10, -0x1

    .line 2301
    .local v10, "nfcVoiceId":J
    const-string v14, "android.nfc.extra.NDEF_MESSAGES"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v13

    .line 2303
    .local v13, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v13, :cond_6

    array-length v14, v13

    if-lez v14, :cond_6

    .line 2304
    array-length v14, v13

    new-array v8, v14, [Landroid/nfc/NdefMessage;

    .line 2305
    .local v8, "msgs":[Landroid/nfc/NdefMessage;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v14, v13

    if-ge v4, v14, :cond_6

    .line 2307
    :try_start_0
    aget-object v14, v13, v4

    check-cast v14, Landroid/nfc/NdefMessage;

    aput-object v14, v8, v4

    .line 2308
    aget-object v14, v8, v4

    invoke-virtual {v14}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    invoke-virtual {v14}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v12

    .line 2310
    .local v12, "payload":[B
    if-eqz v12, :cond_0

    array-length v14, v12

    if-gtz v14, :cond_1

    .line 2305
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2314
    :cond_1
    const/4 v14, 0x0

    aget-byte v14, v12, v14

    and-int/lit16 v14, v14, 0x80

    if-nez v14, :cond_2

    const-string v3, "UTF-8"

    .line 2315
    .local v3, "encoding":Ljava/lang/String;
    :goto_1
    const/4 v14, 0x0

    aget-byte v14, v12, v14

    and-int/lit8 v7, v14, 0x3f

    .line 2316
    .local v7, "langCodeLen":I
    new-instance v6, Ljava/lang/String;

    add-int/lit8 v14, v7, 0x1

    array-length v15, v12

    sub-int/2addr v15, v7

    add-int/lit8 v15, v15, -0x1

    invoke-direct {v6, v12, v14, v15, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 2317
    .local v6, "labelInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isValidTags(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    .line 2319
    .local v5, "isValid":I
    if-nez v5, :cond_4

    .line 2320
    const/16 v14, 0x2f

    invoke-virtual {v6, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v14

    invoke-virtual {v6, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 2321
    .local v9, "path":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentUriFromFilePath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrenId(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v10

    .line 2322
    const-wide/16 v14, -0x1

    cmp-long v14, v10, v14

    if-eqz v14, :cond_3

    move-wide v14, v10

    .line 2341
    .end local v3    # "encoding":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "isValid":I
    .end local v6    # "labelInfo":Ljava/lang/String;
    .end local v7    # "langCodeLen":I
    .end local v8    # "msgs":[Landroid/nfc/NdefMessage;
    .end local v9    # "path":Ljava/lang/String;
    .end local v12    # "payload":[B
    :goto_2
    return-wide v14

    .line 2314
    .restart local v4    # "i":I
    .restart local v8    # "msgs":[Landroid/nfc/NdefMessage;
    .restart local v12    # "payload":[B
    :cond_2
    const-string v3, "UTF-16"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2325
    .restart local v3    # "encoding":Ljava/lang/String;
    .restart local v5    # "isValid":I
    .restart local v6    # "labelInfo":Ljava/lang/String;
    .restart local v7    # "langCodeLen":I
    .restart local v9    # "path":Ljava/lang/String;
    :cond_3
    const-wide/16 v14, -0x1

    goto :goto_2

    .line 2328
    .end local v9    # "path":Ljava/lang/String;
    :cond_4
    const/4 v14, -0x2

    if-ne v5, v14, :cond_5

    .line 2329
    const-wide/16 v14, -0x2

    goto :goto_2

    .line 2331
    :cond_5
    const-wide/16 v14, -0x1

    goto :goto_2

    .line 2334
    .end local v3    # "encoding":Ljava/lang/String;
    .end local v5    # "isValid":I
    .end local v6    # "labelInfo":Ljava/lang/String;
    .end local v7    # "langCodeLen":I
    .end local v12    # "payload":[B
    :catch_0
    move-exception v2

    .line 2335
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 2336
    const-string v14, "VoiceNoteMainActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "hasValidNFCfile : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2337
    const-wide/16 v14, -0x1

    goto :goto_2

    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "i":I
    .end local v8    # "msgs":[Landroid/nfc/NdefMessage;
    :cond_6
    move-wide v14, v10

    .line 2341
    goto :goto_2
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 4493
    sput-object p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 4494
    return-void
.end method

.method private interruptThread(Ljava/lang/Thread;)V
    .locals 1
    .param p1, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 2820
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2821
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 2823
    :cond_0
    return-void
.end method

.method private isAdvancedAndHeadsetMode()Z
    .locals 3

    .prologue
    .line 3358
    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    .line 3359
    .local v1, "record_mode":I
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 3361
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 3363
    const/4 v2, 0x1

    .line 3365
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isAliveThread(Ljava/lang/Thread;)Z
    .locals 1
    .param p1, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 2811
    if-eqz p1, :cond_0

    .line 2812
    invoke-virtual {p1}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2813
    const/4 v0, 0x1

    .line 2816
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAttachOnlyMode()Z
    .locals 1

    .prologue
    .line 4798
    sget-boolean v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    if-eqz v0, :cond_0

    .line 4799
    const/4 v0, 0x1

    .line 4801
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerEasyModeReceiver()V
    .locals 3

    .prologue
    .line 4212
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "registerEasyModeReceiver"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4214
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 4215
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4216
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE_VOICENOTE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4218
    new-instance v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    .line 4233
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 4234
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 4497
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 4498
    return-void
.end method

.method private resetFileNameDialogPositiveButton()V
    .locals 4

    .prologue
    .line 1850
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1851
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    if-eqz v2, :cond_0

    .line 1852
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1853
    .local v1, "origin_text":Ljava/lang/String;
    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->resetOriginalName(Ljava/lang/String;)V

    .line 1855
    .end local v1    # "origin_text":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private resetShareDialogFragment()V
    .locals 4

    .prologue
    .line 1868
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1869
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 1870
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1871
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1872
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1874
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private resetVNAlertDialogFragment()V
    .locals 4

    .prologue
    .line 1858
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1859
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    if-eqz v2, :cond_0

    .line 1860
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3eb

    if-eq v2, v3, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3ec

    if-eq v2, v3, :cond_0

    .line 1861
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1862
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1863
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1866
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private restartEQThread()V
    .locals 2

    .prologue
    .line 2880
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "restartEQThread"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2882
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2883
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    .line 2884
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;->start()V

    .line 2886
    :cond_0
    return-void
.end method

.method private restartRecIconBlinkThread()V
    .locals 2

    .prologue
    .line 2862
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "restartRecIconBlinkThread"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2864
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2865
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    .line 2866
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->start()V

    .line 2868
    :cond_0
    return-void
.end method

.method private restartTimeBlinkThread()V
    .locals 2

    .prologue
    .line 2871
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "restartTimeBlinkThread"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2873
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2874
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    .line 2875
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;->start()V

    .line 2877
    :cond_0
    return-void
.end method

.method private saveMmsRecode()V
    .locals 5

    .prologue
    .line 783
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mAddCategoryChecked:Z

    if-nez v2, :cond_1

    .line 784
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3eb

    if-eq v2, v3, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3ec

    if-ne v2, v3, :cond_1

    .line 786
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doSave()V

    .line 788
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getLastSavedFileUriString()Ljava/lang/String;

    move-result-object v1

    .line 789
    .local v1, "lastSavedFileUriString":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 790
    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setResult(ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 799
    .end local v1    # "lastSavedFileUriString":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finish()V

    .line 802
    :cond_1
    return-void

    .line 793
    .restart local v1    # "lastSavedFileUriString":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setResult(ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 795
    .end local v1    # "lastSavedFileUriString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 796
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private declared-synchronized setDvfsBooster(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    .line 3785
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDvfsHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 3786
    new-instance v0, Landroid/os/DVFSHelper;

    const/4 v1, 0x1

    const-wide/16 v2, 0xa

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;IJ)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 3789
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDvfsHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3790
    monitor-exit p0

    return-void

    .line 3785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setEnableButton(Landroid/widget/Button;Z)V
    .locals 2
    .param p1, "v"    # Landroid/widget/Button;
    .param p2, "enable"    # Z

    .prologue
    .line 4986
    if-eqz p1, :cond_0

    .line 4987
    if-eqz p2, :cond_1

    .line 4988
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 4989
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4995
    :cond_0
    :goto_0
    return-void

    .line 4991
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 4992
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setTitleText()Ljava/lang/String;
    .locals 10

    .prologue
    const v7, 0x7f0b0103

    const-wide/16 v8, -0x1

    .line 4237
    const-string v4, "category_text"

    invoke-static {v4, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v2

    .line 4238
    .local v2, "id":J
    const-string v4, "VoiceNoteMainActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[HIYA]setTitleText() id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4239
    const/4 v1, 0x0

    .line 4241
    .local v1, "setString":Ljava/lang/String;
    cmp-long v4, v2, v8

    if-nez v4, :cond_1

    .line 4242
    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4252
    :goto_0
    if-nez v1, :cond_0

    .line 4253
    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4254
    const-string v4, "category_text"

    invoke-static {v4, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 4255
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onUpdateState(Z)V

    .line 4257
    :cond_0
    return-object v1

    .line 4243
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_2

    .line 4244
    const v4, 0x7f0b0029

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 4246
    :cond_2
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    .line 4247
    .local v0, "helper":Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 4248
    long-to-int v4, v2

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v1

    .line 4249
    goto :goto_0
.end method

.method private showLibraryMenuPopup()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4907
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getActionMode()Landroid/view/ActionMode;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 4937
    :cond_0
    :goto_0
    return v3

    .line 4911
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v5, :cond_2

    .line 4912
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->dismiss()V

    .line 4914
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    .line 4915
    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 4916
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 4919
    new-instance v0, Landroid/view/Gravity;

    invoke-direct {v0}, Landroid/view/Gravity;-><init>()V

    .line 4922
    .local v0, "gravity":Landroid/view/Gravity;
    new-instance v5, Landroid/widget/PopupMenu;

    const v6, 0x800005

    invoke-direct {v5, p0, v2, v6}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 4924
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    const v6, 0x7f0d0009

    invoke-virtual {v5, v6}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 4926
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    .line 4927
    .local v1, "menu":Landroid/view/Menu;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->preparePopupMenu(Landroid/view/Menu;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 4928
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 4929
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->show()V

    .line 4930
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    invoke-virtual {v3, v5}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 4931
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    move v3, v4

    .line 4932
    goto :goto_0

    .line 4934
    :cond_3
    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 4935
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v4, v7}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 4936
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v4, v7}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    goto :goto_0
.end method

.method private showMainMenuPopup()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 4855
    sget-object v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v3

    .line 4856
    :try_start_0
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v4, :cond_1

    .line 4857
    monitor-exit v3

    .line 4891
    :cond_0
    :goto_0
    return v1

    .line 4859
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4860
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v3, :cond_5

    .line 4861
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v3, :cond_2

    .line 4862
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->dismiss()V

    .line 4863
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    .line 4865
    :cond_2
    const/4 v0, 0x0

    .line 4866
    .local v0, "v":Landroid/view/View;
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_4

    .line 4867
    const v3, 0x7f0e00da

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4871
    :goto_1
    if-eqz v0, :cond_0

    .line 4874
    new-instance v1, Landroid/widget/PopupMenu;

    const v3, 0x800005

    invoke-direct {v1, p0, v0, v3}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 4875
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    const v3, 0x7f0d0008

    invoke-virtual {v1, v3}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 4876
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4877
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v3, 0x7f0e0109

    invoke-interface {v1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 4880
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 4881
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 4882
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    invoke-virtual {v1, v3}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 4883
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    move v1, v2

    .line 4884
    goto :goto_0

    .line 4859
    .end local v0    # "v":Landroid/view/View;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 4869
    .restart local v0    # "v":Landroid/view/View;
    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 4885
    .end local v0    # "v":Landroid/view/View;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_0

    .line 4886
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/Menu;->clear()V

    .line 4887
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 4888
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    goto :goto_0
.end method

.method private startNFCWritingActivity()V
    .locals 4

    .prologue
    .line 4166
    const/4 v1, 0x0

    .line 4167
    .local v1, "labelInfo":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 4168
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getStartVoiceLabelAfterRecord()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4169
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSavedID()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 4175
    :cond_0
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4176
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4177
    const-string v2, "labelinfo"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4178
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setNFCfilename(Ljava/lang/String;)V

    .line 4179
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 4180
    return-void

    .line 4171
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private startSearchTask(Ljava/lang/String;)V
    .locals 5
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4544
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    if-eqz v1, :cond_0

    .line 4545
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->cancel(Z)Z

    .line 4546
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .line 4549
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    if-nez v1, :cond_1

    .line 4550
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .line 4552
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_1

    .line 4553
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->setFileOperationListener(Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;)V

    .line 4555
    new-array v0, v4, [Ljava/lang/String;

    aput-object p1, v0, v3

    .line 4558
    .local v0, "param":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 4559
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_1

    .line 4560
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 4564
    .end local v0    # "param":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private updateEQ(Z)V
    .locals 3
    .param p1, "start"    # Z

    .prologue
    const/16 v2, 0x83a

    const/16 v1, 0x839

    .line 3100
    if-eqz p1, :cond_0

    .line 3101
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3102
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 3107
    :goto_0
    return-void

    .line 3104
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3105
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method private updateMicState()V
    .locals 2

    .prologue
    const/16 v1, 0x83b

    .line 3110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3111
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 3112
    return-void
.end method

.method private updateTime()V
    .locals 2

    .prologue
    const/16 v1, 0x835

    .line 3115
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3116
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 3117
    return-void
.end method

.method private updateUIThread()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x80

    .line 2826
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v0

    .line 2828
    .local v0, "state":I
    const-string v1, "VoiceNoteMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateUIThread() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2830
    packed-switch v0, :pswitch_data_0

    .line 2852
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2853
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2854
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2855
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 2856
    invoke-static {p0, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    .line 2859
    :cond_0
    :goto_0
    return-void

    .line 2832
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->restartRecIconBlinkThread()V

    .line 2833
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->restartEQThread()V

    .line 2834
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2835
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v1, :cond_1

    const-string v1, "record_mode"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 2837
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 2838
    :cond_2
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasLED()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2839
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    goto :goto_0

    .line 2844
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->restartTimeBlinkThread()V

    .line 2845
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2846
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2847
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 2848
    invoke-static {p0, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    goto :goto_0

    .line 2830
    nop

    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public TransitAnimationChanged(Z)V
    .locals 11
    .param p1, "isAnimating"    # Z

    .prologue
    const/4 v10, 0x3

    const-wide/16 v8, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 4626
    sget-object v5, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v5

    .line 4627
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    .line 4628
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4629
    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v4, v6, :cond_3

    .line 4630
    if-nez p1, :cond_2

    .line 4631
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 4632
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v4, :cond_1

    .line 4633
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4634
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_bookmarks()V

    .line 4636
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 4637
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 4737
    :cond_1
    :goto_0
    return-void

    .line 4628
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 4641
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNLibraryExpandableListFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 4642
    .local v1, "frag":Landroid/app/Fragment;
    instance-of v4, v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v4, :cond_1

    move-object v2, v1

    .line 4643
    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 4644
    .local v2, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->initListViewSelection()V

    goto :goto_0

    .line 4647
    .end local v1    # "frag":Landroid/app/Fragment;
    .end local v2    # "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_3
    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v4, :cond_b

    .line 4648
    if-eqz p1, :cond_7

    .line 4649
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionbarUpdate:Z

    .line 4650
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 4651
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v4, :cond_4

    .line 4652
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v4, :cond_5

    .line 4653
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 4663
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 4664
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 4655
    :cond_5
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v10, :cond_6

    .line 4657
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    goto :goto_1

    .line 4659
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_main()V

    goto :goto_1

    .line 4667
    :cond_7
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionbarUpdate:Z

    if-nez v4, :cond_8

    .line 4668
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 4669
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v4, :cond_8

    .line 4670
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v4, :cond_9

    .line 4671
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 4682
    :cond_8
    :goto_2
    iput-boolean v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionbarUpdate:Z

    .line 4683
    const-string v4, "category_text"

    invoke-static {v4, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v4, v8, v4

    if-eqz v4, :cond_1

    .line 4684
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v4, :cond_1

    .line 4685
    const-string v4, "category_text"

    invoke-static {v4, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 4686
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    goto/16 :goto_0

    .line 4673
    :cond_9
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v10, :cond_a

    .line 4675
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    goto :goto_2

    .line 4677
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_main()V

    goto :goto_2

    .line 4691
    :cond_b
    if-eqz p1, :cond_1

    .line 4692
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v4, :cond_1

    .line 4693
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 4694
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/high16 v4, 0x7f030000

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 4695
    .local v0, "actionbar_v":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_selectMode(Landroid/view/View;)V

    .line 4697
    const v4, 0x7f0e0001

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    .line 4698
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setFocusable(Z)V

    .line 4699
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 4700
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/voicenote/main/VNMainActivity$16;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$16;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4711
    const v4, 0x7f0e0002

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    .line 4712
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setFocusable(Z)V

    .line 4713
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 4714
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/voicenote/main/VNMainActivity$17;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$17;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4723
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNLibraryExpandableListFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 4724
    .restart local v1    # "frag":Landroid/app/Fragment;
    instance-of v4, v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    if-eqz v4, :cond_c

    move-object v2, v1

    .line 4725
    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .line 4726
    .local v2, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->initListViewSelection()V

    .line 4728
    .end local v2    # "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setEnableButton(Landroid/widget/Button;Z)V

    .line 4729
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    goto/16 :goto_0
.end method

.method public TransitAnimationState(Z)V
    .locals 4
    .param p1, "isAnimating"    # Z

    .prologue
    const/16 v3, 0x866

    const/4 v1, 0x0

    .line 4621
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 4622
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 4623
    return-void

    .line 4622
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public closeOptionsMenu()V
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 1051
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    .line 1052
    invoke-super {p0}, Landroid/app/Activity;->closeOptionsMenu()V

    .line 1053
    return-void
.end method

.method public deleteVVMGreetingFile()V
    .locals 6

    .prologue
    .line 3963
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getLastSavedFileUriString()Ljava/lang/String;

    move-result-object v4

    .line 3964
    .local v4, "lastSavedString":Ljava/lang/String;
    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3965
    .local v1, "fileID":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3966
    .local v2, "fid":J
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->deleteFile(Landroid/content/Context;JLjava/lang/String;)Z

    .line 3967
    sget-object v5, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3971
    .end local v1    # "fileID":Ljava/lang/String;
    .end local v2    # "fid":J
    .end local v4    # "lastSavedString":Ljava/lang/String;
    :goto_0
    return-void

    .line 3968
    :catch_0
    move-exception v0

    .line 3969
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x52

    const/4 v7, 0x1

    .line 4807
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 4808
    const-string v3, "VoiceNoteMainActivity"

    const-string v6, "dispatchKeyEvent()"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4809
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 4810
    .local v2, "keycode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 4811
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    .line 4812
    .local v1, "flags":I
    and-int/lit8 v3, v1, 0x20

    const/16 v6, 0x20

    if-ne v3, v6, :cond_0

    .line 4813
    const-string v3, "VoiceNoteMainActivity"

    const-string v6, "menu canceled"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4814
    iput-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLastPressedMenuKey:J

    .line 4815
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    .line 4837
    .end local v0    # "action":I
    .end local v1    # "flags":I
    .end local v2    # "keycode":I
    :goto_0
    return v3

    .line 4817
    .restart local v0    # "action":I
    .restart local v1    # "flags":I
    .restart local v2    # "keycode":I
    :cond_0
    if-ne v2, v8, :cond_2

    if-nez v0, :cond_2

    .line 4818
    const-string v3, "VoiceNoteMainActivity"

    const-string v6, "menu down"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4819
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLastPressedMenuKey:J

    .line 4837
    .end local v0    # "action":I
    .end local v1    # "flags":I
    .end local v2    # "keycode":I
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0

    .line 4820
    .restart local v0    # "action":I
    .restart local v1    # "flags":I
    .restart local v2    # "keycode":I
    :cond_2
    if-ne v2, v8, :cond_1

    if-ne v0, v7, :cond_1

    .line 4821
    const-string v3, "VoiceNoteMainActivity"

    const-string v6, "menu up"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4822
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 4825
    .local v4, "uptime":J
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    if-eqz v3, :cond_4

    .line 4826
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    .line 4834
    :cond_3
    :goto_2
    iput-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLastPressedMenuKey:J

    goto :goto_1

    .line 4827
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    iget v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v3, :cond_5

    .line 4829
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showMainMenuPopup()Z

    goto :goto_2

    .line 4830
    :cond_5
    iget v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v3, v7, :cond_3

    .line 4831
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showLibraryMenuPopup()Z

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 3816
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isMultiTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3817
    const/4 v0, 0x1

    .line 3819
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected doCancel()V
    .locals 5

    .prologue
    .line 2046
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "doCancel"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2047
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 2049
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v2, :cond_0

    .line 2050
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->cancelRecording()Z

    .line 2051
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->interruptThread(Ljava/lang/Thread;)V

    .line 2052
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    .line 2053
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2054
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 2055
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 2056
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2062
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 2058
    :catch_0
    move-exception v0

    .line 2059
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doCancel RemoteException "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doPause()V
    .locals 5

    .prologue
    .line 1962
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "doPause"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v2, :cond_0

    .line 1977
    :goto_0
    return-void

    .line 1968
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->pauseRecording()Z

    .line 1969
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    .line 1970
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1971
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 1972
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1973
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 1974
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doPause error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doPreRecord()V
    .locals 6

    .prologue
    .line 1947
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "doPreRecord"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    invoke-interface {v1, v2, v4, v5}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->initRecording(Ljava/lang/String;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1950
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->startPreRecording()Z

    .line 1951
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    .line 1959
    :goto_0
    return-void

    .line 1953
    :cond_0
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "initRecording failed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1955
    :catch_0
    move-exception v0

    .line 1956
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VoiceNoteMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doPreRecord error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doRecord(Z)V
    .locals 8
    .param p1, "startVoiceLabelAfterRecord"    # Z

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 1877
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "doRecord"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1878
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAdvancedAndHeadsetMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1879
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v5, :cond_0

    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 1881
    :cond_0
    const v3, 0x7f0b000a

    const v4, 0x7f0b000b

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1944
    :cond_1
    :goto_0
    return-void

    .line 1884
    :cond_2
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 1885
    const v3, 0x7f0b000e

    const v4, 0x7f0b000f

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1891
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v3, :cond_4

    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1895
    :try_start_0
    sget-boolean v3, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v3, :cond_7

    const v2, 0x7f0b00d5

    .line 1897
    .local v2, "messageId":I
    :goto_1
    const v3, 0x7f0b00d3

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1900
    .end local v2    # "messageId":I
    :catch_0
    move-exception v0

    .line 1901
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1906
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_1

    .line 1909
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    invoke-interface {v3, v4, v6, v7}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->initRecording(Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1910
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->startRecording()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1911
    sget-object v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setStartVoiceLabelAfterRecord(Z)V

    .line 1913
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v3, :cond_5

    .line 1914
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->setFromVVM(Z)V

    .line 1917
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    .line 1918
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1919
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 1920
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1921
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f0e000f

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    if-eqz v3, :cond_6

    .line 1922
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f0e000f

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->changVisibleBtn()V

    .line 1924
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 1925
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    new-instance v4, Lcom/sec/android/app/voicenote/main/VNMainActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$4;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    const-wide/16 v6, 0x64

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1940
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :catch_1
    move-exception v0

    .line 1941
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "VoiceNoteMainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doRecord error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1942
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 1895
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    const v2, 0x7f0b00d4

    goto/16 :goto_1

    .line 1934
    :cond_8
    :try_start_2
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "StartRecording failed"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1938
    :cond_9
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "initRecording failed"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method

.method protected doResume()V
    .locals 5

    .prologue
    .line 1980
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "doResume"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1982
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAdvancedAndHeadsetMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1983
    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1985
    :cond_0
    const v2, 0x7f0b000a

    const v3, 0x7f0b000b

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2009
    :cond_1
    :goto_0
    return-void

    .line 1988
    :cond_2
    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 1989
    const v2, 0x7f0b000e

    const v3, 0x7f0b000f

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1997
    :cond_3
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v2, :cond_1

    .line 2000
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->resumeRecording()Z

    .line 2001
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    .line 2002
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2003
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 2004
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2005
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 2006
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doResume error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2007
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected doSave()V
    .locals 1

    .prologue
    .line 2042
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doSave(Z)V

    .line 2043
    return-void
.end method

.method protected doSave(Z)V
    .locals 8
    .param p1, "enable"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2012
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "doSave"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 2015
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_0

    .line 2016
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z

    .line 2019
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v3, :cond_1

    .line 2020
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->setFromVVM(Z)V

    .line 2023
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    .line 2024
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 2025
    .local v2, "ft":Landroid/app/FragmentTransaction;
    const/4 v0, 0x0

    .line 2026
    .local v0, "convertedText":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    if-eqz v3, :cond_2

    .line 2027
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getConvertedText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2029
    :cond_2
    const/4 v3, 0x0

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 2030
    invoke-direct {p0, v2, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;Z)V

    .line 2031
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2032
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2037
    .end local v0    # "convertedText":Ljava/lang/CharSequence;
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    :goto_0
    const-string v3, "change_notification_resume"

    invoke-static {v3, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 2038
    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateEQ(Z)V

    .line 2039
    return-void

    .line 2033
    :catch_0
    move-exception v1

    .line 2034
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "VoiceNoteMainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSave error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2035
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public finishBookmarkActionMode()V
    .locals 2

    .prologue
    .line 4484
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finishCustomActionMode()V

    .line 4485
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onUpdateState(Z)V

    .line 4486
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    .line 4487
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 4488
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    .line 4490
    :cond_0
    return-void
.end method

.method public finishCustomActionMode()V
    .locals 1

    .prologue
    .line 4606
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4607
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->finishActionMode()V

    .line 4609
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    .line 4610
    return-void
.end method

.method public fromAttachMode()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3289
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 3290
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 3301
    :cond_0
    :goto_0
    return v2

    .line 3294
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 3296
    .local v1, "intentAction":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 3297
    const-string v3, "android.provider.MediaStore.RECORD_SOUND"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3298
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getActionBarIsSearched()Z
    .locals 1

    .prologue
    .line 4979
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-nez v0, :cond_0

    .line 4980
    const/4 v0, 0x0

    .line 4982
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v0

    goto :goto_0
.end method

.method public getActionMode()Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 4598
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4599
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 4601
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBookmark()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4366
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDeleteItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 5034
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method public getFileTitle(I)Ljava/lang/String;
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 5234
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-nez v1, :cond_0

    .line 5235
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNLibraryExpandableListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 5237
    .local v0, "frag":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v1, :cond_1

    .line 5238
    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .end local v0    # "frag":Landroid/app/Fragment;
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 5244
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getItemTitle(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    .line 5240
    .restart local v0    # "frag":Landroid/app/Fragment;
    :cond_1
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "getFileTitle() fragment is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 5241
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMainPanelTextId()I
    .locals 3

    .prologue
    const v0, 0x7f0e0010

    .line 3772
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3773
    const v0, 0x7f0e0011

    .line 3780
    :cond_0
    :goto_0
    return v0

    .line 3774
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3776
    const/4 v1, 0x3

    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 3778
    const v0, 0x7f0e000f

    goto :goto_0
.end method

.method public getSearchItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 5030
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method public getShowFragmentAnimator(Landroid/animation/AnimatorSet;)Landroid/animation/Animator;
    .locals 6
    .param p1, "animatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 3369
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v1

    .line 3371
    .local v1, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 3372
    .local v0, "animator":Landroid/animation/Animator;
    instance-of v5, v0, Landroid/animation/ObjectAnimator;

    if-eqz v5, :cond_0

    move-object v5, v0

    .line 3373
    check-cast v5, Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    .line 3374
    .local v4, "propertyName":Ljava/lang/String;
    const-string v5, "translationY"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3375
    new-instance v5, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v5}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0

    .line 3379
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v4    # "propertyName":Ljava/lang/String;
    :cond_1
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 3380
    .local v3, "newAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 3381
    return-object v3
.end method

.method public isLockScreen()Z
    .locals 6

    .prologue
    .line 2382
    const-string v2, "manivel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isLockScreen() start :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPowerManager:Landroid/os/PowerManager;

    if-nez v2, :cond_0

    .line 2384
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPowerManager:Landroid/os/PowerManager;

    .line 2386
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v2, :cond_1

    .line 2387
    const-string v2, "keyguard"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 2389
    :cond_1
    const/4 v1, 0x0

    .line 2390
    .local v1, "isScreenOn":Z
    const/4 v0, 0x0

    .line 2391
    .local v0, "isKeyguardLock":Z
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v2, :cond_2

    .line 2392
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    .line 2394
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v2, :cond_3

    .line 2395
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    .line 2397
    :cond_3
    const-string v2, "manivel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isLockScreen() end :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2398
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public mainActivityInvalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 921
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v0

    if-nez v0, :cond_0

    .line 922
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 923
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->closeOptionsMenu()V

    .line 925
    :cond_0
    return-void
.end method

.method protected moveSecretBox(JZ)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "movetosecret"    # Z

    .prologue
    .line 4153
    const-string v3, "VoiceNoteMainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moveSecretBox E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4155
    .local v1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4157
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    move-result-object v2

    .line 4158
    .local v2, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 4159
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v3, "target_datas"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4160
    const-string v3, "movetosecret"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4161
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_FILE"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 4162
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "moveSecretBox X"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4163
    return-void
.end method

.method public onActionItemSelected(IJ)Z
    .locals 8
    .param p1, "item"    # I
    .param p2, "item_id"    # J

    .prologue
    const/high16 v7, 0x20000000

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 4999
    iput-wide p2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    .line 5001
    packed-switch p1, :pswitch_data_0

    .line 5026
    :goto_0
    return v6

    .line 5003
    :pswitch_0
    invoke-static {p0, p2, p3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 5004
    .local v2, "labelinfo":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNFCEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5005
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5006
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5007
    const-string v3, "labelinfo"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5008
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setNFCfilename(Ljava/lang/String;)V

    .line 5009
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 5011
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v3, "show_nfc_enable"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5012
    new-instance v3, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;-><init>(Landroid/content/Context;Z)V

    new-array v4, v5, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 5013
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5014
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5015
    const-string v3, "labelinfo"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5016
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setNFCfilename(Ljava/lang/String;)V

    .line 5017
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 5019
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    move-result-object v0

    .line 5021
    .local v0, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 5001
    :pswitch_data_0
    .packed-switch 0x7f0e00fd
        :pswitch_0
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x1

    const-wide/16 v10, -0x1

    const/4 v8, -0x1

    .line 1799
    const-string v5, "VoiceNoteMainActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onActivityResult requestCode:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " resultCode:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801
    const v5, 0x186a1

    if-ne p1, v5, :cond_2

    if-ne p2, v8, :cond_2

    if-eqz p3, :cond_2

    .line 1803
    const-string v5, "category_text"

    invoke-static {v5, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1804
    .local v2, "id":J
    const-string v5, "editedId"

    invoke-virtual {p3, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1805
    .local v4, "updatedId":I
    const-string v5, "editedType"

    invoke-virtual {p3, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1807
    .local v0, "edited_type":I
    const/4 v5, 0x2

    if-ne v0, v5, :cond_1

    .line 1808
    const-string v5, "VoiceNoteMainActivity"

    const-string v6, "[HIYA] onActivityResult : "

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1809
    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onUpdateState(Z)V

    .line 1846
    .end local v0    # "edited_type":I
    .end local v2    # "id":J
    .end local v4    # "updatedId":I
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1847
    return-void

    .line 1810
    .restart local v0    # "edited_type":I
    .restart local v2    # "id":J
    .restart local v4    # "updatedId":I
    :cond_1
    cmp-long v5, v2, v10

    if-eqz v5, :cond_0

    .line 1811
    int-to-long v6, v4

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    .line 1812
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1814
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1817
    :pswitch_1
    const-string v5, "category_text"

    invoke-static {v5, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 1819
    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onUpdateState(Z)V

    goto :goto_0

    .line 1824
    .end local v0    # "edited_type":I
    .end local v2    # "id":J
    .end local v4    # "updatedId":I
    :cond_2
    const v5, 0x186a2

    if-ne p1, v5, :cond_3

    if-ne p2, v8, :cond_3

    if-eqz p3, :cond_3

    .line 1826
    const-string v5, "file_to_play"

    invoke-virtual {p3, v5, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1827
    .restart local v2    # "id":J
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1828
    .local v1, "msg":Landroid/os/Message;
    const/16 v5, 0x42e

    iput v5, v1, Landroid/os/Message;->what:I

    .line 1829
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1830
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1832
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "id":J
    :cond_3
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 1834
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onBackPressed()V

    goto :goto_0

    .line 1838
    :pswitch_3
    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResultRecord:Z

    goto :goto_0

    .line 1812
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1832
    :pswitch_data_1
    .packed-switch 0xbb8
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onAlertDialogPositiveBtn(Z)V
    .locals 7
    .param p1, "positive"    # Z

    .prologue
    const/16 v6, 0x7d1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1741
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    sget-object v3, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->FIRST_POPUP:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    if-ne v2, v3, :cond_2

    .line 1742
    if-eqz p1, :cond_1

    .line 1743
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1744
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x384

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1745
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->GOTO_LIST:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .line 1788
    :cond_0
    :goto_0
    return-void

    .line 1748
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->deleteVVMGreetingFile()V

    .line 1749
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .line 1750
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doCancel()V

    goto :goto_0

    .line 1753
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    sget-object v3, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    if-ne v2, v3, :cond_3

    .line 1755
    if-eqz p1, :cond_5

    .line 1756
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f0e0013

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 1757
    .local v1, "list":Landroid/app/Fragment;
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    if-eqz v2, :cond_3

    .line 1758
    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .end local v1    # "list":Landroid/app/Fragment;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->doAttach()V

    .line 1766
    :cond_3
    :goto_1
    iget v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v5, v2, :cond_7

    .line 1767
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1779
    :cond_4
    :goto_2
    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isPressEditBtn:Z

    if-eqz v2, :cond_0

    .line 1780
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-static {v2, p0, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showEditDialoginPlay(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/content/Context;Landroid/app/FragmentManager;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    .line 1781
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isPressEditBtn:Z

    goto :goto_0

    .line 1761
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->deleteVVMGreetingFile()V

    .line 1762
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finish()V

    goto :goto_1

    .line 1769
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isPlayerActive()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1771
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1773
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_4

    .line 1774
    if-eqz p1, :cond_4

    .line 1775
    const/4 v2, 0x0

    invoke-virtual {v0, v4, v5, v2}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_2

    .line 1784
    .end local v0    # "fragment":Landroid/app/Fragment;
    :cond_7
    if-eqz p1, :cond_0

    .line 1785
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doCancel()V

    goto/16 :goto_0
.end method

.method public onBackPressed()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1353
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "onBackPressed"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1355
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V

    .line 1357
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isbtnAnim:Z

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1491
    :cond_2
    :goto_0
    return-void

    .line 1361
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    sget-object v4, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    if-ne v3, v4, :cond_4

    .line 1362
    const v3, 0x7f0b0177

    const v4, 0x7f0b017a

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1367
    :cond_4
    iget v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v5, v3, :cond_f

    .line 1368
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-nez v3, :cond_5

    .line 1369
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 1373
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1374
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1375
    sget-object v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v3, :cond_6

    .line 1376
    sget-object v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v4, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    .line 1378
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v3

    if-nez v3, :cond_7

    .line 1379
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "VNLibraryExpandableListFragment"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 1381
    .local v2, "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v2, :cond_7

    .line 1382
    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setFocusListView(Z)V

    .line 1383
    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEnableListView(Z)V

    .line 1386
    .end local v2    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1390
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isPlayerActive()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1395
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 1396
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 1397
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->removeToolbarHelp()V

    .line 1399
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1400
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "VNLibraryExpandableListFragment"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 1402
    .restart local v2    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v2, :cond_2

    .line 1403
    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEnableListView(Z)V

    goto/16 :goto_0

    .line 1407
    .end local v2    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1408
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    goto/16 :goto_0

    .line 1412
    :cond_a
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v3, :cond_b

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1413
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->clickHomeAtActionBar()V

    goto/16 :goto_0

    .line 1417
    :cond_b
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1418
    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setRecordedNumber(Ljava/lang/String;)V

    .line 1419
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->getnumBindingActivity()I

    move-result v3

    if-le v3, v5, :cond_c

    .line 1420
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "onBackPressed - unbindFromIService"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->unbindFromService(Landroid/content/Context;)V

    .line 1433
    :goto_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0

    .line 1423
    :cond_c
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "onBackPressed - stopService"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_d

    .line 1426
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->unregisterCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1431
    :cond_d
    :goto_2
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->stopService(Landroid/content/Intent;)Z

    goto :goto_1

    .line 1428
    :catch_0
    move-exception v0

    .line 1429
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 1437
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1438
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1, v6, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 1439
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1440
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1441
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    goto/16 :goto_0

    .line 1444
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_f
    const/4 v3, 0x2

    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v3, v4, :cond_10

    .line 1445
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1446
    .restart local v1    # "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1, v6, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 1447
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1448
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V

    goto/16 :goto_0

    .line 1452
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_10
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v3

    const/16 v4, 0x3eb

    if-eq v3, v4, :cond_11

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v3

    const/16 v4, 0x3ec

    if-ne v3, v4, :cond_13

    .line 1455
    :cond_11
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_2

    .line 1456
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getRecDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gez v3, :cond_12

    .line 1457
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doCancel()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1471
    :catch_1
    move-exception v0

    .line 1472
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 1459
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_12
    const/4 v3, 0x1

    :try_start_2
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z

    .line 1460
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doSave()V

    .line 1461
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1462
    .restart local v1    # "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawPanelText(Landroid/app/FragmentTransaction;)V

    .line 1463
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v1, v3, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMicView(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 1464
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1465
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "category_label_color"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateFilename(Ljava/lang/String;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1475
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_13
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->getnumBindingActivity()I

    move-result v3

    if-le v3, v5, :cond_14

    .line 1476
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "onBackPressed - unbindFromIService"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->unbindFromService(Landroid/content/Context;)V

    .line 1489
    :goto_3
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0

    .line 1479
    :cond_14
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "onBackPressed - stopService"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1481
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v3, :cond_15

    .line 1482
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->unregisterCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1487
    :cond_15
    :goto_4
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->stopService(Landroid/content/Intent;)Z

    goto :goto_3

    .line 1484
    :catch_2
    move-exception v0

    .line 1485
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4
.end method

.method public onBookmarkAdded(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V
    .locals 5
    .param p1, "bookmark"    # Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .prologue
    const/16 v4, 0x32

    .line 4322
    const-string v0, ""

    .line 4323
    .local v0, "path":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 4324
    sget-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 4326
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 4327
    .local v1, "tempfragment":Landroid/app/Fragment;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    if-nez v2, :cond_3

    .line 4328
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    .line 4336
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->IsDuplicated(Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4342
    :cond_2
    :goto_0
    return-void

    .line 4329
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v4, :cond_1

    .line 4330
    if-nez v1, :cond_2

    .line 4331
    const v2, 0x7f0b001d

    const v3, 0x7f0b001c

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(III)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 4339
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->insetSttToBookmark(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V

    .line 4340
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4341
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBookmarkChildPressed(JI)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "miliSec"    # I

    .prologue
    .line 4305
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4306
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onBookmarkChildPressed(JI)V

    .line 4308
    :cond_0
    return-void
.end method

.method public onBookmarkItemClick(I)V
    .locals 1
    .param p1, "miliSecond"    # I

    .prologue
    .line 4388
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4389
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onPressBookmarkListView(I)V

    .line 4391
    :cond_0
    return-void
.end method

.method public onBookmarksChanged(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4294
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    .line 4295
    const-string v0, ""

    .line 4296
    .local v0, "path":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 4297
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 4300
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateBookmarkList(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 4301
    return-void
.end method

.method public onBookmarksRenamed(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4312
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const-string v0, ""

    .line 4313
    .local v0, "path":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 4314
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 4316
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    .line 4317
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 4318
    return-void
.end method

.method public onControlButtonSelected(I)Z
    .locals 10
    .param p1, "button"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1495
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1496
    const-string v4, "VoiceNoteMainActivity"

    const-string v5, "onControlButtonSelected - hidden state"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    :cond_0
    :goto_0
    return v3

    .line 1499
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v5, :cond_0

    .line 1502
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v6, 0x7d1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1505
    sget-object v5, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v5

    .line 1506
    :try_start_0
    iget-boolean v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v6, :cond_2

    .line 1507
    monitor-exit v5

    goto :goto_0

    .line 1509
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1510
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1593
    :sswitch_0
    const-string v4, "initial_player_access"

    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 1594
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->removeToolbarHelp()V

    .line 1595
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    .line 1596
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doResume()V

    goto :goto_0

    .line 1512
    :sswitch_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doRecord(Z)V

    move v3, v4

    .line 1513
    goto :goto_0

    .line 1516
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doPause()V

    move v3, v4

    .line 1517
    goto :goto_0

    .line 1520
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doResume()V

    move v3, v4

    .line 1521
    goto :goto_0

    .line 1524
    :sswitch_4
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Landroid/os/SystemClock;->sleep(J)V

    .line 1526
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getRecDuration()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    cmp-long v5, v6, v8

    if-gez v5, :cond_3

    .line 1527
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doCancel()V

    :goto_1
    move v3, v4

    .line 1535
    goto :goto_0

    .line 1529
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doSave(Z)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1531
    :catch_0
    move-exception v0

    .line 1532
    .local v0, "e1":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1538
    .end local v0    # "e1":Landroid/os/RemoteException;
    :sswitch_5
    const v5, 0x7f0e00ca

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 1540
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v3, :cond_4

    .line 1541
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doPause()V

    .line 1544
    :cond_4
    const v3, 0x7f0b0023

    const v5, 0x7f0b0022

    invoke-static {v3, v5, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    move v3, v4

    .line 1545
    goto/16 :goto_0

    .line 1548
    :sswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->addBookmark()V

    move v3, v4

    .line 1549
    goto/16 :goto_0

    .line 1564
    :sswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1565
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 1566
    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v5, :cond_6

    .line 1567
    invoke-direct {p0, v1, v4, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 1571
    :goto_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1572
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1573
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    if-eqz v3, :cond_5

    .line 1574
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_5
    move v3, v4

    .line 1577
    goto/16 :goto_0

    .line 1569
    :cond_6
    invoke-direct {p0, v1, v3, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    goto :goto_2

    .line 1580
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :sswitch_8
    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-nez v5, :cond_0

    .line 1582
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_7

    .line 1583
    new-instance v2, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;-><init>()V

    .line 1584
    .local v2, "mode":Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .end local v2    # "mode":Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;
    :cond_7
    move v3, v4

    .line 1586
    goto/16 :goto_0

    .line 1589
    :sswitch_9
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showMainMenuPopup()Z

    move v3, v4

    .line 1590
    goto/16 :goto_0

    .line 1510
    :sswitch_data_0
    .sparse-switch
        0x7f0e0050 -> :sswitch_0
        0x7f0e00c5 -> :sswitch_7
        0x7f0e00c7 -> :sswitch_1
        0x7f0e00c9 -> :sswitch_8
        0x7f0e00ca -> :sswitch_5
        0x7f0e00cb -> :sswitch_4
        0x7f0e00cc -> :sswitch_3
        0x7f0e00cd -> :sswitch_6
        0x7f0e00ce -> :sswitch_2
        0x7f0e00da -> :sswitch_9
    .end sparse-switch
.end method

.method public onControlButtonSelected(IJ)Z
    .locals 12
    .param p1, "button"    # I
    .param p2, "id"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3828
    :try_start_0
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3829
    :try_start_1
    iget-boolean v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v8, :cond_1

    .line 3830
    monitor-exit v7

    .line 3956
    :cond_0
    :goto_0
    return v5

    .line 3832
    :cond_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3833
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 3834
    .local v2, "ft":Landroid/app/FragmentTransaction;
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 3837
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v7, :cond_2

    .line 3838
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7}, Landroid/widget/SearchView;->clearFocus()V

    .line 3839
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/SearchView;->setFocusable(Z)V

    .line 3840
    const-string v7, "input_method"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 3841
    .local v3, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v3, v7, v8}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 3845
    .end local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 3846
    :cond_3
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "onControlButtonSelected : library_view_fragment : isCallIdle"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 3847
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b00d6

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 3949
    :cond_4
    :goto_1
    const v7, 0x7f0e00a3

    if-eq v7, p1, :cond_5

    .line 3950
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 3952
    :cond_5
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    move v5, v6

    .line 3953
    goto :goto_0

    .line 3832
    .end local v2    # "ft":Landroid/app/FragmentTransaction;
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v6
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0

    .line 3954
    :catch_0
    move-exception v1

    .line 3955
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v6, "VoiceNoteMainActivity"

    const-string v7, "catch the exception while click player_control_setting_mode_button in onControlButtonSelected"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3850
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    .restart local v2    # "ft":Landroid/app/FragmentTransaction;
    :cond_6
    :try_start_5
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V

    .line 3851
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7, p2, p3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onPressListView(J)Z

    goto :goto_1

    .line 3856
    :sswitch_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPause()V

    goto :goto_1

    .line 3861
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doResume()V

    goto :goto_1

    .line 3865
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V

    .line 3866
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct {p0, v2, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 3867
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 3868
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V

    goto :goto_1

    .line 3873
    :sswitch_4
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_4

    .line 3876
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v7

    int-to-long v8, v7

    const-wide/16 v10, 0x44b

    cmp-long v7, v8, v10

    if-gez v7, :cond_7

    .line 3877
    const v7, 0x7f0b00a7

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 3878
    :cond_7
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v7

    sget-object v8, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v8

    add-int/lit8 v8, v8, -0x64

    if-lt v7, v8, :cond_8

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 3881
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPause()V

    .line 3882
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateControlButtons()V

    .line 3886
    const-string v7, "show_trim_warning"

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_9

    .line 3887
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;-><init>()V

    .line 3888
    .local v0, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->setCurrentPlayingID(J)V

    .line 3889
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3891
    .end local v0    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startTrim()V

    goto/16 :goto_1

    .line 3897
    :sswitch_5
    const-wide/16 v8, 0x46

    cmp-long v7, v8, p2

    if-eqz v7, :cond_a

    const-wide/16 v8, 0x49

    cmp-long v7, v8, p2

    if-nez v7, :cond_c

    .line 3898
    :cond_a
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getSaveMode()I

    move-result v7

    const/16 v8, 0x3d

    if-ne v7, v8, :cond_b

    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getTrimResult()I

    move-result v7

    const/16 v8, 0x46

    if-ne v7, v8, :cond_b

    .line 3900
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    .line 3901
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 3902
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlayTrimNewFile()V

    goto/16 :goto_1

    .line 3904
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->restartPlay()V

    .line 3905
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    goto/16 :goto_1

    .line 3908
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    goto/16 :goto_1

    .line 3914
    :sswitch_6
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doNextPlay()V

    goto/16 :goto_1

    .line 3919
    :sswitch_7
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPreviousPlay()V

    goto/16 :goto_1

    .line 3923
    :sswitch_8
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSkipSilenceMode(ZLandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 3927
    :sswitch_9
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_e

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v7

    if-nez v7, :cond_d

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    if-eqz v7, :cond_e

    .line 3928
    :cond_d
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 3931
    :cond_e
    iget-boolean v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    sget-object v8, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    if-ne v7, v8, :cond_f

    .line 3932
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->deleteVVMGreetingFile()V

    .line 3935
    :cond_f
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct {p0, v2, v7, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 3936
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doRecord(Z)V

    goto/16 :goto_1

    .line 3940
    :sswitch_a
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    if-nez v7, :cond_0

    .line 3941
    new-instance v4, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-direct {v4}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;-><init>()V

    .line 3942
    .local v4, "mode":Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v4, v7, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 3834
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e0013 -> :sswitch_0
        0x7f0e0053 -> :sswitch_7
        0x7f0e0054 -> :sswitch_2
        0x7f0e0055 -> :sswitch_6
        0x7f0e0094 -> :sswitch_7
        0x7f0e0095 -> :sswitch_1
        0x7f0e0096 -> :sswitch_2
        0x7f0e0097 -> :sswitch_6
        0x7f0e0099 -> :sswitch_3
        0x7f0e009b -> :sswitch_9
        0x7f0e009c -> :sswitch_a
        0x7f0e00a0 -> :sswitch_4
        0x7f0e00a3 -> :sswitch_8
        0x7f0e00bf -> :sswitch_5
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 263
    const-string v9, "VerificationLog"

    const-string v10, "onCreate"

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v9, "VoiceNoteMainActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onCreate "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/Window;->requestFeature(I)Z

    .line 268
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 269
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWindowStatusBarFlag(Landroid/app/Activity;)V

    .line 271
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 272
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f030005

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 273
    .local v4, "mainView":Landroid/view/View;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setContentView(Landroid/view/View;)V

    .line 275
    const v9, 0x7f0e000d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    .line 276
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mAddCategoryChecked:Z

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->initContext(Landroid/content/Context;)V

    .line 281
    const/16 v9, 0x7d0

    invoke-direct {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setDvfsBooster(I)V

    .line 284
    new-instance v9, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v9, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 285
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v9, :cond_3

    .line 286
    const/4 v9, 0x2

    iput v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 287
    iget v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    iput v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    .line 288
    const/4 v9, 0x1

    sput-boolean v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 298
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromRecordedNumber()Z

    .line 300
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getMaxAmplitude()J

    move-result-wide v10

    long-to-int v9, v10

    iput v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUVThreshold:I

    .line 301
    new-instance v9, Lcom/sec/android/app/voicenote/common/util/VNIntent;

    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v9, p0, v10}, Lcom/sec/android/app/voicenote/common/util/VNIntent;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 302
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 304
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerEasyModeReceiver()V

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEasymode:Z

    .line 307
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    if-eqz v9, :cond_1

    if-nez p1, :cond_1

    .line 308
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->isDestroyed()Z

    move-result v9

    if-eqz v9, :cond_0

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->isFinishing()Z

    move-result v9

    if-nez v9, :cond_1

    .line 309
    :cond_0
    const-string v9, "VoiceNoteMainActivity"

    const-string v10, "onCreate finish previous activity"

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->finish()V

    .line 313
    :cond_1
    sput-object p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    .line 314
    if-nez p1, :cond_4

    .line 315
    const-string v9, "record_mode"

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v8

    .line 316
    .local v8, "record_mode":I
    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onModeChanged(I)V

    .line 317
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v9, :cond_9

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "android.provider.MediaStore.extra.MAX_BYTES"

    const-wide/32 v12, 0x9c5e00

    invoke-virtual {v9, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "mime_type"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    .line 320
    const-string v9, "VoiceNoteMainActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "fromAttachMode mSizeMMS : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mMimetype : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    const-wide/16 v12, 0x2800

    cmp-long v9, v10, v12

    if-ltz v9, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getLimitedRecordingTime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    cmp-long v9, v10, v12

    if-gtz v9, :cond_9

    .line 323
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0b0080

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finish()V

    .line 405
    .end local v8    # "record_mode":I
    :goto_1
    return-void

    .line 290
    :cond_3
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 292
    const v9, 0x7f030022

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    .line 293
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    const v10, 0x7f0e0056

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/SearchView;

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    .line 294
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    const v10, 0x7f0b0114

    invoke-virtual {p0, v10}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->queryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v9, v10}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 296
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    const v10, 0x7f0e0057

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ProgressBar;

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    goto/16 :goto_0

    .line 330
    :cond_4
    const-string v9, "is_actionmode"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    .line 331
    const-string v9, "current_check_item_id"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    .line 332
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v9, :cond_5

    .line 333
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    const-string v10, "searchMode"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->setSearched(Z)V

    .line 336
    :cond_5
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v9, :cond_6

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 338
    .local v3, "libraryFt":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNLibraryExpandableListFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 342
    .local v0, "LibraryFragment":Landroid/app/Fragment;
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 346
    .end local v0    # "LibraryFragment":Landroid/app/Fragment;
    .end local v3    # "libraryFt":Landroid/app/FragmentTransaction;
    :cond_6
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v9, :cond_7

    .line 347
    const-string v9, "searchText"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 349
    .local v7, "query":Ljava/lang/String;
    const-string v9, "VoiceNoteMainActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onCreate()query : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 351
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 352
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_bookmarks()V

    .line 355
    :goto_2
    const-string v9, "keypad"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z

    .line 356
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v10, 0x0

    invoke-virtual {v9, v7, v10}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 357
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setSearchMode(ZZ)V

    .line 366
    .end local v7    # "query":Ljava/lang/String;
    :cond_7
    :goto_3
    const-string v9, "activitymode"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    .line 367
    const-string v9, "filename"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    .line 368
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    if-eqz v9, :cond_8

    .line 369
    const/4 v9, 0x1

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setFixedNewFileName(ZLjava/lang/String;)V

    .line 371
    :cond_8
    const-string v9, "activitymode"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    .line 375
    :cond_9
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 376
    const/4 v9, 0x0

    new-instance v10, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-direct {v10, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v11, 0x20000000

    invoke-virtual {v10, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {p0, v9, v10, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPendingIntent:Landroid/app/PendingIntent;

    .line 378
    const/4 v5, 0x0

    .line 380
    .local v5, "ndef":Landroid/content/IntentFilter;
    :try_start_0
    new-instance v6, Landroid/content/IntentFilter;

    const-string v9, "android.nfc.action.TAG_DISCOVERED"

    invoke-direct {v6, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    .end local v5    # "ndef":Landroid/content/IntentFilter;
    .local v6, "ndef":Landroid/content/IntentFilter;
    :try_start_1
    const-string v9, "android.intent.category.DEFAULT"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v6

    .line 385
    .end local v6    # "ndef":Landroid/content/IntentFilter;
    .restart local v5    # "ndef":Landroid/content/IntentFilter;
    :goto_4
    const/4 v9, 0x1

    new-array v9, v9, [Landroid/content/IntentFilter;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    .line 386
    const/4 v9, 0x1

    new-array v9, v9, [[Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    const-class v13, Landroid/nfc/tech/NfcF;

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    aput-object v11, v9, v10

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->techListsArray:[[Ljava/lang/String;

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->checkNFCRead(Landroid/content/Intent;)V

    .line 394
    :cond_a
    if-eqz p1, :cond_b

    .line 395
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->resetShareDialogFragment()V

    .line 399
    :cond_b
    sget-boolean v9, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v9, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "Source"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    const-string v9, "ATT_VVM"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "Source"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    const/4 v9, 0x1

    :goto_5
    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    .line 404
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerBroadcastReceiverSip(Z)V

    goto/16 :goto_1

    .line 354
    .end local v5    # "ndef":Landroid/content/IntentFilter;
    .restart local v7    # "query":Ljava/lang/String;
    :cond_c
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 359
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 360
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_bookmarks()V

    .line 363
    :goto_6
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setSearchMode(ZZ)V

    goto/16 :goto_3

    .line 362
    :cond_e
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto :goto_6

    .line 382
    .end local v7    # "query":Ljava/lang/String;
    .restart local v5    # "ndef":Landroid/content/IntentFilter;
    :catch_0
    move-exception v1

    .line 383
    .local v1, "e":Ljava/lang/Exception;
    :goto_7
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finish()V

    goto/16 :goto_4

    .line 399
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_f
    const/4 v9, 0x0

    goto :goto_5

    .line 382
    .end local v5    # "ndef":Landroid/content/IntentFilter;
    .restart local v6    # "ndef":Landroid/content/IntentFilter;
    :catch_1
    move-exception v1

    move-object v5, v6

    .end local v6    # "ndef":Landroid/content/IntentFilter;
    .restart local v5    # "ndef":Landroid/content/IntentFilter;
    goto :goto_7
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 917
    const/4 v0, 0x1

    return v0
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 4346
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4347
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateControlButtons()V

    .line 4349
    :cond_0
    return-void
.end method

.method public onDeleteBookmark(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4378
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const-string v0, ""

    .line 4379
    .local v0, "path":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 4380
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 4381
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v1, :cond_1

    .line 4382
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1, p1, v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 4384
    :cond_1
    return-void
.end method

.method public onDeleteFile()V
    .locals 1

    .prologue
    .line 4371
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4372
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    .line 4374
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 827
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    sput-boolean v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 829
    sput-boolean v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeRecording:Z

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 832
    iput-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 834
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerBroadcastReceiverSip(Z)V

    .line 835
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "FRAGMENT_DIALOG"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 838
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 839
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 840
    iput-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMainFragmentWrapperView:Landroid/view/View;

    .line 841
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setRecordedNumber(Ljava/lang/String;)V

    .line 843
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 845
    iput-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    .line 848
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 849
    return-void
.end method

.method public onDoResume()V
    .locals 1

    .prologue
    .line 4424
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4425
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doResume()V

    .line 4427
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showLibraryActionbar()V

    .line 4428
    return-void
.end method

.method public onDoSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "msearchText"    # Ljava/lang/String;

    .prologue
    .line 4402
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v0, :cond_0

    .line 4403
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEmptyList()V

    .line 4405
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_2

    .line 4406
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 4407
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doSearch(Ljava/lang/String;)V

    .line 4411
    :cond_2
    :goto_0
    return-void

    .line 4409
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doSearch(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDoStop()V
    .locals 2

    .prologue
    .line 4415
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isPlayerActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4416
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 4420
    :cond_0
    :goto_0
    return-void

    .line 4417
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4418
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    goto :goto_0
.end method

.method public onFileNameBtnSelected(I)V
    .locals 4
    .param p1, "resourceID"    # I

    .prologue
    .line 1604
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-nez v2, :cond_1

    .line 1623
    :cond_0
    :goto_0
    return-void

    .line 1607
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1609
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isVisible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1612
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1613
    const-string v2, "category_label_id"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    .line 1615
    .local v1, "id":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1616
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const/4 v2, 0x4

    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->setArguments(ILjava/lang/String;I)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1619
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 1607
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e00c8
        :pswitch_0
    .end packed-switch
.end method

.method public onFileNameDialogPositiveBtn(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "id"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "color"    # I
    .param p5, "position"    # I

    .prologue
    .line 1628
    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v1, :cond_0

    .line 1630
    :try_start_0
    sput-object p1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    .line 1631
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v1, p1, p4}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateFilename(Ljava/lang/String;I)V

    .line 1634
    const/4 v1, 0x1

    invoke-static {v1, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setFixedNewFileName(ZLjava/lang/String;)V

    .line 1636
    const-string v1, "category_label_id"

    invoke-static {v1, p2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 1637
    const-string v1, "category_label_title"

    invoke-static {v1, p3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    const-string v1, "category_label_color"

    invoke-static {v1, p4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 1639
    const-string v1, "category_label_position"

    invoke-static {v1, p5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1645
    :cond_0
    :goto_0
    return-void

    .line 1640
    :catch_0
    move-exception v0

    .line 1641
    .local v0, "npe":Ljava/lang/NullPointerException;
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onFileNameDialogPositiveBtn has null pointer exception"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onListitemFileNameDialogPositiveBtn(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 11
    .param p1, "origin_text"    # Ljava/lang/String;
    .param p2, "change_title"    # Ljava/lang/String;
    .param p3, "label_id"    # I
    .param p4, "label_color"    # I

    .prologue
    .line 1655
    :try_start_0
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_1

    .line 1656
    const-wide/16 v2, -0x1

    .line 1657
    .local v2, "currentPlayID":J
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1658
    :cond_0
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    .line 1662
    :goto_0
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    .line 1664
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 1666
    .local v1, "currntPath":Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1667
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v7, "label_id"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1669
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1670
    .local v6, "where":Ljava/lang/StringBuilder;
    const-string v7, "_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1673
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v0, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1676
    if-eqz v1, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v7, :cond_5

    .line 1677
    invoke-static {p0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->DeleteTagsData(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1678
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2, v3, v1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->renameFile(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 1679
    .local v5, "result":I
    const/4 v7, -0x1

    if-ne v5, v7, :cond_3

    .line 1680
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b0086

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1698
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "currntPath":Ljava/lang/String;
    .end local v2    # "currentPlayID":J
    .end local v5    # "result":I
    .end local v6    # "where":Ljava/lang/StringBuilder;
    :cond_1
    :goto_1
    return-void

    .line 1660
    .restart local v2    # "currentPlayID":J
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    goto :goto_0

    .line 1682
    .restart local v0    # "contentValues":Landroid/content/ContentValues;
    .restart local v1    # "currntPath":Ljava/lang/String;
    .restart local v5    # "result":I
    .restart local v6    # "where":Ljava/lang/StringBuilder;
    :cond_3
    if-nez v5, :cond_4

    .line 1683
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b0083

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1692
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "currntPath":Ljava/lang/String;
    .end local v2    # "currentPlayID":J
    .end local v5    # "result":I
    .end local v6    # "where":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v4

    .line 1693
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1694
    const-string v7, "VoiceNoteMainActivity"

    const-string v8, "Fail to update label"

    invoke-static {v7, v8, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1687
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "contentValues":Landroid/content/ContentValues;
    .restart local v1    # "currntPath":Ljava/lang/String;
    .restart local v2    # "currentPlayID":J
    .restart local v5    # "result":I
    .restart local v6    # "where":Ljava/lang/StringBuilder;
    :cond_4
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    .line 1690
    .end local v5    # "result":I
    :cond_5
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshFileName(J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 4742
    const/4 v0, 0x0

    return v0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 3811
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onModeChanged(I)V
    .locals 7
    .param p1, "recordMode"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1706
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAdvancedAndHeadsetMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1707
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 1708
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 1709
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 1710
    const-string v1, "record_mode"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 1711
    if-eq p1, v5, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 1712
    :cond_0
    const v1, 0x7f0b000a

    const v2, 0x7f0b000b

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1718
    :cond_1
    :goto_0
    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v1, :cond_2

    .line 1719
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "record_mode"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v6, :cond_4

    .line 1722
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 1727
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1728
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v4, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 1729
    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawPanelText(Landroid/app/FragmentTransaction;)V

    .line 1730
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1732
    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v1, v5, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1736
    :goto_2
    return-void

    .line 1713
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_3
    if-ne p1, v6, :cond_1

    .line 1714
    const v1, 0x7f0b000e

    const v2, 0x7f0b000f

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1724
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_main()V

    goto :goto_1

    .line 1734
    .restart local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    goto :goto_2
.end method

.method public onMoveToBookmark(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 4395
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4396
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setProgress(I)V

    .line 4398
    :cond_0
    return-void
.end method

.method public onNFCEnableButtonSelected(I)V
    .locals 1
    .param p1, "button"    # I

    .prologue
    .line 4614
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4615
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V

    .line 4617
    :cond_1
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1314
    const-string v1, "VoiceNoteMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1316
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromAttachMode()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    .line 1317
    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1318
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    .line 1321
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v1, :cond_2

    const-string v1, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.nfc.action.TECH_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1325
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b015a

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1349
    :goto_0
    return-void

    .line 1329
    :cond_2
    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, "audio/amr"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1330
    const-string v1, "android.provider.MediaStore.RECORD_SOUND"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331
    const-string v1, "android.provider.MediaStore.extra.MAX_BYTES"

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1332
    const-string v1, "mime_type"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1335
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setIntent(Landroid/content/Intent;)V

    .line 1336
    const-string v1, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.nfc.action.TECH_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1339
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->checkNFCRead(Landroid/content/Intent;)V

    .line 1340
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 1341
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_5

    .line 1342
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1346
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    :cond_5
    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    iput v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    .line 1348
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1057
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v10

    .line 1058
    :try_start_0
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v9, :cond_0

    .line 1059
    const/4 v9, 0x0

    monitor-exit v10

    .line 1309
    :goto_0
    return v9

    .line 1061
    :cond_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1062
    const-string v9, "VoiceNoteMainActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onOptionsItemSelected "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    const/4 v3, 0x0

    .line 1066
    .local v3, "intent":Landroid/content/Intent;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    .line 1309
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v9

    goto :goto_0

    .line 1061
    .end local v3    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v9

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v9

    .line 1068
    .restart local v3    # "intent":Landroid/content/Intent;
    :sswitch_0
    const v9, 0x7f0e00c8

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onFileNameBtnSelected(I)V

    goto :goto_1

    .line 1078
    :sswitch_1
    new-instance v9, Landroid/content/Intent;

    const-class v10, Lcom/sec/android/app/voicenote/settings/VNSettingsActivity;

    invoke-direct {v9, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1082
    :sswitch_2
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    .line 1086
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-nez v9, :cond_2

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1087
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v9, :cond_3

    .line 1088
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 1091
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v9, :cond_1

    .line 1092
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->onStartActionMenu(I)V

    goto :goto_1

    .line 1098
    :sswitch_3
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    .line 1102
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-nez v9, :cond_4

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1103
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v9, :cond_5

    .line 1104
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 1108
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v9, :cond_1

    .line 1109
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->onStartActionMenu(I)V

    goto :goto_1

    .line 1114
    :sswitch_4
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    .line 1117
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-nez v9, :cond_6

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1118
    :cond_6
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    invoke-static {p0, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setArguments(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    move-result-object v1

    .line 1119
    .local v1, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "FRAGMENT_DIALOG_SETAS"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1124
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;
    :sswitch_5
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    .line 1127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1128
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1129
    invoke-static {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasTagData(Landroid/content/Context;Ljava/util/ArrayList;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1130
    .local v2, "hasNFC":Ljava/lang/Boolean;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1131
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isPressEditBtn:Z

    .line 1132
    const v9, 0x7f0b0105

    const v10, 0x7f0b013d

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1136
    :cond_7
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    invoke-static {v9, p0, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showEditDialoginPlay(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/content/Context;Landroid/app/FragmentManager;)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    goto/16 :goto_1

    .line 1142
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v2    # "hasNFC":Ljava/lang/Boolean;
    :sswitch_6
    const/4 v6, 0x1

    .line 1143
    .local v6, "movetosecret":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v9

    const v10, 0x7f0e0111

    if-ne v9, v10, :cond_8

    .line 1144
    const/4 v6, 0x0

    .line 1146
    :cond_8
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    .line 1149
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-nez v9, :cond_9

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1150
    :cond_9
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    invoke-virtual {p0, v10, v11, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->moveSecretBox(JZ)V

    .line 1151
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    goto/16 :goto_1

    .line 1153
    :cond_a
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->onStartActionMenu(I)V

    goto/16 :goto_1

    .line 1158
    .end local v6    # "movetosecret":Z
    :sswitch_7
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;-><init>()V

    .line 1159
    .local v1, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1164
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;
    :sswitch_8
    new-instance v9, Landroid/content/Intent;

    const-class v10, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    invoke-direct {v9, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v10, 0x186a1

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1166
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "MANA"

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1170
    :sswitch_9
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 1171
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1172
    .local v8, "title":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;-><init>()V

    .local v1, "dialog":Landroid/app/DialogFragment;
    move-object v9, v1

    .line 1173
    check-cast v9, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    invoke-virtual {v9, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->SetParameter(Ljava/lang/String;)V

    .line 1174
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v9, v10}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1179
    .end local v1    # "dialog":Landroid/app/DialogFragment;
    .end local v8    # "title":Ljava/lang/String;
    :sswitch_a
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 1180
    sget-boolean v9, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v9, :cond_b

    const v5, 0x7f0b00d5

    .line 1182
    .local v5, "messageId":I
    :goto_2
    const v9, 0x7f0b0048

    invoke-static {v9, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1180
    .end local v5    # "messageId":I
    :cond_b
    const v5, 0x7f0b00d4

    goto :goto_2

    .line 1186
    :cond_c
    const-string v9, "show_policy_info"

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_d

    .line 1187
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 1188
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;-><init>()V

    .line 1189
    .local v1, "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1192
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    const/16 v10, 0x85d    # 3.0E-42f

    invoke-virtual {v9, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1193
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    const/16 v10, 0x85d    # 3.0E-42f

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 1195
    :cond_e
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    const/16 v10, 0x85d    # 3.0E-42f

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 1201
    :sswitch_b
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v9

    if-nez v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_f

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_f
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v9

    if-nez v9, :cond_1

    .line 1204
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->addBookmarkList()V

    .line 1205
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_bookmarks()V

    goto/16 :goto_1

    .line 1210
    :sswitch_c
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_10

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1213
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v10

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->selectBookmark(II)V

    goto/16 :goto_1

    .line 1218
    :sswitch_d
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_11

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_11
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1221
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v10

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->selectBookmark(II)V

    goto/16 :goto_1

    .line 1226
    :sswitch_e
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNFCEnabled(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 1227
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_12

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1228
    :cond_12
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V

    goto/16 :goto_1

    .line 1231
    :cond_13
    const-string v9, "show_nfc_enable"

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_15

    .line 1232
    new-instance v9, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;

    const/4 v10, 0x0

    invoke-direct {v9, p0, v10}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;-><init>(Landroid/content/Context;Z)V

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Boolean;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1233
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_14

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1234
    :cond_14
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V

    goto/16 :goto_1

    .line 1237
    :cond_15
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v9

    if-nez v9, :cond_16

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1238
    :cond_16
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    invoke-static {p0, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 1240
    .local v4, "labelinfo":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    move-result-object v1

    .line 1242
    .local v1, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1248
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    .end local v4    # "labelinfo":Ljava/lang/String;
    :sswitch_f
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_1

    .line 1249
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    const v9, 0x7f0b006d

    invoke-static {v10, v11, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->setArguments(JI)Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;

    move-result-object v1

    .line 1251
    .local v1, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1256
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
    :sswitch_10
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v9, :cond_1

    .line 1257
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    if-eqz v9, :cond_17

    .line 1258
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    const/4 v10, 0x2

    invoke-interface {v9, v10}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1259
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    invoke-interface {v9, v10}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 1261
    :cond_17
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    if-eqz v9, :cond_18

    .line 1262
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1264
    :cond_18
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1265
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v7

    .line 1266
    .local v7, "setTitle":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 1269
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    const/4 v10, 0x1

    invoke-virtual {v9, v7, v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    .line 1271
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v9, :cond_1

    .line 1274
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 1275
    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_19

    sget-object v9, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v9

    const/16 v10, 0x17

    if-eq v9, v10, :cond_1

    .line 1277
    :cond_19
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setSearchMode(ZZ)V

    goto/16 :goto_1

    .line 1284
    .end local v7    # "setTitle":Ljava/lang/String;
    :sswitch_11
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    if-eqz v9, :cond_1a

    .line 1285
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    goto/16 :goto_1

    .line 1286
    :cond_1a
    iget v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1b

    .line 1287
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showLibraryMenuPopup()Z

    goto/16 :goto_1

    .line 1288
    :cond_1b
    iget v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v9, :cond_1

    .line 1289
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showMainMenuPopup()Z

    goto/16 :goto_1

    .line 1294
    :sswitch_12
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v9, :cond_1c

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v9

    if-eqz v9, :cond_1d

    .line 1295
    :cond_1c
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onBackPressed()V

    goto/16 :goto_1

    .line 1297
    :cond_1d
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->clickHomeAtActionBar()V

    goto/16 :goto_1

    .line 1302
    :sswitch_13
    const-wide/16 v10, 0xc8

    invoke-static {v10, v11}, Landroid/os/SystemClock;->sleep(J)V

    .line 1303
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doRecord(Z)V

    goto/16 :goto_1

    .line 1066
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_3
        0x3 -> :sswitch_11
        0x102002c -> :sswitch_12
        0x7f0e0109 -> :sswitch_13
        0x7f0e010a -> :sswitch_0
        0x7f0e010b -> :sswitch_1
        0x7f0e010c -> :sswitch_2
        0x7f0e010d -> :sswitch_7
        0x7f0e010e -> :sswitch_9
        0x7f0e010f -> :sswitch_8
        0x7f0e0110 -> :sswitch_6
        0x7f0e0111 -> :sswitch_6
        0x7f0e0112 -> :sswitch_3
        0x7f0e0113 -> :sswitch_5
        0x7f0e0114 -> :sswitch_4
        0x7f0e0115 -> :sswitch_b
        0x7f0e0116 -> :sswitch_c
        0x7f0e0117 -> :sswitch_d
        0x7f0e0118 -> :sswitch_a
        0x7f0e0119 -> :sswitch_e
        0x7f0e011a -> :sswitch_f
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 663
    const-string v1, "VoiceNoteMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getApkVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->removeToolbarHelp()V

    .line 666
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    if-eqz v1, :cond_0

    .line 667
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 669
    :cond_0
    invoke-static {p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    .line 670
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v1, :cond_1

    .line 672
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1, p0}, Landroid/nfc/NfcAdapter;->disableForegroundDispatch(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_2

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    .line 681
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isShownPopupMenu:Z

    .line 683
    sput-boolean v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeRecording:Z

    .line 684
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v1, :cond_3

    .line 685
    sput-boolean v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 686
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_3

    .line 687
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeRecording:Z

    .line 690
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v1, :cond_4

    .line 691
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->saveBookmarks()V

    .line 694
    :cond_4
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 695
    return-void

    .line 673
    :catch_0
    move-exception v0

    .line 674
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "catch the exception while disableForegroundDispatch the NfcAdapter"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 13
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v12, 0x7f0b00c1

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 932
    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v4

    const/16 v5, 0x3e8

    if-eq v4, v5, :cond_0

    .line 934
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 935
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    .line 1043
    :goto_0
    return v4

    .line 936
    :cond_0
    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v4, v9, :cond_8

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v4

    const/16 v5, 0x3e8

    if-ne v4, v5, :cond_8

    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v4, :cond_8

    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getState()I

    move-result v4

    const/16 v5, 0x17

    if-eq v4, v5, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getState()I

    move-result v4

    const/16 v5, 0x18

    if-eq v4, v5, :cond_8

    .line 940
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 941
    const/4 v3, 0x0

    .line 942
    .local v3, "menuItem":Landroid/view/MenuItem;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountInLibrary(Landroid/content/Context;)I

    move-result v1

    .line 943
    .local v1, "fileCountinLibrary":I
    const-string v4, "category_text"

    const-wide/16 v6, -0x1

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountByLabel(Landroid/content/Context;I)I

    move-result v0

    .line 944
    .local v0, "fileCountinByLabel":I
    const v4, 0x7f0b0114

    invoke-interface {p1, v8, v9, v9, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    .line 945
    const v4, 0x7f020016

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 946
    invoke-interface {v3, v10}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 947
    invoke-interface {p1, v8}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    .line 948
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 949
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v9}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    .line 950
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    if-eqz v4, :cond_1

    .line 951
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 957
    :cond_1
    :goto_1
    const v4, 0x7f0b005d

    invoke-interface {p1, v8, v10, v10, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    .line 958
    const v4, 0x7f020081

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 959
    invoke-interface {v3, v10}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 960
    invoke-interface {p1, v9}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;

    .line 962
    if-eqz v1, :cond_2

    if-nez v0, :cond_3

    .line 963
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 964
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 967
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 969
    invoke-interface {p1, v8, v11, v11, v12}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    .line 970
    const v4, 0x7f020014

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 971
    invoke-interface {v3, v10}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 973
    invoke-interface {p1, v10}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    .line 974
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 976
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v4, :cond_5

    .line 977
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    new-instance v5, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 1035
    .end local v0    # "fileCountinByLabel":I
    .end local v1    # "fileCountinLibrary":I
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1036
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_b

    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v4, :cond_b

    .line 1038
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->MakeMainMenuPopup(Landroid/view/Menu;)Z

    .line 1043
    :cond_6
    :goto_3
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    goto/16 :goto_0

    .line 954
    .restart local v0    # "fileCountinByLabel":I
    .restart local v1    # "fileCountinLibrary":I
    .restart local v3    # "menuItem":Landroid/view/MenuItem;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v8}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1010
    .end local v0    # "fileCountinByLabel":I
    .end local v1    # "fileCountinLibrary":I
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    :cond_8
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v4, :cond_9

    .line 1011
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const v5, 0x7f0e0013

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    .line 1012
    .local v2, "list":Landroid/app/Fragment;
    instance-of v4, v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    if-eqz v4, :cond_5

    .line 1013
    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .end local v2    # "list":Landroid/app/Fragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getSelectedRadioButton()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 1014
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    invoke-direct {p0, v4, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setEnableButton(Landroid/widget/Button;Z)V

    goto :goto_2

    .line 1019
    :cond_9
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 1020
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1022
    invoke-interface {p1, v8, v11, v11, v12}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    .line 1023
    .restart local v3    # "menuItem":Landroid/view/MenuItem;
    const v4, 0x7f020014

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1024
    invoke-interface {v3, v10}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1025
    invoke-interface {p1, v8}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    .line 1026
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1027
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v11, :cond_5

    .line 1028
    :cond_a
    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-eq v4, v9, :cond_5

    .line 1029
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 1039
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    :cond_b
    iget v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-ne v4, v9, :cond_6

    .line 1040
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->MakeLibraryMenuPopup(Landroid/view/Menu;)Z

    goto/16 :goto_3
.end method

.method protected onRestart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 699
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onRestart"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 702
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 703
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const-string v1, "change_notification_resume"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 704
    const/4 v1, 0x0

    invoke-direct {p0, v0, v3, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 705
    const-string v1, "change_notification_resume"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 707
    :cond_0
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 708
    return-void
.end method

.method protected onResume()V
    .locals 14

    .prologue
    .line 409
    const-string v10, "VerificationLog"

    const-string v11, "onResume"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v10, "VoiceNoteMainActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onResume "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getApkVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 413
    sget-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    if-eqz v10, :cond_1

    .line 414
    const-string v10, "VoiceNoteMainActivity"

    const-string v11, "There is no nfc file. finish"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const/4 v10, 0x0

    sput-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->needToFinish:Z

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finish()V

    .line 659
    :cond_0
    :goto_0
    return-void

    .line 421
    :cond_1
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v10, :cond_2

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRegistedIService:Z

    if-nez v10, :cond_2

    .line 422
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v10, v11}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->registerCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 423
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRegistedIService:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    :cond_2
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    if-nez v10, :cond_3

    .line 430
    new-instance v10, Lcom/sec/android/app/voicenote/common/util/VNIntent;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v10, p0, v11}, Lcom/sec/android/app/voicenote/common/util/VNIntent;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 432
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 434
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromQuickPanel()Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 436
    .local v6, "in":Landroid/content/Intent;
    const-string v10, "isFromQuick"

    const/4 v11, 0x0

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 437
    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setIntent(Landroid/content/Intent;)V

    .line 438
    const-string v10, "category_text"

    const-wide/16 v12, -0x1

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "activitymode"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    iput v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    .line 441
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v10, :cond_4

    .line 442
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getStartVoiceLabelAfterRecord()Z

    move-result v10

    if-eqz v10, :cond_17

    .line 443
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNFCEnabled(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 445
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V

    .line 458
    :goto_2
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setStartVoiceLabelAfterRecord(Z)V

    .line 532
    .end local v6    # "in":Landroid/content/Intent;
    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromRecordedNumber()Z

    .line 534
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v10, :cond_5

    .line 535
    const-string v10, "VoiceNoteMainActivity"

    const-string v11, "NFC enableForegroundDispatch"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPendingIntent:Landroid/app/PendingIntent;

    iget-object v12, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    iget-object v13, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->techListsArray:[[Ljava/lang/String;

    invoke-virtual {v10, p0, v11, v12, v13}, Landroid/nfc/NfcAdapter;->enableForegroundDispatch(Landroid/app/Activity;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    .line 540
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 541
    .local v5, "ft":Landroid/app/FragmentTransaction;
    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawPanelText(Landroid/app/FragmentTransaction;)V

    .line 542
    const-string v10, "change_notification_resume"

    invoke-static {v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 543
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 544
    const-string v10, "change_notification_resume"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 546
    :cond_6
    sget-object v11, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v11

    .line 547
    const/4 v10, 0x0

    :try_start_1
    iput-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    .line 548
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 550
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->resetFileNameDialogPositiveButton()V

    .line 551
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->resetVNAlertDialogFragment()V

    .line 553
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v10, :cond_8

    .line 554
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v10

    if-nez v10, :cond_7

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v10

    if-nez v10, :cond_7

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPrepared()Z

    move-result v10

    if-eqz v10, :cond_1f

    .line 555
    :cond_7
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->addPlayerforBackgroundPlay()V

    .line 560
    :cond_8
    :goto_4
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    iget v11, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    if-eq v10, v11, :cond_22

    .line 561
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    if-eqz v10, :cond_21

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v10

    const/16 v11, 0x3eb

    if-eq v10, v11, :cond_21

    .line 562
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_20

    .line 563
    const/4 v10, 0x1

    sput-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 567
    :goto_5
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 589
    :cond_9
    :goto_6
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v10, :cond_b

    .line 590
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v10, :cond_28

    .line 591
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isShowing()Z

    move-result v10

    if-eqz v10, :cond_27

    .line 592
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v10, :cond_a

    const-string v10, "record_mode"

    invoke-static {v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_26

    .line 593
    :cond_a
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 610
    :cond_b
    :goto_7
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v10

    if-eqz v10, :cond_c

    .line 611
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 614
    :cond_c
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSaveMmsModeState:Z

    if-eqz v10, :cond_d

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v10

    const/16 v11, 0x3eb

    if-eq v10, v11, :cond_d

    .line 615
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEnterAddCategory:Z

    if-eqz v10, :cond_2a

    .line 616
    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 625
    :cond_d
    :goto_8
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEnterAddCategory:Z

    .line 626
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSaveMmsModeState:Z

    .line 627
    const/4 v10, 0x0

    sput-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isbtnAnim:Z

    .line 629
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 631
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v10, :cond_e

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getBookmarkcount()I

    move-result v10

    const/16 v11, 0x32

    if-lt v10, v11, :cond_f

    :cond_e
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v10

    const/16 v11, 0x3eb

    if-eq v10, v11, :cond_10

    .line 632
    :cond_f
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v10, :cond_10

    .line 633
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 636
    :cond_10
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v10, :cond_13

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v10

    if-nez v10, :cond_11

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v10

    if-eqz v10, :cond_13

    .line 637
    :cond_11
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 638
    .local v0, "actionMode":Landroid/view/ActionMode;
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v10, :cond_12

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 639
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->clickHomeAtActionBar()V

    .line 641
    :cond_12
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v11, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v12

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    .line 643
    .local v9, "path":Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_13

    .line 644
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 648
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    .end local v9    # "path":Ljava/lang/String;
    :cond_13
    :try_start_2
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v10, :cond_14

    .line 649
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v10}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->hideNotification()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 654
    :cond_14
    :goto_9
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 655
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v10, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountInLibrary(Landroid/content/Context;)I

    move-result v10

    if-nez v10, :cond_0

    .line 656
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->clickHomeAtActionBar()V

    goto/16 :goto_0

    .line 425
    .end local v5    # "ft":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v3

    .line 426
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 447
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local v6    # "in":Landroid/content/Intent;
    :cond_15
    const-string v10, "show_nfc_enable"

    invoke-static {v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_16

    .line 448
    new-instance v10, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;-><init>(Landroid/content/Context;Z)V

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Boolean;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 449
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V

    goto/16 :goto_2

    .line 451
    :cond_16
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSavedID()J

    move-result-wide v12

    invoke-static {v10, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    .line 453
    .local v8, "labelinfo":Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    move-result-object v2

    .line 455
    .local v2, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v10, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 460
    .end local v2    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    .end local v8    # "labelinfo":Ljava/lang/String;
    :cond_17
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->removeBookmarkList()V

    .line 461
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    .line 462
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v10

    if-eqz v10, :cond_18

    .line 463
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 465
    :cond_18
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v11, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 467
    .restart local v5    # "ft":Landroid/app/FragmentTransaction;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 468
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->showLibraryActionbar()V

    .line 470
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v10, :cond_19

    .line 471
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 473
    :cond_19
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V

    .line 474
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    sget-object v11, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSavedID()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onPressListView(J)Z

    .line 475
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isAdded()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 476
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setSelection(I)V

    goto/16 :goto_3

    .line 482
    .end local v5    # "ft":Landroid/app/FragmentTransaction;
    .end local v6    # "in":Landroid/content/Intent;
    :cond_1a
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v10, :cond_1c

    .line 483
    const/4 v10, 0x1

    sput-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    .line 484
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1b

    .line 485
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 486
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const/high16 v10, 0x7f030000

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 487
    .local v1, "actionbar_v":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v10, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_selectMode(Landroid/view/View;)V

    .line 489
    const v10, 0x7f0e0001

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    .line 490
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setFocusable(Z)V

    .line 491
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 492
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v11, Lcom/sec/android/app/voicenote/main/VNMainActivity$1;

    invoke-direct {v11, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$1;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 498
    const v10, 0x7f0e0002

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    .line 499
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setFocusable(Z)V

    .line 500
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 501
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    new-instance v11, Lcom/sec/android/app/voicenote/main/VNMainActivity$2;

    invoke-direct {v11, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$2;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 510
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setEnableButton(Landroid/widget/Button;Z)V

    .line 512
    .end local v1    # "actionbar_v":Landroid/view/View;
    .end local v7    # "inflater":Landroid/view/LayoutInflater;
    :cond_1b
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "android.provider.MediaStore.extra.MAX_BYTES"

    const-wide/32 v12, 0x9c5e00

    invoke-virtual {v10, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    .line 513
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "mime_type"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    .line 514
    const-string v10, "VoiceNoteMainActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "fromAttachMode mSizeMMS : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-wide v12, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mMimetype : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 515
    :cond_1c
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_4

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v10, :cond_4

    .line 516
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v10

    if-nez v10, :cond_1d

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v10

    if-nez v10, :cond_1d

    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPrepared()Z

    move-result v10

    if-eqz v10, :cond_1e

    .line 517
    :cond_1d
    sget-object v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v11, -0x1

    const/4 v12, -0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    .line 518
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->removeBookmarkList()V

    .line 519
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->resetRepeatMode()V

    .line 520
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->endTrim(Z)V

    .line 521
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v10

    if-eqz v10, :cond_1e

    .line 522
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    .line 525
    :cond_1e
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-virtual {v10, v11}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    .line 527
    .local v4, "fragment":Landroid/app/Fragment;
    if-eqz v4, :cond_4

    instance-of v10, v4, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    if-eqz v10, :cond_4

    .line 528
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 548
    .end local v4    # "fragment":Landroid/app/Fragment;
    .restart local v5    # "ft":Landroid/app/FragmentTransaction;
    :catchall_0
    move-exception v10

    :try_start_3
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v10

    .line 556
    :cond_1f
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-eqz v10, :cond_8

    .line 557
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    goto/16 :goto_4

    .line 565
    :cond_20
    const/4 v10, 0x0

    sput-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    goto/16 :goto_5

    .line 569
    :cond_21
    sget-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isKeepListActivity:Z

    if-nez v10, :cond_9

    .line 570
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 571
    const/4 v10, 0x0

    sput-boolean v10, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isKeepListActivity:Z

    goto/16 :goto_6

    .line 575
    :cond_22
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v10, :cond_24

    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_24

    .line 577
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z

    if-eqz v10, :cond_23

    .line 578
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    goto/16 :goto_6

    .line 581
    :cond_23
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    goto/16 :goto_6

    .line 583
    :cond_24
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v10

    const/16 v11, 0x3ec

    if-eq v10, v11, :cond_25

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v10

    const/16 v11, 0x3eb

    if-ne v10, v11, :cond_9

    .line 585
    :cond_25
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V

    .line 586
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V

    goto/16 :goto_6

    .line 595
    :cond_26
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_main()V

    goto/16 :goto_7

    .line 598
    :cond_27
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->setSearched(Z)V

    goto/16 :goto_7

    .line 600
    :cond_28
    iget v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isShowing()Z

    move-result v10

    if-nez v10, :cond_b

    .line 601
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v10, :cond_29

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v10

    if-nez v10, :cond_29

    const-string v10, "record_mode"

    invoke-static {v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_b

    .line 604
    :cond_29
    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v10, :cond_b

    .line 605
    iget-object v10, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show()V

    goto/16 :goto_7

    .line 619
    :cond_2a
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 620
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 621
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 622
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v5, v10, v11}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 650
    :catch_1
    move-exception v10

    goto/16 :goto_9
.end method

.method public onSTTEditPopup(Z)V
    .locals 3
    .param p1, "isShown"    # Z

    .prologue
    const/4 v2, 0x0

    .line 4443
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4444
    if-eqz p1, :cond_3

    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_3

    .line 4445
    const/16 v0, 0x17

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 4446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsNeedResume:Z

    .line 4447
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4448
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPauseForTrim()V

    .line 4466
    :cond_0
    :goto_0
    return-void

    .line 4450
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPause()V

    goto :goto_0

    .line 4452
    :cond_2
    const/16 v0, 0x18

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 4453
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsNeedResume:Z

    goto :goto_0

    .line 4456
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsNeedResume:Z

    if-eqz v0, :cond_0

    .line 4457
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsNeedResume:Z

    .line 4458
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4459
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doResumeForTrim()V

    goto :goto_0

    .line 4461
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doResume()V

    goto :goto_0
.end method

.method public onSTTStringChanged(ILjava/lang/String;Z)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "newLine"    # Z

    .prologue
    .line 4470
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4471
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateSTTString(ILjava/lang/String;Z)V

    .line 4473
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 807
    const-string v0, "activitymode"

    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 808
    iget v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    iput v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSavedActivityMode:I

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 810
    const-string v0, "searchText"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    const-string v0, "keypad"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 816
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_1

    .line 817
    const-string v0, "searchMode"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 819
    :cond_1
    const-string v0, "is_actionmode"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 820
    const-string v0, "current_check_item_id"

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mCurrentCheckedItemID:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 821
    const-string v0, "filename"

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 823
    return-void
.end method

.method public onSetAsNameDialogItemClick()V
    .locals 0

    .prologue
    .line 1649
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finishCustomActionMode()V

    .line 1650
    return-void
.end method

.method public onSingleSelect(J)V
    .locals 2
    .param p1, "id"    # J

    .prologue
    .line 4794
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDoneButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setEnableButton(Landroid/widget/Button;Z)V

    .line 4795
    return-void
.end method

.method protected onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 712
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onStart"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 715
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 716
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 717
    check-cast v1, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    sget v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateTime(J)V

    .line 719
    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "category_label_color"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateFilename(Ljava/lang/String;I)V

    .line 723
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mAddCategoryChecked:Z

    if-nez v1, :cond_1

    .line 724
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z

    .line 728
    :goto_0
    sput-boolean v4, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mAddCategoryChecked:Z

    .line 730
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->start(Landroid/content/Context;)V

    .line 731
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRegistedIService:Z

    .line 732
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    .line 733
    return-void

    .line 726
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z

    goto :goto_0
.end method

.method protected onStop()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 737
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onStop"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->saveMmsRecode()V

    .line 741
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 742
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 743
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 748
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_2

    sget-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeRecording:Z

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPaused()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPreviousActivity:Landroid/app/Activity;

    if-ne v1, p0, :cond_4

    .line 751
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->showNotification()Z

    .line 753
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_5

    .line 754
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->unregisterCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 756
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_6

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 757
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSaveMmsModeState:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 765
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x835

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x836

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x83d

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 768
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x838

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 769
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x839

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0x83b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 772
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->stop()V

    .line 773
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->unbindFromService(Landroid/content/Context;)V

    .line 774
    iput-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 775
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSizeMMS:J

    .line 776
    iput-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    .line 777
    sput-boolean v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeRecording:Z

    .line 779
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 780
    return-void

    .line 760
    :cond_6
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSaveMmsModeState:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 762
    :catch_0
    move-exception v0

    .line 763
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTrimCompleted(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 4432
    const/16 v0, 0x3d

    if-ne v0, p1, :cond_0

    .line 4433
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPlayTrimNewFile()V

    .line 4434
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPause()V

    .line 4435
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 4439
    :goto_0
    return-void

    .line 4437
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->restartPlay()V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 5
    .param p1, "level"    # I

    .prologue
    const/4 v2, 0x0

    .line 853
    const-string v1, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTrimMemory() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    sparse-switch p1, :sswitch_data_0

    .line 911
    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 912
    return-void

    .line 863
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 866
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 867
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecIconBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;

    .line 868
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mTimeBlinkThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;

    .line 869
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEQThread:Lcom/sec/android/app/voicenote/main/VNMainActivity$EQThread;

    .line 870
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 871
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mMimetype:Ljava/lang/String;

    .line 872
    sput-object v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDvfsHelper:Landroid/os/DVFSHelper;

    .line 873
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 874
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 875
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPendingIntent:Landroid/app/PendingIntent;

    .line 876
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    move-object v1, v2

    .line 877
    check-cast v1, [[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->techListsArray:[[Ljava/lang/String;

    .line 878
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    .line 879
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchLayout:Landroid/view/View;

    .line 880
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;

    .line 881
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    .line 882
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .line 883
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;

    .line 885
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLogoFragment:Lcom/sec/android/app/voicenote/main/fragment/VNLogoFragment;

    .line 886
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    .line 887
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecCtrlBtnFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    .line 888
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecLightFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    .line 889
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRecModeFragment:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .line 890
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .line 891
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 892
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSingleAttachFragment:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .line 895
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCacheLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getBookmarkCache()Landroid/util/LruCache;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/LruCache;->evictAll()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 899
    :goto_1
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onTrimMemory clear bookmark cache"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCacheLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getSTTCache()Landroid/util/LruCache;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/LruCache;->evictAll()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 905
    :goto_2
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onTrimMemory clear STT cache"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 896
    :catch_0
    move-exception v0

    .line 897
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onTrimMemory bookmark cache is empty"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 902
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 903
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "onTrimMemory STT cache is empty"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 854
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
        0x28 -> :sswitch_1
        0x3c -> :sswitch_1
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public onUpdateState(Z)V
    .locals 3
    .param p1, "bRequeryCursor"    # Z

    .prologue
    .line 4262
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_1

    .line 4263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4264
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getIsSearched()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    .line 4268
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 4269
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateControlButtons()V

    .line 4270
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->finishActionMode()V

    .line 4272
    :cond_1
    return-void

    .line 4266
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onUpdateStateNoModeChange(Z)V
    .locals 1
    .param p1, "bRequeryCursor"    # Z

    .prologue
    .line 4276
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_1

    .line 4277
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 4278
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateControlButtons()V

    .line 4280
    :cond_1
    return-void
.end method

.method public onVoiceMemoAgree(I)V
    .locals 3
    .param p1, "button"    # I

    .prologue
    .line 3794
    iget v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I

    if-nez v1, :cond_0

    .line 3795
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "record_mode"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 3798
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->hide()V

    .line 3803
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 3804
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 3805
    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawPanelText(Landroid/app/FragmentTransaction;)V

    .line 3806
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3807
    return-void

    .line 3800
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_main()V

    goto :goto_0
.end method

.method public onWarningDialogResult(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 4289
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->startTrim()V

    .line 4290
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 5224
    const v1, 0x7f0e00ca

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 5225
    .local v0, "stopbutton":Landroid/view/View;
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5226
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 5228
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 5229
    return-void
.end method

.method public preparePopupMenu(Landroid/view/Menu;)Z
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 3975
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountInLibrary(Landroid/content/Context;)I

    move-result v3

    .line 3976
    .local v3, "fileCountinLibrary":I
    const-string v7, "category_text"

    const-wide/16 v8, -0x1

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v8

    long-to-int v7, v8

    invoke-static {p0, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountByLabel(Landroid/content/Context;I)I

    move-result v2

    .line 3978
    .local v2, "fileCountinByLabel":I
    sget-object v8, Lcom/sec/android/app/voicenote/main/VNMainActivity;->ANIM_SYNC_KEY:Ljava/lang/Object;

    monitor-enter v8

    .line 3979
    :try_start_0
    iget-boolean v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAnimating:Z

    if-eqz v7, :cond_0

    .line 3980
    const/4 v7, 0x0

    monitor-exit v8

    .line 4148
    :goto_0
    return v7

    .line 3982
    :cond_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3984
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "VNLibraryExpandableListFragment"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    iput-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 3986
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 3987
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 3989
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 3990
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v7

    const v8, 0x7f0d0009

    invoke-virtual {v7, v8, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 3992
    if-eqz v3, :cond_2

    if-nez v2, :cond_3

    .line 3993
    :cond_2
    const v7, 0x7f0e010c

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 3994
    const v7, 0x7f0e0112

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 3996
    :cond_3
    const v7, 0x7f0e010d

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 3997
    const v7, 0x7f0e0119

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 3998
    const v7, 0x7f0e010f

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 3999
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4000
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4001
    const v7, 0x7f0e010e

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4003
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v7

    goto :goto_0

    .line 3982
    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 4004
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v7

    if-nez v7, :cond_c

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_c

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    if-nez v7, :cond_5

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v7

    if-nez v7, :cond_5

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPrepared()Z

    move-result v7

    if-eqz v7, :cond_c

    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v7

    if-nez v7, :cond_c

    .line 4007
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v7

    const v8, 0x7f0d000a

    invoke-virtual {v7, v8, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 4008
    const v7, 0x7f0e0116

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4009
    const v7, 0x7f0e0117

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4010
    const v7, 0x7f0e0118

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4012
    const v7, 0x7f0e0118

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4014
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_b

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 4015
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v6

    .line 4016
    .local v6, "version":Ljava/lang/String;
    const-string v7, "2.0"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 4017
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v0

    .line 4018
    .local v0, "callingUserId":I
    if-gez v0, :cond_8

    .line 4019
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4020
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4035
    .end local v0    # "callingUserId":I
    .end local v6    # "version":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 4036
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_6

    .line 4037
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v4

    .line 4038
    .local v4, "id":J
    const/4 v7, 0x1

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getTypeOfSelectedItem(Landroid/content/Context;J)I

    move-result v8

    if-ne v7, v8, :cond_6

    .line 4040
    const v7, 0x7f0e0114

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4043
    .end local v4    # "id":J
    :cond_6
    sget-boolean v7, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_WRITETONFCTAG:Z

    if-nez v7, :cond_7

    .line 4045
    const v7, 0x7f0e0119

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4047
    :cond_7
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v7

    goto/16 :goto_0

    .line 4021
    .end local v1    # "context":Landroid/content/Context;
    .restart local v0    # "callingUserId":I
    .restart local v6    # "version":Ljava/lang/String;
    :cond_8
    if-nez v0, :cond_9

    .line 4023
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1

    .line 4026
    :cond_9
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1

    .line 4029
    .end local v0    # "callingUserId":I
    :cond_a
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1

    .line 4032
    .end local v6    # "version":Ljava/lang/String;
    :cond_b
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4033
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1

    .line 4048
    :cond_c
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_15

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    if-nez v7, :cond_d

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v7

    if-nez v7, :cond_d

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPrepared()Z

    move-result v7

    if-eqz v7, :cond_15

    .line 4050
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v7

    const v8, 0x7f0d000a

    invoke-virtual {v7, v8, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 4051
    const v7, 0x7f0e0115

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4052
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_13

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v7

    if-eqz v7, :cond_13

    .line 4053
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v6

    .line 4054
    .restart local v6    # "version":Ljava/lang/String;
    const-string v7, "2.0"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 4055
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v0

    .line 4056
    .restart local v0    # "callingUserId":I
    if-gez v0, :cond_10

    .line 4057
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4058
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4074
    .end local v0    # "callingUserId":I
    .end local v6    # "version":Ljava/lang/String;
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isBookmarkList()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 4075
    const v7, 0x7f0e0112

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4076
    const v7, 0x7f0e010c

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4077
    const v7, 0x7f0e0113

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4078
    const v7, 0x7f0e011a

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4079
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4080
    const v7, 0x7f0e0119

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4081
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4082
    const v7, 0x7f0e0114

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4088
    :goto_3
    const v7, 0x7f0e0118

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4091
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 4092
    .restart local v1    # "context":Landroid/content/Context;
    if-eqz v1, :cond_e

    .line 4093
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v4

    .line 4094
    .restart local v4    # "id":J
    const/4 v7, 0x1

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getTypeOfSelectedItem(Landroid/content/Context;J)I

    move-result v8

    if-ne v7, v8, :cond_e

    .line 4096
    const v7, 0x7f0e0114

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4099
    .end local v4    # "id":J
    :cond_e
    sget-boolean v7, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_WRITETONFCTAG:Z

    if-nez v7, :cond_f

    .line 4101
    const v7, 0x7f0e0119

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4103
    :cond_f
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v7

    goto/16 :goto_0

    .line 4059
    .end local v1    # "context":Landroid/content/Context;
    .restart local v0    # "callingUserId":I
    .restart local v6    # "version":Ljava/lang/String;
    :cond_10
    if-nez v0, :cond_11

    .line 4061
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_2

    .line 4064
    :cond_11
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4065
    const v7, 0x7f0e0114

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_2

    .line 4068
    .end local v0    # "callingUserId":I
    :cond_12
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_2

    .line 4071
    .end local v6    # "version":Ljava/lang/String;
    :cond_13
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4072
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_2

    .line 4084
    :cond_14
    const v7, 0x7f0e0116

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4085
    const v7, 0x7f0e0117

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_3

    .line 4105
    :cond_15
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v7

    const v8, 0x7f0d0009

    invoke-virtual {v7, v8, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 4107
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_1e

    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v7

    if-eqz v7, :cond_1e

    .line 4108
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v6

    .line 4109
    .restart local v6    # "version":Ljava/lang/String;
    const-string v7, "2.0"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1d

    .line 4110
    sget-object v7, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v0

    .line 4111
    .restart local v0    # "callingUserId":I
    if-gez v0, :cond_1b

    .line 4112
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4113
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4129
    .end local v0    # "callingUserId":I
    .end local v6    # "version":Ljava/lang/String;
    :cond_16
    :goto_4
    if-eqz v3, :cond_17

    if-nez v2, :cond_18

    .line 4130
    :cond_17
    const v7, 0x7f0e010c

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4131
    const v7, 0x7f0e0112

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4132
    const v7, 0x7f0e0119

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4133
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4134
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4135
    const v7, 0x7f0e0114

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4136
    const v7, 0x7f0e010d

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4139
    :cond_18
    const v7, 0x7f0e0118

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4141
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v7

    if-eqz v7, :cond_19

    .line 4142
    const v7, 0x7f0e010b

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4144
    :cond_19
    sget-boolean v7, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_WRITETONFCTAG:Z

    if-nez v7, :cond_1a

    .line 4146
    const v7, 0x7f0e0119

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4148
    :cond_1a
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 4114
    .restart local v0    # "callingUserId":I
    .restart local v6    # "version":Ljava/lang/String;
    :cond_1b
    if-nez v0, :cond_1c

    .line 4116
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_4

    .line 4119
    :cond_1c
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_4

    .line 4121
    .end local v0    # "callingUserId":I
    :cond_1d
    const-string v7, "1.0"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 4122
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_4

    .line 4125
    .end local v6    # "version":Ljava/lang/String;
    :cond_1e
    const v7, 0x7f0e0110

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 4126
    const v7, 0x7f0e0111

    invoke-interface {p1, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_4
.end method

.method public refreashAnim(Z)V
    .locals 1
    .param p1, "needAnim"    # Z

    .prologue
    .line 4284
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->refreashAnim(Z)V

    .line 4285
    return-void
.end method

.method public registerBroadcastReceiverSip(Z)V
    .locals 2
    .param p1, "register"    # Z

    .prologue
    .line 4183
    if-eqz p1, :cond_2

    .line 4184
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 4208
    :cond_0
    :goto_0
    return-void

    .line 4188
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    .line 4198
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 4199
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4201
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 4203
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 4204
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 4205
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method public removeToolbarHelp()V
    .locals 2

    .prologue
    .line 5212
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 5213
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 5214
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMediaButtonProcessListener()V

    .line 5216
    :cond_0
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "Remove Toolbar Tooltip"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 5217
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->windowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 5218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->toolbarHelp:Landroid/view/View;

    .line 5220
    :cond_1
    return-void
.end method

.method public selectAddCategory()V
    .locals 1

    .prologue
    .line 1702
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEnterAddCategory:Z

    .line 1703
    return-void
.end method

.method public setActionMode(Landroid/view/ActionMode;)V
    .locals 1
    .param p1, "actionMode"    # Landroid/view/ActionMode;

    .prologue
    .line 4590
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    .line 4591
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setActionMode(Landroid/view/ActionMode;)V

    .line 4593
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isActionMode:Z

    .line 4594
    return-void

    .line 4593
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showLibraryActionbar()V
    .locals 3

    .prologue
    .line 4477
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_0

    .line 4478
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->actionbar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_library(Ljava/lang/String;Z)V

    .line 4480
    :cond_0
    return-void
.end method

.method public startTrim()V
    .locals 3

    .prologue
    .line 5248
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isRepeatMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5249
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->startRepeatModeTrim()V

    .line 5258
    :goto_0
    return-void

    .line 5251
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v0

    .line 5252
    .local v0, "repeatTime":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_1

    .line 5253
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->resetRepeatMode()V

    .line 5255
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBookmarks:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setBookmark(Ljava/util/ArrayList;)V

    .line 5256
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->startTrim()V

    goto :goto_0
.end method

.method public stop_HidePlayer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4353
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isPlayerActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4354
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 4355
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 4362
    :cond_0
    :goto_0
    return-void

    .line 4357
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4358
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 4359
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    .line 4360
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    goto :goto_0
.end method
