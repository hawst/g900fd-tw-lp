.class Lcom/sec/android/app/voicenote/common/util/VNIntent$4;
.super Landroid/content/BroadcastReceiver;
.source "VNIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverGPS(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$4;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v2, 0x10d6

    .line 275
    const-string v0, "VNIntent"

    const-string v1, "GPS changed Intent Received"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$4;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$4;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$4;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 280
    :cond_0
    return-void
.end method
