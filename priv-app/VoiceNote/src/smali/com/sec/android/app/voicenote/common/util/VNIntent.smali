.class public Lcom/sec/android/app/voicenote/common/util/VNIntent;
.super Ljava/lang/Object;
.source "VNIntent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;,
        Lcom/sec/android/app/voicenote/common/util/VNIntent$MultiShareMode;,
        Lcom/sec/android/app/voicenote/common/util/VNIntent$MultiSelectMode;
    }
.end annotation


# static fields
.field public static final ACTION_EASY_MODE_CHANGED:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static final ACTION_STATUSBAR_STATE_CLOSE:Ljava/lang/String; = "com.android.systemui.statusbar.COLLAPSED"

.field public static final ACTION_STATUSBAR_STATE_OPEN:Ljava/lang/String; = "com.android.systemui.statusbar.ANIMATING"

.field public static final ACTION_VOICENOTE_EASY_MODE_CHANGED:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE_VOICENOTE"

.field public static final BACKGROUND_VOICENOTE_ACCEPT_CALL:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_accept_call"

.field public static final BACKGROUND_VOICENOTE_CANCEL:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_cancel"

.field public static final BACKGROUND_VOICENOTE_CANCEL_KEYGARD:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_cancel.keygard"

.field public static final BACKGROUND_VOICENOTE_PLAY:Ljava/lang/String; = "com.sec.android.app.voicenote.play"

.field public static final BACKGROUND_VOICENOTE_PLAY_PAUSE:Ljava/lang/String; = "com.sec.android.app.voicenote.play_pause"

.field public static final BACKGROUND_VOICENOTE_PLAY_STOP:Ljava/lang/String; = "com.sec.android.app.voicenote.play_stop"

.field public static final BACKGROUND_VOICENOTE_REC_NEW:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_new"

.field public static final BACKGROUND_VOICENOTE_REC_PAUSE:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_pause"

.field public static final BACKGROUND_VOICENOTE_REC_RESUME:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_reume"

.field public static final BACKGROUND_VOICENOTE_RE_ENTER:Ljava/lang/String; = "com.sec.android.app.voicenote.vrremoteview"

.field public static final BACKGROUND_VOICENOTE_SAVE:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_save"

.field public static final BACKGROUND_VOICENOTE_SAVE_KEYGARD:Ljava/lang/String; = "com.sec.android.app.voicenote.rec_save_keygard"

.field public static final BACKGROUND_VOICENOTE_STANDBY:Ljava/lang/String; = "com.sec.android.app.voicenote.standby"

.field public static final COMMAND:Ljava/lang/String; = "command"

.field public static final COMMAND_PAUSE:Ljava/lang/String; = "pause"

.field public static final COMMAND_STOP:Ljava/lang/String; = "stop"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final GPS_SETTINGS_CHANGED:Ljava/lang/String; = "android.settings.GPS_CHANGED"

.field public static final INTENT_FILE_RELAY_REQUEST:Ljava/lang/String; = "com.sec.knox.container.FileRelayRequest"

.field public static final ISKEYDOWN:Ljava/lang/String; = "iskeydown"

.field public static final KEY_REC_TIME:Ljava/lang/String; = "voicenote_rec_time"

.field public static final KEY_SEND_TIME:Ljava/lang/String; = "voicenote_send_time"

.field public static final KNOX_MESSAGE:Ljava/lang/String; = "KNOX_MESSAGE"

.field public static final KNOX_MODE_USER_SWITCH:Ljava/lang/String; = "com.sec.knox.container.INTENT_KNOX_USER_CHANGED"

.field public static final KNOX_RECIEVED:Ljava/lang/String; = "com.android.voicenote.KNOX_RECIEVED"

.field public static final MEDIA_BUTTON:Ljava/lang/String; = "button"

.field public static final MULTISELECTMODE:Ljava/lang/String; = "multiselectmode"

.field public static final MULTISHAREMODE:Ljava/lang/String; = "multisharemode"

.field public static final MUSIC_SERVICE_COMMAND:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field public static final Media_Button_Recieved:Ljava/lang/String; = "Media_Button_Recieved"

.field public static final PACKAGE_VOICENOTE:Ljava/lang/String; = "com.sec.android.app.voicenote"

.field public static final PACKAGE_VOICERECORDER:Ljava/lang/String; = "com.sec.android.app.voicerecorder"

.field public static final REPEAT_COUNT:Ljava/lang/String; = "repeat"

.field public static final SENDSTATE4TIKER_INIT:Ljava/lang/String; = "com.sec.android.app.voicenote.ticker.init"

.field public static final SENDSTATE4TIKER_PAUSE:Ljava/lang/String; = "com.sec.android.app.voicenote.ticker.pause"

.field public static final SENDSTATE4TIKER_REC:Ljava/lang/String; = "com.sec.android.app.voicenote.ticker.rec"

.field public static final SERVICE_BROADCAST_STOP_FROM_ALARM:Ljava/lang/String; = "com.sec.android.app.voicecommand"

.field private static final TAG:Ljava/lang/String; = "VNIntent"

.field public static final TARGET_TICKER:I = 0x0

.field public static final VOICERECORDER_LIST_REC:Ljava/lang/String; = "com.sec.android.app.voicenotelist.rec"

.field public static final VOICE_INTENT_ACTIVITY_DESTROYED:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_ACTIVITY_DESTROYED"

.field public static final VOICE_INTENT_FILE_DELETE:Ljava/lang/String; = "com.android.voicernote.VOICE_INTENT_FILE_DELETE"

.field public static final VOICE_INTENT_FINISH:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_FINISH"

.field public static final VOICE_INTENT_NEXTBUTTON:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_NEXT"

.field public static final VOICE_INTENT_NEXTLONGBUTTON:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_NEXTLONG"

.field public static final VOICE_INTENT_PLAY_QUICKPANEL:Ljava/lang/String; = "android.intent.action.PLAY_QUICKPANEL"

.field public static final VOICE_INTENT_PREVBUTTON:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_PREV"

.field public static final VOICE_INTENT_PREVLONGBUTTON:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_PREVLONG"

.field public static final VOICE_INTENT_REFRESH:Ljava/lang/String; = "com.android.voicenote.VOICE_INTENT_REFRESH"

.field private static isHeadsetConnected:Z

.field private static isMicrophoneHeadsetConnected:Z


# instance fields
.field private mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverKNOXUserSwitched:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

.field private mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

.field private mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

.field private final mContext:Landroid/content/Context;

.field private mEventHandler:Landroid/os/Handler;

.field private mRefreshRecevier:Landroid/content/BroadcastReceiver;

.field private mTm:Landroid/telephony/TelephonyManager;

.field private mTm2:Landroid/telephony/TelephonyManager;

.field private mVNController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z

    .line 51
    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isMicrophoneHeadsetConnected:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventHandler"    # Landroid/os/Handler;

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mVNController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .line 133
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    .line 134
    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;

    .line 135
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mVNController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .line 136
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 43
    sput-boolean p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z

    return p0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mVNController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    return-object v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 43
    sput-boolean p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isMicrophoneHeadsetConnected:Z

    return p0
.end method

.method public static is4PinHeadsetConnected()Z
    .locals 1

    .prologue
    .line 956
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isMicrophoneHeadsetConnected:Z

    return v0
.end method

.method public static isHeadsetConnected()Z
    .locals 1

    .prologue
    .line 952
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected:Z

    return v0
.end method

.method private registerBroadcastReceiverAlarm(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 397
    if-eqz p1, :cond_2

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 419
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$8;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

    .line 410
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 411
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.voicecommand"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 414
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 416
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverAlarm:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverCoverOnOff(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 675
    if-eqz p1, :cond_2

    .line 676
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$16;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

    .line 693
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 694
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 696
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 698
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 699
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 700
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverCoverOnOff:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverEmergencyStateChanged(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 342
    if-eqz p1, :cond_2

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$6;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

    .line 359
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 360
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 364
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 366
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverEmergencyState:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverFileDelete(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 241
    if-eqz p1, :cond_2

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$3;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

    .line 256
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 257
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.voicernote.VOICE_INTENT_FILE_DELETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 260
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 262
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFileDelete:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverFinish(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 214
    if-eqz p1, :cond_2

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$2;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

    .line 229
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 230
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.voicenote.VOICE_INTENT_FINISH"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 233
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 235
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverFinish:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverGPS(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 268
    if-eqz p1, :cond_2

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$4;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

    .line 283
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 284
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.settings.GPS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 287
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 289
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverGPS:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverHomeKey(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 925
    if-eqz p1, :cond_2

    .line 926
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 949
    :cond_0
    :goto_0
    return-void

    .line 929
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$20;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

    .line 936
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 937
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 938
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 939
    const-string v1, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 941
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 944
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 945
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 946
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHomeKey:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverListActivity(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 482
    if-eqz p1, :cond_2

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$11;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

    .line 496
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 497
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.voicenotelist.rec"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 501
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 502
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 503
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverRecFromList:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverLocale(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 422
    if-eqz p1, :cond_2

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$9;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

    .line 436
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 437
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 438
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 441
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 442
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 443
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverLocale:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverLowBattery(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 185
    if-eqz p1, :cond_2

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$1;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

    .line 202
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 203
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 206
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 208
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverBattery:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverMusicCommand(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 449
    if-eqz p1, :cond_2

    .line 450
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$10;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

    .line 470
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 471
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.musicservicecommand"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 474
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 475
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 476
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverMusicCommand:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverNotification(Z)V
    .locals 5
    .param p1, "register"    # Z

    .prologue
    const/4 v4, 0x0

    .line 509
    if-eqz p1, :cond_2

    .line 510
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$12;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

    .line 523
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 524
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.voicenote.rec_pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 525
    const-string v1, "com.sec.android.app.voicenote.rec_reume"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 526
    const-string v1, "com.sec.android.app.voicenote.rec_save"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 527
    const-string v1, "com.sec.android.app.voicenote.rec_save_keygard"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 528
    const-string v1, "com.sec.android.app.voicenote.play"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 529
    const-string v1, "com.sec.android.app.voicenote.play_pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 530
    const-string v1, "com.sec.android.app.voicenote.play_stop"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 531
    const-string v1, "com.sec.android.app.voicenote.rec_new"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 532
    const-string v1, "com.sec.android.app.voicenote.standby"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 533
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 534
    const-string v1, "com.sec.android.app.voicenote.rec_cancel"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 535
    const-string v1, "com.sec.android.app.voicenote.rec_cancel.keygard"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 536
    const-string v1, "com.sec.android.app.voicenote.rec_accept_call"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 538
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

    const-string v3, "com.sec.android.app.voicenote.Controller"

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    goto :goto_0

    .line 540
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 542
    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverNotification:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverPalmTouch(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 614
    if-eqz p1, :cond_2

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$14;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

    .line 631
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 632
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PALM_DOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 635
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 636
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 637
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverPalmTouch:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverSDCard(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 548
    if-eqz p1, :cond_1

    .line 549
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 550
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$13;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    .line 595
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 596
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 597
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 598
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 599
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 600
    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 601
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 602
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 611
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 607
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 608
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverScreenOnOff(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 643
    if-eqz p1, :cond_2

    .line 644
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 672
    :cond_0
    :goto_0
    return-void

    .line 648
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$15;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

    .line 660
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 661
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 662
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 663
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 665
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 667
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 668
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 669
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverScreenOnOff:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverStatusBarState(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 315
    if-eqz p1, :cond_2

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$5;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

    .line 328
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 329
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.systemui.statusbar.ANIMATING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 330
    const-string v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 334
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 336
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverStatusBarState:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerBroadcastReceiverVolume(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 372
    if-eqz p1, :cond_2

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$7;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

    .line 384
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 385
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 389
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 391
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverVolume:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private registerTelephonyListener(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    const/16 v3, 0x20

    const/4 v2, 0x0

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm:Landroid/telephony/TelephonyManager;

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    if-nez v0, :cond_0

    .line 712
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;Lcom/sec/android/app/voicenote/common/util/VNIntent$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    .line 713
    :cond_0
    if-eqz p1, :cond_2

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    invoke-virtual {v0, v1, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 718
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    const-string v1, "phone2"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm2:Landroid/telephony/TelephonyManager;

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm2:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    .line 720
    if-eqz p1, :cond_3

    .line 721
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm2:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    invoke-virtual {v0, v1, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 725
    :cond_1
    :goto_1
    return-void

    .line 716
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_0

    .line 723
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm2:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mCallListener:Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_1
.end method

.method public static sendKNOXMessageToDialog(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # I

    .prologue
    .line 977
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.voicenote.KNOX_RECIEVED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 978
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "KNOX_MESSAGE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 979
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 980
    return-void
.end method

.method public static sendMediaButtonReceived(Landroid/content/Context;IIZ)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "button"    # I
    .param p2, "repeatCount"    # I
    .param p3, "isKeyDown"    # Z

    .prologue
    .line 967
    new-instance v0, Landroid/content/Intent;

    const-string v1, "Media_Button_Recieved"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 969
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "button"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 970
    const-string v1, "repeat"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 971
    const-string v1, "iskeydown"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 972
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 973
    return-void
.end method

.method public static sendState(Landroid/content/Context;III)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recorder_state"    # I
    .param p2, "rec_time"    # I
    .param p3, "target"    # I

    .prologue
    .line 983
    packed-switch p3, :pswitch_data_0

    .line 990
    :goto_0
    return-void

    .line 985
    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState4Ticker(Landroid/content/Context;II)V

    goto :goto_0

    .line 983
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static sendState4Ticker(Landroid/content/Context;II)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recorder_state"    # I
    .param p2, "rec_time"    # I

    .prologue
    .line 993
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 994
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 995
    .local v2, "sendTime":J
    const-string v1, "voicenote_send_time"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 997
    packed-switch p1, :pswitch_data_0

    .line 1007
    const-string v1, "com.sec.android.app.voicenote.ticker.init"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1008
    const-string v1, "voicenote_rec_time"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1011
    :goto_0
    const-string v1, "VNIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendState4Ticker("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1013
    return-void

    .line 999
    :pswitch_0
    const-string v1, "com.sec.android.app.voicenote.ticker.pause"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1000
    const-string v1, "voicenote_rec_time"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1003
    :pswitch_1
    const-string v1, "com.sec.android.app.voicenote.ticker.rec"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1004
    const-string v1, "voicenote_rec_time"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 997
    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public registerBroadcastReceiverDestroy(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 830
    if-eqz p1, :cond_2

    .line 831
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 835
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$18;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

    .line 844
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 845
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.voicenote.VOICE_INTENT_ACTIVITY_DESTROYED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 847
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 849
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 850
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 851
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverDestroy:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method public registerBroadcastReceiverHeadSet(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 759
    if-eqz p1, :cond_2

    .line 760
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 763
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$17;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

    .line 817
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 818
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 820
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 822
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 823
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 824
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mBroadcastReceiverHeadset:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method public registerBroadcastReceiverToActivity(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 857
    if-eqz p1, :cond_2

    .line 858
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mRefreshRecevier:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 861
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNIntent$19;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mRefreshRecevier:Landroid/content/BroadcastReceiver;

    .line 883
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 884
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 885
    const-string v1, "com.android.voicenote.VOICE_INTENT_NEXT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 886
    const-string v1, "com.android.voicenote.VOICE_INTENT_PREV"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 888
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mRefreshRecevier:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 890
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mRefreshRecevier:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mRefreshRecevier:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 892
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mRefreshRecevier:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method public registerBroadcastReceiversForActivity(Z)V
    .locals 0
    .param p1, "register"    # Z

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverSDCard(Z)V

    .line 163
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverLocale(Z)V

    .line 164
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverLowBattery(Z)V

    .line 165
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverFinish(Z)V

    .line 166
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverVolume(Z)V

    .line 167
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverFileDelete(Z)V

    .line 168
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverGPS(Z)V

    .line 169
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverHeadSet(Z)V

    .line 170
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverToActivity(Z)V

    .line 171
    return-void
.end method

.method public registerBroadcastReceiversForService(Z)V
    .locals 0
    .param p1, "register"    # Z

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverAlarm(Z)V

    .line 296
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverLocale(Z)V

    .line 297
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverMusicCommand(Z)V

    .line 298
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverNotification(Z)V

    .line 299
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverSDCard(Z)V

    .line 300
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverPalmTouch(Z)V

    .line 301
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerTelephonyListener(Z)V

    .line 302
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverListActivity(Z)V

    .line 303
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverScreenOnOff(Z)V

    .line 304
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverCoverOnOff(Z)V

    .line 305
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverLowBattery(Z)V

    .line 306
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverDestroy(Z)V

    .line 307
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverEmergencyStateChanged(Z)V

    .line 309
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverStatusBarState(Z)V

    .line 310
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverHeadSet(Z)V

    .line 311
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverHomeKey(Z)V

    .line 312
    return-void
.end method

.method public registerBroadcastReceiversForSettingActivity(Z)V
    .locals 0
    .param p1, "register"    # Z

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverSDCard(Z)V

    .line 175
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverLocale(Z)V

    .line 176
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverLowBattery(Z)V

    .line 177
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverFinish(Z)V

    .line 178
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverVolume(Z)V

    .line 179
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverFileDelete(Z)V

    .line 180
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverGPS(Z)V

    .line 181
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverToActivity(Z)V

    .line 182
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 960
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm:Landroid/telephony/TelephonyManager;

    .line 961
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mTm2:Landroid/telephony/TelephonyManager;

    .line 962
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;

    .line 963
    return-void
.end method
