.class Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;
.super Ljava/lang/Thread;
.source "VNNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoteViewUpdateThread"
.end annotation


# instance fields
.field private isStarted:Z

.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)V
    .locals 1

    .prologue
    .line 398
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->isStarted:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$1;

    .prologue
    .line 398
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;-><init>(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)V

    return-void
.end method


# virtual methods
.method public interrupt()V
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->isStarted:Z

    .line 410
    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    .line 411
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 415
    const/16 v0, 0x190

    .line 416
    .local v0, "defaultSleepTime":I
    const/16 v1, 0x32

    .line 418
    .local v1, "defaultTimeGap":I
    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->isStarted:Z

    if-eqz v4, :cond_6

    .line 419
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mIsRepeatBackgroundMode:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->access$100(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->access$200(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 420
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->access$200(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPositionForRepeat()I

    .line 425
    :goto_1
    add-int v2, v0, v1

    .line 426
    .local v2, "remainTime":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->access$200(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 427
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->access$200(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentRemainTimeForReapeat()I

    move-result v2

    .line 429
    :cond_1
    const/16 v4, 0x1c2

    if-le v2, v4, :cond_4

    .line 430
    move v3, v0

    .line 437
    .local v3, "sleeptime":I
    :goto_2
    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    .line 438
    move v3, v0

    .line 441
    :cond_2
    if-lez v3, :cond_0

    .line 442
    int-to-long v4, v3

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 422
    .end local v2    # "remainTime":I
    .end local v3    # "sleeptime":I
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->this$0:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->updateNotification()V

    goto :goto_1

    .line 431
    .restart local v2    # "remainTime":I
    :cond_4
    sub-int v4, v2, v1

    if-lez v4, :cond_5

    .line 432
    sub-int v3, v2, v1

    .restart local v3    # "sleeptime":I
    goto :goto_2

    .line 434
    .end local v3    # "sleeptime":I
    :cond_5
    const/4 v3, 0x0

    .restart local v3    # "sleeptime":I
    goto :goto_2

    .line 445
    .end local v2    # "remainTime":I
    .end local v3    # "sleeptime":I
    :cond_6
    return-void
.end method

.method public declared-synchronized start()V
    .locals 1

    .prologue
    .line 403
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->isStarted:Z

    .line 404
    invoke-super {p0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    monitor-exit p0

    return-void

    .line 403
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
