.class Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;
.super Ljava/lang/Thread;
.source "SamsungSTT.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BufferReadThread"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 358
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;

    .prologue
    .line 358
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/16 v13, 0xa

    .line 361
    const/4 v3, 0x0

    .line 362
    .local v3, "error_count":I
    const/4 v8, 0x0

    .line 363
    .local v8, "stream_nullcount":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;->isInterrupted()Z

    move-result v10

    if-nez v10, :cond_4

    .line 366
    const/4 v6, 0x0

    .line 367
    .local v6, "lenRead":I
    const/16 v9, 0x780

    .line 368
    .local v9, "toRead":I
    :try_start_0
    new-array v0, v9, [S

    .line 369
    .local v0, "buf":[S
    const/16 v10, 0xf00

    new-array v1, v10, [B

    .line 370
    .local v1, "buffer":[B
    const/4 v7, 0x0

    .line 372
    .local v7, "shortsRead":I
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 373
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    const/16 v11, 0xf00

    invoke-virtual {v10, v1, v6, v11}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    .line 374
    const/4 v8, 0x0

    .line 383
    :cond_0
    const/4 v3, 0x0

    .line 384
    array-length v10, v1

    if-ge v7, v10, :cond_7

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$900()I

    move-result v10

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v11

    if-eq v10, v11, :cond_7

    .line 385
    const-string v10, "SamsungSTT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "BufferReadThread : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", mReadFilecnt : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 388
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/BufferedInputStream;->close()V

    .line 389
    const/4 v10, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1102(Ljava/io/BufferedInputStream;)Ljava/io/BufferedInputStream;

    .line 391
    :cond_1
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1300()Ljava/io/FileInputStream;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 392
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1300()Ljava/io/FileInputStream;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 393
    const/4 v10, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1302(Ljava/io/FileInputStream;)Ljava/io/FileInputStream;

    .line 395
    :cond_2
    if-gez v7, :cond_3

    .line 396
    const/4 v7, 0x0

    .line 398
    :cond_3
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->deleteFilepath(I)V
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1400(I)V

    .line 400
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$900()I

    move-result v10

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getFilepath(I)Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1500(I)Ljava/lang/String;

    move-result-object v4

    .line 401
    .local v4, "filepath":Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_6

    .line 402
    const-wide/16 v10, 0x78

    invoke-static {v10, v11}, Landroid/os/SystemClock;->sleep(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 431
    .end local v0    # "buf":[S
    .end local v1    # "buffer":[B
    .end local v4    # "filepath":Ljava/lang/String;
    .end local v7    # "shortsRead":I
    :catch_0
    move-exception v2

    .line 432
    .local v2, "e":Ljava/io/IOException;
    const-string v10, "SamsungSTT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "readRunnable error : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    add-int/lit8 v3, v3, 0x1

    .line 434
    if-le v3, v13, :cond_b

    .line 440
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "lenRead":I
    .end local v9    # "toRead":I
    :cond_4
    :goto_1
    return-void

    .line 376
    .restart local v0    # "buf":[S
    .restart local v1    # "buffer":[B
    .restart local v6    # "lenRead":I
    .restart local v7    # "shortsRead":I
    .restart local v9    # "toRead":I
    :cond_5
    :try_start_1
    const-string v10, "SamsungSTT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mBIStream is null, mReadFilecnt: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "stream_nullcount: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    add-int/lit8 v8, v8, 0x1

    .line 378
    if-le v8, v13, :cond_0

    .line 379
    const-string v10, "SamsungSTT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mBIStream is null, mReadFilecnt: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 406
    .restart local v4    # "filepath":Ljava/lang/String;
    :cond_6
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$900()I

    move-result v10

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1202(I)I

    .line 407
    new-instance v10, Ljava/io/FileInputStream;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v11

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getFilepath(I)Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1500(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1302(Ljava/io/FileInputStream;)Ljava/io/FileInputStream;

    .line 408
    new-instance v10, Ljava/io/BufferedInputStream;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1300()Ljava/io/FileInputStream;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {v10}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1102(Ljava/io/BufferedInputStream;)Ljava/io/BufferedInputStream;

    .line 410
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    array-length v11, v1

    sub-int/2addr v11, v7

    invoke-virtual {v10, v1, v7, v11}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    .line 411
    const-string v10, "SamsungSTT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "BufferReadThread : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", mReadFilecnt : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1200()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    .end local v4    # "filepath":Ljava/lang/String;
    :cond_7
    if-gez v7, :cond_8

    .line 414
    const-string v10, "SamsungSTT"

    const-string v11, "BufferReadThread : shortsRead < 0"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-wide/16 v10, 0x78

    invoke-static {v10, v11}, Landroid/os/SystemClock;->sleep(J)V

    goto/16 :goto_0

    .line 419
    :cond_8
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    add-int/lit8 v10, v7, -0x1

    if-ge v5, v10, :cond_9

    .line 420
    div-int/lit8 v10, v5, 0x2

    add-int/lit8 v11, v5, 0x1

    aget-byte v11, v1, v11

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x8

    aget-byte v12, v1, v5

    and-int/lit16 v12, v12, 0xff

    or-int/2addr v11, v12

    int-to-short v11, v11

    aput-short v11, v0, v10

    .line 419
    add-int/lit8 v5, v5, 0x2

    goto :goto_2

    .line 423
    :cond_9
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->addAudioBuffer([S)V

    .line 425
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    if-eqz v10, :cond_a

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/BufferedInputStream;->available()I

    move-result v10

    array-length v11, v1

    mul-int/lit8 v11, v11, 0x2

    if-le v10, v11, :cond_a

    .line 426
    const-string v10, "SamsungSTT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "BIStream available size is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$1100()Ljava/io/BufferedInputStream;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/BufferedInputStream;->available()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ". Reduce READ_INTERVAL"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const-wide/16 v10, 0x3c

    invoke-static {v10, v11}, Landroid/os/SystemClock;->sleep(J)V

    goto/16 :goto_0

    .line 429
    :cond_a
    const-wide/16 v10, 0x78

    invoke-static {v10, v11}, Landroid/os/SystemClock;->sleep(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 437
    .end local v0    # "buf":[S
    .end local v1    # "buffer":[B
    .end local v5    # "i":I
    .end local v7    # "shortsRead":I
    .restart local v2    # "e":Ljava/io/IOException;
    :cond_b
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method
