.class public Lcom/sec/android/app/voicenote/common/util/Bookmark;
.super Ljava/lang/Object;
.source "Bookmark.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
        ">;"
    }
.end annotation


# instance fields
.field private description:Ljava/lang/String;

.field private elapsed:I

.field private isNamed:Z

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "time"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "isNamed"    # Z

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    .line 60
    iput p1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    .line 61
    iput-object p3, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    .line 62
    iput-boolean p4, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V
    .locals 1
    .param p1, "model"    # Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iget-object v0, p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    .line 67
    iget v0, p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    .line 68
    iget-object v0, p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    .line 69
    iget-boolean v0, p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    .line 70
    return-void
.end method

.method private timeToStr(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 113
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 114
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 115
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 116
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 118
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method public IsDuplicated(Ljava/util/ArrayList;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const/4 v0, 0x0

    .line 102
    .local v0, "IsDuplicated":Z
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 103
    .local v1, "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    iget v3, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    iget v4, v1, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    if-ne v3, v4, :cond_0

    .line 104
    const/4 v0, 0x1

    .line 109
    .end local v1    # "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    :cond_1
    return v0
.end method

.method public compareTo(Lcom/sec/android/app/voicenote/common/util/Bookmark;)I
    .locals 2
    .param p1, "arg"    # Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    iget v1, p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->compareTo(Lcom/sec/android/app/voicenote/common/util/Bookmark;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 85
    instance-of v2, p1, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    if-nez v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 89
    check-cast v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 91
    .local v0, "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    iget v3, v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    iget-boolean v3, v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 93
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getElapsed()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    return v0
.end method

.method public getNamed()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "descprition"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setElapsed(I)V
    .locals 0
    .param p1, "elapsed"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    .line 56
    return-void
.end method

.method public setNamed(Z)V
    .locals 0
    .param p1, "isNamed"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->isNamed:Z

    .line 34
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->elapsed:I

    int-to-long v2, v1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->timeToStr(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/Bookmark;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
