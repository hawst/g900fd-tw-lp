.class Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;
.super Landroid/os/Handler;
.source "VNLibraryExpandableListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V
    .locals 0

    .prologue
    .line 1135
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1138
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1153
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1154
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "handleMessage X : "

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    return-void

    .line 1141
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->findPosition()V

    goto :goto_0

    .line 1144
    :sswitch_1
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "closing the cursor"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    .line 1139
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x65 -> :sswitch_0
    .end sparse-switch
.end method
