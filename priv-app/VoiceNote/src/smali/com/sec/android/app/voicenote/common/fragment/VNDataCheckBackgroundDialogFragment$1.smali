.class Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;
.super Ljava/lang/Object;
.source "VNDataCheckBackgroundDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const v4, 0x7f0e001e

    const/4 v3, 0x1

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->key:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "location_tag"

    if-ne v1, v2, :cond_1

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string v1, "key_data_check_location"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 52
    :cond_0
    const-string v1, "show_location_tag_info"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 53
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 66
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->dismiss()V

    .line 67
    return-void

    .line 55
    :cond_2
    const-string v1, "location_tag"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 56
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isAllowedLocation()Z

    move-result v1

    if-nez v1, :cond_3

    .line 57
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 58
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 60
    .end local v0    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    goto :goto_0
.end method
