.class Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;
.super Ljava/lang/Object;
.source "VNDeleteDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 117
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 118
    new-instance v8, Landroid/os/Message;

    invoke-direct {v8}, Landroid/os/Message;-><init>()V

    .line 119
    .local v8, "msg":Landroid/os/Message;
    const/16 v9, 0x2329

    iput v9, v8, Landroid/os/Message;->what:I

    .line 120
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)[J

    move-result-object v9

    iput-object v9, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 121
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)Landroid/os/Handler;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 142
    .end local v8    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    iget-object v9, v9, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 124
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    iget-object v9, v9, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v9}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->stop_HidePlayer()V

    .line 127
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 128
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x0

    .line 130
    .local v3, "failedCnt":I
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)[J

    move-result-object v0

    .local v0, "arr$":[J
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_4

    aget-wide v6, v0, v4

    .line 131
    .local v6, "id":J
    invoke-static {v1, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v6, v7, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->deleteFile(Landroid/content/Context;JLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-nez v9, :cond_3

    .line 132
    add-int/lit8 v3, v3, 0x1

    .line 130
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 135
    .end local v0    # "arr$":[J
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "id":J
    :catch_0
    move-exception v2

    .line 136
    .local v2, "e":Ljava/lang/NullPointerException;
    const-string v9, "VNDeleteDialogFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "positive NPE : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :cond_4
    if-lez v3, :cond_0

    .line 139
    const v9, 0x7f0b0175

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0
.end method
