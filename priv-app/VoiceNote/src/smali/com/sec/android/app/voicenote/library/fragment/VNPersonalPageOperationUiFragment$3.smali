.class Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;
.super Ljava/lang/Object;
.source "VNPersonalPageOperationUiFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z

    .line 675
    return-void
.end method

.method public rename()V
    .locals 3

    .prologue
    .line 679
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getNewFileName(Ljava/io/File;)Ljava/io/File;
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z

    .line 683
    return-void
.end method

.method public replace()V
    .locals 2

    .prologue
    .line 687
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I

    .line 688
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z

    .line 690
    return-void
.end method
