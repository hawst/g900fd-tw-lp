.class public Lcom/sec/android/app/voicenote/common/util/VNSettings;
.super Ljava/lang/Object;
.source "VNSettings.java"


# static fields
.field public static final ADV_REC_INFO_DIALOG_OFF:I = 0x1

.field public static final ADV_REC_INFO_DIALOG_ON:I = 0x0

.field public static final KEY_CATEGORY_ID:Ljava/lang/String; = "category_text"

.field public static final KEY_CATEGORY_LABEL_COLOR:Ljava/lang/String; = "category_label_color"

.field public static final KEY_CATEGORY_LABEL_ID:Ljava/lang/String; = "category_label_id"

.field public static final KEY_CATEGORY_LABEL_POSITION:Ljava/lang/String; = "category_label_position"

.field public static final KEY_CATEGORY_LABEL_TITLE:Ljava/lang/String; = "category_label_title"

.field public static final KEY_CHANGE_NOTIFICATION_RESUME:Ljava/lang/String; = "change_notification_resume"

.field public static final KEY_CHANGE_NOTIFICATION_SERVICE:Ljava/lang/String; = "change_notification_service"

.field public static final KEY_CHANNEL:Ljava/lang/String; = "channel"

.field public static final KEY_CONTEXTUAL_FILENAME:Ljava/lang/String; = "contextual_filename"

.field public static final KEY_CONTEXTUAL_FILENAME_VALUE:Ljava/lang/String; = "contextual_filename_value"

.field public static final KEY_DATA_CHECK_LOCATION:Ljava/lang/String; = "key_data_check_location"

.field public static final KEY_DATA_CHECK_MOBILE_HELP:Ljava/lang/String; = "connect_via_mobile_help"

.field public static final KEY_DATA_CHECK_WIFI_HELP:Ljava/lang/String; = "connect_via_wifi_help"

.field public static final KEY_DEFAULT_NAME:Ljava/lang/String; = "default_name"

.field public static final KEY_GENERAL_CATEGORY:Ljava/lang/String; = "general"

.field public static final KEY_LIBRARY_MODE:Ljava/lang/String; = "library_mode"

.field public static final KEY_LOCATION_TAG:Ljava/lang/String; = "location_tag"

.field public static final KEY_LOGO:Ljava/lang/String; = "logo"

.field public static final KEY_LOGO_BG_COLOR:Ljava/lang/String; = "logo_bg_color"

.field public static final KEY_LOGO_BG_COLOR_TEMP:Ljava/lang/String; = "logo_bg_color_temp"

.field public static final KEY_LOGO_FILE:Ljava/lang/String; = "logo_filepath"

.field public static final KEY_LOGO_TEXT:Ljava/lang/String; = "logo_text"

.field public static final KEY_LOGO_TEXT_CHANGED:Ljava/lang/String; = "logo_text_changed"

.field public static final KEY_LOGO_TEXT_COLOR:Ljava/lang/String; = "logo_text_color"

.field public static final KEY_LOGO_TEXT_COLOR_TEMP:Ljava/lang/String; = "logo_text_color_temp"

.field public static final KEY_LOGO_TEXT_FONT:Ljava/lang/String; = "logo_text_font"

.field public static final KEY_LOGO_TEXT_FONT_TEMP:Ljava/lang/String; = "logo_text_font_temp"

.field public static final KEY_LOGO_TEXT_FONT_TYPEFACE:Ljava/lang/String; = "logo_text_font_typeface"

.field public static final KEY_LOGO_TEXT_FONT_TYPEFACE_TEMP:Ljava/lang/String; = "logo_text_font_typeface_temp"

.field public static final KEY_LOGO_TEXT_TEMP:Ljava/lang/String; = "logo_text_temp"

.field public static final KEY_LOGO_TYPE:Ljava/lang/String; = "logo_type"

.field public static final KEY_LOGO_TYPE_TEMP:Ljava/lang/String; = "logo_type_temp"

.field public static final KEY_MAX_AMPLITUDE:Ljava/lang/String; = "max_amplitude"

.field public static final KEY_MMS_MAX_SIZE:Ljava/lang/String; = "mms_max_size"

.field public static final KEY_MMS_MAX_SIZE_RECEIVED:Ljava/lang/String; = "mms_max_size_received"

.field public static final KEY_NOISE_REDUCTION:Ljava/lang/String; = "noise_reduction"

.field public static final KEY_OPENSOURCE_LICENSE:Ljava/lang/String; = "opensource_license"

.field public static final KEY_PREFERENCES:Ljava/lang/String; = "com.sec.android.app.voicenote_preferences"

.field public static final KEY_RECORDING_QUALITY:Ljava/lang/String; = "recording_quality"

.field public static final KEY_RECORDING_VOLUME:Ljava/lang/String; = "recording_volume"

.field public static final KEY_RECORD_MODE:Ljava/lang/String; = "record_mode"

.field public static final KEY_RECORD_MODE_ADV:Ljava/lang/String; = "record_mode_adv"

.field public static final KEY_RECORD_MODE_PREV:Ljava/lang/String; = "record_mode_prev"

.field public static final KEY_SEARCHT_TEXT:Ljava/lang/String; = "search_text"

.field public static final KEY_SHOW_CONVERSATION_INFO:Ljava/lang/String; = "show_conversation_info"

.field public static final KEY_SHOW_INTERVIEW_INFO:Ljava/lang/String; = "show_interview_info"

.field public static final KEY_SHOW_LOCATION_TAG_INFO:Ljava/lang/String; = "show_location_tag_info"

.field public static final KEY_SHOW_NFC_ENABLE:Ljava/lang/String; = "show_nfc_enable"

.field public static final KEY_SHOW_NFC_RENAME:Ljava/lang/String; = "show_nfc_rename"

.field public static final KEY_SHOW_POLICY_INFO:Ljava/lang/String; = "show_policy_info"

.field public static final KEY_SHOW_STT_REC_INFO:Ljava/lang/String; = "show_stt_recc_info"

.field public static final KEY_SHOW_STT_REC_INFO2:Ljava/lang/String; = "show_stt_recc_info2"

.field public static final KEY_SHOW_STT_WARNING:Ljava/lang/String; = "show_stt_warning"

.field public static final KEY_SHOW_TRIM_WARNING:Ljava/lang/String; = "show_trim_warning"

.field public static final KEY_SKIP_INTERVAL:Ljava/lang/String; = "skip_interval"

.field public static final KEY_SKIP_INTERVAL_VALUE:Ljava/lang/String; = "skip_interval_value"

.field public static final KEY_SORT_MODE:Ljava/lang/String; = "sort_mode"

.field public static final KEY_STORAGE:Ljava/lang/String; = "storage"

.field public static final KEY_STT_AUTOSTART:Ljava/lang/String; = "stt_auto_start"

.field public static final KEY_STT_LANGUAGE:Ljava/lang/String; = "stt_language"

.field public static final KEY_TIME_SHIFT_RECORDING:Ljava/lang/String; = "time_shift_recording"

.field public static final KEY_USE_PERMISSION_ALERT:Ljava/lang/String; = "use_permission_alert"

.field public static final LABEL_DB_NONE_ID:I = 0x0

.field public static final LOCATION_TAG_DIALOG_OFF:I = 0x1

.field public static final LOCATION_TAG_DIALOG_ON:I = 0x0

.field public static final LOGO_IMAGE:I = 0x1

.field public static final LOGO_NONE:I = 0x0

.field public static final LOGO_TEXT:I = 0x2

.field public static final OFF:I = 0x1

.field public static final ON:I = 0x0

.field public static final POLICY_DIALOG_OFF:I = 0x1

.field public static final POLICY_DIALOG_ON:I = 0x0

.field public static final PREF_IS_INITIAL_PLAYER_ACCESS:Ljava/lang/String; = "initial_player_access"

.field public static final QUALITY_HIGH:I = 0x0

.field public static final QUALITY_MMS:I = 0x2

.field public static final QUALITY_NORMAL:I = 0x1

.field public static final SKIP_INTERVAL_ARRAY:[I

.field public static final SKIP_INTERVAL_DEFAULTVALUE:I = 0x2

.field public static final STORAGE_MEMORYCARD:I = 0x1

.field public static final STORAGE_PHONE:I = 0x0

.field public static final STT_WARNING_DIALOG_OFF:I = 0x1

.field public static final STT_WARNING_DIALOG_ON:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VNSettings"

.field public static final TRIM_WARNING_DIALOG_OFF:I = 0x1

.field public static final TRIM_WARNING_DIALOG_ON:I = 0x0

.field public static final VOLUME_HIGH:I = 0x0

.field public static final VOLUME_LOW:I = 0x1

.field private static mContext:Landroid/content/Context;

.field public static mLastTimestamp:J

.field private static mMaxAmplitude:J

.field private static mNFCfilename:Ljava/lang/String;

.field private static mPlaySpeedIndex:I

.field public static mRecordDuration:I

.field private static mRecorderState:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    .line 135
    const-wide/16 v0, 0x61a8

    sput-wide v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    .line 136
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mLastTimestamp:J

    .line 137
    sput v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    .line 138
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecorderState:I

    .line 139
    sput v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 140
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mNFCfilename:Ljava/lang/String;

    return-void

    .line 132
    nop

    :array_0
    .array-data 4
        0x5
        0xa
        0x1e
        0x3c
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static changePlaySpeed()V
    .locals 2

    .prologue
    .line 302
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    .line 303
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 304
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    .line 306
    :cond_0
    return-void
.end method

.method public static getBooleanSettings(Ljava/lang/String;)Z
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 196
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 199
    :goto_0
    return v1

    .line 198
    :cond_0
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 199
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public static getBooleanSettings(Ljava/lang/String;Z)Z
    .locals 6
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 203
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 214
    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .line 205
    .restart local p1    # "defaultValue":Z
    :cond_0
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v4, "com.sec.android.app.voicenote_preferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 207
    .local v1, "pref":Landroid/content/SharedPreferences;
    move v2, p1

    .line 209
    .local v2, "result":Z
    :try_start_0
    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_1
    :goto_1
    move p1, v2

    .line 214
    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/ClassCastException;
    const-string v3, "0"

    invoke-interface {v1, p0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_1

    .line 212
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static getDefaultFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 259
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 262
    .local v0, "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-object v1

    .line 261
    .end local v0    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.voicenote_preferences"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 262
    .restart local v0    # "pref":Landroid/content/SharedPreferences;
    const-string v1, "default_name"

    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0162

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getInternalStorageSelected()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 246
    const-string v2, "storage"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 247
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isAvailableForMemoryCard(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 254
    :goto_0
    return v0

    .line 250
    :cond_0
    const-string v2, "storage"

    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    move v0, v1

    .line 251
    goto :goto_0

    :cond_1
    move v0, v1

    .line 254
    goto :goto_0
.end method

.method public static getLongSettings(Ljava/lang/String;J)J
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "initvalue"    # J

    .prologue
    .line 239
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    const-wide/16 v2, 0x0

    .line 242
    :goto_0
    return-wide v2

    .line 241
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.voicenote_preferences"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 242
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0
.end method

.method public static getMaxAmplitude()J
    .locals 4

    .prologue
    .line 278
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    const-wide/16 v2, 0x0

    .line 282
    .local v0, "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-wide v2

    .line 280
    .end local v0    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.voicenote_preferences"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 281
    .restart local v0    # "pref":Landroid/content/SharedPreferences;
    const-string v1, "max_amplitude"

    sget-wide v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    .line 282
    sget-wide v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    goto :goto_0
.end method

.method public static getMmsMaxSize()J
    .locals 6

    .prologue
    .line 325
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "mms_max_size"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 326
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "mms_max_size_received"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "received":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 328
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNCscCustomerParser;->setMmsMaxSizeFromCustomer(Landroid/content/Context;)V

    .line 330
    :cond_0
    const-string v2, "mms_max_size"

    const-wide/32 v4, 0x49c00

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static getNFCfilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mNFCfilename:Ljava/lang/String;

    return-object v0
.end method

.method public static getPanelFont()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 372
    const-string v0, "/system/fonts/SamsungSans-Regular.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public static getPlaySpeedIndex()I
    .locals 1

    .prologue
    .line 313
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    return v0
.end method

.method public static getRecorderState()I
    .locals 1

    .prologue
    .line 290
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecorderState:I

    return v0
.end method

.method public static getSettings(Ljava/lang/String;)I
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 185
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v1

    .line 187
    :cond_1
    const-string v2, "record_mode"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 191
    :cond_2
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 192
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "0"

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public static getSettings(Ljava/lang/String;I)I
    .locals 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "initvalue"    # I

    .prologue
    const/4 v1, 0x0

    .line 232
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 235
    :goto_0
    return v1

    .line 234
    :cond_0
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 235
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public static getStringSettings(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 218
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 221
    :goto_0
    return-object v1

    .line 220
    :cond_0
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 221
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 225
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 228
    .end local p1    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 227
    .restart local p1    # "defaultValue":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.voicenote_preferences"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 228
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static getTextFont(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 11
    .param p0, "fonttype"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 341
    sget-object v8, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-eqz v8, :cond_0

    if-nez p0, :cond_1

    .line 342
    :cond_0
    const-string v8, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v8}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 368
    :goto_0
    return-object v6

    .line 344
    :cond_1
    const/4 v6, 0x0

    .line 345
    .local v6, "typeface":Landroid/graphics/Typeface;
    const/4 v3, 0x0

    .line 346
    .local v3, "mFontAssetManager":Landroid/content/res/AssetManager;
    sget-object v8, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 347
    .local v4, "mPackageManager":Landroid/content/pm/PackageManager;
    const-string v8, "/"

    invoke-virtual {p0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 348
    .local v7, "word":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 350
    .local v2, "fontPackageName":Ljava/lang/String;
    array-length v8, v7

    if-le v8, v9, :cond_2

    .line 351
    aget-object v2, v7, v9

    .line 357
    const/16 v8, 0x80

    :try_start_0
    invoke-virtual {v4, v2, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 359
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v8, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v8, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 360
    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v5

    .line 362
    .local v5, "res":Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    .line 363
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fonts/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v7, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 353
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "res":Landroid/content/res/Resources;
    :cond_2
    const-string v8, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v8}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    goto :goto_0

    .line 364
    :catch_0
    move-exception v1

    .line 365
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "VNSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "NameNotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static initContext(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    const-string v0, "VNSettings"

    const-string v1, "initAppContext"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    .line 145
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->initSTTLanguage()V

    .line 146
    return-void
.end method

.method private static initSTTLanguage()V
    .locals 4

    .prologue
    const v2, 0x7f0b013a

    const v3, 0x7f0b0136

    .line 410
    const-string v0, "stt_language"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 411
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "de_DE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0131

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :goto_0
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_0
    return-void

    .line 413
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 414
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 415
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 416
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b013b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 417
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "es_ES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 418
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0133

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 419
    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "es_US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 420
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0132

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 421
    :cond_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fr_FR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 422
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0134

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 423
    :cond_6
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "it_IT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 424
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0135

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 425
    :cond_7
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ru_RU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 426
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0139

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 427
    :cond_8
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pt_BR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 428
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0138

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 429
    :cond_9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zh_CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 430
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0130

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 431
    :cond_a
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ja_JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 432
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 433
    :cond_b
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ko_KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 434
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0137

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 436
    :cond_c
    const-string v0, "stt_language"

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static isAllowedLocation()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 376
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 385
    .local v0, "location_prividers":I
    :cond_0
    :goto_0
    return v1

    .line 379
    .end local v0    # "location_prividers":I
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 380
    .restart local v0    # "location_prividers":I
    const-string v2, "VNSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAllowedLocation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    if-eqz v0, :cond_0

    .line 383
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isLocationVoicerecorderOn()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 389
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 396
    :cond_0
    :goto_0
    return v0

    .line 393
    :cond_1
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "tag_current_location_voicerecorder"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isMmsMode()Z
    .locals 2

    .prologue
    .line 334
    const-string v0, "recording_quality"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 335
    const/4 v0, 0x1

    .line 337
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setLocationVoicerecorder(I)V
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 401
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "tag_current_location_voicerecorder"

    invoke-static {v0, v1, p0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    const-string v0, "VNSettings"

    const-string v1, "setLocationVoicerecorder : FAIL"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setMaxAmplitude(J)J
    .locals 6
    .param p0, "maxAmplitude"    # J

    .prologue
    .line 266
    sget-wide v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    cmp-long v2, v2, p0

    if-gez v2, :cond_0

    .line 267
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 268
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 269
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    const-string v2, "max_amplitude"

    invoke-interface {v0, v2, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 270
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 272
    sput-wide p0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    .line 274
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    sget-wide v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mMaxAmplitude:J

    return-wide v2
.end method

.method public static setMmsMaxSize(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mms_max_size"    # Ljava/lang/String;
    .param p2, "mmsMaxSize"    # J

    .prologue
    .line 317
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "mms_max_size"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 318
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 319
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    const-string v2, "mms_max_size"

    invoke-interface {v0, v2, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 320
    const-string v2, "mms_max_size_received"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 321
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 322
    return-void
.end method

.method public static setNFCfilename(Ljava/lang/String;)V
    .locals 0
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 294
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mNFCfilename:Ljava/lang/String;

    .line 295
    return-void
.end method

.method public static setPlaySpeedIndex(I)V
    .locals 0
    .param p0, "speed"    # I

    .prologue
    .line 309
    sput p0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mPlaySpeedIndex:I

    .line 310
    return-void
.end method

.method public static setRecorderState(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 286
    sput p0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecorderState:I

    .line 287
    return-void
.end method

.method public static setSettings(Ljava/lang/String;I)V
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 149
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 150
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 151
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 153
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 155
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static setSettings(Ljava/lang/String;J)V
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 158
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 159
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 160
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 161
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 162
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 164
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static setSettings(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 167
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 168
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 169
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 170
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 171
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 173
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static setSettings(Ljava/lang/String;Z)V
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "bool"    # Z

    .prologue
    .line 176
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 177
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.voicenote_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 178
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 179
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method
