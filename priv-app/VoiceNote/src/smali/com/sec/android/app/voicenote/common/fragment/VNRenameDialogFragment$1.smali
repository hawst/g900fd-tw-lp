.class Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;
.super Ljava/lang/Object;
.source "VNRenameDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x1

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->renameFile()V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 215
    .local v2, "retText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;->onRenameDialogPositiveBtn(Ljava/lang/String;)V

    goto :goto_0

    .line 217
    .end local v2    # "retText":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    .line 223
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$200(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0162

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 226
    .local v0, "defaultName":Ljava/lang/String;
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "iw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 227
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\u200f"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    :cond_3
    const-string v3, "default_name"

    invoke-static {v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    .line 231
    .local v1, "frag":Landroid/app/Fragment;
    if-eqz v1, :cond_0

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getTargetRequestCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v5, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 223
    .end local v0    # "defaultName":Ljava/lang/String;
    .end local v1    # "frag":Landroid/app/Fragment;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 234
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 235
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 236
    .restart local v2    # "retText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;->onRenameDialogPositiveBtn(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
