.class public Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
.super Ljava/lang/Object;
.source "SamsungRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;
    }
.end annotation


# static fields
.field static final DEFAULT_VAD_END_THRESHOLD:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SamsungRecognizer"


# instance fields
.field private _cloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

.field private _cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

.field private final _context:Landroid/content/Context;

.field _fileDump:Z

.field private _micFileSink:Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

.field private _speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

.field private _speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

.field private _tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

.field private mRecordingTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;JLcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)V
    .locals 9
    .param p1, "aContext"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    .param p3, "aRecordingStartTime"    # J
    .param p5, "aAudioSource"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .param p6, "aFileDump"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 63
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    .line 64
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    .line 65
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    .line 68
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 69
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    .line 71
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 72
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_fileDump:Z

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_context:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 78
    const-string v2, "stt_language"

    const-string v3, "eng-GBR"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v4}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;->getDNS(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "host":Ljava/lang/String;
    new-instance v0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;

    const/16 v2, 0x1bb

    new-instance v3, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;

    invoke-direct {v3, v4, v6, v6}, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    const-string v4, "Samsung_Android_VoiceNoteSSL_20140227"

    sget-object v5, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;->AppKey_SSL:[B

    sget-object v7, Lcom/nuance/dragon/toolkit/audio/AudioType;->SPEEX_WB:Lcom/nuance/dragon/toolkit/audio/AudioType;

    sget-object v8, Lcom/nuance/dragon/toolkit/audio/AudioType;->SPEEX_WB:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct/range {v0 .. v8}, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;-><init>(Ljava/lang/String;ILcom/nuance/dragon/toolkit/cloudservices/SSLConfig;Ljava/lang/String;[BLjava/lang/String;Lcom/nuance/dragon/toolkit/audio/AudioType;Lcom/nuance/dragon/toolkit/audio/AudioType;)V

    .line 81
    .local v0, "config":Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_context:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->createCloudServices(Landroid/content/Context;Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 82
    new-instance v2, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    invoke-direct {v2, v3}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;-><init>(Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    .line 84
    new-instance v2, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-direct {v2}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v2, p5}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 87
    iput-wide p3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    .line 89
    new-instance v2, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    invoke-direct {v2, p3, p4}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 92
    iput-boolean p6, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_fileDump:Z

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getWord(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getErrorWord(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getEmptyWord()Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v0

    return-object v0
.end method

.method private createRecogSpec()Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 279
    new-instance v2, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    invoke-direct {v2}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;-><init>()V

    .line 280
    .local v2, "settings":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v4, "dictation_type"

    const-string v5, "dictation"

    invoke-virtual {v2, v4, v5}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v4, "stt_language"

    const-string v5, "eng-GBR"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 282
    .local v3, "stt_language":Ljava/lang/String;
    const-string v4, "dictation_language"

    invoke-virtual {v2, v4, v3}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    new-instance v1, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;

    const-string v4, "NVC_ASR_CMD"

    const-string v5, "AUDIO_INFO"

    invoke-direct {v1, v4, v2, v5}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;-><init>(Ljava/lang/String;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;Ljava/lang/String;)V

    .line 285
    .local v1, "retRecogSpec":Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;
    new-instance v0, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    invoke-direct {v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;-><init>()V

    .line 286
    .local v0, "requestInfo":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v4, "start"

    invoke-virtual {v0, v4, v6}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 287
    const-string v4, "end"

    invoke-virtual {v0, v4, v6}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 288
    const-string v4, "text"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v4, "binary_results"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 290
    const-string v4, "enable_intermediate_response"

    invoke-virtual {v0, v4, v6}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 291
    new-instance v4, Lcom/nuance/dragon/toolkit/cloudservices/DictionaryParam;

    const-string v5, "REQUEST_INFO"

    invoke-direct {v4, v5, v0}, Lcom/nuance/dragon/toolkit/cloudservices/DictionaryParam;-><init>(Ljava/lang/String;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;)V

    invoke-virtual {v1, v4}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;->addParam(Lcom/nuance/dragon/toolkit/cloudservices/DataParam;)V

    .line 293
    return-object v1
.end method

.method private getEmptyWord()Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const/4 v4, 0x0

    .line 254
    new-instance v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/common/VNSTTData;-><init>(I)V

    .line 256
    .local v0, "result":Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    new-instance v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;-><init>()V

    aput-object v2, v1, v4

    .line 257
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    .line 258
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    .line 259
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    const-wide/16 v2, 0x1f4

    iput-wide v2, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    .line 260
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    const-string v2, "...."

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    return-object v0
.end method

.method private getErrorWord(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 8
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x1

    const/4 v4, 0x0

    .line 266
    new-instance v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/common/VNSTTData;-><init>(I)V

    .line 268
    .local v0, "result":Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    new-instance v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;-><init>()V

    aput-object v2, v1, v4

    .line 269
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    .line 270
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    .line 271
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    const-wide/16 v2, 0x1f4

    iput-wide v2, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    .line 272
    iget-object v1, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    return-object v0
.end method

.method private getWord(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 28
    .param p1, "cloudResult"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;

    .prologue
    .line 180
    const/4 v8, 0x0

    .line 181
    .local v8, "dicResult":Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    const/4 v7, 0x0

    .line 183
    .local v7, "buffer":Lcom/nuance/dragon/toolkit/data/Data$Bytes;
    if-nez p1, :cond_0

    .line 184
    const-string v23, "SamsungRecognizer"

    const-string v24, "getWord cloudResult is null"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secE(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v23, "cloudResult is null"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getErrorWord(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v23

    const/16 v24, 0x1

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->addSttData(Lcom/sec/android/app/voicenote/main/common/VNSTTData;Z)V

    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getEmptyWord()Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v16

    .line 249
    :goto_0
    return-object v16

    .line 189
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;->getDictionary()Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    move-result-object v4

    .line 190
    .local v4, "_dictaionResult":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v23, "NMAS_PRFX_SESSION_ID"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getString(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$String;

    move-result-object v19

    .line 191
    .local v19, "sessionId":Lcom/nuance/dragon/toolkit/data/Data$String;
    if-eqz v19, :cond_2

    .line 192
    const-string v23, "SamsungRecognizer"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "===== session_id : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v19 .. v19}, Lcom/nuance/dragon/toolkit/data/Data$String;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " ======"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :goto_1
    :try_start_0
    const-string v23, "transcription"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getBytes(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$Bytes;

    move-result-object v7

    .line 199
    iget-object v0, v7, Lcom/nuance/dragon/toolkit/data/Data$Bytes;->value:[B

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResultManager;->createDictationResult([B)Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 206
    if-eqz v7, :cond_1

    if-nez v8, :cond_3

    .line 207
    :cond_1
    const-string v23, "SamsungRecognizer"

    const-string v24, "getWord buffer or dicResult is null"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secE(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getEmptyWord()Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v16

    goto :goto_0

    .line 194
    :cond_2
    const-string v23, "SamsungRecognizer"

    const-string v24, "===== session_id : null ======"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 200
    :catch_0
    move-exception v9

    .line 201
    .local v9, "e":Ljava/lang/Exception;
    const-string v23, "SamsungRecognizer"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "getWord Exception "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secE(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getErrorWord(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v23

    const/16 v24, 0x1

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->addSttData(Lcom/sec/android/app/voicenote/main/common/VNSTTData;Z)V

    .line 203
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getEmptyWord()Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v16

    goto/16 :goto_0

    .line 210
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_3
    const/16 v16, 0x0

    .line 211
    .local v16, "result":Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    invoke-virtual {v8}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->getExtraInformation()Ljava/util/Map;

    move-result-object v15

    .line 212
    .local v15, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v8}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->getExtraInformation()Ljava/util/Map;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 214
    .local v14, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 215
    .local v11, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_4

    .line 216
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 217
    .local v13, "key":Ljava/lang/String;
    invoke-interface {v15, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 218
    .local v22, "value":Ljava/lang/String;
    const-string v23, "SamsungRecognizer"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "key: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " value: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 221
    .end local v13    # "key":Ljava/lang/String;
    .end local v22    # "value":Ljava/lang/String;
    :cond_4
    const-string v23, "SamsungRecognizer"

    const-string v24, "----------------------------------------------"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v10, 0x0

    .local v10, "index":I
    :goto_3
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ge v10, v0, :cond_8

    .line 223
    invoke-virtual {v8, v10}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->sentenceAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;

    move-result-object v17

    .line 224
    .local v17, "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    const-string v23, "SamsungRecognizer"

    invoke-interface/range {v17 .. v17}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-interface/range {v17 .. v17}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->size()I

    move-result v18

    .line 227
    .local v18, "sentencesize":I
    new-instance v16, Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    .end local v16    # "result":Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/common/VNSTTData;-><init>(I)V

    .line 228
    .restart local v16    # "result":Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    const/16 v20, 0x0

    .local v20, "t":I
    :goto_4
    move/from16 v0, v20

    move/from16 v1, v18

    if-ge v0, v1, :cond_7

    .line 229
    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v21

    .line 230
    .local v21, "token":Lcom/nuance/dragon/toolkit/recognition/dictation/Token;
    invoke-interface/range {v21 .. v21}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v24

    invoke-interface/range {v21 .. v21}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getEndTime()J

    move-result-wide v26

    move-wide/from16 v0, v24

    move-wide/from16 v2, v26

    invoke-virtual {v8, v0, v1, v2, v3}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->getAlternatives(JJ)Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;

    move-result-object v5

    .line 231
    .local v5, "alters":Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;
    if-eqz v5, :cond_6

    .line 232
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    new-instance v24, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;-><init>()V

    aput-object v24, v23, v20

    .line 233
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    aget-object v23, v23, v20

    invoke-interface/range {v21 .. v21}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    .line 234
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    aget-object v23, v23, v20

    invoke-interface/range {v21 .. v21}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getEndTime()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lEndTime:J

    .line 235
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    aget-object v23, v23, v20

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v24, v0

    aget-object v24, v24, v20

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lEndTime:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v26, v0

    aget-object v26, v26, v20

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    move-wide/from16 v26, v0

    sub-long v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    .line 236
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    aget-object v23, v23, v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v26, v0

    aget-object v26, v26, v20

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    move-wide/from16 v26, v0

    add-long v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    .line 237
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    aget-object v23, v23, v20

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v24, v0

    aget-object v24, v24, v20

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    .line 239
    invoke-interface {v5}, Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;->size()I

    move-result v6

    .line 240
    .local v6, "alterssize":I
    const/4 v12, 0x0

    .local v12, "k":I
    :goto_5
    if-ge v12, v6, :cond_5

    .line 241
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v23, v0

    aget-object v23, v23, v20

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-interface {v5, v12}, Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;->getAlternativeAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Alternative;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lcom/nuance/dragon/toolkit/recognition/dictation/Alternative;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 243
    :cond_5
    const-string v23, "SamsungRecognizer"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "    "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "("

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getEndTime()J

    move-result-wide v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ")"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "   FirstTime:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " TimeStamp:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    move-object/from16 v25, v0

    aget-object v25, v25, v20

    move-object/from16 v0, v25

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    move-wide/from16 v26, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    .end local v6    # "alterssize":I
    .end local v12    # "k":I
    :cond_6
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_4

    .line 222
    .end local v5    # "alters":Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;
    .end local v21    # "token":Lcom/nuance/dragon/toolkit/recognition/dictation/Token;
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 248
    .end local v17    # "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    .end local v18    # "sentencesize":I
    .end local v20    # "t":I
    :cond_8
    const-string v23, "SamsungRecognizer"

    const-string v24, "============================================"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->release()V

    .line 306
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->release()V

    .line 310
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 311
    return-void
.end method

.method public getRecentTimeStamp()J
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->getRecentTimeStamp()J

    move-result-wide v0

    return-wide v0
.end method

.method public processResult()V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_fileDump:Z

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_micFileSink:Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_micFileSink:Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;->stopPlaying()V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;->release()V

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 114
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_fileDump:Z

    if-nez v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;->processResult()V

    .line 119
    :cond_2
    return-void
.end method

.method public setCallback(Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 97
    return-void
.end method

.method public startRecognition()V
    .locals 10

    .prologue
    .line 122
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->getFirstChunkTimeStamp()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mRecordingTime:J

    .line 124
    .local v0, "lFirstTimeStamp":J
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_fileDump:Z

    if-eqz v2, :cond_0

    .line 125
    new-instance v2, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    invoke-direct {v2}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    invoke-virtual {v2, v3}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 128
    new-instance v2, Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

    sget-object v3, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    new-instance v4, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "raw"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".pcm"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_micFileSink:Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_micFileSink:Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_speexDecoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexDecoderPipe;

    invoke-virtual {v2, v3}, Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_micFileSink:Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/audio/sinks/FilePlayerSink;->startPlaying()V

    .line 177
    :goto_0
    return-void

    .line 135
    :cond_0
    const-string v2, "SamsungRecognizer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "=== First TimeStamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ==="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->createRecogSpec()Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_tempPipe:Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;

    new-instance v5, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;->startRecognition(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer$Listener;)V

    goto :goto_0
.end method
