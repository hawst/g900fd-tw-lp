.class Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;
.super Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;
.source "VNBookmarkListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionModeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;IIZ)V
    .locals 2
    .param p2, "viewPosition"    # I
    .param p3, "mode"    # I
    .param p4, "needAnim"    # Z

    .prologue
    .line 482
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 483
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;-><init>(Landroid/content/Context;I)V

    .line 484
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 485
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->setSelectionMode(ZZ)V

    .line 488
    :cond_0
    iput p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->mMenuMode:I

    .line 489
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->mMenuMode:I

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionMode:I
    invoke-static {p1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;I)I

    .line 490
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;IZ)V
    .locals 2
    .param p2, "viewPosition"    # I
    .param p3, "needAnim"    # Z

    .prologue
    .line 475
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 476
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;-><init>(Landroid/content/Context;I)V

    .line 477
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 478
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->setSelectionMode(ZZ)V

    .line 480
    :cond_0
    return-void
.end method


# virtual methods
.method protected finishActionMode()V
    .locals 2

    .prologue
    .line 572
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 573
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_0

    .line 574
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 576
    :cond_0
    return-void
.end method

.method protected getItemCount()I
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->getCount()I

    move-result v0

    .line 597
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 602
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->getItemId(I)J

    move-result-wide v0

    .line 606
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getList()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 588
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getParentFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    return-object v0
.end method

.method protected isCursorValid()Z
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 612
    :cond_0
    const/4 v0, 0x0

    .line 615
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 513
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 514
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 518
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v3, 0x1

    .line 495
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 496
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 497
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    .line 499
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 500
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    .line 501
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 504
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->isCursorValid()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 505
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->setSelectionMode(ZZ)V

    .line 507
    :cond_2
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 9
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v8, 0x7f0e00f5

    const v7, 0x7f0e00f4

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 523
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v3

    .line 524
    .local v3, "listView":Landroid/widget/ListView;
    if-nez v3, :cond_0

    .line 553
    :goto_0
    return v4

    .line 527
    :cond_0
    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    .line 528
    .local v0, "checkedItemCount":I
    if-nez v0, :cond_4

    .line 529
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z

    .line 533
    :goto_1
    if-ne v0, v6, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->getMenuMode()I

    move-result v5

    if-ne v5, v6, :cond_5

    .line 534
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameMenuEnable:Z
    invoke-static {v4, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$502(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z

    .line 538
    :goto_2
    invoke-interface {p2}, Landroid/view/Menu;->clear()V

    .line 539
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 540
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v4, 0x7f0d0001

    invoke-virtual {v2, v4, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 541
    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 542
    .local v1, "del":Landroid/view/MenuItem;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->getMenuMode()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 543
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 544
    const v4, 0x7f0b0070

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 546
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 547
    invoke-interface {p2, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 548
    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 550
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameMenuEnable:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 551
    invoke-interface {p2, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 553
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v4

    goto :goto_0

    .line 531
    .end local v1    # "del":Landroid/view/MenuItem;
    .end local v2    # "inflater":Landroid/view/MenuInflater;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z

    goto :goto_1

    .line 536
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameMenuEnable:Z
    invoke-static {v5, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$502(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z

    goto :goto_2
.end method

.method protected refreashActionMode()V
    .locals 4

    .prologue
    .line 558
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 559
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_0

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    .line 561
    .local v1, "checkItemCount":I
    if-eqz v1, :cond_1

    .line 562
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z

    .line 566
    :goto_0
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 568
    .end local v1    # "checkItemCount":I
    :cond_0
    return-void

    .line 564
    .restart local v1    # "checkItemCount":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z

    goto :goto_0
.end method
