.class Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;
.super Ljava/lang/Object;
.source "VNRecordingModeFragment.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field skip:I

.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;)V
    .locals 1

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->skip:I

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 206
    iget v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->skip:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->skip:I

    .line 207
    iget v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->skip:I

    rem-int/lit8 v2, v2, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 209
    :try_start_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    .line 211
    .local v1, "value":Ljava/lang/Float;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->access$000(Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;)Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090026

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->access$000(Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;)Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->requestLayout()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    .end local v1    # "value":Ljava/lang/Float;
    :cond_0
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
