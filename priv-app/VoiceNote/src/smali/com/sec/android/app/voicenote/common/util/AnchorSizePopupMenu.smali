.class public Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;
.super Ljava/lang/Object;
.source "AnchorSizePopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;,
        Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;
    }
.end annotation


# instance fields
.field private mActionTitle:Landroid/view/View;

.field private mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

.field private mContext:Landroid/content/Context;

.field private mMenuKeys:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOnItemClickListener:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;

.field private mPopupWindow:Landroid/widget/ListPopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V
    .locals 3
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "titleView"    # Landroid/view/View;
    .param p3, "anchorView"    # Landroid/view/View;
    .param p4, "minWidth"    # I

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    .line 27
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;-><init>(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    .line 29
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mActionTitle:Landroid/view/View;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    .line 33
    new-instance v0, Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mActionTitle:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p3}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    new-instance v1, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$1;-><init>(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 49
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mOnItemClickListener:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private setDropdownWidth()V
    .locals 8

    .prologue
    .line 94
    const/4 v3, -0x1

    .line 95
    .local v3, "longestItem":I
    const/4 v2, -0x1

    .line 96
    .local v2, "lengthForItem":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mActionTitle:Landroid/view/View;

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    .line 97
    .local v5, "paint":Landroid/text/TextPaint;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09001a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 98
    const/4 v1, 0x0

    .line 99
    .local v1, "item":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->getCount()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 100
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "item":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 102
    .restart local v1    # "item":Ljava/lang/String;
    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v6

    float-to-int v2, v6

    .line 103
    if-le v2, v3, :cond_0

    .line 104
    move v3, v2

    .line 99
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 109
    .local v4, "paddingSize":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    mul-int/lit8 v7, v4, 0x2

    add-int/2addr v7, v3

    invoke-virtual {v6, v7}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 110
    return-void
.end method


# virtual methods
.method public addMenu(ILjava/lang/String;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "menu"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 53
    .local v0, "exist":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->removeMenu(Ljava/lang/String;)V

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->addMenu(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public clearMenu()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->clearMenu()V

    .line 70
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 91
    return-void
.end method

.method public getAnchorView()Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mActionTitle:Landroid/view/View;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public removeMenu(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 61
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    .local v0, "exist":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mAdapter:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->removeMenu(Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method

.method public setOnMenuItemClickListener(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mOnItemClickListener:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$OnMenuItemClickListener;

    .line 87
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->setDropdownWidth()V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 79
    return-void
.end method
