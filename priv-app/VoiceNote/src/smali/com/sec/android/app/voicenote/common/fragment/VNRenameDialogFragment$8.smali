.class Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;
.super Ljava/lang/Object;
.source "VNRenameDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 461
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$200(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 462
    .local v0, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$600(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 464
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_0

    .line 465
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$600(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 470
    :goto_0
    return-void

    .line 468
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->access$600(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method
