.class public Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;
.super Ljava/lang/Object;
.source "SamsungSTT.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;,
        Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;
    }
.end annotation


# static fields
.field private static final BUFFER_READ_START:I = 0x0

.field private static final DEFAULT_VAD_BEGIN_THRESHOLD:I = 0x3

.field private static final DEFAULT_VAD_END_LEN:I = 0x1e

.field private static final DEFAULT_VAD_END_THRESHOLD:I = 0x1

.field private static final PCM_WRITE_MAX_TIME:I = 0x4e20

.field private static final R:I = 0x1bc

.field private static final READ_INTERVAL:I = 0x78

.field private static final STT_FILE:Ljava/lang/String; = "/data/data/com.sec.android.app.voicenote/voice_note/voice_note.stt"

.field private static final STT_FOLDER:Ljava/lang/String; = "/data/data/com.sec.android.app.voicenote/voice_note"

.field private static final TAG:Ljava/lang/String; = "SamsungSTT"

.field private static final W:I = 0xde

.field private static final X:I = 0x6f

.field private static _detectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

.field private static _endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

.field private static _epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

.field private static _recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

.field private static _recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

.field private static _speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

.field private static _voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field private static final bFileDump:Z

.field private static mAudioManager:Landroid/media/AudioManager;

.field private static mBIStream:Ljava/io/BufferedInputStream;

.field private static mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

.field private static mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

.field private static mContext:Landroid/content/Context;

.field private static mFileInputStream:Ljava/io/FileInputStream;

.field private static mPCMFilecnt:I

.field private static mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

.field private static mReadFilecnt:I

.field private static mSTTState:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static final mTTSHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mContext:Landroid/content/Context;

    .line 61
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 62
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .line 63
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 64
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    .line 65
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_detectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    .line 66
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .line 68
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    .line 69
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    .line 70
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    .line 71
    sput v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I

    .line 72
    sput v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I

    .line 73
    sput v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    .line 75
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    .line 76
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    .line 77
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 80
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .line 81
    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 504
    new-instance v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$2;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    return-void
.end method

.method static synthetic access$000()Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .prologue
    .line 46
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    return-object p0
.end method

.method static synthetic access$100()Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    return-object v0
.end method

.method static synthetic access$1000()Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$1100()Ljava/io/BufferedInputStream;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    return-object v0
.end method

.method static synthetic access$1102(Ljava/io/BufferedInputStream;)Ljava/io/BufferedInputStream;
    .locals 0
    .param p0, "x0"    # Ljava/io/BufferedInputStream;

    .prologue
    .line 46
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    return-object p0
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I

    return v0
.end method

.method static synthetic access$1202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 46
    sput p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I

    return p0
.end method

.method static synthetic access$1300()Ljava/io/FileInputStream;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    return-object v0
.end method

.method static synthetic access$1302(Ljava/io/FileInputStream;)Ljava/io/FileInputStream;
    .locals 0
    .param p0, "x0"    # Ljava/io/FileInputStream;

    .prologue
    .line 46
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    return-object p0
.end method

.method static synthetic access$1400(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 46
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->deleteFilepath(I)V

    return-void
.end method

.method static synthetic access$1500(I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 46
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getFilepath(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800()V
    .locals 0

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->restartBufferReadThread()V

    return-void
.end method

.method static synthetic access$200()Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    return-object v0
.end method

.method static synthetic access$202(Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;)Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    .locals 0
    .param p0, "x0"    # Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .prologue
    .line 46
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    return-object p0
.end method

.method static synthetic access$300()Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    return-object v0
.end method

.method static synthetic access$302(Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;)Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;
    .locals 0
    .param p0, "x0"    # Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    .prologue
    .line 46
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    return-object p0
.end method

.method static synthetic access$400()Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    return-object v0
.end method

.method static synthetic access$500()Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_detectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    return-object v0
.end method

.method static synthetic access$600()Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700()Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    return-object v0
.end method

.method static synthetic access$800()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$900()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I

    return v0
.end method

.method static synthetic access$908()I
    .locals 2

    .prologue
    .line 46
    sget v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I

    return v0
.end method

.method public static addAudioBuffer([S)V
    .locals 2
    .param p0, "buf"    # [S

    .prologue
    .line 329
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    if-eqz v0, :cond_0

    .line 330
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    array-length v1, p0

    invoke-virtual {v0, p0, v1}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    .line 332
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    if-eqz v0, :cond_1

    .line 333
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->addAudioBuffer([S)V

    .line 335
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    if-eqz v0, :cond_2

    .line 336
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->addAudioBuffer([S)V

    .line 338
    :cond_2
    return-void
.end method

.method private static declared-synchronized deleteAllPCMfiles()V
    .locals 3

    .prologue
    .line 493
    const-class v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;

    monitor-enter v1

    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x10

    if-gt v0, v2, :cond_0

    .line 494
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->deleteFilepath(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 496
    :cond_0
    monitor-exit v1

    return-void

    .line 493
    :catchall_0
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method private static deleteFilepath(I)V
    .locals 7
    .param p0, "count"    # I

    .prologue
    .line 483
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s%2$03d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "/data/data/com.sec.android.app.voicenote/voice_note/voice_note.stt"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 484
    .local v1, "filepath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 486
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 487
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 488
    const-string v2, "SamsungSTT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFilepath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_0
    return-void
.end method

.method public static deleteSTTfolder()V
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data/com.sec.android.app.voicenote/voice_note"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 106
    :cond_0
    return-void
.end method

.method private static getFilepath(I)Ljava/lang/String;
    .locals 6
    .param p0, "count"    # I

    .prologue
    .line 499
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s%2$03d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/data/data/com.sec.android.app.voicenote/voice_note/voice_note.stt"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "mFilepath":Ljava/lang/String;
    return-object v0
.end method

.method public static getSTTState()I
    .locals 1

    .prologue
    .line 231
    sget v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    return v0
.end method

.method private static interruptThread(Ljava/lang/Thread;)V
    .locals 3
    .param p0, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 475
    const-string v0, "SamsungSTT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "interruptThread : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    .line 480
    :cond_0
    return-void
.end method

.method private static isAliveThread(Ljava/lang/Thread;)Z
    .locals 1
    .param p0, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 466
    if-eqz p0, :cond_0

    .line 467
    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const/4 v0, 0x1

    .line 471
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeSTTfolder(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    new-instance v1, Ljava/io/File;

    const-string v3, "/data/data/com.sec.android.app.voicenote/voice_note"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 86
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 89
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chmod "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x309

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/data/data/com.sec.android.app.voicenote/voice_note"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 90
    .local v0, "chmod":Ljava/lang/Process;
    invoke-virtual {v0}, Ljava/lang/Process;->waitFor()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 99
    .end local v0    # "chmod":Ljava/lang/Process;
    :cond_0
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v2

    .line 92
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "SamsungSTT"

    const-string v4, "chmod IOException"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 94
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 95
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v3, "SamsungSTT"

    const-string v4, "chmod InterruptedException"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized pauseSTT()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 173
    const-class v2, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;

    monitor-enter v2

    :try_start_0
    const-string v1, "SamsungSTT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pauseSTT : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    sget v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v5, :cond_0

    .line 205
    .local v0, "e":Ljava/io/IOException;
    :goto_0
    monitor-exit v2

    return-void

    .line 179
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_1

    .line 180
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "voice_note_recording=off"

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 182
    :cond_1
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 183
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->interruptThread(Ljava/lang/Thread;)V

    .line 184
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->interruptThread(Ljava/lang/Thread;)V

    .line 185
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->stopRecording()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :try_start_2
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_2

    .line 189
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 191
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    if-eqz v1, :cond_3

    .line 192
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 197
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_3
    :goto_1
    :try_start_3
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->deleteAllPCMfiles()V

    .line 199
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    .line 200
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    .line 201
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    .line 202
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    .line 203
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 204
    const/4 v1, 0x2

    sput v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 194
    .end local v0    # "e":Ljava/io/IOException;
    :catch_0
    move-exception v0

    .line 195
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v1, "SamsungSTT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopSTT IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private static restartBufferReadThread()V
    .locals 2

    .prologue
    .line 455
    const-string v0, "SamsungSTT"

    const-string v1, "restartBufferReadThread"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->interruptThread(Ljava/lang/Thread;)V

    .line 460
    new-instance v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    .line 461
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;->start()V

    .line 463
    :cond_0
    return-void
.end method

.method private static restartPCMThread()V
    .locals 2

    .prologue
    .line 444
    const-string v0, "SamsungSTT"

    const-string v1, "restartPCMThread"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->isAliveThread(Ljava/lang/Thread;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 447
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->interruptThread(Ljava/lang/Thread;)V

    .line 449
    new-instance v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    .line 450
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;->start()V

    .line 452
    :cond_0
    return-void
.end method

.method public static resumeSTT()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 208
    const-string v0, "SamsungSTT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeSTT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    if-ne v0, v5, :cond_0

    .line 221
    :goto_0
    return-void

    .line 214
    :cond_0
    sput v4, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I

    .line 215
    sput v5, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I

    .line 217
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->restartPCMThread()V

    .line 218
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 219
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 220
    sput v5, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    goto :goto_0
.end method

.method public static setCallback(Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;)V
    .locals 2
    .param p0, "callback"    # Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .prologue
    .line 224
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 225
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->setCallback(Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;)V

    .line 228
    :cond_0
    return-void
.end method

.method public static startRecording(J)V
    .locals 10
    .param p0, "lChunkTimeStamp"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235
    const-string v0, "SamsungSTT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startRecording : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    new-instance v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    sget-object v3, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct {v0, v3, p0, p1, v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;JZ)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .line 238
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->startRecording()V

    .line 240
    new-instance v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    sget-object v3, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct {v0, v3, p0, p1, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;JZ)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .line 241
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->startRecording()V

    .line 243
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    const/16 v3, 0x32

    const/16 v4, 0xf

    const/4 v5, 0x3

    const/16 v7, 0x1e

    const/16 v9, 0x23

    move v6, v2

    move v8, v1

    invoke-direct/range {v0 .. v9}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;-><init>(IIIIIIIII)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 253
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 255
    new-instance v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_detectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    .line 304
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_detectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    invoke-direct {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;-><init>(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    .line 305
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 307
    new-instance v3, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    sget-object v4, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    sget-object v8, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    move-wide v6, p0

    move v9, v2

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;-><init>(Landroid/content/Context;Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;JLcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)V

    sput-object v3, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .line 308
    return-void
.end method

.method public static startSTT(Landroid/content/Context;Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 109
    const-string v1, "SamsungSTT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startSTT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    sget v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    if-ne v1, v5, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    sput v4, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMFilecnt:I

    .line 116
    sput v5, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mReadFilecnt:I

    .line 117
    sput-object p0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mContext:Landroid/content/Context;

    .line 118
    sput-object p1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 119
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    .line 121
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->restartPCMThread()V

    .line 122
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 123
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 124
    sput v5, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    .line 126
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    if-eqz v1, :cond_2

    .line 127
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;->onClearScreen()V

    .line 129
    :cond_2
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 130
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeNS()I

    move-result v0

    .line 131
    .local v0, "ret":I
    if-eqz v0, :cond_0

    .line 132
    const-string v1, "SamsungSTT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "voiceEngine.initializeNS returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized stopRecording()V
    .locals 3

    .prologue
    .line 311
    const-class v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;

    monitor-enter v1

    :try_start_0
    const-string v0, "SamsungSTT"

    const-string v2, "stopRecording"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    if-eqz v0, :cond_0

    .line 314
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->stopRecording()J

    .line 315
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .line 317
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    if-eqz v0, :cond_1

    .line 318
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 319
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->release()V

    .line 320
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 322
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    if-eqz v0, :cond_2

    .line 323
    sget-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 324
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    :cond_2
    monitor-exit v1

    return-void

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized stopSTT()V
    .locals 5

    .prologue
    .line 137
    const-class v2, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;

    monitor-enter v2

    :try_start_0
    const-string v1, "SamsungSTT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopSTT : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 170
    .local v0, "e":Ljava/io/IOException;
    :goto_0
    monitor-exit v2

    return-void

    .line 143
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_1

    .line 144
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "voice_note_recording=off"

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 146
    :cond_1
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mTTSHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 147
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->interruptThread(Ljava/lang/Thread;)V

    .line 148
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->interruptThread(Ljava/lang/Thread;)V

    .line 149
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->stopRecording()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    :try_start_2
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_2

    .line 153
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 155
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    if-eqz v1, :cond_3

    .line 156
    sget-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 161
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_3
    :goto_1
    :try_start_3
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->deleteAllPCMfiles()V

    .line 163
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mAudioManager:Landroid/media/AudioManager;

    .line 164
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mPCMThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$PCMThread;

    .line 165
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBufferReadThread:Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$BufferReadThread;

    .line 166
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mFileInputStream:Ljava/io/FileInputStream;

    .line 167
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mBIStream:Ljava/io/BufferedInputStream;

    .line 168
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 169
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mSTTState:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 158
    .end local v0    # "e":Ljava/io/IOException;
    :catch_0
    move-exception v0

    .line 159
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v1, "SamsungSTT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopSTT IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
