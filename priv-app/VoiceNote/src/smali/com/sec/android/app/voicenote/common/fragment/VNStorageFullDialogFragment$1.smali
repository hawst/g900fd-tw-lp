.class Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;
.super Ljava/lang/Object;
.source "VNStorageFullDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    .line 64
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->dismissAllowingStateLoss()V

    .line 67
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.INTERNAL_STORAGE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method
