.class Lcom/sec/android/app/voicenote/main/VNMainActivity$8;
.super Landroid/os/Handler;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2578
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0x15

    .line 2581
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2726
    :cond_0
    :goto_0
    return-void

    .line 2583
    :cond_1
    const/4 v0, 0x0

    .line 2584
    .local v0, "frag":Landroid/app/DialogFragment;
    const/4 v1, 0x0

    .line 2585
    .local v1, "frag1":Landroid/app/DialogFragment;
    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    .line 2725
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 2587
    :sswitch_0
    const-string v5, "VoiceNoteMainActivity"

    const-string v6, "BroadcastMessage.LOW_BATTERY"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2588
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 2589
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finish()V

    goto :goto_1

    .line 2593
    :sswitch_1
    const-string v5, "VoiceNoteMainActivity"

    const-string v6, "BroadcastMessage.HEADSET_PLUGIN"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v5

    const/16 v6, 0x3e8

    if-ne v5, v6, :cond_3

    .line 2595
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    .line 2596
    .local v3, "record_mode":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onModeChanged(I)V

    goto :goto_1

    .line 2597
    .end local v3    # "record_mode":I
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAdvancedAndHeadsetMode()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2598
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doPause()V

    .line 2599
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v9, :cond_4

    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 2601
    :cond_4
    const v5, 0x7f0b000a

    const v6, 0x7f0b000b

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 2604
    :cond_5
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    .line 2605
    const v5, 0x7f0b000e

    const v6, 0x7f0b000f

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2609
    :cond_6
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2611
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v5

    const/16 v6, 0x3eb

    if-ne v5, v6, :cond_2

    .line 2612
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0102

    invoke-static {v5, v6, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_1

    .line 2619
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    .line 2621
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .end local v0    # "frag":Landroid/app/DialogFragment;
    check-cast v0, Landroid/app/DialogFragment;

    .line 2623
    .restart local v0    # "frag":Landroid/app/DialogFragment;
    if-eqz v0, :cond_7

    instance-of v5, v0, Landroid/app/DialogFragment;

    if-eqz v5, :cond_7

    .line 2624
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 2627
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG_SETAS"

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .end local v1    # "frag1":Landroid/app/DialogFragment;
    check-cast v1, Landroid/app/DialogFragment;

    .line 2629
    .restart local v1    # "frag1":Landroid/app/DialogFragment;
    if-eqz v1, :cond_8

    instance-of v5, v1, Landroid/app/DialogFragment;

    if-eqz v5, :cond_8

    .line 2630
    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismiss()V

    .line 2633
    :cond_8
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-eqz v5, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v5

    if-eq v5, v7, :cond_2

    .line 2636
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 2637
    .local v2, "path":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2638
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    goto/16 :goto_1

    .line 2643
    .end local v2    # "path":Ljava/lang/String;
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    goto/16 :goto_1

    .line 2647
    :sswitch_4
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2650
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v5

    if-ne v5, v9, :cond_2

    .line 2651
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isTrimActive()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2652
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doUpdateTrimView()V

    goto/16 :goto_1

    .line 2654
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateControlButtons()V

    .line 2655
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updatePlayer()V

    .line 2656
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->updateLibrary(Z)V

    goto/16 :goto_1

    .line 2662
    :sswitch_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2665
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-eqz v5, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v5

    if-eq v5, v7, :cond_2

    .line 2666
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doNextPlay()V

    goto/16 :goto_1

    .line 2671
    :sswitch_6
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2674
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-eqz v5, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v5

    if-eq v5, v7, :cond_2

    .line 2675
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPreviousPlay()V

    goto/16 :goto_1

    .line 2680
    :sswitch_7
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2683
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-eqz v5, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v5

    if-eq v5, v7, :cond_2

    .line 2684
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doNextPlay()V

    goto/16 :goto_1

    .line 2689
    :sswitch_8
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2692
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-eqz v5, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v5

    if-eq v5, v7, :cond_2

    .line 2693
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPreviousPlay()V

    goto/16 :goto_1

    .line 2698
    :sswitch_9
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 2699
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "search"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->startSearchTask(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3800(Lcom/sec/android/app/voicenote/main/VNMainActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2703
    :sswitch_a
    const v5, 0x7f0b004c

    const v6, 0x7f0b00f8

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->setArguments(II)Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2706
    new-instance v4, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Context;)V

    .line 2707
    .local v4, "thread":Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;->start()V

    goto/16 :goto_1

    .line 2711
    .end local v4    # "thread":Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;
    :sswitch_b
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .end local v0    # "frag":Landroid/app/DialogFragment;
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    .line 2713
    .restart local v0    # "frag":Landroid/app/DialogFragment;
    if-eqz v0, :cond_2

    instance-of v5, v0, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    if-eqz v5, :cond_2

    .line 2714
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    goto/16 :goto_1

    .line 2718
    :sswitch_c
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2719
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$8;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->refreshList()V

    goto/16 :goto_1

    .line 2585
    nop

    :sswitch_data_0
    .sparse-switch
        0x848 -> :sswitch_9
        0x85d -> :sswitch_a
        0x85e -> :sswitch_b
        0xfd2 -> :sswitch_3
        0xfd3 -> :sswitch_2
        0xfd5 -> :sswitch_c
        0xff0 -> :sswitch_0
        0x1130 -> :sswitch_1
        0x125c -> :sswitch_4
        0x125d -> :sswitch_5
        0x125e -> :sswitch_6
        0x125f -> :sswitch_7
        0x1260 -> :sswitch_8
    .end sparse-switch
.end method
