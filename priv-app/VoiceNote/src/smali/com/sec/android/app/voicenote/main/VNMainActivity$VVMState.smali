.class final enum Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
.super Ljava/lang/Enum;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VVMState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

.field public static final enum FIRST_POPUP:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

.field public static final enum GOTO_LIST:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

.field public static final enum INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

.field public static final enum POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 247
    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    const-string v1, "INTIAL_STATE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    const-string v1, "FIRST_POPUP"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->FIRST_POPUP:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    const-string v1, "GOTO_LIST"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->GOTO_LIST:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    new-instance v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    const-string v1, "POPUP_ON_BACK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .line 246
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->FIRST_POPUP:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->GOTO_LIST:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->$VALUES:[Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 246
    const-class v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->$VALUES:[Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    invoke-virtual {v0}, [Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    return-object v0
.end method
