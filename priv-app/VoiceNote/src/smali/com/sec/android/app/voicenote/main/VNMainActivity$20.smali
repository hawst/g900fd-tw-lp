.class Lcom/sec/android/app/voicenote/main/VNMainActivity$20;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 5065
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0e0050

    .line 5068
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 5088
    :goto_0
    :pswitch_0
    return-void

    .line 5070
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(I)Z

    goto :goto_0

    .line 5074
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(IJ)Z

    .line 5075
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(I)Z

    goto :goto_0

    .line 5079
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(IJ)Z

    .line 5080
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(I)Z

    goto :goto_0

    .line 5084
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(IJ)Z

    .line 5085
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$20;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onControlButtonSelected(I)Z

    goto :goto_0

    .line 5068
    :pswitch_data_0
    .packed-switch 0x7f0e0050
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
