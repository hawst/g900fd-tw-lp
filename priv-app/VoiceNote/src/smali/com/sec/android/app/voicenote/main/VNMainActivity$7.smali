.class Lcom/sec/android/app/voicenote/main/VNMainActivity$7;
.super Landroid/os/Handler;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2400
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 19
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2403
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isDestroyed()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2575
    :cond_0
    :goto_0
    return-void

    .line 2406
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v6

    .line 2408
    .local v6, "ft":Landroid/app/FragmentTransaction;
    :try_start_0
    move-object/from16 v0, p1

    iget v13, v0, Landroid/os/Message;->what:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    sparse-switch v13, :sswitch_data_0

    .line 2573
    :cond_2
    :goto_1
    invoke-virtual {v6}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2574
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 2410
    :sswitch_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getLimitedRecordingTime()J
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)J

    move-result-wide v14

    .line 2412
    .local v14, "time":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 2413
    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v13, v0

    sput v13, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 2414
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateTime(J)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2570
    .end local v14    # "time":J
    :catch_0
    move-exception v4

    .line 2571
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 2420
    .end local v4    # "e":Landroid/os/RemoteException;
    :sswitch_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v13

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isFinishing()Z

    move-result v13

    if-nez v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    if-eqz v13, :cond_2

    const-string v13, "record_mode"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v13

    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v13, v0, :cond_2

    sget-boolean v13, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    if-nez v13, :cond_2

    .line 2425
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->startAnimationRecIcon()V

    goto/16 :goto_1

    .line 2431
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 2432
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMainPanelTextId()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->startTimeBlinkAnimation()V

    goto/16 :goto_1

    .line 2438
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    instance-of v13, v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    if-eqz v13, :cond_2

    .line 2439
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUVThreshold:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->updateEQ(IF)V

    goto/16 :goto_1

    .line 2444
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    instance-of v13, v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    if-eqz v13, :cond_2

    .line 2445
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->removeEQ()V

    goto/16 :goto_1

    .line 2450
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v13

    const/16 v16, 0x3eb

    move/from16 v0, v16

    if-ne v13, v0, :cond_3

    .line 2451
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    instance-of v13, v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    if-eqz v13, :cond_2

    .line 2452
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->updateMic(Z)V

    goto/16 :goto_1

    .line 2455
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    instance-of v13, v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    if-eqz v13, :cond_2

    .line 2456
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e000e

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->updateMic(Z)V

    goto/16 :goto_1

    .line 2462
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 2463
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2464
    const/4 v2, 0x0

    .line 2465
    .local v2, "convertedText":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 2466
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSTTFragment:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getConvertedText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 2470
    :cond_4
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v13

    if-eqz v13, :cond_8

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getStartVoiceLabelAfterRecord()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 2471
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNFCEnabled(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2472
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3200(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2485
    :goto_2
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v13

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setStartVoiceLabelAfterRecord(Z)V

    .line 2516
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/16 v16, 0x0

    move/from16 v0, v16

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z
    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2002(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    goto/16 :goto_1

    .line 2474
    :cond_6
    const-string v13, "show_nfc_enable"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 2475
    new-instance v13, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v13, v0, v1}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;-><init>(Landroid/content/Context;Z)V

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Boolean;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2476
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->startNFCWritingActivity()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3200(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    goto :goto_2

    .line 2478
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSavedID()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v13, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    .line 2480
    .local v7, "labelinfo":Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    move-result-object v3

    .line 2482
    .local v3, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const-string v16, "FRAGMENT_DIALOG"

    move-object/from16 v0, v16

    invoke-virtual {v3, v13, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    .line 2486
    .end local v3    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    .end local v7    # "labelinfo":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 2488
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mATTVVMEnabled:Z
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 2489
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    move-result-object v13

    sget-object v16, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->INTIAL_STATE:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    move-object/from16 v0, v16

    if-ne v13, v0, :cond_9

    .line 2490
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    sget-object v16, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->FIRST_POPUP:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    move-object/from16 v0, v16

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3402(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .line 2491
    const v13, 0x7f0b0177

    const v16, 0x7f0b0179

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v13, v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v16

    const-string v17, "FRAGMENT_DIALOG"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2495
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    move-result-object v13

    sget-object v16, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->GOTO_LIST:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    move-object/from16 v0, v16

    if-ne v13, v0, :cond_a

    .line 2496
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    sget-object v16, Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;->POPUP_ON_BACK:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    move-object/from16 v0, v16

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mVVMState:Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;
    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3402(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;)Lcom/sec/android/app/voicenote/main/VNMainActivity$VVMState;

    .line 2500
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/16 v16, 0x0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    invoke-static {v13, v6, v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1500(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V

    .line 2502
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const v16, 0x7f0e0013

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v9

    .line 2504
    .local v9, "list":Landroid/app/Fragment;
    instance-of v13, v9, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 2505
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v13

    invoke-interface {v13}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->getFilePath()Ljava/lang/String;

    move-result-object v8

    .line 2506
    .local v8, "lastSavedFilePath":Ljava/lang/String;
    if-eqz v8, :cond_5

    .line 2507
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getIDFromFilePath(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v10

    .line 2509
    .local v10, "lastSavedId":J
    const-wide/16 v16, 0x0

    cmp-long v13, v10, v16

    if-lez v13, :cond_5

    .line 2510
    check-cast v9, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .end local v9    # "list":Landroid/app/Fragment;
    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->selectRadioButtonId(J)V

    goto/16 :goto_3

    .line 2521
    .end local v2    # "convertedText":Ljava/lang/CharSequence;
    .end local v8    # "lastSavedFilePath":Ljava/lang/String;
    .end local v10    # "lastSavedId":J
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 2522
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2523
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v17

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V
    invoke-static {v13, v6, v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2600(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2537
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2538
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V
    invoke-static {v13, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$700(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V

    .line 2539
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2540
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/16 v16, 0x0

    move/from16 v0, v16

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mAmplitude:I
    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2902(Lcom/sec/android/app/voicenote/main/VNMainActivity;I)I

    .line 2541
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/16 v16, 0x1

    move/from16 v0, v16

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateEQ(Z)V
    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3600(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)V

    goto/16 :goto_1

    .line 2545
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2546
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V
    invoke-static {v13, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$700(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V

    .line 2547
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateTime()V
    invoke-static {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    goto/16 :goto_1

    .line 2551
    :sswitch_a
    const v13, 0x7f0b004c

    const v16, 0x7f0b00f8

    move/from16 v0, v16

    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->setArguments(II)Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v16

    const-string v17, "FRAGMENT_DIALOG"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2554
    new-instance v12, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v12, v13, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Context;)V

    .line 2555
    .local v12, "thread":Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;
    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;->start()V

    goto/16 :goto_1

    .line 2559
    .end local v12    # "thread":Lcom/sec/android/app/voicenote/main/VNMainActivity$ConvertThread;
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const-string v16, "FRAGMENT_DIALOG"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    .line 2561
    .local v5, "frag":Landroid/app/DialogFragment;
    if-eqz v5, :cond_2

    instance-of v13, v5, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    if-eqz v13, :cond_2

    .line 2562
    invoke-virtual {v5}, Landroid/app/DialogFragment;->dismiss()V

    goto/16 :goto_1

    .line 2566
    .end local v5    # "frag":Landroid/app/DialogFragment;
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget v13, v0, Landroid/os/Message;->arg1:I

    if-nez v13, :cond_b

    const/4 v13, 0x1

    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->TransitAnimationChanged(Z)V

    .line 2567
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity$7;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V
    invoke-static {v13, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$700(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 2566
    :cond_b
    const/4 v13, 0x0

    goto :goto_4

    .line 2408
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_7
        0x3eb -> :sswitch_9
        0x3ec -> :sswitch_8
        0x7d1 -> :sswitch_6
        0x7d5 -> :sswitch_7
        0x835 -> :sswitch_0
        0x836 -> :sswitch_1
        0x839 -> :sswitch_3
        0x83a -> :sswitch_4
        0x83b -> :sswitch_5
        0x83d -> :sswitch_2
        0x85d -> :sswitch_a
        0x85e -> :sswitch_b
        0x866 -> :sswitch_c
    .end sparse-switch
.end method
