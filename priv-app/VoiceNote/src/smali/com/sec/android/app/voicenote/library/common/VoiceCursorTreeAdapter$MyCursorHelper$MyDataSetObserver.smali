.class Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "VoiceCursorTreeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$1;

    .prologue
    .line 366
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;-><init>(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mDataValid:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$402(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;Z)Z

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->notifyDataSetChanged()V

    .line 371
    return-void
.end method

.method public onInvalidated()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mDataValid:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$402(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;Z)Z

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyDataSetObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->notifyDataSetInvalidated()V

    .line 377
    return-void
.end method
