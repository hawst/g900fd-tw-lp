.class Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;
.super Landroid/telephony/PhoneStateListener;
.source "VNIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V
    .locals 0

    .prologue
    .line 727
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;Lcom/sec/android/app/voicenote/common/util/VNIntent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNIntent;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/common/util/VNIntent$1;

    .prologue
    .line 727
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;-><init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 730
    const-string v1, "VNIntent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCallStateChanged : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    packed-switch p1, :pswitch_data_0

    .line 755
    :goto_0
    return-void

    .line 735
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xfdc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 736
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xfdc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 752
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VNIntent"

    const-string v2, "ignore telephony event"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 740
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xfdd

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 741
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xfdd

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 745
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xfde

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 746
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$CallListener;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xfde

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 733
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
