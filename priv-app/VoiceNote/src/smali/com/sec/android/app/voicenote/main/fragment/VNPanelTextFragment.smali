.class public Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
.super Landroid/app/Fragment;
.source "VNPanelTextFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;,
        Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;,
        Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNPanelTextFragment"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private LabelButton:Landroid/widget/ImageView;

.field private mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

.field private mColor:I

.field private mDuration:I

.field private mFilename:Landroid/widget/TextView;

.field private mRecIconOff:Landroid/widget/ImageView;

.field private mRecIconOn:Landroid/widget/ImageView;

.field private mSetting_Btn:Landroid/widget/ImageView;

.field private mStrFilename:Ljava/lang/String;

.field private mTime:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->LabelButton:Landroid/widget/ImageView;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOff:Landroid/widget/ImageView;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    .line 67
    iput v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mDuration:I

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mStrFilename:Ljava/lang/String;

    .line 69
    iput v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mColor:I

    .line 73
    return-void
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 346
    sput-object p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 347
    return-void
.end method

.method public static newInstance(I)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    .locals 3
    .param p0, "duration"    # I

    .prologue
    .line 76
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;-><init>()V

    .line 77
    .local v1, "f":Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "duration"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 79
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->setArguments(Landroid/os/Bundle;)V

    .line 80
    return-object v1
.end method

.method public static newInstance(Ljava/lang/String;I)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "color"    # I

    .prologue
    .line 84
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;-><init>()V

    .line 85
    .local v1, "f":Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 86
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "filename"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v2, "color"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->setArguments(Landroid/os/Bundle;)V

    .line 89
    return-object v1
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 351
    return-void
.end method


# virtual methods
.method public changVisibleBtn()V
    .locals 2

    .prologue
    .line 354
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_1

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public disableSettingBtnForVoiceMemo()V
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 366
    :cond_0
    return-void
.end method

.method public getMainPanelTextLayoutId()I
    .locals 5

    .prologue
    const v2, 0x7f030052

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 331
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 334
    const v2, 0x7f030053

    .line 341
    :cond_0
    :goto_0
    return v2

    .line 335
    :cond_1
    instance-of v3, v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    const-string v3, "android.provider.MediaStore.RECORD_SOUND"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 337
    :cond_2
    const/4 v3, 0x3

    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 339
    const v2, 0x7f030054

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 180
    instance-of v0, p1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 181
    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

    .line 183
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 184
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;->onFileNameBtnSelected(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$Callbacks;->onControlButtonSelected(I)Z

    .line 176
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 101
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 102
    const-string v4, "duration"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mDuration:I

    .line 103
    const-string v4, "color"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mColor:I

    .line 104
    const-string v4, "filename"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mStrFilename:Ljava/lang/String;

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getMainPanelTextLayoutId()I

    move-result v4

    invoke-virtual {p1, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 108
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0e00d8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    .line 109
    const v4, 0x7f0e00c8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    .line 110
    const v4, 0x7f0e006a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->LabelButton:Landroid/widget/ImageView;

    .line 111
    const v4, 0x7f0e00d7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOff:Landroid/widget/ImageView;

    .line 112
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOff:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOff:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    :cond_1
    const v4, 0x7f0e00d9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    .line 116
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 117
    const v4, 0x7f0e00da

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    .line 118
    sget-object v4, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_2

    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v9, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOff:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOff:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasPermanentMenuKey(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    if-eqz v4, :cond_3

    .line 125
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 126
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    .line 129
    :cond_3
    const-string v4, "%02d:%02d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 130
    .local v2, "timeText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v9, :cond_4

    .line 133
    const-string v1, "/system/fonts/SamsungSans-Num3T.ttf"

    .line 134
    .local v1, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 137
    .end local v1    # "path":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setSelected(Z)V

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    invoke-virtual {v4, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mStrFilename:Ljava/lang/String;

    if-nez v4, :cond_5

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mStrFilename:Ljava/lang/String;

    .line 144
    :cond_5
    iget v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mColor:I

    if-nez v4, :cond_6

    .line 145
    const-string v4, "category_label_color"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mColor:I

    .line 147
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mStrFilename:Ljava/lang/String;

    iget v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mColor:I

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateFilename(Ljava/lang/String;I)V

    .line 148
    iget v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mDuration:I

    int-to-long v4, v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateTime(J)V

    .line 150
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    if-eqz v4, :cond_8

    .line 151
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_7

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 158
    :cond_8
    new-instance v4, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$1;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 167
    return-object v3
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v13, 0x35

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 371
    const/4 v9, 0x2

    new-array v5, v9, [I

    .line 372
    .local v5, "screenPos":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 374
    .local v1, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p1, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 375
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 377
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    .line 378
    .local v8, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 379
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v6, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 381
    .local v6, "screenWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 383
    .local v0, "cheatSheet":Landroid/widget/Toast;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 384
    .local v4, "res":Landroid/content/res/Resources;
    const v9, 0x7f09000f

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 386
    .local v3, "leftOffset":I
    const v9, 0x7f090010

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    .line 388
    .local v7, "topOffset":I
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-ne v9, v12, :cond_0

    .line 389
    aget v9, v5, v11

    sub-int v9, v6, v9

    div-int/lit8 v10, v8, 0x2

    sub-int/2addr v9, v10

    sub-int/2addr v9, v3

    sub-int v10, v2, v7

    invoke-virtual {v0, v13, v9, v10}, Landroid/widget/Toast;->setGravity(III)V

    .line 395
    :goto_0
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 396
    return v12

    .line 392
    :cond_0
    aget v9, v5, v11

    sub-int v9, v6, v9

    div-int/lit8 v10, v8, 0x2

    sub-int/2addr v9, v10

    add-int/2addr v9, v3

    sub-int v10, v2, v7

    invoke-virtual {v0, v13, v9, v10}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method

.method public startAnimationRecIcon()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 276
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    const-string v0, "record_mode"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->getOnAnimation()Landroid/view/animation/AlphaAnimation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 282
    :cond_0
    return-void
.end method

.method public startTimeBlinkAnimation()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->getBlinkAnimation()Landroid/view/animation/AlphaAnimation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 272
    :cond_0
    return-void
.end method

.method public stopAnimationRecIcon()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mRecIconOn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 289
    :cond_0
    return-void
.end method

.method public updateFilename(Ljava/lang/String;I)V
    .locals 7
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "color"    # I

    .prologue
    const v6, 0x7f08001b

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 225
    sget-boolean v1, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "Source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "ATT_VVM"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Source"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    const v2, 0x7f0b0178

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 234
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getMainPanelTextLayoutId()I

    move-result v1

    const v2, 0x7f030053

    if-ne v1, v2, :cond_3

    .line 266
    :cond_1
    :goto_1
    return-void

    .line 230
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 238
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getMainPanelTextLayoutId()I

    move-result v1

    const v2, 0x7f030054

    if-ne v1, v2, :cond_5

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->LabelButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 242
    const-string v1, "VNPanelTextFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFilename() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    if-nez p2, :cond_4

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->LabelButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 245
    const-string v1, "category_label_id"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_1

    .line 247
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->LabelButton:Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 252
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 253
    .local v0, "category":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 256
    if-nez p2, :cond_6

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 259
    const-string v1, "category_label_id"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 263
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 264
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mFilename:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 261
    :cond_6
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_2
.end method

.method public updateTime(J)V
    .locals 13
    .param p1, "time"    # J

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 187
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 188
    const-wide/16 v6, 0x3e8

    div-long v6, p1, v6

    long-to-int v3, v6

    .line 189
    .local v3, "second":I
    div-int/lit16 v0, v3, 0xe10

    .line 190
    .local v0, "h":I
    div-int/lit8 v5, v3, 0x3c

    rem-int/lit8 v1, v5, 0x3c

    .line 191
    .local v1, "m":I
    rem-int/lit8 v2, v3, 0x3c

    .line 193
    .local v2, "s":I
    const/4 v4, 0x0

    .line 194
    .local v4, "timeText":Ljava/lang/String;
    if-lez v0, :cond_4

    .line 195
    const-string v5, "%02d:%02d:%02d"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 199
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 203
    .end local v0    # "h":I
    .end local v1    # "m":I
    .end local v2    # "s":I
    .end local v3    # "second":I
    .end local v4    # "timeText":Ljava/lang/String;
    :cond_0
    sget-object v5, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    .line 204
    sget-object v5, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v5

    const/16 v6, 0x3e8

    if-ne v5, v6, :cond_5

    .line 205
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 210
    :cond_1
    :goto_1
    sget-object v5, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    if-eqz v5, :cond_3

    .line 211
    sget-object v5, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v5

    const/16 v6, 0x3eb

    if-eq v5, v6, :cond_2

    sget-object v5, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v5

    const/16 v6, 0x3ec

    if-eq v5, v6, :cond_2

    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v11, :cond_6

    .line 214
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setAlpha(F)V

    .line 220
    :cond_3
    :goto_2
    return-void

    .line 197
    .restart local v0    # "h":I
    .restart local v1    # "m":I
    .restart local v2    # "s":I
    .restart local v3    # "second":I
    .restart local v4    # "timeText":Ljava/lang/String;
    :cond_4
    const-string v5, "%02d:%02d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 207
    .end local v0    # "h":I
    .end local v1    # "m":I
    .end local v2    # "s":I
    .end local v3    # "second":I
    .end local v4    # "timeText":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mSetting_Btn:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 216
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->mTime:Landroid/widget/TextView;

    const v6, 0x3f333333    # 0.7f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_2
.end method
