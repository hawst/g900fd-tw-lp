.class public Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNWarningSTTDialogFragment.java"


# instance fields
.field private mCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->doPositiveAction()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->doNegativeAction()V

    return-void
.end method

.method private doNegativeAction()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    const-string v1, "show_stt_warning"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 93
    :goto_0
    const-string v1, "stt_auto_start"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 96
    .local v0, "frag":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->dismissAllowingStateLoss()V

    .line 100
    return-void

    .line 90
    .end local v0    # "frag":Landroid/app/Fragment;
    :cond_1
    const-string v1, "show_stt_warning"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private doPositiveAction()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "show_stt_warning"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 83
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->dismissAllowingStateLoss()V

    .line 84
    return-void

    .line 81
    :cond_0
    const-string v0, "show_stt_warning"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03005f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    .line 49
    .local v1, "layout":Landroid/widget/ScrollView;
    const v3, 0x7f0e00c3

    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 50
    .local v2, "warningText":Landroid/widget/TextView;
    const v3, 0x7f0e00ef

    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 52
    const v3, 0x7f0b013c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 53
    const v3, 0x7f0b0017

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 54
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 56
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 74
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNWarningSTTDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 105
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 106
    return-void
.end method
