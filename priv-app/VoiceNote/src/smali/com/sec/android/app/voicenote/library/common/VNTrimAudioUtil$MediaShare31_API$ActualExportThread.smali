.class Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
.super Landroid/os/AsyncTask;
.source "VNTrimAudioUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActualExportThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mCanceled:Z

.field private mCompleted:Z

.field private mExportStarted:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field start:J

.field final synthetic this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)V
    .locals 4

    .prologue
    .line 651
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 652
    iget-object v1, p1, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 654
    .local v0, "powerManager":Landroid/os/PowerManager;
    const v1, 0x20000006

    const-string v2, "VNTrimAudioUtil"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 656
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->start:J

    .line 657
    return-void
.end method

.method static synthetic access$1502(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    .param p1, "x1"    # Z

    .prologue
    .line 644
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mExportStarted:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    .prologue
    .line 644
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mCanceled:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    .param p1, "x1"    # Z

    .prologue
    .line 644
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mCompleted:Z

    return p1
.end method


# virtual methods
.method public cancel()V
    .locals 5

    .prologue
    .line 764
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    iget-object v1, v1, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSyncKey:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 765
    :try_start_0
    const-string v1, "VNTrimAudioUtil"

    const-string v3, "**** ActualExportThread cancel"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mCanceled:Z

    .line 768
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mExportStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 770
    :try_start_1
    const-string v1, "VNTrimAudioUtil"

    const-string v3, "**** AudioEditorImpl cancelExport() call."

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1900(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mOutput:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/lifevibes/videoeditor/VideoEditor;->cancelExport(Ljava/lang/String;)V

    .line 772
    const-string v1, "VNTrimAudioUtil"

    const-string v3, "**** AudioEditorImpl cancelExport() done."

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 779
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1900(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 780
    const-string v1, "VNTrimAudioUtil"

    const-string v3, "**** Trim AudioEditorImpl release() call."

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1900(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v1

    invoke-interface {v1}, Lcom/lifevibes/videoeditor/VideoEditor;->release()V

    .line 782
    const-string v1, "VNTrimAudioUtil"

    const-string v3, "**** Trim AudioEditorImpl release() done."

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1902(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/lifevibes/videoeditor/VideoEditor;)Lcom/lifevibes/videoeditor/VideoEditor;

    .line 787
    :cond_0
    monitor-exit v2

    .line 788
    return-void

    .line 773
    :catch_0
    move-exception v0

    .line 774
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "VNTrimAudioUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AudioEditorImpl cancelExport()\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 787
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 775
    :catch_1
    move-exception v0

    .line 776
    .local v0, "ex":Ljava/lang/RuntimeException;
    :try_start_3
    const-string v1, "VNTrimAudioUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AudioEditorImpl cancelExport()\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 11
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 661
    const-string v5, "VNTrimAudioUtil"

    const-string v7, "**** ActualExportThread doInBackground"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mOutput:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    move-object v5, v6

    .line 744
    :goto_0
    return-object v5

    .line 666
    :cond_1
    const/4 v4, 0x1

    .line 667
    .local v4, "shouldFinish":Z
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 668
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    const/4 v7, 0x2

    iput v7, v5, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mTrimmingStatus:I

    .line 671
    :try_start_0
    const-string v5, "VNTrimAudioUtil"

    const-string v7, "**** AudioEditorImpl export() call."

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1900(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mOutput:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;-><init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)V

    invoke-interface {v5, v7, v8}, Lcom/lifevibes/videoeditor/VideoEditor;->export(Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V

    .line 694
    const/4 v4, 0x0

    .line 695
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    const/4 v7, 0x3

    iput v7, v5, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mTrimmingStatus:I

    .line 696
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->start:J

    .line 697
    const-string v5, "VNTrimAudioUtil"

    const-string v7, "**** AudioEditorImpl export() done."

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 733
    :cond_2
    :goto_1
    if-eqz v4, :cond_6

    .line 734
    :try_start_1
    const-string v5, "VNTrimAudioUtil"

    const-string v7, "**** export() - exception occurred"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 738
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 739
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v7, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    goto :goto_0

    .line 698
    :catch_0
    move-exception v1

    .line 699
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_2
    const-string v5, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEditorImpl export() : IllegalArgumentException\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 738
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v5

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 739
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v7, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    throw v5

    .line 701
    :catch_1
    move-exception v1

    .line 702
    .local v1, "ex":Ljava/lang/IllegalStateException;
    :try_start_3
    const-string v5, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEditorImpl export() : IllegalStateException\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 705
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 706
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v5, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEditorImpl export() : RuntimeException\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "="

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    .line 710
    .local v3, "msgItem":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 712
    .local v0, "code":I
    :try_start_4
    array-length v5, v3

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 713
    const-string v5, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEditorImpl export() : RuntimeException\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 719
    :goto_2
    const v5, 0x41100001    # 9.000001f

    if-eq v0, v5, :cond_3

    const v5, -0x7e71fff9

    if-ne v0, v5, :cond_4

    .line 720
    :cond_3
    const/16 v5, 0x88

    :try_start_5
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v5

    .line 738
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 739
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v7, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    goto/16 :goto_0

    .line 715
    :catch_3
    move-exception v2

    .line 716
    .local v2, "ex2":Ljava/lang/NumberFormatException;
    :try_start_6
    const-string v5, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEditorImpl export() : NumberFormatException\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 723
    .end local v2    # "ex2":Ljava/lang/NumberFormatException;
    :cond_4
    const v5, -0x7e82ffaf

    if-ne v0, v5, :cond_5

    .line 724
    const/16 v5, 0x89

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v5

    .line 738
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 739
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v7, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    goto/16 :goto_0

    .line 726
    :cond_5
    const v5, 0x40000001    # 2.0000002f

    if-ne v0, v5, :cond_2

    .line 727
    const/16 v5, 0x8a

    :try_start_7
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v5

    .line 738
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 739
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v7, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    goto/16 :goto_0

    .line 729
    .end local v0    # "code":I
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    .end local v3    # "msgItem":[Ljava/lang/String;
    :catch_4
    move-exception v1

    .line 730
    .local v1, "ex":Ljava/io/IOException;
    :try_start_8
    const-string v5, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEditorImpl export() : IOException\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 738
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 739
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    .line 741
    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mCompleted:Z

    if-eqz v5, :cond_7

    .line 742
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    move-result-object v5

    invoke-interface {v5, v10}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;->onCompletion(I)V

    .line 744
    :cond_7
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 644
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v2, 0x0

    .line 749
    const-string v0, "VNTrimAudioUtil"

    const-string v1, "**** ActualExportThread onPostExecute"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    iput v2, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mTrimmingStatus:I

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 755
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_1

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;->onError(II)V

    .line 758
    :cond_1
    const-string v0, "VNTrimAudioUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** total trimming time :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->start:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 761
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 644
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
