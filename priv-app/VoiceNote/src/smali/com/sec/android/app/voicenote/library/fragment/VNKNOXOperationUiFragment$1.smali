.class Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "VNKNOXOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->registerBroadcastReceiverSecretMode(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 203
    const-string v11, "PACKAGENAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 205
    .local v8, "packagename":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    const-string v11, "com.sec.knox.container.FileRelayExist"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 210
    const-string v11, "com.sec.knox.container.FileRelayDone"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 211
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v11, v11, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_0

    .line 212
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # operator++ for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mMovedCount:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$008(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I

    .line 213
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    goto :goto_0

    .line 215
    :cond_2
    const-string v11, "com.sec.knox.container.FileRelayFail"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 216
    const-string v11, "ERRORCODE"

    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 217
    .local v4, "errorcode":I
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->errorDisplay(I)V
    invoke-static {v11, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)V

    goto :goto_0

    .line 218
    .end local v4    # "errorcode":I
    :cond_3
    const-string v11, "com.sec.knox.container.FileRelayFail2"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 219
    const-string v11, "ERROR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "error":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->errorDisplay20(Ljava/lang/String;)V
    invoke-static {v11, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 221
    .end local v3    # "error":Ljava/lang/String;
    :cond_4
    const-string v11, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 222
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    const-string v12, "PROGRESS"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I
    invoke-static {v11, v12}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$402(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)I

    .line 223
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    goto/16 :goto_0

    .line 224
    :cond_5
    const-string v11, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 225
    const-string v11, "SUCCESSCNT"

    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 227
    .local v2, "cnt":I
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const-string v12, "persona"

    invoke-virtual {v11, v12}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PersonaManager;

    .line 228
    .local v7, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v7}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v9

    .line 229
    .local v9, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    const/4 v1, 0x0

    .line 231
    .local v1, "KNOXTitle":Ljava/lang/String;
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PersonaInfo;

    .line 232
    .local v6, "info":Landroid/content/pm/PersonaInfo;
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXTargetId:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I

    move-result v11

    invoke-virtual {v6}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v12

    if-ne v11, v12, :cond_6

    goto :goto_1

    .line 238
    .end local v6    # "info":Landroid/content/pm/PersonaInfo;
    :cond_7
    const/4 v10, 0x0

    .line 239
    .local v10, "toastText":Ljava/lang/String;
    const/4 v11, 0x1

    if-ne v2, v11, :cond_9

    .line 240
    const v11, 0x7f0b0153

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v1, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 242
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 249
    :cond_8
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    goto/16 :goto_0

    .line 243
    :cond_9
    const/4 v11, 0x1

    if-le v2, v11, :cond_8

    .line 244
    const v11, 0x7f0b0152

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 246
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_2
.end method
