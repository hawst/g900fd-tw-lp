.class Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$1;
.super Ljava/lang/Object;
.source "VNSelectLabelDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 116
    move-object v2, p1

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 117
    .local v1, "positiveButton":Landroid/widget/Button;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "arg0":Landroid/content/DialogInterface;
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 120
    .local v0, "negativeButton":Landroid/widget/Button;
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    return-void
.end method
