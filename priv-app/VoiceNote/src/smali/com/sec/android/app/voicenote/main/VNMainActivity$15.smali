.class Lcom/sec/android/app/voicenote/main/VNMainActivity$15;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 4566
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled(Ljava/util/ArrayList;I)V
    .locals 2
    .param p2, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 4582
    .local p1, "remainList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4702(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;)Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .line 4583
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4584
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 4585
    :cond_0
    return-void
.end method

.method public onCompleted(IILjava/lang/String;)V
    .locals 2
    .param p1, "totalCount"    # I
    .param p2, "doneCount"    # I
    .param p3, "searchText"    # Ljava/lang/String;

    .prologue
    .line 4571
    if-ne p1, p2, :cond_0

    .line 4572
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onDoSearch(Ljava/lang/String;)V

    .line 4574
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->msearchOperationTask:Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4702(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;)Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;

    .line 4575
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4576
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$15;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchProgressbar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 4577
    :cond_1
    return-void
.end method
