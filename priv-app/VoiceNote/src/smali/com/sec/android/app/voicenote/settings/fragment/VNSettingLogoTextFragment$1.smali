.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;
.super Ljava/lang/Object;
.source "VNSettingLogoTextFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 267
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 258
    .local v0, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 259
    sget-boolean v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->misInputMethodShown:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_1

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 261
    const-string v1, "VNSettingLogoTextFragment"

    const-string v2, "showSoftInput"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 265
    const-string v1, "VNSettingLogoTextFragment"

    const-string v2, "hideSoftInputFromWindow"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
