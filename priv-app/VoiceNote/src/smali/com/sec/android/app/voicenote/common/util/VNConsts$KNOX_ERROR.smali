.class public final Lcom/sec/android/app/voicenote/common/util/VNConsts$KNOX_ERROR;
.super Ljava/lang/Object;
.source "VNConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KNOX_ERROR"
.end annotation


# static fields
.field public static final FAIL_ERROR_CODE_CANCELED:I = 0x2

.field public static final FAIL_ERROR_CODE_CONTAINER_STATE_PROBLEM:I = 0x1

.field public static final FAIL_ERROR_CODE_FILE_MOVE_FAIL:I = 0x8

.field public static final FAIL_ERROR_CODE_FILE_MOVE_ONGOING:I = 0x9

.field public static final FAIL_ERROR_CODE_NOT_ALLOWED_FILENAME:I = 0x5

.field public static final FAIL_ERROR_CODE_NOT_ALLOWED_SRCPATH:I = 0x4

.field public static final FAIL_ERROR_CODE_NOT_ERROR:I = 0x0

.field public static final FAIL_ERROR_CODE_SRCDIR_REMOVE_FAIL:I = 0x3

.field public static final FAIL_ERROR_CODE_SRC_NOT_EXIST:I = 0x6

.field public static final FAIL_ERROR_CODE_STORAGE_FULL:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
