.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;
.super Landroid/os/AsyncTask;
.source "VNSettingLogoTextFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;

    .prologue
    .line 551
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 551
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getInstanceFontListAdapter(Landroid/content/Context;)Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    move-result-object v1

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1402(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;)Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1400(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->loadTypefaces()V

    .line 564
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 551
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "unused"    # Ljava/lang/Void;

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->finishCreateView()V

    .line 570
    invoke-static {}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->destroyInstance()V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1300(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1300(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1302(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1500(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1500(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->enableDoneButton()Z
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Z

    move-result v1

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$902(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Z)Z

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z
    invoke-static {v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$900(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Z

    move-result v2

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Button;Z)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1500(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Button;Z)V

    .line 582
    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    .line 555
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0141

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 556
    .local v1, "sTitle":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 557
    .local v0, "sMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v3, v1, v0, v4, v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v3

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1302(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 558
    return-void
.end method
